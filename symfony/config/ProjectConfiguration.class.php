<?php

require_once dirname(__FILE__) . '/../lib/vendor/symfony/lib/autoload/sfCoreAutoload.class.php';
sfCoreAutoload::register();

class ProjectConfiguration extends sfProjectConfiguration {
    public function setup() {
        $this->enablePlugins('sfDoctrinePlugin');
        $this->enablePlugins('sfDoctrineGuardPlugin');
        $this->enablePlugins('sfFormExtraPlugin');
        //    $this->enablePlugins('sfMediaBrowserPlugin');
        $this->enablePlugins('sfImageTransformPlugin');
        $this->enablePlugins('sfWidgetFormInputSWFUploadPlugin');
        //    $this->enablePlugins('sfJQueryUIPlugin');
        $this->enablePlugins('sfCaptchaGDPlugin');
        //$this->enablePlugins('gyCaptchaPlugin');
        $this->setWebDir($this->getRootDir() . '/../www');

        $this->dispatcher->connect('context.load_factories', array($this, 'detectSite'));
    }

    /**
     * Detects the current site based on the url
     * Fallback to the main site (main = 1 in database) if we can't find a suitable entry
     *
     * @param sfEvent $event
     */
    public function detectSite(sfEvent $event) {
        $request = sfContext::getInstance()->getRequest();

        $domain = $request->getHost();

        // remove www. from begin if there
        if (!strcasecmp(substr($domain, 0, 4), "www.")) {
            $domain = substr($domain, 4);
            // permament redirect (no www)
            header("HTTP/1.1 301 Moved Permanently");
            header("Location: http://$domain" . $_SERVER['REQUEST_URI']);
            exit;
        }
        if (!strcasecmp(substr($domain, 0, 4), "dev.")) {
            $domain = substr($domain, 4);
        }

        $siteTable = Doctrine::getTable('SmsDomain');

        $site = $siteTable->retrieveByDomain($domain);
        if($site===false) {
            if (!strcasecmp(substr($domain, -4), 'test')) {
                $alt_domain_name = substr($domain, 0, -4) . "com";
            }
            if (!strcasecmp(substr($domain, -3), 'dev')) {
                $alt_domain_name = substr($domain, 0, -3) . "com";
            }
            if($alt_domain_name) {
                $site = $siteTable->retrieveByDomain($alt_domain_name);
            }
        }

        if ($site !== false) {
            $siteTable->setCurrent($site);
        }
        else {
            $site = $siteTable->retrieveMain();
            $siteTable->setCurrent($site);
        }
        $_SESSION['domain_id'] = $site->getDomainId();
        $_SESSION['domain_name'] = $site->getDomainName();
        sfConfig::set('verse_domain_tmp_dir', sfConfig::get('sf_web_dir') . "/files/" . $site->getDomainName() . '/tmp');
        sfConfig::set('verse_domain_movies_dir', sfConfig::get('sf_web_dir') . "/files/" . $site->getDomainName() . '/movies');
        sfConfig::set('verse_domain_dir', sfConfig::get('sf_web_dir') . "/files/" . $site->getDomainName());

        $obituary_id = $request->getParameter('obituary_id', 0);
        if ($obituary_id) {
            $obituaryTable = Doctrine::getTable('PlgObituary');

            $obituary = $obituaryTable->retrieveByObituaryId($obituary_id);
            if ($obituary) {
                $obituaryTable->setCurrent($obituary);
            }
        }
        else {

        }
    }
}

// The following is free for any use provided credit is given where due.
// This code comes with NO WARRANTY of any kind, including any implied warranty.

/**
 * MySQL "OLD_PASSWORD()" AKA MySQL323 HASH FUNCTION
 * This is the password hashing function used in MySQL prior to version 4.1.1
 * By Rev. Dustin Fineout 10/9/2009 9:12:16 AM
 **/
function mysql_old_password_hash($input, $hex = true) {
    $nr = 1345345333;
    $add = 7;
    $nr2 = 0x12345671;
    $tmp = null;
    $inlen = strlen($input);
    for ($i = 0; $i < $inlen; $i++) {
        $byte = substr($input, $i, 1);
        if ($byte == ' ' || $byte == "\t") continue;
        $tmp = ord($byte);
        $nr ^= ((($nr & 63) + $add) * $tmp) + (($nr << 8) & 0xFFFFFFFF);
        $nr2 += (($nr2 << 8) & 0xFFFFFFFF) ^ $nr;
        $add += $tmp;
    }
    $out_a = $nr & 2147483647; // ((1 << 31) - 1);
    $out_b = $nr2 & 2147483647; // ((1 << 31) - 1);
    $output = sprintf("%08x%08x", $out_a, $out_b);
    if ($hex) return $output;
    return hex_hash_to_bin($output);
} //END function mysql_old_password_hash

/**
 * Computes each hexidecimal pair into the corresponding binary octet.
 * Similar to mysql hex2octet function.
 **/
function hex_hash_to_bin($hex) {
    $bin = "";
    $len = strlen($hex);
    for ($i = 0; $i < $len; $i += 2) {
        $byte_hex = substr($hex, $i, 2);
        $byte_dec = hexdec($byte_hex);
        $byte_char = chr($byte_dec);
        $bin .= $byte_char;
    }
    return $bin;
} //END function hex_hash_to_bin

