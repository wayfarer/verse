<?php
/*
 * (c) 2008 - 2009 GOYELLO IT Services
 * (c) 2008 - 2009 Jakub Zalas
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

require_once(dirname(__FILE__) . '/../lib/BasegyCaptchaActions.class.php');

/**
 * gyCaptcha actions.
 *
 * @package    gyCaptchaPlugin
 * @subpackage action
 * @author     Jakub Zalas <jakub.zalas@goyello.nl>
 * @version    SVN: $Id$
 */
class gyCaptchaActions extends BasegyCaptchaActions
{
}
