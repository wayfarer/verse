function gyCaptcha_toggleImage(inputId, img) {
  var input = document.getElementById(inputId);
  if (input && img) {
    if (input.value == 1) {
      input.value = 0;
      img.className = 'gyCaptchaUnselected';
    } else {
      input.value = 1;
      img.className = 'gyCaptchaSelected';
    }
  }
}

