<?php
/*
 * (c) 2008 - 2009 GOYELLO IT Services
 * (c) 2008 - 2009 Jakub Zalas
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Image based captcha (YELLOcaptcha).
 *
 * @package     gyCaptchaPlugin
 * @subpackage  lib
 * @author      Jakub Zalas <jakub@zalas.pl>
 * @version     SVN: $Id$
 */
class gyCaptcha
{
  /**
   * Captcha instance
   *
   * @var gyCaptcha
   */
  private static $instance = null;

  /**
   * Returns singleton captcha instance
   *
   * @return gyCaptcha
   */
  public static function getInstance()
  {
    if (is_null(self::$instance))
    {
      self::$instance = new gyCaptcha();
    }

    return self::$instance;
  }

  /**
   * Initializes set of images
   *
   * Randomly registers category, valid and noise images.
   *
   * @param integer $size  Size of image set
   * @param integer $valid Number of required valid images
   * @param boolean
   */
  public function initialize($size, $valid)
  {
    if ($size <= $valid)
    {
      throw new sfInitializationException('Size of image set has to be greater than total number of valid images');
    }

    $category    = $this->getRandomCategory();
    $noiseImages = $this->getRandomImagesNotForCategory($size - $valid, $category);
    $validImages = $this->getRandomImagesForCategory($valid, $category);

    $this->setNoiseImages($noiseImages);
    $this->setValidImages($validImages);
    $this->setCurrentCategory($category);

    return true;
  }

  /**
   * Validates if given images are the right ones
   *
   * Array of images is indexed by public file name. Values can be either '1' or '0'.
   *
   * Example array of images:
   * <code>
   * array (
   *   '09d996cb906167464ad620192bdefebd.jpg' => 1,
   *   '38fbe048343578fb232b94802ff89cb5.jpg' => 0,
   *   '6c8417766e01534b985c21e11982f78b.jpg' => 1,
   *   '6e254586065f7cb3bc5923ccf47468ba.jpg' => 0,
   *   '89bdb9df5d4d4f459684dc6a4b834092.jpg' => 0
   * )
   * </code>
   *
   * '1' means that user selected it and '0' the opposite. Selection is only valid when:
   *  * all images selected during initialization are passed in array
   *  * there is no additional images 
   *  * only valid images are selected (value equals '1')
   *
   * @param array $images
   * @return boolean
   */
  public function validate($images)
  {
    $validImages = $this->getValidImages();
    $noiseImages = $this->getNoiseImages();
    $valid       = count($validImages);
    $size        = $valid + (count($noiseImages));

    $properlySelected = 0;

    foreach ($images as $fileName => $selected)
    {
      if ($selected && (!array_key_exists($fileName, $validImages) || array_key_exists($fileName, $noiseImages)))
      {
        return false;
      }
      elseif (!$selected && (!array_key_exists($fileName, $noiseImages) || array_key_exists($fileName, $validImages)))
      {
        return false;
      }

      $properlySelected++;
    }

    if ($size != $properlySelected || $size != count($images))
    {
      return false;
    }

    return true;
  }

  /**
   * Returns path to the image based on its public file name
   * 
   * If image real image name is not found or file doesn't exist null is returned.
   *
   * @param string $fileName Public file name
   * @return string|null
   */
  public function getImagePath($fileName)
  {
    $image    = $this->getImage($fileName);
    $filePath = $this->getImagesDir() . DIRECTORY_SEPARATOR . $image;

    if (!$image || !file_exists($filePath))
    {
      return null;
    }

    return $filePath;
  }

  /**
   * Returns random category name
   *
   * @return string
   */
  protected function getRandomCategory()
  {
    $categories = $this->getCaptchaCategories();
    $category   = array_rand($categories);

    return $category;
  }

  /**
   * Returns question for given category name
   *
   * Once i18n is enabled question is translated.
   *
   * @return string
   */
  public function getCategoryQuestion($category)
  {
    $categories = $this->getCaptchaCategories();

    $question = isset($categories[$category]['question']) ? $categories[$category]['question'] : null;

    if ($question && sfConfig::get('sf_i18n', false))
    {
      $question = sfContext::getInstance()->getI18N()->__($question, null, 'gyCaptcha');
    }

    return $question;
  }

  /**
   * Returns question for current category
   *
   * @return string
   */
  public function getCurrentCategoryQuestion()
  {
    $category = $this->getCurrentCategory();

    return $this->getCategoryQuestion($category);
  }

  /**
   * Gets random images for given category name
   *
   * @param integer $howMany Number of images
   * @param string $category Category name
   * @return array
   */
  protected function getRandomImagesForCategory($howMany, $category)
  {
    $categories = $this->getCaptchaCategories();
    $indexes    = array_rand($categories[$category]['images'], $howMany);
    $images     = array();

    foreach ($indexes as $i => $index)
    {
      $realFileName = $categories[$category]['images'][$index];
      $fileName = $this->generateFileName($realFileName);
      $images[$fileName] = $realFileName;
    }

    return $images;
  }

  /**
   * Gets random imagess from all categories but the given one
   * 
   * @param integer $howMany Number of images
   * @param string $category Category name to be excluded
   * @return array
   */
  protected function getRandomImagesNotForCategory($howMany, $excludedCategory)
  {
    $categories = $this->getCaptchaCategories();
    unset($categories[$excludedCategory]);

    $noiseImages = array();
    foreach ($categories as $category => $data)
    {
      if (isset($data['images']))
      {
        $noiseImages = array_merge($noiseImages, $data['images']);
      }
    }

    $indexes = array_rand($noiseImages, $howMany);
    $images  = array();
    foreach ($indexes as $i => $index)
    {
      $realFileName = $noiseImages[$index];
      $fileName = $this->generateFileName($realFileName);
      $images[$fileName] = $realFileName;
    }

    return $images;
  }

  /**
   * Generates public file name for image
   *
   * @param string $realFileName Real file name
   * @return string
   */
  protected function generateFileName($realFileName)
  {
    $salt      = sfConfig::get('app_gyCaptcha_salt', 'a9c8ae334de23c1c030699209d3395cd');
    $fileName  = md5(time() . rand(0, 100) . $salt . $realFileName);
    $extension = substr($realFileName, strrpos($realFileName, '.'));

    return $fileName . $extension;
  }

  /**
   * Returns full path to the directory with images
   *
   * @return string
   */
  public function getImagesDir()
  {
    return sfConfig::get('app_gyCaptcha_images_dir', sfConfig::get('sf_uploads_dir') . DIRECTORY_SEPARATOR . 'gyCaptchaPlugin' . DIRECTORY_SEPARATOR . 'images');
  }

  /**
   * Returns array of captcha categories with images and questions
   *
   * Example:
   * <code>
   * array(
   *   'airplane' => array(
   *     'question' => 'Select images with airplane',
   *     'images'   => array('airplane_01.jpg', 'airplane_02.jpg')
   *   ),
   *   'apple' => array(
   *     'question' => 'Select images with apple',
   *     'images'   => array('apple_01.jpg', 'apple_02.jpg')
   *   )
   * )
   * </code>
   *
   * @return array
   */
  public function getCaptchaCategories()
  {
    return sfConfig::get('app_gyCaptcha_categories', array());
  }

  /**
   * Sets noise images
   *
   * @param array $noiseImages
   */
  public function setNoiseImages($noiseImages)
  {
    $this->setAttribute('noise_images', $noiseImages);
  }

  /**
   * Returns noise images
   *
   * @return array
   */
  public function getNoiseImages()
  {
    return $this->getAttribute('noise_images');
  }

  /**
   * Sets valid images
   *
   * @param array $validImages
   */
  public function setValidImages($validImages)
  {
    $this->setAttribute('valid_images', $validImages);
  }

  /**
   * Returns valid images
   *
   * @return array
   */
  public function getValidImages()
  {
    return $this->getAttribute('valid_images');
  }

  /**
   * Returns all images sorted by name
   *
   * As name is random sorting is easy way to return images in random order.
   *
   * @return array
   */
  public function getImages()
  {
    $images = array_merge($this->getValidImages(), $this->getNoiseImages());
    ksort($images);
    
    return $images;
  }

  /**
   * Returns real image name based on its public name
   *
   * @param string $fileName
   * @return string
   */
  public function getImage($fileName)
  {
    $images = $this->getImages();

    return isset($images[$fileName]) ? $images[$fileName] : null;
  }

  /**
   * Sets current category
   *
   * @param string $category
   */
  public function setCurrentCategory($category)
  {
    $this->setAttribute('category', $category);
  }

  /**
   * Returns current category
   *
   * @return string
   */
  public function getCurrentCategory()
  {
    return $this->getAttribute('category');
  }

  /**
   * Gets user session
   *
   * @return sfUser
   */
  public function getUser()
  {
    return sfContext::getInstance()->getUser();
  }

  /**
   * Sets attribute in the user session
   *
   * @param string $name Attribute name
   * @param mixed $value Attribute value
   * @param string $namespace Attribute namespace
   */
  public function setAttribute($name, $value, $namespace = 'gyCaptcha')
  {
    $this->getUser()->setAttribute($name, $value, $namespace);
  }

  /**
   * Returns value of attribute from user session
   *
   * @param string $name Attribute name
   * @param string $value Default value 
   * @param string $namespace Attribute namespace
   * @return mixed
   */
  public function getAttribute($name, $default = null, $namespace = 'gyCaptcha')
  {
    return $this->getUser()->getAttribute($name, $default, $namespace);
  }
}

