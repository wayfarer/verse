<?php
/*
 * (c) 2008 - 2009 GOYELLO IT Services
 * (c) 2008 - 2009 Jakub Zalas
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Captcha widget
 *
 * <code>
 * class CommentForm extends sfForm
 * {
 *   public function configure()
 *   {
 *     $this->setWidgets(array(
 *       'comment_author'  => new sfWidgetFormInput(),
 *       'comment_content' => new sfWidgetFormTextarea(),
 *       'captcha'         => new gyWidgetFormCaptcha(array('size' => 5, 'valid' => 2))
 *     ));
 *
 *     $this->setValidators(array(
 *       'comment_author'   => new sfValidatorString(array('max_length' => 255)),
 *       'comment_content'  => new sfValidatorString(),
 *       'captcha'          => new gyValidatorCaptcha()
 *     ));
 *   }
 * }
 * </code>
 *
 * @package     gyCaptchaPlugin
 * @subpackage  widget
 * @author      Jakub Zalas <jakub@zalas.pl>
 * @version     SVN: $Id$
 */
class gyWidgetFormCaptcha extends sfWidgetForm
{
  /**
   * Indicates if captcha was initialized
   * For some reason it has to be static. Otherwise state is not
   * kept between rendering methods (probably because of cloning the objects).
   * @var boolean
   */
  private static $captchaInitialized = false;

  /**
   * Constructor.
   *
   * Available options:
   *  * size:  The size of image set
   *  * valid: The number of images that should be valid
   *
   * @param array $options     An array of options
   * @param array $attributes  An array of default HTML attributes
   *
   * @see sfWidget
   */
  public function __construct($options = array(), $attributes = array())
  {
    $this->addRequiredOption('size');
    $this->addRequiredOption('valid');

    parent::__construct($options, $attributes);
  }

  /**
   * Renders the widget as HTML (img and input hidden tags).
   *
   * Images need to be refreshed before every rendering. This is
   * accomplished by requesting captcha object which is initialized
   * with first request.
   *
   * @param  string $name       The name of the HTML widget
   * @param  mixed  $value      The value of the widget
   * @param  array  $attributes An array of HTML attributes
   * @param  array  $errors     An array of errors
   *
   * @return string A HTML representation of the widget
   */
  public function render($name, $value = null, $attributes = array(), $errors = array())
  {
    // Refresh images before every rendering
    sfContext::getInstance()->getConfiguration()->loadHelpers(array('Url'));

    $img   = '';
    $input = '';

    foreach ($this->getCaptcha()->getImages() as $fileName => $image)
    {
      $inputName = $name . '[' . $fileName . ']';
      $inputId   = $this->generateId($inputName);

      $input.= $this->renderTag('input', array(
        'type'  => 'hidden', 
        'name'  => $inputName, 
        'id'    => $inputId, 
        'value' => 0
      ));

      $img.= $this->renderTag('img', array(
        'class'   => 'gyCaptchaUnselected', 
        'src'     => '/frontend.php'.url_for('@gyCaptcha_show?image_name='. $fileName),
        'alt'     => 'Captcha Image', 
        'onclick' => '%TOGGLE_IMAGE%;'
      ));

      // renderTag escapes quotes
      $img = str_replace('%TOGGLE_IMAGE%', 'gyCaptcha_toggleImage(\'' . $inputId . '\', this)', $img);
    }

    return '<div class="gyCaptcha">' . $img . $input . '</div>';
  }

  /**
   * Returns captcha question as widget's label
   *
   * @return string
   */
  public function getLabel()
  {
    return $this->getCaptcha()->getCurrentCategoryQuestion();
  }

  /**
   * Returns captcha object
   *
   * First time it's  requested it is initialized. Captcha needs 
   * to be initialized only if form is rendered. Otherwise 
   * validation will fail.
   */
  protected function getCaptcha()
  {
    if (false == $this->isCaptchaInitialized())
    {
      gyCaptcha::getInstance()->initialize($this->getOption('size'), $this->getOption('valid'));
      self::$captchaInitialized = true;
    }

    return gyCaptcha::getInstance();
  }

  /**
   * Checks if captcha was initialized
   *
   * @return boolean
   */
  public function isCaptchaInitialized()
  {
    return self::$captchaInitialized;
  }

  /**
   * Gets the stylesheet paths associated with the widget.
   *
   * @return array An array of stylesheet paths
   */
  public function getStylesheets()
  {
    return array('/gyCaptchaPlugin/css/gyCaptcha.css' => 'screen');
  }

  /**
   * Gets the JavaScript paths associated with the widget.
   *
   * @return array An array of JavaScript paths
   */
  public function getJavaScripts()
  {
    return array('/gyCaptchaPlugin/js/gyCaptcha.js');
  }
}

