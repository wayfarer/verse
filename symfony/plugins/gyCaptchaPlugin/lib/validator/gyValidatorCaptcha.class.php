<?php
/*
 * (c) 2008 - 2009 GOYELLO IT Services
 * (c) 2008 - 2009 Jakub Zalas
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Captcha validator
 *
 * @package     gyCaptchaPlugin
 * @subpackage  validator
 * @author      Jakub Zalas <jakub@zalas.pl>
 * @version     SVN: $Id$
 */
class gyValidatorCaptcha extends sfValidatorBase
{
  /**
   * Configures the current validator.
   *
   * @param array $options   An array of options
   * @param array $messages  An array of error messages
   *
   * @see __construct()
   */
  protected function configure($options = array(), $messages = array())
  {
    $this->setMessage('invalid', 'Invalid selection');
  }

  /**
   * Validates if selected images are the right ones
   *
   * @see sfValidatorChoice
   */
  protected function doClean($value)
  {
    if (!gyCaptcha::getInstance()->validate($value))
    {
      throw new sfValidatorError($this, 'invalid', array('value' => $value));
    }
    
    return $value;
  }
}
