[?php if ($value): ?]
  [?php echo image_tag('/img/enabled.png', array('alt' => __('Enabled', array(), 'sf_admin'), 'title' => __('Enabled', array(), 'sf_admin'))) ?]
[?php else: ?]
  [?php echo image_tag('/img/disabled.png', array('alt' => __('Disabled', array(), 'sf_admin'), 'title' => __('Disabled', array(), 'sf_admin'))) ?]
[?php endif; ?]
