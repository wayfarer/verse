<?php

class verseMediaUploadValidator extends sfValidatorBase {

    public function clean($value) {
        return $this->doClean($value);
    }

    protected function doClean($value) {
        $files = sfContext::getInstance()->getUser()->getAttribute('files', array()); //get uploaded files from $_SESSION
        if (!count($files)) {
             throw new sfValidatorError($this, 'Please upload at least one file.');
        }
    }
}
