<?php

class verseDomainEmailValidator extends sfValidatorEmail
{
  public function configure($options = array(), $messages = array())
  {
    parent::configure($options, $messages);

    $this->addOption('email_field', 'email');
    $this->addOption('domain_type_field', 'domain_type');
    $this->addOption('throw_global_error', false);

    $this->setMessage('invalid', 'Valid email is required for the normal domain');
  }

  protected function doClean($values)
  {
    if($values[$this->getOption('domain_type_field')]==1) return $values;
    $values[$this->getOption('email_field')] = parent::doClean($values[$this->getOption('email_field')]);
    return $values;
  }
}
