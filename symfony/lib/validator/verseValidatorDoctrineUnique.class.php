<?php

class verseValidatorDoctrineUnique extends sfValidatorDoctrineUnique {
  protected function doClean($values) {
    $originalValues = $values;
    $columns = $this->getOption('column');
    if(!is_array($values)) {
      $values = array($columns[0] => $values);
    }
    $notempty = false;
    // if all column values is empty (null) - pass it, it do not corrupt uniqeness
    foreach ($columns as $column) {
      if($values[$column]) {
        $notempty = true;
      }
    }
    if($notempty) {
      return parent::doClean($originalValues);
    }
    else {
      return $originalValues;
    }
  }
}
