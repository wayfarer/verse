<?php

class verseValidatorUser extends sfGuardValidatorUser
{
  protected function doClean($values)
  {
    $username = isset($values[$this->getOption('username_field')]) ? $values[$this->getOption('username_field')] : '';
    $password = isset($values[$this->getOption('password_field')]) ? $values[$this->getOption('password_field')] : '';

    // user exists?
    if ($username) {
      $users = $this->getTable()->createQuery()->where('username = ?', $username)->execute();
      foreach($users as $user) {
        // password is ok?
        if ($user->getIsActive() && $user->checkPassword($password)) {
          return array_merge($values, array('user' => $user));
        }
      }
    }

    if ($this->getOption('throw_global_error'))
    {
      throw new sfValidatorError($this, 'invalid');
    }

    throw new sfValidatorErrorSchema($this, array($this->getOption('username_field') => new sfValidatorError($this, 'invalid')));
  }
}
