<?php

/**
 * PlgMovieclip filter form.
 *
 * @package    verse3
 * @subpackage filter
 * @author     Vladimir Droznik
 * @version    SVN: $Id: sfDoctrineFormFilterTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class PlgMovieclipFormFilter extends BasePlgMovieclipFormFilter
{
  public function configure()
  {
    // statuses
    $this->widgetSchema['status'] = new sfWidgetFormChoice(array(
      'choices' => PlgMovieclipTable::getStatusTexts(),
      'expanded' => true,
      'multiple' => true
    ));
    $this->validatorSchema['status'] = new sfValidatorChoice(array(
      'choices' => array_keys(PlgMovieclipTable::getStatusTexts()),
      'multiple' => true,
      'required' => false
    ));
  }

  public function getFields()
  {
    $fields = parent::getFields();
    // in order for status dropdown work
    $fields['status'] = 'ForeignKey';
    return $fields;
  }
}
