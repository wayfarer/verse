<?php

/**
 * PlgSubscribe filter form.
 *
 * @package    verse3
 * @subpackage filter
 * @author     Vladimir Droznik
 * @version    SVN: $Id: sfDoctrineFormFilterTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class PlgSubscribeFormFilter extends BasePlgSubscribeFormFilter
{
  public function configure()
  {
    $this->widgetSchema['created_at'] = new sfWidgetFormFilterDate(array(
      'from_date' => new sfWidgetFormDateJQueryUI(array("year_range"=>"2009:c")),
      'to_date' => new sfWidgetFormDateJQueryUI(array("year_range"=>"2009:c")),
      'with_empty' => false
    ));
  }
}
