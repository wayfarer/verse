<?php

/**
 * PlgObituarySubscribe filter form.
 *
 * @package    verse3
 * @subpackage filter
 * @author     Vladimir Droznik
 * @version    SVN: $Id: sfDoctrineFormFilterTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class PlgObituarySubscribeFormFilter extends BasePlgObituarySubscribeFormFilter
{
  public function configure() {
    $this->widgetSchema['obituary_id']->addOption('order_by', array('last_name', 'asc'))->addOption('method', 'getNameForSelect')->addOption('add_empty', '--- All ---');
    // enabled checkboxes
    $this->widgetSchema['enabled'] = new sfWidgetFormChoice(array(
      'choices' => array(0=>'off', 1=>'on'),
      'expanded' => true,
      'multiple' => true
    ));

    $this->validatorSchema['enabled'] = new sfValidatorChoice(array(
      'choices' => array(0, 1),
      'required' => false,
      'multiple' => true
    ));

    // attach dropdown calendars for date fields
    $this->widgetSchema['created'] = new sfWidgetFormFilterDate(array(
      'from_date' => new sfWidgetFormDateJQueryUI(array("year_range"=>"2008:c")),
      'to_date' => new sfWidgetFormDateJQueryUI(array("year_range"=>"2008:c")),
      'with_empty' => false
    ));
  }

  public function getFields() {
    $fields = parent::getFields();
    // in order for enabled field work
    $fields['enabled'] = 'ForeignKey';
    return $fields;
  }

}
