<?php

/**
 * PlgProduct filter form.
 *
 * @package    verse3
 * @subpackage filter
 * @author     Vladimir Droznik
 * @version    SVN: $Id: sfDoctrineFormFilterTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class PlgProductFormFilter extends BasePlgProductFormFilter
{
  public function configure()
  {
    $this->widgetSchema['domain_id'] = new sfWidgetFormChoice(array(
      'choices' => Doctrine::getTable('PlgProduct')->getDomainsForSelect(true),
      'expanded' => false,
      'multiple' => false
    ));

    // enabled checkboxes
    $this->widgetSchema['enabled'] = new sfWidgetFormChoice(array(
      'choices' => array(0=>'off', 1=>'on'),
      'expanded' => true,
      'multiple' => true
    ));

    $this->validatorSchema['enabled'] = new sfValidatorChoice(array(
      'choices' => array(0, 1),
      'required' => false,
      'multiple' => true
    ));

    // product enabled checkboxes for domain_product module
    $this->widgetSchema['product_enabled'] = new sfWidgetFormChoice(array(
      'choices' => array(0=>'off', 1=>'on'),
      'expanded' => true,
      'multiple' => true
    ));

    $this->validatorSchema['product_enabled'] = new sfValidatorChoice(array(
      'choices' => array(0, 1),
      'required' => false,
      'multiple' => true
    ));
  }

  public function getFields() {
    $fields = parent::getFields();
    // in order for enabled field work
    $fields['enabled'] = 'ForeignKey';
    return $fields;
  }

  // product enabled filter for domain_product module
  protected function addProductEnabledColumnQuery(Doctrine_Query $query, $field, $values) {
      $alias = $query->getRootAlias();
      $query->leftJoin($alias.'.DomainLinkProduct as l');

      if (in_array('0', $values) && !in_array('1', $values)) {
          $query->addWhere('l.product_id IS NULL OR l.product_id = ""');
      }
      if (in_array('1', $values) && !in_array('0', $values)) {
          $query->addWhere('l.product_id IS NOT NULL AND l.product_id <> ""');
      }
  }
}
