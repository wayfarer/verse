<?php

/**
 * PlgObituaryCandle filter form.
 *
 * @package    verse3
 * @subpackage filter
 * @author     Vladimir Droznik
 * @version    SVN: $Id: sfDoctrineFormFilterTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class PlgObituaryCandleFormFilter extends BasePlgObituaryCandleFormFilter
{
  public function configure()
  {
    $this->widgetSchema['obituary_id']->addOption('order_by', array('last_name', 'asc'))->addOption('method', 'getNameForSelect')->addOption('add_empty', '--- All ---');
    $this->widgetSchema['name']->setOption('with_empty', false);
    $this->widgetSchema['email']->setOption('with_empty', false);
    $this->widgetSchema['ip']->setOption('with_empty', false);
    $this->widgetSchema['thoughts']->setOption('with_empty', false);

    // candle states
    $this->widgetSchema['state'] = new sfWidgetFormChoice(array(
      'choices' => PlgObituaryCandleTable::getCandlesStates(),
      'expanded' => true,
      'multiple' => true
    ));
    $this->validatorSchema['state'] = new sfValidatorChoice(array(
      'choices' => array_keys(PlgObituaryCandleTable::getCandlesStates()),
      'multiple' => true,
      'required' => false
    ));
    $this->widgetSchema['created_at'] = new sfWidgetFormFilterDate(array(
      'from_date' => new sfWidgetFormDateJQueryUI(array("year_range"=>"2000:c")),
      'to_date' => new sfWidgetFormDateJQueryUI(array("year_range"=>"2000:c")),
      'with_empty' => false
    ));

  }

  public function getFields()
  {
    $fields = parent::getFields();
  	// in order for state dropdown work
    $fields['state'] = 'ForeignKey';
    return $fields;
  }
}
