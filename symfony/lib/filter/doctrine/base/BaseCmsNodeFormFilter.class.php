<?php

/**
 * CmsNode filter form base class.
 *
 * @package    verse3
 * @subpackage filter
 * @author     Vladimir Droznik <vladimir.droznik@gmail.com>
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseCmsNodeFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'internal_name' => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'domain_id'     => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'page_id'       => new sfWidgetFormFilterInput(),
      'ord'           => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'depth'         => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'menu_type'     => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'display_name'  => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'params'        => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'page_id_old'   => new sfWidgetFormFilterInput(),
    ));

    $this->setValidators(array(
      'internal_name' => new sfValidatorPass(array('required' => false)),
      'domain_id'     => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'page_id'       => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'ord'           => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'depth'         => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'menu_type'     => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'display_name'  => new sfValidatorPass(array('required' => false)),
      'params'        => new sfValidatorPass(array('required' => false)),
      'page_id_old'   => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
    ));

    $this->widgetSchema->setNameFormat('cms_node_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'CmsNode';
  }

  public function getFields()
  {
    return array(
      'node_id'       => 'Number',
      'internal_name' => 'Text',
      'domain_id'     => 'Number',
      'page_id'       => 'Number',
      'ord'           => 'Number',
      'depth'         => 'Number',
      'menu_type'     => 'Number',
      'display_name'  => 'Text',
      'params'        => 'Text',
      'page_id_old'   => 'Number',
    );
  }
}
