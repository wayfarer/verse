<?php

/**
 * PlgProduct filter form base class.
 *
 * @package    verse3
 * @subpackage filter
 * @author     Vladimir Droznik <vladimir.droznik@gmail.com>
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BasePlgProductFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'guid'        => new sfWidgetFormFilterInput(),
      'domain_id'   => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'category_id' => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('ProductCategory'), 'add_empty' => true)),
      'name'        => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'price'       => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'quantity'    => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'image'       => new sfWidgetFormFilterInput(),
      'description' => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'sold_times'  => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'timestamp'   => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'enabled'     => new sfWidgetFormChoice(array('choices' => array('' => '', 1 => 'yes', 0 => 'no'))),
    ));

    $this->setValidators(array(
      'guid'        => new sfValidatorPass(array('required' => false)),
      'domain_id'   => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'category_id' => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('ProductCategory'), 'column' => 'category_id')),
      'name'        => new sfValidatorPass(array('required' => false)),
      'price'       => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
      'quantity'    => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'image'       => new sfValidatorPass(array('required' => false)),
      'description' => new sfValidatorPass(array('required' => false)),
      'sold_times'  => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'timestamp'   => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'enabled'     => new sfValidatorChoice(array('required' => false, 'choices' => array('', 1, 0))),
    ));

    $this->widgetSchema->setNameFormat('plg_product_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'PlgProduct';
  }

  public function getFields()
  {
    return array(
      'product_id'  => 'Number',
      'guid'        => 'Text',
      'domain_id'   => 'Number',
      'category_id' => 'ForeignKey',
      'name'        => 'Text',
      'price'       => 'Number',
      'quantity'    => 'Number',
      'image'       => 'Text',
      'description' => 'Text',
      'sold_times'  => 'Number',
      'timestamp'   => 'Number',
      'enabled'     => 'Boolean',
    );
  }
}
