<?php

/**
 * SmsDomainLink filter form base class.
 *
 * @package    verse3
 * @subpackage filter
 * @author     Vladimir Droznik <vladimir.droznik@gmail.com>
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseSmsDomainLinkFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'group_id'  => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('DomainLinkGroup'), 'add_empty' => true)),
      'domain_id' => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Domain'), 'add_empty' => true)),
    ));

    $this->setValidators(array(
      'group_id'  => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('DomainLinkGroup'), 'column' => 'id')),
      'domain_id' => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('Domain'), 'column' => 'domain_id')),
    ));

    $this->widgetSchema->setNameFormat('sms_domain_link_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'SmsDomainLink';
  }

  public function getFields()
  {
    return array(
      'id'        => 'Number',
      'group_id'  => 'ForeignKey',
      'domain_id' => 'ForeignKey',
    );
  }
}
