<?php

/**
 * PlgSubscribeToKeyword filter form base class.
 *
 * @package    verse3
 * @subpackage filter
 * @author     Vladimir Droznik <vladimir.droznik@gmail.com>
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BasePlgSubscribeToKeywordFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'subscribe_id' => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'keyword_id'   => new sfWidgetFormFilterInput(array('with_empty' => false)),
    ));

    $this->setValidators(array(
      'subscribe_id' => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'keyword_id'   => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
    ));

    $this->widgetSchema->setNameFormat('plg_subscribe_to_keyword_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'PlgSubscribeToKeyword';
  }

  public function getFields()
  {
    return array(
      'id'           => 'Number',
      'subscribe_id' => 'Number',
      'keyword_id'   => 'Number',
    );
  }
}
