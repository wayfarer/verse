<?php

/**
 * SmsDomainLinkGroup filter form base class.
 *
 * @package    verse3
 * @subpackage filter
 * @author     Vladimir Droznik <vladimir.droznik@gmail.com>
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseSmsDomainLinkGroupFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'is_shared_obits'      => new sfWidgetFormChoice(array('choices' => array('' => '', 1 => 'yes', 0 => 'no'))),
      'is_shared_pages'      => new sfWidgetFormChoice(array('choices' => array('' => '', 1 => 'yes', 0 => 'no'))),
      'is_shared_members'    => new sfWidgetFormChoice(array('choices' => array('' => '', 1 => 'yes', 0 => 'no'))),
      'is_shared_lists'      => new sfWidgetFormChoice(array('choices' => array('' => '', 1 => 'yes', 0 => 'no'))),
      'is_shared_products'   => new sfWidgetFormChoice(array('choices' => array('' => '', 1 => 'yes', 0 => 'no'))),
      'is_shared_movieclips' => new sfWidgetFormChoice(array('choices' => array('' => '', 1 => 'yes', 0 => 'no'))),
      'is_shared_structure'  => new sfWidgetFormChoice(array('choices' => array('' => '', 1 => 'yes', 0 => 'no'))),
    ));

    $this->setValidators(array(
      'is_shared_obits'      => new sfValidatorChoice(array('required' => false, 'choices' => array('', 1, 0))),
      'is_shared_pages'      => new sfValidatorChoice(array('required' => false, 'choices' => array('', 1, 0))),
      'is_shared_members'    => new sfValidatorChoice(array('required' => false, 'choices' => array('', 1, 0))),
      'is_shared_lists'      => new sfValidatorChoice(array('required' => false, 'choices' => array('', 1, 0))),
      'is_shared_products'   => new sfValidatorChoice(array('required' => false, 'choices' => array('', 1, 0))),
      'is_shared_movieclips' => new sfValidatorChoice(array('required' => false, 'choices' => array('', 1, 0))),
      'is_shared_structure'  => new sfValidatorChoice(array('required' => false, 'choices' => array('', 1, 0))),
    ));

    $this->widgetSchema->setNameFormat('sms_domain_link_group_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'SmsDomainLinkGroup';
  }

  public function getFields()
  {
    return array(
      'id'                   => 'Number',
      'is_shared_obits'      => 'Boolean',
      'is_shared_pages'      => 'Boolean',
      'is_shared_members'    => 'Boolean',
      'is_shared_lists'      => 'Boolean',
      'is_shared_products'   => 'Boolean',
      'is_shared_movieclips' => 'Boolean',
      'is_shared_structure'  => 'Boolean',
    );
  }
}
