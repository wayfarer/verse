<?php

/**
 * PlgObituary filter form base class.
 *
 * @package    verse3
 * @subpackage filter
 * @author     Vladimir Droznik <vladimir.droznik@gmail.com>
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BasePlgObituaryFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'obituary_case_id'               => new sfWidgetFormFilterInput(),
      'first_name'                     => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'middle_name'                    => new sfWidgetFormFilterInput(),
      'last_name'                      => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'home_place'                     => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'death_date'                     => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
      'birth_place'                    => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'birth_date'                     => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
      'service_date'                   => new sfWidgetFormFilterInput(),
      'service_time'                   => new sfWidgetFormFilterInput(),
      'service_place'                  => new sfWidgetFormFilterInput(),
      'visitation_date'                => new sfWidgetFormFilterInput(),
      'visitation_place'               => new sfWidgetFormFilterInput(),
      'final_disposition'              => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'image'                          => new sfWidgetFormFilterInput(),
      'obit_text'                      => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'candles_policy'                 => new sfWidgetFormFilterInput(),
      'service_type'                   => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'title'                          => new sfWidgetFormFilterInput(),
      'suffix'                         => new sfWidgetFormFilterInput(),
      'finaldisposition_google_method' => new sfWidgetFormFilterInput(),
      'finaldisposition_google_addr'   => new sfWidgetFormFilterInput(),
      'service_google_method'          => new sfWidgetFormFilterInput(),
      'service_google_addr'            => new sfWidgetFormFilterInput(),
      'created_at'                     => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
      'updated_at'                     => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
      'movieclips_list'                => new sfWidgetFormDoctrineChoice(array('multiple' => true, 'model' => 'PlgMovieclip')),
    ));

    $this->setValidators(array(
      'obituary_case_id'               => new sfValidatorPass(array('required' => false)),
      'first_name'                     => new sfValidatorPass(array('required' => false)),
      'middle_name'                    => new sfValidatorPass(array('required' => false)),
      'last_name'                      => new sfValidatorPass(array('required' => false)),
      'home_place'                     => new sfValidatorPass(array('required' => false)),
      'death_date'                     => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDateTime(array('required' => false)))),
      'birth_place'                    => new sfValidatorPass(array('required' => false)),
      'birth_date'                     => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDateTime(array('required' => false)))),
      'service_date'                   => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'service_time'                   => new sfValidatorPass(array('required' => false)),
      'service_place'                  => new sfValidatorPass(array('required' => false)),
      'visitation_date'                => new sfValidatorPass(array('required' => false)),
      'visitation_place'               => new sfValidatorPass(array('required' => false)),
      'final_disposition'              => new sfValidatorPass(array('required' => false)),
      'image'                          => new sfValidatorPass(array('required' => false)),
      'obit_text'                      => new sfValidatorPass(array('required' => false)),
      'candles_policy'                 => new sfValidatorPass(array('required' => false)),
      'service_type'                   => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'title'                          => new sfValidatorPass(array('required' => false)),
      'suffix'                         => new sfValidatorPass(array('required' => false)),
      'finaldisposition_google_method' => new sfValidatorPass(array('required' => false)),
      'finaldisposition_google_addr'   => new sfValidatorPass(array('required' => false)),
      'service_google_method'          => new sfValidatorPass(array('required' => false)),
      'service_google_addr'            => new sfValidatorPass(array('required' => false)),
      'created_at'                     => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
      'updated_at'                     => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
      'movieclips_list'                => new sfValidatorDoctrineChoice(array('multiple' => true, 'model' => 'PlgMovieclip', 'required' => false)),
    ));

    $this->widgetSchema->setNameFormat('plg_obituary_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function addMovieclipsListColumnQuery(Doctrine_Query $query, $field, $values)
  {
    if (!is_array($values))
    {
      $values = array($values);
    }

    if (!count($values))
    {
      return;
    }

    $query->leftJoin('r.PlgObituaryMovieclip PlgObituaryMovieclip')
          ->andWhereIn('PlgObituaryMovieclip.obituary_id', $values);
  }

  public function getModelName()
  {
    return 'PlgObituary';
  }

  public function getFields()
  {
    return array(
      'obituary_id'                    => 'Number',
      'domain_id'                      => 'Number',
      'obituary_case_id'               => 'Text',
      'first_name'                     => 'Text',
      'middle_name'                    => 'Text',
      'last_name'                      => 'Text',
      'home_place'                     => 'Text',
      'death_date'                     => 'Date',
      'birth_place'                    => 'Text',
      'birth_date'                     => 'Date',
      'service_date'                   => 'Number',
      'service_time'                   => 'Text',
      'service_place'                  => 'Text',
      'visitation_date'                => 'Text',
      'visitation_place'               => 'Text',
      'final_disposition'              => 'Text',
      'image'                          => 'Text',
      'obit_text'                      => 'Text',
      'candles_policy'                 => 'Text',
      'service_type'                   => 'Number',
      'title'                          => 'Text',
      'suffix'                         => 'Text',
      'finaldisposition_google_method' => 'Text',
      'finaldisposition_google_addr'   => 'Text',
      'service_google_method'          => 'Text',
      'service_google_addr'            => 'Text',
      'created_at'                     => 'Date',
      'updated_at'                     => 'Date',
      'movieclips_list'                => 'ManyKey',
    );
  }
}
