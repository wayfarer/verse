<?php

/**
 * PlgFuneralDirector filter form base class.
 *
 * @package    verse3
 * @subpackage filter
 * @author     Vladimir Droznik <vladimir.droznik@gmail.com>
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BasePlgFuneralDirectorFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'status'      => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'title'       => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'first_name'  => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'last_name'   => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'firm'        => new sfWidgetFormFilterInput(),
      'address'     => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'city'        => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'state'       => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'zip'         => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'phone'       => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'fax'         => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'county'      => new sfWidgetFormFilterInput(array('with_empty' => false)),
    ));

    $this->setValidators(array(
      'status'      => new sfValidatorPass(array('required' => false)),
      'title'       => new sfValidatorPass(array('required' => false)),
      'first_name'  => new sfValidatorPass(array('required' => false)),
      'last_name'   => new sfValidatorPass(array('required' => false)),
      'firm'        => new sfValidatorPass(array('required' => false)),
      'address'     => new sfValidatorPass(array('required' => false)),
      'city'        => new sfValidatorPass(array('required' => false)),
      'state'       => new sfValidatorPass(array('required' => false)),
      'zip'         => new sfValidatorPass(array('required' => false)),
      'phone'       => new sfValidatorPass(array('required' => false)),
      'fax'         => new sfValidatorPass(array('required' => false)),
      'county'      => new sfValidatorPass(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('plg_funeral_director_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'PlgFuneralDirector';
  }

  public function getFields()
  {
    return array(
      'director_id' => 'Number',
      'status'      => 'Text',
      'title'       => 'Text',
      'first_name'  => 'Text',
      'last_name'   => 'Text',
      'firm'        => 'Text',
      'address'     => 'Text',
      'city'        => 'Text',
      'state'       => 'Text',
      'zip'         => 'Text',
      'phone'       => 'Text',
      'fax'         => 'Text',
      'county'      => 'Text',
    );
  }
}
