<?php

/**
 * CmsWallMessage filter form base class.
 *
 * @package    verse3
 * @subpackage filter
 * @author     Vladimir Droznik <vladimir.droznik@gmail.com>
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseCmsWallMessageFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'domain_id'       => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'thread_id'       => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'user_id'         => new sfWidgetFormFilterInput(),
      'name'            => new sfWidgetFormFilterInput(),
      'email'           => new sfWidgetFormFilterInput(),
      'message'         => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'created_at'      => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
      'reply_for_id'    => new sfWidgetFormFilterInput(),
      'level'           => new sfWidgetFormFilterInput(),
      'is_published'    => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'lft'             => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'rgt'             => new sfWidgetFormFilterInput(array('with_empty' => false)),
    ));

    $this->setValidators(array(
      'domain_id'       => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'thread_id'       => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'user_id'         => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'name'            => new sfValidatorPass(array('required' => false)),
      'email'           => new sfValidatorPass(array('required' => false)),
      'message'         => new sfValidatorPass(array('required' => false)),
      'created_at'      => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
      'reply_for_id'    => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'level'           => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'is_published'    => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'lft'             => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'rgt'             => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
    ));

    $this->widgetSchema->setNameFormat('cms_wall_message_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'CmsWallMessage';
  }

  public function getFields()
  {
    return array(
      'wall_message_id' => 'Number',
      'domain_id'       => 'Number',
      'thread_id'       => 'Number',
      'user_id'         => 'Number',
      'name'            => 'Text',
      'email'           => 'Text',
      'message'         => 'Text',
      'created_at'      => 'Date',
      'reply_for_id'    => 'Number',
      'level'           => 'Number',
      'is_published'    => 'Number',
      'lft'             => 'Number',
      'rgt'             => 'Number',
    );
  }
}
