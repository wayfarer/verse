<?php

/**
 * PlgObituaryMovie filter form base class.
 *
 * @package    verse3
 * @subpackage filter
 * @author     Vladimir Droznik <vladimir.droznik@gmail.com>
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BasePlgObituaryMovieFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'obituary_id'  => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Obituary'), 'add_empty' => true)),
      'domain_id'    => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Domain'), 'add_empty' => true)),
      'local'        => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'path'         => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'origfilename' => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'thumbnail'    => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'state'        => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'hash'         => new sfWidgetFormFilterInput(),
      'status'       => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'created_at'   => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
      'updated_at'   => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
    ));

    $this->setValidators(array(
      'obituary_id'  => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('Obituary'), 'column' => 'obituary_id')),
      'domain_id'    => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('Domain'), 'column' => 'domain_id')),
      'local'        => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'path'         => new sfValidatorPass(array('required' => false)),
      'origfilename' => new sfValidatorPass(array('required' => false)),
      'thumbnail'    => new sfValidatorPass(array('required' => false)),
      'state'        => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'hash'         => new sfValidatorPass(array('required' => false)),
      'status'       => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'created_at'   => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
      'updated_at'   => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
    ));

    $this->widgetSchema->setNameFormat('plg_obituary_movie_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'PlgObituaryMovie';
  }

  public function getFields()
  {
    return array(
      'id'           => 'Number',
      'obituary_id'  => 'ForeignKey',
      'domain_id'    => 'ForeignKey',
      'local'        => 'Number',
      'path'         => 'Text',
      'origfilename' => 'Text',
      'thumbnail'    => 'Text',
      'state'        => 'Number',
      'hash'         => 'Text',
      'status'       => 'Number',
      'created_at'   => 'Date',
      'updated_at'   => 'Date',
    );
  }
}
