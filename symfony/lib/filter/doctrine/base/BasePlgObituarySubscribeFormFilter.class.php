<?php

/**
 * PlgObituarySubscribe filter form base class.
 *
 * @package    verse3
 * @subpackage filter
 * @author     Vladimir Droznik <vladimir.droznik@gmail.com>
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BasePlgObituarySubscribeFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'obituary_id'           => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Obituary'), 'add_empty' => true)),
      'name'                  => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'email'                 => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'created'               => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
      'hash'                  => new sfWidgetFormFilterInput(),
      'enabled'               => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'isadmin'               => new sfWidgetFormFilterInput(array('with_empty' => false)),
    ));

    $this->setValidators(array(
      'obituary_id'           => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('Obituary'), 'column' => 'obituary_id')),
      'name'                  => new sfValidatorPass(array('required' => false)),
      'email'                 => new sfValidatorPass(array('required' => false)),
      'created'               => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
      'hash'                  => new sfValidatorPass(array('required' => false)),
      'enabled'               => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'isadmin'               => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
    ));

    $this->widgetSchema->setNameFormat('plg_obituary_subscribe_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'PlgObituarySubscribe';
  }

  public function getFields()
  {
    return array(
      'obituary_subscribe_id' => 'Number',
      'obituary_id'           => 'ForeignKey',
      'domain_id'             => 'Number',
      'name'                  => 'Text',
      'email'                 => 'Text',
      'created'               => 'Date',
      'hash'                  => 'Text',
      'enabled'               => 'Number',
      'isadmin'               => 'Number',
    );
  }
}
