<?php

/**
 * CmsWallThread filter form base class.
 *
 * @package    verse3
 * @subpackage filter
 * @author     Vladimir Droznik <vladimir.droznik@gmail.com>
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseCmsWallThreadFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'domain_id'  => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'wall_id'    => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'title'      => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'created_at' => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
      'user_id'    => new sfWidgetFormFilterInput(),
    ));

    $this->setValidators(array(
      'domain_id'  => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'wall_id'    => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'title'      => new sfValidatorPass(array('required' => false)),
      'created_at' => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
      'user_id'    => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
    ));

    $this->widgetSchema->setNameFormat('cms_wall_thread_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'CmsWallThread';
  }

  public function getFields()
  {
    return array(
      'thread_id'  => 'Number',
      'domain_id'  => 'Number',
      'wall_id'    => 'Number',
      'title'      => 'Text',
      'created_at' => 'Date',
      'user_id'    => 'Number',
    );
  }
}
