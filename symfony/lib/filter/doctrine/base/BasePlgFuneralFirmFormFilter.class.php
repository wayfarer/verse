<?php

/**
 * PlgFuneralFirm filter form base class.
 *
 * @package    verse3
 * @subpackage filter
 * @author     Vladimir Droznik <vladimir.droznik@gmail.com>
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BasePlgFuneralFirmFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'name'    => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'address' => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'city'    => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'state'   => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'zip'     => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'phone'   => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'fax'     => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'url'     => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'county'  => new sfWidgetFormFilterInput(array('with_empty' => false)),
    ));

    $this->setValidators(array(
      'name'    => new sfValidatorPass(array('required' => false)),
      'address' => new sfValidatorPass(array('required' => false)),
      'city'    => new sfValidatorPass(array('required' => false)),
      'state'   => new sfValidatorPass(array('required' => false)),
      'zip'     => new sfValidatorPass(array('required' => false)),
      'phone'   => new sfValidatorPass(array('required' => false)),
      'fax'     => new sfValidatorPass(array('required' => false)),
      'url'     => new sfValidatorPass(array('required' => false)),
      'county'  => new sfValidatorPass(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('plg_funeral_firm_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'PlgFuneralFirm';
  }

  public function getFields()
  {
    return array(
      'firm_id' => 'Number',
      'name'    => 'Text',
      'address' => 'Text',
      'city'    => 'Text',
      'state'   => 'Text',
      'zip'     => 'Text',
      'phone'   => 'Text',
      'fax'     => 'Text',
      'url'     => 'Text',
      'county'  => 'Text',
    );
  }
}
