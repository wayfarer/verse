<?php

/**
 * PlgMovieclip filter form base class.
 *
 * @package    verse3
 * @subpackage filter
 * @author     Vladimir Droznik <vladimir.droznik@gmail.com>
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BasePlgMovieclipFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'domain_id'         => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'title'             => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'description'       => new sfWidgetFormFilterInput(),
      'filename'          => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'timestamp'         => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'protected'         => new sfWidgetFormChoice(array('choices' => array('' => '', 1 => 'yes', 0 => 'no'))),
      'password'          => new sfWidgetFormFilterInput(),
      'status'            => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'tmpfilename'       => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'origfilename'      => new sfWidgetFormFilterInput(),
      'obituaries_list'   => new sfWidgetFormDoctrineChoice(array('multiple' => true, 'model' => 'PlgMovieclip')),
      'plg_obituary_list' => new sfWidgetFormDoctrineChoice(array('multiple' => true, 'model' => 'PlgObituary')),
    ));

    $this->setValidators(array(
      'domain_id'         => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'title'             => new sfValidatorPass(array('required' => false)),
      'description'       => new sfValidatorPass(array('required' => false)),
      'filename'          => new sfValidatorPass(array('required' => false)),
      'timestamp'         => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'protected'         => new sfValidatorChoice(array('required' => false, 'choices' => array('', 1, 0))),
      'password'          => new sfValidatorPass(array('required' => false)),
      'status'            => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'tmpfilename'       => new sfValidatorPass(array('required' => false)),
      'origfilename'      => new sfValidatorPass(array('required' => false)),
      'obituaries_list'   => new sfValidatorDoctrineChoice(array('multiple' => true, 'model' => 'PlgMovieclip', 'required' => false)),
      'plg_obituary_list' => new sfValidatorDoctrineChoice(array('multiple' => true, 'model' => 'PlgObituary', 'required' => false)),
    ));

    $this->widgetSchema->setNameFormat('plg_movieclip_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function addObituariesListColumnQuery(Doctrine_Query $query, $field, $values)
  {
    if (!is_array($values))
    {
      $values = array($values);
    }

    if (!count($values))
    {
      return;
    }

    $query->leftJoin('r.PlgObituaryMovieclip PlgObituaryMovieclip')
          ->andWhereIn('PlgObituaryMovieclip.movieclip_id', $values);
  }

  public function addPlgObituaryListColumnQuery(Doctrine_Query $query, $field, $values)
  {
    if (!is_array($values))
    {
      $values = array($values);
    }

    if (!count($values))
    {
      return;
    }

    $query->leftJoin('r.PlgObituaryMovieclip PlgObituaryMovieclip')
          ->andWhereIn('PlgObituaryMovieclip.obituary_id', $values);
  }

  public function getModelName()
  {
    return 'PlgMovieclip';
  }

  public function getFields()
  {
    return array(
      'movieclip_id'      => 'Number',
      'domain_id'         => 'Number',
      'title'             => 'Text',
      'description'       => 'Text',
      'filename'          => 'Text',
      'timestamp'         => 'Number',
      'protected'         => 'Boolean',
      'password'          => 'Text',
      'status'            => 'Number',
      'tmpfilename'       => 'Text',
      'origfilename'      => 'Text',
      'obituaries_list'   => 'ManyKey',
      'plg_obituary_list' => 'ManyKey',
    );
  }
}
