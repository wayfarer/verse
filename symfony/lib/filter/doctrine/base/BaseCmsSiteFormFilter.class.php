<?php

/**
 * CmsSite filter form base class.
 *
 * @package    verse3
 * @subpackage filter
 * @author     Vladimir Droznik <vladimir.droznik@gmail.com>
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseCmsSiteFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'title'        => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'properties'   => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'css'          => new sfWidgetFormFilterInput(),
      'header_mode'  => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'header'       => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'main_menu'    => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'popup_menu'   => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'footer'       => new sfWidgetFormFilterInput(),
      'google_stats' => new sfWidgetFormFilterInput(array('with_empty' => false)),
    ));

    $this->setValidators(array(
      'title'        => new sfValidatorPass(array('required' => false)),
      'properties'   => new sfValidatorPass(array('required' => false)),
      'css'          => new sfValidatorPass(array('required' => false)),
      'header_mode'  => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'header'       => new sfValidatorPass(array('required' => false)),
      'main_menu'    => new sfValidatorPass(array('required' => false)),
      'popup_menu'   => new sfValidatorPass(array('required' => false)),
      'footer'       => new sfValidatorPass(array('required' => false)),
      'google_stats' => new sfValidatorPass(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('cms_site_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'CmsSite';
  }

  public function getFields()
  {
    return array(
      'domain_id'    => 'Number',
      'title'        => 'Text',
      'properties'   => 'Text',
      'css'          => 'Text',
      'header_mode'  => 'Number',
      'header'       => 'Text',
      'main_menu'    => 'Text',
      'popup_menu'   => 'Text',
      'footer'       => 'Text',
      'google_stats' => 'Text',
    );
  }
}
