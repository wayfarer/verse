<?php

/**
 * CmsAffiliate filter form base class.
 *
 * @package    verse3
 * @subpackage filter
 * @author     Vladimir Droznik <vladimir.droznik@gmail.com>
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseCmsAffiliateFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'domain_id'    => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'ref'          => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'title'        => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'page_id'      => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('AffiliatePage'), 'add_empty' => true)),
    ));

    $this->setValidators(array(
      'domain_id'    => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'ref'          => new sfValidatorPass(array('required' => false)),
      'title'        => new sfValidatorPass(array('required' => false)),
      'page_id'      => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('AffiliatePage'), 'column' => 'page_id')),
    ));

    $this->widgetSchema->setNameFormat('cms_affiliate_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'CmsAffiliate';
  }

  public function getFields()
  {
    return array(
      'affiliate_id' => 'Number',
      'domain_id'    => 'Number',
      'ref'          => 'Text',
      'title'        => 'Text',
      'page_id'      => 'ForeignKey',
    );
  }
}
