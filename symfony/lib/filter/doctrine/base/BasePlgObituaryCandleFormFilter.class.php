<?php

/**
 * PlgObituaryCandle filter form base class.
 *
 * @package    verse3
 * @subpackage filter
 * @author     Vladimir Droznik <vladimir.droznik@gmail.com>
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BasePlgObituaryCandleFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'candle_case_id' => new sfWidgetFormFilterInput(),
      'obituary_id'    => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Obituary'), 'add_empty' => true)),
      'name'           => new sfWidgetFormFilterInput(),
      'email'          => new sfWidgetFormFilterInput(),
      'ip'             => new sfWidgetFormFilterInput(),
      'thoughts'       => new sfWidgetFormFilterInput(),
      'timestamp'      => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
      'state'          => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'hash'           => new sfWidgetFormFilterInput(),
      'created_at'     => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
      'updated_at'     => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
    ));

    $this->setValidators(array(
      'candle_case_id' => new sfValidatorPass(array('required' => false)),
      'obituary_id'    => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('Obituary'), 'column' => 'obituary_id')),
      'name'           => new sfValidatorPass(array('required' => false)),
      'email'          => new sfValidatorPass(array('required' => false)),
      'ip'             => new sfValidatorPass(array('required' => false)),
      'thoughts'       => new sfValidatorPass(array('required' => false)),
      'timestamp'      => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
      'state'          => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'hash'           => new sfValidatorPass(array('required' => false)),
      'created_at'     => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
      'updated_at'     => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
    ));

    $this->widgetSchema->setNameFormat('plg_obituary_candle_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'PlgObituaryCandle';
  }

  public function getFields()
  {
    return array(
      'candle_id'      => 'Number',
      'candle_case_id' => 'Text',
      'domain_id'      => 'Number',
      'obituary_id'    => 'ForeignKey',
      'name'           => 'Text',
      'email'          => 'Text',
      'ip'             => 'Text',
      'thoughts'       => 'Text',
      'timestamp'      => 'Date',
      'state'          => 'Number',
      'hash'           => 'Text',
      'created_at'     => 'Date',
      'updated_at'     => 'Date',
    );
  }
}
