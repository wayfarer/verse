<?php

/**
 * CmsPage filter form base class.
 *
 * @package    verse3
 * @subpackage filter
 * @author     Vladimir Droznik <vladimir.droznik@gmail.com>
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseCmsPageFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'domain_id'     => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'ord'           => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'page_type'     => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'internal_name' => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'no_header'     => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'content'       => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'restricted'    => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'properties'    => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'custom_header' => new sfWidgetFormFilterInput(),
    ));

    $this->setValidators(array(
      'domain_id'     => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'ord'           => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'page_type'     => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'internal_name' => new sfValidatorPass(array('required' => false)),
      'no_header'     => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'content'       => new sfValidatorPass(array('required' => false)),
      'restricted'    => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'properties'    => new sfValidatorPass(array('required' => false)),
      'custom_header' => new sfValidatorPass(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('cms_page_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'CmsPage';
  }

  public function getFields()
  {
    return array(
      'page_id'       => 'Number',
      'domain_id'     => 'Number',
      'ord'           => 'Number',
      'page_type'     => 'Number',
      'internal_name' => 'Text',
      'no_header'     => 'Number',
      'content'       => 'Text',
      'restricted'    => 'Number',
      'properties'    => 'Text',
      'custom_header' => 'Text',
    );
  }
}
