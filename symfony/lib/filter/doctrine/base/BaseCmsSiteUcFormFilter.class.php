<?php

/**
 * CmsSiteUc filter form base class.
 *
 * @package    verse3
 * @subpackage filter
 * @author     Vladimir Droznik <vladimir.droznik@gmail.com>
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseCmsSiteUcFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'title'       => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'properties'  => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'css'         => new sfWidgetFormFilterInput(),
      'header_mode' => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'header'      => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'footer'      => new sfWidgetFormFilterInput(),
    ));

    $this->setValidators(array(
      'title'       => new sfValidatorPass(array('required' => false)),
      'properties'  => new sfValidatorPass(array('required' => false)),
      'css'         => new sfValidatorPass(array('required' => false)),
      'header_mode' => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'header'      => new sfValidatorPass(array('required' => false)),
      'footer'      => new sfValidatorPass(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('cms_site_uc_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'CmsSiteUc';
  }

  public function getFields()
  {
    return array(
      'domain_id'   => 'Number',
      'title'       => 'Text',
      'properties'  => 'Text',
      'css'         => 'Text',
      'header_mode' => 'Number',
      'header'      => 'Text',
      'footer'      => 'Text',
    );
  }
}
