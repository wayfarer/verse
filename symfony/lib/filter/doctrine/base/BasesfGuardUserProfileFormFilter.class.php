<?php

/**
 * sfGuardUserProfile filter form base class.
 *
 * @package    verse3
 * @subpackage filter
 * @author     Vladimir Droznik <vladimir.droznik@gmail.com>
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BasesfGuardUserProfileFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'name'             => new sfWidgetFormFilterInput(),
      'domain_id'        => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'ftp_user_id'      => new sfWidgetFormFilterInput(),
      'piwik_token_auth' => new sfWidgetFormFilterInput(),
    ));

    $this->setValidators(array(
      'name'             => new sfValidatorPass(array('required' => false)),
      'domain_id'        => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'ftp_user_id'      => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'piwik_token_auth' => new sfValidatorPass(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('sf_guard_user_profile_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'sfGuardUserProfile';
  }

  public function getFields()
  {
    return array(
      'user_id'          => 'Number',
      'name'             => 'Text',
      'domain_id'        => 'Number',
      'ftp_user_id'      => 'Number',
      'piwik_token_auth' => 'Text',
    );
  }
}
