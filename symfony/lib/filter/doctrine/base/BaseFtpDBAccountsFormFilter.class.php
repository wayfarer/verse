<?php

/**
 * FtpDBAccounts filter form base class.
 *
 * @package    verse3
 * @subpackage filter
 * @author     Vladimir Droznik <vladimir.droznik@gmail.com>
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseFtpDBAccountsFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'username' => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'passwd'   => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'uid'      => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'gid'      => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'homedir'  => new sfWidgetFormFilterInput(),
      'shell'    => new sfWidgetFormFilterInput(),
    ));

    $this->setValidators(array(
      'username' => new sfValidatorPass(array('required' => false)),
      'passwd'   => new sfValidatorPass(array('required' => false)),
      'uid'      => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'gid'      => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'homedir'  => new sfValidatorPass(array('required' => false)),
      'shell'    => new sfValidatorPass(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('ftp_db_accounts_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'FtpDBAccounts';
  }

  public function getFields()
  {
    return array(
      'user_id'  => 'Number',
      'username' => 'Text',
      'passwd'   => 'Text',
      'uid'      => 'Number',
      'gid'      => 'Number',
      'homedir'  => 'Text',
      'shell'    => 'Text',
    );
  }
}
