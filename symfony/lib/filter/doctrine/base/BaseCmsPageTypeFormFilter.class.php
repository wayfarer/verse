<?php

/**
 * CmsPageType filter form base class.
 *
 * @package    verse3
 * @subpackage filter
 * @author     Vladimir Droznik <vladimir.droznik@gmail.com>
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseCmsPageTypeFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'page_type_name'   => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'page_type_handle' => new sfWidgetFormFilterInput(array('with_empty' => false)),
    ));

    $this->setValidators(array(
      'page_type_name'   => new sfValidatorPass(array('required' => false)),
      'page_type_handle' => new sfValidatorPass(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('cms_page_type_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'CmsPageType';
  }

  public function getFields()
  {
    return array(
      'page_type_id'     => 'Number',
      'page_type_name'   => 'Text',
      'page_type_handle' => 'Text',
    );
  }
}
