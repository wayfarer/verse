<?php

/**
 * PlgEvent filter form base class.
 *
 * @package    verse3
 * @subpackage filter
 * @author     Vladimir Droznik <vladimir.droznik@gmail.com>
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BasePlgEventFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'event_case_id' => new sfWidgetFormFilterInput(),
      'domain_id'     => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'obituary_id'   => new sfWidgetFormFilterInput(),
      'event_type'    => new sfWidgetFormFilterInput(),
      'timestamp'     => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'description'   => new sfWidgetFormFilterInput(array('with_empty' => false)),
    ));

    $this->setValidators(array(
      'event_case_id' => new sfValidatorPass(array('required' => false)),
      'domain_id'     => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'obituary_id'   => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'event_type'    => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'timestamp'     => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'description'   => new sfValidatorPass(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('plg_event_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'PlgEvent';
  }

  public function getFields()
  {
    return array(
      'event_id'      => 'Number',
      'event_case_id' => 'Text',
      'domain_id'     => 'Number',
      'obituary_id'   => 'Number',
      'event_type'    => 'Number',
      'timestamp'     => 'Number',
      'description'   => 'Text',
    );
  }
}
