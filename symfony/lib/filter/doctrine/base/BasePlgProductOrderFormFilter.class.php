<?php

/**
 * PlgProductOrder filter form base class.
 *
 * @package    verse3
 * @subpackage filter
 * @author     Vladimir Droznik <vladimir.droznik@gmail.com>
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BasePlgProductOrderFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'domain_id'      => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'timestamp'      => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
      'total'          => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'payer'          => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'affiliate_id'   => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('OrderAffiliate'), 'add_empty' => true)),
      'affiliate_from' => new sfWidgetFormFilterInput(),
      'user_data'      => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'status'         => new sfWidgetFormFilterInput(array('with_empty' => false)),
    ));

    $this->setValidators(array(
      'domain_id'      => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'timestamp'      => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
      'total'          => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
      'payer'          => new sfValidatorPass(array('required' => false)),
      'affiliate_id'   => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('OrderAffiliate'), 'column' => 'affiliate_id')),
      'affiliate_from' => new sfValidatorPass(array('required' => false)),
      'user_data'      => new sfValidatorPass(array('required' => false)),
      'status'         => new sfValidatorPass(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('plg_product_order_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'PlgProductOrder';
  }

  public function getFields()
  {
    return array(
      'order_id'       => 'Number',
      'domain_id'      => 'Number',
      'timestamp'      => 'Date',
      'total'          => 'Number',
      'payer'          => 'Text',
      'affiliate_id'   => 'ForeignKey',
      'affiliate_from' => 'Text',
      'user_data'      => 'Text',
      'status'         => 'Text',
    );
  }
}
