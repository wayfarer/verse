<?php

/**
 * PlgProductCategory filter form base class.
 *
 * @package    verse3
 * @subpackage filter
 * @author     Vladimir Droznik <vladimir.droznik@gmail.com>
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BasePlgProductCategoryFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'domain_id'         => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'name'              => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'ecommerce_enabled' => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'price_enabled'     => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'additional_fields' => new sfWidgetFormChoice(array('choices' => array('' => '', 1 => 'yes', 0 => 'no'))),
      'field1enabled'     => new sfWidgetFormChoice(array('choices' => array('' => '', 1 => 'yes', 0 => 'no'))),
      'field1title'       => new sfWidgetFormFilterInput(),
      'field1type'        => new sfWidgetFormFilterInput(),
      'field2enabled'     => new sfWidgetFormChoice(array('choices' => array('' => '', 1 => 'yes', 0 => 'no'))),
      'field2title'       => new sfWidgetFormFilterInput(),
      'field2type'        => new sfWidgetFormFilterInput(),
    ));

    $this->setValidators(array(
      'domain_id'         => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'name'              => new sfValidatorPass(array('required' => false)),
      'ecommerce_enabled' => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'price_enabled'     => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'additional_fields' => new sfValidatorChoice(array('required' => false, 'choices' => array('', 1, 0))),
      'field1enabled'     => new sfValidatorChoice(array('required' => false, 'choices' => array('', 1, 0))),
      'field1title'       => new sfValidatorPass(array('required' => false)),
      'field1type'        => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'field2enabled'     => new sfValidatorChoice(array('required' => false, 'choices' => array('', 1, 0))),
      'field2title'       => new sfValidatorPass(array('required' => false)),
      'field2type'        => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
    ));

    $this->widgetSchema->setNameFormat('plg_product_category_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'PlgProductCategory';
  }

  public function getFields()
  {
    return array(
      'category_id'       => 'Number',
      'domain_id'         => 'Number',
      'name'              => 'Text',
      'ecommerce_enabled' => 'Number',
      'price_enabled'     => 'Number',
      'additional_fields' => 'Boolean',
      'field1enabled'     => 'Boolean',
      'field1title'       => 'Text',
      'field1type'        => 'Number',
      'field2enabled'     => 'Boolean',
      'field2title'       => 'Text',
      'field2type'        => 'Number',
    );
  }
}
