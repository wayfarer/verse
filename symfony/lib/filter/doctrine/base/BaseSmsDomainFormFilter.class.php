<?php

/**
 * SmsDomain filter form base class.
 *
 * @package    verse3
 * @subpackage filter
 * @author     Vladimir Droznik <vladimir.droznik@gmail.com>
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseSmsDomainFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'domain_name'        => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'server_id'          => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'enabled'            => new sfWidgetFormChoice(array('choices' => array('' => '', 1 => 'yes', 0 => 'no'))),
      'email'              => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'postfix'            => new sfWidgetFormFilterInput(),
      'email_enabled'      => new sfWidgetFormChoice(array('choices' => array('' => '', 1 => 'yes', 0 => 'no'))),
      'ecommerce_enabled'  => new sfWidgetFormChoice(array('choices' => array('' => '', 1 => 'yes', 0 => 'no'))),
      'price_enabled'      => new sfWidgetFormChoice(array('choices' => array('' => '', 1 => 'yes', 0 => 'no'))),
      'paypal_checkout'    => new sfWidgetFormChoice(array('choices' => array('' => '', 1 => 'yes', 0 => 'no'))),
      'affiliates_enabled' => new sfWidgetFormChoice(array('choices' => array('' => '', 1 => 'yes', 0 => 'no'))),
      'mode'               => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'alias_domain_id'    => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('DomainAlias'), 'add_empty' => true)),
      'space_consumed'     => new sfWidgetFormFilterInput(),
      'created_at'         => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
      'updated_at'         => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
    ));

    $this->setValidators(array(
      'domain_name'        => new sfValidatorPass(array('required' => false)),
      'server_id'          => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'enabled'            => new sfValidatorChoice(array('required' => false, 'choices' => array('', 1, 0))),
      'email'              => new sfValidatorPass(array('required' => false)),
      'postfix'            => new sfValidatorPass(array('required' => false)),
      'email_enabled'      => new sfValidatorChoice(array('required' => false, 'choices' => array('', 1, 0))),
      'ecommerce_enabled'  => new sfValidatorChoice(array('required' => false, 'choices' => array('', 1, 0))),
      'price_enabled'      => new sfValidatorChoice(array('required' => false, 'choices' => array('', 1, 0))),
      'paypal_checkout'    => new sfValidatorChoice(array('required' => false, 'choices' => array('', 1, 0))),
      'affiliates_enabled' => new sfValidatorChoice(array('required' => false, 'choices' => array('', 1, 0))),
      'mode'               => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'alias_domain_id'    => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('DomainAlias'), 'column' => 'domain_id')),
      'space_consumed'     => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'created_at'         => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
      'updated_at'         => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
    ));

    $this->widgetSchema->setNameFormat('sms_domain_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'SmsDomain';
  }

  public function getFields()
  {
    return array(
      'domain_id'          => 'Number',
      'domain_name'        => 'Text',
      'server_id'          => 'Number',
      'enabled'            => 'Boolean',
      'email'              => 'Text',
      'postfix'            => 'Text',
      'email_enabled'      => 'Boolean',
      'ecommerce_enabled'  => 'Boolean',
      'price_enabled'      => 'Boolean',
      'paypal_checkout'    => 'Boolean',
      'affiliates_enabled' => 'Boolean',
      'mode'               => 'Number',
      'alias_domain_id'    => 'ForeignKey',
      'space_consumed'     => 'Number',
      'created_at'         => 'Date',
      'updated_at'         => 'Date',
    );
  }
}
