<?php

/**
 * PlgBlockKeyword filter form base class.
 *
 * @package    verse3
 * @subpackage filter
 * @author     Vladimir Droznik <vladimir.droznik@gmail.com>
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BasePlgBlockKeywordFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'keyword'   => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'domain_id' => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Domain'), 'add_empty' => true)),
    ));

    $this->setValidators(array(
      'keyword'   => new sfValidatorPass(array('required' => false)),
      'domain_id' => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('Domain'), 'column' => 'domain_id')),
    ));

    $this->widgetSchema->setNameFormat('plg_block_keyword_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'PlgBlockKeyword';
  }

  public function getFields()
  {
    return array(
      'id'        => 'Number',
      'keyword'   => 'Text',
      'domain_id' => 'ForeignKey',
    );
  }
}
