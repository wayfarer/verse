<?php

/**
 * CmsPageUc filter form base class.
 *
 * @package    verse3
 * @subpackage filter
 * @author     Vladimir Droznik <vladimir.droznik@gmail.com>
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseCmsPageUcFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'domain_id'  => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'page_type'  => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'name'       => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'no_header'  => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'content'    => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'properties' => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'ord'        => new sfWidgetFormFilterInput(array('with_empty' => false)),
    ));

    $this->setValidators(array(
      'domain_id'  => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'page_type'  => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'name'       => new sfValidatorPass(array('required' => false)),
      'no_header'  => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'content'    => new sfValidatorPass(array('required' => false)),
      'properties' => new sfValidatorPass(array('required' => false)),
      'ord'        => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
    ));

    $this->widgetSchema->setNameFormat('cms_page_uc_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'CmsPageUc';
  }

  public function getFields()
  {
    return array(
      'page_id'    => 'Number',
      'domain_id'  => 'Number',
      'page_type'  => 'Number',
      'name'       => 'Text',
      'no_header'  => 'Number',
      'content'    => 'Text',
      'properties' => 'Text',
      'ord'        => 'Number',
    );
  }
}
