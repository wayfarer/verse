<?php

/**
 * PlgProductMylist filter form base class.
 *
 * @package    verse3
 * @subpackage filter
 * @author     Vladimir Droznik <vladimir.droznik@gmail.com>
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BasePlgProductMylistFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'domain_id' => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'user_id'   => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'hash'      => new sfWidgetFormFilterInput(array('with_empty' => false)),
    ));

    $this->setValidators(array(
      'domain_id' => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'user_id'   => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'hash'      => new sfValidatorPass(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('plg_product_mylist_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'PlgProductMylist';
  }

  public function getFields()
  {
    return array(
      'mylist_id' => 'Number',
      'domain_id' => 'Number',
      'user_id'   => 'Number',
      'hash'      => 'Text',
    );
  }
}
