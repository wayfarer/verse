<?php

/**
 * SmsDomainLinkPage filter form.
 *
 * @package    verse3
 * @subpackage filter
 * @author     Vladimir Droznik <vladimir.droznik@gmail.com>
 * @version    SVN: $Id: sfDoctrineFormFilterTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class SmsDomainLinkPageFormFilter extends BaseSmsDomainLinkPageFormFilter
{
  public function configure()
  {
  }
}
