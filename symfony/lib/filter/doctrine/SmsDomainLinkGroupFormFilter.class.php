<?php

/**
 * SmsDomainLinkGroup filter form.
 *
 * @package    verse3
 * @subpackage filter
 * @author     Vladimir Droznik <vladimir.droznik@gmail.com>
 * @version    SVN: $Id: sfDoctrineFormFilterTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class SmsDomainLinkGroupFormFilter extends BaseSmsDomainLinkGroupFormFilter {
    public function configure() {
        $this->widgetSchema['id'] = new sfWidgetFormDoctrineChoice(array(
            'model' => $this->getModelName(),
            'method' => 'getGroupNameForSelect',
            'add_empty' => true,
            'order_by' => array('group_name', 'asc'))
        );
        $this->validatorSchema['id'] = new sfValidatorDoctrineChoice(array(
            'required' => false,
            'model' => $this->getModelName(),
            'column' => 'id')
        );

        $this->widgetSchema['is_shared_obits'] = new sfWidgetFormChoice(array(
            'choices' => SmsDomainLinkGroupTable::getSharedStates(),
            'expanded' => true,
            'multiple' => true
        ));
        $this->validatorSchema['is_shared_obits'] = new sfValidatorChoice(array(
            'choices' => array_keys(SmsDomainLinkGroupTable::getSharedStates()),
            'multiple' => true,
            'required' => false
        ));

        $this->widgetSchema['is_shared_pages'] = new sfWidgetFormChoice(array(
            'choices' => SmsDomainLinkGroupTable::getSharedStates(),
            'expanded' => true,
            'multiple' => true
        ));
        $this->validatorSchema['is_shared_pages'] = new sfValidatorChoice(array(
            'choices' => array_keys(SmsDomainLinkGroupTable::getSharedStates()),
            'multiple' => true,
            'required' => false
        ));

        $this->widgetSchema['is_shared_members'] = new sfWidgetFormChoice(array(
            'choices' => SmsDomainLinkGroupTable::getSharedStates(),
            'expanded' => true,
            'multiple' => true
        ));
        $this->validatorSchema['is_shared_members'] = new sfValidatorChoice(array(
            'choices' => array_keys(SmsDomainLinkGroupTable::getSharedStates()),
            'multiple' => true,
            'required' => false
        ));

        $this->widgetSchema['is_shared_lists'] = new sfWidgetFormChoice(array(
            'choices' => SmsDomainLinkGroupTable::getSharedStates(),
            'expanded' => true,
            'multiple' => true
        ));
        $this->validatorSchema['is_shared_lists'] = new sfValidatorChoice(array(
            'choices' => array_keys(SmsDomainLinkGroupTable::getSharedStates()),
            'multiple' => true,
            'required' => false
        ));

        $this->widgetSchema['is_shared_products'] = new sfWidgetFormChoice(array(
            'choices' => SmsDomainLinkGroupTable::getSharedStates(),
            'expanded' => true,
            'multiple' => true
        ));
        $this->validatorSchema['is_shared_products'] = new sfValidatorChoice(array(
            'choices' => array_keys(SmsDomainLinkGroupTable::getSharedStates()),
            'multiple' => true,
            'required' => false
        ));

        $this->widgetSchema['is_shared_movieclips'] = new sfWidgetFormChoice(array(
            'choices' => SmsDomainLinkGroupTable::getSharedStates(),
            'expanded' => true,
            'multiple' => true
        ));
        $this->validatorSchema['is_shared_movieclips'] = new sfValidatorChoice(array(
            'choices' => array_keys(SmsDomainLinkGroupTable::getSharedStates()),
            'multiple' => true,
            'required' => false
        ));

        $this->widgetSchema['is_shared_structure'] = new sfWidgetFormChoice(array(
            'choices' => SmsDomainLinkGroupTable::getSharedStates(),
            'expanded' => true,
            'multiple' => true
        ));
        $this->validatorSchema['is_shared_structure'] = new sfValidatorChoice(array(
            'choices' => array_keys(SmsDomainLinkGroupTable::getSharedStates()),
            'multiple' => true,
            'required' => false
        ));
    }

    public function getFields() {
        $fields = parent::getFields();
        $fields['id'] = 'ForeignKey';
        $fields['is_shared_obits'] = 'ForeignKey';
        $fields['is_shared_pages'] = 'ForeignKey';
        $fields['is_shared_members'] = 'ForeignKey';
        $fields['is_shared_lists'] = 'ForeignKey';
        $fields['is_shared_products'] = 'ForeignKey';
        $fields['is_shared_movieclips'] = 'ForeignKey';
        $fields['is_shared_structure'] = 'ForeignKey';
        return $fields;
    }
}
