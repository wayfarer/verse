<?php

/**
 * PlgObituary filter form.
 *
 * @package    verse3
 * @subpackage filter
 * @author     Vladimir Droznik
 * @version    SVN: $Id: sfDoctrineFormFilterTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class PlgObituaryFormFilter extends BasePlgObituaryFormFilter
{
  public function configure() {
    $this->widgetSchema['name'] = new sfWidgetFormFilterInput(array('with_empty' => false));
    $this->validatorSchema['name'] = new sfValidatorPass(array('required' => false));

    // home place autocompleter
    $this->widgetSchema['home_place'] = new sfWidgetFormFilterJQuerySuggester(array(
      'with_empty' => false,
      'url' => sfContext::getInstance()->getController()->genUrl('obituaries/ac_homeplace')
    ));

    // attach dropdown calendars for date fields
    $this->widgetSchema['death_date'] = new sfWidgetFormFilterDate(array(
      'from_date' => new sfWidgetFormDateJQueryUI(array("year_range"=>"1990:c")),
      'to_date' => new sfWidgetFormDateJQueryUI(array("year_range"=>"1990:c")),
      'with_empty' => false
    ));
/*    $this->widgetSchema['death_date'] = new sfWidgetFormFilterDate(array(
      'from_date' => new sfWidgetFormJQueryDate( array('date_widget' => $this->widgetSchema['death_date']->getOption('from_date')) ),
      'to_date' => new sfWidgetFormJQueryDate( array('date_widget' => $this->widgetSchema['death_date']->getOption('to_date')) ),
      'with_empty' => false
    ));
*/
  }

  protected function addNameColumnQuery(Doctrine_Query $query, $field, $values) {
    // $values['text'] contains the field value
  	if($values['text']!=='') {
      $where = 'first_name LIKE ? OR middle_name LIKE ? OR last_name LIKE ? OR CONCAT_WS(" ", first_name, last_name) LIKE ? OR CONCAT_WS(" ", first_name, middle_name, last_name) LIKE ?';
      $search_params = array();
      $search_params[] = '%'.$values['text'].'%';
      $search_params[] = '%'.$values['text'].'%';
      $search_params[] = '%'.$values['text'].'%';
      $search_params[] = '%'.$values['text'].'%';
      $search_params[] = '%'.$values['text'].'%';

    	if($where) {
        $query->addWhere($where, $search_params);
    	}
    }
  }

}
