<?php

/**
 * PlgEvent filter form.
 *
 * @package    verse3
 * @subpackage filter
 * @author     Vladimir Droznik
 * @version    SVN: $Id: sfDoctrineFormFilterTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class PlgEventFormFilter extends BasePlgEventFormFilter
{
  public function configure()
  {
    $this->widgetSchema['timestamp'] = new sfWidgetFormFilterDate(array(
      'from_date' => new sfWidgetFormDateJQueryUI(array("year_range"=>"1990:c+1")),
      'to_date' => new sfWidgetFormDateJQueryUI(array("year_range"=>"1990:c+1")),
      'with_empty' => false
    ));
    $this->validatorSchema['timestamp'] = new sfValidatorDateRange(array('from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)) ) );
  }

  public function convertTimestampValue($value) {
    if($value['from']) {
      $value['from'] = strtotime($value['from']);
    }
    if($value['to']) {
      $value['to'] = strtotime($value['to']);
    }
    return $value;
  }

  public function getFields() {
    $fields = parent::getFields();
    $fields['timestamp'] = 'Date';
    return $fields;
  }
}
