<?php

/**
 * sfGuardPermission filter form.
 *
 * @package    verse3
 * @subpackage filter
 * @author     Vladimir Droznik
 * @version    SVN: $Id: sfDoctrinePluginFormFilterTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class sfGuardPermissionFormFilter extends PluginsfGuardPermissionFormFilter
{
  public function configure()
  {
  }
}
