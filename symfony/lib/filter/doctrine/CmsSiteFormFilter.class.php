<?php

/**
 * CmsSite filter form.
 *
 * @package    verse3
 * @subpackage filter
 * @author     Vladimir Droznik
 * @version    SVN: $Id: sfDoctrineFormFilterTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class CmsSiteFormFilter extends BaseCmsSiteFormFilter
{
  public function configure()
  {
  }
}
