<?php

/**
 * PlgObituaryImage filter form.
 *
 * @package    verse3
 * @subpackage filter
 * @author     Vladimir Droznik <vladimir.droznik@gmail.com>
 * @version    SVN: $Id: sfDoctrineFormFilterTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class PlgObituaryImageFormFilter extends BasePlgObituaryImageFormFilter {

    public function configure() {
        $this->widgetSchema['obituary_id']->addOption('order_by', array('last_name', 'asc'))->addOption('method', 'getNameForSelect')->addOption('add_empty', '--- All ---');
        $this->widgetSchema['state'] = new sfWidgetFormChoice(array(
            'choices' => PlgObituaryImageTable::getImageStates(),
            'expanded' => true,
            'multiple' => true
        ));

        $this->widgetSchema['created_at'] = new sfWidgetFormFilterDate(array(
            'from_date' => new sfWidgetFormDateJQueryUI(array("year_range"=>"1990:c")),
            'to_date' => new sfWidgetFormDateJQueryUI(array("year_range"=>"1990:c")),
            'with_empty' => false
        ));

        $this->validatorSchema['state'] = new sfValidatorChoice(array(
            'choices' => array_keys(PlgObituaryImageTable::getImageStates()),
            'multiple' => true,
            'required' => false
        ));
    }

    public function getFields() {
        $fields = parent::getFields();
        // in order for state dropdown work
        $fields['state'] = 'ForeignKey';
        return $fields;
    }
}
