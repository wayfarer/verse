<?php

/**
 * PlgList filter form.
 *
 * @package    verse3
 * @subpackage filter
 * @author     Vladimir Droznik
 * @version    SVN: $Id: sfDoctrineFormFilterTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class PlgListFormFilter extends BasePlgListFormFilter
{
  public function configure()
  {
    //  suggester for type
    $this->widgetSchema['type'] = new sfWidgetFormFilterJQuerySuggester(array(
      'url' => sfContext::getInstance()->getController()->genUrl('lists/ac_type'),
      'with_empty' => false
    ));

    // attach dropdown calendars for date fields
    $this->widgetSchema['created_at'] = new sfWidgetFormFilterDate(array(
      'from_date' => new sfWidgetFormDateJQueryUI(array("year_range"=>"1990:c")),
      'to_date' => new sfWidgetFormDateJQueryUI(array("year_range"=>"1990:c")),
      'with_empty' => false
    ));
  }
}
