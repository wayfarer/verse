<?php

/**
 * PlgBlockIp filter form.
 *
 * @package    verse3
 * @subpackage filter
 * @author     Vladimir Droznik <vladimir.droznik@gmail.com>
 * @version    SVN: $Id: sfDoctrineFormFilterTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class PlgBlockIpFormFilter extends BasePlgBlockIpFormFilter {

    public function configure() {
        if (!sfContext::getInstance()->getUser()->isSuperAdmin()) {
            unset($this['domain_id']);
        }
        $this->widgetSchema['created_at'] = new sfWidgetFormFilterDate(array(
            'from_date' => new sfWidgetFormDateJQueryUI(array("year_range"=>"1990:c")),
            'to_date' => new sfWidgetFormDateJQueryUI(array("year_range"=>"1990:c")),
            'with_empty' => false
        ));
        $this->widgetSchema['updated_at'] = new sfWidgetFormFilterDate(array(
            'from_date' => new sfWidgetFormDateJQueryUI(array("year_range"=>"1990:c")),
            'to_date' => new sfWidgetFormDateJQueryUI(array("year_range"=>"1990:c")),
            'with_empty' => false
        ));
    }
}
