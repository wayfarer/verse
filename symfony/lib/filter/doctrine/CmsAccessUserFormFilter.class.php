<?php

/**
 * CmsAccessUser filter form.
 *
 * @package    verse3
 * @subpackage filter
 * @author     Vladimir Droznik
 * @version    SVN: $Id: sfDoctrineFormFilterTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class CmsAccessUserFormFilter extends BaseCmsAccessUserFormFilter
{
  public function configure()
  {
    // statuses
    $this->widgetSchema['enabled'] = new sfWidgetFormChoice(array(
      'choices' => CmsAccessUserTable::getUserStatuses(),
      'expanded' => true,
      'multiple' => true
    ) );

    $this->validatorSchema['enabled'] = new sfValidatorChoice(array(
      'choices' => array_keys(CmsAccessUserTable::getUserStatuses()),
      'multiple' => true,
      'required' => false
    ));
  }

  public function getFields()
  {
    $fields = parent::getFields();
    // in order for status dropdown work
    $fields['enabled'] = 'ForeignKey';
    return $fields;
  }

}
