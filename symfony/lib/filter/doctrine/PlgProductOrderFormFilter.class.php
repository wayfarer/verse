<?php

/**
 * PlgProductOrder filter form.
 *
 * @package    verse3
 * @subpackage filter
 * @author     Vladimir Droznik
 * @version    SVN: $Id: sfDoctrineFormFilterTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class PlgProductOrderFormFilter extends BasePlgProductOrderFormFilter
{
  public function configure()
  {
    // attach dropdown calendars for date fields
    $this->widgetSchema['timestamp'] = new sfWidgetFormFilterDate(array(
      'from_date' => new sfWidgetFormDateJQueryUI(array("year_range"=>"2008:c")),
      'to_date' => new sfWidgetFormDateJQueryUI(array("year_range"=>"2008:c")),
      'with_empty' => false
    ));

    $this->widgetSchema['order_domain_name'] = new sfWidgetFormFilterJQuerySuggester(array(
      'with_empty' => true,
      'empty_label' => 'this domain',
      'url' => sfContext::getInstance()->getController()->genUrl('products_orders/order_domain')
    ));
  }
}
