<?php

/**
 * SmsDomain filter form.
 *
 * @package    verse3
 * @subpackage filter
 * @author     Vladimir Droznik
 * @version    SVN: $Id: sfDoctrineFormFilterTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class SmsDomainFormFilter extends BaseSmsDomainFormFilter
{
  public function configure()
  {
    $this->widgetSchema['server_id'] = new sfWidgetFormChoice(array(
      'choices' => SmsDomainTable::getServers(),
    ));
    $this->validatorSchema['server_id'] = new sfValidatorChoice(array(
      'choices' => array_keys(SmsDomainTable::getServers()),
      'required' => false
    ));

    // domains modes
    $this->widgetSchema['mode'] = new sfWidgetFormChoice(array(
      'choices' => SmsDomainTable::getDomainModes(),
      'expanded' => true,
      'multiple' => false
    ));
    $this->validatorSchema['mode'] = new sfValidatorChoice(array(
      'choices' => array_keys(SmsDomainTable::getDomainModes()),
      'multiple' => false,
      'required' => false
    ));

    // attach dropdown calendars for date fields
    $this->widgetSchema['created_at'] = new sfWidgetFormFilterDate(array(
      'from_date' => new sfWidgetFormDateJQueryUI(array("year_range"=>"2009:c")),
      'to_date' => new sfWidgetFormDateJQueryUI(array("year_range"=>"2009:c")),
    ));
  }

  public function getFields()
  {
    $fields = parent::getFields();
    // in order for state dropdown work
    $fields['mode'] = 'ForeignKey';
    $fields['server_id'] = 'ForeignKey';
    return $fields;
  }
}
