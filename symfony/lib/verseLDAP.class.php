<?php

chdir('pear');
include_once 'Net/LDAP2.php';
chdir('..');

class VerseLDAP {
  protected
    $myCacheConfig = array(
            'path'    => '/tmp/ldap.cache',
            'max_age' => 1200),
    $baseDN = "o=domains,dc=twintierstech,dc=net",
    $con,
    $logger;

  public function __construct() {
    $logPath = sfConfig::get('sf_log_dir').'/ldap.log';
    $this->logger = new sfFileLogger(new sfEventDispatcher(), array('file' => $logPath));
  }


  public function connect() {
    $myCacheObject = new Net_LDAP2_SimpleFileSchemaCache($this->myCacheConfig);

    $ldap_config = sfConfig::get('app_ldap_config');

    $this->con = Net_LDAP2::connect($ldap_config);

    $this->logger->info('Connecting to LDAP: '.(PEAR::isError($this->con)?'failed':'success'));

    return !PEAR::isError($this->con);
  }

  public function entryExists($dsn) {
    $entry = $this->con->getEntry($dsn.','.$this->baseDN);
    $this->logger->info('entryExists: '.$dsn.' => '.(PEAR::isError($entry)?'0':'1'));
    return !PEAR::isError($entry);
  }

  public function move($dsn, $newdsn) {
    $this->logger->info('move: '.$dsn.','.$this->baseDN."=>".$newdsn.','.$this->baseDN);
    $ret = $this->con->move($dsn.','.$this->baseDN, $newdsn.','.$this->baseDN);
    if(PEAR::isError($ret)) {
      $this->logger->info('move error: '.$ret->getMessage().'; '.$ret->getUserInfo());
    }
  }

  public function modify($dsn, $params) {
    $this->logger->info('modify: '.$dsn);
    $ret = $this->con->modify($dsn.','.$this->baseDN, $params);
    if(PEAR::isError($ret)) {
      $this->logger->info('modify error: '.$ret->getMessage().'; '.$ret->getUserInfo());
    }
  }

  public function addEntry($dsn, $attributes) {
    $this->logger->info('addEntry: '.$dsn.','.$this->baseDN);
    $entry = Net_LDAP2_Entry::createFresh($dsn.','.$this->baseDN, $attributes);
    $ret = $this->con->add($entry);
    if(PEAR::isError($ret)) {
      $this->logger->info('addEntry error: '.$ret->getMessage().'; '.$ret->getUserInfo());
    }
    return $ret;
  }
}
