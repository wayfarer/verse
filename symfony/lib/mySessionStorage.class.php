<?php

class mySessionStorage extends sfSessionStorage
{
  public function initialize($options = null)
  {
    // work-around for swfuploader
    if(sfContext::getInstance()->getRequest()->getParameter('su')) {
      session_id(sfContext::getInstance()->getRequest()->getParameter('su'));
    }

    // cross-domain session passing mechanizm
    if(sfContext::getInstance()->getRequest()->getParameter('z')) {
      // catch session data
      session_id(sfContext::getInstance()->getRequest()->getParameter('z'));
      session_start();
      // change session id
      session_regenerate_id();

      // proceed with new session id and catched session data from specified session
      header("Location: {$_SERVER['PHP_SELF']}");
      exit;
    }

    parent::initialize($options);
  }
}
