<?php

class verseMediaFile {
    protected
        $name = '',
        $extension = '',
        $lower_extension = '',
        $fullname = '',
        $orig_file_name = '',
        $directory = '',
        $path = '';

    public function __construct($path) {
        $this->path = $path;
        $pathinfo = pathinfo($path);
        $this->name = $pathinfo['filename'];
        $this->extension = $pathinfo['extension'];
        $this->lower_extension = strtolower($pathinfo['extension']);
        $this->fullname = $pathinfo['basename'];
        $this->orig_file_name = $pathinfo['basename'];
        $this->directory = $pathinfo['dirname'];
    }

    public function getName() {
        return $this->name;
    }

    public function setName($name) {
        $this->name = $name;
        $this->path = $this->directory.'/'.$name;
    }
      
    public function rename() {
        $this->name .= time();
        $this->fullname = $this->name.'.'.$this->extension;
        $this->path = $this->directory.'/'.$this->fullname;
    }

    public function newName($obituary_id) {
        $this->name = $obituary_id.'-'.rand(10000, 99999);
        $this->fullname = $this->name.'.'.$this->extension;
    }

    public function getPath() {
        return $this->path;
    }

    public function getFullName() {
        return $this->fullname;
    }

    public function getDirectory() {
        return $this->directory;
    }

    public function getExtension() {
        return $this->extension;
    }

    public function isMovie() {
        if ($this->lower_extension == 'avi' || $this->lower_extension == 'mpg' || $this->lower_extension == 'flv' || $this->lower_extension == 'mov' || $this->lower_extension == 'wmv' || $this->lower_extension == 'mp4') {
            return true;
        } else {
            return false;
        }
    }

    public function getThumbnail() {
        if ($this->isMovie()) {
            return $this->getThumbnailForMovie();
        } else {
            return $this->getThumbnailForPhoto();
        }
    }

    public function getThumbnailForMovie() {
        $this->createThumbnailForMovie();
        return '<img src="/'.$this->directory.'/'.$this->name.".jpg".'" alt="'.$this->name.".jpg".'"" />';
    }

    public function createThumbnailForMovie() {
        $this->removeSpacesFromName();
        $video_duration = $this->getDuration();
		$step_frame = $video_duration/10;
        $i = 0;
        for ($a = 1; $a < $video_duration; $a = $a + $step_frame) {
		    exec(sfConfig::get('app_ffmpegpath')."ffmpeg -ss ".$a." -i ".$this->path." -vcodec mjpeg -vframes 1 -an -f rawvideo -s ".$this->getAddressability()." -y ".$this->directory.'/'.$this->name."-".$i.".jpg");
            $filesize = filesize($this->directory.'/'.$this->name."-".$i.".jpg");
			$thumb_sizes[$filesize] = $i;
            $i++;
		}
		ksort($thumb_sizes);
        end($thumb_sizes);
        rename($this->directory.'/'.$this->name."-".prev($thumb_sizes).".jpg", $this->directory.'/'.$this->name.".jpg");
        for ($i = 0; $i < 10; $i++) {
            if (file_exists($this->directory.'/'.$this->name."-".$i.".jpg")) {
                unlink($this->directory.'/'.$this->name."-".$i.".jpg");
            }
        }
    }

    public function removeSpacesFromName() {
        $this->name = str_replace(" ", "_", $this->name);
        $this->fullname = $this->name.'.'.$this->extension;
        $old_path = $this->path;
        $this->path = $this->directory.'/'.$this->fullname;
        if ($this->path !== $old_path) {
            rename($old_path, $this->path);
        }
    }

    public function getThumbnailForPhoto() {
        if (!trim($this->fullname)) return;
        $tnname = $this->name.'_thumb.'.$this->lower_extension;
        $max_size = 100;
        $img = new sfImage($this->path, 'image/'.$this->lower_extension);
        if (!file_exists($this->directory.'/'.$tnname) && ($img->getHeight() > $max_size || $img->getWidth() > $max_size)) {
            $img->thumbnail($max_size, $max_size, 'scale');
            $img->setQuality(60);
            $img->saveAs($this->directory.'/'.$tnname);
            $imgsrc = $this->directory.'/'.$tnname;
            $imgalt = $tnname;
        } else {
            $imgsrc = $this->path;
            $imgalt = $this->fullname;
        }
        return '<img src="/'.$imgsrc.'" alt="'.$imgalt.'"" />';
    }

    public function getDuration() {
        ob_start();
        passthru(sfConfig::get('app_ffmpegpath') . "ffmpeg -i " . $this->path . " 2>&1");
        $duration = ob_get_clean();
        $search = '/Duration: (.*?)[.]/';
        $duration = preg_match($search, $duration, $matches, PREG_OFFSET_CAPTURE);
        $duration = @$matches[1][0];
        @list($hours, $mins, $secs) = split('[:]', $duration);
        $hours_in_secs = $hours * 60 * 60;
        $mins_in_secs = $mins * 60;
        $total_secs = $hours_in_secs + $mins_in_secs + $secs;
        return $total_secs;
    }

    public function getSizes() {
        ob_start();
        passthru(sfConfig::get('app_ffmpegpath') . "ffmpeg -i " . $this->path . " 2>&1");
        $sizes_a = ob_get_clean();
        if (preg_match('/(\d+)x(\d+)/', $sizes_a, $matches)) {
            return array(
                'w' => $matches[1],
                'h' => $matches[2]
            );
        } else {
            return false;
        }
    }

    public function getAddressability() {
        $video_sizes = $this->getSizes();
        if ($video_sizes !== false) {
            $addressability = '100x'.(100*$video_sizes['h']/$video_sizes['w']);
        } else {
            $addressability = '100x100';
        }
        return $addressability;
    }

    public function save($obituary_id, $domain_name, $domain_id, $state = 1, $caption = '') {
        $uploadDir = 'files/'.$domain_name.'/';
        if ($this->isMovie()) {
            $table = new PlgObituaryMovie();
            $path = 'movies/';
        } else {
            $table = new PlgObituaryImage();
            $path = 'image/scrap/';
        }
        $uploadDir .= $path;
        if (!file_exists($uploadDir)) {
            mkdir($uploadDir);
        }

        if (file_exists($this->path)) {
            // create thumbnail for movie uploaded from backend
            if ($this->isMovie() && !file_exists($this->directory.'/'.$this->name.'.jpg')) {
                $this->createThumbnailForMovie();
            }

            $oldname = $this->name;
            $this->newName($obituary_id);
            $destination_path = realpath($uploadDir).'/';

            if(rename($this->path, $destination_path.$this->name.'.'.$this->lower_extension)) {
                $table->setObituaryId($obituary_id);
                $table->setDomainId($domain_id);
                $table->setState($state);
                $table->setCaption($caption);
                $table->setOrigfilename($this->orig_file_name);
                if ($this->isMovie()) {
                    if (rename($this->directory.'/'.$oldname.'.jpg', $destination_path.$this->name.'.jpg')) {
                        $table->setThumbnail($path.$this->name.'.jpg');
                    }
                    $table->setLocal(1);
                    $table->setPath($path.$this->name.'.'.$this->lower_extension);
                    $table->Save();
                    $thumbname = $oldname.'.jpg';
                } else {
                    $table->setPath($path.$this->name.'.'.$this->lower_extension);
                    $table->Save();
                    $thumbname = $oldname.'_thumb.'.$this->lower_extension;
                }
                if (file_exists($this->directory.'/'.$thumbname)) {
                    unlink($this->directory.'/'.$thumbname);
                }
            }
        }
    }

    public function delete() {
        if (file_exists($this->path)) {
            unlink($this->path);
        }
        if ($this->isMovie()) {
            $thumbname = $this->name.".jpg";
        } else {
            $thumbname = $this->name.'_thumb.'.$this->extension;
        }
        if (file_exists($this->directory.'/'.$thumbname)) {
            unlink($this->directory.'/'.$thumbname);
        }
    }
}
