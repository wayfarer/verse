<?php

class sfWidgetFormDateTimeJQueryUI extends sfWidgetFormDateTime
{
// TODO: generate minutes with 5 minutes interval
/*  protected function configure($options = array(), $attributes = array()) {
    parent::configure();
    $this->setOption('time', array('minutes'=>array("0"=>"00")));
  }
*/
  function render($name, $value = null, $attributes = array(), $errors = array())
  {
    $date = $this->getDateWidget($attributes)->render($name.'[date]', $value);

    if (!$this->getOption('with_time'))
    {
      return $date;
    }

    return strtr($this->getOption('format'), array(
      '%date%' => $date,
      '%time%' => $this->getTimeWidget($attributes)->render($name, $value),
    ));
  }

  protected function getDateWidget($attributes = array()) {
    return new sfWidgetFormDateJQueryUI($this->getOptionsFor('date'), $this->getAttributesFor('date', $attributes));
  }
 }
