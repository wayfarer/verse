<?php
class sfWidgetFormInputCKFinder extends sfWidgetFormInputText
{
  /**
   * Constructor.
   *
   * Available options:
   *
   *  * theme:  The Tiny MCE theme
   *  * width:  Width
   *  * height: Height
   *  * config: The javascript configuration
   *
   * @param array $options     An array of options
   * @param array $attributes  An array of default HTML attributes
   *
   * @see sfWidgetForm
   */
  protected function configure($options = array(), $attributes = array())
  {
    parent::configure($options, $attributes);

    $this->addOption('startupPath', 'Images:/');
    $this->addOption('type');
    $this->addOption('configImages', null);
  }

  /**
   * @param  string $name        The element name
   * @param  string $value       The value selected in this widget
   * @param  array  $attributes  An array of HTML attributes to be merged with the default HTML attributes
   * @param  array  $errors      An array of errors for the field
   *
   * @return string An HTML tag string
   *
   * @see sfWidgetForm
   */
  public function render($name, $value = null, $attributes = array(), $errors = array())
  {
    $attributes['id'] = $this->generateId($name);
    $attributes['class'] = 'ckfinder_input';
    $attributes['readonly'] = 'true';
    $input = parent::render($name, $value, $attributes, $errors);
    $input.=$this->renderTag('input', array('type'=>'button', 'value'=>'browse', 'onclick'=>'CKFinder_Popup()'));
    $input.=$this->renderTag('input', array('type'=>'button', 'value'=>'clear', 'onclick'=>'$("#'.$attributes['id'].'").val("")'));
    if($value) {
      $input.='<br/>'.image_tag(VerseUtil::thumbnail($value, 200));
    }
//    $img = image_tag('/img/album.gif', array('id'=>$attributes['id'].'_browse'));

    $js = sprintf( <<<JS
<script type="text/javascript">
      function CKFinder_SetURL(url) {
        $("#%s").val(unescape(url));
      }

      function CKFinder_Popup() {
        var finder = new CKFinder() ;
        // The path for the installation of CKFinder (default = "/ckfinder/").
        finder.BasePath = '/ckfinder/' ;

        //Startup path in a form: "Type:/path/to/directory/"
        finder.StartupPath = '%s';
  //      if(this.only_type) finder.type = this.only_type;
        finder.ResourceType = '%s';

        // Name of a function which is called when a file is selected in CKFinder.
        finder.SelectFunction = CKFinder_SetURL;

        // Additional data to be passed to the SelectFunction in a second argument.
        // We'll use this feature to pass the Id of a field that will be updated.
        // finder.SelectFunctionData = functionData ;

        // Name of a function which is called when a thumbnail is selected in CKFinder.
        // finder.SelectThumbnailFunction = ShowThumbnails ;

        finder.Popup();
      }
</script>
JS
    ,
    $attributes['id'],
    $this->getOption('startupPath'),
    $this->getOption('type'),
    $attributes['id']
    );

    if (!is_null($this->getOption('configImages'))) {
        $_SESSION['CKFinder_configImages'] = $this->getOption('configImages');
    }

    return $input.$js;
  }
}
