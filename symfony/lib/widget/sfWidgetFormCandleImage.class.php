<?php

class sfWidgetFormCandleImage extends sfWidgetFormChoice {

    /**
     * Constructor.
     *
     * Available options:
     *
     *  * theme:  Obituary icons theme
     *  * choices: Obituary candle images
     *
     * @param array $options     An array of options
     * @param array $attributes  An array of default HTML attributes
     *
     * @see sfWidgetForm
     */
    protected function configure($options = array(), $attributes = array()) {
        parent::configure($options, $attributes);
        $this->addOption('expanded', true);
        $this->addRequiredOption('theme');
        $this->addRequiredOption('choices');
    }

    /**
     * @param  string $name        The element name
     * @param  string $value       The value selected in this widget
     * @param  array  $attributes  An array of HTML attributes to be merged with the default HTML attributes
     * @param  array  $errors      An array of errors for the field
     *
     * @return string An HTML tag string
     *
     * @see sfWidgetForm
     */
    public function render($name, $value = null, $attributes = array(), $errors = array()) {
        $input = '';

        $theme = $this->getOption('theme');
        $images = $this->getOption('choices');

        $input .= '<ul class="radio_list candle-images">';
        foreach($images as $key => $path) {
            $input .= '<li><img src="/obits/icons/'.$theme.'/'.$path.'" class="candle-image" />'.$this->renderTag('input', array('type'=>'radio', 'value'=>$key, 'name'=>'candle[candle_image]', 'id'=>'candle_candle_image_'.$key)).'</li>';
        }
        $input .= '</ul>';

        return $input;
    }
}