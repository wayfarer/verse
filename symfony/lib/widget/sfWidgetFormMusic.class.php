<?php

class sfWidgetFormMusic extends sfWidgetFormChoice
{
  protected function configure($options = array(), $attributes = array())
  {
    parent::configure($options, $attributes);
  }

  public function render($name, $value = null, $attributes = array(), $errors = array())
  {
    $html = parent::render($name, $value, $attributes, $errors);
    $html .= '<div style="clear:left; margin-top:3px"><label></label><div id="music_preview"></div></div>';
    $html .= <<<EOF
<script type="text/javascript">
jQuery(function() {
    $('#config_obituary_music').bind('change', function() {
      document.getElementById('mpl').sendEvent("stop");
      document.getElementById('mpl').loadFile({file:'/'+$(this).val()});
    } );
    // write player
    var so = new SWFObject('/tools/mediaplayer.swf','mpl','220','20','8');
    so.addParam('allowscriptaccess','always');
    so.addParam('allowfullscreen','false');
    so.addVariable('height','20');
    so.addVariable('width','220');
    so.addVariable('showdigits','false');
    so.addVariable('showstop','true');
    so.addVariable('repeat','false');
    so.addVariable('type','mp3');
    so.addVariable("enablejs", "true");
    so.write('music_preview');
});
</script>
EOF;
    return $html;
  }

/*  public function getJavaScripts()
  {
  	return array_merge(array('/js/swfobject.js'), parent::getJavaScripts());
  }
*/
  /*
   *
   * Gets the stylesheet paths associated with the widget.
   *
   * @return array An array of stylesheet paths
   */
/*  public function getStylesheets()
  {
    $theme = $this->getOption('theme');
    return array($theme => 'screen');
  }
*/
  /**
   * Gets the JavaScript paths associated with the widget.
   *
   * @return array An array of JavaScript paths
   */
/*  public function getJavaScripts()
  {
    //check if jquery is loaded
    $js = array();
    if (sfConfig::has('sf_jquery_web_dir') && sfConfig::has('sf_jquery_core'))
      $js[] = sfConfig::get('sf_jquery_web_dir').'/js/'.sfConfig::get('sf_jquery_core');
    else
      $js[] = '/sfJQueryUIPlugin/js/jquery-1.3.1.min.js';

    $js[] = '/sfJQueryUIPlugin/js/jquery-ui.js';

    $culture = $this->getOption('culture');
    if ($culture!='en')
      $js[] = '/sfJQueryUIPlugin/js/i18n/ui.datepicker-'.$culture.".js";

    return $js;
  }
*/
}
