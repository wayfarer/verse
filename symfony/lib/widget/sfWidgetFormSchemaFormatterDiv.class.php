<?php

class sfWidgetFormSchemaFormatterDiv extends sfWidgetFormSchemaFormatter
{
 protected
    $rowFormat      = "<div>\n %error%%label%\n %field%%help%\n%hidden_fields%</div>\n",
    $nestedFormFormat = "%field%",
    $errorRowFormat = "<div>\n%errors%</div>\n",
    $helpFormat     = '<br />%help%',
    $decoratorFormat = "\n %content%";
}
