<?php

class VerseUtil {
  static public $threeStates = array(0=>"Default", 1=>"On", 2=>"Off");

  static public function getThreeStates() {
    return self::$threeStates;
  }

  static public function leadingSlash($path) {
    $path = trim($path);
    if($path && $path[0]!="/") $path = "/".$path;
    return $path;
  }

  static public function getSuggestions($component, $field, $query, $limit = 0) {
    if(!$limit) return $query;

    $table = Doctrine::getTable($component)->getTableName();

    // make use of rawsql here because we need DISTINCT values
    // doctrine objects automatically add primary key to SELECT set, so DISTINCT has no effect
    $q = new Doctrine_RawSql();
    $q->select("{t.$field}")
        ->from("$table t")
        ->addComponent('t', $component)
        ->distinct()
        ->where("t.$field LIKE ? AND t.domain_id=?", array("%$query%", Doctrine::getTable('SmsDomain')->getCurrent()->getDomainId()) )
        ->limit($limit);

/*    $q = Doctrine::getTable($table)->createQuery()
          ->select($field)
          ->distinct(true)
          ->where("$field LIKE ? AND domain_id=?", array("%$query%", Doctrine::getTable('SmsDomain')->getCurrent()->getDomainId()) )
          ->limit($limit);
*/
    $ret = $q->execute();
    $suggestions = array();
    foreach($ret as $record) {
//      $suggestions[$record->get($field)] = $record->get($field);
      $suggestions[] = $record->get($field);
    }
    return $suggestions;
  }

  static public function decorateValue($value, $class) {
    if($value) {
      $value = '<span class="'.$class.'">'.$value.'</span>';
    }
    return $value;
  }

  static public function thumbnail($imagepath, $max_size = 100) {
    $domain_name = Doctrine::getTable('SmsDomain')->getCurrent()->getDomainName();
    $a_domain_name = $domain_name;
    
    if(!trim($imagepath)) return;
    if($imagepath[0]=='/') {
        $imagepath = substr($imagepath, 1);
    }
    if(strpos($imagepath, "/")!==false || strpos($imagepath, "\\")!==false) {
        $potential_domain = substr($imagepath, 0, strpos($imagepath, '/'));
        if(is_dir("files/$potential_domain")) {
            $a_domain_name = $potential_domain;
            $imagepath = substr($imagepath, strpos($imagepath, '/')+1);
        }
    }
    else {
        $imagepath = "image/$imagepath";
    }
    $uploadDir = sfConfig::get('sf_web_dir').'/files/'.$a_domain_name;
    $full_imagepath = $uploadDir.self::leadingSlash($imagepath);

    if(!file_exists($full_imagepath)) { // try to find it under image/ still not found
        $full_imagepath = $uploadDir.'/image'.self::leadingSlash($imagepath);
    }
    if(file_exists($full_imagepath)) {
      $pathinfo = pathinfo($imagepath);
      $tnname = $pathinfo['dirname'].'/'.$pathinfo['filename'].'_'.$max_size.'.'.$pathinfo['extension'];
      $tnpath = sfConfig::get('app_thumbnails_dir').self::leadingSlash($tnname);
  //    $tndir = $pathinfo['dirname'].'/'.sfConfig::get('app_thumbnails_dir');
  //    $tnpath = $tndir.'/'.$pathinfo['basename'];
      if(!file_exists($uploadDir.self::leadingSlash($tnpath))) {
        $img = new sfImage($full_imagepath);
        $img->thumbnail($max_size, $max_size, 'scale');
        //$img->resize(100, null);
        $img->setQuality(60);

        // ensure destination directory
        $pathinfo = pathinfo($tnpath);
        if(!file_exists($uploadDir.'/'.$pathinfo['dirname'])) {
          // recursively create destination directory
          mkdir($uploadDir.'/'.$pathinfo['dirname'], 0755, true);
        }
        $img->saveAs($uploadDir.'/'.$tnpath);
      }
      if($a_domain_name != $domain_name) {
          $tnpath = $a_domain_name.'/'.$tnpath;
      }
      return $tnpath;
    }
    else return $imagepath;
  }

  static public function obit_photo_thumbnail($imagepath, $max_width = 180) {
    $imagepath_old = $imagepath;

    if($imagepath[0]=='/') {
        $imagepath = substr($imagepath, 1);
    }

    if(strpos($imagepath, "/")!==false) {
        $potential_domain = substr($imagepath, 0, strpos($imagepath, '/'));
        if($potential_domain && is_dir("files/$potential_domain")) {
            $a_domain_name = $potential_domain;
            $imagepath = substr($imagepath, strpos($imagepath, '/')+1);
        }
    }

    $imagepath = self::leadingSlash($imagepath);

    if(strpos($imagepath, "obituaries/")!==false) {
    	$imagepath = str_replace("obituaries/", "image/obituaries/", $imagepath);
    }
    if(isset($a_domain_name)) {
        $full_image_path = sfConfig::get('sf_web_dir').'/files/'.$a_domain_name.($imagepath);
    } else {
        $full_image_path = sfConfig::get('sf_web_dir').'/files/'.Doctrine::getTable('SmsDomain')->getCurrent()->getDomainName().($imagepath);
    }

    if(file_exists($full_image_path)) {
        $img = new sfImage($full_image_path);
        $height = $img->getHeight();
        $width = $img->getWidth();
        if($height > $width) {
            $max_size = $max_width*$height/$width;
        } else {
            $max_size = $max_width;
        }
    } else {
        $max_size = $max_width;
    }
    
    $path = self::thumbnail($imagepath_old, $max_size);
    return self::leadingSlash($path);
  }
  
  static public function resizeOriginal($imagepath, $max_width) {
    if(!trim($imagepath)) return;
    $uploadDir = sfConfig::get('sf_web_dir').'/files/'.Doctrine::getTable('SmsDomain')->getCurrent()->getDomainName();
    $full_imagepath = $uploadDir.self::leadingSlash($imagepath);

    if(file_exists($full_imagepath)) {
      $img = new sfImage($full_imagepath);
      if($img->getWidth() > $max_width) {
        $img->setQuality(100);
        $img->resize($max_width, null);
        $img->save();
      }
    }
    return $imagepath;
  }

  static public function obitThemeImageThumbnail($imagepath, $max_size = 100) {
      if(!trim($imagepath)) return;

      $tndir = sfConfig::get('sf_web_dir').'/img/ob_theme_thumbnails';
      $full_imagepath = sfConfig::get('sf_web_dir').self::leadingSlash($imagepath);

      if(file_exists($full_imagepath)) {
          $pathinfo = pathinfo($imagepath);
          $tnname = str_replace('img/ob_theme', '', $pathinfo['dirname']).'/'.$pathinfo['filename'].'_'.$max_size.'.'.$pathinfo['extension'];
          $tnpath = $tndir.self::leadingSlash($tnname);
          if(!file_exists($tnpath)) {
              $img = new sfImage($full_imagepath);
              $img->thumbnail($max_size, $max_size, 'scale');
              $img->setQuality(100);

              // ensure destination directory
              $pathinfo = pathinfo($tnpath);
              if(!file_exists($pathinfo['dirname'])) {
                  // recursively create destination directory
                  mkdir($pathinfo['dirname'], 0755, true);
              }
              $img->saveAs($tnpath);
          }
          return str_replace(sfConfig::get('sf_web_dir'), '', $tnpath);
      } else return $imagepath;
  }

  static public function getObituaryThemes($with_default = false) {
    $themes = array('classic'=>'Classic', 'classic_frame'=>'Classic with Frame', 'classic_frame_blue'=>'Classic with Blue Frame', 'fo' => 'FO', 'nt_frame' => 'NT with Frame', 'nt_without_frame' => 'NT without Frame');
    if($with_default) {
    	$themes = array(''=>'Default') + $themes;
    }
  	return $themes;
  }

  static public function getObituaryThemeImages($with_default = false, $path = "") {
	  static $theme_images = array();
      if(!count($theme_images)) {
          $theme_images = self::getObituaryBgImages("img/ob_theme".$path, $with_default);
      }
      return $theme_images;
  }

  static public function getObituaryMilitaryImages($with_default = false, $path = "") {
      static $military_images = array();
      if(!count($military_images)) {
          $military_images = self::getObituaryBgImages("img/ob_theme/military".$path, $with_default);
      }
      return $military_images;
  }

  static public function getAllObituaryThemeImages($with_default = false) {
      static $all_obituary_theme_images = array();
      if(!count($all_obituary_theme_images)) {
          $all_obituary_theme_images = self::getAllObituaryBgImages($with_default, false);
      }
      return $all_obituary_theme_images;
  }

  static public function getAllObituaryMilitaryImages($with_default = false) {
      static $all_obituary_military_images = array();
      if(!count($all_obituary_military_images)) {
          $all_obituary_military_images = self::getAllObituaryBgImages($with_default, true);
      }
      return $all_obituary_military_images;
  }

  static protected function getAllObituaryBgImages($with_default = false, $military = false) {
      if($military) {
          $path = "/military";
      } else {
          $path = "";
      }
      $all_images = self::getObituaryBgImages("img/ob_theme".$path, $with_default);

      $dirs = array();
      foreach(glob("img/ob_theme".$path."/*", GLOB_ONLYDIR) as $dir) {
          $dirs[$dir] = $dir;
      }
      unset($dirs["img/ob_theme/military"]);

      foreach($dirs as $directory) {
          $all_images += self::getObituaryBgImages($directory, $with_default);
      }

      return $all_images;
  }

  static protected function getObituaryBgImages($image_path, $with_default) {
      $images = array();
      $images['none'] = "No image";

      foreach(glob("$image_path/*.jpg") as $filename) {
          $title = ucfirst(pathinfo($filename, PATHINFO_FILENAME));
          $images[$filename] = $title;
      }

      if($with_default) {
          return array(''=>'Default') + $images;
      } else {
          return $images;
      }
  }

  static public function getObituaryCalendarThemes() {
    $themes = array('default'=>'Default');
  	return $themes;
  }

  static public function getObituarySearchThemes() {
      $themes = array('violet' => 'Violet');
      return $themes;
  }

  static public function getObituaryIconsThemes($with_default = false) {
    $themes = array('candles'=>'Candles', 
                    'guestbook'=>'Guestbook', 
                    'gift'=>'Candles & Gift (Send a Gift)',
                    'localflorist'=>'Candles & Local florist');
    if($with_default) {
      $themes = array(''=>'Default') + $themes;
    }
    return $themes;
  }

  static public function createDomainDirs($dirname) {
    if(@mkdir($dirname)) {
      mkdir("$dirname/exchange");
      mkdir("$dirname/file");
      mkdir("$dirname/image");
      mkdir("$dirname/movies");
      mkdir("$dirname/image/merchandise");
      mkdir("$dirname/image/obituaries");
      mkdir("$dirname/image/obituaries/scrap");
    }
  }

  static public function updateFtpPathes(sfEvent $event) {
    $params = $event->getParameters();
    if($params['object'] instanceof SmsDomain) {
      $domain = $params['object'];
      foreach($domain->getUsersWithFtpAccess() as $user) {
        $user->updateFtpPath();
      }
    }
  }

  static public function updateLdapEmailDomain(sfEvent $event) {
    $params = $event->getParameters();
    if($params['object'] instanceof SmsDomain) {
      $domain = $params['object'];
      $modified_last = $domain->getLastModified(true);
      if(isset($modified_last['domain_name']) || isset($modified_last['email_enabled'])) {
        $ldap = new verseLDAP();
        if($ldap->connect()) {
          if(isset($modified_last['domain_name'])) {
            $dsn = 'domainName='.$modified_last['domain_name'];
          }
          else {
            $dsn = 'domainName='.$domain['domain_name'];
          }

          if($ldap->entryExists($dsn)) {
            if(isset($modified_last['domain_name'])) {
              // rename entry
              $newdsn = 'domainName='.$domain['domain_name'];
              $ldap->move($dsn, $newdsn);
              $dsn = $newdsn;
            }
            // modify enabled attribute if changed
            if(isset($modified_last['email_enabled'])) {
              $replace = array("accountStatus"=>$domain['email_enabled']?'active':"disabled");
              $ldap->modify($dsn, array("replace"=>$replace));
            }
          }
          else {
            // create new LDAP entry if email enabled for the production domain
            if($domain['email_enabled'] && $domain['mode']==0) {
              // create new entry
              $attributes = array(
                'objectClass'     => 'mailDomain',
                'domainName'      => $domain['domain_name'],
                'accountStatus'   => 'active',
                'cn'              => $domain['domain_name'],
                'enabledService' => array('mail', 'recipientbcc', 'senderbcc'),
                'mtaTransport'    => 'dovecot'
              );
              $dsn = 'domainName='.$domain['domain_name'];

              // create domain entry
              $ret = $ldap->addEntry($dsn, $attributes);

              if(PEAR::isError($ret)) {
                // log error
              }
              else {
                // create ou=Users and ou=Aliases entries under the domain
                $users_attributes = array(
                  'objectClass' => array('organizationalUnit', 'top'),
                  'ou'          => "Users"
                );
                $aliases_attributes = array(
                  'objectClass' => array('organizationalUnit', 'top'),
                  'ou'          => "Aliases"
                );
                $ret = $ldap->addEntry("ou=Users,$dsn", $users_attributes);
                if(PEAR::isError($ret)) {
                  // log error
                }
                $ret = $ldap->addEntry("ou=Aliases,$dsn", $aliases_attributes);
                if(PEAR::isError($ret)) {
                  // log error
                }
              }
            }
          }
        }
      }
    }
  }

  static public function updatePiwikDomain(sfEvent $event) {
    $params = $event->getParameters();
    if($params['object'] instanceof SmsDomain) {
      $domain = $params['object'];
      $modified_last = $domain->getLastModified(true);
      // if domain name changed
      if(isset($modified_last['domain_name'])) {
        $piwik = new PiwikAdapter();
        $piwik->renameDomainLogins($modified_last['domain_name'], $domain);
//        echo 'piwik done';
//        exit;
      }
      else {
        // if switching from UC to production - ensure statistics exist for the domain
        if(isset($modified_last['mode']) && $domain['mode']==0) {
          $piwik = new PiwikAdapter();
          $piwik->ensureDomain($domain);
        }
      }
    }
  }

  static public function onDomainUpdate(sfEvent $event) {
    $params = $event->getParameters();
    if($params['object'] instanceof SmsDomain) {
      $domain = $params['object'];
      if($domain->getServerId() == sfConfig::get('app_verse_server_id')) {
        // process domain if local
        self::updateFtpPathes($event);
      }
      else {
        // pass event to other server
        self::transferDomainEvent($domain);
      }
      self::updateLdapEmailDomain($event);
      self::updatePiwikDomain($event);
    }
  }

  static public function onAdminDeleteObject(sfEvent $event) {
    $params = $event->getParameters();
    if($params['object'] instanceof sfGuardUser) {
      $sfGuardUser = $params['object'];
      if($ftp_user_id = $sfGuardUser->getProfile()->getFtpUserId()) {
        $ftp_user = Doctrine_Core::getTable('FtpDBAccounts')->find($ftp_user_id);
        if($ftp_user) {
          $ftp_user->delete();
        }
      }
    }
  }

  static public function transferDomainEvent($domain) {
    $server_id = $domain->getServerId();
    if($url = sfConfig::get('app_verse_server_url_'.$server_id)) {
      $url.="/gate_domain_event.php";
      $fields[] = 'domain='.serialize($domain);

      $result = self::executeRequest($url, $fields);
      sfContext::getInstance()->getUser()->setFlash('notice', $result);
    }
  }

  static public function transferSession($server_id) {
    if($url = sfConfig::get('app_verse_server_url_'.$server_id)) {
      $url.="/gate_session.php";
      $fields[] = 'session_id='.session_id();
      $fields[] = 'session_data='.serialize($_SESSION);

      self::executeRequest($url, $fields);
    }
  }

  static public function executeRequest($url, $fields) {
    $post = implode("&", $fields);
    $ch = curl_init();

    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

    $result = curl_exec($ch);
    if(curl_errno($ch)) {
      $result = curl_error($ch);
    }

    return $result;
  }

  static public function getMusicFiles($with_default = false) {
    $domain_name = Doctrine::getTable('SmsDomain')->getCurrent()->getDomainName();
  	$music_files = array('off'=>'No music');
  	if($with_default) {
  		$music_files = array_merge(array('default'=>'Default'), $music_files);
  	}
	  // shared music
	  foreach(glob("assets/*.mp3") as $filename) {
	    $fn = substr($filename, 7, -4);
	    $title = ucfirst($fn);
	    $music_files[$filename] = $title;
	  }
	  // local music
	  foreach(glob("files/$domain_name/mp3/*.mp3") as $filename) {
	    $fn = substr($filename, strrpos($filename, "/")+1);
	    $filename = "mp3/$fn";
	    $title = ucfirst(substr($fn, 0, -4));
	    $music_files[$filename] = $title;
	  }
	  return $music_files;
  }

  static public function noBlockKeywords($text) {
      $block_keywords = Doctrine_Core::getTable('PlgBlockKeyword')->createQuery()->select('keyword as keyword')->where('domain_id=0 OR domain_id='.Doctrine::getTable('SmsDomain')->getCurrent()->getDomainId())->fetchArray();
      $no_block_keyword = true;
      foreach ($block_keywords as $block_keyword) {
          if (stripos($text, $block_keyword['keyword']) !== false) {
              $no_block_keyword = false;
              break;
          }
      }
      return $no_block_keyword;
  }

  static public function isCrematory($final_disposition) {
      $filter_crematorium_keywords = array("crematory", "cremation", "crematorium");
      foreach ($filter_crematorium_keywords as $fkw) {
          if (strpos(strtolower($final_disposition), strtolower($fkw)) !== false) {
              return true;
          }
      }
      return false;
  }

  static public function getObituarySlug(array $obituary) {
      if(!empty($obituary['slug'])) {
          $slug = $obituary['slug'];
      } else {
          $death_date = !empty($obituary['death_date']) ? $obituary['death_date'] : @$obituary['death'];
          $slug = self::slugify(date("Y-m", strtotime($death_date)).' '.@$obituary["first_name"].' '.@$obituary["middle_name"].' '.@$obituary["last_name"]);
      }

      return $slug;
  }

  static public function slugify($text) {
      // replace all non letters or digits by -
      $text = preg_replace('/\W+/', '-', $text);

      // trim and lowercase
      $text = strtolower(trim($text, '-'));

      return $text;
  }
}
