<?php

function verse_image_tag($imagepath) {
  return image_tag('/files/'.Doctrine::getTable('SmsDomain')->getCurrent()->getDomainName().VerseUtil::leadingSlash($imagepath));
}