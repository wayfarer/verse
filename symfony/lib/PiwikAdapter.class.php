<?php

/**
 * @desc Integrates VerseSMS and Piwik
 */

class PiwikAdapter {
  protected
    $token_auth = "7494d4b88b8e5e795c72cf2679f15f38",
    $piwik_url = "https://twintierstech.net/piwik/",
    $piwik_get_jscode_tpl = "?module=API&method=SitesManager.getJavascriptTag&idSite=%s&format=PHP",
    $piwik_add_domain_with_id_tpl = "?module=API&method=VerseIntegration.addSiteWithId&idSite=%d&siteName=%s&urls=%s&format=PHP",

    $piwik_add_user_tpl = "?module=API&method=UsersManager.addUser&userLogin=%s&password=%s&email=%s&alias=%s&format=PHP",
    $piwik_update_user_tpl = "?module=API&method=UsersManager.updateUser&userLogin=%s&password=%s&alias=%s&format=PHP",
    $piwik_set_user_access_tpl = "?module=API&method=UsersManager.setUserAccess&userLogin=%s&access=%s&idSites=%s&format=PHP",
    $piwik_user_exists_tpl = "?module=API&method=UsersManager.userExists&userLogin=%s&format=PHP",
    $piwik_get_token_auth_tpl = "?module=API&method=UsersManager.getTokenAuth&userLogin=%s&md5Password=%s&format=PHP",
    $piwik_delete_user_tpl = "?module=API&method=UsersManager.deleteUser&userLogin=%s&format=PHP",

    $piwik_logger;

  public function __construct() {
    $logPath = sfConfig::get('sf_log_dir').'/piwik-adapter.log';
    $this->piwik_logger = new sfFileLogger(new sfEventDispatcher(), array('file' => $logPath));
  }

  public function ensureUser($user) {
    if(!$user->isSuperAdmin()) {
      $domain_name = Doctrine_Core::getTable('SmsDomain')->find($user->getProfile()->getDomainId())->getDomainName();
      $password = substr(md5(uniqid(rand())),0,16);
      $this->piwik_logger->info($domain_name.": piwik_ensure_user: ".$user->getUsername().'.'.$domain_name);

      $piwik_userlogin = $user->getUsername().'.'.$domain_name;
      $piwik_email = $user->getUsername()."@".$domain_name;
      if(strpos($user->getProfile()->getName(), 'pw:')!==false || strpos($user->getProfile()->getName(), 'pass:')!==false) {
        $piwik_alias = $user->getUsername();
      }
      else {
        $piwik_alias = $user->getProfile()->getName();
      }

      // check user
      $piwik_user_exists = sprintf($this->piwik_user_exists_tpl, $piwik_userlogin)."&token_auth=$this->token_auth";
      $user_exists = unserialize(file_get_contents($this->piwik_url.$piwik_user_exists));
      $this->piwik_logger->info("user exists: '".print_r($user_exists, 1)."'");
//      $this->piwik_logger->info($piwik_url.$piwik_user_exists);
      if($user_exists) {
        // update piwik users' password in order to retrieve token_auth below
        $piwik_update_user = sprintf($this->piwik_update_user_tpl, urlencode($piwik_userlogin), urlencode($password), urlencode($piwik_alias))."&token_auth=$this->token_auth";
        $result = unserialize(file_get_contents($this->piwik_url.$piwik_update_user));
        $this->piwik_logger->info($domain_name.": update user $piwik_userlogin: ".print_r($result, 1));
      }
      else {
        $piwik_add_user = sprintf($this->piwik_add_user_tpl, urlencode($piwik_userlogin), urlencode($password), urlencode($piwik_email), urlencode($piwik_alias))."&token_auth=$this->token_auth";
        $result = unserialize(file_get_contents($this->piwik_url.$piwik_add_user));
        $this->piwik_logger->info($domain_name.": adding user $piwik_userlogin: '".print_r($result, 1)."'");

        $piwik_set_user_access = sprintf($this->piwik_set_user_access_tpl, $piwik_userlogin, "view", Doctrine::getTable('SmsDomain')->getCurrent()->getDomainId())."&token_auth=$this->token_auth";
        $result = unserialize(file_get_contents($this->piwik_url.$piwik_set_user_access));
        $this->piwik_logger->info("set user access view: '".print_r($result, 1)."'");
      }
      // retreive and remember the token_auth at local DB
      $piwik_get_token_auth = sprintf($this->piwik_get_token_auth_tpl, $piwik_userlogin, md5($password))."&token_auth=$this->token_auth";
      $token_auth = unserialize(file_get_contents($this->piwik_url.$piwik_get_token_auth));
      $this->piwik_logger->info("token: '".print_r($token_auth, 1)."'");
    }
    else {
      $this->piwik_logger->info('token for superadmin set');
      $token_auth = $this->token_auth;
    }
    if($token_auth) {
      $user->getProfile()->setPiwikTokenAuth($token_auth)->save();
      return $token_auth;
    }
  }

  public function ensureDomain(SmsDomain $domain) {
    $this->piwik_logger->info($domain["domain_name"].": ensureDomain");
    if(strpos($domain["footer"], "<!-- Piwik -->")===false) {
      // add domain to piwik
      $sites = Doctrine_Core::getTable("CmsSite");
      $site = $sites->find($domain['domain_id']);
      $title = $site["title"];
      if(!$title) $title = $domain["domain_name"];
      $piwik_add_domain_with_id = sprintf($this->piwik_add_domain_with_id_tpl, $domain["domain_id"], urlencode($title), "http://".$domain["domain_name"])."&token_auth=$this->token_auth";
      $result = unserialize(file_get_contents($this->piwik_url.$piwik_add_domain_with_id));
      $this->piwik_logger->info($domain["domain_name"].": adding to piwik: ".print_r($result, 1));

      // get JS tracking code and add to the site's footer
      $piwik_get_jscode = sprintf($this->piwik_get_jscode_tpl, $domain["domain_id"])."&token_auth=$this->token_auth";
      $piwik_js_code = html_entity_decode(unserialize(file_get_contents($this->piwik_url.$piwik_get_jscode)));

      $domain["footer"].="\n".$piwik_js_code;
      $domain->save();
    }
  }

  public function renameDomainLogins($old_domain_name, $domain) {
    $this->piwik_logger->info($domain["domain_name"].": renameDomainLogins");

    $users = $domain->getUsersWithStatisticsPermission();
    // if there is users with statistics privilege
    if(count($users)) {
      // unsure piwik domain exists
      $this->ensureDomain($domain);
      foreach($users as $user) {
        $old_login = $user['username'].'.'.$old_domain_name;
        $this->deleteUserByLogin($old_login);
        $this->ensureUser($user);
      }
    }
  }

  public function deleteUser(sfGuardUser $user) {
    $login = $user['username'].'.'.Doctrine_Core::getTable('SmsDomain')->find($user->getProfile()->getDomainId())->getDomainName();
    $this->piwik_logger->info("piwik_delete_user: ".$login);

    $piwik_delete_user = sprintf($this->piwik_delete_user_tpl, urlencode($login))."&token_auth=$this->token_auth";
    $result = unserialize(file_get_contents($this->piwik_url.$piwik_delete_user));
    $this->piwik_logger->info("delete user $login: ".print_r($result, 1));
  }

  protected function deleteUserByLogin($login) {
    $this->piwik_logger->info("piwik_delete_user_by_login: ".$login);

    $piwik_delete_user = sprintf($this->piwik_delete_user_tpl, urlencode($login))."&token_auth=$this->token_auth";
    $result = unserialize(file_get_contents($this->piwik_url.$piwik_delete_user));
    $this->piwik_logger->info("delete user $login: ".print_r($result, 1));
  }

/*  protected function addUserByLogin($user) {
    $login = $user['username'].".".Doctrine_Core::getTable('SmsDomain')->find($user->getProfile->getDomainId())->getDomainName();

    $this->piwik_logger->info("piwik_add_user_by_login: ".$login);

    // prepare needed stuff
    $password = substr(md5(uniqid(rand())),0,16);
    // transform login to email
    $piwik_email = $login;
    $piwik_email[strpos($piwik_email, '.')] = '@';
    // alias
    if(strpos($user['profile']['name'], 'pw:')!==false || strpos($user['profile']['name'], 'pass:')!==false) {
      $piwik_alias = $user['username'];
    }
    else {
      $piwik_alias = $user['profile']['name'];
    }

    $piwik_add_user = sprintf($this->piwik_add_user_tpl, urlencode($login), urlencode($password), urlencode($piwik_email), urlencode($piwik_alias));
    $result = unserialize(file_get_contents($this->piwik_url.$this->piwik_add_user));
    $this->piwik_logger->info("adding user $login: ".print_r($result, 1));

    $piwik_set_user_access = sprintf($this->piwik_set_user_access_tpl, $login, "view", $user['profile']['domain_id']);
    $result = unserialize(file_get_contents($this->piwik_url.$this->piwik_set_user_access));
    $this->piwik_logger->info("set user access view: ".print_r($result, 1));

    // retreive and remember the token_auth at local DB
    $piwik_get_token_auth = sprintf($this->piwik_get_token_auth_tpl, $login, md5($password));
    $token_auth = unserialize(file_get_contents($this->piwik_url.$this->piwik_get_token_auth));
    $this->piwik_logger->info("token: ".print_r($token_auth, 1));
    if($token_auth) {
      $user['profile']['piwik_token_auth'] = $token_auth;
      $user->save();
      return $token_auth;
    }
  }
*/
}
