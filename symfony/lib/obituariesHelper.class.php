<?php
class obituariesHelper {
    static public function onCandleCreate($candle_event) {
        $params = $candle_event->getParameters();
        $candle = $params['object'];
        self::logCandle($candle);
        self::notifyCandleAdmin($candle);
        if ($candle['state'] == 0) {
            self::notifyCandleSubscribers($candle);
        }
    }

    static public function onCandlePublish($candle_event) {
        $params = $candle_event->getParameters();
        $candle = $params['object'];
        self::notifyCandleSubscribers($candle);
        self::notifyCandleAuthor($candle);
    }

    static protected function logCandle($candle) {
        // emulate SQL query here and log it
        $query = sprintf("INSERT plg_obituary_candle SET obituary_id='%d', domain_id='%d', name='%s', email='%s', ip='%s', thoughts='%s', timestamp='%s', state='%s'",
                         $candle['obituary_id'],
                         $candle['domain_id'],
                         $candle['name'],
                         $candle['email'],
                         $candle['ip'],
                         $candle['thoughts'],
                         $candle['created_at'],
                         $candle['state']);
        file_put_contents('../backup/candles.sql', $query . "\n", FILE_APPEND);
    }

    static protected function notifyCandleAdmin($candle) {
        $domain_name = Doctrine_Core::getTable('SmsDomain')->getCurrent()->getDomainName();
        $mail_to = Doctrine_Core::getTable('SmsDomain')->getCurrent()->getEmail();
        $obituary = Doctrine_Core::getTable('PlgObituary')->createQuery()
                ->andWhere('obituary_id = ?', $candle->getObituaryId())
                ->fetchOne();

        $text = "Obituary: {$obituary->getName()}\nName: {$candle->getName()}\nEmail: {$candle->getEmail()}\nThoughts: {$candle->getThoughts()}\nIP: {$candle->getIp()}\n\n";
        $obit_view_page = Doctrine_Core::getTable('CmsPage')->getObituaryViewPage();
        if ($candle->getState() > 0) {
            $text .= "A candle submitted to your site is awaiting for approval. If you would like to publish this candle, please click on the following link:\n";
            $text .= "http://$domain_name/$obit_view_page/{$obituary->getObituarySlug()}/{$candle->getObituaryId()}/publish/{$candle->getHash()}\n";
            $text .= "Note: If you do not want to publish this candle, you do not need to take any additional actions. By not clicking this link the candle will remain unpublished.";
        }
        else {
            $text .= "This candle has been published. To view it, please click on the following link:\n";
            $text .= "http://$domain_name/$obit_view_page/{$obituary->getObituarySlug()}/{$candle->getObituaryId()}?candle=all";
        }
        $subject = "New candle/message: {$obituary->getName()}";

        $message = sfContext::getInstance()->getMailer()->compose(
            'noreply@twintierstech.net',
            $mail_to,
            $subject,
            $text);
        sfContext::getInstance()->getMailer()->send($message);
    }

    static protected function notifyCandleSubscribers(PlgObituaryCandle $candle) {
        $subscribers = Doctrine_Core::getTable('PlgObituarySubscribe')->createQuery()->andWhere('obituary_id = ?', array($candle->getObituaryId()))->execute();
        /*								                    ->where('domain_id = ? AND obituary_id = ?', array(
                                                        Doctrine_Core::getTable('SmsDomain')->getCurrent()->getDomainId(),
                                                        $candle->getObituaryId()
                                                      ) )->execute();*/
        if (count($subscribers)) {
            /*      $obituary = Doctrine_Core::getTable('PlgObituary')->createQuery()
                                              ->andWhere('obituary_id = ?', $candle->getObituaryId())
                                              ->execute();
            */
            $obituary = $candle->getObituary();
            $mail_to = array();
            $replacements = array();
            $subject = "New obituary candle/message for {$obituary->getName()}";
            $text = "From: {$candle->getName()}\n\n{$candle->getThoughts()}\n\n----------\nIf you wish to unsubscribe from future email notifications for {$obituary->getName()}, please click the following link: http://" . Doctrine_Core::getTable('SmsDomain')->getCurrent()->getDomainName() . "/subscribe.php?action=unsubscribe&id={hash}";

            foreach ($subscribers as $subscriber) {
                if (!$subscriber['hash']) {
                    $subscriber['hash'] = substr(md5(uniqid("")), 0, 16);
                    $subscriber->save();
                }
                $mail_to[$subscriber['email']] = $subscriber['name'];
                $replacements[$subscriber['email']] = array('{hash}' => $subscriber['hash']);
            }

            $message = sfContext::getInstance()->getMailer()->compose(
                'noreply@twintierstech.net',
                $mail_to,
                $subject,
                $text);

            $decorator = new Swift_Plugins_DecoratorPlugin($replacements);
            $mailer = sfContext::getInstance()->getMailer();
            $mailer->registerPlugin($decorator);
            $mailer->batchSend($message);
        }
    }

    static public function onObituaryUpdatesSubscribe(PlgObituarySubscribe $obituaryUpdatesSubscribe) {
        $obituary = Doctrine_Core::getTable("PlgObituary")->findOneByObituaryId($obituaryUpdatesSubscribe->getObituaryId());
        $subject = "Subscribe for obituary updates: " . $obituary->getName();
        $text = "This email address has requested to receive new candles to the obituary for {$obituary->getName()}. To complete the subscription and receive them, please click the following link: http://" . Doctrine_Core::getTable("SmsDomain")->getCurrent()->getDomainName() . "/subscribe.php?action=confirm&id={$obituaryUpdatesSubscribe->getHash()}\n\nIf you did not initiate this request, please ignore this email and you will not be subscribed.";

        $message = sfContext::getInstance()->getMailer()->compose(
            'noreply@twintierstech.net',
            $obituaryUpdatesSubscribe->getEmail(),
            $subject,
            $text);

        sfContext::getInstance()->getMailer()->send($message);
    }

    static protected function notifyCandleAuthor($candle) {
        if($candle['email']) {
            $obituary = Doctrine_Core::getTable("PlgObituary")->findOneByObituaryId($candle['obituary_id']);
            $subject = "Published obituary candle";
            $text = "Hello, ".$candle['name']."! Your candle for ".$obituary->getName()." has been published. If you would like to view this candle, please click on the following link:\n";
            $text .= "http://" . Doctrine_Core::getTable("SmsDomain")->findOneByDomainId($obituary->getDomainId())->getDomainName() ."/online-obituary/".$obituary->getObituarySlug()."/".$obituary->getObituaryId()."/messages";

            $message = sfContext::getInstance()->getMailer()->compose(
                'noreply@twintierstech.net',
                $candle['email'],
                $subject,
                $text);

            sfContext::getInstance()->getMailer()->send($message);
        }
    }

    static public function onObituaryCreate($event) {
        $parameters = $event->getParameters();
        $obituary = $parameters['object'];
        $domain_name = Doctrine_Core::getTable('SmsDomain')->getCurrent()->getDomainName();
        $obituary_view_page_name = Doctrine_Core::getTable('CmsPage')->createQuery()->andWhere("page_type=4")->fetchOne()->getInternalName();

        $text_tpl = "An obituary for %s (%s - %s) has been published. To view this obituary click on the following link: http://$domain_name/$obituary_view_page_name/%s/%d\n\n----------\nIf you wish to unsubscribe from future email notifications for new obituaries, please click the following link: http://{$domain_name}/subscribe.php?action=unobits&id=%s";
        if ($obituary->getBirthDate()) {
            $birthdate = date("F j, Y", strtotime($obituary->getBirthDate()));
        } else {
            $birthdate = "N/A";
        }
        if ($obituary->getDeathDate()) {
            $deathdate = date("F j, Y", strtotime($obituary->getDeathDate()));
        } else {
            $deathdate = "N/A";
        }

        // fetch recipients
        $subscribers = Doctrine_Core::getTable('PlgSubscribe')->createQuery()->select("id, email, hash")->andWhere("enabled=1")->fetchArray();

        foreach ($subscribers as $subscriber) {
            if (self::if_contain_keywords($obituary, $subscriber["id"])) {
                $text = sprintf($text_tpl, $obituary->getName(), $birthdate, $deathdate, $obituary->getObituarySlug(), $obituary->getObituaryId(), $subscriber["hash"]);

                $message = sfContext::getInstance()->getMailer()->compose(
                    'noreply@twintierstech.net',
                    $subscriber["email"],
                    "New obituary published",
                    $text);

                sfContext::getInstance()->getMailer()->send($message);
            }
        }

        $obituary_config = $obituary->getConfig();
        if (!empty($obituary_config['facebook_user_access_token'])) {
            $facebook_message = "The online obituary for ".$obituary->getName()." is available to view and leave online condolences. http://$domain_name/$obituary_view_page_name/".$obituary->getObituarySlug()."/".$obituary->getObituaryId();
            self::publishMessageOnFacebookWall($obituary_config['facebook_user_access_token'], @$obituary_config['facebook_page'], $facebook_message);
        }
    }

    static protected function if_contain_keywords($obituary, $subscribe_id) {
        $keywords = Doctrine_Core::getTable('PlgKeyword')->createQuery("k")->select("k.keyword")->innerJoin("k.SubscribeToKeyword sk")->andWhere("sk.subscribe_id=$subscribe_id")->fetchArray();

        if (!count($keywords)) {
            return true;
        } else {
            foreach ($keywords as $keyword) {
                $search_fields = array(
                    $obituary->getHomePlace(),
                    $obituary->getBirthPlace(),
                    $obituary->getServicePlace(),
                    $obituary->getVisitationPlace(),
                    $obituary->getFinalDisposition(),
                    $obituary->getObitText()
                );
                if (self::find_keyword($search_fields, $keyword['keyword'])) {
                    return true;
                }
            }
            return false;
        }
    }

    static protected function find_keyword($search_fields, $keyword) {
        foreach($search_fields as $search_field) {
            if(stripos($search_field, $keyword)) {
                return true;
            }
        }
        return false;
    }

    static public function publishMessageOnFacebookWall($facebookUserAccessToken, $facebookPage = null, $message) {
        require_once dirname(__FILE__).'/../../util/facebook/facebook.php';

        Facebook::$CURL_OPTS[CURLOPT_SSL_VERIFYPEER] = false;

        $facebook = new Facebook(array('appId' => sfConfig::get('app_facebook_app_id'), 'secret' => sfConfig::get('app_facebook_app_secret')));

        $parameters = array('message' => $message);

        if(!empty($facebookPage)) {
            $postPage = $facebookPage;

            try {
                $pageInfo = $facebook->api($facebookPage, 'get');
            } catch(Exception $e) {
                self::logFacebookError($e);
            }

            if(!empty($pageInfo["id"])) {
                try {
                    $accounts =  $facebook->api("me/accounts", 'get', array('access_token' => $facebookUserAccessToken));
                    if(count($accounts['data'])) {
                        foreach($accounts["data"] as $account) {
                            if($account["id"] == $pageInfo["id"]) {
                                $parameters["access_token"] = $account["access_token"];
                                break;
                            }
                        }
                    }
                } catch(Exception $e) {
                    self::logFacebookError($e);
                }
            }
        } else {
            $postPage = 'me';
            $parameters["access_token"] = $facebookUserAccessToken;
        }

        try {
           $facebook->api($postPage.'/feed', 'post', $parameters);
        } catch(Exception $e) {
            self::logFacebookError($e);
        }
    }

    static protected function logFacebookError(Exception $e) {
        $logger = new sfFileLogger(new sfEventDispatcher(), array('file' => sfConfig::get('sf_log_dir').'/facebook.log'));;
        $logger->log("Domain Id: ".$_SESSION['domain_id']."   Message: ".$e->getMessage());
    }
}
