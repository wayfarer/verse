<?php

class CmsPageTable extends Doctrine_Table
{
  public function getScrapbookPage() {
    return $this->createQuery()
                ->andWhere('page_type = 7')->fetchOne();
  }

  public function getObituaryViewPage() {
    return $this->createQuery()
                ->andWhere('page_type = 4')->fetchOne();
  }

  /**
   * Creates a query, adding the site criteria automatically
   *
   * @return Doctrine_Query
   * @see Doctrine_Table::createQuery()
   */
  public function createQuery($alias = '')
  {
    $query = parent::createQuery($alias);
    $query->where('domain_id = ?', Doctrine::getTable('SmsDomain')->getCurrent()->getDomainId());

    return $query;
  }
}