<?php


class PlgBlockIpTable extends Doctrine_Table {
    
    public static function getInstance() {
        return Doctrine_Core::getTable('PlgBlockIp');
    }

    public function retrieveBackendBlockIpList(Doctrine_Query $query) {
        $alias = $query->getRootAlias();
        $query->select('*, domain_name as domain_name');
        $query->leftJoin($alias . '.Domain');
        if (!sfContext::getInstance()->getUser()->isSuperAdmin()) {
            $query->andWhere('domain_id = ?', Doctrine::getTable('SmsDomain')->getCurrent()->getDomainId());
        }
        return $query;
    }
}