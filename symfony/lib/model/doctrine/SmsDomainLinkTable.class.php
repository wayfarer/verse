<?php


class SmsDomainLinkTable extends Doctrine_Table
{
    
    public static function getInstance()
    {
        return Doctrine_Core::getTable('SmsDomainLink');
    }

    public function getGroupDomainIds($group_id = null) {
        if(!is_null($group_id)) {
            $domain_ids = $this->createQuery()->select('domain_id')->where('group_id = ?', $group_id)->fetchArray();
            $ids = array();
            foreach($domain_ids as $domain_id) {
                $ids[] = $domain_id['domain_id'];
            }
            return $ids;
        } else {
            return array();
        }
    }

    public function setGroupDomainIds($group_id = null, $domain_ids = array()) {
        if(!is_null($group_id)) {
            if(count($domain_ids)) {
                $domain_ids_old = $this->getGroupDomainIds($group_id);
                foreach($domain_ids as $domain_id) {
                    if(!in_array($domain_id, $domain_ids_old)) {
                        $sms_domain_link = new SmsDomainLink();
                        $sms_domain_link->group_id = $group_id;
                        $sms_domain_link->domain_id = $domain_id;
                        $sms_domain_link->save();
                    }
                }
                foreach($domain_ids_old as $domain_id_old) {
                    if(!in_array($domain_id_old, $domain_ids)) {
                        $this->createQuery()->delete()->where('group_id = ? AND domain_id = ?', array($group_id, $domain_id_old))->execute();
                    }
                }
            } else {
                $this->createQuery()->delete()->where('group_id = ?', $group_id)->execute();
            }
        }
    }
}