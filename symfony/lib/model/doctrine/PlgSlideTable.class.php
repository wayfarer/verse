<?php

class PlgSlideTable extends Doctrine_Table
{
  /**
   * Creates a query, adding the site criteria automatically
   *
   * @return Doctrine_Query
   * @see Doctrine_Table::createQuery()
   */
  public function createQuery($alias = '')
  {
    $query = parent::createQuery($alias);
    $query->where('domain_id = ?', Doctrine::getTable('SmsDomain')->getCurrent()->getDomainId());

    return $query;
  }

  public function deleteObituarySlides($obituary_id) {
      $slides = Doctrine_Core::getTable('PlgSlide')->createQuery()->andWhere('obituary_id = ?', $obituary_id)->execute()->getData();
      foreach($slides as $slide) {
          $slide->delete();
      }
  }
}