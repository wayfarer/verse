<?php

class SmsDomainTable extends Doctrine_Table
{
  static protected $domainModes = array(
    0 => 'production',
    1 => 'under construction',
    2 => 'removed'
  );

  static protected $servers = array(
    '' => '',
    1 => 'ws1',
    2 => 'ws2'
  );

  /**
   * Holds the current site
   * @var Site
   */
  protected $current;

  /**
   * @static
   * @return SmsDomainTable
   */
  public static function getInstance()
  {
    return Doctrine_Core::getTable('SmsDomain');
  }

  /**
   * Retrieve a Site by its domain
   *
   * @param string $domain
   * @return Site or false if no site is found
   */
  public function retrieveByDomain($domain)
  {
    return $this->createQuery()->where('domain_name = ?', $domain)->fetchOne();
  }

  /**
   * Retrieve the main site
   *
   * @return Site or false if no site is found
   */
  public function retrieveMain()
  {
    return $this->createQuery()->where('domain_id = 1')->fetchOne();
//    return 1;
  }

  /**
   * Sets the current site
   *
   * @param Site $site
   */
  public function setCurrent(SmsDomain $site)
  {
    $this->current = $site;
  }

  /**
   * Gets the current site
   *
   * @return SmsDomain
   */
  public function getCurrent()
  {
    if(!$this->current) {
      $this->setCurrent($this->retrieveMain());
    }
    return $this->current;
  }

  static public function getDomainModes() {
    return self::$domainModes;
  }

  static public function getServers() {
    return self::$servers;
  }
}