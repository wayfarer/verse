<?php

/**
 * SmsDomainLinkGroup
 * 
 * This class has been auto-generated by the Doctrine ORM Framework
 * 
 * @package    verse3
 * @subpackage model
 * @author     Vladimir Droznik <vladimir.droznik@gmail.com>
 * @version    SVN: $Id: Builder.php 7490 2010-03-29 19:53:27Z jwage $
 */
class SmsDomainLinkGroup extends BaseSmsDomainLinkGroup {

    public function getGroupNameForSelect() {
        return $this->getGroupName();
    }

    public function getDomainsInAnotherGroupsWithSharedProducts($domain_ids = array()) {
        $domains_in_another_groups = array();
        if (count($domain_ids)) {
            $domains = Doctrine_Core::getTable('SmsDomainLinkGroup')->createQuery('g')
                                                                ->select('g.group_name, l.domain_id, d.domain_name')
                                                                ->innerJoin('g.DomainLink l')
                                                                ->innerJoin('l.Domain d')
                                                                ->where('group_id != ? AND is_shared_products = 1', $this->getId())
                                                                ->andWhereIn('l.domain_id', $domain_ids)
                                                                ->fetchArray();
            foreach ($domains as $domain) {
                $domains_in_another_groups[] = array(
                    'domain_name' => $domain['DomainLink'][0]['Domain']['domain_name'],
                    'group_name' => $domain['group_name']
                );
            }
        }
        return $domains_in_another_groups;
    }
}