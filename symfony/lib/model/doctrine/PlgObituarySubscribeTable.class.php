<?php

class PlgObituarySubscribeTable extends Doctrine_Table
{

  /**
   * Creates a query, adding the site and obituary criteria automatically
   *
   * @return Doctrine_Query
   * @see Doctrine_Table::createQuery()
   */
  public function createQuery($alias = '') {
    $query = parent::createQuery($alias);
    $query->where('domain_id = ?', Doctrine::getTable('SmsDomain')->getCurrent()->getDomainId());

    //$query->andWhere('obituary_id = ?', Doctrine::getTable('PlgObituary')->getCurrent()->getObituaryId());

    return $query;
  }

  public function retrieveBackendSubscribeList(Doctrine_Query $query) {
    $alias = $query->getRootAlias();
    $query->select('*, CONCAT_WS(" ", first_name, middle_name, last_name) as obituary_name');
    $query->leftJoin($alias . '.Obituary');

    return $query;
  }
}