<?php


class PlgObituaryImageTable extends Doctrine_Table
{

    public static function getInstance()
    {
        return Doctrine_Core::getTable('PlgObituaryImage');
    }

    static protected $imageStates = array(
        0 => 'published',
        1 => 'in moderation',
        2 => 'hidden'
    );

    static public function getImageStates()
    {
        return self::$imageStates;
    }

    /**
     * Creates a query, adding the site criteria automatically
     *
     * @return Doctrine_Query
     * @see Doctrine_Table::createQuery()
     */
    public function createQuery($alias = '')
    {
        $query = parent::createQuery($alias);
        $cur_domain = SmsDomainTable::getInstance()->getCurrent();
        $linked_domain_ids = $cur_domain->getSharedObitDomainIds();
        if(count($linked_domain_ids)) {
            $query->andWhereIn('domain_id', $linked_domain_ids);
        }
        else {
            $query->andWhere('domain_id = ?', $cur_domain->getDomainId());
        }

        return $query;
    }

    public function retrieveBackendImageList(Doctrine_Query $query) {
        $alias = $query->getRootAlias();
        $query->select('*, CONCAT_WS(" ", first_name, middle_name, last_name) as name');
        $query->leftJoin($alias . '.Obituary');

        return $query;
    }
}