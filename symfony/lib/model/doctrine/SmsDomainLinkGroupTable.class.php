<?php


class SmsDomainLinkGroupTable extends Doctrine_Table {
    
    public static function getInstance() {
        return Doctrine_Core::getTable('SmsDomainLinkGroup');
    }

    static protected $sharedStates = array(
        '0' => 'no',
        '1' => 'yes'
    );

    public static function getSharedStates() {
        return self::$sharedStates;
    }
}