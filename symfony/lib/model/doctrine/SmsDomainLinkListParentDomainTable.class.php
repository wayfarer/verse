<?php


class SmsDomainLinkListParentDomainTable extends Doctrine_Table
{
    
    public static function getInstance()
    {
        return Doctrine_Core::getTable('SmsDomainLinkListParentDomain');
    }

    public function getGroupListDomainIds($group_id = null) {
        $ids = array();

        if(!is_null($group_id)) {
            $domain_ids = $this->createQuery()->select('domain_id')->where('group_id = ?', $group_id)->fetchArray();
            foreach($domain_ids as $domain_id) {
                $ids[] = $domain_id['domain_id'];
            }

        }

        return $ids;
    }

    public function setGroupListDomainIds($group_id = null, $domain_ids = array()) {
        if (!is_null($group_id)) {
            if (count($domain_ids)) {
                $domain_ids_old = $this->getGroupListDomainIds($group_id);
                foreach ($domain_ids as $domain_id) {
                    if (!in_array($domain_id, $domain_ids_old)) {
                        $sms_domain_link_list_domain = new SmsDomainLinkListParentDomain();
                        $sms_domain_link_list_domain->group_id = $group_id;
                        $sms_domain_link_list_domain->domain_id = $domain_id;
                        $sms_domain_link_list_domain->save();
                    }
                }
                foreach ($domain_ids_old as $domain_id_old) {
                    if (!in_array($domain_id_old, $domain_ids)) {
                        $this->createQuery()->delete()->where('group_id = ? AND domain_id = ?', array($group_id, $domain_id_old))->execute();
                    }
                }
            } else {
                $this->createQuery()->delete()->where('group_id = ?', $group_id)->execute();
            }
        }
    }
}