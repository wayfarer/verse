<?php

class PlgProductCategoryTable extends Doctrine_Table
{
  /**
   * Creates a query, adding the site criteria automatically
   *
   * @return Doctrine_Query
   * @see Doctrine_Table::createQuery()
   */
  public function createQuery($alias = '')
  {
    $query = parent::createQuery($alias);
    if(strpos(sfContext::getInstance()->getRouting()->getCurrentRouteName(), "domain_product") !== false && count($domain_ids = Doctrine::getTable('SmsDomain')->getCurrent()->getSharedProductDomainIds())) {
        // change category filter for domain_product module
        $query->where('domain_id != ?', Doctrine::getTable('SmsDomain')->getCurrent()->getDomainId());
        $query->andWhereIn('domain_id', $domain_ids);
    } else {
        $query->where('domain_id = ?', Doctrine::getTable('SmsDomain')->getCurrent()->getDomainId());
    }
    
    return $query;
  }
}