<?php

/**
 * PlgSlide
 *
 * This class has been auto-generated by the Doctrine ORM Framework
 *
 * @package    verse3
 * @subpackage model
 * @author     Vladimir Droznik
 * @version    SVN: $Id: Builder.php 6820 2009-11-30 17:27:49Z jwage $
 */
class PlgSlide extends BasePlgSlide
{
  public function setTableDefinition() {
    parent::setTableDefinition();

    // mark domain_id as primary column for Doctrine to use it at where clause for update operations
    // we need it for security, so one domain's user wouldn't cross with others
// DONE: use next 2 lines of code instead all below, when doctrine bug fixed
    $this->setColumnOptions('domain_id', array('primary' => true));
    // the same for obituary_id
    $this->setColumnOptions('obituary_id', array('primary' => true));
/*    $this->hasColumn('domain_id', 'integer', 4, array(
             'fixed' => 0,
             'type' => 'integer',
             'unsigned' => true,
             'primary' => true,
             'default' => '0',
             'notnull' => true,
             'autoincrement' => false,
             'length' => '4',
           ));
    $this->hasColumn('obituary_id', 'integer', 8, array(
             'type' => 'integer',
             'fixed' => 0,
             'unsigned' => true,
             'primary' => true,
             'default' => '0',
             'notnull' => true,
             'autoincrement' => false,
             'length' => '8',
           ));
*/
//    $table = $this->getTable();
//    var_dump($table);
//    exit;
  }

  // returns first primary key instead last one
  public function getPrimaryKey() {
    $identifier = (array) $this->identifier();
    return reset($identifier);
  }

  /**
   * Fills in obituary_id field for new slide
   */
  public function construct() {
    parent::construct();
    if($this->isNew()) {
      if(Doctrine::getTable('PlgObituary')->getCurrent()) {
        $this->setObituaryId(Doctrine::getTable('PlgObituary')->getCurrent()->getObituaryId());
      }
      $this->setDomainId(Doctrine::getTable('SmsDomain')->getCurrent()->getDomainId());
    }
  }

  public function delete() {
    $full_imagepath = sfConfig::get('sf_web_dir').'/files/'.Doctrine::getTable('SmsDomain')->getCurrent()->getDomainName().
                        VerseUtil::leadingSlash($this->getImage());
    @unlink($full_imagepath);
    parent::delete();
  }

	/**
   * Automatically populates the domain_id field,
   * creates thumbnail of uploaded image
   *
   * @see sfDoctrineRecord::save()
   */
  public function save(Doctrine_Connection $conn = null)
  {
    // thumbnails are created just before showing in the list
/*	  if($this->isModified()) {
	  	$uploadDir = sfConfig::get('sf_web_dir').'/files/'.Doctrine::getTable('SmsDomain')->getCurrent()->getDomainName();
	  	$img_path = $uploadDir.VerseUtil::leadingSlash($this->getImage());

	  	$img = new sfImage($img_path);
      $pathinfo = pathinfo($img_path);
      $img->thumbnail(100, 100);
      $img->setQuality(50);

      $tnDir = $pathinfo['dirname'].'/'.sfConfig::get('app_thumbnails_dir');
      if(!file_exists($tnDir)) {
        mkdir($tnDir);
      }

      $img->saveAs($tnDir.'/'.$pathinfo['basename']);
	  }
*/
/*	  if (empty($this->domain_id)) {
      $this->setDomainId(Doctrine::getTable('SmsDomain')->getCurrent()->getDomainId());
    }
*/
/*    if (empty($this->obituary_id)) {
      $this->setObituaryId(Doctrine::getTable('PlgObituary')->getCurrent()->getObituaryId());
    }
*/
	  return parent::save($conn);
  }
}