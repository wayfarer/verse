<?php

class PlgObituaryCandleTable extends Doctrine_Table
{
  static protected $candlesStates = array(
    0 => 'published',
    1 => 'in moderation',
    2 => 'hidden'
  );

  static public function getCandlesStates()
  {
    return self::$candlesStates;
  }

  public function retrieveCandleList(Doctrine_Query $q) {
      $rootAlias = $q->getRootAlias();

      // re-create query to view only domain's obituaries, not from linked domains
      $q = parent::createQuery($rootAlias);
      $q->andWhere('domain_id = ?', SmsDomainTable::getInstance()->getCurrent()->getDomainId());

      $q->leftJoin($rootAlias.'.Obituary o');
      return $q;
  }

  /**
   * Creates a query, adding the site criteria automatically
   *
   * @return Doctrine_Query
   * @see Doctrine_Table::createQuery()
   */
  public function createQuery($alias = '') {
    $query = parent::createQuery($alias);
    $cur_domain = SmsDomainTable::getInstance()->getCurrent();
    $linked_domain_ids = $cur_domain->getSharedObitDomainIds();
    if(count($linked_domain_ids)) {
        $query->andWhereIn('domain_id', $linked_domain_ids);
//        $query->leftJoin('PlgObituaryCandle.Domain d');
//        $query->leftJoin('d.DomainLinks l');
//        $query->andWhere('l.group_id = ?', $cur_domain->getDomainLinks()->getFirst()->getGroupId());
    }
    else {
        $query->andWhere('domain_id = ?', $cur_domain->getDomainId());
    }

    return $query;
  }
}