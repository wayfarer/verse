<?php


class SmsDomainLinkGroupConfigTable extends Doctrine_Table
{
    
    public static function getInstance()
    {
        return Doctrine_Core::getTable('SmsDomainLinkGroupConfig');
    }

    public function getShareObitsFromParentOption($group_id = null) {
        $result = false;
        if(!is_null($group_id)) {
            $share_obits_from_parent = $this->createQuery()->select("value")->where('group_id = ? AND param="share_obits_from_parent"', $group_id)->fetchOne();
            if($share_obits_from_parent) {
                $result = $share_obits_from_parent->getValue();
            }
        }
        return $result;
    }

    public function setShareObitsFromParentOption($group_id = null, $value = "") {
        if(!is_null($group_id)) {
            if($value) {
                $share_obits_from_parent = $this->getShareObitsFromParentOption($group_id);
                if (!$share_obits_from_parent) {
                    $config_option = new SmsDomainLinkGroupConfig();
                    $config_option->setGroupId($group_id);
                    $config_option->setParam("share_obits_from_parent");
                    $config_option->setValue("1");
                    $config_option->save();
                }
            } else {
                $this->createQuery()->delete()->where('group_id = ? AND param="share_obits_from_parent"', $group_id)->execute();
            }
        }
    }
}