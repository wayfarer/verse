<?php

class PlgEventTable extends Doctrine_Table
{
  public function obituaryServiceExists($obituary_id) {
    return count($this->createQuery()->addWhere('obituary_id = ? AND event_type="1"', $obituary_id)->execute())>0;
  }

  public function obituaryVisitationExists($obituary_id) {
    return count($this->createQuery()->addWhere('obituary_id = ? AND event_type="2"', $obituary_id)->execute())>0;
  }

  /**
   * Creates a query, adding the site criteria automatically
   *
   * @return Doctrine_Query
   * @see Doctrine_Table::createQuery()
   */
  public function createQuery($alias = '')
  {
    $query = parent::createQuery($alias);
    $query->where('domain_id = ?', Doctrine::getTable('SmsDomain')->getCurrent()->getDomainId());

    return $query;
  }
}