<?php

class PlgObituaryConfigTable extends Doctrine_Table {
    /**
     * Creates a query, adding the site criteria automatically
     *
     * @return Doctrine_Query
     * @see Doctrine_Table::createQuery()
     */
    public function createQuery($alias = '') {
        $query = parent::createQuery($alias);
        $cur_domain = SmsDomainTable::getInstance()->getCurrent();
        $linked_domain_ids = $cur_domain->getSharedObitDomainIds();
        if(count($linked_domain_ids)) {
            $query->andWhereIn('domain_id', $linked_domain_ids);
        }
        else {
            $query->andWhere('domain_id = ?', $cur_domain->getDomainId());
        }
        return $query;
    }
}