<?php


class PlgBlockKeywordTable extends Doctrine_Table {
    
    public static function getInstance() {
        return Doctrine_Core::getTable('PlgBlockKeyword');
    }
    
    public function retrieveBackendBlockKeywordList(Doctrine_Query $query) {
        $alias = $query->getRootAlias();
        $query->select('*, IF(domain_id=0, "All", domain_name) as domain_name');
        $query->leftJoin($alias . '.Domain');
        if (!sfContext::getInstance()->getUser()->isSuperAdmin()) {
            $query->andWhere('domain_id = ?', Doctrine::getTable('SmsDomain')->getCurrent()->getDomainId());
        }
        return $query;
    }
}