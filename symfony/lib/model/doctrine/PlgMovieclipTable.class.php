<?php

class PlgMovieclipTable extends Doctrine_Table
{
  static protected $statusTexts = array(0=>'unknown', 1=>'ready', 2=>'converting', 3=>'invalid file');

  static public function getStatusTexts() {
    return self::$statusTexts;
  }

  /**
   * Creates a query, adding the site criteria automatically
   *
   * @return Doctrine_Query
   * @see Doctrine_Table::createQuery()
   */
  public function createQuery($alias = '')
  {
    $query = parent::createQuery($alias);
    $query->andWhere('domain_id = ?', Doctrine::getTable('SmsDomain')->getCurrent()->getDomainId());

    return $query;
  }
}