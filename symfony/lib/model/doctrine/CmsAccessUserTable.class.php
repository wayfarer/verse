<?php

class CmsAccessUserTable extends Doctrine_Table
{
  static protected $statuses = array('0'=>'unconfirmed', '1'=>'active', '2'=>'blocked', '3'=>'new');

  static public function getUserStatuses() {
    return self::$statuses;
  }

  /**
   * Creates a query, adding the site criteria automatically
   *
   * @return Doctrine_Query
   * @see Doctrine_Table::createQuery()
   */
  public function createQuery($alias = '')
  {
    $query = parent::createQuery($alias);
    $query->where('domain_id = ?', Doctrine::getTable('SmsDomain')->getCurrent()->getDomainId());

    return $query;
  }
}