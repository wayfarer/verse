<?php
// Connection Component Binding
Doctrine_Manager::getInstance()->bindComponent('PlgObituaryCandle', 'doctrine');

/**
 * BasePlgObituaryCandle
 * 
 * This class has been auto-generated by the Doctrine ORM Framework
 * 
 * @property integer $candle_id
 * @property string $candle_case_id
 * @property integer $domain_id
 * @property integer $obituary_id
 * @property string $name
 * @property string $email
 * @property string $ip
 * @property blob $thoughts
 * @property timestamp $timestamp
 * @property integer $state
 * @property string $hash
 * @property SmsDomain $Domain
 * @property PlgObituary $Obituary
 * 
 * @method integer           getCandleId()       Returns the current record's "candle_id" value
 * @method string            getCandleCaseId()   Returns the current record's "candle_case_id" value
 * @method integer           getDomainId()       Returns the current record's "domain_id" value
 * @method integer           getObituaryId()     Returns the current record's "obituary_id" value
 * @method string            getName()           Returns the current record's "name" value
 * @method string            getEmail()          Returns the current record's "email" value
 * @method string            getIp()             Returns the current record's "ip" value
 * @method blob              getThoughts()       Returns the current record's "thoughts" value
 * @method timestamp         getTimestamp()      Returns the current record's "timestamp" value
 * @method integer           getState()          Returns the current record's "state" value
 * @method string            getHash()           Returns the current record's "hash" value
 * @method SmsDomain         getDomain()         Returns the current record's "Domain" value
 * @method PlgObituary       getObituary()       Returns the current record's "Obituary" value
 * @method PlgObituaryCandle setCandleId()       Sets the current record's "candle_id" value
 * @method PlgObituaryCandle setCandleCaseId()   Sets the current record's "candle_case_id" value
 * @method PlgObituaryCandle setDomainId()       Sets the current record's "domain_id" value
 * @method PlgObituaryCandle setObituaryId()     Sets the current record's "obituary_id" value
 * @method PlgObituaryCandle setName()           Sets the current record's "name" value
 * @method PlgObituaryCandle setEmail()          Sets the current record's "email" value
 * @method PlgObituaryCandle setIp()             Sets the current record's "ip" value
 * @method PlgObituaryCandle setThoughts()       Sets the current record's "thoughts" value
 * @method PlgObituaryCandle setTimestamp()      Sets the current record's "timestamp" value
 * @method PlgObituaryCandle setState()          Sets the current record's "state" value
 * @method PlgObituaryCandle setHash()           Sets the current record's "hash" value
 * @method PlgObituaryCandle setDomain()         Sets the current record's "Domain" value
 * @method PlgObituaryCandle setObituary()       Sets the current record's "Obituary" value
 * 
 * @package    verse3
 * @subpackage model
 * @author     Vladimir Droznik <vladimir.droznik@gmail.com>
 * @version    SVN: $Id: Builder.php 7490 2010-03-29 19:53:27Z jwage $
 */
abstract class BasePlgObituaryCandle extends sfDoctrineRecord
{
    public function setTableDefinition()
    {
        $this->setTableName('plg_obituary_candle');
        $this->hasColumn('candle_id', 'integer', 8, array(
             'type' => 'integer',
             'fixed' => 0,
             'unsigned' => true,
             'primary' => true,
             'autoincrement' => true,
             'length' => 8,
             ));
        $this->hasColumn('candle_case_id', 'string', 16, array(
             'type' => 'string',
             'fixed' => 0,
             'unsigned' => false,
             'primary' => false,
             'notnull' => false,
             'autoincrement' => false,
             'length' => 16,
             ));
        $this->hasColumn('domain_id', 'integer', 4, array(
             'type' => 'integer',
             'fixed' => 0,
             'unsigned' => true,
             'primary' => false,
             'default' => '0',
             'notnull' => true,
             'autoincrement' => false,
             'length' => 4,
             ));
        $this->hasColumn('obituary_id', 'integer', 8, array(
             'type' => 'integer',
             'fixed' => 0,
             'unsigned' => true,
             'primary' => false,
             'default' => '0',
             'notnull' => true,
             'autoincrement' => false,
             'length' => 8,
             ));
        $this->hasColumn('name', 'string', 128, array(
             'type' => 'string',
             'fixed' => 0,
             'unsigned' => false,
             'primary' => false,
             'notnull' => false,
             'autoincrement' => false,
             'length' => 128,
             ));
        $this->hasColumn('email', 'string', 64, array(
             'type' => 'string',
             'fixed' => 0,
             'unsigned' => false,
             'primary' => false,
             'notnull' => false,
             'autoincrement' => false,
             'length' => 64,
             ));
        $this->hasColumn('ip', 'string', 16, array(
             'type' => 'string',
             'fixed' => 0,
             'unsigned' => false,
             'primary' => false,
             'notnull' => false,
             'autoincrement' => false,
             'length' => 16,
             ));
        $this->hasColumn('thoughts', 'blob', null, array(
             'type' => 'blob',
             'fixed' => 0,
             'unsigned' => false,
             'primary' => false,
             'notnull' => false,
             'autoincrement' => false,
             'length' => '',
             ));
        $this->hasColumn('timestamp', 'timestamp', 25, array(
             'type' => 'timestamp',
             'fixed' => 0,
             'unsigned' => false,
             'primary' => false,
             'default' => '0000-00-00 00:00:00',
             'notnull' => true,
             'autoincrement' => false,
             'length' => 25,
             ));
        $this->hasColumn('state', 'integer', 1, array(
             'type' => 'integer',
             'fixed' => 0,
             'unsigned' => true,
             'primary' => false,
             'default' => '0',
             'notnull' => true,
             'autoincrement' => false,
             'length' => 1,
             ));
        $this->hasColumn('hash', 'string', 16, array(
             'type' => 'string',
             'fixed' => 0,
             'unsigned' => false,
             'primary' => false,
             'notnull' => false,
             'autoincrement' => false,
             'length' => 16,
             ));
    }

    public function setUp()
    {
        parent::setUp();
        $this->hasOne('SmsDomain as Domain', array(
             'local' => 'domain_id',
             'foreign' => 'domain_id'));

        $this->hasOne('PlgObituary as Obituary', array(
             'local' => 'obituary_id',
             'foreign' => 'obituary_id'));

        $timestampable0 = new Doctrine_Template_Timestampable(array(
             ));
        $this->actAs($timestampable0);
    }
}