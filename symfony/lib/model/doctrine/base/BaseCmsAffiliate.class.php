<?php
// Connection Component Binding
Doctrine_Manager::getInstance()->bindComponent('CmsAffiliate', 'doctrine');

/**
 * BaseCmsAffiliate
 * 
 * This class has been auto-generated by the Doctrine ORM Framework
 * 
 * @property integer $affiliate_id
 * @property integer $domain_id
 * @property string $ref
 * @property string $title
 * @property integer $page_id
 * @property CmsPage $AffiliatePage
 * @property Doctrine_Collection $Orders
 * 
 * @method integer             getAffiliateId()   Returns the current record's "affiliate_id" value
 * @method integer             getDomainId()      Returns the current record's "domain_id" value
 * @method string              getRef()           Returns the current record's "ref" value
 * @method string              getTitle()         Returns the current record's "title" value
 * @method integer             getPageId()        Returns the current record's "page_id" value
 * @method CmsPage             getAffiliatePage() Returns the current record's "AffiliatePage" value
 * @method Doctrine_Collection getOrders()        Returns the current record's "Orders" collection
 * @method CmsAffiliate        setAffiliateId()   Sets the current record's "affiliate_id" value
 * @method CmsAffiliate        setDomainId()      Sets the current record's "domain_id" value
 * @method CmsAffiliate        setRef()           Sets the current record's "ref" value
 * @method CmsAffiliate        setTitle()         Sets the current record's "title" value
 * @method CmsAffiliate        setPageId()        Sets the current record's "page_id" value
 * @method CmsAffiliate        setAffiliatePage() Sets the current record's "AffiliatePage" value
 * @method CmsAffiliate        setOrders()        Sets the current record's "Orders" collection
 * 
 * @package    verse3
 * @subpackage model
 * @author     Vladimir Droznik <vladimir.droznik@gmail.com>
 * @version    SVN: $Id: Builder.php 7490 2010-03-29 19:53:27Z jwage $
 */
abstract class BaseCmsAffiliate extends sfDoctrineRecord
{
    public function setTableDefinition()
    {
        $this->setTableName('cms_affiliate');
        $this->hasColumn('affiliate_id', 'integer', 4, array(
             'type' => 'integer',
             'fixed' => 0,
             'unsigned' => true,
             'primary' => true,
             'autoincrement' => true,
             'length' => 4,
             ));
        $this->hasColumn('domain_id', 'integer', 4, array(
             'type' => 'integer',
             'fixed' => 0,
             'unsigned' => true,
             'primary' => false,
             'default' => '0',
             'notnull' => true,
             'autoincrement' => false,
             'length' => 4,
             ));
        $this->hasColumn('ref', 'string', 16, array(
             'type' => 'string',
             'fixed' => 0,
             'unsigned' => false,
             'primary' => false,
             'default' => '',
             'notnull' => true,
             'autoincrement' => false,
             'length' => 16,
             ));
        $this->hasColumn('title', 'string', 100, array(
             'type' => 'string',
             'fixed' => 0,
             'unsigned' => false,
             'primary' => false,
             'default' => '',
             'notnull' => true,
             'autoincrement' => false,
             'length' => 100,
             ));
        $this->hasColumn('page_id', 'integer', 8, array(
             'type' => 'integer',
             'fixed' => 0,
             'unsigned' => true,
             'primary' => false,
             'default' => '0',
             'notnull' => true,
             'autoincrement' => false,
             'length' => 8,
             ));
    }

    public function setUp()
    {
        parent::setUp();
        $this->hasOne('CmsPage as AffiliatePage', array(
             'local' => 'page_id',
             'foreign' => 'page_id'));

        $this->hasMany('PlgProductOrder as Orders', array(
             'local' => 'affiliate_id',
             'foreign' => 'affiliate_id'));
    }
}