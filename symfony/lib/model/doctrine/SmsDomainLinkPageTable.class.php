<?php


class SmsDomainLinkPageTable extends Doctrine_Table {
    
    public static function getInstance() {
        return Doctrine_Core::getTable('SmsDomainLinkPage');
    }

    public function getSharedPageIds($group_id = null) {
        if(!is_null($group_id)) {
            $shared_page_ids = $this->createQuery()->select('p.domain_id, page_id')->leftJoin('SmsDomainLinkPage.Page p')->where('group_id = ?', $group_id)->fetchArray();
            $ids = array();
            foreach($shared_page_ids as $shared_page_id) {
                $ids[$shared_page_id['Page']['domain_id']][] = $shared_page_id['Page']['page_id'];
            }
            return $ids;
        } else {
            return array();
        }
    }

    public function setSharedPageIds($group_id = null, $shared_page_ids = array()) {
        if(!is_null($group_id)) {
            if(count($shared_page_ids)) {
                $shared_page_ids_old = $this->getSharedPageIds($group_id);
                foreach($shared_page_ids as $domain_id => $page_ids) {
                    foreach($page_ids as $page_id) {
                        if(!isset($shared_page_ids_old[$domain_id]) || !in_array($page_id, $shared_page_ids_old[$domain_id])) {
                            $sms_domain_link_page = new SmsDomainLinkPage();
                            $sms_domain_link_page->group_id = $group_id;
                            $sms_domain_link_page->page_id = $page_id;
                            $sms_domain_link_page->save();
                        }
                    }
                }
                foreach($shared_page_ids_old as $domain_id_old => $page_ids_old) {
                    foreach($page_ids_old as $page_id_old) {
                        if(!isset($shared_page_ids[$domain_id_old]) || !in_array($page_id_old, $shared_page_ids[$domain_id_old])) {
                            $this->createQuery()->delete()->where('group_id = ? AND page_id = ?', array($group_id, $page_id_old))->execute();
                        }
                    }
                }
            } else {
                $this->createQuery()->delete()->where('group_id = ?', $group_id)->execute();
            }
        }
    }
}