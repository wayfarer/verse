<?php

class sfGuardPermissionTable extends PluginsfGuardPermissionTable
{
  /**
   * Creates a query, filtering permissions to current users' permission set
   *
   * @return Doctrine_Query
   * @see Doctrine_Table::createQuery()
   */
  public function createQuery($alias = '')
  {
    $user = sfContext::getInstance()->getUser();
    $query = parent::createQuery($alias);

    if(!$user->isAnonymous() && !$user->isSuperAdmin()) {
      $permission_set = array();
      foreach($user->getPermissions() as $permission) {
        $permission_set[] = $permission->getId();
      }
      $query->whereIn('id', $permission_set);
    }
    return $query;
  }
}