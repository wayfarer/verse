<?php

class sfGuardUserTable extends PluginsfGuardUserTable
{
/*  public function retrieveBackendUserList(Doctrine_Query $q) {
    $rootAlias = $q->getRootAlias();
    $q->leftJoin($rootAlias . '.Profile p');
    $q->andWhere('p.domain_id = ?', Doctrine::getTable('SmsDomain')->getCurrent()->getDomainId());
    return $q;
  }
*/
  public function findByPermission($domain, $permission_name) {
    $permission = Doctrine_Core::getTable('sfGuardPermission')->findOneByName($permission_name);
    $query = parent::createQuery('u')
              ->leftJoin('u.sfGuardUserPermission pm')
              ->leftJoin('u.Profile p')
              ->where('p.domain_id = ? AND pm.permission_id = ?', array($domain->getDomainId(), $permission->getId()));
    return $query->execute();
  }

  /**
   * Creates a query, adding the site criteria automatically
   *
   * @return Doctrine_Query
   * @see Doctrine_Table::createQuery()
   */
  public function createQuery($alias = '')
  {
    if(!$alias) $alias = 'sfgu';
    $query = parent::createQuery($alias);
    // DONE: determine why update function doesn't function without hack
//    if(sfContext::getInstance()->getActionName()!='update') {
    $query->leftJoin($alias . '.Profile p');
    $query->where('p.domain_id = ?', Doctrine::getTable('SmsDomain')->getCurrent()->getDomainId());
//    }
    return $query;
  }
}