<?php

class PlgObituaryTable extends Doctrine_Table {
    /**
     * Holds the current obituary
     */
    protected $current = null;

    static protected $candlesPolicies = array(
        'on' => 'on',
        'moderated' => 'moderated',
        'off' => 'off',
        'moderated_n_days' => 'moderated after N days',
        '' => 'default'
    );

    static protected $serviceTypes = array(
        0 => 'Private',
        1 => 'Funeral Service',
        2 => 'Mass',
        3 => 'Memorial Service',
        4 => 'Service',
        5 => 'Graveside Service',
        6 => 'Prayer Service',
        7 => 'Organization Service',
        8 => 'Shiva Service'
    );

    static protected $flowerPageTypes = array(
        1 => 'Standard Flowers Page',
        2 => 'FloristOne Integration'
    );

    static public function getCandlesPolicies($no_default = false) {
        if (!$no_default) {
            return self::$candlesPolicies;
        }
        else {
            $policies = self::$candlesPolicies;
            unset($policies['']);
            return $policies;
        }
    }

    static public function getServiceTypes() {
        return self::$serviceTypes;
    }

    static public function getFlowerPageTypes() {
        return self::$flowerPageTypes;
    }

    public function loadConfig() {
        $q = Doctrine::getTable('PlgObituaryConfig')->createQuery()
                ->select('param, value')
                ->where('obituary_id = 0 AND domain_id = ?', Doctrine::getTable('SmsDomain')->getCurrent()->getDomainId());

        $results = $q->execute();
        $config = array();
        foreach ($results as $result) {
            $config[$result['param']] = $result['value'];
        }
        if (isset($config['flowers_page_id']) && $config['flowers_page_id'] && $config['flowers_page_id'] != 'off') {
            $config['flowers'] = true;
            $config['flowers_page'] = (string)Doctrine::getTable('CmsPage')->find($config['flowers_page_id']);
        }
        else {
            $config['flowers_page'] = "off";
        }

        if (!count($results->getData())) {
            $config += array(
                'field_home_place_enabled' => 'on',
                'field_death_date_enabled' => 'on',
                'field_birth_place_enabled' => 'on',
                'field_birth_date_enabled' => 'on',
                'field_age_enabled' => 'on',
                'field_service_enabled' => 'on',
                'field_visitation_enabled' => 'on',
                'field_interment_enabled' => 'on');
        }

        return $config;
    }

    public function saveConfig($form_values) {
        // unnecessary config entries collection
        $remove_coll = new Doctrine_Collection('PlgObituaryConfig');

        foreach ($form_values as $key => $value) {
            // save flowers_page_id if flowers enabled and set it to 0 if disabled
            if ($key == "flowers_page_id") continue;
            if ($key == "flowers") {
                $key = "flowers_page_id";
                if ($value) {
                    $value = $form_values['flowers_page_id'];
                }
                else {
                    $value = 0;
                }
            }

            if (( preg_match('/field_.*_title/', $key) || $key == 'addthis_username' || $key == 'facebook_user' || $key == 'facebook_page' ) && (!$value || $value == "Use default" || $value == "Disable")) {
                // mark config entry for removing
                $config_entry = Doctrine_Core::getTable('PlgObituaryConfig')->createQuery()
                        ->where('domain_id = ? AND obituary_id = 0 AND param = ?', array(
                                                                                        Doctrine::getTable('SmsDomain')->getCurrent()->getDomainId(),
                                                                                        $key))->fetchOne();
                if ($config_entry) {
                    $remove_coll->add($config_entry);
                }
                continue;
            }

            if ($value) {
                $config_entry = new PlgObituaryConfig();
                $config_entry->domain_id = Doctrine::getTable('SmsDomain')->getCurrent()->getDomainId();
                $config_entry->obituary_id = 0;
                $config_entry->param = $key;
                $config_entry->value = $value ? $value : 'off';
                $config_entry->replace();
            }
            else {
                // mark config entry for removing
                $config_entry = Doctrine_Core::getTable('PlgObituaryConfig')->createQuery()
                        ->where('domain_id = ? AND obituary_id = 0 AND param = ?', array(
                                                                                        Doctrine::getTable('SmsDomain')->getCurrent()->getDomainId(),
                                                                                        $key))->fetchOne();
                if ($config_entry) {
                    $remove_coll->add($config_entry);
                }
            }
        }
        if ($remove_coll->count()) {
            // NOTE: generates query: DELETE FROM plg_obituary_config WHERE (domain_id = ? AND param = ?) - (1, field_interment_title)
            // without obituary_id, not good, but ok, we are not using per-obit fields naming
            $remove_coll->delete();
        }
    }

    public function retrieveObituaryList(Doctrine_Query $q) {
        $rootAlias = $q->getRootAlias();

        // re-create query to view only domain's obituaries, not from linked domains
        $q = parent::createQuery($rootAlias);
        $q->andWhere('domain_id = ?', SmsDomainTable::getInstance()->getCurrent()->getDomainId());

        $q->select("*, (SELECT COUNT(*) FROM PlgObituaryCandle oc1 WHERE oc1.state=0 AND oc1.obituary_id=$rootAlias.obituary_id) as active_candle_count, " .
                   "(SELECT COUNT(*) FROM PlgObituaryCandle oc2 WHERE oc2.state=1 AND oc2.obituary_id=$rootAlias.obituary_id) as pending_candle_count, " .
                   "(SELECT COUNT(*) FROM PlgObituaryCandle oc3 WHERE oc3.state=2 AND oc3.obituary_id=$rootAlias.obituary_id) as hidden_candle_count, " .
                   "(SELECT COUNT(*) FROM PlgObituarySubscribe WHERE obituary_id=$rootAlias.obituary_id) as subscribe_count, " .
                   "(SELECT COUNT(*) FROM PlgSlide WHERE obituary_id=$rootAlias.obituary_id) as scrapbook_count, " .
                   "(SELECT COUNT(*) FROM PlgObituaryImage oi1 WHERE oi1.state=0 AND oi1.obituary_id=$rootAlias.obituary_id) as active_photo_count, " .
                   "(SELECT COUNT(*) FROM PlgObituaryImage oi2 WHERE oi2.state=1 AND oi2.obituary_id=$rootAlias.obituary_id) as pending_photo_count, " .
                   "(SELECT COUNT(*) FROM PlgObituaryImage oi3 WHERE oi3.state=2 AND oi3.obituary_id=$rootAlias.obituary_id) as hidden_photo_count, " .
                   "(SELECT COUNT(*) FROM PlgObituaryMovie om1 WHERE om1.state=0 AND om1.obituary_id=$rootAlias.obituary_id) as active_movie_count, " .
                   "(SELECT COUNT(*) FROM PlgObituaryMovie om2 WHERE om2.state=1 AND om2.obituary_id=$rootAlias.obituary_id) as pending_movie_count, " .
                   "(SELECT COUNT(*) FROM PlgObituaryMovie om3 WHERE om3.state=2 AND om3.obituary_id=$rootAlias.obituary_id) as hidden_movie_count"
        );

        return $q;
    }

/*  static public function retrieveForAutocomplete($field, $query, $limit = 0) {
		if(!$limit) return $query;
  	$q = Doctrine_Query::create()
		    ->from('PlgObituary o')
		    ->select("o.$field")
		    ->where("o.$field LIKE ? AND o.domain_id=?", array("%$query%", Doctrine::getTable('SmsDomain')->getCurrent()->getDomainId()) )
		    ->limit($limit);

    $ret = $q->execute();
		$suggests = array();
    foreach($ret as $obituary) {
			$suggests[$obituary->get($field)] = $obituary->get($field);
		}
		return $suggests;
  }
*/
    /**
     *  retrieves obituary by its id taking in account domain_id (through createQuery)
     */
    public function retrieveByObituaryId($obituary_id) {
        return $this->createQuery()->andWhere('obituary_id = ?', $obituary_id)->fetchOne();
    }

    /**
     * Sets the current obituary
     */
    public function setCurrent(PlgObituary $obituary) {
        $this->current = $obituary;
    }

    /**
     * Gets the current obituary
     */
    public function getCurrent() {
        /*    if(!$this->current) {
              $this->current = new PlgObituary();
            }
        */
        return $this->current;
    }

    /**
     * Creates a query, adding the site criteria automatically
     *
     * @return Doctrine_Query
     * @see Doctrine_Table::createQuery()
     */
    public function createQuery($alias = '') {
        $query = parent::createQuery($alias);
        $cur_domain = SmsDomainTable::getInstance()->getCurrent();
        $linked_domain_ids = $cur_domain->getSharedObitDomainIds();
        if(count($linked_domain_ids)) {
            $query->andWhereIn('domain_id', $linked_domain_ids);
//            $query->leftJoin('PlgObituary.Domain d');
//            $query->leftJoin('d.DomainLinks l');
//            $query->andWhere('l.group_id = ?', $cur_domain->getDomainLinks()->getFirst()->getGroupId());
        }
        else {
            $query->andWhere('domain_id = ?', $cur_domain->getDomainId());
        }

        return $query;
    }

    public function removeFacebookConfig() {
        Doctrine::getTable('PlgObituaryConfig')->createQuery()->delete()->where('obituary_id = 0 AND domain_id = ? AND (param="facebook_user_access_token" OR param="facebook_page")', Doctrine::getTable('SmsDomain')->getCurrent()->getDomainId())->execute();
    }
}