<?php

class CmsSiteTable extends Doctrine_Table {

    public function retrieveBackendGoogleStats(Doctrine_Query $query) {
        $query->andWhere('domain_id = ?', SmsDomainTable::getInstance()->getCurrent()->getDomainId());
        return $query;
    }
}