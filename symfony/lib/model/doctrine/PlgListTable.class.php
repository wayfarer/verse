<?php

class PlgListTable extends Doctrine_Table
{

/*  static public function retrieveForAutocomplete($field, $query, $limit = 0) {
    if(!$limit) return $query;
    $q = Doctrine_Query::create()
        ->from('PlgList l')
        ->select('l.type')
        ->where('l.type LIKE ?', "%$query%")
        ->limit($limit);

    $ret = $q->execute();
    $suggests = array();
    foreach($ret as $list) {
      $suggests[$list->getType()] = $list->getType();
    }
    return $suggests;
  }
*/
	/**
   * Creates a query, adding the site criteria automatically
   *
   * @return Doctrine_Query
   * @see Doctrine_Table::createQuery()
   */
  public function createQuery($alias = '')
  {
    $query = parent::createQuery($alias);
    $query->where('domain_id = ?', Doctrine::getTable('SmsDomain')->getCurrent()->getDomainId());

    return $query;
  }
}
