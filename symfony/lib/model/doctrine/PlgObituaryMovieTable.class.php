<?php


class PlgObituaryMovieTable extends Doctrine_Table
{
    static protected $statusTexts = array(
        0 => 'unknown',
        1 => 'ready',
        2 => 'converting',
        3 => 'invalid file'
    );

    static public function getStatusTexts() {
        return self::$statusTexts;
    }

    public static function getInstance() {
        return Doctrine_Core::getTable('PlgObituaryMovie');
    }

    static protected $movieStates = array(
        0 => 'published',
        1 => 'in moderation',
        2 => 'hidden'
    );

    static public function getMovieStates() {
        return self::$movieStates;
    }


     /**
     * Creates a query, adding the site criteria automatically
     *
     * @return Doctrine_Query
     * @see Doctrine_Table::createQuery()
     */
    public function createQuery($alias = '') {
        $query = parent::createQuery($alias);
        $cur_domain = SmsDomainTable::getInstance()->getCurrent();
        $linked_domain_ids = $cur_domain->getSharedObitDomainIds();
        if(count($linked_domain_ids)) {
            $query->andWhereIn('domain_id', $linked_domain_ids);
        }
        else {
            $query->andWhere('domain_id = ?', $cur_domain->getDomainId());
        }

        return $query;
    }

    public function retrieveBackendMovieList(Doctrine_Query $query) {
        $alias = $query->getRootAlias();
        $query->select('*, CONCAT_WS(" ", first_name, middle_name, last_name) as name');
        $query->leftJoin($alias . '.Obituary');

        return $query;
    }
}