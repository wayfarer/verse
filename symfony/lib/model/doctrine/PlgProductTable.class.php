<?php

class PlgProductTable extends Doctrine_Table
{
  public function retrieveProductList(Doctrine_Query $q) {
    $rootAlias = $q->getRootAlias();

    $q->leftJoin($rootAlias . '.ProductCategory c');

    return $q;
  }

  public function retrieveBackendDomainGroupProductList(Doctrine_Query $query) {
      $domain_id = Doctrine::getTable('SmsDomain')->getCurrent()->getDomainId();
      $alias = $query->getRootAlias();
      $query->select('product_id, d.domain_name as domain_name, c.name AS category_name, name, price, image, IF(lp.product_id IS NOT NULL, 1, 0) AS product_enabled');
      $query->leftJoin($alias.'.DomainLinkProduct as lp WITH lp.domain_id='.$domain_id);
      $query->innerJoin($alias.'.Domain as d');
      $query->innerJoin($alias.'.ProductCategory as c');

      $domain_ids = Doctrine::getTable('SmsDomain')->getCurrent()->getSharedProductDomainIds();
      if(count($domain_ids)) {
          $query->where('domain_id IN ('.implode(", ", $domain_ids).')');
          $query->andWhere('domain_id != ?', $domain_id);
      }

      $query->andWhere('enabled = 1');

      return $query;
  }

  public function getDomainsForSelect($with_empty = false) {
      $domains_for_select = array();
      if($with_empty) {
          $domains_for_select[''] = '--- All ---';
      }
      $domain_ids = Doctrine::getTable('SmsDomain')->getCurrent()->getSharedProductDomainIds();
      if(count($domain_ids)) {
          $domains = Doctrine::getTable('SmsDomain')->createQuery()->select('domain_id, domain_name')->whereIn('domain_id', $domain_ids)->andWhere('domain_id != ?', Doctrine::getTable('SmsDomain')->getCurrent()->getDomainId())->orderBy('domain_name')->fetchArray();
      } else {
          $domains = Doctrine::getTable('SmsDomain')->createQuery()->select('domain_id, domain_name')->where('enabled=1')->orderBy('domain_name')->fetchArray();
      }
      foreach($domains as $domain) {
          $domains_for_select[$domain['domain_id']] = $domain['domain_name'];
      }
      return $domains_for_select;
  }

  public function loadConfig() {
    $q = Doctrine::getTable('PlgProductConfig')->createQuery()
      ->select('param, value')
      ->where('domain_id = ?', Doctrine::getTable('SmsDomain')->getCurrent()->getDomainId());

    $results = $q->execute();
    $config = array();
    foreach($results as $result) {
      $config[$result['param']] = $result['value'];
    }
    if(isset($config['return_page']) && $config['return_page']) {
      $config['return_page_id'] = Doctrine::getTable('CmsPage')->createQuery()
        ->select('page_id')->where('internal_name = ? AND domain_id = ?', array($config['return_page'], Doctrine::getTable('SmsDomain')->getCurrent()->getDomainId()))
        ->execute(array(), Doctrine::HYDRATE_SINGLE_SCALAR);
    }
    else {
      $config['return_page'] = 'N/A';
    }
    return $config;
  }

  public function saveConfig($form_values) {
    foreach($form_values as $key=>$value) {
      if($key=="return_page_id") {
        $key = "return_page";
        $value = (string)Doctrine::getTable('CmsPage')->find($value);
      }
      $config_entry = new PlgProductConfig();
      $config_entry->domain_id = Doctrine::getTable('SmsDomain')->getCurrent()->getDomainId();
      $config_entry->param = $key;
      $config_entry->value = $value;
      $config_entry->replace();
    }
  }


  /**
   * Creates a query, adding the site criteria automatically
   *
   * @return Doctrine_Query
   * @see Doctrine_Table::createQuery()
   */
  public function createQuery($alias = '')
  {
    $query = parent::createQuery($alias);

    if(strpos(sfContext::getInstance()->getRouting()->getCurrentRouteName(), "domain_product") !== false && count($domain_ids = Doctrine::getTable('SmsDomain')->getCurrent()->getSharedProductDomainIds())) {
        $query->whereIn('domain_id', $domain_ids);
    } else {
        $query->where('domain_id = ?', Doctrine::getTable('SmsDomain')->getCurrent()->getDomainId());
    }

    return $query;
  }
}