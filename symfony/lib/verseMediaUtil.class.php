<?php

class VerseMediaUtil {
    const EMPTY_CAPTION = 'Type caption here';

    static public function saveFiles($domain, $obituary, $files) {
        $obituary_id = $obituary->getObituaryId();
        $domain_name = $domain->getDomainName();
        $domain_id = $domain->getDomainId();
        foreach($files as $file) {
            $media_file = new verseMediaFile($file['path']);
            $media_file->save($obituary_id, $domain_name, $domain_id, 1, $file['caption']);
        }
    }

    static public function uploadFile($domain_name, $user, $files_tmp) {
        $uploadDir = 'files/'.$domain_name.'/tmp/';
        if (!file_exists($uploadDir)) {
            mkdir($uploadDir);
        }
        if (is_uploaded_file($files_tmp['media']['tmp_name'])) {
            $fileName = $files_tmp['media']['name'];
            $path = $uploadDir.$fileName;
            $file = new verseMediaFile($path);
            if (file_exists($path) || ($file->isMovie() && file_exists($uploadDir.$file->getName().'.jpg'))) {
                $file->rename();
            }
            move_uploaded_file($files_tmp['media']['tmp_name'], $file->getPath());

            if (!is_null($user)) {
                $file->removeSpacesFromName();
                $files = $user->getAttribute('files', array());
                $files[$file->getFullName()] = array(
                    'path' => $file->getPath(),
                    'caption' => ''
                );
                $user->getAttributeHolder()->remove('files');
                $user->setAttribute('files', $files);
            }

            return $file;
        }
        return null;
    }

    static public function getThumbnailAndCaptionForFiles($files) {
        $thumbnails_captions = array();
        foreach ($files as $name => $file) {
            $media_file = new verseMediaFile($file['path']);
            if ($media_file->isMovie()) {
                $thumbnail_path = $media_file->getDirectory().'/'.$media_file->getName().".jpg";
            } else {
                $thumbnail_path = $media_file->getDirectory().'/'.$media_file->getName().'_thumb.'.$media_file->getExtension();
            }
            $thumbnails_captions[$name] = array(
                'path' => $thumbnail_path,
                'caption' => $file['caption']
            );
        }
        return $thumbnails_captions;
    }

    static public function filesize2bytes($str) {
        $bytes = 0;
        $bytes_array = array(
            'B' => 1,
            'KB' => 1024,
            'K' => 1024,
            'MB' => 1024 * 1024,
            'M' => 1024 * 1024,
            'GB' => 1024 * 1024 * 1024,
            'G' => 1024 * 1024 * 1024,
            'TB' => 1024 * 1024 * 1024 * 1024,
            'T' => 1024 * 1024 * 1024 * 1024,
            'PB' => 1024 * 1024 * 1024 * 1024 * 1024,
            'P' => 1024 * 1024 * 1024 * 1024 * 1024,
        );
        $bytes = floatval($str);
        if (preg_match('#([KMGTP]?B|[KMGTP])$#si', $str, $matches) && !empty($bytes_array[$matches[1]])) {
            $bytes *= $bytes_array[$matches[1]];
        }
        $bytes = intval(round($bytes, 2));

        return $bytes;
    }
}