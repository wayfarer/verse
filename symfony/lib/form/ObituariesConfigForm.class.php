<?php

class ObituariesConfigForm extends BaseForm {
  public function configure() {
    $this->setWidgets(array(
      'candles_policy_form' => new sfWidgetFormSchema(array(
          'candles_policy' => new sfWidgetFormChoice(array(
              'choices' => PlgObituaryTable::getCandlesPolicies(true),
              'expanded' => true,
          )),
          'days_without_moderation' => new sfWidgetFormInputText(array('label' => false), array('size' => 2)),
      )),
      'candle_images' => new sfWidgetFormInputCheckbox(),
      'captcha' => new sfWidgetFormInputCheckbox(),
      'media' => new sfWidgetFormInputCheckbox(),
      'google_maps' => new sfWidgetFormInputCheckbox(),
      'flowers' => new sfWidgetFormInputCheckbox(),
      'flowers_page_type' => new sfWidgetFormChoice(array(
          'choices' => PlgObituaryTable::getFlowerPageTypes(),
          'expanded' => true
      )),
      'flowers_page_id' => new sfWidgetFormDoctrineChoice(array('model' => 'CmsPage', 'add_empty' => false)),
//      'light_candle' => new sfWidgetFormInputText(),

      'obituary_theme' => new sfWidgetFormChoice(array('choices'=>VerseUtil::getObituaryThemes())),
      'obituary_theme_image' => new sfWidgetFormChoice(array('choices'=>VerseUtil::getObituaryThemeImages())),
      'obituary_selected_theme_image' => new sfWidgetFormInputHidden(),
      'obituary_military_image' => new sfWidgetFormChoice(array('choices'=>VerseUtil::getObituaryMilitaryImages())),
      'obituary_selected_military_image' => new sfWidgetFormInputHidden(),
      'obituary_search_theme' => new sfWidgetFormChoice(array('choices'=>VerseUtil::getObituarySearchThemes())),
      'obituary_calendar_theme' => new sfWidgetFormChoice(array('choices'=>VerseUtil::getObituaryCalendarThemes())),
      'obituary_icons_theme' => new sfWidgetFormChoice(array('choices'=>VerseUtil::getObituaryIconsThemes())),

      'obituary_music' => new sfWidgetFormMusic(array('choices'=>VerseUtil::getMusicFiles())),

      'addthis_username' => new sfWidgetFormInputText(),

      'facebook_user_access_token' => new sfWidgetFormInputHidden(),
      'facebook_page' => new sfWidgetFormInputText(),

      'field_home_place_enabled' => new sfWidgetFormInputCheckbox(),
      'field_home_place_title' => new sfWidgetFormInputText(),
      'field_death_date_enabled' => new sfWidgetFormInputCheckbox(),
      'field_death_date_title' => new sfWidgetFormInputText(),
      'field_birth_place_enabled' => new sfWidgetFormInputCheckbox(),
      'field_birth_place_title' => new sfWidgetFormInputText(),
      'field_birth_date_enabled' => new sfWidgetFormInputCheckbox(),
      'field_birth_date_title' => new sfWidgetFormInputText(),
      'field_age_enabled' => new sfWidgetFormInputCheckbox(),
      'field_age_title' => new sfWidgetFormInputText(),
      'field_service_enabled' => new sfWidgetFormInputCheckbox(),
      'field_service_title' => new sfWidgetFormInputText(),
      'field_visitation_enabled' => new sfWidgetFormInputCheckbox(),
      'field_visitation_title' => new sfWidgetFormInputText(),
      'field_interment_enabled' => new sfWidgetFormInputCheckbox(),
      'field_interment_title' => new sfWidgetFormInputText(),
    ));
    $this->setValidators(array(
      'candles_policy_form' => new sfValidatorSchema(array(
          'candles_policy' => new sfValidatorChoice(array('choices'=>array_keys(PlgObituaryTable::getCandlesPolicies(true)))),
          'days_without_moderation' => new sfValidatorString()
      )),
      'candle_images' => new sfValidatorPass(),
      'captcha' => new sfValidatorPass(),
      'media' => new sfValidatorPass(),
      'google_maps' => new sfValidatorPass(),
      'flowers' => new sfValidatorPass(),
      'flowers_page_type' => new sfValidatorChoice(array('choices'=>array_keys(PlgObituaryTable::getFlowerPageTypes()))),
      'flowers_page_id' => new sfValidatorDoctrineChoice(array('model' => 'CmsPage', 'column' => 'page_id')),
//      'light_candle' => new sfValidatorString(array('required' => false)),

      'obituary_theme' => new sfValidatorChoice(array('choices'=>array_keys(VerseUtil::getObituaryThemes()))),
      'obituary_theme_image' => new sfValidatorChoice(array('choices'=>array_keys(VerseUtil::getAllObituaryThemeImages()))),
      'obituary_selected_theme_image' => new sfValidatorString(array('required' => false)),
      'obituary_military_image' => new sfValidatorChoice(array('choices'=>array_keys(VerseUtil::getAllObituaryMilitaryImages()))),
      'obituary_selected_military_image' => new sfValidatorString(array('required' => false)),
      'obituary_calendar_theme' => new sfValidatorChoice(array('choices'=>array_keys(VerseUtil::getObituaryCalendarThemes()))),
      'obituary_search_theme' => new sfValidatorChoice(array('choices'=>array_keys(VerseUtil::getObituarySearchThemes()))),
      'obituary_icons_theme' => new sfValidatorChoice(array('choices'=>array_keys(VerseUtil::getObituaryIconsThemes()))),

      'obituary_music' => new sfValidatorChoice(array('choices'=>array_keys(VerseUtil::getMusicFiles()))),

      'addthis_username' => new sfValidatorPass(),

      'facebook_user_access_token' => new sfValidatorString(array('required' => false)),
      'facebook_page' => new sfValidatorPass(),

      'field_home_place_enabled' => new sfValidatorPass(),
      'field_home_place_title' => new sfValidatorPass(),
      'field_death_date_enabled' => new sfValidatorPass(),
      'field_death_date_title' => new sfValidatorPass(),
      'field_birth_place_enabled' => new sfValidatorPass(),
      'field_birth_place_title' => new sfValidatorPass(),
      'field_birth_date_enabled' => new sfValidatorPass(),
      'field_birth_date_title' => new sfValidatorPass(),
      'field_age_enabled' => new sfValidatorPass(),
      'field_age_title' => new sfValidatorPass(),
      'field_service_enabled' => new sfValidatorPass(),
      'field_service_title' => new sfValidatorPass(),
      'field_visitation_enabled' => new sfValidatorPass(),
      'field_visitation_title' => new sfValidatorPass(),
      'field_interment_enabled' => new sfValidatorPass(),
      'field_interment_title' => new sfValidatorPass()
    ));

    $this->widgetSchema->setLabels(array(
      'addthis_username' => 'AddThis Username',
      'facebook_page' => 'Facebook Page',
      'candles_policy_form' => false,
      'google_maps' => 'Google Maps',
      'light_candle' => 'Light a candle text',
      'field_home_place_enabled' => 'Home',
      'field_death_date_enabled' => 'Date of Death',
      'field_birth_place_enabled' => 'Place of Birth',
      'field_birth_date_enabled' => 'Birthdate',
      'field_age_enabled' => 'Age',
      'field_service_enabled' => '<small>[Service type (use %s as placeholder)] Service</small>',
      'field_visitation_enabled' => 'Visitation',
      'field_interment_enabled' => 'Interment'
    ));

    $this->widgetSchema->setHelps(array(
      'field_service_title' => 'Use %s for service type word'
    ));

    $this->widgetSchema->setNameFormat('config[%s]');

//    $this->widgetSchema->setFormFormatterName('list');
  }

  public function getValues() {
      $values = parent::getValues();
      $values['candles_policy'] = $values['candles_policy_form']['candles_policy'];
      $values['days_without_moderation'] = $values['candles_policy_form']['days_without_moderation'];
      unset($values['candles_policy_form'], $values['obituary_selected_theme_image'], $values['obituary_selected_military_image']);

      return $values;
  }

  public function setDefaults($defaults) {
      $defaults['candles_policy_form'] = array();
      $defaults['candles_policy_form']['candles_policy'] = isset($defaults['candles_policy']) ? $defaults['candles_policy'] : 'off';
      $defaults['candles_policy_form']['days_without_moderation'] = isset($defaults['days_without_moderation']) ? $defaults['days_without_moderation'] : '0';
      $defaults['obituary_selected_theme_image'] = isset($defaults['obituary_theme_image']) ? $defaults['obituary_theme_image'] : 'none';
      $defaults['obituary_selected_military_image'] = isset($defaults['obituary_military_image']) ? $defaults['obituary_military_image'] : 'none';
      $defaults['flowers_page_type'] = isset($defaults['flowers_page_type']) ? $defaults['flowers_page_type'] : 1;
      unset($defaults['candles_policy'], $defaults['days_without_moderation']);

      parent::setDefaults($defaults);
  }

  protected function doBind($values) {
      unset($values['obituary_selected_theme_image'], $values['obituary_selected_military_image']);
      parent::doBind($values);
  }

  protected function doUpdateObject($values) {
      unset($values['obituary_selected_theme_image'], $values['obituary_selected_military_image']);
      parent::doUpdateObject($values);
  }
}