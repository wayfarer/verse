<?php

class ObituariesCandleForm extends sfFormSymfony {
  public function configure() {
    $obituary_config = Doctrine_Core::getTable('PlgObituary')->getCurrent()->getConfig();

    $this->widgetSchema['name'] = new sfWidgetFormInputText();
    $this->widgetSchema['email'] = new sfWidgetFormInputText();
    $this->widgetSchema['memories'] = new sfWidgetFormTextarea();

    if($obituary_config['candle_images'] && !$obituary_config['disable_candle_images'] && ($obituary_config['obituary_theme'] == 'nt_frame' || $obituary_config['obituary_theme'] == 'nt_without_frame')) {
        $icons = sfYaml::load('obits/icons/'.$obituary_config['obituary_icons_theme'].'/theme.yml');

        $this->widgetSchema['candle_image'] = new sfWidgetFormCandleImage(array('theme' => $obituary_config['obituary_icons_theme'], 'choices' => $icons['actions']['lightacandle']['candle_images']));
        $this->widgetSchema['candle_image']->setLabel('Select image for your message');
        $this->validatorSchema['candle_image'] = new sfValidatorChoice(array('choices'=>array_keys($icons['actions']['lightacandle']['candle_images'])));
    }

    if(!empty($obituary_config['captcha'])) {
/*
        $this->widgetSchema['captcha'] = new gyWidgetFormCaptcha(array('size' => 6, 'valid' => 2));
        $this->validatorSchema['captcha'] = new gyValidatorCaptcha();
*/
        $this->widgetSchema['captcha'] = new sfWidgetCaptchaGD();
        $this->validatorSchema['captcha'] = new sfCaptchaGDValidator();
        $this->widgetSchema->setLabels(array(
            'captcha' => 'Captcha (<span class="verse-obit-captcha-parenthesis">please enter all letters in uppercase</span>)'
        ));
    }

    $this->validatorSchema['name'] = new sfValidatorString();
    $this->validatorSchema['email'] = new sfValidatorEmail(array('required' => false));
    $this->validatorSchema['memories'] = new sfValidatorString(array('min_length'=>5));


    $this->widgetSchema->setNameFormat('candle[%s]');

    $this->widgetSchema->setFormFormatterName('div');
  }
}