<?php

/**
 * PlgObituaryCandle form.
 *
 * @package    verse3
 * @subpackage form
 * @author     Vladimir Droznik
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class PlgObituaryCandleForm extends BasePlgObituaryCandleForm
{
  public function configure() {
    // do not touch next fields
  	unset($this['ip'], $this['candle_case_id'], $this['hash'], $this['created_at'], $this['updated_at']);

    $this->widgetSchema['obituary_id']->addOption('order_by', array('last_name', 'asc'))->addOption('method', 'getNameForSelect')->addOption('add_empty', '--- Select Obituary ---');
    $this->setDefault('obituary_id', sfContext::getInstance()->getUser()->getAttribute('candle_obituary_id', ''));
    $this->validatorSchema['obituary_id'] = new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Obituary'), 'required' => true));


  	// candle states
    $this->widgetSchema['state'] = new sfWidgetFormChoice(array(
      'choices' => PlgObituaryCandleTable::getCandlesStates()
    ));
    $this->validatorSchema['state'] = new sfValidatorChoice(array(
      'choices' => array_keys(PlgObituaryCandleTable::getCandlesStates())
    ));
  }

  //
  public function updateTimestampColumn($value) {
    if(!$value) {
      $value = date("Y-m-d H:i");
    }
    return $value;
  }

  protected function doBind(array $values) {
    $values['domain_id'] = Doctrine::getTable('SmsDomain')->getCurrent()->getDomainId();
    parent::doBind($values);
  }
}
