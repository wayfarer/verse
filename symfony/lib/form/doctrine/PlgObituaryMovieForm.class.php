<?php

/**
 * PlgObituaryMovie form.
 *
 * @package    verse3
 * @subpackage form
 * @author     Vladimir Droznik <vladimir.droznik@gmail.com>
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class PlgObituaryMovieForm extends BasePlgObituaryMovieForm
{
  public function configure() {
      unset($this['domain_id'], $this['local'], $this['origfilename'], $this['thumbnail'], $this['hash'], $this['created_at'], $this['updated_at'], $this['status']);

      $this->widgetSchema['obituary_id']->addOption('order_by', array('last_name', 'asc'))->addOption('method', 'getNameForSelect')->addOption('add_empty', '--- Select Obituary ---');
      $this->setDefault('obituary_id', sfContext::getInstance()->getUser()->getAttribute('movie_obituary_id', ''));
      $this->validatorSchema['obituary_id'] = new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Obituary'), 'required' => true));

      $this->widgetSchema['path'] = new sfWidgetFormInputSWFUpload(array('swfupload_file_types'=>'*.avi;*.mpg;*.flv;*.mov;*.wmv;*.mp4','swfupload_file_types_description'=>'Movie files', 'collapse_queue_on_init'=>false, 'swfupload_file_queue_limit'=>1, 'prevent_form_submit'=>true, 'swfupload_post_params'=>'su: "'.session_id().'"'));
      $this->validatorSchema['path'] = new sfValidatorString(array('max_length' => 255, 'required' => true));

      $this->widgetSchema['state'] = new sfWidgetFormChoice(array('choices' => PlgObituaryMovieTable::getMovieStates()));
      $this->setDefault('state', 1);
      $this->validatorSchema['state'] = new sfValidatorChoice(array('choices' => array_keys(PlgObituaryMovieTable::getMovieStates())));

  }
}
