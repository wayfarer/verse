<?php

/**
 * CmsAffiliate form.
 *
 * @package    verse3
 * @subpackage form
 * @author     Vladimir Droznik
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class CmsAffiliateForm extends BaseCmsAffiliateForm
{
  public function configure()
  {
    unset($this['domain_id']);

    if($this->getObject()->isNew()) {
      $this->setDefault('ref', $this->randomString(8));
    }
  }

  public function randomString($length) {
    $ret = '';
    for ($i=0; $i<$length; $i++) {
        $d=rand(1,30)%2;
        $ret.=($d ? chr(rand(97,122)) : chr(rand(48,57)));
    }
    return $ret;
  }
}