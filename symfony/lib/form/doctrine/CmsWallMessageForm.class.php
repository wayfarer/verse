<?php

/**
 * CmsWallMessage form.
 *
 * @package    verse3
 * @subpackage form
 * @author     Vladimir Droznik
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class CmsWallMessageForm extends BaseCmsWallMessageForm
{
  public function configure()
  {
  }
}
