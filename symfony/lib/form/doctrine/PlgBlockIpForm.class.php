<?php

/**
 * PlgBlockIp form.
 *
 * @package    verse3
 * @subpackage form
 * @author     Vladimir Droznik <vladimir.droznik@gmail.com>
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class PlgBlockIpForm extends BasePlgBlockIpForm {
    public function configure() {
        unset($this['created_at'], $this['updated_at']);
    }
}
