<?php

/**
 * PlgList form.
 *
 * @package    verse3
 * @subpackage form
 * @author     Vladimir Droznik
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class PlgListForm extends BasePlgListForm
{
  public function configure()
  {
    unset($this['domain_id'], $this['guid'], $this['created_at'], $this['updated_at'], $this['company'], $this['address'], $this['address2'], $this['phone'], $this['fax'], $this['website'], $this['email']);

    //  suggester for type
    $this->widgetSchema['type'] = new sfWidgetFormJQuerySuggester(array(
        'url' => sfContext::getInstance()->getController()->genUrl('lists/ac_type')
    ));
    $this->validatorSchema['type'] = new sfValidatorString(array('max_length' => 16, 'required' => true));

    if(!in_array(Doctrine_Core::getTable('SmsDomain')->getCurrent()->getDomainId(), array(671, 56, 124))) {
        unset($this['summary']);
    }
    else {
      $this->widgetSchema['summary'] = new sfWidgetFormTextareaTinyMCE(array(
        'width'  => 730,
        'height' => 250,
        'config' => '
          mode : "exact",
          theme : "advanced",
          theme_advanced_buttons1 : "bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,|,sub,sup,|,bullist,numlist,link,unlink,|,image,|,formatselect,removeformat,|,undo,redo,|,pastetext,pasteword,|,code",
          theme_advanced_buttons2 : "",
          theme_advanced_buttons3 : "",
          theme_advanced_toolbar_location : "top",
          theme_advanced_toolbar_align : "left",
          plugins: "paste,inlinepopups",
          paste_auto_cleanup_on_paste : true
        ',
      ));
    }
    // WYSIWYG for description
    $this->widgetSchema['description'] = new sfWidgetFormTextareaTinyMCE(array(
      'width'  => 730,
      'height' => 370,
      'config' => '
        mode : "exact",
        theme : "advanced",
        theme_advanced_buttons1 : "bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,|,sub,sup,|,bullist,numlist,link,unlink,|,image,|,formatselect,removeformat,|,undo,redo,|,pastetext,pasteword,|,code",
        theme_advanced_buttons2 : "",
        theme_advanced_buttons3 : "",
        theme_advanced_toolbar_location : "top",
        theme_advanced_toolbar_align : "left",
        plugins: "paste,inlinepopups,advimage",
        paste_auto_cleanup_on_paste : true
      ',
    ));

  }
}
