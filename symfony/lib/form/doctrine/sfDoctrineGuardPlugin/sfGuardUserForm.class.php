<?php

/**
 * sfGuardUser form.
 *
 * @package    verse3
 * @subpackage form
 * @author     Vladimir Droznik
 * @version    SVN: $Id: sfDoctrinePluginFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class sfGuardUserForm extends BasesfGuardUserAdminForm
{
  public function configure()
  {
    if(!sfContext::getInstance()->getUser()->isSuperAdmin()) {
      unset($this['is_super_admin']);
    }

    $this->widgetSchema['name'] = new sfWidgetFormInputText();
    $this->validatorSchema['name'] = new sfValidatorPass();
    $this->setDefault('name', $this->getObject()->getProfile()->getName());

    $this->widgetSchema['ftp_password'] = new sfWidgetFormInputPassword();
    // TODO: do not allow empty password when FTP permission changes from not set to set
    $this->validatorSchema['ftp_password'] = new sfValidatorPass();
    $this->widgetSchema->setHelp('ftp_password', '<span style="color: #0055CC">FTP login: '.$this->getObject()->generateFTPUsername().'</span>');

    $this->widgetSchema['groups_list']->setOption('expanded', true);
    $this->widgetSchema['permissions_list']->setOption('expanded', true);
  }

  protected function doUpdateObject($values) {
    // TODO: move this code to sfGuardUser class save method,
    // user getModified method there to determine modified columns
    $this->getObject()->getProfile()->setName($values['name']);
    if(@in_array('3', $values['permissions_list']) && strlen($values['ftp_password'])) { // FTP permisson id = 3
      // add FTP record
      $ftpdb_userid = $this->getObject()->getProfile()->getFtpUserId();
      $ftpdb_account = null;
      if($ftpdb_userid) {
        $ftpdb_account = Doctrine::getTable('FtpDBAccounts')->find($ftpdb_userid);
      }
      if(!$ftpdb_account) {
        $ftpdb_account = new FtpDBAccounts();
        // to generate FTP username correctly
        $this->getObject()->setUsername($values['username']);
        $ftpdb_account->init($this->getObject());
      }
      $ftpdb_account->setPasswd($values['ftp_password'])->setHomedir($this->getObject()->generateFTPPath())->save();
      $this->getObject()->getProfile()->setFtpUserId($ftpdb_account->getUserId());
    }
    else {
      // TODO: maybe find other way to determine changing state of FTP permission
      // getDefault sounds not logical
      if(@in_array('3', $this->getDefault('permissions_list')) && !@in_array('3', $values['permissions_list'])) {
        // FTP permission removed - remove accounts table record
        $ftpdb_userid = $this->getObject()->getProfile()->getFtpUserId();
        $obj = Doctrine::getTable('FtpDBAccounts')->find($ftpdb_userid);
        if($obj) {
          $obj->delete();
        }
        $this->getObject()->getProfile()->setFtpUserId(null);
      }
    }
    //$this->getObject()->getProfile()->save();
    parent::doUpdateObject($values);
  }
}
