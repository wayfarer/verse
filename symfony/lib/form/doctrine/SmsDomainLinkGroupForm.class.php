<?php

/**
 * SmsDomainLinkGroup form.
 *
 * @package    verse3
 * @subpackage form
 * @author     Vladimir Droznik <vladimir.droznik@gmail.com>
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class SmsDomainLinkGroupForm extends BaseSmsDomainLinkGroupForm
{
  public function configure() {
    $this->setWidgets(array(
      'id'                   => new sfWidgetFormInputHidden(),
      'group_name'           => new sfWidgetFormInputText(),
      'domain_id'            => new sfWidgetFormDoctrineChoice(array('model' => 'SmsDomain', 'add_empty' => true, 'order_by' => array('domain_name', 'asc'))),
      'domain_ids'           => new sfWidgetFormInputHidden(),
      'share_obits_from_parent' => new sfWidgetFormInputHidden(),
      'is_shared_obits'      => new sfWidgetFormInputCheckbox(),
      'is_shared_pages'      => new sfWidgetFormInputCheckbox(),
      'shared_page_ids'      => new sfWidgetFormInputHidden(),
      'is_shared_members'    => new sfWidgetFormInputCheckbox(),
      'is_shared_lists'      => new sfWidgetFormInputCheckbox(),
      'list_domain_ids'      => new sfWidgetFormInputHidden(),
      'is_shared_products'   => new sfWidgetFormInputCheckbox(),
      'is_shared_movieclips' => new sfWidgetFormInputCheckbox(),
      'is_shared_structure'  => new sfWidgetFormInputCheckbox(),
    ));
    $this->widgetSchema['domain_id']->setLabel('Domains');

    $this->setValidators(array(
      'id'                   => new sfValidatorDoctrineChoice(array('model' => $this->getModelName(), 'column' => 'id', 'required' => false)),
      'group_name'           => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'domain_id'            => new sfValidatorDoctrineChoice(array('required' => false, 'model' => 'SmsDomain', 'column' => 'domain_id')),
      'domain_ids'           => new sfValidatorString(array('required' => false)),
      'share_obits_from_parent' => new sfValidatorString(array('required' => false)),
      'is_shared_obits'      => new sfValidatorBoolean(),
      'is_shared_pages'      => new sfValidatorBoolean(),
      'shared_page_ids'      => new sfValidatorString(array('required' => false)),
      'is_shared_members'    => new sfValidatorBoolean(),
      'is_shared_lists'      => new sfValidatorBoolean(),
      'list_domain_ids'      => new sfValidatorString(array('required' => false)),
      'is_shared_products'   => new sfValidatorBoolean(),
      'is_shared_movieclips' => new sfValidatorBoolean(),
      'is_shared_structure'  => new sfValidatorBoolean(),
    ));

    $this->widgetSchema->setNameFormat('sms_domain_link_group[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $domain_ids = Doctrine::getTable('SmsDomainLink')->getGroupDomainIds($this->getObject()->getId());
    $this->setDefault('domain_ids', implode(';', $domain_ids));

    $share_obits_from_parent = Doctrine::getTable('SmsDomainLinkGroupConfig')->getShareObitsFromParentOption($this->getObject()->getId());
    $this->setDefault('share_obits_from_parent', $share_obits_from_parent ? $share_obits_from_parent : "");

    $shared_page_ids = Doctrine::getTable('SmsDomainLinkPage')->getSharedPageIds($this->getObject()->getId());
    $this->setDefault('shared_page_ids', count($shared_page_ids) ? json_encode($shared_page_ids) : json_encode($shared_page_ids, JSON_FORCE_OBJECT));

    $list_parent_domain_ids = Doctrine::getTable('SmsDomainLinkListParentDomain')->getGroupListDomainIds($this->getObject()->getId());
    $this->setDefault('list_domain_ids', implode(';', $list_parent_domain_ids));
  }

  protected function doUpdateObject($values) {
    Doctrine::getTable('SmsDomainLink')->setGroupDomainIds($values['id'], $values['domain_ids'] ? explode(';', $values['domain_ids']) : array());
    Doctrine::getTable('SmsDomainLinkGroupConfig')->setShareObitsFromParentOption($values['id'], $values['share_obits_from_parent'] );
    Doctrine::getTable('SmsDomainLinkPage')->setSharedPageIds($values['id'], $values['shared_page_ids'] ? json_decode($values['shared_page_ids'], true) : array());
    Doctrine::getTable('SmsDomainLinkListParentDomain')->setGroupListDomainIds($values['id'], $values['list_domain_ids'] ? explode(';', $values['list_domain_ids']) : array());
    unset($values['domain_id'], $values['domain_ids'], $values['share_obits_from_parent'], $values['shared_page_ids'], $values['list_domain_ids']);
    parent::doUpdateObject($values);
  }
}
