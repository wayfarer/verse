<?php

/**
 * PlgProductCategory form.
 *
 * @package    verse3
 * @subpackage form
 * @author     Vladimir Droznik
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class PlgProductCategoryForm extends BasePlgProductCategoryForm
{
  public function configure()
  {
    unset($this['domain_id'], $this['paypal_checkout']);

    // ecommerce radios
    $this->widgetSchema['ecommerce_enabled'] = new sfWidgetFormChoice(array(
      'choices' => VerseUtil::getThreeStates(),
      'expanded' => true
    ));
    $this->validatorSchema['ecommerce_enabled'] = new sfValidatorChoice(array(
      'choices' => array_keys(VerseUtil::getThreeStates()),
      'required' => true
    ));

    // price radios
    $this->widgetSchema['price_enabled'] = new sfWidgetFormChoice(array(
      'choices' => VerseUtil::getThreeStates(),
      'expanded' => true
    ));
    $this->validatorSchema['price_enabled'] = new sfValidatorChoice(array(
      'choices' => array_keys(VerseUtil::getThreeStates()),
      'required' => true
    ));

    // type dropdown
    $this->widgetSchema['field1type'] = new sfWidgetFormChoice(array(
      'choices' => $this->getAdditionalFieldTypes()
    ));
    $this->validatorSchema['field1type'] = new sfValidatorChoice(array(
      'choices' => array_keys($this->getAdditionalFieldTypes()),
      'required' => true
    ));
    // type dropdown
    $this->widgetSchema['field2type'] = new sfWidgetFormChoice(array(
      'choices' => $this->getAdditionalFieldTypes()
    ));
    $this->validatorSchema['field2type'] = new sfValidatorChoice(array(
      'choices' => array_keys($this->getAdditionalFieldTypes()),
      'required' => true
    ));
  }

  public function getAdditionalFieldTypes() {
    return array('1'=>'Text Area', '2'=>'Text Field');
  }
}
