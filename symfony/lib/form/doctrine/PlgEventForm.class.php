<?php

/**
 * PlgEvent form.
 *
 * @package    verse3
 * @subpackage form
 * @author     Vladimir Droznik
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class PlgEventForm extends BasePlgEventForm
{
  public function configure()
  {
    unset($this['domain_id'], $this['event_case_id'], $this['event_type']);

    $years = range(date('Y'), "2000");
    // TODO: determine why 'default' dont work here, patched below
    $this->widgetSchema['timestamp'] = new sfWidgetFormDateTimeJQueryUI(array('date'=>array("year_range"=>"2000:c+1"), 'time'=>array()));
    $this->validatorSchema['timestamp'] = new sfValidatorDateTime();
//    $this->widgetSchema['timestamp'] = new sfWidgetFormJQueryDate(array(
//      'date_widget' => new sfWidgetFormDateTime(array('years' => array_combine($years, $years)))
//      'date_widget' => new sfWidgetFormDateTime()
//    ));
//    $this->validatorSchema['timestamp'] = new sfValidatorDate(array('required' => false));

    $this->widgetSchema['description'] = new sfWidgetFormTextareaTinyMCE(array(
      'width'  => 730,
      'height' => 370,
      'config' => '
        mode : "exact",
        theme : "advanced",
        theme_advanced_buttons1 : "bold,italic,underline,strikethrough,|,sub,sup,|,bullist,numlist,link,unlink,|,formatselect,|,undo,redo,|,code",
        theme_advanced_buttons2 : "",
        theme_advanced_buttons3 : "",
        theme_advanced_toolbar_location : "top",
        theme_advanced_toolbar_align : "left",
        plugins: "paste,inlinepopups",
        paste_auto_cleanup_on_paste : true
      ',
    ));

    // patch for default values
    if($this->getObject()->isNew()) {
      //
      $this->setDefault('timestamp', time());
    }
  }

  protected function doBind(array $values) {
    $values['timestamp'] = $values['timestamp']['date'].' '.$values['timestamp']['hour'].':'.$values['timestamp']['minute'];
    parent::doBind($values);
  }

  // converts timestamp from date format returned by form to unix timestamp
  public function updateTimestampColumn($value) {
    return strtotime($value);
  }

}
