<?php

/**
 * PlgObituarySubscribe form.
 *
 * @package    verse3
 * @subpackage form
 * @author     Vladimir Droznik
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class PlgObituarySubscribeForm extends BasePlgObituarySubscribeForm
{
  public function configure()
  {
    unset($this['domain_id'], $this['created']);

    $this->widgetSchema['obituary_id']->addOption('order_by', array('last_name', 'asc'))->addOption('method', 'getNameForSelect')->addOption('add_empty', '--- Select Obituary ---');

    $this->validatorSchema['email'] = new sfValidatorEmail();

    // enabled radio
    $this->widgetSchema['enabled'] = new sfWidgetFormChoice(array(
      'choices' => array(0=>'off', 1=>'on'),
      'expanded' => true
    ));

    // isadmin
    $this->widgetSchema['isadmin'] = new sfWidgetFormChoice(array(
      'choices' => array(0=>'normal', 1=>'admin'),
      'expanded' => true
    ));

    $this->validatorSchema['domain_id'] = new sfValidatorPass();
    $this->validatorSchema['obituary_id'] = new sfValidatorInteger();
    $this->validatorSchema->getPostValidator()->setMessage('invalid', 'Email is already subscribed to the current obituary');
  }

  protected function doBind(array $values) {
    $values['domain_id'] = Doctrine::getTable('SmsDomain')->getCurrent()->getDomainId();
    //$values['obituary_id'] = Doctrine::getTable('PlgObituary')->getCurrent()->getObituaryId();
    parent::doBind($values);
  }

/*  public function updateCreatedColumn($value) {
    if(!$value) {
      $value = date("Y-m-d H:i");
    }
    return $value;
  }
*/
}
