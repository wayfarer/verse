<?php

/**
 * PlgObituary form.
 *
 * @package    verse3
 * @subpackage form
 * @author     Vladimir Droznik
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class PlgObituaryForm extends BasePlgObituaryForm
{
  public function configure()
  {
		unset($this['domain_id'], $this['movieclips_list'], $this['created_at'], $this['updated_at']);

		// create new values for years
    $years = range("1900", date('Y'));
    $this['birth_date']->getWidget()->setOption('years', array_combine($years, $years));
    $years = range(date('Y'), "2000");
    $this['death_date']->getWidget()->setOption('years', array_combine($years, $years));

    // attach dropdown calendars for date fields
    $this->widgetSchema['death_date'] = new sfWidgetFormDateJQueryUI(array("year_range"=>"1990:c"));
    $this->validatorSchema['death_date'] = new sfValidatorDate();
/*    $this->widgetSchema['death_date'] = new sfWidgetFormJQueryDate(array(
		  'date_widget' => $this['death_date']->getWidget(),
		));
*/
    $this->widgetSchema['birth_date'] = new sfWidgetFormDateJQueryUI(array("year_range"=>"1900:c"));
    $this->validatorSchema['birth_date'] = new sfValidatorDate(array('required'=>false));
/*    $this->widgetSchema['birth_date'] = new sfWidgetFormJQueryDate(array(
		  'date_widget' => $this['birth_date']->getWidget()
		));
*/

		// service date field
    $this->widgetSchema['service_date'] = new sfWidgetFormDateJQueryUI(array("year_range"=>"1990:c+1"));
/*		$this->widgetSchema['service_date'] = new sfWidgetFormJQueryDate(array(
      'date_widget' => new sfWidgetFormDate(array('years' => array_combine($years, $years)))
		));
*/
		$this->validatorSchema['service_date'] = new sfValidatorDate(array('required' => false));

      $this->widgetSchema['visitation_date'] = new sfWidgetFormDateJQueryUI(array("year_range"=>"1990:c+1"));
      $this->validatorSchema['visitation_date'] = new sfValidatorDate(array('required' => false));

		// WYSIWYG for obituary text
		$this->widgetSchema['obit_text'] = new sfWidgetFormTextareaTinyMCE(array(
		  'width'  => 730,
		  'height' => 370,
		  'config' => '
		    mode : "exact",
    		theme : "advanced",
        theme_advanced_buttons1 : "bold,italic,underline,strikethrough,|,sub,sup,|,bullist,numlist,link,unlink,|,formatselect,|,undo,redo,|,code",
        theme_advanced_buttons2 : "",
        theme_advanced_buttons3 : "",
        theme_advanced_toolbar_location : "top",
        theme_advanced_toolbar_align : "left",
        plugins: "paste,inlinepopups",
        paste_auto_cleanup_on_paste : true
		  ',
		));
		$this->validatorSchema['obit_text'] = new sfValidatorString(array('required'=>false));

    // candles policies radios
    $this->widgetSchema['candles_policy'] = new sfWidgetFormChoice(array(
      'choices' => PlgObituaryTable::getCandlesPolicies(),
		  'expanded' => true
    ));
    $this->validatorSchema['candles_policy'] = new sfValidatorChoice(array(
      'choices' => array_keys(PlgObituaryTable::getCandlesPolicies()),
      'required' => false
    ));

    // service types
		$this->widgetSchema['service_type'] = new sfWidgetFormChoice(array(
      'choices' => PlgObituaryTable::getServiceTypes()
    ));
    $this->validatorSchema['service_type'] = new sfValidatorChoice(array(
      'choices' => array_keys(PlgObituaryTable::getServiceTypes())
    ));

    //military status checkbox
    $this->widgetSchema['military_status'] = new sfWidgetFormInputCheckbox();
    $this->validatorSchema['military_status'] = new sfValidatorPass();

    // home place autocompleter
    $this->widgetSchema['home_place'] = new sfWidgetFormJQuerySuggester(array(
      'url' => sfContext::getInstance()->getController()->genUrl('obituaries/ac_homeplace')
    ));
    // birth place autocompleter
    $this->widgetSchema['birth_place'] = new sfWidgetFormJQuerySuggester(array(
      'url' => sfContext::getInstance()->getController()->genUrl('obituaries/ac_birthplace')
    ));
    // service place autocompleter
    $this->widgetSchema['service_place'] = new sfWidgetFormJQuerySuggester(array(
      'url' => sfContext::getInstance()->getController()->genUrl('obituaries/ac_serviceplace')
    ));
    // visitation place autocompleter
    $this->widgetSchema['visitation_place'] = new sfWidgetFormJQuerySuggester(array(
      'url' => sfContext::getInstance()->getController()->genUrl('obituaries/ac_visitplace')
    ));

    // final disposition autocompleter
    $this->widgetSchema['final_disposition'] = new sfWidgetFormJQuerySuggester(array(
      'url' => sfContext::getInstance()->getController()->genUrl('obituaries/ac_finaldisp')
    ));

    // image with image browser
    $this->widgetSchema['image'] = new sfWidgetFormInputCKFinder(array('startupPath'=>'Images:/obituaries'));
//    $this->widgetSchema['image'] = new sfWidgetFormInputMediaBrowser();
//    $this->validatorSchema['image'] = new sfValidatorMediaBrowserFile(array('required'=>false));
    $this->validatorSchema['image'] = new sfValidatorPass();

    // add to events calendar options
    $this->widgetSchema['service_to_events'] = new sfWidgetFormInputCheckbox();
    $this->validatorSchema['service_to_events'] = new sfValidatorPass();
    $this->widgetSchema['service_to_events']->setLabel('Add to events calendar');

    $this->widgetSchema['visitation_to_events'] = new sfWidgetFormInputCheckbox();
    $this->validatorSchema['visitation_to_events'] = new sfValidatorPass();
    $this->widgetSchema['visitation_to_events']->setLabel('Add to events calendar');

    $this->validatorSchema['domain_id'] = new sfValidatorPass();

    $this->validatorSchema->getPostValidator()->setMessage('invalid', 'Case ID already exists');

    $this->setDefault('death_date', time()-86400);

    // TODO: review - other way of movieclips setup, probably some nice visual way
    $this->widgetSchema['movieclips'] = new sfWidgetFormInputText();
    $this->validatorSchema['movieclips'] = new sfValidatorPass();

    // $this->embedForm('obituary_config', new ObituaryConfigForm());

    // initialize field with associated movieclips
    if(!$this->getObject()->isNew()) {
      $obituary_has_movieclips = Doctrine::getTable('PlgObituaryMovieclip')->findByObituaryId($this->getObject()->getObituaryId());
      $movieclip_ids = array();
      foreach($obituary_has_movieclips as $obituary_has_movieclip) {
        $movieclip_ids[] = $obituary_has_movieclip->getMovieclipId();
      }
      $this->setDefault('movieclips', implode(',', $movieclip_ids));

      $this->setDefault('service_to_events', Doctrine_Core::getTable('PlgEvent')->obituaryServiceExists($this->getObject()->getObituaryId()));
      $this->setDefault('visitation_to_events', Doctrine_Core::getTable('PlgEvent')->obituaryVisitationExists($this->getObject()->getObituaryId()));

      $obituary_military_status_config = Doctrine_Core::getTable('PlgObituaryConfig')->createQuery()->addWhere("param = 'military_status' AND obituary_id = ?", $this->getObject()->getObituaryId())->fetchOne();
      if($obituary_military_status_config) {
        if(!$obituary_military_status_config->getValue() || $obituary_military_status_config->getValue() === 'off') {
          $checked = false;
        } else {
          $checked = true;
        }
        $this->setDefault('military_status', $checked);
      }
    }
  }

  protected function doBind(array $values) {
    $values['domain_id'] = Doctrine::getTable('SmsDomain')->getCurrent()->getDomainId();
    $values['finaldisposition_google_method'] = 'Address';
    $values['service_google_method'] = 'Address';
    parent::doBind($values);
  }

  // converts service date from date format returned by form to unix timestamp
  public function updateServiceDateColumn($value) {
		$value = trim($value);
    if($value) {
      return strtotime($value);
    }
    return null;
  }

  public function updateVisitationDateColumn($value) {
    $value = trim($value);
    if($value) {
        return strtotime($value);
    }
    return null;
  }

  public function updateObituaryCaseIdColumn($value) {
    if($value=="") $value = null;
    return $value;
  }

  // resize image here
  public function updateImageColumn($value) {
    return VerseUtil::resizeOriginal($value, sfConfig::get('app_obituary_image_width_max', 800));
  }

/*  protected function doBind(array $values) {
    $this->movieclips = $values['movieclips'];

    parent::doBind($values);
  }
*/
  protected function doUpdateObject($values) {
    unset($values['slug']);
    parent::doUpdateObject($values);

    // TODO: maybe find out a better way to determine obituary_id
    if($this->getObject()->isNew()) {
      $this->getObject()->save();
      $this->getObject()->setObituaryId(Doctrine_Manager::getInstance()->getCurrentConnection()->lastInsertId('obituary_id'));
    }

    // manage obituary to events associations
    $conn = Doctrine_Manager::connection();
    $ret = $conn->standaloneQuery("DELETE FROM plg_event WHERE domain_id=? AND obituary_id=? AND (event_type='1' OR event_type='2')", array(Doctrine::getTable('SmsDomain')->getCurrent()->getDomainId(), $this->getObject()->getObituaryId()));
    if(@$values['service_to_events']) {
      if(@$values['service_date']) {
        $ret = $conn->standaloneQuery("INSERT plg_event SET domain_id=?, obituary_id=?, event_type='1', timestamp=?, description=?", array(Doctrine::getTable('SmsDomain')->getCurrent()->getDomainId(), $this->getObject()->getObituaryId(), $values['service_date'], $values['service_place']));
      }
    }
    if(@$values['visitation_to_events']) {
      if($values["visitation_date"]) {
        $ret = $conn->standaloneQuery("INSERT plg_event SET domain_id=?, obituary_id=?, event_type='2', timestamp=?, description=?", array(Doctrine::getTable('SmsDomain')->getCurrent()->getDomainId(), $this->getObject()->getObituaryId(), $values["visitation_date"], $values['visitation_place']));
      }
    }

    // TODO: review this, maybe add link in schema and see autogenerated functionality
    if(isset($values['movieclips'])) {
      // delete existing associations
      $obituary_has_movieclips = Doctrine::getTable('PlgObituaryMovieclip')->findByObituaryId($this->getObject()->getObituaryId());
      $movieclip_ids = explode(',', $values['movieclips']);

      foreach($obituary_has_movieclips as $obituary_has_movieclip){
        if(in_array($obituary_has_movieclip['movieclip_id'], $movieclip_ids)) {
          // movieclip already associated with obituary, do nothing
          unset($movieclip_ids[array_search($obituary_has_movieclip['movieclip_id'], $movieclip_ids)]);
        }
        else {
          // movieclip no longer associated with obituary, delete it
          $obituary_has_movieclip->delete();
        }
      }

      $domain_ids = Doctrine::getTable('SmsDomain')->getCurrent()->getSharedMovieclipDomainIds();
      if (count($domain_ids)) {
          $and_where = "domain_id IN (".implode(", ", $domain_ids).")";
      } else {
          $and_where = "domain_id = ".Doctrine::getTable('SmsDomain')->getCurrent()->getDomainId();
      }

      // create new associations
      foreach($movieclip_ids as $movieclip_id) {
        $q = Doctrine::getTable('PlgMovieclip')->createQuery('m')
              ->where('m.movieclip_id = ?', $movieclip_id)->andWhere($and_where);
        $movieclip = $q->fetchOne();
        if($movieclip) {
          // create association with movieclip
          $obituary_has_movieclip = new PlgObituaryMovieclip();
          $obituary_has_movieclip->setMovieclipId($movieclip_id);
          $obituary_has_movieclip->setObituaryId($this->getObject()->getObituaryId());
          $obituary_has_movieclip->save();
        }
      }
    }

    $domain_id = Doctrine::getTable('SmsDomain')->getCurrent()->getDomainId();
    if(isset($values['military_status'])) {
      $obituary_config = Doctrine_Core::getTable('PlgObituaryConfig')->createQuery()->addWhere("param = 'military_status' AND obituary_id = ?", $this->getObject()->getObituaryId())->fetchOne();
      if($obituary_config) {
        $obituary_config->setValue('on');
        $obituary_config->save();
      } else {
        $obituary_config = new PlgObituaryConfig();
        $obituary_config->setDomainId($domain_id);
        $obituary_config->setObituaryId($this->getObject()->getObituaryId());
        $obituary_config->setParam('military_status');
        $obituary_config->setValue('on');
        $obituary_config->save();
      }
    } else {
      Doctrine_Query::create()->delete()->from('PlgObituaryConfig')->where("domain_id = ? AND param = 'military_status' AND obituary_id = ?", array($domain_id, $this->getObject()->getObituaryId()))->execute();
    }
  }
}
