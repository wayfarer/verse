<?php

/**
 * PlgBlockKeyword form.
 *
 * @package    verse3
 * @subpackage form
 * @author     Vladimir Droznik <vladimir.droznik@gmail.com>
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class PlgBlockKeywordForm extends BasePlgBlockKeywordForm {
    public function configure() {
        $this->widgetSchema['domain_id']->addOption('add_empty', '--- All ---');
    }
}
