<?php

/**
 * PlgObituaryImage form.
 *
 * @package    verse3
 * @subpackage form
 * @author     Vladimir Droznik <vladimir.droznik@gmail.com>
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class PlgObituaryImageForm extends BasePlgObituaryImageForm
{
  public function configure() {
      unset($this['domain_id'], $this['origfilename'], $this['hash'], $this['created_at'], $this['updated_at']);

      $this->widgetSchema['obituary_id']->addOption('order_by', array('last_name', 'asc'))->addOption('method', 'getNameForSelect')->addOption('add_empty', '--- Select Obituary ---');
      $this->setDefault('obituary_id', sfContext::getInstance()->getUser()->getAttribute('photo_obituary_id', ''));
      $this->validatorSchema['obituary_id'] = new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Obituary'), 'required' => true));

      $this->widgetSchema['path'] = new sfWidgetFormInputCKFinder(array('startupPath'=>'Images:/scrap'));
      $this->validatorSchema['path'] = new sfValidatorString(array('max_length' => 255, 'required' => true));

      $this->widgetSchema['state'] = new sfWidgetFormChoice(array('choices' => PlgObituaryImageTable::getImageStates()));
      $this->validatorSchema['state'] = new sfValidatorChoice(array('choices' => array_keys(PlgObituaryImageTable::getImageStates())));
  }
}
