<?php

/**
 * PlgEvent form base class.
 *
 * @method PlgEvent getObject() Returns the current form's model object
 *
 * @package    verse3
 * @subpackage form
 * @author     Vladimir Droznik <vladimir.droznik@gmail.com>
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BasePlgEventForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'event_id'      => new sfWidgetFormInputHidden(),
      'event_case_id' => new sfWidgetFormInputText(),
      'domain_id'     => new sfWidgetFormInputText(),
      'obituary_id'   => new sfWidgetFormInputText(),
      'event_type'    => new sfWidgetFormInputText(),
      'timestamp'     => new sfWidgetFormInputText(),
      'description'   => new sfWidgetFormTextarea(),
    ));

    $this->setValidators(array(
      'event_id'      => new sfValidatorDoctrineChoice(array('model' => $this->getModelName(), 'column' => 'event_id', 'required' => false)),
      'event_case_id' => new sfValidatorString(array('max_length' => 50, 'required' => false)),
      'domain_id'     => new sfValidatorInteger(array('required' => false)),
      'obituary_id'   => new sfValidatorInteger(array('required' => false)),
      'event_type'    => new sfValidatorInteger(array('required' => false)),
      'timestamp'     => new sfValidatorInteger(array('required' => false)),
      'description'   => new sfValidatorString(),
    ));

    $this->widgetSchema->setNameFormat('plg_event[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'PlgEvent';
  }

}
