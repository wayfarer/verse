<?php

/**
 * CmsWebcasting form base class.
 *
 * @method CmsWebcasting getObject() Returns the current form's model object
 *
 * @package    verse3
 * @subpackage form
 * @author     Vladimir Droznik <vladimir.droznik@gmail.com>
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseCmsWebcastingForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'webcasting_id' => new sfWidgetFormInputHidden(),
      'domain_id'     => new sfWidgetFormInputText(),
      'obituary_id'   => new sfWidgetFormInputText(),
      'name'          => new sfWidgetFormInputText(),
      'schedule_from' => new sfWidgetFormDateTime(),
      'schedule_to'   => new sfWidgetFormDateTime(),
      'was_from'      => new sfWidgetFormDateTime(),
      'was_to'        => new sfWidgetFormDateTime(),
      'created'       => new sfWidgetFormDateTime(),
      'record'        => new sfWidgetFormInputText(),
      'status'        => new sfWidgetFormInputText(),
    ));

    $this->setValidators(array(
      'webcasting_id' => new sfValidatorDoctrineChoice(array('model' => $this->getModelName(), 'column' => 'webcasting_id', 'required' => false)),
      'domain_id'     => new sfValidatorInteger(),
      'obituary_id'   => new sfValidatorInteger(array('required' => false)),
      'name'          => new sfValidatorString(array('max_length' => 50)),
      'schedule_from' => new sfValidatorDateTime(array('required' => false)),
      'schedule_to'   => new sfValidatorDateTime(array('required' => false)),
      'was_from'      => new sfValidatorDateTime(array('required' => false)),
      'was_to'        => new sfValidatorDateTime(array('required' => false)),
      'created'       => new sfValidatorDateTime(),
      'record'        => new sfValidatorInteger(),
      'status'        => new sfValidatorInteger(),
    ));

    $this->widgetSchema->setNameFormat('cms_webcasting[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'CmsWebcasting';
  }

}
