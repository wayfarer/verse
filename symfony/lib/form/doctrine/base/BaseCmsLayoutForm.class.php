<?php

/**
 * CmsLayout form base class.
 *
 * @method CmsLayout getObject() Returns the current form's model object
 *
 * @package    verse3
 * @subpackage form
 * @author     Vladimir Droznik <vladimir.droznik@gmail.com>
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseCmsLayoutForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'layout_id'   => new sfWidgetFormInputHidden(),
      'layout_name' => new sfWidgetFormInputText(),
    ));

    $this->setValidators(array(
      'layout_id'   => new sfValidatorDoctrineChoice(array('model' => $this->getModelName(), 'column' => 'layout_id', 'required' => false)),
      'layout_name' => new sfValidatorString(array('max_length' => 32, 'required' => false)),
    ));

    $this->widgetSchema->setNameFormat('cms_layout[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'CmsLayout';
  }

}
