<?php

/**
 * PlgBlockIp form base class.
 *
 * @method PlgBlockIp getObject() Returns the current form's model object
 *
 * @package    verse3
 * @subpackage form
 * @author     Vladimir Droznik <vladimir.droznik@gmail.com>
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BasePlgBlockIpForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'         => new sfWidgetFormInputHidden(),
      'ip'         => new sfWidgetFormInputText(),
      'created_at' => new sfWidgetFormDateTime(),
      'updated_at' => new sfWidgetFormDateTime(),
      'count'      => new sfWidgetFormInputText(),
      'domain_id'  => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Domain'), 'add_empty' => true)),
    ));

    $this->setValidators(array(
      'id'         => new sfValidatorDoctrineChoice(array('model' => $this->getModelName(), 'column' => 'id', 'required' => false)),
      'ip'         => new sfValidatorString(array('max_length' => 15, 'required' => false)),
      'created_at' => new sfValidatorDateTime(),
      'updated_at' => new sfValidatorDateTime(),
      'count'      => new sfValidatorInteger(array('required' => false)),
      'domain_id'  => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Domain'), 'required' => false)),
    ));

    $this->widgetSchema->setNameFormat('plg_block_ip[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'PlgBlockIp';
  }

}
