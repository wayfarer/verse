<?php

/**
 * CmsNode form base class.
 *
 * @method CmsNode getObject() Returns the current form's model object
 *
 * @package    verse3
 * @subpackage form
 * @author     Vladimir Droznik <vladimir.droznik@gmail.com>
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseCmsNodeForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'node_id'       => new sfWidgetFormInputHidden(),
      'internal_name' => new sfWidgetFormInputText(),
      'domain_id'     => new sfWidgetFormInputText(),
      'page_id'       => new sfWidgetFormInputText(),
      'ord'           => new sfWidgetFormInputText(),
      'depth'         => new sfWidgetFormInputText(),
      'menu_type'     => new sfWidgetFormInputText(),
      'display_name'  => new sfWidgetFormInputText(),
      'params'        => new sfWidgetFormTextarea(),
      'page_id_old'   => new sfWidgetFormInputText(),
    ));

    $this->setValidators(array(
      'node_id'       => new sfValidatorDoctrineChoice(array('model' => $this->getModelName(), 'column' => 'node_id', 'required' => false)),
      'internal_name' => new sfValidatorString(array('max_length' => 32, 'required' => false)),
      'domain_id'     => new sfValidatorInteger(array('required' => false)),
      'page_id'       => new sfValidatorInteger(array('required' => false)),
      'ord'           => new sfValidatorInteger(array('required' => false)),
      'depth'         => new sfValidatorInteger(array('required' => false)),
      'menu_type'     => new sfValidatorInteger(array('required' => false)),
      'display_name'  => new sfValidatorString(array('max_length' => 64, 'required' => false)),
      'params'        => new sfValidatorString(),
      'page_id_old'   => new sfValidatorInteger(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('cms_node[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'CmsNode';
  }

}
