<?php

/**
 * CmsConfig form base class.
 *
 * @method CmsConfig getObject() Returns the current form's model object
 *
 * @package    verse3
 * @subpackage form
 * @author     Vladimir Droznik <vladimir.droznik@gmail.com>
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseCmsConfigForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'domain_id' => new sfWidgetFormInputHidden(),
      'param'     => new sfWidgetFormInputHidden(),
      'value'     => new sfWidgetFormTextarea(),
    ));

    $this->setValidators(array(
      'domain_id' => new sfValidatorDoctrineChoice(array('model' => $this->getModelName(), 'column' => 'domain_id', 'required' => false)),
      'param'     => new sfValidatorDoctrineChoice(array('model' => $this->getModelName(), 'column' => 'param', 'required' => false)),
      'value'     => new sfValidatorString(),
    ));

    $this->widgetSchema->setNameFormat('cms_config[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'CmsConfig';
  }

}
