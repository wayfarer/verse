<?php

/**
 * PlgSlide form base class.
 *
 * @method PlgSlide getObject() Returns the current form's model object
 *
 * @package    verse3
 * @subpackage form
 * @author     Vladimir Droznik <vladimir.droznik@gmail.com>
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BasePlgSlideForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'slide_id'    => new sfWidgetFormInputHidden(),
      'domain_id'   => new sfWidgetFormInputHidden(),
      'obituary_id' => new sfWidgetFormInputHidden(),
      'image'       => new sfWidgetFormInputText(),
      'description' => new sfWidgetFormTextarea(),
    ));

    $this->setValidators(array(
      'slide_id'    => new sfValidatorDoctrineChoice(array('model' => $this->getModelName(), 'column' => 'slide_id', 'required' => false)),
      'domain_id'   => new sfValidatorDoctrineChoice(array('model' => $this->getModelName(), 'column' => 'domain_id', 'required' => false)),
      'obituary_id' => new sfValidatorDoctrineChoice(array('model' => $this->getModelName(), 'column' => 'obituary_id', 'required' => false)),
      'image'       => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'description' => new sfValidatorString(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('plg_slide[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'PlgSlide';
  }

}
