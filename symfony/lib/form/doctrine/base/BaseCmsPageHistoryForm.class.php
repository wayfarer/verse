<?php

/**
 * CmsPageHistory form base class.
 *
 * @method CmsPageHistory getObject() Returns the current form's model object
 *
 * @package    verse3
 * @subpackage form
 * @author     Vladimir Droznik <vladimir.droznik@gmail.com>
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseCmsPageHistoryForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'page_history_id' => new sfWidgetFormInputHidden(),
      'page_id'         => new sfWidgetFormInputText(),
      'user_id'         => new sfWidgetFormInputText(),
      'timestamp'       => new sfWidgetFormInputText(),
      'action'          => new sfWidgetFormInputText(),
      'content'         => new sfWidgetFormTextarea(),
    ));

    $this->setValidators(array(
      'page_history_id' => new sfValidatorDoctrineChoice(array('model' => $this->getModelName(), 'column' => 'page_history_id', 'required' => false)),
      'page_id'         => new sfValidatorInteger(array('required' => false)),
      'user_id'         => new sfValidatorInteger(array('required' => false)),
      'timestamp'       => new sfValidatorInteger(array('required' => false)),
      'action'          => new sfValidatorInteger(array('required' => false)),
      'content'         => new sfValidatorString(),
    ));

    $this->widgetSchema->setNameFormat('cms_page_history[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'CmsPageHistory';
  }

}
