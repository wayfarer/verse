<?php

/**
 * PlgProductCategory form base class.
 *
 * @method PlgProductCategory getObject() Returns the current form's model object
 *
 * @package    verse3
 * @subpackage form
 * @author     Vladimir Droznik <vladimir.droznik@gmail.com>
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BasePlgProductCategoryForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'category_id'       => new sfWidgetFormInputHidden(),
      'domain_id'         => new sfWidgetFormInputText(),
      'name'              => new sfWidgetFormInputText(),
      'ecommerce_enabled' => new sfWidgetFormInputText(),
      'price_enabled'     => new sfWidgetFormInputText(),
      'additional_fields' => new sfWidgetFormInputCheckbox(),
      'field1enabled'     => new sfWidgetFormInputCheckbox(),
      'field1title'       => new sfWidgetFormInputText(),
      'field1type'        => new sfWidgetFormInputText(),
      'field2enabled'     => new sfWidgetFormInputCheckbox(),
      'field2title'       => new sfWidgetFormInputText(),
      'field2type'        => new sfWidgetFormInputText(),
    ));

    $this->setValidators(array(
      'category_id'       => new sfValidatorDoctrineChoice(array('model' => $this->getModelName(), 'column' => 'category_id', 'required' => false)),
      'domain_id'         => new sfValidatorInteger(array('required' => false)),
      'name'              => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'ecommerce_enabled' => new sfValidatorInteger(array('required' => false)),
      'price_enabled'     => new sfValidatorInteger(array('required' => false)),
      'additional_fields' => new sfValidatorBoolean(array('required' => false)),
      'field1enabled'     => new sfValidatorBoolean(array('required' => false)),
      'field1title'       => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'field1type'        => new sfValidatorInteger(array('required' => false)),
      'field2enabled'     => new sfValidatorBoolean(array('required' => false)),
      'field2title'       => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'field2type'        => new sfValidatorInteger(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('plg_product_category[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'PlgProductCategory';
  }

}
