<?php

/**
 * PlgFuneralDirector form base class.
 *
 * @method PlgFuneralDirector getObject() Returns the current form's model object
 *
 * @package    verse3
 * @subpackage form
 * @author     Vladimir Droznik <vladimir.droznik@gmail.com>
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BasePlgFuneralDirectorForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'director_id' => new sfWidgetFormInputHidden(),
      'status'      => new sfWidgetFormInputText(),
      'title'       => new sfWidgetFormInputText(),
      'first_name'  => new sfWidgetFormInputText(),
      'last_name'   => new sfWidgetFormInputText(),
      'firm'        => new sfWidgetFormInputText(),
      'address'     => new sfWidgetFormInputText(),
      'city'        => new sfWidgetFormInputText(),
      'state'       => new sfWidgetFormInputText(),
      'zip'         => new sfWidgetFormInputText(),
      'phone'       => new sfWidgetFormInputText(),
      'fax'         => new sfWidgetFormInputText(),
      'county'      => new sfWidgetFormInputText(),
    ));

    $this->setValidators(array(
      'director_id' => new sfValidatorDoctrineChoice(array('model' => $this->getModelName(), 'column' => 'director_id', 'required' => false)),
      'status'      => new sfValidatorString(array('max_length' => 2, 'required' => false)),
      'title'       => new sfValidatorString(array('max_length' => 5, 'required' => false)),
      'first_name'  => new sfValidatorString(array('max_length' => 32, 'required' => false)),
      'last_name'   => new sfValidatorString(array('max_length' => 32, 'required' => false)),
      'firm'        => new sfValidatorString(array('max_length' => 64, 'required' => false)),
      'address'     => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'city'        => new sfValidatorString(array('max_length' => 64, 'required' => false)),
      'state'       => new sfValidatorString(array('max_length' => 2, 'required' => false)),
      'zip'         => new sfValidatorString(array('max_length' => 6, 'required' => false)),
      'phone'       => new sfValidatorString(array('max_length' => 16, 'required' => false)),
      'fax'         => new sfValidatorString(array('max_length' => 16, 'required' => false)),
      'county'      => new sfValidatorString(array('max_length' => 16, 'required' => false)),
    ));

    $this->widgetSchema->setNameFormat('plg_funeral_director[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'PlgFuneralDirector';
  }

}
