<?php

/**
 * SmsDomain form base class.
 *
 * @method SmsDomain getObject() Returns the current form's model object
 *
 * @package    verse3
 * @subpackage form
 * @author     Vladimir Droznik <vladimir.droznik@gmail.com>
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseSmsDomainForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'domain_id'          => new sfWidgetFormInputHidden(),
      'domain_name'        => new sfWidgetFormInputText(),
      'server_id'          => new sfWidgetFormInputText(),
      'enabled'            => new sfWidgetFormInputCheckbox(),
      'email'              => new sfWidgetFormInputText(),
      'postfix'            => new sfWidgetFormInputText(),
      'email_enabled'      => new sfWidgetFormInputCheckbox(),
      'ecommerce_enabled'  => new sfWidgetFormInputCheckbox(),
      'price_enabled'      => new sfWidgetFormInputCheckbox(),
      'paypal_checkout'    => new sfWidgetFormInputCheckbox(),
      'affiliates_enabled' => new sfWidgetFormInputCheckbox(),
      'mode'               => new sfWidgetFormInputText(),
      'alias_domain_id'    => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('DomainAlias'), 'add_empty' => false)),
      'space_consumed'     => new sfWidgetFormInputText(),
      'created_at'         => new sfWidgetFormDateTime(),
      'updated_at'         => new sfWidgetFormDateTime(),
    ));

    $this->setValidators(array(
      'domain_id'          => new sfValidatorDoctrineChoice(array('model' => $this->getModelName(), 'column' => 'domain_id', 'required' => false)),
      'domain_name'        => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'server_id'          => new sfValidatorInteger(array('required' => false)),
      'enabled'            => new sfValidatorBoolean(array('required' => false)),
      'email'              => new sfValidatorString(array('max_length' => 64, 'required' => false)),
      'postfix'            => new sfValidatorString(array('max_length' => 8, 'required' => false)),
      'email_enabled'      => new sfValidatorBoolean(array('required' => false)),
      'ecommerce_enabled'  => new sfValidatorBoolean(array('required' => false)),
      'price_enabled'      => new sfValidatorBoolean(array('required' => false)),
      'paypal_checkout'    => new sfValidatorBoolean(array('required' => false)),
      'affiliates_enabled' => new sfValidatorBoolean(array('required' => false)),
      'mode'               => new sfValidatorInteger(array('required' => false)),
      'alias_domain_id'    => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('DomainAlias'), 'required' => false)),
      'space_consumed'     => new sfValidatorInteger(array('required' => false)),
      'created_at'         => new sfValidatorDateTime(),
      'updated_at'         => new sfValidatorDateTime(),
    ));

    $this->validatorSchema->setPostValidator(
      new sfValidatorAnd(array(
        new sfValidatorDoctrineUnique(array('model' => 'SmsDomain', 'column' => array('domain_name'))),
        new sfValidatorDoctrineUnique(array('model' => 'SmsDomain', 'column' => array('postfix'))),
      ))
    );

    $this->widgetSchema->setNameFormat('sms_domain[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'SmsDomain';
  }

}
