<?php

/**
 * CmsWallThread form base class.
 *
 * @method CmsWallThread getObject() Returns the current form's model object
 *
 * @package    verse3
 * @subpackage form
 * @author     Vladimir Droznik <vladimir.droznik@gmail.com>
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseCmsWallThreadForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'thread_id'  => new sfWidgetFormInputHidden(),
      'domain_id'  => new sfWidgetFormInputText(),
      'wall_id'    => new sfWidgetFormInputText(),
      'title'      => new sfWidgetFormInputText(),
      'created_at' => new sfWidgetFormDateTime(),
      'user_id'    => new sfWidgetFormInputText(),
    ));

    $this->setValidators(array(
      'thread_id'  => new sfValidatorDoctrineChoice(array('model' => $this->getModelName(), 'column' => 'thread_id', 'required' => false)),
      'domain_id'  => new sfValidatorInteger(),
      'wall_id'    => new sfValidatorInteger(),
      'title'      => new sfValidatorString(array('max_length' => 255)),
      'created_at' => new sfValidatorDateTime(),
      'user_id'    => new sfValidatorInteger(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('cms_wall_thread[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'CmsWallThread';
  }

}
