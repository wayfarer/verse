<?php

/**
 * PlgMailbox form base class.
 *
 * @method PlgMailbox getObject() Returns the current form's model object
 *
 * @package    verse3
 * @subpackage form
 * @author     Vladimir Droznik <vladimir.droznik@gmail.com>
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BasePlgMailboxForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'mailbox_id' => new sfWidgetFormInputHidden(),
      'domain_id'  => new sfWidgetFormInputText(),
      'type'       => new sfWidgetFormInputText(),
      'name'       => new sfWidgetFormInputText(),
      'login'      => new sfWidgetFormInputText(),
      'forward'    => new sfWidgetFormInputText(),
    ));

    $this->setValidators(array(
      'mailbox_id' => new sfValidatorDoctrineChoice(array('model' => $this->getModelName(), 'column' => 'mailbox_id', 'required' => false)),
      'domain_id'  => new sfValidatorInteger(array('required' => false)),
      'type'       => new sfValidatorInteger(array('required' => false)),
      'name'       => new sfValidatorString(array('max_length' => 64, 'required' => false)),
      'login'      => new sfValidatorString(array('max_length' => 64, 'required' => false)),
      'forward'    => new sfValidatorString(array('max_length' => 255, 'required' => false)),
    ));

    $this->widgetSchema->setNameFormat('plg_mailbox[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'PlgMailbox';
  }

}
