<?php

/**
 * PlgObituarySubscribe form base class.
 *
 * @method PlgObituarySubscribe getObject() Returns the current form's model object
 *
 * @package    verse3
 * @subpackage form
 * @author     Vladimir Droznik <vladimir.droznik@gmail.com>
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BasePlgObituarySubscribeForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'obituary_subscribe_id' => new sfWidgetFormInputHidden(),
      'obituary_id'           => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Obituary'), 'add_empty' => false)),
      'domain_id'             => new sfWidgetFormInputHidden(),
      'name'                  => new sfWidgetFormInputText(),
      'email'                 => new sfWidgetFormInputText(),
      'created'               => new sfWidgetFormDateTime(),
      'hash'                  => new sfWidgetFormInputText(),
      'enabled'               => new sfWidgetFormInputText(),
      'isadmin'               => new sfWidgetFormInputText(),
    ));

    $this->setValidators(array(
      'obituary_subscribe_id' => new sfValidatorDoctrineChoice(array('model' => $this->getModelName(), 'column' => 'obituary_subscribe_id', 'required' => false)),
      'obituary_id'           => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Obituary'))),
      'domain_id'             => new sfValidatorDoctrineChoice(array('model' => $this->getModelName(), 'column' => 'domain_id', 'required' => false)),
      'name'                  => new sfValidatorString(array('max_length' => 45)),
      'email'                 => new sfValidatorString(array('max_length' => 45)),
      'created'               => new sfValidatorDateTime(),
      'hash'                  => new sfValidatorString(array('max_length' => 16, 'required' => false)),
      'enabled'               => new sfValidatorInteger(),
      'isadmin'               => new sfValidatorInteger(array('required' => false)),
    ));

    $this->validatorSchema->setPostValidator(
      new sfValidatorDoctrineUnique(array('model' => 'PlgObituarySubscribe', 'column' => array('obituary_id', 'domain_id', 'email')))
    );

    $this->widgetSchema->setNameFormat('plg_obituary_subscribe[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'PlgObituarySubscribe';
  }

}
