<?php

/**
 * PlgProduct form base class.
 *
 * @method PlgProduct getObject() Returns the current form's model object
 *
 * @package    verse3
 * @subpackage form
 * @author     Vladimir Droznik <vladimir.droznik@gmail.com>
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BasePlgProductForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'product_id'  => new sfWidgetFormInputHidden(),
      'guid'        => new sfWidgetFormInputText(),
      'domain_id'   => new sfWidgetFormInputText(),
      'category_id' => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('ProductCategory'), 'add_empty' => false)),
      'name'        => new sfWidgetFormInputText(),
      'price'       => new sfWidgetFormInputText(),
      'quantity'    => new sfWidgetFormInputText(),
      'image'       => new sfWidgetFormInputText(),
      'description' => new sfWidgetFormTextarea(),
      'sold_times'  => new sfWidgetFormInputText(),
      'timestamp'   => new sfWidgetFormInputText(),
      'enabled'     => new sfWidgetFormInputCheckbox(),
    ));

    $this->setValidators(array(
      'product_id'  => new sfValidatorDoctrineChoice(array('model' => $this->getModelName(), 'column' => 'product_id', 'required' => false)),
      'guid'        => new sfValidatorString(array('max_length' => 50, 'required' => false)),
      'domain_id'   => new sfValidatorInteger(array('required' => false)),
      'category_id' => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('ProductCategory'), 'required' => false)),
      'name'        => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'price'       => new sfValidatorNumber(array('required' => false)),
      'quantity'    => new sfValidatorInteger(array('required' => false)),
      'image'       => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'description' => new sfValidatorString(),
      'sold_times'  => new sfValidatorInteger(array('required' => false)),
      'timestamp'   => new sfValidatorInteger(array('required' => false)),
      'enabled'     => new sfValidatorBoolean(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('plg_product[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'PlgProduct';
  }

}
