<?php

/**
 * SmsUser form base class.
 *
 * @method SmsUser getObject() Returns the current form's model object
 *
 * @package    verse3
 * @subpackage form
 * @author     Vladimir Droznik <vladimir.droznik@gmail.com>
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseSmsUserForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'user_id'   => new sfWidgetFormInputHidden(),
      'login'     => new sfWidgetFormInputText(),
      'password'  => new sfWidgetFormInputText(),
      'enabled'   => new sfWidgetFormInputText(),
      'domain_id' => new sfWidgetFormInputText(),
      'name'      => new sfWidgetFormInputText(),
      'email'     => new sfWidgetFormInputText(),
    ));

    $this->setValidators(array(
      'user_id'   => new sfValidatorDoctrineChoice(array('model' => $this->getModelName(), 'column' => 'user_id', 'required' => false)),
      'login'     => new sfValidatorString(array('max_length' => 32, 'required' => false)),
      'password'  => new sfValidatorString(array('max_length' => 48, 'required' => false)),
      'enabled'   => new sfValidatorInteger(array('required' => false)),
      'domain_id' => new sfValidatorInteger(array('required' => false)),
      'name'      => new sfValidatorString(array('max_length' => 64, 'required' => false)),
      'email'     => new sfValidatorString(array('max_length' => 64, 'required' => false)),
    ));

    $this->widgetSchema->setNameFormat('sms_user[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'SmsUser';
  }

}
