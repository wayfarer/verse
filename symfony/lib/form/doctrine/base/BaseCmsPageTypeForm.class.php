<?php

/**
 * CmsPageType form base class.
 *
 * @method CmsPageType getObject() Returns the current form's model object
 *
 * @package    verse3
 * @subpackage form
 * @author     Vladimir Droznik <vladimir.droznik@gmail.com>
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseCmsPageTypeForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'page_type_id'     => new sfWidgetFormInputHidden(),
      'page_type_name'   => new sfWidgetFormInputText(),
      'page_type_handle' => new sfWidgetFormInputText(),
    ));

    $this->setValidators(array(
      'page_type_id'     => new sfValidatorDoctrineChoice(array('model' => $this->getModelName(), 'column' => 'page_type_id', 'required' => false)),
      'page_type_name'   => new sfValidatorString(array('max_length' => 32, 'required' => false)),
      'page_type_handle' => new sfValidatorString(array('max_length' => 16, 'required' => false)),
    ));

    $this->widgetSchema->setNameFormat('cms_page_type[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'CmsPageType';
  }

}
