<?php

/**
 * CmsMenuType form base class.
 *
 * @method CmsMenuType getObject() Returns the current form's model object
 *
 * @package    verse3
 * @subpackage form
 * @author     Vladimir Droznik <vladimir.droznik@gmail.com>
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseCmsMenuTypeForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'menu_type_id' => new sfWidgetFormInputHidden(),
      'name'         => new sfWidgetFormInputText(),
    ));

    $this->setValidators(array(
      'menu_type_id' => new sfValidatorDoctrineChoice(array('model' => $this->getModelName(), 'column' => 'menu_type_id', 'required' => false)),
      'name'         => new sfValidatorString(array('max_length' => 32, 'required' => false)),
    ));

    $this->widgetSchema->setNameFormat('cms_menu_type[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'CmsMenuType';
  }

}
