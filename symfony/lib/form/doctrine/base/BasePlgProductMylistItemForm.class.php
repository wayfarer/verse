<?php

/**
 * PlgProductMylistItem form base class.
 *
 * @method PlgProductMylistItem getObject() Returns the current form's model object
 *
 * @package    verse3
 * @subpackage form
 * @author     Vladimir Droznik <vladimir.droznik@gmail.com>
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BasePlgProductMylistItemForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'mylist_id'  => new sfWidgetFormInputHidden(),
      'product_id' => new sfWidgetFormInputHidden(),
      'price'      => new sfWidgetFormInputText(),
    ));

    $this->setValidators(array(
      'mylist_id'  => new sfValidatorDoctrineChoice(array('model' => $this->getModelName(), 'column' => 'mylist_id', 'required' => false)),
      'product_id' => new sfValidatorDoctrineChoice(array('model' => $this->getModelName(), 'column' => 'product_id', 'required' => false)),
      'price'      => new sfValidatorNumber(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('plg_product_mylist_item[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'PlgProductMylistItem';
  }

}
