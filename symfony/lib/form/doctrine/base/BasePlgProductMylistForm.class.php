<?php

/**
 * PlgProductMylist form base class.
 *
 * @method PlgProductMylist getObject() Returns the current form's model object
 *
 * @package    verse3
 * @subpackage form
 * @author     Vladimir Droznik <vladimir.droznik@gmail.com>
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BasePlgProductMylistForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'mylist_id' => new sfWidgetFormInputHidden(),
      'domain_id' => new sfWidgetFormInputText(),
      'user_id'   => new sfWidgetFormInputText(),
      'hash'      => new sfWidgetFormInputText(),
    ));

    $this->setValidators(array(
      'mylist_id' => new sfValidatorDoctrineChoice(array('model' => $this->getModelName(), 'column' => 'mylist_id', 'required' => false)),
      'domain_id' => new sfValidatorInteger(array('required' => false)),
      'user_id'   => new sfValidatorInteger(array('required' => false)),
      'hash'      => new sfValidatorString(array('max_length' => 16, 'required' => false)),
    ));

    $this->widgetSchema->setNameFormat('plg_product_mylist[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'PlgProductMylist';
  }

}
