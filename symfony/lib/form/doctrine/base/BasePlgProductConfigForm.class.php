<?php

/**
 * PlgProductConfig form base class.
 *
 * @method PlgProductConfig getObject() Returns the current form's model object
 *
 * @package    verse3
 * @subpackage form
 * @author     Vladimir Droznik <vladimir.droznik@gmail.com>
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BasePlgProductConfigForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'domain_id' => new sfWidgetFormInputHidden(),
      'param'     => new sfWidgetFormInputHidden(),
      'value'     => new sfWidgetFormInputText(),
    ));

    $this->setValidators(array(
      'domain_id' => new sfValidatorDoctrineChoice(array('model' => $this->getModelName(), 'column' => 'domain_id', 'required' => false)),
      'param'     => new sfValidatorDoctrineChoice(array('model' => $this->getModelName(), 'column' => 'param', 'required' => false)),
      'value'     => new sfValidatorString(array('max_length' => 255, 'required' => false)),
    ));

    $this->widgetSchema->setNameFormat('plg_product_config[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'PlgProductConfig';
  }

}
