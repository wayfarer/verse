<?php

/**
 * CmsSiteUc form base class.
 *
 * @method CmsSiteUc getObject() Returns the current form's model object
 *
 * @package    verse3
 * @subpackage form
 * @author     Vladimir Droznik <vladimir.droznik@gmail.com>
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseCmsSiteUcForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'domain_id'   => new sfWidgetFormInputHidden(),
      'title'       => new sfWidgetFormInputText(),
      'properties'  => new sfWidgetFormTextarea(),
      'css'         => new sfWidgetFormTextarea(),
      'header_mode' => new sfWidgetFormInputText(),
      'header'      => new sfWidgetFormTextarea(),
      'footer'      => new sfWidgetFormTextarea(),
    ));

    $this->setValidators(array(
      'domain_id'   => new sfValidatorDoctrineChoice(array('model' => $this->getModelName(), 'column' => 'domain_id', 'required' => false)),
      'title'       => new sfValidatorString(array('max_length' => 128, 'required' => false)),
      'properties'  => new sfValidatorString(),
      'css'         => new sfValidatorString(array('required' => false)),
      'header_mode' => new sfValidatorInteger(array('required' => false)),
      'header'      => new sfValidatorString(),
      'footer'      => new sfValidatorString(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('cms_site_uc[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'CmsSiteUc';
  }

}
