<?php

/**
 * SmsDomainLink form base class.
 *
 * @method SmsDomainLink getObject() Returns the current form's model object
 *
 * @package    verse3
 * @subpackage form
 * @author     Vladimir Droznik <vladimir.droznik@gmail.com>
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseSmsDomainLinkForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'        => new sfWidgetFormInputHidden(),
      'group_id'  => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('DomainLinkGroup'), 'add_empty' => false)),
      'domain_id' => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Domain'), 'add_empty' => false)),
    ));

    $this->setValidators(array(
      'id'        => new sfValidatorDoctrineChoice(array('model' => $this->getModelName(), 'column' => 'id', 'required' => false)),
      'group_id'  => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('DomainLinkGroup'))),
      'domain_id' => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Domain'))),
    ));

    $this->widgetSchema->setNameFormat('sms_domain_link[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'SmsDomainLink';
  }

}
