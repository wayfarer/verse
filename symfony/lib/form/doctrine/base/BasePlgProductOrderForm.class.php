<?php

/**
 * PlgProductOrder form base class.
 *
 * @method PlgProductOrder getObject() Returns the current form's model object
 *
 * @package    verse3
 * @subpackage form
 * @author     Vladimir Droznik <vladimir.droznik@gmail.com>
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BasePlgProductOrderForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'order_id'       => new sfWidgetFormInputHidden(),
      'domain_id'      => new sfWidgetFormInputText(),
      'timestamp'      => new sfWidgetFormDateTime(),
      'total'          => new sfWidgetFormInputText(),
      'payer'          => new sfWidgetFormInputText(),
      'affiliate_id'   => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('OrderAffiliate'), 'add_empty' => true)),
      'affiliate_from' => new sfWidgetFormInputText(),
      'user_data'      => new sfWidgetFormTextarea(),
      'status'         => new sfWidgetFormInputText(),
    ));

    $this->setValidators(array(
      'order_id'       => new sfValidatorDoctrineChoice(array('model' => $this->getModelName(), 'column' => 'order_id', 'required' => false)),
      'domain_id'      => new sfValidatorInteger(array('required' => false)),
      'timestamp'      => new sfValidatorDateTime(array('required' => false)),
      'total'          => new sfValidatorNumber(array('required' => false)),
      'payer'          => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'affiliate_id'   => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('OrderAffiliate'), 'required' => false)),
      'affiliate_from' => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'user_data'      => new sfValidatorString(),
      'status'         => new sfValidatorString(array('max_length' => 32, 'required' => false)),
    ));

    $this->widgetSchema->setNameFormat('plg_product_order[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'PlgProductOrder';
  }

}
