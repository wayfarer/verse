<?php

/**
 * FtpDBAccounts form base class.
 *
 * @method FtpDBAccounts getObject() Returns the current form's model object
 *
 * @package    verse3
 * @subpackage form
 * @author     Vladimir Droznik <vladimir.droznik@gmail.com>
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseFtpDBAccountsForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'user_id'  => new sfWidgetFormInputHidden(),
      'username' => new sfWidgetFormInputText(),
      'passwd'   => new sfWidgetFormInputText(),
      'uid'      => new sfWidgetFormInputText(),
      'gid'      => new sfWidgetFormInputText(),
      'homedir'  => new sfWidgetFormInputText(),
      'shell'    => new sfWidgetFormInputText(),
    ));

    $this->setValidators(array(
      'user_id'  => new sfValidatorDoctrineChoice(array('model' => $this->getModelName(), 'column' => 'user_id', 'required' => false)),
      'username' => new sfValidatorString(array('max_length' => 100, 'required' => false)),
      'passwd'   => new sfValidatorString(array('max_length' => 50, 'required' => false)),
      'uid'      => new sfValidatorInteger(array('required' => false)),
      'gid'      => new sfValidatorInteger(array('required' => false)),
      'homedir'  => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'shell'    => new sfValidatorString(array('max_length' => 30, 'required' => false)),
    ));

    $this->widgetSchema->setNameFormat('ftp_db_accounts[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'FtpDBAccounts';
  }

}
