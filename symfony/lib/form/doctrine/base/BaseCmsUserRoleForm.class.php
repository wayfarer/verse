<?php

/**
 * CmsUserRole form base class.
 *
 * @method CmsUserRole getObject() Returns the current form's model object
 *
 * @package    verse3
 * @subpackage form
 * @author     Vladimir Droznik <vladimir.droznik@gmail.com>
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseCmsUserRoleForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'user_id'     => new sfWidgetFormInputHidden(),
      'role_id'     => new sfWidgetFormInputHidden(),
      'role_params' => new sfWidgetFormTextarea(),
    ));

    $this->setValidators(array(
      'user_id'     => new sfValidatorDoctrineChoice(array('model' => $this->getModelName(), 'column' => 'user_id', 'required' => false)),
      'role_id'     => new sfValidatorDoctrineChoice(array('model' => $this->getModelName(), 'column' => 'role_id', 'required' => false)),
      'role_params' => new sfValidatorString(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('cms_user_role[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'CmsUserRole';
  }

}
