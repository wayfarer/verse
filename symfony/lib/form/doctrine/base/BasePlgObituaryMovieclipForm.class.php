<?php

/**
 * PlgObituaryMovieclip form base class.
 *
 * @method PlgObituaryMovieclip getObject() Returns the current form's model object
 *
 * @package    verse3
 * @subpackage form
 * @author     Vladimir Droznik <vladimir.droznik@gmail.com>
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BasePlgObituaryMovieclipForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'obituary_id'  => new sfWidgetFormInputHidden(),
      'movieclip_id' => new sfWidgetFormInputHidden(),
    ));

    $this->setValidators(array(
      'obituary_id'  => new sfValidatorDoctrineChoice(array('model' => $this->getModelName(), 'column' => 'obituary_id', 'required' => false)),
      'movieclip_id' => new sfValidatorDoctrineChoice(array('model' => $this->getModelName(), 'column' => 'movieclip_id', 'required' => false)),
    ));

    $this->widgetSchema->setNameFormat('plg_obituary_movieclip[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'PlgObituaryMovieclip';
  }

}
