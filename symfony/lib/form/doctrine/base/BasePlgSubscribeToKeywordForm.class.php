<?php

/**
 * PlgSubscribeToKeyword form base class.
 *
 * @method PlgSubscribeToKeyword getObject() Returns the current form's model object
 *
 * @package    verse3
 * @subpackage form
 * @author     Vladimir Droznik <vladimir.droznik@gmail.com>
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BasePlgSubscribeToKeywordForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'           => new sfWidgetFormInputHidden(),
      'subscribe_id' => new sfWidgetFormInputText(),
      'keyword_id'   => new sfWidgetFormInputText(),
    ));

    $this->setValidators(array(
      'id'           => new sfValidatorDoctrineChoice(array('model' => $this->getModelName(), 'column' => 'id', 'required' => false)),
      'subscribe_id' => new sfValidatorInteger(),
      'keyword_id'   => new sfValidatorInteger(),
    ));

    $this->widgetSchema->setNameFormat('plg_subscribe_to_keyword[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'PlgSubscribeToKeyword';
  }

}
