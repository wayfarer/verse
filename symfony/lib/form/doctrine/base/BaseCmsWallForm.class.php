<?php

/**
 * CmsWall form base class.
 *
 * @method CmsWall getObject() Returns the current form's model object
 *
 * @package    verse3
 * @subpackage form
 * @author     Vladimir Droznik <vladimir.droznik@gmail.com>
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseCmsWallForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'wall_id'    => new sfWidgetFormInputHidden(),
      'domain_id'  => new sfWidgetFormInputText(),
      'handle'     => new sfWidgetFormInputText(),
      'created_at' => new sfWidgetFormDateTime(),
    ));

    $this->setValidators(array(
      'wall_id'    => new sfValidatorDoctrineChoice(array('model' => $this->getModelName(), 'column' => 'wall_id', 'required' => false)),
      'domain_id'  => new sfValidatorInteger(),
      'handle'     => new sfValidatorString(array('max_length' => 16)),
      'created_at' => new sfValidatorDateTime(),
    ));

    $this->widgetSchema->setNameFormat('cms_wall[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'CmsWall';
  }

}
