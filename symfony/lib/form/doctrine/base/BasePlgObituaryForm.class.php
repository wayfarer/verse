<?php

/**
 * PlgObituary form base class.
 *
 * @method PlgObituary getObject() Returns the current form's model object
 *
 * @package    verse3
 * @subpackage form
 * @author     Vladimir Droznik <vladimir.droznik@gmail.com>
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BasePlgObituaryForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'obituary_id'                    => new sfWidgetFormInputHidden(),
      'domain_id'                      => new sfWidgetFormInputHidden(),
      'obituary_case_id'               => new sfWidgetFormInputText(),
      'first_name'                     => new sfWidgetFormInputText(),
      'middle_name'                    => new sfWidgetFormInputText(),
      'last_name'                      => new sfWidgetFormInputText(),
      'home_place'                     => new sfWidgetFormInputText(),
      'death_date'                     => new sfWidgetFormDate(),
      'birth_place'                    => new sfWidgetFormInputText(),
      'birth_date'                     => new sfWidgetFormDate(),
      'service_date'                   => new sfWidgetFormInputText(),
      'service_time'                   => new sfWidgetFormInputText(),
      'service_place'                  => new sfWidgetFormInputText(),
      'visitation_date'                => new sfWidgetFormInputText(),
      'visitation_place'               => new sfWidgetFormInputText(),
      'final_disposition'              => new sfWidgetFormInputText(),
      'image'                          => new sfWidgetFormInputText(),
      'obit_text'                      => new sfWidgetFormTextarea(),
      'candles_policy'                 => new sfWidgetFormInputText(),
      'service_type'                   => new sfWidgetFormInputText(),
      'title'                          => new sfWidgetFormInputText(),
      'suffix'                         => new sfWidgetFormInputText(),
      'finaldisposition_google_method' => new sfWidgetFormInputText(),
      'finaldisposition_google_addr'   => new sfWidgetFormInputText(),
      'service_google_method'          => new sfWidgetFormInputText(),
      'service_google_addr'            => new sfWidgetFormInputText(),
      'created_at'                     => new sfWidgetFormDateTime(),
      'updated_at'                     => new sfWidgetFormDateTime(),
      'movieclips_list'                => new sfWidgetFormDoctrineChoice(array('multiple' => true, 'model' => 'PlgMovieclip')),
    ));

    $this->setValidators(array(
      'obituary_id'                    => new sfValidatorDoctrineChoice(array('model' => $this->getModelName(), 'column' => 'obituary_id', 'required' => false)),
      'domain_id'                      => new sfValidatorDoctrineChoice(array('model' => $this->getModelName(), 'column' => 'domain_id', 'required' => false)),
      'obituary_case_id'               => new sfValidatorString(array('max_length' => 50, 'required' => false)),
      'first_name'                     => new sfValidatorString(array('max_length' => 32, 'required' => false)),
      'middle_name'                    => new sfValidatorString(array('max_length' => 32, 'required' => false)),
      'last_name'                      => new sfValidatorString(array('max_length' => 32, 'required' => false)),
      'home_place'                     => new sfValidatorString(array('max_length' => 128, 'required' => false)),
      'death_date'                     => new sfValidatorDate(array('required' => false)),
      'birth_place'                    => new sfValidatorString(array('max_length' => 128, 'required' => false)),
      'birth_date'                     => new sfValidatorDate(array('required' => false)),
      'service_date'                   => new sfValidatorInteger(array('required' => false)),
      'service_time'                   => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'service_place'                  => new sfValidatorString(array('max_length' => 128, 'required' => false)),
      'visitation_date'                => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'visitation_place'               => new sfValidatorString(array('max_length' => 128, 'required' => false)),
      'final_disposition'              => new sfValidatorString(array('max_length' => 128, 'required' => false)),
      'image'                          => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'obit_text'                      => new sfValidatorString(),
      'candles_policy'                 => new sfValidatorString(array('max_length' => 10, 'required' => false)),
      'service_type'                   => new sfValidatorInteger(array('required' => false)),
      'title'                          => new sfValidatorString(array('max_length' => 50, 'required' => false)),
      'suffix'                         => new sfValidatorString(array('max_length' => 50, 'required' => false)),
      'finaldisposition_google_method' => new sfValidatorString(array('max_length' => 128, 'required' => false)),
      'finaldisposition_google_addr'   => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'service_google_method'          => new sfValidatorString(array('max_length' => 128, 'required' => false)),
      'service_google_addr'            => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'created_at'                     => new sfValidatorDateTime(),
      'updated_at'                     => new sfValidatorDateTime(),
      'movieclips_list'                => new sfValidatorDoctrineChoice(array('multiple' => true, 'model' => 'PlgMovieclip', 'required' => false)),
    ));

    $this->validatorSchema->setPostValidator(
      new sfValidatorDoctrineUnique(array('model' => 'PlgObituary', 'column' => array('obituary_case_id', 'domain_id')))
    );

    $this->widgetSchema->setNameFormat('plg_obituary[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'PlgObituary';
  }

  public function updateDefaultsFromObject()
  {
    parent::updateDefaultsFromObject();

    if (isset($this->widgetSchema['movieclips_list']))
    {
      $this->setDefault('movieclips_list', $this->object->Movieclips->getPrimaryKeys());
    }

  }

  protected function doSave($con = null)
  {
    $this->saveMovieclipsList($con);

    parent::doSave($con);
  }

  public function saveMovieclipsList($con = null)
  {
    if (!$this->isValid())
    {
      throw $this->getErrorSchema();
    }

    if (!isset($this->widgetSchema['movieclips_list']))
    {
      // somebody has unset this widget
      return;
    }

    if (null === $con)
    {
      $con = $this->getConnection();
    }

    $existing = $this->object->Movieclips->getPrimaryKeys();
    $values = $this->getValue('movieclips_list');
    if (!is_array($values))
    {
      $values = array();
    }

    $unlink = array_diff($existing, $values);
    if (count($unlink))
    {
      $this->object->unlink('Movieclips', array_values($unlink));
    }

    $link = array_diff($values, $existing);
    if (count($link))
    {
      $this->object->link('Movieclips', array_values($link));
    }
  }

}
