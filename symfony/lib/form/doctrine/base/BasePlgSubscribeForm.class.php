<?php

/**
 * PlgSubscribe form base class.
 *
 * @method PlgSubscribe getObject() Returns the current form's model object
 *
 * @package    verse3
 * @subpackage form
 * @author     Vladimir Droznik <vladimir.droznik@gmail.com>
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BasePlgSubscribeForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'               => new sfWidgetFormInputHidden(),
      'email'            => new sfWidgetFormInputText(),
      'domain_id'        => new sfWidgetFormInputText(),
      'name'             => new sfWidgetFormInputText(),
      'hash'             => new sfWidgetFormInputText(),
      'enabled'          => new sfWidgetFormInputCheckbox(),
      'confirmation_cnt' => new sfWidgetFormInputText(),
      'created_at'       => new sfWidgetFormDateTime(),
      'updated_at'       => new sfWidgetFormDateTime(),
    ));

    $this->setValidators(array(
      'id'               => new sfValidatorDoctrineChoice(array('model' => $this->getModelName(), 'column' => 'id', 'required' => false)),
      'email'            => new sfValidatorString(array('max_length' => 50)),
      'domain_id'        => new sfValidatorInteger(),
      'name'             => new sfValidatorString(array('max_length' => 50)),
      'hash'             => new sfValidatorString(array('max_length' => 16)),
      'enabled'          => new sfValidatorBoolean(array('required' => false)),
      'confirmation_cnt' => new sfValidatorInteger(array('required' => false)),
      'created_at'       => new sfValidatorDateTime(),
      'updated_at'       => new sfValidatorDateTime(),
    ));

    $this->validatorSchema->setPostValidator(
      new sfValidatorDoctrineUnique(array('model' => 'PlgSubscribe', 'column' => array('domain_id', 'email')))
    );

    $this->widgetSchema->setNameFormat('plg_subscribe[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'PlgSubscribe';
  }

}
