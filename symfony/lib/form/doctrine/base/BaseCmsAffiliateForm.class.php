<?php

/**
 * CmsAffiliate form base class.
 *
 * @method CmsAffiliate getObject() Returns the current form's model object
 *
 * @package    verse3
 * @subpackage form
 * @author     Vladimir Droznik <vladimir.droznik@gmail.com>
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseCmsAffiliateForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'affiliate_id' => new sfWidgetFormInputHidden(),
      'domain_id'    => new sfWidgetFormInputText(),
      'ref'          => new sfWidgetFormInputText(),
      'title'        => new sfWidgetFormInputText(),
      'page_id'      => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('AffiliatePage'), 'add_empty' => false)),
    ));

    $this->setValidators(array(
      'affiliate_id' => new sfValidatorDoctrineChoice(array('model' => $this->getModelName(), 'column' => 'affiliate_id', 'required' => false)),
      'domain_id'    => new sfValidatorInteger(array('required' => false)),
      'ref'          => new sfValidatorString(array('max_length' => 16, 'required' => false)),
      'title'        => new sfValidatorString(array('max_length' => 100, 'required' => false)),
      'page_id'      => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('AffiliatePage'), 'required' => false)),
    ));

    $this->widgetSchema->setNameFormat('cms_affiliate[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'CmsAffiliate';
  }

}
