<?php

/**
 * PlgObituaryMovie form base class.
 *
 * @method PlgObituaryMovie getObject() Returns the current form's model object
 *
 * @package    verse3
 * @subpackage form
 * @author     Vladimir Droznik <vladimir.droznik@gmail.com>
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BasePlgObituaryMovieForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'           => new sfWidgetFormInputHidden(),
      'obituary_id'  => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Obituary'), 'add_empty' => false)),
      'domain_id'    => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Domain'), 'add_empty' => false)),
      'local'        => new sfWidgetFormInputText(),
      'path'         => new sfWidgetFormInputText(),
      'origfilename' => new sfWidgetFormInputText(),
      'thumbnail'    => new sfWidgetFormInputText(),
      'state'        => new sfWidgetFormInputText(),
      'hash'         => new sfWidgetFormInputText(),
      'status'       => new sfWidgetFormInputText(),
      'created_at'   => new sfWidgetFormDateTime(),
      'updated_at'   => new sfWidgetFormDateTime(),
    ));

    $this->setValidators(array(
      'id'           => new sfValidatorDoctrineChoice(array('model' => $this->getModelName(), 'column' => 'id', 'required' => false)),
      'obituary_id'  => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Obituary'))),
      'domain_id'    => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Domain'))),
      'local'        => new sfValidatorInteger(array('required' => false)),
      'path'         => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'origfilename' => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'thumbnail'    => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'state'        => new sfValidatorInteger(array('required' => false)),
      'hash'         => new sfValidatorString(array('max_length' => 16, 'required' => false)),
      'status'       => new sfValidatorInteger(array('required' => false)),
      'created_at'   => new sfValidatorDateTime(),
      'updated_at'   => new sfValidatorDateTime(),
    ));

    $this->widgetSchema->setNameFormat('plg_obituary_movie[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'PlgObituaryMovie';
  }

}
