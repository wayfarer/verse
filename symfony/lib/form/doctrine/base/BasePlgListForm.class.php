<?php

/**
 * PlgList form base class.
 *
 * @method PlgList getObject() Returns the current form's model object
 *
 * @package    verse3
 * @subpackage form
 * @author     Vladimir Droznik <vladimir.droznik@gmail.com>
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BasePlgListForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'list_id'     => new sfWidgetFormInputHidden(),
      'guid'        => new sfWidgetFormInputText(),
      'domain_id'   => new sfWidgetFormInputText(),
      'type'        => new sfWidgetFormInputText(),
      'name'        => new sfWidgetFormInputText(),
      'company'     => new sfWidgetFormInputText(),
      'address'     => new sfWidgetFormInputText(),
      'address2'    => new sfWidgetFormInputText(),
      'phone'       => new sfWidgetFormInputText(),
      'fax'         => new sfWidgetFormInputText(),
      'website'     => new sfWidgetFormInputText(),
      'email'       => new sfWidgetFormInputText(),
      'summary'     => new sfWidgetFormInputText(),
      'description' => new sfWidgetFormTextarea(),
      'created_at'  => new sfWidgetFormDateTime(),
      'updated_at'  => new sfWidgetFormDateTime(),
    ));

    $this->setValidators(array(
      'list_id'     => new sfValidatorDoctrineChoice(array('model' => $this->getModelName(), 'column' => 'list_id', 'required' => false)),
      'guid'        => new sfValidatorString(array('max_length' => 50, 'required' => false)),
      'domain_id'   => new sfValidatorInteger(array('required' => false)),
      'type'        => new sfValidatorString(array('max_length' => 16, 'required' => false)),
      'name'        => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'company'     => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'address'     => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'address2'    => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'phone'       => new sfValidatorString(array('max_length' => 32, 'required' => false)),
      'fax'         => new sfValidatorString(array('max_length' => 32, 'required' => false)),
      'website'     => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'email'       => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'summary'     => new sfValidatorPass(array('required' => false)),
      'description' => new sfValidatorString(array('required' => false)),
      'created_at'  => new sfValidatorDateTime(),
      'updated_at'  => new sfValidatorDateTime(),
    ));

    $this->widgetSchema->setNameFormat('plg_list[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'PlgList';
  }

}
