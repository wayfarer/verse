<?php

/**
 * CmsAccessUser form base class.
 *
 * @method CmsAccessUser getObject() Returns the current form's model object
 *
 * @package    verse3
 * @subpackage form
 * @author     Vladimir Droznik <vladimir.droznik@gmail.com>
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseCmsAccessUserForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'user_id'               => new sfWidgetFormInputHidden(),
      'domain_id'             => new sfWidgetFormInputText(),
      'login'                 => new sfWidgetFormInputText(),
      'password'              => new sfWidgetFormInputText(),
      'force_password_change' => new sfWidgetFormInputText(),
      'name'                  => new sfWidgetFormInputText(),
      'email'                 => new sfWidgetFormInputText(),
      'enabled'               => new sfWidgetFormInputText(),
      'privileges'            => new sfWidgetFormInputText(),
      'webcasting_id'         => new sfWidgetFormInputText(),
      'hash'                  => new sfWidgetFormInputText(),
      'internal_name'         => new sfWidgetFormInputText(),
      'created_at'            => new sfWidgetFormDateTime(),
      'updated_at'            => new sfWidgetFormDateTime(),
    ));

    $this->setValidators(array(
      'user_id'               => new sfValidatorDoctrineChoice(array('model' => $this->getModelName(), 'column' => 'user_id', 'required' => false)),
      'domain_id'             => new sfValidatorInteger(array('required' => false)),
      'login'                 => new sfValidatorString(array('max_length' => 64)),
      'password'              => new sfValidatorString(array('max_length' => 32, 'required' => false)),
      'force_password_change' => new sfValidatorInteger(),
      'name'                  => new sfValidatorString(array('max_length' => 50)),
      'email'                 => new sfValidatorString(array('max_length' => 50, 'required' => false)),
      'enabled'               => new sfValidatorInteger(array('required' => false)),
      'privileges'            => new sfValidatorInteger(),
      'webcasting_id'         => new sfValidatorInteger(array('required' => false)),
      'hash'                  => new sfValidatorString(array('max_length' => 32, 'required' => false)),
      'internal_name'         => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'created_at'            => new sfValidatorDateTime(),
      'updated_at'            => new sfValidatorDateTime(),
    ));

    $this->widgetSchema->setNameFormat('cms_access_user[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'CmsAccessUser';
  }

}
