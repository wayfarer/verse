<?php

/**
 * CmsPage form base class.
 *
 * @method CmsPage getObject() Returns the current form's model object
 *
 * @package    verse3
 * @subpackage form
 * @author     Vladimir Droznik <vladimir.droznik@gmail.com>
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseCmsPageForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'page_id'       => new sfWidgetFormInputHidden(),
      'domain_id'     => new sfWidgetFormInputText(),
      'ord'           => new sfWidgetFormInputText(),
      'page_type'     => new sfWidgetFormInputText(),
      'internal_name' => new sfWidgetFormInputText(),
      'no_header'     => new sfWidgetFormInputText(),
      'content'       => new sfWidgetFormTextarea(),
      'restricted'    => new sfWidgetFormInputText(),
      'properties'    => new sfWidgetFormTextarea(),
      'custom_header' => new sfWidgetFormTextarea(),
    ));

    $this->setValidators(array(
      'page_id'       => new sfValidatorDoctrineChoice(array('model' => $this->getModelName(), 'column' => 'page_id', 'required' => false)),
      'domain_id'     => new sfValidatorInteger(array('required' => false)),
      'ord'           => new sfValidatorInteger(array('required' => false)),
      'page_type'     => new sfValidatorInteger(array('required' => false)),
      'internal_name' => new sfValidatorString(array('max_length' => 32, 'required' => false)),
      'no_header'     => new sfValidatorInteger(array('required' => false)),
      'content'       => new sfValidatorString(),
      'restricted'    => new sfValidatorInteger(array('required' => false)),
      'properties'    => new sfValidatorString(),
      'custom_header' => new sfValidatorString(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('cms_page[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'CmsPage';
  }

}
