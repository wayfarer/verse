<?php

/**
 * PlgMovieclip form base class.
 *
 * @method PlgMovieclip getObject() Returns the current form's model object
 *
 * @package    verse3
 * @subpackage form
 * @author     Vladimir Droznik <vladimir.droznik@gmail.com>
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BasePlgMovieclipForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'movieclip_id'      => new sfWidgetFormInputHidden(),
      'domain_id'         => new sfWidgetFormInputText(),
      'title'             => new sfWidgetFormInputText(),
      'description'       => new sfWidgetFormInputText(),
      'filename'          => new sfWidgetFormInputText(),
      'timestamp'         => new sfWidgetFormInputText(),
      'protected'         => new sfWidgetFormInputCheckbox(),
      'password'          => new sfWidgetFormInputText(),
      'status'            => new sfWidgetFormInputText(),
      'tmpfilename'       => new sfWidgetFormInputText(),
      'origfilename'      => new sfWidgetFormInputText(),
      'obituaries_list'   => new sfWidgetFormDoctrineChoice(array('multiple' => true, 'model' => 'PlgMovieclip')),
      'plg_obituary_list' => new sfWidgetFormDoctrineChoice(array('multiple' => true, 'model' => 'PlgObituary')),
    ));

    $this->setValidators(array(
      'movieclip_id'      => new sfValidatorDoctrineChoice(array('model' => $this->getModelName(), 'column' => 'movieclip_id', 'required' => false)),
      'domain_id'         => new sfValidatorInteger(array('required' => false)),
      'title'             => new sfValidatorString(array('max_length' => 128, 'required' => false)),
      'description'       => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'filename'          => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'timestamp'         => new sfValidatorInteger(array('required' => false)),
      'protected'         => new sfValidatorBoolean(array('required' => false)),
      'password'          => new sfValidatorString(array('max_length' => 50, 'required' => false)),
      'status'            => new sfValidatorInteger(array('required' => false)),
      'tmpfilename'       => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'origfilename'      => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'obituaries_list'   => new sfValidatorDoctrineChoice(array('multiple' => true, 'model' => 'PlgMovieclip', 'required' => false)),
      'plg_obituary_list' => new sfValidatorDoctrineChoice(array('multiple' => true, 'model' => 'PlgObituary', 'required' => false)),
    ));

    $this->widgetSchema->setNameFormat('plg_movieclip[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'PlgMovieclip';
  }

  public function updateDefaultsFromObject()
  {
    parent::updateDefaultsFromObject();

    if (isset($this->widgetSchema['obituaries_list']))
    {
      $this->setDefault('obituaries_list', $this->object->Obituaries->getPrimaryKeys());
    }

    if (isset($this->widgetSchema['plg_obituary_list']))
    {
      $this->setDefault('plg_obituary_list', $this->object->PlgObituary->getPrimaryKeys());
    }

  }

  protected function doSave($con = null)
  {
    $this->saveObituariesList($con);
    $this->savePlgObituaryList($con);

    parent::doSave($con);
  }

  public function saveObituariesList($con = null)
  {
    if (!$this->isValid())
    {
      throw $this->getErrorSchema();
    }

    if (!isset($this->widgetSchema['obituaries_list']))
    {
      // somebody has unset this widget
      return;
    }

    if (null === $con)
    {
      $con = $this->getConnection();
    }

    $existing = $this->object->Obituaries->getPrimaryKeys();
    $values = $this->getValue('obituaries_list');
    if (!is_array($values))
    {
      $values = array();
    }

    $unlink = array_diff($existing, $values);
    if (count($unlink))
    {
      $this->object->unlink('Obituaries', array_values($unlink));
    }

    $link = array_diff($values, $existing);
    if (count($link))
    {
      $this->object->link('Obituaries', array_values($link));
    }
  }

  public function savePlgObituaryList($con = null)
  {
    if (!$this->isValid())
    {
      throw $this->getErrorSchema();
    }

    if (!isset($this->widgetSchema['plg_obituary_list']))
    {
      // somebody has unset this widget
      return;
    }

    if (null === $con)
    {
      $con = $this->getConnection();
    }

    $existing = $this->object->PlgObituary->getPrimaryKeys();
    $values = $this->getValue('plg_obituary_list');
    if (!is_array($values))
    {
      $values = array();
    }

    $unlink = array_diff($existing, $values);
    if (count($unlink))
    {
      $this->object->unlink('PlgObituary', array_values($unlink));
    }

    $link = array_diff($values, $existing);
    if (count($link))
    {
      $this->object->link('PlgObituary', array_values($link));
    }
  }

}
