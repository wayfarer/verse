<?php

/**
 * CmsAlign form base class.
 *
 * @method CmsAlign getObject() Returns the current form's model object
 *
 * @package    verse3
 * @subpackage form
 * @author     Vladimir Droznik <vladimir.droznik@gmail.com>
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseCmsAlignForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'align_id'   => new sfWidgetFormInputHidden(),
      'align_name' => new sfWidgetFormInputText(),
    ));

    $this->setValidators(array(
      'align_id'   => new sfValidatorDoctrineChoice(array('model' => $this->getModelName(), 'column' => 'align_id', 'required' => false)),
      'align_name' => new sfValidatorString(array('max_length' => 32, 'required' => false)),
    ));

    $this->widgetSchema->setNameFormat('cms_align[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'CmsAlign';
  }

}
