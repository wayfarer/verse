<?php

/**
 * SmsDomainLinkGroup form base class.
 *
 * @method SmsDomainLinkGroup getObject() Returns the current form's model object
 *
 * @package    verse3
 * @subpackage form
 * @author     Vladimir Droznik <vladimir.droznik@gmail.com>
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseSmsDomainLinkGroupForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'                   => new sfWidgetFormInputHidden(),
      'is_shared_obits'      => new sfWidgetFormInputCheckbox(),
      'is_shared_pages'      => new sfWidgetFormInputCheckbox(),
      'is_shared_members'    => new sfWidgetFormInputCheckbox(),
      'is_shared_lists'      => new sfWidgetFormInputCheckbox(),
      'is_shared_products'   => new sfWidgetFormInputCheckbox(),
      'is_shared_movieclips' => new sfWidgetFormInputCheckbox(),
      'is_shared_structure'  => new sfWidgetFormInputCheckbox(),
    ));

    $this->setValidators(array(
      'id'                   => new sfValidatorDoctrineChoice(array('model' => $this->getModelName(), 'column' => 'id', 'required' => false)),
      'is_shared_obits'      => new sfValidatorBoolean(),
      'is_shared_pages'      => new sfValidatorBoolean(),
      'is_shared_members'    => new sfValidatorBoolean(),
      'is_shared_lists'      => new sfValidatorBoolean(),
      'is_shared_products'   => new sfValidatorBoolean(),
      'is_shared_movieclips' => new sfValidatorBoolean(),
      'is_shared_structure'  => new sfValidatorBoolean(),
    ));

    $this->widgetSchema->setNameFormat('sms_domain_link_group[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'SmsDomainLinkGroup';
  }

}
