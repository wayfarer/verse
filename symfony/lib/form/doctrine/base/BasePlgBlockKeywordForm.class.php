<?php

/**
 * PlgBlockKeyword form base class.
 *
 * @method PlgBlockKeyword getObject() Returns the current form's model object
 *
 * @package    verse3
 * @subpackage form
 * @author     Vladimir Droznik <vladimir.droznik@gmail.com>
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BasePlgBlockKeywordForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'        => new sfWidgetFormInputHidden(),
      'keyword'   => new sfWidgetFormInputText(),
      'domain_id' => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Domain'), 'add_empty' => true)),
    ));

    $this->setValidators(array(
      'id'        => new sfValidatorDoctrineChoice(array('model' => $this->getModelName(), 'column' => 'id', 'required' => false)),
      'keyword'   => new sfValidatorString(array('max_length' => 128, 'required' => false)),
      'domain_id' => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Domain'), 'required' => false)),
    ));

    $this->widgetSchema->setNameFormat('plg_block_keyword[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'PlgBlockKeyword';
  }

}
