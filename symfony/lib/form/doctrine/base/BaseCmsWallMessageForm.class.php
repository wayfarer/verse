<?php

/**
 * CmsWallMessage form base class.
 *
 * @method CmsWallMessage getObject() Returns the current form's model object
 *
 * @package    verse3
 * @subpackage form
 * @author     Vladimir Droznik <vladimir.droznik@gmail.com>
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseCmsWallMessageForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'wall_message_id' => new sfWidgetFormInputHidden(),
      'domain_id'       => new sfWidgetFormInputText(),
      'thread_id'       => new sfWidgetFormInputText(),
      'user_id'         => new sfWidgetFormInputText(),
      'name'            => new sfWidgetFormInputText(),
      'email'           => new sfWidgetFormInputText(),
      'message'         => new sfWidgetFormTextarea(),
      'created_at'      => new sfWidgetFormDateTime(),
      'reply_for_id'    => new sfWidgetFormInputText(),
      'level'           => new sfWidgetFormInputText(),
      'is_published'    => new sfWidgetFormInputText(),
      'lft'             => new sfWidgetFormInputText(),
      'rgt'             => new sfWidgetFormInputText(),
    ));

    $this->setValidators(array(
      'wall_message_id' => new sfValidatorDoctrineChoice(array('model' => $this->getModelName(), 'column' => 'wall_message_id', 'required' => false)),
      'domain_id'       => new sfValidatorInteger(),
      'thread_id'       => new sfValidatorInteger(),
      'user_id'         => new sfValidatorInteger(array('required' => false)),
      'name'            => new sfValidatorString(array('max_length' => 50, 'required' => false)),
      'email'           => new sfValidatorString(array('max_length' => 50, 'required' => false)),
      'message'         => new sfValidatorString(),
      'created_at'      => new sfValidatorDateTime(),
      'reply_for_id'    => new sfValidatorInteger(array('required' => false)),
      'level'           => new sfValidatorInteger(array('required' => false)),
      'is_published'    => new sfValidatorInteger(array('required' => false)),
      'lft'             => new sfValidatorInteger(),
      'rgt'             => new sfValidatorInteger(),
    ));

    $this->widgetSchema->setNameFormat('cms_wall_message[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'CmsWallMessage';
  }

}
