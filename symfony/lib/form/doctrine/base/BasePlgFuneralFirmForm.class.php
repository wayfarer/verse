<?php

/**
 * PlgFuneralFirm form base class.
 *
 * @method PlgFuneralFirm getObject() Returns the current form's model object
 *
 * @package    verse3
 * @subpackage form
 * @author     Vladimir Droznik <vladimir.droznik@gmail.com>
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BasePlgFuneralFirmForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'firm_id' => new sfWidgetFormInputHidden(),
      'name'    => new sfWidgetFormInputText(),
      'address' => new sfWidgetFormInputText(),
      'city'    => new sfWidgetFormInputText(),
      'state'   => new sfWidgetFormInputText(),
      'zip'     => new sfWidgetFormInputText(),
      'phone'   => new sfWidgetFormInputText(),
      'fax'     => new sfWidgetFormInputText(),
      'url'     => new sfWidgetFormInputText(),
      'county'  => new sfWidgetFormInputText(),
    ));

    $this->setValidators(array(
      'firm_id' => new sfValidatorDoctrineChoice(array('model' => $this->getModelName(), 'column' => 'firm_id', 'required' => false)),
      'name'    => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'address' => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'city'    => new sfValidatorString(array('max_length' => 64, 'required' => false)),
      'state'   => new sfValidatorString(array('max_length' => 2, 'required' => false)),
      'zip'     => new sfValidatorString(array('max_length' => 6, 'required' => false)),
      'phone'   => new sfValidatorString(array('max_length' => 16, 'required' => false)),
      'fax'     => new sfValidatorString(array('max_length' => 16, 'required' => false)),
      'url'     => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'county'  => new sfValidatorString(array('max_length' => 16, 'required' => false)),
    ));

    $this->widgetSchema->setNameFormat('plg_funeral_firm[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'PlgFuneralFirm';
  }

}
