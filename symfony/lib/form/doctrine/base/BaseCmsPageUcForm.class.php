<?php

/**
 * CmsPageUc form base class.
 *
 * @method CmsPageUc getObject() Returns the current form's model object
 *
 * @package    verse3
 * @subpackage form
 * @author     Vladimir Droznik <vladimir.droznik@gmail.com>
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseCmsPageUcForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'page_id'    => new sfWidgetFormInputHidden(),
      'domain_id'  => new sfWidgetFormInputText(),
      'page_type'  => new sfWidgetFormInputText(),
      'name'       => new sfWidgetFormInputText(),
      'no_header'  => new sfWidgetFormInputText(),
      'content'    => new sfWidgetFormTextarea(),
      'properties' => new sfWidgetFormTextarea(),
      'ord'        => new sfWidgetFormInputText(),
    ));

    $this->setValidators(array(
      'page_id'    => new sfValidatorDoctrineChoice(array('model' => $this->getModelName(), 'column' => 'page_id', 'required' => false)),
      'domain_id'  => new sfValidatorInteger(array('required' => false)),
      'page_type'  => new sfValidatorInteger(array('required' => false)),
      'name'       => new sfValidatorString(array('max_length' => 32, 'required' => false)),
      'no_header'  => new sfValidatorInteger(array('required' => false)),
      'content'    => new sfValidatorString(),
      'properties' => new sfValidatorString(),
      'ord'        => new sfValidatorInteger(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('cms_page_uc[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'CmsPageUc';
  }

}
