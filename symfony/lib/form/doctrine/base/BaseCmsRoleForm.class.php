<?php

/**
 * CmsRole form base class.
 *
 * @method CmsRole getObject() Returns the current form's model object
 *
 * @package    verse3
 * @subpackage form
 * @author     Vladimir Droznik <vladimir.droznik@gmail.com>
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseCmsRoleForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'role_id'   => new sfWidgetFormInputHidden(),
      'role_name' => new sfWidgetFormInputText(),
    ));

    $this->setValidators(array(
      'role_id'   => new sfValidatorDoctrineChoice(array('model' => $this->getModelName(), 'column' => 'role_id', 'required' => false)),
      'role_name' => new sfValidatorString(array('max_length' => 128, 'required' => false)),
    ));

    $this->widgetSchema->setNameFormat('cms_role[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'CmsRole';
  }

}
