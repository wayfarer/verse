<?php

/**
 * PlgObituaryCandle form base class.
 *
 * @method PlgObituaryCandle getObject() Returns the current form's model object
 *
 * @package    verse3
 * @subpackage form
 * @author     Vladimir Droznik <vladimir.droznik@gmail.com>
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BasePlgObituaryCandleForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'candle_id'      => new sfWidgetFormInputHidden(),
      'candle_case_id' => new sfWidgetFormInputText(),
      'domain_id'      => new sfWidgetFormInputHidden(),
      'obituary_id'    => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Obituary'), 'add_empty' => false)),
      'name'           => new sfWidgetFormInputText(),
      'email'          => new sfWidgetFormInputText(),
      'ip'             => new sfWidgetFormInputText(),
      'thoughts'       => new sfWidgetFormTextarea(),
      'timestamp'      => new sfWidgetFormDateTime(),
      'state'          => new sfWidgetFormInputText(),
      'hash'           => new sfWidgetFormInputText(),
      'created_at'     => new sfWidgetFormDateTime(),
      'updated_at'     => new sfWidgetFormDateTime(),
    ));

    $this->setValidators(array(
      'candle_id'      => new sfValidatorDoctrineChoice(array('model' => $this->getModelName(), 'column' => 'candle_id', 'required' => false)),
      'candle_case_id' => new sfValidatorString(array('max_length' => 16, 'required' => false)),
      'domain_id'      => new sfValidatorDoctrineChoice(array('model' => $this->getModelName(), 'column' => 'domain_id', 'required' => false)),
      'obituary_id'    => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Obituary'), 'required' => false)),
      'name'           => new sfValidatorString(array('max_length' => 128, 'required' => false)),
      'email'          => new sfValidatorString(array('max_length' => 64, 'required' => false)),
      'ip'             => new sfValidatorString(array('max_length' => 16, 'required' => false)),
      'thoughts'       => new sfValidatorString(array('required' => false)),
      'timestamp'      => new sfValidatorDateTime(array('required' => false)),
      'state'          => new sfValidatorInteger(array('required' => false)),
      'hash'           => new sfValidatorString(array('max_length' => 16, 'required' => false)),
      'created_at'     => new sfValidatorDateTime(),
      'updated_at'     => new sfValidatorDateTime(),
    ));

    $this->widgetSchema->setNameFormat('plg_obituary_candle[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'PlgObituaryCandle';
  }

}
