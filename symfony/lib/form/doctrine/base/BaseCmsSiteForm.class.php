<?php

/**
 * CmsSite form base class.
 *
 * @method CmsSite getObject() Returns the current form's model object
 *
 * @package    verse3
 * @subpackage form
 * @author     Vladimir Droznik <vladimir.droznik@gmail.com>
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseCmsSiteForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'domain_id'    => new sfWidgetFormInputHidden(),
      'title'        => new sfWidgetFormInputText(),
      'properties'   => new sfWidgetFormTextarea(),
      'css'          => new sfWidgetFormTextarea(),
      'header_mode'  => new sfWidgetFormInputText(),
      'header'       => new sfWidgetFormTextarea(),
      'main_menu'    => new sfWidgetFormTextarea(),
      'popup_menu'   => new sfWidgetFormTextarea(),
      'footer'       => new sfWidgetFormTextarea(),
      'google_stats' => new sfWidgetFormInputText(),
    ));

    $this->setValidators(array(
      'domain_id'    => new sfValidatorDoctrineChoice(array('model' => $this->getModelName(), 'column' => 'domain_id', 'required' => false)),
      'title'        => new sfValidatorString(array('max_length' => 128, 'required' => false)),
      'properties'   => new sfValidatorString(),
      'css'          => new sfValidatorString(array('required' => false)),
      'header_mode'  => new sfValidatorInteger(array('required' => false)),
      'header'       => new sfValidatorString(),
      'main_menu'    => new sfValidatorString(),
      'popup_menu'   => new sfValidatorString(),
      'footer'       => new sfValidatorString(array('required' => false)),
      'google_stats' => new sfValidatorString(array('max_length' => 20, 'required' => false)),
    ));

    $this->widgetSchema->setNameFormat('cms_site[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'CmsSite';
  }

}
