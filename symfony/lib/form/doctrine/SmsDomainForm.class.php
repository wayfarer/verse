<?php

/**
 * SmsDomain form.
 *
 * @package    verse3
 * @subpackage form
 * @author     Vladimir Droznik
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class SmsDomainForm extends BaseSmsDomainForm
{
  public function configure()
  {
    unset($this['server_id'], $this['space_consumed'], $this['price_enabled'], $this['affiliates_enabled'], $this['created_at'], $this['updated_at']);

    if($this->isNew()) {
      unset($this['mode']);
    }
    else {
      $this->widgetSchema['mode'] = new sfWidgetFormChoice(array(
        'choices' => SmsDomainTable::getDomainModes(),
        'multiple' => false
      ));
      $this->validatorSchema['mode'] = new sfValidatorChoice(array(
        'choices' => array_keys(SmsDomainTable::getDomainModes()),
        'multiple' => false
      ));
    }

    $this->widgetSchema['alias_domain_id'] = new sfWidgetFormDoctrineChoice(array(
      'model' => $this->getRelatedModelName('DomainAlias'),
      'query' => Doctrine::getTable('SmsDomain')->createQuery()->where('alias_domain_id = 0 AND enabled=1')->orderBy('domain_name'),
      'add_empty' => false));

    $this->widgetSchema['domain_type'] = new sfWidgetFormChoice(array(
      'choices'=>array('0'=>'normal', '1'=>'alias'),
      'expanded'=>true
    ));


    $this->validatorSchema['domain_type'] = new sfValidatorChoice(array(
      'choices'=>array(0,1)
    ));

    $this->validatorSchema['email'] = new sfValidatorEmail(array('required'=>false));

    $this->validatorSchema->setPostValidator(
      new sfValidatorAnd(array(
        new sfValidatorDoctrineUnique(array('model' => 'SmsDomain', 'column' => array('domain_name'))),
        new verseValidatorDoctrineUnique(array('model' => 'SmsDomain', 'column' => array('postfix'))),
        new verseDomainEmailValidator(),
      ))
    );

    if($this->getObject()->getAliasDomainId()==0) {
      $this->setDefault('domain_type', 0);
    }
    else {
      $this->setDefault('domain_type', 1);
    }
  }

/*  protected function doBind(array $values) {
    if(!$values['postfix']) {
      unset($values['postfix']);// = null;
    }
    var_dump($values);
    parent::doBind($values);
  }
*/

  public function updatePostfixColumn($value) {
    if($value=='') $value = null;
    return $value;
  }

  public function processValues($values) {
    $values = parent::processValues($values);
    if($values['domain_type']==0) {
      $values['alias_domain_id'] = '0';
    }
    return $values;
  }

  protected function doUpdateObject($values) {
    parent::doUpdateObject($values);
    if($this->isNew()) {
      // create domain
      VerseUtil::createDomainDirs(sfConfig::get('sf_web_dir').'/files/'.$values['domain_name']);
    }
    else {
      // update domain
      $modified = $this->getObject()->getModified(true);
      if(isset($modified['domain_name'])) {
        // domain name changed - rename corresponding dir or symlink
        @rename(sfConfig::get('sf_web_dir').'/files/'.$modified['domain_name'], sfConfig::get('sf_web_dir').'/files/'.$values['domain_name']);
      }
      if(isset($modified['alias_domain_id'])) {
        // domain type changed
        if($values['domain_type'] == 1) {
          // change from normal domain to alias
          // temporarily rename existing domain
          @rename(sfConfig::get('sf_web_dir').'/files/'.$values['domain_name'], sfConfig::get('sf_web_dir').'/files/'.$values['domain_name'].'--backup');
          @symlink(sfConfig::get('sf_web_dir').'/files/'.Doctrine_Core::getTable('SmsDomain')->find($values['alias_domain_id']), sfConfig::get('sf_web_dir').'/files/'.$values['domain_name']);
        }
        else {
          // change from alias to normal domain
          @unlink(sfConfig::get('sf_web_dir').'/files/'.$values['domain_name']);
          // for windows boxes
          // @rmdir(sfConfig::get('sf_web_dir').'/files/'.$values['domain_name']);
          // restore backed up stuff if exist
          if(file_exists(sfConfig::get('sf_web_dir').'/files/'.$values['domain_name'].'--backup')) {
            @rename(sfConfig::get('sf_web_dir').'/files/'.$values['domain_name'].'--backup', sfConfig::get('sf_web_dir').'/files/'.$values['domain_name']);
          }
          else {
            // create new dir
            VerseUtil::createDomainDirs(sfConfig::get('sf_web_dir').'/files/'.$values['domain_name']);
          }
        }
      }
    }
//    var_dump($modified);
//    var_dump($values);
//    var_dump($this->getObject()->getDomainName());
//    exit;
  }

}
