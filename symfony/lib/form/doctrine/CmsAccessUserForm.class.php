<?php

/**
 * CmsAccessUser form.
 *
 * @package    verse3
 * @subpackage form
 * @author     Vladimir Droznik
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class CmsAccessUserForm extends BaseCmsAccessUserForm
{
  public function configure() {
    unset($this['domain_id'], $this['webcasting'], $this['hash'], $this['created_at'], $this['updated_at'], $this['internal_name']);

    $this->widgetSchema['privileges'] = new sfWidgetFormChoice(array('choices'=>array(0=>'user', 1=>'moderator', 2=>'group')));
    $this->validatorSchema['privileges'] = new sfValidatorChoice(array('choices'=>array(0, 1, 2)));

    $this->widgetSchema['force_password_change'] = new sfWidgetFormChoice(array('choices'=>array(1=>'Force the user to change the password on first login', 0=>'Do not force the user to change the password')));
    $this->validatorSchema['force_password_change'] = new sfValidatorChoice(array('choices'=>array(0, 1)));

    $this->validatorSchema['email'] = new sfValidatorEmail(array('required'=>false));

    $this->widgetSchema['password'] = new sfWidgetFormInputPassword();

    $this->widgetSchema->setHelp('password', 'leave empty to keep old password');

    $this->widgetSchema['enabled'] = new sfWidgetFormChoice(array('choices' => CmsAccessUserTable::getUserStatuses()));
    $this->validatorSchema['enabled'] = new sfValidatorChoice(array('choices' => array_keys(CmsAccessUserTable::getUserStatuses())));
  }

  public function updateEmailColumn($value) {
    if($value=='') $value = null;
    return $value;
  }  
}
