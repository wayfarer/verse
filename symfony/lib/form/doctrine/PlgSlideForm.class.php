<?php

/**
 * PlgSlide form.
 *
 * @package    verse3
 * @subpackage form
 * @author     Vladimir Droznik
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class PlgSlideForm extends BasePlgSlideForm
{
  public function configure()
  {
    unset($this['domain_id'], $this['obituary_id']);

    $this->widgetSchema['image'] = new sfWidgetFormInputCKFinder(array('startupPath'=>'Images:/obituaries/scrap', 'configImages' => array('maxWidth' => 750, 'maxHeight' => 750)));
  }



}
