<?php

/**
 * PlgMovieclip form.
 *
 * @package    verse3
 * @subpackage form
 * @author     Vladimir Droznik
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class PlgMovieclipForm extends BasePlgMovieclipForm
{
  public function configure()
  {
    unset($this['domain_id'], $this['description'], $this['status'], $this['tmpfilename'], $this['timestamp']);

//    $this->validatorSchema['title']->setOption('required', true);

    $this->widgetSchema['filename'] = new sfWidgetFormInputSWFUpload(array('swfupload_file_types'=>'*.avi;*.mpg;*.flv;*.mov;*.wmv','swfupload_file_types_description'=>'Movie files', 'collapse_queue_on_init'=>false, 'swfupload_file_queue_limit'=>1, 'prevent_form_submit'=>true, 'swfupload_post_params'=>'su: "'.session_id().'"'));
    $this->validatorSchema['filename'] = new sfValidatorFile(array('path'=>sfConfig::get('verse_domain_tmp_dir'), 'validated_file_class'=>'verseValidatedMovieclip'));

    if(!$this->getObject()->isNew()) {
      $this->validatorSchema['filename']->setOption('required', false);
    }
  }

  public function updateOrigfilenameColumn($param) {
    if(isset($this->values['filename'])) {
      return $this->values['filename']->getOriginalName();
    }
  }

/*  public function updateTmpfilenameColumn() {
    return 'tmp/'.$this->values['filename']->getFilename();
  }
*/
//  public function updateFilenameColumn($filename) {
//    var_dump($filename);
//    exit;
//    $filename->setFilename('movies/'.$filename->getFilename());
//    return $filename;
//  }

/*  public function updateTimestampColumn() {
    return time();
  }
*/
}
