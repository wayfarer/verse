<?php

/**
 * PlgSubscribe form.
 *
 * @package    verse3
 * @subpackage form
 * @author     Vladimir Droznik
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class PlgSubscribeForm extends BasePlgSubscribeForm
{
  public function configure()
  {
  	unset($this['domain_id'], $this['created_at'], $this['updated_at'], $this['confirmation_cnt'], $this['hash']);

  	$this->validatorSchema['domain_id'] = new sfValidatorPass();
  }

  // needed for validator to work correctly
  protected function doBind(array $values) {
    $values['domain_id'] = Doctrine::getTable('SmsDomain')->getCurrent()->getDomainId();
    parent::doBind($values);
  }

}
