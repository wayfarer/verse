<?php

/**
 * CmsSite form.
 *
 * @package    verse3
 * @subpackage form
 * @author     Vladimir Droznik
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class CmsSiteForm extends BaseCmsSiteForm {
    public function configure() {
        unset($this['domain_id'], $this['title'], $this['properties'], $this['css'], $this['header_mode'], $this['header'], $this['main_menu'], $this['popup_menu'], $this['footer']);
    }
}
