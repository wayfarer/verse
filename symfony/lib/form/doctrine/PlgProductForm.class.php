<?php

/**
 * PlgProduct form.
 *
 * @package    verse3
 * @subpackage form
 * @author     Vladimir Droznik
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class PlgProductForm extends BasePlgProductForm
{
  public function configure()
  {
    unset($this['domain_id'], $this['guid'], $this['quantity'], $this['sold_times']);

    $this->widgetSchema['image'] = new sfWidgetFormInputCKFinder(array('startupPath'=>'Images:/merchandise'));

    // WYSIWYG for product description
    $this->widgetSchema['description'] = new sfWidgetFormTextareaTinyMCE(array(
      'width'  => 730,
      'height' => 370,
      'config' => '
        mode : "exact",
        theme : "advanced",
        theme_advanced_buttons1 : "bold,italic,underline,strikethrough,|,sub,sup,|,bullist,numlist,link,unlink,|,formatselect,|,undo,redo,|,pastetext,pasteword,|,code",
        theme_advanced_buttons2 : "",
        theme_advanced_buttons3 : "",
        theme_advanced_toolbar_location : "top",
        theme_advanced_toolbar_align : "left",
        plugins: "paste,inlinepopups",
        paste_auto_cleanup_on_paste : true
      ',
    ));
  }
  
  
}
