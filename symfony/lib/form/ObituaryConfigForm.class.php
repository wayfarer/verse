<?php

class ObituaryConfigForm extends BaseForm {
  public function configure() {
    $triple = array(''=>'Default', 'on'=>'On', 'off'=>'Off');
  	$this->setWidgets(array(
      'obituary_theme' => new sfWidgetFormChoice(array('choices'=>VerseUtil::getObituaryThemes(true))),
      'obituary_theme_image' => new sfWidgetFormChoice(array('choices'=>VerseUtil::getObituaryThemeImages(true))),
      'obituary_selected_theme_image' => new sfWidgetFormInputHidden(),
      'obituary_military_image' => new sfWidgetFormChoice(array('choices'=>VerseUtil::getObituaryMilitaryImages(true))),
      'obituary_selected_military_image' => new sfWidgetFormInputHidden(),
      'obituary_icons_theme' => new sfWidgetFormChoice(array('choices'=>VerseUtil::getObituaryIconsThemes(true))),

      'obituary_music' => new sfWidgetFormMusic(array('choices'=>VerseUtil::getMusicFiles(true))),

      'disable_candle_images' => new sfWidgetFormInputCheckbox(),

      //'military_status' => new sfWidgetFormInputCheckbox(),
      'disable_media' => new sfWidgetFormInputCheckbox(),
      'disable_google_maps' => new sfWidgetFormInputCheckbox(),

      'field_home_place_enabled' => new sfWidgetFormChoice(array('choices'=>$triple, 'expanded'=>true)),
      'field_death_date_enabled' => new sfWidgetFormChoice(array('choices'=>$triple, 'expanded'=>true)),
      'field_birth_place_enabled' => new sfWidgetFormChoice(array('choices'=>$triple, 'expanded'=>true)),
      'field_birth_date_enabled' => new sfWidgetFormChoice(array('choices'=>$triple, 'expanded'=>true)),
      'field_age_enabled' => new sfWidgetFormChoice(array('choices'=>$triple, 'expanded'=>true)),
      'field_service_enabled' => new sfWidgetFormChoice(array('choices'=>$triple, 'expanded'=>true)),
      'field_service_title' => new sfWidgetFormInputText(),
      'field_visitation_enabled' => new sfWidgetFormChoice(array('choices'=>$triple, 'expanded'=>true)),
      'field_interment_enabled' => new sfWidgetFormChoice(array('choices'=>$triple, 'expanded'=>true))
    ));
    $this->setValidators(array(
      'obituary_theme' => new sfValidatorChoice(array('choices'=>array_keys(VerseUtil::getObituaryThemes(true)), 'required'=>false)),
      'obituary_theme_image' => new sfValidatorChoice(array('choices'=>array_keys(VerseUtil::getAllObituaryThemeImages(true)), 'required'=>false)),
      'obituary_selected_theme_image' => new sfValidatorString(array('required' => false)),
      'obituary_military_image' => new sfValidatorChoice(array('choices'=>array_keys(VerseUtil::getAllObituaryMilitaryImages(true)), 'required'=>false)),
      'obituary_selected_military_image' => new sfValidatorString(array('required' => false)),
      'obituary_icons_theme' => new sfValidatorChoice(array('choices'=>array_keys(VerseUtil::getObituaryIconsThemes(true)), 'required'=>false)),

      'obituary_music' => new sfValidatorChoice(array('choices'=>array_keys(VerseUtil::getMusicFiles(true)))),

      'disable_candle_images' => new sfValidatorPass(),

      //'military_status' => new sfValidatorPass(),
      'disable_media' => new sfValidatorPass(),
      'disable_google_maps' => new sfValidatorPass(),

      'field_home_place_enabled' => new sfValidatorChoice(array('choices'=>array_keys($triple), 'required'=>false)),
      'field_death_date_enabled' => new sfValidatorChoice(array('choices'=>array_keys($triple), 'required'=>false)),
      'field_birth_place_enabled' => new sfValidatorChoice(array('choices'=>array_keys($triple), 'required'=>false)),
      'field_birth_date_enabled' => new sfValidatorChoice(array('choices'=>array_keys($triple), 'required'=>false)),
      'field_age_enabled' => new sfValidatorChoice(array('choices'=>array_keys($triple), 'required'=>false)),
      'field_service_enabled' => new sfValidatorChoice(array('choices'=>array_keys($triple), 'required'=>false)),
      'field_service_title' => new sfValidatorPass(),
      'field_visitation_enabled' => new sfValidatorChoice(array('choices'=>array_keys($triple), 'required'=>false)),
      'field_interment_enabled' => new sfValidatorChoice(array('choices'=>array_keys($triple), 'required'=>false))
    ));

    $this->widgetSchema->setLabels(array(
      'disable_media' => 'Disable Media',
      'disable_google_maps' => 'Disable Google Maps',
      'field_home_place_enabled' => 'Home',
      'field_death_date_enabled' => 'Date of Death',
      'field_birth_place_enabled' => 'Place of Birth',
      'field_birth_date_enabled' => 'Birthdate',
      'field_age_enabled' => 'Age',
      'field_service_enabled' => 'Service',
      'field_service_title' => 'Service title',
      'field_visitation_enabled' => 'Visitation',
      'field_interment_enabled' => 'Interment'
    ));

    $this->widgetSchema->setHelps(array(
      'field_service_title' => 'Use %s for service type word'
    ));

    $this->widgetSchema->setNameFormat('config[%s]');

//    $this->widgetSchema->setFormFormatterName('list');
  }

  public function setDefaults($defaults) {
    $defaults['obituary_selected_theme_image'] = isset($defaults['obituary_theme_image']) ? $defaults['obituary_theme_image'] : '';
    $defaults['obituary_selected_military_image'] = isset($defaults['obituary_military_image']) ? $defaults['obituary_military_image'] : '';

    parent::setDefaults($defaults);
  }

  protected function doBind($values) {
    unset($values['obituary_selected_theme_image'], $values['obituary_selected_military_image']);
    parent::doBind($values);
  }

  protected function doUpdateObject($values) {
    unset($values['obituary_selected_theme_image'], $values['obituary_selected_military_image']);
    parent::doUpdateObject($values);
  }

}
