<?php

class ObituaryUpdatesSubscribeForm extends sfFormSymfony {
  public function configure() {
    $obituary_config = Doctrine_Core::getTable('PlgObituary')->getCurrent()->getConfig();

    $this->setWidgets(array(
      'name' => new sfWidgetFormInputText(),
      'email' => new sfWidgetFormInputText(),
    ) );
    $this->setValidators(array(
      'name' => new sfValidatorString(),
      'email' => new sfValidatorEmail(),
      'domain_id' => new sfValidatorPass(),
      'obituary_id' => new sfValidatorPass(),
    ));

    if(!empty($obituary_config['captcha'])) {
/*
        $this->widgetSchema['captcha'] = new gyWidgetFormCaptcha(array('size' => 6, 'valid' => 2));
        $this->validatorSchema['captcha'] = new gyValidatorCaptcha();
*/
        $this->widgetSchema['captcha'] = new sfWidgetCaptchaGD();
        $this->validatorSchema['captcha'] = new sfCaptchaGDValidator();
        $this->widgetSchema->setLabels(array(
            'captcha' => 'Captcha (<span class="verse-obit-captcha-parenthesis">please enter all letters in uppercase</span>)'
        ));
    }

    $this->validatorSchema->setPostValidator(
      new sfValidatorDoctrineUnique(array('model' => 'PlgObituarySubscribe', 'column' => array('domain_id', 'obituary_id', 'email')),
                                    array('invalid' => 'You already subscribed for this obituary updates') )
    );

    $this->widgetSchema->setNameFormat('candle[%s]');

    $this->widgetSchema->setFormFormatterName('div');
  }

  protected function doBind(array $values) {
    $values['domain_id'] = Doctrine::getTable('SmsDomain')->getCurrent()->getDomainId();
    $values['obituary_id'] = Doctrine::getTable('PlgObituary')->getCurrent()->getObituaryId();
    parent::doBind($values);
  }

}