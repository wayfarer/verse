<?php

class ObituariesAddMediaForm extends sfFormSymfony {
    public function configure() {
        $obituary_config = Doctrine_Core::getTable('PlgObituary')->getCurrent()->getConfig();

        $upload_max_filesize = (verseMediaUtil::filesize2bytes(ini_get('upload_max_filesize')) > verseMediaUtil::filesize2bytes('30M')) ? '30M' : ini_get('upload_max_filesize');
        $this->widgetSchema['media'] = new sfWidgetFormInputSWFUpload(array(
            'swfupload_post_name' => 'media',
            'swfupload_file_types' => '*.jpg;*.jpeg;*.png;*.gif;*.avi;*.mpg;*.flv;*.mov;*.wmv;*.mp4',
            'swfupload_file_types_description' => 'Media files',
            'swfupload_file_size_limit' => $upload_max_filesize,
            'swfupload_upload_url' => '/frontend.php/online-obituary/'.sfContext::getInstance()->getRequest()->getParameter('obituary_id').'/upload-media',
            'swfupload_post_params' => 'su: "' . session_id() . '"',
            'swfupload_swfupload_loaded_handler' => 'onLoad',
            'swfupload_file_dialog_start_handler' => 'onStartFileDialog',
            'swfupload_file_dialog_complete_handler' => 'onCompleteFileDialog',
            'swfupload_button_image_url' => 'img/upload_4.png',
            'swfupload_button_width' => '35',
            'swfupload_button_height' => '39',
            'swfupload_button_text' => '',
            'swfupload_button_text_style' => '',
            'swfupload_button_text_left_padding' => '0',
            'swfupload_button_text_top_padding' => '0',
            'swfupload_file_queued_handler' => 'addFileQueued',
            'swfupload_upload_success_handler' => 'uploadSuccess',
            'swfupload_upload_progress_handler' => 'uploadProgress',
            'swfupload_upload_error_handler' => 'uploadError',
            'swfupload_file_queue_error_handler' => 'queueError',
            'swfupload_button_cursor' => 'SWFUpload.CURSOR.HAND',
        ));
        $this->validatorSchema['media'] = new verseMediaUploadValidator();
        $this->widgetSchema->setHelps(array(
            'media' => '<div class="upload-help-message">Max upload file size '.$upload_max_filesize.'</div>'
        ));

        if(!empty($obituary_config['captcha'])) {
/*
            $this->widgetSchema['captcha'] = new gyWidgetFormCaptcha(array('size' => 6, 'valid' => 2));
            $this->validatorSchema['captcha'] = new gyValidatorCaptcha();
*/
            $this->widgetSchema['captcha'] = new sfWidgetCaptchaGD();
            $this->validatorSchema['captcha'] = new sfCaptchaGDValidator();
            $this->widgetSchema->setLabels(array('captcha' => 'Captcha (<span class="verse-obit-captcha-parenthesis">please enter all letters in uppercase</span>)', 'media' => 'Browse to add files'));
        }

        $this->widgetSchema->setNameFormat('media[%s]');

        $this->widgetSchema->setFormFormatterName('div');
    }
}