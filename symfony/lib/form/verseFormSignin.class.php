<?php

class verseFormSignin extends sfGuardFormSignin
{
  public function configure()
  {
    $this->validatorSchema->setPostValidator(new verseValidatorUser());
  }
}
