<?php

class EcommerceConfigForm extends BaseForm {
  public function configure() {
    $this->setWidgets(array(
      'paypal_business' => new sfWidgetFormInputText(),
      'return_page_id' => new sfWidgetFormDoctrineChoice(array('model' => 'CmsPage', 'add_empty' => false)),
    ));
    $this->setValidators(array(
      'paypal_business' => new sfValidatorEmail(array('required'=>false)),
      'return_page_id' => new sfValidatorDoctrineChoice(array('model' => 'CmsPage', 'column' => 'page_id')),
    ));

    $this->widgetSchema->setLabels(array(
      'paypal_business' => 'Paypal Account'
    ));

    $this->widgetSchema->setHelp('paypal_business', 'Paypal account to receive payments to');

    $this->widgetSchema->setNameFormat('config[%s]');
  }
}