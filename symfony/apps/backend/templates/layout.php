<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
  <?php include_http_metas() ?>
  <?php include_metas() ?>
  <?php include_title() ?>
  <link rel="shortcut icon" href="/favicon.ico" />
  <?php include_javascripts() ?>
  <script type="text/javascript" src="/js/jmenu.js"></script>
  <script type="text/javascript">
    jQuery(function(){
      $('#verse-admin-menu').jmenu();
    });
  </script>
  <?php include_stylesheets() ?>
</head>
<body>
  <?php $current_route = $sf_context->getRouting()->getCurrentRouteName(); if($current_route=='homepage') $current_route='obituary'; $shared_product_domain_ids = Doctrine_Core::getTable('SmsDomain')->getCurrent()->getSharedProductDomainIds(); ?>
  <div id="container">
  <?php if ($sf_user->isAuthenticated()): ?>
    <div id="menu">
      <div class="inner">
      <ul id="verse-admin-menu">
      <?php if ($sf_user->hasCredential('Structure')): ?>
        <li><?php echo link_to('Site Structure', '/structure.php') ?></li>
      <?php endif ?>
      <?php if ($sf_user->hasCredential('Obituaries')): ?>
        <li><?php echo link_to('Obituaries', '@plg_obituary', array('class'=>((strpos($current_route, 'obituary')!==false || strpos($current_route, 'candle')!==false ||
                                                                               strpos($current_route, 'config')!==false || strpos($current_route, 'subscribe')!==false ||
                                                                               strpos($current_route, 'block_ip')!==false || strpos($current_route, 'block_keyword')!==false)?'current':''))) ?>
            <ul class="popup_menu">
                <li><?php echo link_to('Configure', '@config_obituary', array('class'=>((strpos($current_route, 'config')!==false)?'current':''))) ?></li>
                <li><?php echo link_to('Obits Subscriptions', '@subscribe', array('class'=>((strpos($current_route, 'subscribe')!==false)?'current':''))) ?></li>
                <li><?php echo link_to('Candles Approval', '/backend.php/candles/filter/action', array('query_string' => '_moderation_candles',
                                                                                                       'class'=>((strpos($current_route, 'candle')!==false)?'current':''))) ?></li>
                <li><?php echo link_to('Photos Approval', '/backend.php/moderate_photos/filter/action', array('query_string' => '_moderated_photos',
                                                                                                              'class'=>((strpos($current_route, 'obituary_image')!==false)?'current':''))) ?></li>
                <li><?php echo link_to('Movies Approval', '/backend.php/moderate_movie/filter/action', array('query_string' => '_moderated_movies',
                                                                                                             'class'=>((strpos($current_route, 'obituary_movie')!==false)?'current':''))) ?></li>
                <?php if ($sf_user->hasCredential('Obituaries')): ?>
                    <li><?php echo link_to('Block IP List', '/backend.php/block_ip', array('class'=>((strpos($current_route, 'block_ip')!==false)?'current':''))) ?></li>
                    <li><?php echo link_to('Block Keyword List', '/backend.php/block_keyword', array('class'=>((strpos($current_route, 'block_keyword')!==false)?'current':''))) ?></li>
                <?php endif; ?>
            </ul>
        </li>
      <?php endif ?>
      <?php if ($sf_user->hasCredential('Obituaries')): ?>
        <li><?php echo link_to('Events', '@event', array('class'=>(strpos($current_route, 'event')!==false?'current':''))) ?></li>
      <?php endif ?>
      <?php if ($sf_user->hasCredential('Products')): ?>
        <li><?php echo link_to('Products', '@product', array('class'=>(((strpos($current_route, 'product')!==false && $current_route !='domain_product') || strpos($current_route, 'ecommerce')!==false )?'current':''))) ?></li>
      <?php endif ?>
      <?php if ($sf_user->hasCredential('Products sublists') && !$sf_user->isSuperAdmin()): ?>
        <li><?php echo link_to('Products selection', '/sublists.php') ?></li>
      <?php endif ?>
      <?php if ($sf_user->hasCredential('Lists')): ?>
        <li><?php echo link_to('Lists', '@list', array('class'=>(strpos($current_route, 'list')!==false?'current':''))) ?></li>
      <?php endif ?>
      <?php if ($sf_user->hasCredential('Lists') && Doctrine_Core::getTable('SmsDomain')->getCurrent()->getDomainId() == 53): ?>
        <li><?php echo link_to('Funeral Directors', '/funeral_directors.php') ?></li>
      <?php endif ?>
      <?php if ($sf_user->hasCredential('Movieclips')): ?>
        <li class="endofgroup"><?php echo link_to('Movie Clips', '@movieclip', array('class'=>(strpos($current_route, 'movieclip')!==false?'current':''))) ?></li>
      <?php endif ?>
      <?php if ($sf_user->hasCredential('Users')): ?>
        <li><?php echo link_to('Users', '@sf_guard_user', array('class'=>(strpos($current_route, 'guard')!==false?'current':''))) ?></li>
      <?php endif ?>
      <?php if ($sf_user->hasCredential('Members')): ?>
        <li class="endofgroup"><?php echo link_to('Members', '@member', array('class'=>(strpos($current_route, 'member')!==false?'current':''))) ?></li>
      <?php endif ?>
	  <!-- Removed email feature 4/5/2013 WB-->
      <!--<?php if ($sf_user->hasCredential('Emails')): ?> 
        <li><?php echo link_to('Emails', '/emails.php') ?></li>
      <?php endif ?>-->
      <?php if ($sf_user->hasCredential('Statistics')): ?>
        <li class="endofgroup"><?php echo link_to('Statistics', '@statistics', array('class'=>(strpos($current_route, 'statistics')!==false?'current':''))) ?></li>
      <?php endif ?>
      <?php if ($sf_user->isSuperAdmin()): ?>
        <li class="endofgroup"><?php echo link_to('Domains', '@domain') ?></li>
      <?php endif ?>
      <?php if ($sf_user->isSuperAdmin()): ?>
        (<?php echo Doctrine::getTable('SmsDomain')->getCurrent()->getDomainId()?>)
      <?php endif ?>
      <?php if ($sf_user->isSuperAdmin()): ?>
        <li class="endofgroup"><?php echo link_to('Domain Groups', '@sms_domain_link_group') ?></li>
      <?php endif ?>
      <?php if ($sf_user->hasCredential('Products') && Doctrine_Core::getTable('SmsDomain')->getCurrent()->hasAccessToSharedProducts()): ?>
        <li><?php echo link_to('Shared Products', '@domain_product', array('class'=>(strpos($current_route, 'domain_product')!==false ?'current':''))) ?></li>
      <?php endif ?>
        <li style="float:right"><?php echo link_to('Logout', '@sf_guard_signout') ?></li>
      </ul>
      </div>
    </div>
  <div id="help"><a href="http://twintierstech.com/cms_manual_2010.pdf">
    <img width="48" height="48" title="Download CMS Manual" src="/img/documentation.png"/>
  </a></div>
  <div id="content">
    <?php echo $sf_content ?>
  </div>
  <?php else: ?>
    <?php echo $sf_content ?>
  <?php endif; ?>
  <div id="footer">
    <div class="in">
      &#169; Twin Tiers Technologies, Inc.<br/>
	  23 W Pulteney St. Suite 101 <br />
      Phone: 800-480-6467<br/>
      Fax: 607-962-4637
    </div>
	<div class="web">
      Website Support<br/>
      Phone: 800-480-6467 Option #3<br/>
	  Email: <a target="_blank" href="mailto:websupport@twintierstech.com">websupport@twintierstech.com</a>
    </div>
	<div class="mims">
      MIMS Support<br/>
      Phone: 800-480-6467 Option #2<br/>
	  Email: <a target="_blank" href="mailto:mimssupport@twintierstech.com">mimssupport@twintierstech.com</a>
    </div>
	<div class="billing">
      Billing<br/>
      Phone: 800-480-6467 Option #4<br/>
	  Email: <a target="_blank" href="mailto:billing@twintierstech.com">billing@twintierstech.com</a>
    </div>
	<div class="sales">
      Sales<br/>
      Phone: 800-480-6467 Option #1<br/>
	  Email: <a target="_blank" href="mailto:sales@twintierstech.com">sales@twintierstech.com</a>
    </div>
	
  </div>
  </div>
<script type="text/javascript">
  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-6375385-2']);
  _gaq.push(['_setDomainName', 'none']);
  _gaq.push(['_setAllowLinker', true]);
  _gaq.push(['_trackPageview']);
  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();
</script>
</body>
</html>
