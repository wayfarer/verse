<?php

class myUser extends sfGuardSecurityUser
{
  function setAuthenticated($authenticated) {
    $sms_user_uid = 'sms_useruid';
    $sms_user_data = 'sms_userudata';

    parent::setAuthenticated($authenticated);

    if($authenticated) {
      if($this->isSuperAdmin()) {
        $data['domain_id'] = 0;
      }
      else {
        $data['domain_id'] = Doctrine::getTable('SmsDomain')->getCurrent()->getDomainId();
      }
      $data['user_id'] = $this->getAttribute('user_id', null, 'sfGuardSecurityUser');
      $data['roles'] = array();
      foreach($this->getPermissions() as $permission) {
        $data['roles'][] = $permission->getId();
      }
      // set authentication status for old sms
      $_SESSION[$sms_user_uid] = $this->getUsername();
      $_SESSION[$sms_user_data] = $data;
    }
    else {
      unset($_SESSION[$sms_user_uid]);
      unset($_SESSION[$sms_user_data]);
    }
  }
}
