<?php

class backendConfiguration extends sfApplicationConfiguration
{
  public function configure()
  {
    $this->dispatcher->connect('admin.save_object', array('VerseUtil', 'onDomainUpdate'));
    $this->dispatcher->connect('admin.delete_object', array('VerseUtil', 'onAdminDeleteObject'));
    $this->dispatcher->connect('obituary.publish_candle', array('obituariesHelper', 'onCandlePublish'));
    $this->dispatcher->connect('obituary.create_obituary', array('obituariesHelper', 'onObituaryCreate'));
  }
}
