<?php use_helper('I18N', 'Date') ?>
<?php include_partial('affiliates/assets') ?>

<div id="sf_admin_container">
  <h1><?php echo __('Ecommerce :: Affiliates', array(), 'messages') ?></h1>

  <div id="verse_submenu">
    <?php echo link_to('products', '@product') ?> ::
    <?php echo link_to('categories', '@product_category') ?> ::
    <?php echo link_to('orders', '@product_order') ?> ::
    <?php echo link_to('affiliates', '@product_affiliate') ?>
  </div>

  <?php include_partial('affiliates/flashes') ?>

  <div id="sf_admin_header">
    <?php include_partial('affiliates/list_header', array('pager' => $pager)) ?>
  </div>

  <div id="sf_admin_bar">
    <?php include_partial('affiliates/filters', array('form' => $filters, 'configuration' => $configuration)) ?>
  </div>

  <div id="sf_admin_content">
    <form action="<?php echo url_for('product_affiliate_collection', array('action' => 'batch')) ?>" method="post">
    <ul class="sf_admin_actions">
      <?php include_partial('affiliates/list_batch_actions', array('helper' => $helper)) ?>
      <?php include_partial('affiliates/list_actions', array('helper' => $helper)) ?>
    </ul>
    <?php include_partial('affiliates/list', array('pager' => $pager, 'sort' => $sort, 'helper' => $helper)) ?>
    <ul class="sf_admin_actions">
      <?php include_partial('affiliates/list_batch_actions', array('helper' => $helper)) ?>
      <?php include_partial('affiliates/list_actions', array('helper' => $helper)) ?>
    </ul>
    </form>
  </div>

  <div id="sf_admin_footer">
    <?php include_partial('affiliates/list_footer', array('pager' => $pager)) ?>
  </div>
</div>
