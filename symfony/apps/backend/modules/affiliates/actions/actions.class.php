<?php

require_once dirname(__FILE__).'/../lib/affiliatesGeneratorConfiguration.class.php';
require_once dirname(__FILE__).'/../lib/affiliatesGeneratorHelper.class.php';

/**
 * affiliates actions.
 *
 * @package    verse3
 * @subpackage affiliates
 * @author     Vladimir Droznik
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class affiliatesActions extends autoAffiliatesActions
{
}
