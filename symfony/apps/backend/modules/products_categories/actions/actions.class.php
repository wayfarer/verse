<?php

require_once dirname(__FILE__).'/../lib/products_categoriesGeneratorConfiguration.class.php';
require_once dirname(__FILE__).'/../lib/products_categoriesGeneratorHelper.class.php';

/**
 * products_categories actions.
 *
 * @package    verse3
 * @subpackage products_categories
 * @author     Vladimir Droznik
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class products_categoriesActions extends autoProducts_categoriesActions
{
}
