<?php use_helper('I18N', 'Date') ?>
<?php include_partial('products_categories/assets') ?>

<div id="sf_admin_container">
  <h1><?php echo __('Ecommerce :: Products Categories', array(), 'messages') ?></h1>

  <div id="verse_submenu">
    <?php echo link_to('products', '@product') ?> ::
    <?php echo link_to('categories', '@product_category') ?> ::
    <?php echo link_to('orders', '@product_order') ?> ::
    <?php echo link_to('affiliates', '@product_affiliate') ?>
  </div>

  <?php include_partial('products_categories/flashes') ?>

  <div id="sf_admin_header">
    <?php include_partial('products_categories/list_header', array('pager' => $pager)) ?>
  </div>


  <div id="sf_admin_content">
    <form action="<?php echo url_for('product_category_collection', array('action' => 'batch')) ?>" method="post">
    <ul class="sf_admin_actions">
      <?php include_partial('products_categories/list_batch_actions', array('helper' => $helper)) ?>
      <?php include_partial('products_categories/list_actions', array('helper' => $helper)) ?>
    </ul>
    <?php include_partial('products_categories/list', array('pager' => $pager, 'sort' => $sort, 'helper' => $helper)) ?>
    <ul class="sf_admin_actions">
      <?php include_partial('products_categories/list_batch_actions', array('helper' => $helper)) ?>
      <?php include_partial('products_categories/list_actions', array('helper' => $helper)) ?>
    </ul>
    </form>
  </div>

  <div id="sf_admin_footer">
    <?php include_partial('products_categories/list_footer', array('pager' => $pager)) ?>
  </div>
</div>
