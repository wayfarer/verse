<?php

require_once dirname(__FILE__).'/../lib/membersGeneratorConfiguration.class.php';
require_once dirname(__FILE__).'/../lib/membersGeneratorHelper.class.php';

/**
 * members actions.
 *
 * @package    verse3
 * @subpackage members
 * @author     Vladimir Droznik
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class membersActions extends autoMembersActions
{
  public function executeBlock(sfWebRequest $request) {
    $this->getRoute()->getObject()->setEnabled(2)->notifyUser('block')->save();
    $this->redirect('@member');
  }

  public function executeUnblock(sfWebRequest $request) {
    $this->getRoute()->getObject()->setEnabled(1)->notifyUser('active')->save();
    $this->redirect('@member');
  }

  public function executeApprove(sfWebRequest $request) {
    $this->getRoute()->getObject()->setEnabled(1)->notifyUser('active')->save();
    $this->redirect('@member');
  }
}
