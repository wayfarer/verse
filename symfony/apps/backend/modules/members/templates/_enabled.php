<?php
  // $statuses = Doctrine::getTable('CmsAccessUser')->getUserStatuses();
  $statuses = CmsAccessUserTable::getUserStatuses();
  
  echo '<span style="color:';
  switch($cms_access_user->getEnabled()) {
    case 1:
      echo "#0A0";  // active
      break;
    case 2:
      echo "#000";  // blocked
      break;
    case 3:
      echo "#E00";  // new
      break;
    case 0:
      echo "#999";  // unconfirmed
      break;
  }
  echo '">';
  echo $statuses[$cms_access_user->getEnabled()];
  echo '</span>';
