<td>
  <ul class="sf_admin_td_actions">
    <?php echo $helper->linkToEdit($cms_access_user, array(  'params' =>   array(  ),  'class_suffix' => 'edit',  'label' => 'Edit',)) ?>
<?php if($cms_access_user->getEnabled()==3 || $cms_access_user->getEnabled()==0): ?>
    <li class="sf_admin_action_approve">
      <?php echo link_to(__('Approve', array(), 'messages'), 'members/approve?user_id='.$cms_access_user->getUserId(), array()) ?>
    </li>
<?php endif ?>
<?php if($cms_access_user->getEnabled()!=2): ?>
    <li class="sf_admin_action_block">
      <?php echo link_to(__('Block', array(), 'messages'), 'members/block?user_id='.$cms_access_user->getUserId(), array('confirm'=>'Are you sure you want to block the member? Member will be notified by email.')) ?>
    </li>
<?php endif ?>
<?php if($cms_access_user->getEnabled()==2): ?>
    <li class="sf_admin_action_unblock">
      <?php echo link_to(__('Unblock', array(), 'messages'), 'members/unblock?user_id='.$cms_access_user->getUserId(), array('confirm'=>'Are you sure you want to unblock the member? Member will be notified by email.')) ?>
    </li>
<?php endif ?>
    <?php echo $helper->linkToDelete($cms_access_user, array(  'params' =>   array(  ),  'confirm' => 'Are you sure?',  'class_suffix' => 'delete',  'label' => 'Delete',)) ?>
  </ul>
</td>
