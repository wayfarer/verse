<?php

/**
 * moderate_photos module configuration.
 *
 * @package    verse3
 * @subpackage moderate_photos
 * @author     Vladimir Droznik <vladimir.droznik@gmail.com>
 * @version    SVN: $Id: configuration.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class moderate_photosGeneratorConfiguration extends BaseModerate_photosGeneratorConfiguration
{
}
