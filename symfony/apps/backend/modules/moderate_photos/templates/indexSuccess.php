<link media="screen" type="text/css" href="/css/highslide.css" rel="stylesheet">
<!--[if lte IE 6 ]>
    <link media="screen" type="text/css" href="/css/highslide-ie6.css" rel="stylesheet">
<![endif]-->
<script type="text/javascript" src="/js/highslide-with-gallery.js"></script>
<script type="text/javascript">
    hs.graphicsDir = '/css/graphics/';
    hs.align = 'center';
    hs.transitions = ['expand', 'crossfade'];
    hs.wrapperClassName = 'dark borderless floating-caption';
    hs.fadeInOut = true;
    hs.dimmingOpacity = .75;
    // Add the controlbar
    if (hs.addSlideshow) hs.addSlideshow({
        interval: 5000,
        repeat: false,
        useControls: true,
        fixedControls: 'fit',
        overlayOptions: {
            opacity: .6,
            position: 'bottom center',
            hideOnMouseOut: true
        }
    });
</script>
<?php use_helper('I18N', 'Date') ?>
<?php include_partial('moderate_photos/assets') ?>

<div id="sf_admin_container">
    <h1><?php echo __('Photos List', array(), 'messages') ?></h1>

  <?php include_partial('moderate_photos/flashes') ?>

  <div id="sf_admin_header">
    <?php include_partial('moderate_photos/list_header', array('pager' => $pager)) ?>
      <?php echo link_to('<< back to obituaries', 'plg_obituary')?> ::
      <?php echo link_to('Photos List', '/backend.php/moderate_photos', array()) ?> ::
      <?php echo link_to('Movies List', '/backend.php/moderate_movie', array()) ?>
  </div>

  <div id="sf_admin_bar">
    <?php include_partial('moderate_photos/filters', array('form' => $filters, 'configuration' => $configuration)) ?>
  </div>

  <div id="sf_admin_content">
    <form action="<?php echo url_for('plg_obituary_image_collection', array('action' => 'batch')) ?>" method="post">
    <ul class="sf_admin_actions">
      <?php include_partial('moderate_photos/list_batch_actions', array('helper' => $helper)) ?>
      <?php include_partial('moderate_photos/list_actions', array('helper' => $helper)) ?>
    </ul>
    <?php include_partial('moderate_photos/list', array('pager' => $pager, 'sort' => $sort, 'helper' => $helper)) ?>
    <ul class="sf_admin_actions">
      <?php include_partial('moderate_photos/list_batch_actions', array('helper' => $helper)) ?>
      <?php include_partial('moderate_photos/list_actions', array('helper' => $helper)) ?>
    </ul>
    </form>
  </div>

  <div id="sf_admin_footer">
    <?php include_partial('moderate_photos/list_footer', array('pager' => $pager)) ?>
  </div>
</div>
