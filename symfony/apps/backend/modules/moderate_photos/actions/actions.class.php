<?php

require_once dirname(__FILE__).'/../lib/moderate_photosGeneratorConfiguration.class.php';
require_once dirname(__FILE__).'/../lib/moderate_photosGeneratorHelper.class.php';

/**
 * moderate_photos actions.
 *
 * @package    verse3
 * @subpackage moderate_photos
 * @author     Vladimir Droznik <vladimir.droznik@gmail.com>
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class moderate_photosActions extends autoModerate_photosActions {

    protected function processForm(sfWebRequest $request, sfForm $form) {
        $plg_obituary_image = $form->getObject();
        $is_new = $plg_obituary_image->isNew();

        $taintedValues = $request->getParameter($form->getName());
        if (!$is_new) {
            $taintedValues['obituary_id'] = $plg_obituary_image->getObituaryId();
            $taintedValues['path'] = $plg_obituary_image->getPath();
        }
        $form->bind($taintedValues, $request->getFiles($form->getName()));

        if ($form->isValid()) {
            $notice = $is_new ? 'The item was created successfully.' : 'The item was updated successfully.';

            if($is_new) {
                $domain = Doctrine::getTable('SmsDomain')->getCurrent();
                $image = new verseMediaFile('files/'.$domain->getDomainName().VerseUtil::leadingSlash($form->getValue('path')));
                $image->save($form->getValue('obituary_id'), $domain->getDomainName(), $domain->getDomainId(), $form->getValue('state'), $form->getValue('caption'));
            } else {
                try {
                    $image = $form->save();
                } catch (Doctrine_Validator_Exception $e) {

                    $errorStack = $form->getObject()->getErrorStack();

                    $message = get_class($form->getObject()) . ' has ' . count($errorStack) . " field" . (count($errorStack) > 1 ?  's' : null) . " with validation errors: ";
                    foreach ($errorStack as $field => $errors) {
                        $message .= "$field (" . implode(", ", $errors) . "), ";
                    }
                    $message = trim($message, ', ');

                    $this->getUser()->setFlash('error', $message);
                    return sfView::SUCCESS;
                }
            }

            if ($request->hasParameter('_save_and_add')) {
                $this->getUser()->setFlash('notice', $notice.' You can add another one below.');

                $this->redirect('@plg_obituary_image_new');
            } else {
                $this->getUser()->setFlash('notice', $notice);

                $this->redirect('@plg_obituary_image');
            }
        } else {
            $this->getUser()->setFlash('error', 'The item has not been saved due to some errors.', false);
        }
    }

    public function executeApprove(sfWebRequest $request) {
        $photo = $this->getRoute()->getObject();
        $photo->approve();
        $this->getUser()->setFlash('notice', 'The selected photo has been approved successfully.');
        $this->redirect('plg_obituary_image');
    }

    public function executeHide(sfWebRequest $request) {
        $photo = $this->getRoute()->getObject();
        $photo->hide();
        $this->getUser()->setFlash('notice', 'The selected photo has been hidden successfully.');
        $this->redirect('plg_obituary_image');
    }

    public function executeUnhide(sfWebRequest $request) {
        $photo = $this->getRoute()->getObject();
        $photo->unhide();
        $this->getUser()->setFlash('notice', 'The selected photo has been unhidden successfully.');
        $this->redirect('plg_obituary_image');
    }

    public function executeDelete(sfWebRequest $request) {
        $photo = $this->getRoute()->getObject();
        $file = new verseMediaFile('files/'.Doctrine::getTable('SmsDomain')->getCurrent()->getDomainName().'/'.$photo->getPath());
        $file->delete();
        $photo->delete();
        $this->getUser()->setFlash('notice', 'The selected photo has been deleted successfully.');
        $this->redirect('plg_obituary_image');
    }

    public function executeBatch(sfWebRequest $request) {
        $request->checkCSRFProtection();

        if (!$ids = $request->getParameter('ids'))
        {
          $this->getUser()->setFlash('error', 'You must at least select one item.');

          $this->redirect('plg_obituary_image');
        }

        $action = null;
        foreach($request->getParameter('batch_action') as $batch_action) {
          if($batch_action) {
            $action = $batch_action;
            break;
          }
        }
        if (!$action)
        {
          $this->getUser()->setFlash('error', 'You must select an action to execute on the selected items.');

          $this->redirect('plg_obituary_image');
        }

        if (!method_exists($this, $method = 'execute'.ucfirst($action)))
        {
          throw new InvalidArgumentException(sprintf('You must create a "%s" method for action "%s"', $method, $action));
        }

        if (!$this->getUser()->hasCredential($this->configuration->getCredentials($action)))
        {
          $this->forward(sfConfig::get('sf_secure_module'), sfConfig::get('sf_secure_action'));
        }

        $validator = new sfValidatorDoctrineChoice(array('multiple' => true, 'model' => 'PlgObituaryImage'));
        try
        {
            // validate ids
          $ids = $validator->clean($ids);

          // execute batch
          $this->$method($request);
        }
        catch (sfValidatorError $e)
        {
          $this->getUser()->setFlash('error', 'A problem occurs when deleting the selected items as some items do not exist anymore.');
        }

        $this->redirect('plg_obituary_image');
    } 

    public function executeBatchHide(sfWebRequest $request) {
        $ids = $request->getParameter('ids');
        $q = Doctrine::getTable('PlgObituaryImage')->createQuery()
          ->whereIn('id', $ids);
        foreach ($q->execute() as $photo) {
          $photo->hide();
        }
        $this->getUser()->setFlash('notice', 'The selected photos have been hidden successfully.');
        $this->redirect('plg_obituary_image');
    }

    public function executeBatchUnhide(sfWebRequest $request) {
        $ids = $request->getParameter('ids');
        $q = Doctrine::getTable('PlgObituaryImage')->createQuery()
          ->whereIn('id', $ids);
        foreach ($q->execute() as $photo) {
          $photo->unhide();
        }
        $this->getUser()->setFlash('notice', 'The selected photos have been unhidden successfully.');
        $this->redirect('plg_obituary_image');
    }

    public function executeBatchApprove(sfWebRequest $request) {
        $ids = $request->getParameter('ids');
        $q = Doctrine::getTable('PlgObituaryImage')->createQuery()
          ->whereIn('id', $ids);
        foreach ($q->execute() as $photo) {
          $photo->approve();
        }
        $this->getUser()->setFlash('notice', 'The selected photos have been approved successfully.');
        $this->redirect('plg_obituary_image');
    }

    public function executeBatchDelete(sfWebRequest $request) {
        $ids = $request->getParameter('ids');
        $q = Doctrine::getTable('PlgObituaryImage')->createQuery()
          ->whereIn('id', $ids);
        $domain_name = Doctrine::getTable('SmsDomain')->getCurrent()->getDomainName();
        foreach ($q->execute() as $photo) {
            $file = new verseMediaFile('files/'.$domain_name.'/'.$photo->getPath());
            $file->delete();
            $photo->delete();
        }
        $this->getUser()->setFlash('notice', 'The selected photos have been deleted successfully.');
        $this->redirect('plg_obituary_image');
    }

    protected function getFilters() {
        return $this->getUser()->getAttribute('moderate_media.filters', $this->configuration->getFilterDefaults(), 'admin_module');
    }

    protected function setFilters(array $filters) {
        return $this->getUser()->setAttribute('moderate_media.filters', $filters, 'admin_module');
    }

    public function executeFilter(sfWebRequest $request) {
        $this->setPage(1);

        if ($request->hasParameter('_reset')) {
            $this->setFilters($this->configuration->getFilterDefaults());
            $this->redirect('plg_obituary_image');
        }

        if($request->hasParameter('obituary')) {
            $this->getUser()->setAttribute('movie_obituary_id', $request->getParameter('obituary'));
            $this->getUser()->setAttribute('photo_obituary_id', $request->getParameter('obituary'));
            $filters = $this->configuration->getFilterDefaults();
            $filters['obituary_id'] = intval($request->getParameter('obituary'));
            $filters['state'] = 0;
            $this->setFilters($filters);
            $this->redirect('plg_obituary_image');
        }

        if($request->hasParameter('_active_photos')) {
            $filters = $this->getFilters();
            $filters['state'] = 0;
            $this->setFilters($filters);
            $this->redirect('plg_obituary_image');
        }

        if($request->hasParameter('_moderated_photos')) {
            $filters = $this->configuration->getFilterDefaults();
            $filters['state'] = 1;
            unset($filters['obituary_id']);
            $this->getUser()->setAttribute('movie_obituary_id', '');
            $this->getUser()->setAttribute('photo_obituary_id', '');
            $this->setFilters($filters);
            $this->redirect('plg_obituary_image');
        }

        if($request->hasParameter('_hidden_photos')) {
            $filters = $this->getFilters();
            $filters['state'] = 2;
            $this->setFilters($filters);
            $this->redirect('plg_obituary_image');
        }

        $this->filters = $this->configuration->getFilterForm($this->getFilters());

        $this->filters->bind($request->getParameter($this->filters->getName()));
        if ($this->filters->isValid()) {
            $this->setFilters($this->filters->getValues());
            $this->redirect('plg_obituary_image');
        }

        $this->pager = $this->getPager();
        $this->sort = $this->getSort();

        $this->setTemplate('index');
    }
}
