<?php

/**
 * domain_product module configuration.
 *
 * @package    verse3
 * @subpackage domain_product
 * @author     Vladimir Droznik <vladimir.droznik@gmail.com>
 * @version    SVN: $Id: configuration.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class domain_productGeneratorConfiguration extends BaseDomain_productGeneratorConfiguration
{
}
