<?php

require_once dirname(__FILE__).'/../lib/domain_productGeneratorConfiguration.class.php';
require_once dirname(__FILE__).'/../lib/domain_productGeneratorHelper.class.php';

/**
 * domain_product actions.
 *
 * @package    verse3
 * @subpackage domain_product
 * @author     Vladimir Droznik <vladimir.droznik@gmail.com>
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class domain_productActions extends autoDomain_productActions {

    public function executeIndex(sfWebRequest $request) {
        if (Doctrine_Core::getTable('SmsDomain')->getCurrent()->hasAccessToSharedProducts()) {
            parent::executeIndex($request);
        } else {
            $this->redirect('@product');
        }
    }

    public function executeNew() {
        $this->redirect('@domain_product');
    }

    public function executeEdit() {
        $this->redirect('@domain_product');
    }

    public function executeDelete() {
        $this->redirect('@domain_product');
    }

    public function executeEnable() {
        $domain_product = new SmsDomainLinkProduct();
        $domain_product->setProductId($this->getRoute()->getObject()->getProductId());
        $domain_product->setDomainId(Doctrine::getTable('SmsDomain')->getCurrent()->getDomainId());
        $domain_product->save();
        $this->getUser()->setFlash('notice', 'Product has been enabled successfully.');
        $this->redirect('@domain_product');
    }

    public function executeDisable() {
        $product_id = $this->getRoute()->getObject()->getProductId();
        $domain_product = Doctrine::getTable('SmsDomainLinkProduct')->createQuery()->where('domain_id = ? AND product_id = ?', array(Doctrine::getTable('SmsDomain')->getCurrent()->getDomainId(), $product_id))->fetchOne();
        $domain_product->delete();
        $this->getUser()->setFlash('notice', 'Product has been disabled successfully.');
        $this->redirect('@domain_product');
    }

    public function executeBatchEnable(sfWebRequest $request) {
        $product_ids = $request->getParameter('ids');
        $domain_id = Doctrine::getTable('SmsDomain')->getCurrent()->getDomainId();

        foreach ($product_ids as $product_id) {
            $domain_product = new SmsDomainLinkProduct();
            $domain_product->setProductId($product_id);
            $domain_product->setDomainId($domain_id);
            $domain_product->save();
        }

        $this->getUser()->setFlash('notice', 'The selected products have been enabled successfully.');

        $this->redirect('@domain_product');
    }

    public function executeBatchDisable(sfWebRequest $request) {
        $product_ids = $request->getParameter('ids');

        Doctrine::getTable('SmsDomainLinkProduct')->createQuery()->delete()->where('domain_id = ?', Doctrine::getTable('SmsDomain')->getCurrent()->getDomainId())->andWhereIn('product_id', $product_ids)->execute();

        $this->getUser()->setFlash('notice', 'The selected products have been disabled successfully.');

        $this->redirect('@domain_product');
    }

    protected function getFilters() {
        return $this->getUser()->getAttribute('domain_product.filters', $this->configuration->getFilterDefaults(), 'admin_module');
    }

    protected function setFilters(array $filters) {
        return $this->getUser()->setAttribute('domain_product.filters', $filters, 'admin_module');
    }

    public function executeFilter(sfWebRequest $request) {
        $this->setPage(1);

        if ($request->hasParameter('_reset')) {
            $this->setFilters($this->configuration->getFilterDefaults());

            $this->redirect('@domain_product');
        }

        $this->filters = $this->configuration->getFilterForm($this->getFilters());

        $this->filters->bind($request->getParameter($this->filters->getName()));
        if ($this->filters->isValid()) {
            $this->setFilters($this->filters->getValues());

            $this->redirect('@domain_product');
        }

        $this->pager = $this->getPager();
        $this->sort = $this->getSort();

        $this->setTemplate('index');
    }
}
