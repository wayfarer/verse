<?php
  use_helper('Thumbnail');

  $image = $plg_product->getImage();
  if($image) {
    $image = thumbnail($plg_product->getDomainName().VerseUtil::leadingSlash($image));
  }
  else {
    $image = 'img/noimage-tn.jpg';
  }
  echo '<img src="'.VerseUtil::leadingSlash($image).'"/>';
