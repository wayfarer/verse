<td>
  <ul class="sf_admin_td_actions">
<?php if( !$plg_product->getProductEnabled() ): ?>
    <li class="sf_admin_action_enable">
      <?php echo link_to(__('Enable', array(), 'messages'), 'domain_product/enable?product_id='.$plg_product->getProductId(), array()) ?>
    </li>
<?php else: ?>
    <li class="sf_admin_action_disable">
      <?php echo link_to(__('Disable', array(), 'messages'), 'domain_product/disable?product_id='.$plg_product->getProductId(), array()) ?>
    </li>
<?php endif; ?>
  </ul>
</td>
