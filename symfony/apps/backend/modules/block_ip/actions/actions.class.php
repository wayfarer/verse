<?php

require_once dirname(__FILE__).'/../lib/block_ipGeneratorConfiguration.class.php';
require_once dirname(__FILE__).'/../lib/block_ipGeneratorHelper.class.php';

/**
 * block_ip actions.
 *
 * @package    verse3
 * @subpackage block_ip
 * @author     Vladimir Droznik <vladimir.droznik@gmail.com>
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class block_ipActions extends autoBlock_ipActions {
    
}
