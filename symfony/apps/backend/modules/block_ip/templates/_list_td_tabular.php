<td class="sf_admin_text sf_admin_list_td_ip">
  <?php echo $plg_block_ip->getIp() ?>
</td>
<td class="sf_admin_date sf_admin_list_td_created_at">
  <?php echo false !== strtotime($plg_block_ip->getCreatedAt()) ? format_date($plg_block_ip->getCreatedAt(), "yyyy-MM-dd") : '&nbsp;' ?>
</td>
<td class="sf_admin_date sf_admin_list_td_updated_at">
  <?php echo false !== strtotime($plg_block_ip->getUpdatedAt()) ? format_date($plg_block_ip->getUpdatedAt(), "yyyy-MM-dd") : '&nbsp;' ?>
</td>
<td class="sf_admin_text sf_admin_list_td_count">
  <?php echo $plg_block_ip->getCount() ?>
</td>
<?php if (sfContext::getInstance()->getUser()->isSuperAdmin()): ?>
    <td class="sf_admin_text sf_admin_list_td_domain_name">
        <?php echo $plg_block_ip->getDomainName() ?>
    </td>
<?php endif; ?>