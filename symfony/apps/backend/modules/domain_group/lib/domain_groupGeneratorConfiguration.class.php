<?php

/**
 * domain_group module configuration.
 *
 * @package    verse3
 * @subpackage domain_group
 * @author     Vladimir Droznik <vladimir.droznik@gmail.com>
 * @version    SVN: $Id: configuration.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class domain_groupGeneratorConfiguration extends BaseDomain_groupGeneratorConfiguration
{
}
