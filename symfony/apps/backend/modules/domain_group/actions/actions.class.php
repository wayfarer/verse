<?php

require_once dirname(__FILE__).'/../lib/domain_groupGeneratorConfiguration.class.php';
require_once dirname(__FILE__).'/../lib/domain_groupGeneratorHelper.class.php';

/**
 * domain_group actions.
 *
 * @package    verse3
 * @subpackage domain_group
 * @author     Vladimir Droznik <vladimir.droznik@gmail.com>
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class domain_groupActions extends autoDomain_groupActions {

    protected function processForm(sfWebRequest $request, sfForm $form) {
        $form->bind($request->getParameter($form->getName()), $request->getFiles($form->getName()));
        if ($form->isValid()) {
            $domains_in_anoother_groups_with_shared_products = $form->getObject()->getDomainsInAnotherGroupsWithSharedProducts(explode(';', $form->getValue('domain_ids')));
            if (!count($domains_in_anoother_groups_with_shared_products) || !$form->getValue('is_shared_products')) {
                $notice = $form->getObject()->isNew() ? 'The item was created successfully.' : 'The item was updated successfully.';

                try {
                    $sms_domain_link_group = $form->save();
                } catch (Doctrine_Validator_Exception $e) {

                    $errorStack = $form->getObject()->getErrorStack();

                    $message = get_class($form->getObject()) . ' has ' . count($errorStack) . " field" . (count($errorStack) > 1 ? 's' : null) . " with validation errors: ";
                    foreach ($errorStack as $field => $errors) {
                        $message .= "$field (" . implode(", ", $errors) . "), ";
                    }
                    $message = trim($message, ', ');

                    $this->getUser()->setFlash('error', $message);
                    return sfView::SUCCESS;
                }

                $this->dispatcher->notify(new sfEvent($this, 'admin.save_object', array('object' => $sms_domain_link_group)));

                if ($request->hasParameter('_save_and_add')) {
                    $this->getUser()->setFlash('notice', $notice . ' You can add another one below.');

                    $this->redirect('@sms_domain_link_group_new');
                } else {
                    $this->getUser()->setFlash('notice', $notice);

                    $this->redirect('@sms_domain_link_group');
                }
            } else {
                $domains_message = 'There are domains which have been already added to another domain group with shared products:';
                foreach ($domains_in_anoother_groups_with_shared_products as $domain) {
                    $domains_message .= ' domain "'.$domain['domain_name'].'" - "'.$domain['group_name'].'" domain group;';

                }
                $this->getUser()->setFlash('error', $domains_message, false);
            }
        } else {
            $this->getUser()->setFlash('error', 'The item has not been saved due to some errors.', false);
        }
    }

    public function executeGetGroupDomainIds(sfWebRequest $request) {
        $group_id = $request->getParameter('id', null);
        $domain_ids = Doctrine::getTable('SmsDomainLink')->getGroupDomainIds($group_id);
        echo json_encode($domain_ids);
        $this->setLayout(false);
        return sfView::NONE;
    }

    public function executeGetDomainPages(sfWebRequest $request) {
        $domain_id = $request->getParameter('domain_id', null);
        $pages = Doctrine::getTable('CmsPage')->createQuery()->select('page_id, internal_name')->where('domain_id = ?', $domain_id)->orderBy('internal_name')->fetchArray();

        $domain_pages = array();
        foreach($pages as $page) {
            $domain_pages[$page['page_id']] = $page['internal_name'];
        }

        echo json_encode($domain_pages);
        $this->setLayout(false);
        return sfView::NONE;
    }

    public function executeGetGroupSharedPageIds(sfWebRequest $request) {
        $group_id = $request->getParameter('id', null);
        $page_ids = Doctrine::getTable('SmsDomainLinkPage')->getSharedPageIds($group_id);
        if(count($page_ids)) {
            echo json_encode($page_ids);
        } else {
            echo json_encode(array(), JSON_FORCE_OBJECT);
        }

        $this->setLayout(false);
        return sfView::NONE;
    }
}
