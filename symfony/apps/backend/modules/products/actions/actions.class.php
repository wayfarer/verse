<?php

require_once dirname(__FILE__).'/../lib/productsGeneratorConfiguration.class.php';
require_once dirname(__FILE__).'/../lib/productsGeneratorHelper.class.php';

/**
 * products actions.
 *
 * @package    verse3
 * @subpackage products
 * @author     Vladimir Droznik
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class productsActions extends autoProductsActions
{
  public function executeIndex(sfWebRequest $request) {
    parent::executeIndex($request);
    $this->config = Doctrine::getTable('PlgProduct')->loadConfig();
  }

  public function executeEcommerceConfig(sfWebRequest $request) {
    $this->form = new EcommerceConfigForm();
    if ($request->isMethod('post')) {
      $this->form->bind($request->getParameter('config'));
      if ($this->form->isValid()) {
        Doctrine::getTable('PlgProduct')->saveConfig($this->form->getValues());

        $this->getUser()->setFlash('notice', 'Ecommerce configuration saved');
        $this->redirect('@product');
      }
    }
    else {
      $this->form->setDefaults(Doctrine::getTable('PlgProduct')->loadConfig());
    }
  }

  public function executeEnable() {
    $product = $this->getRoute()->getObject();
    $product->enable();
    $this->redirect('@product');
  }

  public function executeDisable() {
    $product = $this->getRoute()->getObject();
    $product->disable();
    $this->redirect('@product');
  }

  public function executeBatchEnable(sfWebRequest $request) {
    $ids = $request->getParameter('ids');

    $q = Doctrine::getTable('PlgProduct')->createQuery()
      ->whereIn('product_id', $ids);

    foreach ($q->execute() as $product) {
      $product->enable();
    }

    $this->getUser()->setFlash('notice', 'The selected products have been enabled successfully.');

    $this->redirect('@product');
  }

  public function executeBatchDisable(sfWebRequest $request) {
    $ids = $request->getParameter('ids');

    $q = Doctrine::getTable('PlgProduct')->createQuery()
      ->whereIn('product_id', $ids);

    foreach ($q->execute() as $product) {
      $product->disable();
    }

    $this->getUser()->setFlash('notice', 'The selected products have been disabled successfully.');

    $this->redirect('@product');
  }
}
