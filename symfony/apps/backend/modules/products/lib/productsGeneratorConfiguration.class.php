<?php

/**
 * products module configuration.
 *
 * @package    verse3
 * @subpackage products
 * @author     Vladimir Droznik
 * @version    SVN: $Id: configuration.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class productsGeneratorConfiguration extends BaseProductsGeneratorConfiguration
{
}
