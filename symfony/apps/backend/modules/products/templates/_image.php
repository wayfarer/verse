<?php
  use_helper('Thumbnail');

  $image = $plg_product->getImage();
  if($image) {
    $image = thumbnail($image);
  }
  else {
    $image = '/img/noimage-tn.jpg';
  }
  echo image_tag($image);
