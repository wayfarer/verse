<div id="sf_admin_container">
<h1>Ecommerce Configuration</h1>
<form action="<?php echo url_for('@config_ecommerce')?>" method="post">
  <table>
    <?php echo $form ?>
  </table>
  <ul class="sf_admin_actions">
    <li><?php echo link_to('Back', '@product')?></li>
    <li><input type="submit" value="Save" /></li>
  </ul>
</form>
</div>