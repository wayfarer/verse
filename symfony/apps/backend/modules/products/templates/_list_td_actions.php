<td>
  <ul class="sf_admin_td_actions">
<?php if($plg_product->getEnabled()==0): ?>
    <li class="sf_admin_action_enable">
      <?php echo link_to(__('Enable', array(), 'messages'), 'products/enable?product_id='.$plg_product->getProductId(), array()) ?>
    </li>
<?php endif ?>
<?php if($plg_product->getEnabled()==1): ?>
    <li class="sf_admin_action_disable">
      <?php echo link_to(__('Disable', array(), 'messages'), 'products/disable?product_id='.$plg_product->getProductId(), array()) ?>
    </li>
<?php endif ?>
    <?php echo $helper->linkToEdit($plg_product, array(  'params' =>   array(  ),  'class_suffix' => 'edit',  'label' => 'Edit',)) ?>
    <?php echo $helper->linkToDelete($plg_product, array(  'params' =>   array(  ),  'confirm' => 'Are you sure?',  'class_suffix' => 'delete',  'label' => 'Delete',)) ?>
  </ul>
</td>
