<?php

/**
 * scrapbook2 actions.
 *
 * @package    verse3
 * @subpackage scrapbook2
 * @author     Vladimir Droznik
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class scrapbook2Actions extends sfActions
{
  public function executeIndex(sfWebRequest $request)
  {
    $this->plg_slides = Doctrine::getTable('PlgSlide')
      ->createQuery('a')
      ->execute();
  }

  public function executeNew(sfWebRequest $request)
  {
    $this->form = new PlgSlideForm();
  }

  public function executeCreate(sfWebRequest $request)
  {
    $this->forward404Unless($request->isMethod(sfRequest::POST));

    $this->form = new PlgSlideForm();

    $this->processForm($request, $this->form);

    $this->setTemplate('new');
  }

  public function executeEdit(sfWebRequest $request)
  {
    $this->forward404Unless($plg_slide = Doctrine::getTable('PlgSlide')->find(array($request->getParameter('slide_id'),
                          $request->getParameter('domain_id'),
                          $request->getParameter('obituary_id'))), sprintf('Object plg_slide does not exist (%s).', $request->getParameter('slide_id'),
                          $request->getParameter('domain_id'),
                          $request->getParameter('obituary_id')));
    $this->form = new PlgSlideForm($plg_slide);
  }

  public function executeUpdate(sfWebRequest $request)
  {
    $this->forward404Unless($request->isMethod(sfRequest::POST) || $request->isMethod(sfRequest::PUT));
    $this->forward404Unless($plg_slide = Doctrine::getTable('PlgSlide')->find(array($request->getParameter('slide_id'),
                          $request->getParameter('domain_id'),
                          $request->getParameter('obituary_id'))), sprintf('Object plg_slide does not exist (%s).', $request->getParameter('slide_id'),
                          $request->getParameter('domain_id'),
                          $request->getParameter('obituary_id')));
    $this->form = new PlgSlideForm($plg_slide);

    $this->processForm($request, $this->form);

    $this->setTemplate('edit');
  }

  public function executeDelete(sfWebRequest $request)
  {
    $request->checkCSRFProtection();

    $this->forward404Unless($plg_slide = Doctrine::getTable('PlgSlide')->find(array($request->getParameter('slide_id'),
                          $request->getParameter('domain_id'),
                          $request->getParameter('obituary_id'))), sprintf('Object plg_slide does not exist (%s).', $request->getParameter('slide_id'),
                          $request->getParameter('domain_id'),
                          $request->getParameter('obituary_id')));
    $plg_slide->delete();

    $this->redirect('scrapbook2/index');
  }

  protected function processForm(sfWebRequest $request, sfForm $form)
  {
    $form->bind($request->getParameter($form->getName()), $request->getFiles($form->getName()));
    if ($form->isValid())
    {
      $plg_slide = $form->save();

      $this->redirect('scrapbook2/edit?slide_id='.$plg_slide->getSlideId().'&domain_id='.$plg_slide->getDomainId().'&obituary_id='.$plg_slide->getObituaryId());
    }
  }
}
