<?php

require_once dirname(__FILE__).'/../lib/subscription_candlesGeneratorConfiguration.class.php';
require_once dirname(__FILE__).'/../lib/subscription_candlesGeneratorHelper.class.php';

/**
 * subscription_candles actions.
 *
 * @package    verse3
 * @subpackage subscription_candles
 * @author     Vladimir Droznik <vladimir.droznik@gmail.com>
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class subscription_candlesActions extends autoSubscription_candlesActions {

    public function executeFilter(sfWebRequest $request) {
        $this->setPage(1);

        if ($request->hasParameter('_reset')) {
            $this->setFilters($this->configuration->getFilterDefaults());
            $this->redirect('@plg_obituary_subscribe');
        }

        if($request->hasParameter('obituary')) {
            $filters = $this->configuration->getFilterDefaults();
            $filters['obituary_id'] = intval($request->getParameter('obituary'));
            $filters['enabled'] = 1;
            $this->setFilters($filters);
            $this->redirect('@plg_obituary_subscribe');
        }

        $this->filters = $this->configuration->getFilterForm($this->getFilters());

        $this->filters->bind($request->getParameter($this->filters->getName()));
        if ($this->filters->isValid()) {
            $this->setFilters($this->filters->getValues());
            $this->redirect('@plg_obituary_subscribe');
        }

        $this->pager = $this->getPager();
        $this->sort = $this->getSort();

        $this->setTemplate('index');
    }

    public function executeBatch(sfWebRequest $request) {
        $request->checkCSRFProtection();

        if (!$ids = $request->getParameter('ids')) {
            $this->getUser()->setFlash('error', 'You must at least select one item.');
            $this->redirect('@plg_obituary_subscribe');
        }

        $action = null;
        foreach($request->getParameter('batch_action') as $batch_action) {
            if($batch_action) {
                $action = $batch_action;
                break;
            }
        }
        if (!$action) {
            $this->getUser()->setFlash('error', 'You must select an action to execute on the selected items.');

            $this->redirect('@plg_obituary_subscribe');
        }

        if (!method_exists($this, $method = 'execute'.ucfirst($action))) {
            throw new InvalidArgumentException(sprintf('You must create a "%s" method for action "%s"', $method, $action));
        }

        if (!$this->getUser()->hasCredential($this->configuration->getCredentials($action))) {
            $this->forward(sfConfig::get('sf_secure_module'), sfConfig::get('sf_secure_action'));
        }

        $validator = new sfValidatorDoctrineChoice(array('multiple' => true, 'model' => 'PlgObituarySubscribe'));
        try {
            // validate ids
            $ids = $validator->clean($ids);

            // execute batch
            $this->$method($request);
        }
        catch (sfValidatorError $e) {
            $this->getUser()->setFlash('error', 'A problem occurs when deleting the selected items as some items do not exist anymore.');
        }

        $this->redirect('@plg_obituary_subscribe');
    }

    public function executeDelete(sfWebRequest $request) {
        $request->checkCSRFProtection();

        $this->dispatcher->notify(new sfEvent($this, 'admin.delete_object', array('object' => $this->getRoute()->getObject())));

        if ($this->getRoute()->getObject()->delete()) {
            $this->getUser()->setFlash('notice', 'The item was deleted successfully.');
        }

        $this->redirect('@plg_obituary_subscribe');
    }

    protected function executeBatchDelete(sfWebRequest $request) {
        $ids = $request->getParameter('ids');

        // TODO: query doesn't take in account domain_id and obituary_id, investigate and rewrite it
        $records = Doctrine_Query::create()
                    ->from('PlgObituarySubscribe')
                    ->andWhereIn('obituary_subscribe_id', $ids)
                    ->execute();

        foreach ($records as $record) {
            $record->delete();
        }

        $this->getUser()->setFlash('notice', 'The selected items have been deleted successfully.');
        $this->redirect('@plg_obituary_subscribe');
    }

    protected function processForm(sfWebRequest $request, sfForm $form) {
        $form->bind($request->getParameter($form->getName()), $request->getFiles($form->getName()));
        if ($form->isValid()) {
            $notice = $form->getObject()->isNew() ? 'The item was created successfully.' : 'The item was updated successfully.';

            try {
                $plg_obituary_subscribe = $form->save();
            } catch (Doctrine_Validator_Exception $e) {
                $errorStack = $form->getObject()->getErrorStack();

                $message = get_class($form->getObject()) . ' has ' . count($errorStack) . " field" . (count($errorStack) > 1 ?  's' : null) . " with validation errors: ";
                foreach ($errorStack as $field => $errors) {
                    $message .= "$field (" . implode(", ", $errors) . "), ";
                }
                $message = trim($message, ', ');

                $this->getUser()->setFlash('error', $message);
                return sfView::SUCCESS;
            }

            $this->dispatcher->notify(new sfEvent($this, 'admin.save_object', array('object' => $plg_obituary_subscribe)));

            if ($request->hasParameter('_save_and_add')) {
                $this->getUser()->setFlash('notice', $notice.' You can add another one below.');
                $this->redirect('@plg_obituary_subscribe_new');
            }
            else {
                $this->getUser()->setFlash('notice', $notice);
                $this->redirect('@plg_obituary_subscribe');
            }
        } else {
            $this->getUser()->setFlash('error', 'The item has not been saved due to some errors.', false);
        }
    }
}
