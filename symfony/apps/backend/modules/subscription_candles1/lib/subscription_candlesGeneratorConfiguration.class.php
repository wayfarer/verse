<?php

/**
 * subscription_candles module configuration.
 *
 * @package    verse3
 * @subpackage subscription_candles
 * @author     Vladimir Droznik <vladimir.droznik@gmail.com>
 * @version    SVN: $Id: configuration.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class subscription_candlesGeneratorConfiguration extends BaseSubscription_candlesGeneratorConfiguration
{
}
