<?php use_helper('I18N', 'Date') ?>
<?php include_partial('subscription_candles/assets') ?>

<div id="sf_admin_container">
  <h1><?php echo __('Candles subscription management', array(), 'messages') ?></h1>

  <div id="verse_submenu">
    <?php echo link_to('<< back to obituaries', '@plg_obituary')?>
  </div>

  <?php include_partial('subscription_candles/flashes') ?>

  <div id="sf_admin_header">
    <?php include_partial('subscription_candles/list_header', array('pager' => $pager)) ?>
  </div>

  <div id="sf_admin_bar">
    <?php include_partial('subscription_candles/filters', array('form' => $filters, 'configuration' => $configuration)) ?>
  </div>

  <div id="sf_admin_content">
    <form action="<?php echo url_for('plg_obituary_subscribe_collection', array('action' => 'batch')) ?>" method="post">
    <ul class="sf_admin_actions">
      <?php include_partial('subscription_candles/list_batch_actions', array('helper' => $helper )) ?>
      <?php include_partial('subscription_candles/list_actions', array('helper' => $helper)) ?>
    </ul>
    <?php include_partial('subscription_candles/list', array('pager' => $pager, 'sort' => $sort, 'helper' => $helper)) ?>
    <ul class="sf_admin_actions">
      <?php include_partial('subscription_candles/list_batch_actions', array('helper' => $helper )) ?>
      <?php include_partial('subscription_candles/list_actions', array('helper' => $helper)) ?>
    </ul>
    </form>
  </div>

  <div id="sf_admin_footer">
    <?php include_partial('subscription_candles/list_footer', array('pager' => $pager)) ?>
  </div>
</div>
