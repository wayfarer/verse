<?php

require_once dirname(__FILE__) . '/../lib/candlesGeneratorConfiguration.class.php';
require_once dirname(__FILE__) . '/../lib/candlesGeneratorHelper.class.php';

/**
 * candles actions.
 *
 * @package    verse3
 * @subpackage candles
 * @author     Vladimir Droznik <vladimir.droznik@gmail.com>
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class candlesActions extends autoCandlesActions {

    protected function processForm(sfWebRequest $request, sfForm $form) {
        $form->bind($request->getParameter($form->getName()), $request->getFiles($form->getName()));
        if ($form->isValid()) {
            $is_new = $form->getObject()->isNew();
            $notice = $is_new ? 'The item was created successfully.' : 'The item was updated successfully.';

            try {
                $plg_obituary_candle = $form->save();
            } catch (Doctrine_Validator_Exception $e) {

                $errorStack = $form->getObject()->getErrorStack();

                $message = get_class($form->getObject()) . ' has ' . count($errorStack) . " field" . (count($errorStack) > 1 ?  's' : null) . " with validation errors: ";
                foreach ($errorStack as $field => $errors) {
                    $message .= "$field (" . implode(", ", $errors) . "), ";
                }
                $message = trim($message, ', ');

                $this->getUser()->setFlash('error', $message);
                return sfView::SUCCESS;
            }

            $this->dispatcher->notify(new sfEvent($this, 'admin.save_object', array('object' => $plg_obituary_candle)));
            if ($is_new && $plg_obituary_candle->getState() == 0) {
                $this->dispatcher->notify(new sfEvent($this, 'obituary.publish_candle', array('object' => $plg_obituary_candle)));
            }

            if ($request->hasParameter('_save_and_add')) {
                $this->getUser()->setFlash('notice', $notice.' You can add another one below.');

                $this->redirect('@candle_new');
            } else {
                $this->getUser()->setFlash('notice', $notice);

                $this->redirect('@candle');
            }
        } else {
            $this->getUser()->setFlash('error', 'The item has not been saved due to some errors.', false);
        }
    }

    public function executeApprove(sfWebRequest $request) {
        $candle = $this->getRoute()->getObject();
        $candle->approve();
        $this->dispatcher->notify(new sfEvent($this, 'obituary.publish_candle', array('object' => $candle)));
        $this->redirect('@candle');
    }

    public function executeHide(sfWebRequest $request) {
        $candle = $this->getRoute()->getObject();
        $candle->hide();
        $this->redirect('@candle');
    }

    public function executeUnhide(sfWebRequest $request) {
        $candle = $this->getRoute()->getObject();
        $candle->unhide();
        $this->redirect('@candle');
    }

    public function executeBatchHide(sfWebRequest $request) {
        $ids = $request->getParameter('ids');

        $q = Doctrine::getTable('PlgObituaryCandle')->createQuery()
                ->whereIn('candle_id', $ids);

        foreach ($q->execute() as $candle) {
            $candle->hide();
        }

        $this->getUser()->setFlash('notice', 'The selected candles have been hidden successfully.');
    }

    public function executeBatchUnhide(sfWebRequest $request) {
        $ids = $request->getParameter('ids');

        $q = Doctrine::getTable('PlgObituaryCandle')->createQuery()
                ->whereIn('candle_id', $ids);

        foreach ($q->execute() as $candle) {
            $candle->unhide();
        }

        $this->getUser()->setFlash('notice', 'The selected candles have been unhidden successfully.');
    }

    public function executeBatchApprove(sfWebRequest $request) {
        $ids = $request->getParameter('ids');

        $q = Doctrine::getTable('PlgObituaryCandle')->createQuery()
                ->whereIn('candle_id', $ids);

        foreach ($q->execute() as $candle) {
            $candle->approve();
            $this->dispatcher->notify(new sfEvent($this, 'obituary.publish_candle', array('object' => $candle)));
        }

        $this->getUser()->setFlash('notice', 'The selected candles have been approved successfully.');
    }

    public function executeFilter(sfWebRequest $request) {
        parent::executeFilter($request);
        if($request->hasParameter('obituary')) {
            $this->getUser()->setAttribute('candle_obituary_id', $request->getParameter('obituary'));
            $filters = $this->getFilters();
            $filters['obituary_id'] = intval($request->getParameter('obituary'));
            $filters['state'] = 0;
            $this->setFilters($filters);
            $this->redirect('@candle');
        }
        if($request->hasParameter('_active_candles')) {
            $filters = $this->getFilters();
            $filters['state'] = 0;
            $this->setFilters($filters);
            $this->redirect('@candle');
        }
        if($request->hasParameter('_hidden_candles')) {
            $filters = $this->getFilters();
            $filters['state'] = 2;
            $this->setFilters($filters);
            $this->redirect('@candle');
        }
        if($request->hasParameter('_moderation_candles')) {
            $filters = $this->getFilters();
            $filters['state'] = 1;
            unset($filters['obituary_id']);
            $this->getUser()->setAttribute('candle_obituary_id', '');
            $this->setFilters($filters);
            $this->redirect('@candle');
        }
    }

}
