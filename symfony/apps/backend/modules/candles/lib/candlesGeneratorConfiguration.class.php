<?php

/**
 * candles module configuration.
 *
 * @package    verse3
 * @subpackage candles
 * @author     Vladimir Droznik <vladimir.droznik@gmail.com>
 * @version    SVN: $Id: configuration.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class candlesGeneratorConfiguration extends BaseCandlesGeneratorConfiguration {
    public function getFilterDefaults() {
        return array('state' => 0);
    }
}
