<?php use_helper('I18N', 'Date') ?>
<?php include_partial('candles/assets') ?>

<div id="sf_admin_container">
  <h1><?php echo __('Candles List', array(), 'messages') ?></h1>

  <div id="verse_submenu">
      <?php echo link_to('<< back to obituaries', 'plg_obituary')?> ::
      <?php echo link_to('published candles', 'candle_collection', array('action'=>'filter'), array('query_string'=>'_active_candles') )?> ::
      <?php echo link_to('hidden candles', 'candle_collection', array('action'=>'filter'), array('query_string'=>'_hidden_candles') )?> ::
      <?php echo link_to('moderation queue', 'candle_collection', array('action'=>'filter'), array('query_string'=>'_moderation_candles') )?>
  </div>

  <?php include_partial('candles/flashes') ?>

  <div id="sf_admin_header">
    <?php include_partial('candles/list_header', array('pager' => $pager)) ?>
  </div>

  <div id="sf_admin_bar">
    <?php include_partial('candles/filters', array('form' => $filters, 'configuration' => $configuration)) ?>
  </div>

  <div id="sf_admin_content">
    <form action="<?php echo url_for('candle_collection', array('action' => 'batch')) ?>" method="post">
    <ul class="sf_admin_actions">
      <?php include_partial('candles/list_batch_actions', array('helper' => $helper)) ?>
      <?php include_partial('candles/list_actions', array('helper' => $helper)) ?>
    </ul>
    <?php include_partial('candles/list', array('pager' => $pager, 'sort' => $sort, 'helper' => $helper)) ?>
    <ul class="sf_admin_actions">
      <?php include_partial('candles/list_batch_actions', array('helper' => $helper)) ?>
      <?php include_partial('candles/list_actions', array('helper' => $helper)) ?>
    </ul>
    </form>
  </div>

  <div id="sf_admin_footer">
    <?php include_partial('candles/list_footer', array('pager' => $pager)) ?>
  </div>
</div>
