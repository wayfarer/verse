<td>
  <ul class="sf_admin_td_actions">
    <?php echo $helper->linkToEdit($plg_obituary_candle, array(  'params' =>   array(  ),  'class_suffix' => 'edit',  'label' => 'Edit',)) ?>
<?php if($plg_obituary_candle->getState() != 2): ?>
    <li class="sf_admin_action_hide">
      <?php echo link_to(__('Hide', array(), 'messages'), 'candles/hide?candle_id='.$plg_obituary_candle->getCandleId().'&domain_id='.$plg_obituary_candle->getDomainId(), array()) ?>
    </li>
<?php endif; ?>
<?php if($plg_obituary_candle->getState() == 1): ?>
    <li class="sf_admin_action_approve">
      <?php echo link_to(__('Approve', array(), 'messages'), 'candles/approve?candle_id='.$plg_obituary_candle->getCandleId().'&domain_id='.$plg_obituary_candle->getDomainId(), array()) ?>
    </li>
<?php endif; ?>
<?php if($plg_obituary_candle->getState() == 2): ?>
    <li class="sf_admin_action_unhide">
      <?php echo link_to(__('Unhide', array(), 'messages'), 'candles/unhide?candle_id='.$plg_obituary_candle->getCandleId().'&domain_id='.$plg_obituary_candle->getDomainId(), array()) ?>
    </li>
<?php endif; ?>
<?php if($plg_obituary_candle->getState() == 2): ?>
    <?php echo $helper->linkToDelete($plg_obituary_candle, array(  'params' =>   array(  ),  'confirm' => 'Are you sure?',  'class_suffix' => 'delete',  'label' => 'Delete',)) ?>
<?php endif; ?>
  </ul>
</td>
