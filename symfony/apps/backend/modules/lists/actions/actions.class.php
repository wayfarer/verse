<?php

require_once dirname(__FILE__).'/../lib/listsGeneratorConfiguration.class.php';
require_once dirname(__FILE__).'/../lib/listsGeneratorHelper.class.php';

/**
 * lists actions.
 *
 * @package    verse3
 * @subpackage lists
 * @author     Vladimir Droznik
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class listsActions extends autoListsActions
{
  public function executeAction($request) {
    // action is at list_id parameter, due to autocompleter uses GET request
    switch($request->getParameter('list_id')) {
      case 'ac_type':
        return $this->executeSuggest('type', $request);
      break;
    }
  }

  public function executeSuggest($field, $request) {
    $this->getResponse()->setContentType('application/json');
    $fds = VerseUtil::getSuggestions('PlgList', $field, $request->getParameter('q'), $request->getParameter('limit'));
    return $this->renderText(json_encode($fds));
  }

}
