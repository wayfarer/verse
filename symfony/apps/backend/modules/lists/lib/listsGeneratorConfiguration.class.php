<?php

/**
 * lists module configuration.
 *
 * @package    verse3
 * @subpackage lists
 * @author     Vladimir Droznik
 * @version    SVN: $Id: configuration.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class listsGeneratorConfiguration extends BaseListsGeneratorConfiguration
{
}
