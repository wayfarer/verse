<?php

require_once dirname(__FILE__).'/../lib/obituariesGeneratorConfiguration.class.php';
require_once dirname(__FILE__).'/../lib/obituariesGeneratorHelper.class.php';

/**
 * obituaries actions.
 *
 * @package    verse3
 * @subpackage obituaries
 * @author     Vladimir Droznik
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class obituariesActions extends autoObituariesActions
{
  public function executeIndex(sfWebRequest $request) {
    parent::executeIndex($request);
    $this->config = Doctrine::getTable('PlgObituary')->loadConfig();
    $this->moderation_queue_size = Doctrine::getTable('PlgObituaryCandle')->createQuery()->select('count(*)')->andWhere('state = 1')->execute(array(), Doctrine::HYDRATE_SINGLE_SCALAR);
    $this->moderation_photo_queue_size = Doctrine::getTable('PlgObituaryImage')->createQuery()->select('count(*)')->andWhere('state = 1')->execute(array(), Doctrine::HYDRATE_SINGLE_SCALAR);
    $this->moderation_movie_queue_size = Doctrine::getTable('PlgObituaryMovie')->createQuery()->select('count(*)')->andWhere('state = 1')->execute(array(), Doctrine::HYDRATE_SINGLE_SCALAR);
  }

  public function processForm(sfWebRequest $request, sfForm $form) {
      $form->bind($request->getParameter($form->getName()), $request->getFiles($form->getName()));

      if ($form->isValid()) {
          $is_new_obituary = $form->getObject()->isNew();
          $notice = $is_new_obituary ? 'The item was created successfully.' : 'The item was updated successfully.';

          try {
              $plg_obituary = $form->save();
          } catch (Doctrine_Validator_Exception $e) {

              $errorStack = $form->getObject()->getErrorStack();

              $message = get_class($form->getObject()) . ' has ' . count($errorStack) . " field" . (count($errorStack) > 1 ? 's' : null) . " with validation errors: ";
              foreach ($errorStack as $field => $errors) {
                  $message .= "$field (" . implode(", ", $errors) . "), ";
              }
              $message = trim($message, ', ');

              $this->getUser()->setFlash('error', $message);
              return sfView::SUCCESS;
          }

          $this->dispatcher->notify(new sfEvent($this, 'admin.save_object', array('object' => $plg_obituary)));

          if ($is_new_obituary) {
              $this->dispatcher->notify(new sfEvent($this, 'obituary.create_obituary', array('object' => $plg_obituary)));
          }

          if ($request->hasParameter('_save_and_add')) {
              $this->getUser()->setFlash('notice', $notice . ' You can add another one below.');
              $this->redirect('@plg_obituary_new');
          } else {
              $this->getUser()->setFlash('notice', $notice);
              $this->redirect('@plg_obituary');
          }
      } else {
          $this->getUser()->setFlash('error', 'The item has not been saved due to some errors.', false);
      }
  }


  public function executeObituariesConfig(sfWebRequest $request) {
    $this->form = new ObituariesConfigForm();
    if ($request->isMethod('post')) {
      $this->form->bind($request->getParameter('config'));
      if ($this->form->isValid()) {
        Doctrine::getTable('PlgObituary')->saveConfig($this->form->getValues());

        $this->getUser()->setFlash('notice', 'Obituaries configuration saved');
        $this->redirect('@plg_obituary');
      }
    }
    else {
      $this->form->setDefaults(Doctrine::getTable('PlgObituary')->loadConfig());
    }
  }

  public function executeConfig(sfWebRequest $request) {
    $this->obituary = $this->getRoute()->getObject();
    $this->form = new ObituaryConfigForm();
    if ($request->isMethod('post')) {
      $this->form->bind($request->getParameter('config'));
      if ($this->form->isValid()) {
        $this->getRoute()->getObject()->saveConfig($this->form->getValues());

        $this->getUser()->setFlash('notice', 'Obituary configuration saved');
        $this->redirect('@plg_obituary');
      }
    }
    else {
      $this->form->setDefaults($this->getRoute()->getObject()->loadConfig());
    }
  }

/*  public function executeCandles($request) {
    $obituary = $this->getRoute()->getObject();
  	$this->redirect('plg_obituary_candle', $obituary);
  }
*/
	public function executeAction($request) {
    // action is at obituary_id parameter, due to autocompleter uses GET request
	  switch($request->getParameter('obituary_id')) {
		  case 'ac_homeplace':
		    return $this->executeAutocomplete('home_place', $request);
		  break;
      case 'ac_birthplace':
        return $this->executeAutocomplete('birth_place', $request);
      break;
      case 'ac_serviceplace':
        return $this->executeAutocomplete('service_place', $request);
      break;
      case 'ac_visitplace':
        return $this->executeAutocomplete('visitation_place', $request);
      break;
      case 'ac_finaldisp':
        return $this->executeAutocomplete('final_disposition', $request);
		  break;
		}
  }

  public function executeAutocomplete($field, $request) {
	  $this->getResponse()->setContentType('application/json');
    $fds = VerseUtil::getSuggestions('PlgObituary', $field, $request->getParameter('q'), $request->getParameter('limit'));
 //	  $fds = PlgObituaryTable::retrieveForAutocomplete($field, $request->getParameter('q'), $request->getParameter('limit'));
	  return $this->renderText(json_encode($fds));
  }

  public function executeGetObitImages($request) {
      if(isset($_POST['obit_theme'])) {
          switch ($_POST['obit_theme']) {
              case "nt_frame":
                  $path = "/with_frame";
                  break;
              case "nt_without_frame":
                  $path = "/without_frame";
                  break;
              default:
                  $path = "";
                  break;
          }
          $with_default = isset($_POST['with_default']) && $_POST['with_default'] == 'true' ? true : false;
          $theme_images = VerseUtil::getObituaryThemeImages($with_default, $path);
          $military_images = VerseUtil::getObituaryMilitaryImages($with_default, $path);

          $images = array(
              'theme_images' => $theme_images,
              'military_images' => $military_images
          );
          echo json_encode($images, JSON_FORCE_OBJECT);
      }

      $this->setLayout(false);
      return sfView::NONE;
  }

  public function executeGetObitImageThumbnail($request) {
      if(isset($_POST['image'])) {
          $thumb = VerseUtil::obitThemeImageThumbnail($_POST['image']);
          echo json_encode($thumb);
      }

      $this->setLayout(false);
      return sfView::NONE;
  }

  public function executeDisableFacebookPublishing($request) {
      Doctrine::getTable('PlgObituary')->removeFacebookConfig();

      $this->setLayout(false);
      return sfView::NONE;
  }

/*  public function executeScrapbookOrder($request) {
    $order = $request->getParameter('slide_id');
    return $this->renderText(var_dump($order));
    return sfView::NONE;
  }
*/
/*  protected function processForm(sfWebRequest $request, sfForm $form)
  {
    $form->bind($request->getParameter($form->getName()), $request->getFiles($form->getName()));
    if ($form->isValid())
    {
      $notice = $form->getObject()->isNew() ? 'The item was created successfully.' : 'The item was updated successfully.';

      try {
        $plg_obituary = $form->save();
      } catch (Doctrine_Validator_Exception $e) {

        $errorStack = $form->getObject()->getErrorStack();

        $message = get_class($form->getObject()) . ' has ' . count($errorStack) . " field" . (count($errorStack) > 1 ?  's' : null) . " with validation errors: ";
        foreach ($errorStack as $field => $errors) {
            $message .= "$field (" . implode(", ", $errors) . "), ";
        }
        $message = trim($message, ', ');

        $this->getUser()->setFlash('error', $message);
        return sfView::SUCCESS;
      }

      $this->dispatcher->notify(new sfEvent($this, 'admin.save_object', array('object' => $plg_obituary)));

      if ($request->hasParameter('_save_and_add'))
      {
        $this->getUser()->setFlash('notice', $notice.' You can add another one below.');

        $this->redirect('@plg_obituary_new');
      }
      else
      {
        $this->getUser()->setFlash('notice', $notice);

        // redirect to obituaries list
        $this->redirect('@plg_obituary');
      }
    }
    else
    {
      $this->getUser()->setFlash('error', 'The item has not been saved due to some errors.', false);
    }
  }
*/
}
