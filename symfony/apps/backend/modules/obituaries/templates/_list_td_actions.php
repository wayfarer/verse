<td>
  <ul class="sf_admin_td_actions sf_admin_obituary_actions">
    <?php echo $helper->linkToEdit($plg_obituary, array(  'params' =>   array(  ),  'class_suffix' => 'edit',  'label' => '<img src="/img/edit.png" alt="Edit" title="Edit">',)) ?>
    <li class="sf_admin_action_config">
      <?php echo link_to(__('<img src="/img/configure.png" alt="Config" title="Config">', array(), 'messages'), 'obituaries/config?obituary_id='.$plg_obituary->getObituaryId(), array()) ?>
    </li>
    <?php echo $helper->linkToDelete($plg_obituary, array(  'params' =>   array(  ),  'confirm' => 'Are you sure you want to delete the obituary?\nAll candles will be LOST!',  'class_suffix' => 'delete',  'label' => '<img src="/img/delete.png" alt="Delete" title="Delete">',)) ?>
    <li class="sf_admin_action_scrapbook">
      <?php echo link_to(__('<img src="/img/scrapbook.png" alt="Scrapbook" title="Scrapbook">', array(), 'messages').' ('.VerseUtil::decorateValue($plg_obituary->getScrapbookCount(), 'items_count').')', 'obituaries/scrapbook?obituary_id='.$plg_obituary->getObituaryId(), array()) ?>
    </li>
    <li class="sf_admin_action_subscriptions">
      <?php echo link_to(__('<img src="/img/subscribe.png" alt="Subscriptions" title="Subscriptions">', array(), 'messages').' ('.VerseUtil::decorateValue($plg_obituary->getSubscribeCount(), 'items_count').')', 'subscription_candles/filter/action?obituary='.$plg_obituary->getObituaryId(), array()) ?>
    </li>
    <li class="sf_admin_action_candles">
      <?php echo link_to(__('<img src="/img/candle.png" alt="Candles" title="Candles">', array(), 'messages').' ('.VerseUtil::decorateValue($plg_obituary->getActiveCandleCount(), 'active_candle_count').'/'.VerseUtil::decorateValue($plg_obituary->getPendingCandleCount(), 'pending_candle_count').'/'.VerseUtil::decorateValue($plg_obituary->getHiddenCandleCount(), 'hidden_candle_count').')', 'candles/filter/action?obituary='.$plg_obituary->getObituaryId(), array()) ?>
    </li>
    <li class="sf_admin_action_photos">
      <?php echo link_to(__('<img src="/img/photo.png" alt="Photos" title="Photos">', array(), 'messages').' ('.VerseUtil::decorateValue($plg_obituary->getActivePhotoCount(), 'active_photo_count').'/'.VerseUtil::decorateValue($plg_obituary->getPendingPhotoCount(), 'pending_photo_count').'/'.VerseUtil::decorateValue($plg_obituary->getHiddenPhotoCount(), 'hidden_photo_count').')', 'moderate_photos/filter/action?obituary='.$plg_obituary->getObituaryId(), array()) ?>
    </li>
    <li class="sf_admin_action_movies">
      <?php echo link_to(__('<img src="/img/movie.png" alt="Movies" title="Movies">', array(), 'messages').' ('.VerseUtil::decorateValue($plg_obituary->getActiveMovieCount(), 'active_movie_count').'/'.VerseUtil::decorateValue($plg_obituary->getPendingMovieCount(), 'pending_movie_count').'/'.VerseUtil::decorateValue($plg_obituary->getHiddenMovieCount(), 'hidden_movie_count').')', 'moderate_movie/filter/action?obituary='.$plg_obituary->getObituaryId(), array()) ?>
    </li>
  </ul>
</td>
