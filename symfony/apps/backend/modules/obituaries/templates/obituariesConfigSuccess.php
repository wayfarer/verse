<div id="sf_admin_container">
<h1>Obituaries Configuration</h1>
<p>These options are only available with our latest obituary style. If you currently have an obit style from before August 2010 and would like to upgrade, please contact us. There is no charge to upgrade to the new obituaries.</p>
<div class="sf_admin_form_obituaries_config">
<form action="<?php echo url_for('@config_obituary')?>" method="post">
  <ul class="sf_admin_actions">
    <li><?php echo link_to('Back', '@plg_obituary')?></li>
    <li><input type="submit" value="Save" /></li>
  </ul>
  <?php echo $form->renderHiddenFields(false) ?>

  <?php $set = 0; foreach($form as $field):?>
    <?php if($field->isHidden()) continue;?>
    <?php if($set < 1): ?>
      <fieldset>
    <?php $set = 1; endif?>
    <?php if($set == 1 && substr($field->getName(),0,6)=='field_'):?>
      </fieldset>
      <fieldset class="fields_config">
        <h2>Fields configuration</h2>
        <div class="sf_admin_form_row sf_admin_form_row_title">
          <span style="margin-right: 30px">Default name</span>
          <span style="margin-right: 10px">Enabled</span>
          <span>Custom name (leave it blank to use default column name)</span>
        </div>
    <?php $set = 2; $i=0; endif?>
    <div class="sf_admin_form_row<?php if($set == 2): if($i++ % 2 == 1):?> sf_admin_form_row_odd<?php else:?> sf_admin_form_row_even<?php endif; endif;?>">
	    <?php if($set != 2 || $i % 2 == 1) { echo $field->renderLabel(); } ?>
	    <?php echo $field->render() ?>
    </div>
  <?php endforeach?>
      </fieldset>
  <ul class="sf_admin_actions">
    <li><?php echo link_to('Back', '@plg_obituary')?></li>
    <li><input type="submit" value="Save" /></li>
  </ul>
</form>
<script>
jQuery(function() {
  $('input[type="text"]').blur( function() {
    if($(this).val() == '' || $(this).val() == 'Use default') {
      if($(this).attr("id") == 'config_facebook_user' || $(this).attr("id") == 'config_facebook_page') {
          $(this).val('Disable');
      } else {
          $(this).val('Use default');
      }
      $(this).css('color', '#999');
    }
  } );
  $('input[type="text"]').focus( function() {
    if($(this).val() == 'Use default' || $(this).val() == 'Disable') {
      $(this).val('').css('color', '#000');
    }
  } );
  $('input[type="text"]').blur();

  add_obituary_image_loaders();

  $('#config_obituary_theme').change(function() {
    set_obituary_images($('#config_obituary_theme option:selected').val(), false);
  });

  $('#config_obituary_theme').change();

  $('#config_obituary_theme_image').change(function() {
      add_obituary_image_preview('theme');
  });

  $('#config_obituary_military_image').change(function() {
      add_obituary_image_preview('military');
  });

  var facebook_link = '<a class="facebook-link" href="https://www.facebook.com/dialog/oauth?client_id=<?php echo sfConfig::get('app_facebook_app_id'); ?>&redirect_uri=<?php echo urlencode('http://'.sfConfig::get('app_facebook_app_redirect_domain').'/facebook.php'); ?>&state=<?php echo substr(md5(uniqid("")), 0, 32); ?>&canvas=1&fbconnect=0&display=page&scope=offline_access%2Cpublish_stream%2Cmanage_pages" target="_blank">link</a>';
  if($('#config_facebook_user_access_token').val() == '') {
      $('#config_facebook_page').after('<div id="verse-obituaries-config-facebook-link">To enable publishing messages about new obituaries on facebook wall click this '+facebook_link+'</div>');
  } else {
      $('#config_facebook_page').after('<div id="verse-obituaries-config-facebook-link">To change user for publishing messages about new obituaries on facebook wall click this '+facebook_link+
                                        '<br/>To disable this feature click this <a id="facebook-disable-link" href="#">link</a></div>');
      $('#facebook-disable-link').click(function() {
          $.post('/backend.php/config/obituaries/disable_facebook_publishing', function() {
              location.reload();
          });
          return false;
      });
  }
  $('.facebook-link').click(function() {
      $('form').submit();
  });

});
</script>
</div>

</div>