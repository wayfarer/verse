<div id="sf_admin_container">
<h1>Obituary Configuration</h1>

<div class="sf_admin_form_obituaries_config">
<form action="<?php echo url_for('obituary_config', $obituary)?>" method="post">
  <?php echo $form->renderHiddenFields(false) ?>

  <?php foreach($form as $field):?>
    <?php if($field->isHidden()) continue;?>
    <div class="sf_admin_form_row">
	    <?php echo $field->renderLabel(); ?>
	    <?php echo $field->render() ?>
    </div>
  <?php endforeach?>
  <ul class="sf_admin_actions">
    <li><?php echo link_to('Back', '@plg_obituary')?></li>
    <li><input type="submit" value="Save" /></li>
  </ul>
</form>
</div>

</div>
<script>
jQuery(function() {
  $('input[type="text"]').blur( function() {
    if($(this).val() == '' || $(this).val() == 'Use default') {
      $(this).val('Use default').css('color', '#999');
    }
  } );
  $('input[type="text"]').focus( function() {
    if($(this).val() == 'Use default') {
      $(this).val('').css('color', '#000');
    }
  } );
  $('input[type="text"]').blur();

  add_obituary_image_loaders();

  $('#config_obituary_theme').change(function() {
    var obit_theme = $('#config_obituary_theme option:selected').val();
    if(obit_theme == '') {
        obit_theme = '<?php $config = Doctrine::getTable('PlgObituary')->loadConfig(); echo $config['obituary_theme'] ?>';
    }
    set_obituary_images(obit_theme, true);
  });

  $('#config_obituary_theme').change();

  $('#config_obituary_theme_image').change(function() {
      add_obituary_image_preview('theme');
  });

  $('#config_obituary_military_image').change(function() {
      add_obituary_image_preview('military');
  });
});
</script>
