<?php use_helper('I18N', 'Date') ?>
<?php include_partial('obituaries/assets') ?>

<div id="sf_admin_container">
  <h1><?php echo __('Obituaries Management', array(), 'messages') ?></h1>

  <div id="section_config">
    Candles: <?php echo $config['candles_policy'] == 'moderated_n_days' ? 'moderated after '.$config['days_without_moderation'].' day(s)' : $config['candles_policy'] ?><br/>
    Captcha: <?php echo $config['captcha']?'on':'off'?><br/>
    Flowers page: <?php echo $config['flowers_page']?><br/>
    <?php echo button_to('configure', '@config_obituary') ?>
    <?php echo button_to('obits subscriptions', '@subscribe') ?><br/>
  </div>

  <div id="section_config2">
    <?php echo link_to($moderation_queue_size.' candle(s) are waiting for approval', '/backend.php/candles/filter/action', array('query_string' => '_moderation_candles')) ?><br/>
    <?php echo link_to($moderation_photo_queue_size.' photo(s) are waiting for approval', '/backend.php/moderate_photos/filter/action', array('query_string' => '_moderated_photos')) ?><br/>
    <?php echo link_to($moderation_movie_queue_size.' movie(s) are waiting for approval', '/backend.php/moderate_movie/filter/action', array('query_string' => '_moderated_movies')) ?><br/>
    <?php echo link_to('Block IP List', '/backend.php/block_ip').' :: '.link_to('Block Keyword List', '/backend.php/block_keyword') ?><br/>
  </div>

  <?php include_partial('obituaries/flashes') ?>

  <div id="sf_admin_header">
    <?php include_partial('obituaries/list_header', array('pager' => $pager)) ?>
  </div>

  <div id="sf_admin_bar">
    <?php include_partial('obituaries/filters', array('form' => $filters, 'configuration' => $configuration)) ?>
  </div>

  <div id="sf_admin_content">
    <form action="<?php echo url_for('plg_obituary_collection', array('action' => 'batch')) ?>" method="post">
    <ul class="sf_admin_actions">
      <?php include_partial('obituaries/list_batch_actions', array('helper' => $helper)) ?>
      <?php include_partial('obituaries/list_actions', array('helper' => $helper)) ?>
    </ul>
    <?php include_partial('obituaries/list', array('pager' => $pager, 'sort' => $sort, 'helper' => $helper)) ?>
    <ul class="sf_admin_actions">
      <?php include_partial('obituaries/list_batch_actions', array('helper' => $helper)) ?>
      <?php include_partial('obituaries/list_actions', array('helper' => $helper)) ?>
    </ul>
    </form>
  </div>

  <div id="sf_admin_footer">
    <?php include_partial('obituaries/list_footer', array('pager' => $pager)) ?>
  </div>
</div>
