<?php use_stylesheets_for_form($form) ?>
<?php use_javascripts_for_form($form) ?>

<div class="sf_admin_form">
  <?php echo form_tag_for($form, '@domain') ?>
    <?php echo $form->renderHiddenFields(false) ?>

    <?php if ($form->hasGlobalErrors()): ?>
      <?php echo $form->renderGlobalErrors() ?>
    <?php endif; ?>

    <?php include_partial('domains/form_actions', array('sms_domain' => $sms_domain, 'form' => $form, 'configuration' => $configuration, 'helper' => $helper)) ?>

    <?php foreach ($configuration->getFormFields($form, $form->isNew() ? 'new' : 'edit') as $fieldset => $fields): ?>
      <?php include_partial('domains/form_fieldset', array('sms_domain' => $sms_domain, 'form' => $form, 'fields' => $fields, 'fieldset' => $fieldset)) ?>
    <?php endforeach; ?>

    <?php include_partial('domains/form_actions', array('sms_domain' => $sms_domain, 'form' => $form, 'configuration' => $configuration, 'helper' => $helper)) ?>
  </form>
</div>
<script>
jQuery( function() {
	if($('#sms_domain_domain_type_0').attr('checked')) {
    $('#sf_fieldset_alias').hide();
	}
	else {
    $('#sf_fieldset_properties').hide();
  }
	$('#sms_domain_domain_type_0, #sms_domain_domain_type_1').bind('click', function(e) {
		if($(this).attr('value')==0) {
			$('#sf_fieldset_alias').hide();
			$('#sf_fieldset_properties').show();
		}
		else {
      $('#sf_fieldset_properties').hide();
      $('#sf_fieldset_alias').show();
		}
	} );
} );
</script>