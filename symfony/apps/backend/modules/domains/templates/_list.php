<div class="sf_admin_list">
  <?php if (!$pager->getNbResults()): ?>
    <p><?php echo __('No result', array(), 'sf_admin') ?></p>
  <?php else: ?>
    <table id="domains_tree" cellspacing="0">
      <thead>
        <tr>
          <th id="sf_admin_list_batch_actions"><input id="sf_admin_list_batch_checkbox" type="checkbox" onclick="checkAll();" /></th>
          <?php include_partial('domains/list_th_tabular', array('sort' => $sort)) ?>
          <th id="sf_admin_list_th_actions">
          <?php if ($pager->haveToPaginate()): ?>
            <?php include_partial('domains/pagination', array('pager' => $pager)) ?>
          <?php else: ?>
            <?php echo __('Actions', array(), 'sf_admin') ?>
          <?php endif; ?>
          </th>
        </tr>
      </thead>
      <tfoot>
        <tr>
          <th colspan="7">
            <?php if ($pager->haveToPaginate()): ?>
              <?php include_partial('domains/pagination', array('pager' => $pager)) ?>
            <?php endif; ?>

            <?php echo format_number_choice('[0] no result|[1] 1 result|(1,+Inf] %1% results', array('%1%' => $pager->getNbResults()), $pager->getNbResults(), 'sf_admin') ?>
            <?php if ($pager->haveToPaginate()): ?>
              <?php echo __('(page %%page%%/%%nb_pages%%)', array('%%page%%' => $pager->getPage(), '%%nb_pages%%' => $pager->getLastPage()), 'sf_admin') ?>
            <?php endif; ?>
          </th>
        </tr>
      </tfoot>
      <tbody>
        <?php $expanded = false; $met_domains = array(); ?>
        <?php foreach ($pager->getResults() as $i => $sms_domain): $odd = fmod(++$i, 2) ? 'odd' : 'even' ?>
          <tr id="node-<?php echo $sms_domain['domain_id'] ?>" class="sf_admin_row <?php echo $odd;
            $met_domains[] = $sms_domain['domain_id'];
            if($sms_domain['alias_domain_id']) {
              echo ' child-of-node-'.$sms_domain['alias_domain_id'];
              if(!in_array($sms_domain['alias_domain_id'], $met_domains)) {
                $expanded = true;
              }
            }
          ?>">
            <?php include_partial('domains/list_td_batch_actions', array('sms_domain' => $sms_domain, 'helper' => $helper)) ?>
            <?php include_partial('domains/list_td_tabular', array('sms_domain' => $sms_domain)) ?>
            <?php include_partial('domains/list_td_actions', array('sms_domain' => $sms_domain, 'helper' => $helper)) ?>
          </tr>
        <?php endforeach; ?>
      </tbody>
    </table>
  <?php endif; ?>
</div>
<script type="text/javascript">
/* <![CDATA[ */
function checkAll()
{
  var boxes = document.getElementsByTagName('input'); for(var index = 0; index < boxes.length; index++) { box = boxes[index]; if (box.type == 'checkbox' && box.className == 'sf_admin_batch_checkbox') box.checked = document.getElementById('sf_admin_list_batch_checkbox').checked } return true;
}
jQuery( function() {
  $("#domains_tree").treeTable({
    treeColumn: 2,
<?php if(!$expanded): ?>
    initialState: 'collapsed'
<?php else: ?>
    initialState: 'expanded'
<?php endif ?>
  });
} );
/* ]]> */
</script>
