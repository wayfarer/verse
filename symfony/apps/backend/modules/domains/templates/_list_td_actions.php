<td>
  <ul class="sf_admin_td_actions">
<?php if($sms_domain->getAliasDomainId()==0): ?>
    <li class="sf_admin_action_manage">
      <?php echo link_to(__('Manage', array(), 'messages'), 'domains/manage?domain_id='.$sms_domain->getDomainId(), array()) ?>
    </li>
    <li class="sf_admin_action_obituaries">
      <?php echo link_to(__('Obituaries', array(), 'messages'), 'domains/obituaries?domain_id='.$sms_domain->getDomainId(), array()) ?>
    </li>
    <li class="sf_admin_action_products">
      <?php echo link_to(__('Products', array(), 'messages'), 'domains/products?domain_id='.$sms_domain->getDomainId(), array()) ?>
    </li>
    <li class="sf_admin_action_users">
      <?php echo link_to(__('Users', array(), 'messages'), 'domains/users?domain_id='.$sms_domain->getDomainId(), array()) ?>
    </li>
    <li class="sf_admin_action_statistics">
      <?php echo link_to(__('Stats', array(), 'messages'), 'domains/statistics?domain_id='.$sms_domain->getDomainId(), array()) ?>
    </li>
<?php endif ?>
    <?php echo $helper->linkToEdit($sms_domain, array(  'params' =>   array(  ),  'class_suffix' => 'edit',  'label' => 'Edit',)) ?>
<?php if($sms_domain->getAliasDomainId()==0): ?>
  <?php if($sms_domain['mode']==0): ?>
      <li class="sf_admin_action_to_uc">
        <?php echo link_to(__('To UC', array(), 'messages'), 'domains/touc?domain_id='.$sms_domain->getDomainId(), array('confirm'=>'Are you sure you want to move '.$sms_domain['domain_name'].'\nand it\'s related domains to under construction mode?')) ?>
      </li>
  <?php endif ?>
  <?php if($sms_domain['mode']==1): ?>
      <li class="sf_admin_action_to_production">
        <?php echo link_to(__('To Production', array(), 'messages'), 'domains/toproduction?domain_id='.$sms_domain->getDomainId(), array('confirm'=>'Are you sure you want to move '.$sms_domain['domain_name'].'\nand it\'s related domains to production mode?')) ?>
      </li>
      <li class="sf_admin_action_remove">
        <?php echo link_to(__('Remove', array(), 'messages'), 'domains/remove?domain_id='.$sms_domain->getDomainId(), array('confirm'=>'Are you sure you want to remove '.$sms_domain['domain_name'].'\nand it\'s related domains?')) ?>
      </li>
  <?php endif ?>
  <?php if($sms_domain['mode']==2): ?>
    <li class="sf_admin_action_restore">
      <?php echo link_to(__('Restore', array(), 'messages'), 'domains/restore?domain_id='.$sms_domain->getDomainId(), array('confirm'=>'Are you sure you want to restore '.$sms_domain['domain_name'].'\nand it\'s related domains?')) ?>
    </li>
  <?php endif ?>
<?php endif ?>

  </ul>
</td>
