<?php use_javascript("jquery.treeTable.min.js") ?>
<?php use_helper('I18N', 'Date') ?>
<?php include_partial('domains/assets') ?>

<div id="sf_admin_container">
  <h1><?php echo __('Domains List', array(), 'messages') ?> :: <?php $modes = Doctrine::getTable('SmsDomain')->getDomainModes(); echo ucwords($modes[$filters['mode']->getValue()]) ?></h1>

  <div id="verse_submenu">
    <?php echo link_to('production domains', 'domain_collection', array('action'=>'filter'), array('query_string'=>'_production', 'method' => 'post') )?> ::
    <?php echo link_to('under construction domains', 'domain_collection', array('action'=>'filter'), array('query_string'=>'_uc', 'method' => 'post') )?> ::
    <?php echo link_to('removed domains', 'domain_collection', array('action'=>'filter'), array('query_string'=>'_removed', 'method' => 'post') )?>
  </div>

  <div id="section_config">
    <?php echo link_to('MX records list', '/mx.php') ?><br/>
    <?php echo link_to('Candle reports', '/candle_report_sites.php') ?>
  </div>

  <?php include_partial('domains/flashes') ?>

  <div id="sf_admin_header">
    <?php include_partial('domains/list_header', array('pager' => $pager)) ?>
  </div>

  <div id="sf_admin_bar">
    <?php include_partial('domains/filters', array('form' => $filters, 'configuration' => $configuration)) ?>
  </div>

  <div id="sf_admin_content">
    <form action="<?php echo url_for('domain_collection', array('action' => 'batch')) ?>" method="post">
    <ul class="sf_admin_actions">
      <?php include_partial('domains/list_batch_actions', array('helper' => $helper)) ?>
      <?php include_partial('domains/list_actions', array('helper' => $helper)) ?>
    </ul>
    <?php include_partial('domains/list', array('pager' => $pager, 'sort' => $sort, 'helper' => $helper)) ?>
    <ul class="sf_admin_actions">
      <?php include_partial('domains/list_batch_actions', array('helper' => $helper)) ?>
      <?php include_partial('domains/list_actions', array('helper' => $helper)) ?>
    </ul>
    </form>
  </div>

  <div id="sf_admin_footer">
    <?php include_partial('domains/list_footer', array('pager' => $pager)) ?>
  </div>
<br/>
  <small>* Note: Space consumed by domains are automatically calculated once per week on Mondays. If you want actual information right now, please click the following link: <a href="/calc_spaces.php?all=1">Calculate domains spaces now</a></small>

</div>
