<?php

require_once dirname(__FILE__).'/../lib/domainsGeneratorConfiguration.class.php';
require_once dirname(__FILE__).'/../lib/domainsGeneratorHelper.class.php';

/**
 * domains actions.
 *
 * @package    verse3
 * @subpackage domains
 * @author     Vladimir Droznik
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class domainsActions extends autoDomainsActions
{
  public function executeIndex(sfWebRequest $request) {
    // allow domain management only from ws1 webserver or zero (test) server
    if(sfConfig::get('app_verse_server_id')!=0 && sfConfig::get('app_verse_server_id')!=1) {
      // transfer current session to ws1 webserver and redirect there
      VerseUtil::transferSession(1);
      $url = sfConfig::get('app_verse_server_url_1');
      header("Location: $url/backend.php/domains?z=".session_id());
      exit;
    }
    else parent::executeIndex($request);
  }

  public function executeTouc(sfWebRequest $request) {
    $domain = $this->getRoute()->getObject();
    $domain->toUc();
    $this->dispatcher->notify(new sfEvent($this, 'admin.save_object', array('object' => $domain)));
    $this->getUser()->setFlash('notice', $domain.' is under construction now');
    $this->redirect('@domain');
  }
  public function executeToproduction(sfWebRequest $request) {
    $domain = $this->getRoute()->getObject();
    $domain->toProduction();
    $this->dispatcher->notify(new sfEvent($this, 'admin.save_object', array('object' => $domain)));
    $this->getUser()->setFlash('notice', $domain.' is in production now');
    $this->redirect('@domain');
  }
  public function executeRemove(sfWebRequest $request) {
    $domain = $this->getRoute()->getObject();
    $domain->removeDomain();
    $this->dispatcher->notify(new sfEvent($this, 'admin.save_object', array('object' => $domain)));
    $this->getUser()->setFlash('notice', $domain.' removed');
    $this->redirect('@domain');
  }
  public function executeRestore(sfWebRequest $request) {
    $domain = $this->getRoute()->getObject();
    $domain->restoreDomain();
    $this->dispatcher->notify(new sfEvent($this, 'admin.save_object', array('object' => $domain)));
    $this->getUser()->setFlash('notice', $domain.' restored');
    $this->redirect('@domain');
  }

  public function executeManage($request) {
    $this->executeAction('manage');
  }
  public function executeObituaries($request) {
    $this->executeAction('obituaries');
  }
  public function executeProducts($request) {
    $this->executeAction('products');
  }
  public function executeUsers($request) {
    $this->executeAction('users');
  }
  public function executeStatistics($request) {
    $this->executeAction('statistics');
  }

  protected function executeAction($action) {
    $domain = $this->getRoute()->getObject();
    if(sfConfig::get('app_verse_server_id') != $domain->getServerId()) {
      // transfer session to other webserver
      VerseUtil::transferSession($domain->getServerId());
    }
    switch($action) {
      case 'manage':
        header("Location: http://$domain/structure.php?z=".session_id());
      break;
      case 'users':
        header("Location: http://$domain/backend.php/sf_guard_user?z=".session_id());
      break;
      case 'obituaries':
      case 'products':
      case 'statistics':
        header("Location: http://$domain/backend.php/$action?z=".session_id());
      break;
    }
    exit;
  }

  public function executeFilter(sfWebRequest $request) {
    if($request->hasParameter('_production')) {
      $this->setPage(1);
      $filters = $this->getFilters();
      $filters['mode'] = 0;
      $this->setFilters($filters);
      $this->redirect('@domain');
    }
    if($request->hasParameter('_uc')) {
      $this->setPage(1);
      $filters = $this->getFilters();
      $filters['mode'] = 1;
      $this->setFilters($filters);
      $this->redirect('@domain');
    }
    if($request->hasParameter('_removed')) {
      $this->setPage(1);
      $filters = $this->getFilters();
      $filters['mode'] = 2;
      $this->setFilters($filters);
      $this->redirect('@domain');
    }
    parent::executeFilter($request);
  }

  protected function buildQuery()
  {
    $tableMethod = $this->configuration->getTableMethod();
    if (null === $this->filters)
    {
      $this->filters = $this->configuration->getFilterForm($this->getFilters());
    }

    $this->filters->setTableMethod($tableMethod);

    $query = $this->filters->buildQuery($this->getFilters());

    $sort = $this->getSort();
    if($sort[0]=='domain_id') {
      $dest = '';
      if($sort[1]=='desc') {
        $query->addOrderBy("IF(alias_domain_id, alias_domain_id-0.5, domain_id) desc");
      }
      else {
        $query->addOrderBy("IF(alias_domain_id, alias_domain_id+0.5, domain_id)");
      }
    }
    else {
      $alias = $query->getRootAlias();
      // this helps to avoid series of queries for fields data
      $query->select("$alias.*");

      $query->leftJoin("$alias.DomainAlias d2");

      $sortfield = $sort[0];

      $col_def = Doctrine::getTable('SmsDomain')->getColumnDefinition($sortfield);
      switch($col_def['type']) {
        case 'integer':
          if($sort[1]=='desc') {
            $query->addOrderBy("IF($alias.alias_domain_id, IF(d2.$sortfield, d2.$sortfield-0.5, CAST(d2.domain_id AS SIGNED)-10000.5), IF($alias.$sortfield, $alias.$sortfield, CAST($alias.domain_id AS SIGNED)-10000)) desc");
          }
          else {
            $query->addOrderBy("IF($alias.alias_domain_id, IF(d2.$sortfield, d2.$sortfield+0.5, CAST(d2.domain_id AS SIGNED)-9999.5), IF($alias.$sortfield, $alias.$sortfield, CAST($alias.domain_id AS SIGNED)-10000))");
          }
        break;
        default:
          if($sort[1]=='desc') {
            $query->addOrderBy("IF($alias.alias_domain_id, IF(LENGTH(d2.$sortfield), d2.$sortfield, CONCAT('0', d2.domain_id)), IF(LENGTH($alias.$sortfield), CONCAT($alias.$sortfield, '1'), CONCAT('0', $alias.domain_id, '1'))) DESC");
            //$query->addOrderBy("IF($alias.alias_domain_id, d2.$sortfield, CONCAT($alias.$sortfield, '1')) desc");
          }
          else {
            $query->addOrderBy("IF($alias.alias_domain_id, IF(LENGTH(d2.$sortfield), CONCAT(d2.$sortfield, '1'), CONCAT('0', d2.domain_id, '1')), IF(LENGTH($alias.$sortfield), $alias.$sortfield, CONCAT('0', $alias.domain_id)))");
//            $query->addOrderBy("IF($alias.alias_domain_id, CONCAT(d2.$sortfield,'1'), $alias.$sortfield)");
          }
      };
    }

    $event = $this->dispatcher->filter(new sfEvent($this, 'admin.build_query'), $query);
    $query = $event->getReturnValue();

    return $query;
  }

}
