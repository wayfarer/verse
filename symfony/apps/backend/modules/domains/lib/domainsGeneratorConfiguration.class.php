<?php

/**
 * domains module configuration.
 *
 * @package    verse3
 * @subpackage domains
 * @author     Vladimir Droznik
 * @version    SVN: $Id: configuration.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class domainsGeneratorConfiguration extends BaseDomainsGeneratorConfiguration
{
  public function getFilterDefaults() {
    return array('mode'=>0);
  }
}
