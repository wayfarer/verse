<link rel="stylesheet" type="text/css" href="/shadowbox/shadowbox.css">
<script type="text/javascript" src="/shadowbox/shadowbox.js"></script>
<script type="text/javascript">
    Shadowbox.init();
</script>
<?php use_helper('I18N', 'Date') ?>
<?php include_partial('moderate_movie/assets') ?>

<div id="sf_admin_container">
    <h1><?php echo __('Movies List', array(), 'messages') ?></h1>

  <?php include_partial('moderate_movie/flashes') ?>

  <div id="sf_admin_header">
    <?php include_partial('moderate_movie/list_header', array('pager' => $pager)) ?>
    <?php echo link_to('<< back to obituaries', 'plg_obituary')?> ::
    <?php echo link_to('Photos List', '/backend.php/moderate_photos', array()) ?> ::
    <?php echo link_to('Movies List', '/backend.php/moderate_movie', array()) ?>
  </div>

  <div id="sf_admin_bar">
    <?php include_partial('moderate_movie/filters', array('form' => $filters, 'configuration' => $configuration)) ?>
  </div>

  <div id="sf_admin_content">
    <form action="<?php echo url_for('plg_obituary_movie_collection', array('action' => 'batch')) ?>" method="post">
    <ul class="sf_admin_actions">
      <?php include_partial('moderate_movie/list_batch_actions', array('helper' => $helper)) ?>
      <?php include_partial('moderate_movie/list_actions', array('helper' => $helper)) ?>
    </ul>
    <?php include_partial('moderate_movie/list', array('pager' => $pager, 'sort' => $sort, 'helper' => $helper)) ?>
    <ul class="sf_admin_actions">
      <?php include_partial('moderate_movie/list_batch_actions', array('helper' => $helper)) ?>
      <?php include_partial('moderate_movie/list_actions', array('helper' => $helper)) ?>
    </ul>
    </form>
  </div>

  <div id="sf_admin_footer">
    <?php include_partial('moderate_movie/list_footer', array('pager' => $pager)) ?>
  </div>
</div>
