<td>
  <ul class="sf_admin_td_actions">
<?php if($plg_obituary_movie->getState() != 2): ?>
    <li class="sf_admin_action_hide">
      <?php echo link_to(__('Hide', array(), 'messages'), 'moderate_movie/hide?id='.$plg_obituary_movie->getId(), array()) ?>
    </li>
<?php endif; ?>    
<?php if($plg_obituary_movie->getState() == 1): ?>
    <li class="sf_admin_action_approve">
      <?php echo link_to(__('Approve', array(), 'messages'), 'moderate_movie/approve?id='.$plg_obituary_movie->getId(), array()) ?>
    </li>
<?php endif; ?>    
<?php if($plg_obituary_movie->getState() == 2): ?>
    <li class="sf_admin_action_unhide">
      <?php echo link_to(__('Unhide', array(), 'messages'), 'moderate_movie/unhide?id='.$plg_obituary_movie->getId(), array()) ?>
    </li>    
<?php endif; ?>
    <li class="sf_admin_action_edit">
        <?php echo link_to(__('Edit', array(), 'messages'), 'moderate_movie/edit?id='.$plg_obituary_movie->getId(), array()) ?>
    </li>
    <li class="sf_admin_action_delete">
        <?php echo $helper->linkToDelete($plg_obituary_movie, array(  'params' =>   array(  ),  'confirm' => 'Are you sure?',  'class_suffix' => 'delete',  'label' => 'Delete',)) ?>
    </li>
  </ul>
</td>
