<?php use_stylesheets_for_form($form) ?>
<?php use_javascripts_for_form($form) ?>

<div class="sf_admin_form">
  <?php echo form_tag_for($form, '@plg_obituary_movie') ?>
    <?php echo $form->renderHiddenFields(false) ?>

    <?php if ($form->hasGlobalErrors()): ?>
      <?php echo $form->renderGlobalErrors() ?>
    <?php endif; ?>

    <?php include_partial('moderate_movie/form_actions', array('plg_obituary_movie' => $plg_obituary_movie, 'form' => $form, 'configuration' => $configuration, 'helper' => $helper)) ?>

    <?php foreach ($configuration->getFormFields($form, $form->isNew() ? 'new' : 'edit') as $fieldset => $fields): ?>
      <?php include_partial('moderate_movie/form_fieldset', array('plg_obituary_movie' => $plg_obituary_movie, 'form' => $form, 'fields' => $fields, 'fieldset' => $fieldset)) ?>
    <?php endforeach; ?>

    <?php include_partial('moderate_movie/form_actions', array('plg_obituary_movie' => $plg_obituary_movie, 'form' => $form, 'configuration' => $configuration, 'helper' => $helper)) ?>
  </form>
</div>
<script>
swfu_widget.addObserver(swfu_widget.handlers, 'queue_complete', function(event) {
	document.location = '<?php echo url_for('@plg_obituary_movie'); ?>';
} );
</script>