<?php

/**
 * moderation_movie module configuration.
 *
 * @package    verse3
 * @subpackage moderation_movie
 * @author     Vladimir Droznik <vladimir.droznik@gmail.com>
 * @version    SVN: $Id: configuration.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class moderate_movieGeneratorConfiguration extends BaseModerate_movieGeneratorConfiguration
{
}
