<?php

require_once dirname(__FILE__).'/../lib/moderate_movieGeneratorConfiguration.class.php';
require_once dirname(__FILE__).'/../lib/moderate_movieGeneratorHelper.class.php';

/**
 * moderation_movie actions.
 *
 * @package    verse3
 * @subpackage moderation_movie
 * @author     Vladimir Droznik <vladimir.droznik@gmail.com>
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class moderate_movieActions extends autoModerate_movieActions
{
    public function executeCreate(sfWebRequest $request) {
        $this->form = $this->configuration->getForm();
        $this->plg_obituary_movie = $this->form->getObject();

        if($this->processForm($request, $this->form)!=sfView::NONE) {
            $this->setTemplate('new');
        } else {
            return sfView::NONE;
        }
    }

    protected function processForm(sfWebRequest $request, sfForm $form) {
        $plg_obituary_movie = $form->getObject();
        $is_new = $plg_obituary_movie->isNew();

        $taintedValues = $request->getParameter($form->getName());
        if (!$is_new) {
            $taintedValues['obituary_id'] = $plg_obituary_movie->getObituaryId();
            $taintedValues['path'] = $plg_obituary_movie->getPath();
        }
        $form->bind($taintedValues, $request->getFiles($form->getName()));

        if ($form->isValid()) {
            $notice = $is_new ? 'The item was created successfully.' : 'The item was updated successfully.';

            $files = $request->getFiles($form->getName());
            if($is_new && count( $files )) {
                $domain = Doctrine::getTable('SmsDomain')->getCurrent();

                $movie = verseMediaUtil::uploadFile( $domain->getDomainName(), null, array( 'media' => $files['path'] ) );
                $movie->save($form->getValue('obituary_id'), $domain->getDomainName(), $domain->getDomainId(), $form->getValue('state'), $form->getValue('caption'));

                $this->setLayout(false);
                return sfView::NONE;
            } else {
                try {
                    $movie = $form->save();
                } catch (Doctrine_Validator_Exception $e) {

                    $errorStack = $form->getObject()->getErrorStack();

                    $message = get_class($form->getObject()) . ' has ' . count($errorStack) . " field" . (count($errorStack) > 1 ?  's' : null) . " with validation errors: ";
                    foreach ($errorStack as $field => $errors) {
                        $message .= "$field (" . implode(", ", $errors) . "), ";
                    }
                    $message = trim($message, ', ');

                    $this->getUser()->setFlash('error', $message);
                    return sfView::SUCCESS;
                }
            }
            $this->getUser()->setFlash('notice', $notice);
            $this->redirect('@plg_obituary_movie');
        } else {
            $this->getUser()->setFlash('error', 'The item has not been saved due to some errors.', false);
        }
    }

    public function executeApprove(sfWebRequest $request) {
        $movie = $this->getRoute()->getObject();
        $movie->approve();
        $this->getUser()->setFlash('notice', 'The selected movie has been approved successfully.');
        $this->redirect('plg_obituary_movie');
    }

    public function executeHide(sfWebRequest $request) {
        $movie = $this->getRoute()->getObject();
        $movie->hide();
        $this->getUser()->setFlash('notice', 'The selected movie has been hidden successfully.');
        $this->redirect('plg_obituary_movie');
    }

    public function executeUnhide(sfWebRequest $request) {
        $movie = $this->getRoute()->getObject();
        $movie->unhide();
        $this->getUser()->setFlash('notice', 'The selected movie has been unhidden successfully.');
        $this->redirect('plg_obituary_movie');
    }

    public function executeDelete(sfWebRequest $request) {
        $movie = $this->getRoute()->getObject();
        $file = new verseMediaFile('files/'.Doctrine::getTable('SmsDomain')->getCurrent()->getDomainName().'/'.$movie->getPath());
        $file->delete();
        $movie->delete();
        $this->getUser()->setFlash('notice', 'The selected movie has been deleted successfully.');
        $this->redirect('plg_obituary_movie');
    }

    public function executeBatch(sfWebRequest $request) {
        $request->checkCSRFProtection();

        if (!$ids = $request->getParameter('ids'))
        {
          $this->getUser()->setFlash('error', 'You must at least select one item.');

          $this->redirect('plg_obituary_movie');
        }

        $action = null;
        foreach($request->getParameter('batch_action') as $batch_action) {
          if($batch_action) {
            $action = $batch_action;
            break;
          }
        }
        if (!$action)
        {
          $this->getUser()->setFlash('error', 'You must select an action to execute on the selected items.');

          $this->redirect('plg_obituary_movie');
        }

        if (!method_exists($this, $method = 'execute'.ucfirst($action)))
        {
          throw new InvalidArgumentException(sprintf('You must create a "%s" method for action "%s"', $method, $action));
        }

        if (!$this->getUser()->hasCredential($this->configuration->getCredentials($action)))
        {
          $this->forward(sfConfig::get('sf_secure_module'), sfConfig::get('sf_secure_action'));
        }

        $validator = new sfValidatorDoctrineChoice(array('multiple' => true, 'model' => 'PlgObituaryMovie'));
        try
        {
            // validate ids
          $ids = $validator->clean($ids);

          // execute batch
          $this->$method($request);
        }
        catch (sfValidatorError $e)
        {
          $this->getUser()->setFlash('error', 'A problem occurs when deleting the selected items as some items do not exist anymore.');
        }

        $this->redirect('plg_obituary_movie');
    }

    public function executeBatchHide(sfWebRequest $request) {
        $ids = $request->getParameter('ids');
        $q = Doctrine::getTable('PlgObituaryMovie')->createQuery()
          ->whereIn('id', $ids);
        foreach ($q->execute() as $movie) {
          $movie->hide();
        }
        $this->getUser()->setFlash('notice', 'The selected movies have been hidden successfully.');
        $this->redirect('plg_obituary_movie');
    }

    public function executeBatchUnhide(sfWebRequest $request) {
        $ids = $request->getParameter('ids');
        $q = Doctrine::getTable('PlgObituaryMovie')->createQuery()
          ->whereIn('id', $ids);
        foreach ($q->execute() as $movie) {
          $movie->unhide();
        }
        $this->getUser()->setFlash('notice', 'The selected movies have been unhidden successfully.');
        $this->redirect('plg_obituary_movie');
    }

    public function executeBatchApprove(sfWebRequest $request) {
        $ids = $request->getParameter('ids');
        $q = Doctrine::getTable('PlgObituaryMovie')->createQuery()
          ->whereIn('id', $ids);
        foreach ($q->execute() as $movie) {
          $movie->approve();
        }
        $this->getUser()->setFlash('notice', 'The selected movies have been approved successfully.');
        $this->redirect('plg_obituary_movie');
    }

    public function executeBatchDelete(sfWebRequest $request) {
        $ids = $request->getParameter('ids');
        $q = Doctrine::getTable('PlgObituaryMovie')->createQuery()
          ->whereIn('id', $ids);
        $domain_name = Doctrine::getTable('SmsDomain')->getCurrent()->getDomainName();
        foreach ($q->execute() as $movie) {
            $file = new verseMediaFile('files/'.$domain_name.'/'.$movie->getPath());
            $file->delete();
            $movie->delete();
        }
        $this->getUser()->setFlash('notice', 'The selected movies have been deleted successfully.');
        $this->redirect('plg_obituary_movie');
    }

    protected function getFilters() {
        return $this->getUser()->getAttribute('moderate_media.filters', $this->configuration->getFilterDefaults(), 'admin_module');
    }

    protected function setFilters(array $filters) {
        return $this->getUser()->setAttribute('moderate_media.filters', $filters, 'admin_module');
    }

    public function executeFilter(sfWebRequest $request) {
        $this->setPage(1);

        if ($request->hasParameter('_reset')) {
            $this->setFilters($this->configuration->getFilterDefaults());
            $this->redirect('plg_obituary_movie');
        }

        if($request->hasParameter('obituary')) {
            $this->getUser()->setAttribute('movie_obituary_id', $request->getParameter('obituary'));
            $this->getUser()->setAttribute('photo_obituary_id', $request->getParameter('obituary'));
            $filters = $this->configuration->getFilterDefaults();
            $filters['obituary_id'] = intval($request->getParameter('obituary'));
            $filters['state'] = 0;
            $this->setFilters($filters);
            $this->redirect('plg_obituary_movie');
        }

        if($request->hasParameter('_active_movies')) {
            $filters = $this->getFilters();
            $filters['state'] = 0;
            $this->setFilters($filters);
            $this->redirect('plg_obituary_movie');
        }

        if($request->hasParameter('_moderated_movies')) {
            $filters = $this->configuration->getFilterDefaults();
            $filters['state'] = 1;
            unset($filters['obituary_id']);
            $this->getUser()->setAttribute('movie_obituary_id', '');
            $this->getUser()->setAttribute('photo_obituary_id', '');
            $this->setFilters($filters);
            $this->redirect('plg_obituary_movie');
        }

        if($request->hasParameter('_hidden_movies')) {
            $filters = $this->getFilters();
            $filters['state'] = 2;
            $this->setFilters($filters);
            $this->redirect('plg_obituary_movie');
        }

        if($request->hasParameter('_unknown_movies')) {
            $filters = $this->getFilters();
            $filters['status'] = 0;
            $this->setFilters($filters);
            $this->redirect('plg_obituary_movie');
        }

        if($request->hasParameter('_ready_movies')) {
            $filters = $this->getFilters();
            $filters['status'] = 1;
            $this->setFilters($filters);
            $this->redirect('plg_obituary_movie');
        }

        if($request->hasParameter('_converting_movies')) {
            $filters = $this->getFilters();
            $filters['status'] = 2;
            $this->setFilters($filters);
            $this->redirect('plg_obituary_movie');
        }

        if($request->hasParameter('_invalid_movies')) {
            $filters = $this->getFilters();
            $filters['status'] = 3;
            $this->setFilters($filters);
            $this->redirect('plg_obituary_movie');
        }

        $this->filters = $this->configuration->getFilterForm($this->getFilters());

        $this->filters->bind($request->getParameter($this->filters->getName()));
        if ($this->filters->isValid()) {
            $this->setFilters($this->filters->getValues());
            $this->redirect('plg_obituary_movie');
        }

        $this->pager = $this->getPager();
        $this->sort = $this->getSort();

        $this->setTemplate('index');
    }
}
