<?php

require_once dirname(__FILE__).'/../lib/movieclipsGeneratorConfiguration.class.php';
require_once dirname(__FILE__).'/../lib/movieclipsGeneratorHelper.class.php';

/**
 * movieclips actions.
 *
 * @package    verse3
 * @subpackage movieclips
 * @author     Vladimir Droznik
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class movieclipsActions extends autoMovieclipsActions
{
  public function executeUpdate(sfWebRequest $request)
  {
    $this->plg_movieclip = $this->getRoute()->getObject();
    $this->form = $this->configuration->getForm($this->plg_movieclip);

    if($this->processForm($request, $this->form)!=sfView::NONE) {
      $this->setTemplate('edit');
    }
    else {
      return sfView::NONE;
    }
  }

  public function executeCreate(sfWebRequest $request)
  {
    $this->form = $this->configuration->getForm();
    $this->plg_movieclip = $this->form->getObject();

    if($this->processForm($request, $this->form)!=sfView::NONE) {
      $this->setTemplate('new');
    }
    else {
      return sfView::NONE;
    }
  }

  protected function processForm(sfWebRequest $request, sfForm $form)
  {
    $form->bind($request->getParameter($form->getName()), $request->getFiles($form->getName()));
    if ($form->isValid())
    {

      $notice = $form->getObject()->isNew() ? 'The item was created successfully.' : 'The item was updated successfully.';

      try {
        $plg_movieclip = $form->save();
      } catch (Doctrine_Validator_Exception $e) {

        $errorStack = $form->getObject()->getErrorStack();

        $message = get_class($form->getObject()) . ' has ' . count($errorStack) . " field" . (count($errorStack) > 1 ?  's' : null) . " with validation errors: ";
        foreach ($errorStack as $field => $errors) {
            $message .= "$field (" . implode(", ", $errors) . "), ";
        }
        $message = trim($message, ', ');

        $this->getUser()->setFlash('error', $message);
        return sfView::SUCCESS;
      }

      $this->dispatcher->notify(new sfEvent($this, 'admin.save_object', array('object' => $plg_movieclip)));

      $this->getUser()->setFlash('notice', $notice);

      if(!count( $request->getFiles($form->getName()) )) {
        $this->redirect('@movieclip');
      }
      else {
        $this->setLayout(false);
        return sfView::NONE;
      }
    }
    else
    {
      $this->getUser()->setFlash('error', 'The item has not been saved due to some errors.', false);
    }
  }

  public function executeDelete(sfWebRequest $request)
  {
    $request->checkCSRFProtection();

    $this->dispatcher->notify(new sfEvent($this, 'admin.delete_object', array('object' => $this->getRoute()->getObject())));

    if ($this->getRoute()->getObject()->delete())
    {
	    // delete movieclip - obituary associations
	    $conn = Doctrine_Manager::connection();
	    $conn->standaloneQuery("DELETE FROM plg_obituary_movieclip WHERE movieclip_id = ?", array($this->getRoute()->getObject()->getMovieclipId()));
    	$this->getUser()->setFlash('notice', 'The item was deleted successfully.');
    }

    $this->redirect('@movieclip');
  }

}
