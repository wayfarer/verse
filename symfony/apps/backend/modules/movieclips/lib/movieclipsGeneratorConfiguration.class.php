<?php

/**
 * movieclips module configuration.
 *
 * @package    verse3
 * @subpackage movieclips
 * @author     Vladimir Droznik
 * @version    SVN: $Id: configuration.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class movieclipsGeneratorConfiguration extends BaseMovieclipsGeneratorConfiguration
{
}
