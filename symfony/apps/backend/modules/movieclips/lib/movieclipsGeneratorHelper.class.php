<?php

/**
 * movieclips module helper.
 *
 * @package    verse3
 * @subpackage movieclips
 * @author     Vladimir Droznik
 * @version    SVN: $Id: helper.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class movieclipsGeneratorHelper extends BaseMovieclipsGeneratorHelper
{
  public function linkToPreview($object, $params) {
    return '<li class="sf_admin_action_preview">'.link_to(__($params['label'], array(), 'sf_admin'), '@movieclip', array('onClick'=>'movieclip_preview('.$object->getMovieclipId().'); return false;')).'</li>';
  }
}
