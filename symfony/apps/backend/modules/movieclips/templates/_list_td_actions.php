<td>
  <ul class="sf_admin_td_actions">
    <?php echo $helper->linkToPreview($plg_movieclip, array( 'label' => 'Preview')) ?>
    <?php echo $helper->linkToEdit($plg_movieclip, array(  'params' =>   array(  ),  'class_suffix' => 'edit',  'label' => 'Edit',)) ?>
    <?php echo $helper->linkToDelete($plg_movieclip, array(  'params' =>   array(  ),  'confirm' => 'Are you sure?',  'class_suffix' => 'delete',  'label' => 'Delete',)) ?>
  </ul>
</td>
