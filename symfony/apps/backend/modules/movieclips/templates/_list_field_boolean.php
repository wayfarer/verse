<?php if ($value): ?>
  <?php echo image_tag('/img/enabled.png', array('alt' => __('Enabled', array(), 'sf_admin'), 'title' => __('Enabled', array(), 'sf_admin'))) ?>
<?php else: ?>
  &nbsp;
<?php endif; ?>
