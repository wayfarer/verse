<?php

/**
 * statistics actions.
 *
 * @package    verse3
 * @subpackage statistics
 * @author     Vladimir Droznik
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class statisticsActions extends sfActions
{
 /**
  * Executes index action
  *
  * @param sfRequest $request A request object
  */
  public function executeIndex(sfWebRequest $request)
  {
    if(!sfContext::getInstance()->getUser()->getProfile()->getPiwikTokenAuth()) {
      $pa = new PiwikAdapter();
      $pa->ensureUser($this->getUser());
    }

    $currentPeriod = $request->getParameter('period', 'day');
    $today = $request->getParameter('date');
    if($today) {
      $today = strtotime($today);
    }
    else {
      $today = time() - 86400;
    }

    $availablePeriods = array('day', 'week', 'month', 'year');
    if(!in_array($currentPeriod,$availablePeriods)) {
      $currentPeriod = 'day';
    }
    $periodNames = array(
    'day' => array('singular' => 'Day', 'plural' => 'Days'),
    'week' => array('singular' => 'Week', 'plural' => 'Weeks'),
    'month' => array('singular' => 'Month', 'plural' => 'Monthes'),
    'year' => array('singular' => 'Year', 'plural' => 'Years')
    );

    $found = array_search($currentPeriod, $availablePeriods);
    if($found !== false)
    {
      unset($availablePeriods[$found]);
    }

    $minDate = strtotime("2010-01-01");
    $maxDate = time();

    switch($currentPeriod) {
      case 'day':
        $this->prettyDate = date('l, jS F Y', $today);
        break;
      case 'week':
        $week_begin = strtotime('last Monday', $today+86400);
        $week_end = strtotime('next Sunday', $today-86400);
        $this->prettyDate = date('\W\e\e\k W \o\f Y', $today).' ('.date('jS F', $week_begin).' - '.date('jS F', $week_end).')';
        break;
      case 'month':
        $this->prettyDate = date('F Y', $today);
        break;
      case 'year':
        $this->prettyDate = date('Y', $today);
        break;
    }

    $this->minDateYear = date("Y", $minDate);
    $this->minDateMonth = date("m", $minDate);
    $this->minDateDay = date("d", $minDate);

    $this->maxDateYear = date("Y", $maxDate);
    $this->maxDateMonth = date("m", $maxDate);
    $this->maxDateDay = date("d", $maxDate);

    $this->date = date("Y-m-d", $today);

    $this->period = $currentPeriod;
    $this->otherPeriods = $availablePeriods;
    $this->periodsNames = $periodNames;

    $this->keep_phpmv = 0;

  }
}
