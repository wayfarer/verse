<div id="sf_admin_container">
<h1>Website statistics</h1>
<div style="float:right">
<a href="https://www.google.com/analytics/web/#report/visitors-overview" target="_blank">View Google Analitics Statistics</a><br/>
<a href="#more_options">Click here for pre-2010 statistics and more statistics options</a>
</div>
<script type="text/javascript">
  var piwik = {};
  piwik.token_auth = "<?php $sf_user->getProfile()->getPiwikTokenAuth()?>";
  piwik.piwik_url = "";
  piwik.period = "<?php echo $period?>";
  piwik.currentDateString = "<?php echo $date?>";
  piwik.minDateYear = <?php echo $minDateYear ?>;
  piwik.minDateMonth = parseInt("<?php echo $minDateMonth?>", 10);
  piwik.minDateDay = parseInt("<?php echo $minDateDay?>", 10);
  piwik.maxDateYear = <?php echo $maxDateYear?>;
  piwik.maxDateMonth = parseInt("<?php echo $maxDateMonth?>", 10);
  piwik.maxDateDay = parseInt("<?php echo $maxDateDay?>", 10);
</script>

<script type="text/javascript" src="/js/piwik/calendar.js"></script>
<script type="text/javascript" src="/js/piwik/date.js"></script>

<div id="periodString">
  <span id="date"><img src='/img/icon-calendar.gif' style="vertical-align:middle" alt="" /> <?php echo $prettyDate?></span> -&nbsp;
  <span id="periods">
    <span id="currentPeriod"><?php echo $periodsNames[$period]['singular']?></span>
    <span id="otherPeriods">
      <?php foreach($otherPeriods as $thisPeriod):?>
 | <a href='?period=<?php echo $thisPeriod?>'><?php echo $periodsNames[$thisPeriod]['singular']?></a>
      <?php endforeach ?>
    </span>
  </span>
  <br/>
  <span id="datepicker"></span>
</div>

<script language="javascript">
$(document).ready(function() {
     // this will trigger to change only the period value on search query and hash string.
     $("#otherPeriods a").bind('click',function(e) {
        e.preventDefault();
        var request_URL = $(e.target).attr("href");
        var new_period = getUrlParamValue('period', request_URL);
        propagateNewPage('period='+new_period);
     });
});</script>

<div style="clear:both"></div>

<?php if($sf_user->getProfile()->getPiwikTokenAuth()): ?>
<div id="widgetIframe">
<?php
  $dashboard_url = urlencode("index.php?module=Widgetize&action=iframe&moduleToWidgetize=Dashboard&actionToWidgetize=index&idSite=".($sf_user->isSuperAdmin()?"1":Doctrine::getTable('SmsDomain')->getCurrent()->getDomainId())."&period=$period&date=$date");
?>
<iframe src="https://twintierstech.net/piwik/index.php?module=VerseIntegration&action=logme_token&login=<?php echo $sf_user->getUsername()?>.<?php echo Doctrine::getTable('SmsDomain')->getCurrent()->getDomainName()?>&token_auth=<?php echo $sf_user->getProfile()->getPiwikTokenAuth()?>&url=<?php echo $dashboard_url?>" frameborder="0" marginheight="0" marginwidth="0" width="100%" height="1300"></iframe>
</div>
<?php endif ?>
</div>
<br/>
<a name="more_options"></a>
<h3>We are upgrading our statistics services!</h3>
<p>We have started to collect statistics using a new software called Piwik since January 1, 2010.
<em>(You can see it in action above)</em></p>

<p>We are still collecting stats using the old software (phpMyVisites),
but we are planning to stop collecting new statistics with phpMyVisities
(though you will still <strong>be able to access your old historical data</strong> in phpMyVisites).</p>

<p>With this system, if you would like to compare statistics from 2009 and earlier with statistics from 2010
and on you will not be able to do so using one statistics system (you will need to compare the numbers
from each program separately). If you would like to be able to continue to use phpMyVisites to collect
current statistics so that you may continue to compare numbers from before 2010 with the numbers after 2010
in the same graph, we can enable that for you.</p>

<p>If you would like to us to enable phpMyVisites to continue to collect data for you, please check the box
below and we will contact you regading this.</p>

<strong><label for="keep_phpmv">Contact me about continuing phpMyVisites data collection:</label> <input type="checkbox" name="keep_phpmv" id="keep_phpmv" onclick="keep_phpmv()"<?php if($keep_phpmv):?> checked<?php endif?>/></strong><br/><br/>
Now you can:<br/>
<h2><a href="/phpmv_login_v3.php">go to phpMyVisites (statistics for pre 2010 periods)</a></h2>
<h2><a href="/piwik_login_v3.php">go to full Piwik UI (statistics since Jan 1st, 2010)</a></h2>
<br/><br/><br/><br/>
<script>
  function keep_phpmv() {
    $.post("statistics.php", {action: "keep_phpmv", keep_phpmv: $("#keep_phpmv").attr('checked')?1:0});
    $("#keep_phpmv").after(" saved!");
  }
</script>
