<?php

require_once dirname(__FILE__).'/../lib/block_keywordGeneratorConfiguration.class.php';
require_once dirname(__FILE__).'/../lib/block_keywordGeneratorHelper.class.php';

/**
 * block_keyword actions.
 *
 * @package    verse3
 * @subpackage block_keyword
 * @author     Vladimir Droznik <vladimir.droznik@gmail.com>
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class block_keywordActions extends autoBlock_keywordActions {
    
}
