<?php

/**
 * block_keyword module configuration.
 *
 * @package    verse3
 * @subpackage block_keyword
 * @author     Vladimir Droznik <vladimir.droznik@gmail.com>
 * @version    SVN: $Id: configuration.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class block_keywordGeneratorConfiguration extends BaseBlock_keywordGeneratorConfiguration {
    
}
