<td class="sf_admin_text sf_admin_list_td_keyword">
  <?php echo $plg_block_keyword->getKeyword() ?>
</td>
<?php if (sfContext::getInstance()->getUser()->isSuperAdmin()): ?>
    <td class="sf_admin_text sf_admin_list_td_domain_name">
        <?php echo $plg_block_keyword->getDomainName() ?>
    </td>
<?php endif; ?>