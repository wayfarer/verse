<?php use_helper('I18N', 'Date') ?>
<?php include_partial('block_keyword/assets') ?>

<div id="sf_admin_container">
  <h1><?php echo __('Block Keyword List', array(), 'messages') ?></h1>

  <div id="verse_submenu">
    <?php echo link_to('<< back to obituaries', 'plg_obituary')?> ::
    <?php echo link_to('Block IP List', '/backend.php/block_ip') ?> ::
    <?php echo link_to('Block Keyword List', '/backend.php/block_keyword') ?>
  </div>

  <?php include_partial('block_keyword/flashes') ?>

  <div id="sf_admin_header">
    <?php include_partial('block_keyword/list_header', array('pager' => $pager)) ?>
  </div>

  <div id="sf_admin_bar">
    <?php include_partial('block_keyword/filters', array('form' => $filters, 'configuration' => $configuration)) ?>
  </div>

  <div id="sf_admin_content">
    <form action="<?php echo url_for('plg_block_keyword_collection', array('action' => 'batch')) ?>" method="post">
    <ul class="sf_admin_actions">
      <?php include_partial('block_keyword/list_batch_actions', array('helper' => $helper)) ?>
      <?php include_partial('block_keyword/list_actions', array('helper' => $helper)) ?>
    </ul>
    <?php include_partial('block_keyword/list', array('pager' => $pager, 'sort' => $sort, 'helper' => $helper)) ?>
    <ul class="sf_admin_actions">
      <?php include_partial('block_keyword/list_batch_actions', array('helper' => $helper)) ?>
      <?php include_partial('block_keyword/list_actions', array('helper' => $helper)) ?>
    </ul>
    </form>
  </div>

  <div id="sf_admin_footer">
    <?php include_partial('block_keyword/list_footer', array('pager' => $pager)) ?>
  </div>
</div>
