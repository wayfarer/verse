<?php slot('sf_admin.current_header') ?>
<th class="sf_admin_text sf_admin_list_th_keyword">
  <?php if ('keyword' == $sort[0]): ?>
    <?php echo link_to(__('Keyword', array(), 'messages'), '@plg_block_keyword', array('query_string' => 'sort=keyword&sort_type='.($sort[1] == 'asc' ? 'desc' : 'asc'))) ?>
    <?php echo image_tag(sfConfig::get('sf_admin_module_web_dir').'/images/'.$sort[1].'.png', array('alt' => __($sort[1], array(), 'sf_admin'), 'title' => __($sort[1], array(), 'sf_admin'))) ?>
  <?php else: ?>
    <?php echo link_to(__('Keyword', array(), 'messages'), '@plg_block_keyword', array('query_string' => 'sort=keyword&sort_type=asc')) ?>
  <?php endif; ?>
</th>
<?php end_slot(); ?>
<?php include_slot('sf_admin.current_header') ?>

<?php if (sfContext::getInstance()->getUser()->isSuperAdmin()): ?>
    <?php slot('sf_admin.current_header') ?>
    <th class="sf_admin_text sf_admin_list_th_domain_name">
        <?php echo __('Domain name', array(), 'messages') ?>
    </th>
    <?php end_slot(); ?>
    <?php include_slot('sf_admin.current_header') ?>
<?php endif; ?>