<?php

require_once dirname(__FILE__).'/../lib/eventsGeneratorConfiguration.class.php';
require_once dirname(__FILE__).'/../lib/eventsGeneratorHelper.class.php';

/**
 * events actions.
 *
 * @package    verse3
 * @subpackage events
 * @author     Vladimir Droznik
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class eventsActions extends autoEventsActions
{
}
