<?php use_helper('I18N') ?>
<div id="login-container">
    <div id="login-title">
        <img src="/img/ttt-web.png" />
    </div>
    <div id="login-form">
        <form action="<?php echo url_for('@sf_guard_signin') ?>" method="post">
            <table>
                <?php echo $form ?>
                <tr><td></td>
                    <td>
                        <input type="submit" value="<?php echo __('Login') ?>" />
                    </td>
                </tr>
            </table>
            <!-- <a href="<?php echo url_for('@sf_guard_password') ?>"><?php echo __('Forgot your password?') ?></a> -->
        </form>
    </div>
    <div id="message">
        <h1>Welcome to your website's Content Management System!</h1>
        <p><b>Supported Browsers:</b> Mozilla Firefox, Google Chrome, Internet Explorer 9 and 10 with compatibilty mode, Internet Explorer 7 and 8.</p>
        <p>View our <a target="_blank" href="http://mymims.net/web-support">tutorials</a>.</p>
        <!--<h1>Announcements</h1>
        <p><b>LONG DAY, MONTH DAY, YEAR</b></p>
        <p>MESSAGE</p>
        <p>Wayne Boka, <i>Website Development Manager</i></p>-->
    </div>
</div>