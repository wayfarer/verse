<?php use_helper('Thumbnail') ?>
<?php // use_javascript('sortlist'); ?>

<div id="sf_admin_container">
<h1>Scrapbook Images</h1>
<div id="verse_submenu">
  <?php echo link_to('<< back to obituaries', 'plg_obituary')?>
</div>
<?php if(count($slides)): ?>
  <a href="<?php echo url_for('scrapbook/new?obituary_id='.$obituary_id) ?>">Add image</a> :: 
  <?php echo link_to('Delete all images', '/backend.php/obituaries/'.$obituary_id.'/scrapbook/delete_all', array('confirm' => 'Are you sure?')) ?>
  <ul id="scrapbook-images-list">
  <?php foreach ($slides as $slide): ?>
    <li id="slide_id-<?php echo $slide->getSlideId()?>">
      <?php echo image_tag(thumbnail($slide->getImage(), 150)) ?><br/>
      <strong><?php echo $slide->getDescription() ?></strong><br/>
      <?php echo link_to('edit', 'scrapbook/edit?slide_id='.$slide->getSlideId().'&obituary_id='.$slide->getObituaryId()) ?> ::
      <?php echo link_to('delete', 'scrapbook/delete?slide_id='.$slide->getSlideId().'&obituary_id='.$slide->getObituaryId(), array('method' => 'delete', 'confirm' => 'Are you sure?')) ?>
    </li>
  <?php endforeach; ?>
  </ul>
<?php else: ?>
Scrapbook is empty<br/><br/>
<?php endif; ?>
<a href="<?php echo url_for('scrapbook/new?obituary_id='.$obituary_id) ?>">Add image</a>
<?php if(count($slides)): ?>
     :: <?php echo link_to('Delete all images', '/backend.php/obituaries/'.$obituary_id.'/scrapbook/delete_all', array('confirm' => 'Are you sure?')) ?>
<?php endif; ?>
</div>