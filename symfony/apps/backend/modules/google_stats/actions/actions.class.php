<?php

require_once dirname(__FILE__).'/../lib/google_statsGeneratorConfiguration.class.php';
require_once dirname(__FILE__).'/../lib/google_statsGeneratorHelper.class.php';

/**
 * google_stats actions.
 *
 * @package    verse3
 * @subpackage google_stats
 * @author     Vladimir Droznik <vladimir.droznik@gmail.com>
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class google_statsActions extends autoGoogle_statsActions {
    
}
