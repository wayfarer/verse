<?php use_helper('I18N', 'Date') ?>
<?php include_partial('google_stats/assets') ?>

<div id="sf_admin_container">
  <h1><?php echo __('Google Analytics', array(), 'messages') ?></h1>

  <?php include_partial('google_stats/flashes') ?>

  <div id="sf_admin_header">
    <?php include_partial('google_stats/list_header', array('pager' => $pager)) ?>
    <?php echo '<a href="/structure.php"><< back to site structure</a>'?>
  </div>


  <div id="sf_admin_content">
    <ul class="sf_admin_actions">
      <?php include_partial('google_stats/list_batch_actions', array('helper' => $helper)) ?>
      <?php include_partial('google_stats/list_actions', array('helper' => $helper)) ?>
    </ul>
    <?php include_partial('google_stats/list', array('pager' => $pager, 'sort' => $sort, 'helper' => $helper)) ?>
    <ul class="sf_admin_actions">
      <?php include_partial('google_stats/list_batch_actions', array('helper' => $helper)) ?>
      <?php include_partial('google_stats/list_actions', array('helper' => $helper)) ?>
    </ul>
  </div>

  <div id="sf_admin_footer">
    <?php include_partial('google_stats/list_footer', array('pager' => $pager)) ?>
  </div>
</div>
