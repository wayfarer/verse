<div class="sf_admin_list">
  <?php if (!$pager->getNbResults()): ?>
    <p><?php echo __('No result', array(), 'sf_admin') ?></p>
  <?php else: ?>
    <table cellspacing="0">
      <thead>
        <tr>
          <?php include_partial('google_stats/list_th_tabular', array('sort' => $sort)) ?>
          <th id="sf_admin_list_th_actions">
          <?php if ($pager->haveToPaginate()): ?>
            <?php include_partial('google_stats/pagination', array('pager' => $pager)) ?>
          <?php else: ?>
            <?php echo __('Actions', array(), 'sf_admin') ?>
          <?php endif; ?>
          </th>
        </tr>
      </thead>
      <tfoot>
        <tr>
          <th colspan="2">
            <?php if ($pager->haveToPaginate()): ?>
              <?php include_partial('google_stats/pagination', array('pager' => $pager)) ?>
            <?php endif; ?>

            <?php if ($pager->haveToPaginate()): ?>
              <?php echo __('(page %%page%%/%%nb_pages%%)', array('%%page%%' => $pager->getPage(), '%%nb_pages%%' => $pager->getLastPage()), 'sf_admin') ?>
            <?php endif; ?>
          </th>
        </tr>
      </tfoot>
      <tbody>
        <?php foreach ($pager->getResults() as $i => $cms_site): $odd = fmod(++$i, 2) ? 'odd' : 'even' ?>
          <tr class="sf_admin_row <?php echo $odd ?>">
            <?php include_partial('google_stats/list_td_tabular', array('cms_site' => $cms_site)) ?>
            <?php include_partial('google_stats/list_td_actions', array('cms_site' => $cms_site, 'helper' => $helper)) ?>
          </tr>
        <?php endforeach; ?>
      </tbody>
    </table>
  <?php endif; ?>
</div>
<script type="text/javascript">
/* <![CDATA[ */
function checkAll()
{
  var boxes = document.getElementsByTagName('input'); for(var index = 0; index < boxes.length; index++) { box = boxes[index]; if (box.type == 'checkbox' && box.className == 'sf_admin_batch_checkbox') box.checked = document.getElementById('sf_admin_list_batch_checkbox').checked } return true;
}
/* ]]> */
</script>
