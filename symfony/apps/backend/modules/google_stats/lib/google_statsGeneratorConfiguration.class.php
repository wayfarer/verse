<?php

/**
 * google_stats module configuration.
 *
 * @package    verse3
 * @subpackage google_stats
 * @author     Vladimir Droznik <vladimir.droznik@gmail.com>
 * @version    SVN: $Id: configuration.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class google_statsGeneratorConfiguration extends BaseGoogle_statsGeneratorConfiguration
{
}
