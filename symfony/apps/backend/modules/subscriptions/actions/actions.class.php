<?php

require_once dirname(__FILE__).'/../lib/subscriptionsGeneratorConfiguration.class.php';
require_once dirname(__FILE__).'/../lib/subscriptionsGeneratorHelper.class.php';

/**
 * subscriptions actions.
 *
 * @package    verse3
 * @subpackage subscriptions
 * @author     Vladimir Droznik
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class subscriptionsActions extends autoSubscriptionsActions
{
}
