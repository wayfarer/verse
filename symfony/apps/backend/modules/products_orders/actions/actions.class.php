<?php

require_once dirname(__FILE__).'/../lib/products_ordersGeneratorConfiguration.class.php';
require_once dirname(__FILE__).'/../lib/products_ordersGeneratorHelper.class.php';

/**
 * products_orders actions.
 *
 * @package    verse3
 * @subpackage products_orders
 * @author     Vladimir Droznik
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class products_ordersActions extends autoProducts_ordersActions
{
  public function executeView(sfWebRequest $request) {
    $this->order = $this->getRoute()->getObject();
  }

  public function executeAction($request) {
      // autocomplete for order_domain_name filter field
      if($request->getParameter('order_id') == 'order_domain') {
          $this->getResponse()->setContentType('application/json');
          $fds = VerseUtil::getSuggestions('PlgProductOrder', 'order_domain_name', $request->getParameter('q'), $request->getParameter('limit'));
          return $this->renderText(json_encode($fds));
      }
  }
}
