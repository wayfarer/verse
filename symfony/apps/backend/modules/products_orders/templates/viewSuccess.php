<?php
//  var_dump(unserialize( htmlspecialchars_decode($order->getUserData()) ));
?>
<div id="ecommerce">
    <h2>Order Details</h2>
    <table cellpadding="0" cellspacing="0" class="ecommerce_order">
        <?php
        // TODO: rewrite the code using template syntax
        $i = 1;
        $u = unserialize( htmlspecialchars_decode($order->getUserData()) );
        //  $u["status"] = $order->getStatus();
        while(isset($u["item_name$i"])) {
//    echo '<tr><td>'.link_to($u["item_name$i"], '@product_show?product_id='.$u["item_number$i"]).'</td><td>x'.$u["quantity$i"].'</td><td>$'.$u["mc_gross_$i"].'</td></tr>';
            echo '<tr><td>'.$u["item_name$i"].'</td><td>x'.$u["quantity$i"].'</td><td>$'.$u["mc_gross_$i"].'</td></tr>';
            $i++;
        }
        echo '<tr><td colspan="3"><hr size="1"></td></tr>';
        echo '<tr><td>Shipping ('.$u['shipping_method'].'):</td><td></td><td>$'.$u["mc_shipping"].'</td></tr>';
        echo '<tr><td>Taxes:</td><td></td><td>$'.$u["tax"].'</td></tr>';
        echo '<tr><td>Total:</td><td></td><td><b>$'.$u["mc_gross"].'</b></td></tr>';
        ?>
    </table>
    <?php if(@$u['memo']): ?>
        <strong>Memo: <?php echo $u['memo']?></strong><br/><br/>
    <?php endif ?>
    <?php $i = 1; while(isset($u["option_name{$i}_1"])): ?>
        <?php echo $u["option_name{$i}_1"]?>: <strong><?php echo $u["option_selection{$i}_1"]?></strong><br/><br/>
        <?php $i++; endwhile ?>

    <strong><u>Status: <?php echo $order->getStatus()?></u></strong><br/><br/>
    <strong>Receipt ID: <?php echo $u['receipt_id']?></strong>

    <h2>User Details</h2>
    <table cellpadding="0" cellspacing="0" class="ecommerce_order">
        <?php
        $fields = array("From"=>array("first_name", "last_name"), "To"=>"address_name", "Street"=>"address_street", "City"=>"address_city", "State"=>"address_state", "ZIP"=>"address_zip", "Country"=>"address_country", "Email"=>"payer_email", "Pending reason"=>"pending_reason");
        echo '';
        foreach($fields as $title=>$field) {
            if(!is_array($field)) {
                echo "<tr><td>$title</td><td>".(@$u[$field]?@$u[$field]:'N/A')."</td></tr>";
            }
            else {
                echo "<tr><td>$title</td><td>";
                foreach($field as $field_part) {
                    echo @$u[$field_part]," ";
                }
                echo '</td></tr>';
            }
        }
        ?>
    </table>
    <?php if(($affiliate=$order->getOrderAffiliate())!=''):?>
        <h2>Affiliate</h2>
        <?php echo $affiliate?>, from: <?php echo $order->getAffiliateFrom()?><br/><br/>
    <?php endif?>
    <?php echo button_to('OK', '@product_order') ?>
</div>