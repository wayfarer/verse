<?php

class frontendConfiguration extends sfApplicationConfiguration
{
  public function configure()
  {
    $this->dispatcher->connect('obituary.create_candle', array('obituariesHelper', 'onCandleCreate'));
    $this->dispatcher->connect('obituary.publish_candle', array('obituariesHelper', 'onCandlePublish'));
  }
}
