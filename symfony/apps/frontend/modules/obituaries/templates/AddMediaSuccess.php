<?php use_stylesheets_for_form($form) ?>
<?php use_javascripts_for_form($form) ?>
<link rel="stylesheet" href="/css/swfupload.custom.css" type="text/css" />
<script type="text/javascript">
    var empty_caption = '<?php echo verseMediaUtil::EMPTY_CAPTION; ?>';

    function removefile(link) {
        $.post("<?php echo url_for('obituaries_removefile', $obituary) ?>", {
            'filename':$(link).attr('filename')
        });
        $(link).parent().parent().remove();
    }

    function onLoad() {
        // swfu_widget.fire(swfu_widget.handlers, 'swfobject_loaded');
        // $('.swfupload-wrapper').hide(); //hide widget, but anyway call him to turn the input field to the swf object

        //Show already uploaded files
        $.post("<?php echo url_for('obituaries_getuploadedfiles', $obituary) ?>", function(data){
            $.each(data, function(name, file){
                var caption, input_class;
                if(file.caption !== '') {
                    caption = file.caption;
                    input_class = '';
                } else {
                    caption = empty_caption;
                    input_class = ' emptyCaption';
                }
                $('#uploadedItems').append('<div class="uploadedItem"><span><img alt="'+name+'" src="/'+file.path+'"><a class="uploadedItemRemove" onclick="return removefile(this)" href="javascript:void(0)" filename="'+name+'"><img src="/img/b_remove.png"></a></span><input type="text" class="uploadedItemCaption'+input_class+'" value="'+caption+'" filename="'+name+'" /></div>');
            });
        }, "json");
        
        // $('#SWFUpload_0').width(35);
        // $('#SWFUpload_0').height(39);

    }

    function onStartFileDialog() {
        $('#uploadedItemsErr').html('');//clear error messages
    }

    function onCompleteFileDialog(numFilesSelected, numFilesQueued) {
        try {
            if (this.getStats().files_queued > 0) {
                //document.getElementById(this.customSettings.cancelButtonId).disabled = false;
                //this action will be enabled when cancel button will be added on site
            }
            /* I want auto start and I can do that here */
            this.startUpload();
        } catch (ex) {
            this.debug(ex);
        }
    }

    function addFileQueued(file) {
        $('#uploadedItems').append('<div class="uploadedItem" id="i' + file.id + '"><div id="v' + file.id + '"></div><p>' + file.name + '</p></div>');
        $("#v" + file.id).progressbar({value: 0});
    }

    function uploadSuccess(file, server_data, received_response) {
        $('#i' + file.id).html('<span>' + server_data + '</span>');
    }


    function uploadProgress(file, bytes_complete, total_bytes) {
        var data = 100 * bytes_complete / total_bytes;
        var progress = parseInt(data, 10);
        $("#v" + file.id).progressbar({ value: progress });
    }


    function uploadError(file, error_code, message) {
        $('#uploadedItemsErr').html(message);
    }

    function queueError(file, error_code, message) {
        $('#uploadedItemsErr').html(message);
    }

    $('#uploadedItems .uploadedItemCaption').live('focus', function() {
        if($($(this)).attr('value') == empty_caption) {
            $($(this)).removeClass('emptyCaption');
            $($(this)).attr('value','');
        }
    });

    $('#uploadedItems .uploadedItemCaption').live('blur', function() {
        if($($(this)).attr('value') == '') {
            $($(this)).addClass('emptyCaption');
            $($(this)).attr('value',empty_caption);
        }
        saveCaption(this);
    });

    function saveCaption(input) {
        $.post("<?php echo url_for('obituaries_setfilecaption', $obituary) ?>", { filename: $(input).attr('filename'), caption: $(input).attr('value') });
    }
</script>

<h2><?php echo $icons['actions']['addmedia']['title']?></h2>

<div id="uploadedItems">
    <div id="uploadedItemsErr"></div>
</div>



<div class="verse-obit-splitter"></div>
<form action="<?php echo url_for('obituaries_addmedia', $obituary)?>" method="post">
    <div class="verse-obit-addmediaform">
        <?php echo $form; ?>
        <label>&nbsp;</label><input type="submit" value="Save uploaded">
    </div>
</form>
