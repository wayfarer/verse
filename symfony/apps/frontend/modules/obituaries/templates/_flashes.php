<?php if ($sf_user->hasFlash('notice') || ($sf_user->hasFlash('error'))): ?>
<div class="verse-obit-info">
<h2>Information</h2>
<div class="verse-obit-splitter"></div>
<?php if ($sf_user->hasFlash('notice')): ?>
  <div class="notice"><?php echo $sf_user->getFlash('notice') ?></div>
<?php endif; ?>
<?php if ($sf_user->hasFlash('error')): ?>
  <div class="error"><?php echo $sf_user->getFlash('error') ?></div>
<?php endif; ?>
</div>
<div class="verse-obit-spacer"></div>
<?php endif ?>
