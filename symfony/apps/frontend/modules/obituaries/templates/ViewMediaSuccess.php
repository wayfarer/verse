<link rel="stylesheet" type="text/css" href="/shadowbox/shadowbox.css">
<script type="text/javascript" src="/shadowbox/shadowbox.js"></script>
<script type="text/javascript">
    Shadowbox.init();
</script>
<h2><?php echo $icons['actions']['viewmedia']['title']?></h2>
<?php if (count($photos) || count($movies)): ?>
    <div class="verse-obit-candles-order">
        <?php if ($order == 'desc'): ?>
            <a href="<?php echo url_for('@obituaries_viewmedia?order=asc&obituary_id=' . $obituary->getObituaryId().'&obituary_slug='.$obituary->getObituarySlug())?>">show old to new</a>
        <?php else: ?>
            showing old to new
        <?php endif?> /
        <?php if ($order == 'desc'): ?>
            showing new to old
        <?php else: ?>
            <a href="<?php echo url_for('@obituaries_viewmedia?order=desc&obituary_id=' . $obituary->getObituaryId().'&obituary_slug='.$obituary->getObituarySlug())?>">show new to old</a>
        <?php endif?>
    </div>
    <div class="verse-obit-splitter"></div>
    <?php if(count($photos)): ?>
        <h3>Photos</h3>
        <div class="verse-obit-mediaphotos highslide-gallery">
            <?php foreach($photos as $photo): ?>
                <a href="/<?= $photo['path'] ?>" class="highslide verse-obit-media" onclick="return hs.expand(this, { slideshowGroup: 'obit-media' })" ><img src="/<?php echo VerseUtil::thumbnail($photo['path'], 100); ?>" title="Click to enlarge"></a>
            <?php endforeach; ?>
        </div>
    <?php endif; ?>
    <div class="verse-obit-splitter"></div>
    <?php if(count($movies)): ?>
        <h3>Movies</h3>
        <div class="verse-obit-mediamovies">
            <?php foreach($movies as $movie): ?>
                <a href="/<?= $movie['path'] ?>" class="verse-obit-media" rel="shadowbox"><img src="/<?= $movie['thumbnail'] ?>" alt="Movie"></a>
            <?php endforeach; ?>
        </div>
    <?php endif; ?>
<?php else: ?>
    <div class="verse-obit-splitter"></div>
    <div class="verse-obit-no-media-message"><?php echo $icons['actions']['viewmedia']['no_media_message'] ?></div>
    <div class="verse-obit-splitter verse-obit-spacer"></div>
    <a href="<?php echo url_for('obituaries_addmedia', $obituary) ?>" class="verse-obit-action-ajax verse-obit-backlink">
        <?php echo $icons['actions']['addmedia']['title']?>
    </a>
<?php endif ?>
<div class="verse-obit-spacer"></div>
<a href="javascript:history.go(-1)" class="verse-obit-backlink">&laquo; back</a>
<div class="verse-obit-spacer"></div>