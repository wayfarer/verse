<h2>Biography</h2>
<div class="verse-obit-splitter"></div>
<div class="verse-obit-text">
  <?php echo $obituary->getObitText(ESC_RAW)?>
</div>
<div class="verse-obit-spacer"></div>

<?php if(count($candles)): ?>
<h2><?php echo $icons['actions']['viewallcandles']['content_header']?></h2>
<div class="verse-obit-splitter"></div>
<?php foreach($candles as $candle): ?>
  <div class="verse-obit-candle">
    <div class="verse-obit-candle-name">
      <strong><?php echo $candle['name']?></strong>
      <?php echo $candle['timestamp']?>
    </div>
    <div class="verse-obit-candle-text">
      <div><?php echo $candle['thoughts']?></div>
    </div>
  </div>
  <div class="verse-obit-splitter"></div>
<?php endforeach ?>
<?php endif?>
<script type="text/javascript">
jQuery( function() {
  $('.verse-obit-splitter').replaceWith('<img class="verse-obit-splitter-img" src="/obits/themes/<?php echo $config['obituary_theme']?>/img/splitter_print.gif">');
  window.print();
} );
</script>
