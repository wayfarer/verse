<?php

/**
 * obituaries actions.
 *
 * @package    verse3
 * @subpackage obituaries
 * @author     Vladimir Droznik
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class obituariesActions extends sfActions
{
    public function preExecute()
    {
        $obituary_id = $this->getRequest()->getParameter('obituary_id');

        $obituary = Doctrine_Core::getTable('PlgObituary')->createQuery()
                ->andWhere('obituary_id = ?', $obituary_id)
                ->fetchOne();

        if ($obituary) {
            $this->obituary = $obituary;
            $config = $obituary->getConfig();
            $this->config = $config;
            $this->theme = $config['obituary_theme'];
            //$this->theme = $obituary->getConfig('obituary_theme');
            // TODO: debug, doesn't fetch whole config correctly after upper statement

            $icons_path = 'obits/icons/' . $config['obituary_icons_theme'] . '/';
            $this->icons = sfYaml::load($icons_path . 'theme.yml');
            // web path
            $this->icons_path = '/' . $icons_path;

            $this->getResponse()->addStylesheet("/obits/themes/$this->theme/style.css", '', array('media' => 'all'));
            $this->setLayout(sfConfig::get('sf_web_dir') . "/obits/themes/$this->theme/layout");
        }
    }

    /**
     * Executes index action
     *
     * @param sfRequest $request A request object
     */
    public function executeIndex(sfWebRequest $request)
    {
        //$this->setTemplate(sfConfig::get('sf_web_dir')."/obits/themes/$this->theme/index");
    }

    public function executeNewCandle(sfWebRequest $request)
    {
/*
        $this->getResponse()->addJavascript('/gyCaptchaPlugin/js/gyCaptcha.js');
        $this->getResponse()->addStylesheet('/gyCaptchaPlugin/css/gyCaptcha.css');
*/
        $this->redirectUnless($this->config['candles_policy'], 'obituaries', $this->obituary);
        $form = new ObituariesCandleForm();
        $form->getWidget('memories')->setLabel($this->icons['actions']['lightacandle']['form_message_title']);

        if ($request->getMethod() == 'POST') {
            $form_data = $request->getParameter($form->getName());
            $form->bind($form_data);
            $obituary = $this->getRoute()->getObject();
            if ($form->isValid() && $obituary->getConfig('candles_policy') != 'off') {
                $ip = $_SERVER['REMOTE_ADDR'];
                $block_ip = Doctrine_Core::getTable('PlgBlockIp')->createQuery()->addWhere('ip = ?', $ip)->fetchOne();

                if ($block_ip === false && VerseUtil::noBlockKeywords($form_data['memories'])) {
                    try {
                        $candle = new PlgObituaryCandle();
                        $candle['domain_id'] = $obituary['domain_id'];
                        $candle['obituary_id'] = $obituary['obituary_id'];
                        $candle['name'] = $form_data['name'];
                        $candle['email'] = $form_data['email'];
                        $candle['ip'] = $ip;
                        $candle['thoughts'] = $form_data['memories'];
                        if(isset($form_data['candle_image'])) {
                            $candle['image'] = $form_data['candle_image'];
                        }
                        $candle['timestamp'] = new Doctrine_Expression('NOW()');

                        if ($obituary->getConfig('candles_policy') == 'moderated' || ($obituary->getConfig('candles_policy') == 'moderated_n_days' && $obituary->withCandleModeration())) {
                            // put candle to moderation queue
                            $candle['state'] = 1;
                            $candle['hash'] = substr(md5(uniqid("")), 0, 16);
                        }
                        else {
                            // publish candles
                            $candles['state'] = 0;
                        }

                        $candle->save();

                        //$candle->setCandleId(Doctrine_Manager::getInstance()->getCurrentConnection()->lastInsertId('candle_id'));
                        //echo $candle->getTable()->getConnection()->lastInsertId('candle_id'),'/';
                        //	        echo Doctrine_Manager::getInstance()->getCurrentConnection()->getDbh()->lastInsertId('candle_id');
                        //	        exit;

                        $this->dispatcher->notify(new sfEvent($this, 'obituary.create_candle', array('object' => $candle)));

                        if ($candle['state'] > 0) {
                            $this->getUser()->setFlash('notice', 'Thank you for submitting your thoughts! Your message may not be immediately available for public viewing, but be sure that the family will receive it.');
                        }
                        else {
                            $this->getUser()->setFlash('notice', 'Your thoughts have been published.');
                        }

                        $this->redirect('obituaries_allcandles', $obituary);
                    } catch (Doctrine_Validator_Exception $e) {

                        $errorStack = $form->getObject()->getErrorStack();

                        $message = get_class($form->getObject()) . ' has ' . count($errorStack) . " field" . (count($errorStack) > 1 ? 's' : null) . " with validation errors: ";
                        foreach ($errorStack as $field => $errors) {
                            $message .= "$field (" . implode(", ", $errors) . "), ";
                        }
                        $message = trim($message, ', ');

                        $this->getUser()->setFlash('error', $message);
                    }
                } else {
                    if ($block_ip !== false) {
                        $block_ip->setCount($block_ip->getCount()+1);
                        $block_ip->save();
                    }
                    $this->redirect('obituaries_allcandles', $obituary);
                }

            }
        }

        $this->form = $form;
        //$this->obituary = $this->getRoute()->getObject();
        //$this->setTemplate(sfConfig::get('sf_web_dir').'/obits/themes/classic/candleForm');
    }

    public function executeAllCandles(sfWebRequest $request)
    {
        $this->redirectUnless($this->config['candles_policy'], 'obituaries', $this->obituary);
        if ($request->getParameter('order')) {
            $this->order = $request->getParameter('order') == 'desc' ? 'desc' : 'asc';
            $this->getUser()->setAttribute('candles_order', $this->order);
        }
        else {
            $this->order = $this->getUser()->getAttribute('candles_order', 'asc');
        }
        $this->candles = Doctrine_Core::getTable('PlgObituaryCandle')->createQuery()
                ->where('obituary_id = ? AND state = 0', $this->obituary['obituary_id'])
                ->orderBy('timestamp ' . $this->order)
                ->execute();
        if (file_exists(sfConfig::get('sf_web_dir') . "/obits/themes/$this->theme/allCandlesSuccess.php")) {
            $this->setTemplate(sfConfig::get('sf_web_dir') . "/obits/themes/$this->theme/allCandles");
        }
    }

    public function executePublishCandle(sfWebRequest $request)
    {
        $obituary = $this->getRoute()->getObject();

        $candle = Doctrine_Core::getTable('PlgObituaryCandle')->createQuery()
                ->andWhere('hash = ?', $request->getParameter('candle_hash'))->fetchOne();
        if ($candle) {
            $candle->setState(0);
            $candle->save();

            $this->dispatcher->notify(new sfEvent($this, 'obituary.publish_candle', array('object' => $candle)));
        }
        $this->redirect('obituaries_allcandles', $obituary);
    }

    public function executeSubscribeObituaryUpdates(sfWebRequest $request)
    {
        $this->redirectUnless($this->config['candles_policy'], 'obituaries', $this->obituary);
/*
        $this->getResponse()->addJavascript('/gyCaptchaPlugin/js/gyCaptcha.js');
        $this->getResponse()->addStylesheet('/gyCaptchaPlugin/css/gyCaptcha.css');
*/
        $form = new ObituaryUpdatesSubscribeForm();
        $obituary = $this->getRoute()->getObject();

        if ($request->getMethod() == 'POST') {
            $form_data = $request->getParameter($form->getName());
            $form->bind($form_data);
            if ($form->isValid()) {
                $ou_subscription = new PlgObituarySubscribe();
                $ou_subscription['obituary_id'] = $obituary['obituary_id'];
                $ou_subscription['domain_id'] = $obituary['domain_id'];//Doctrine_Core::getTable('SmsDomain')->getCurrent()->getDomainId();
                $ou_subscription['name'] = $form_data['name'];
                $ou_subscription['email'] = $form_data['email'];
                $ou_subscription['created'] = new Doctrine_Expression('NOW()');
                $ou_subscription['hash'] = substr(md5(uniqid("")), 0, 16);
                $ou_subscription['enabled'] = 0;

                $ou_subscription->save();

                // send confirmation email
                obituariesHelper::onObituaryUpdatesSubscribe($ou_subscription);

                $this->getUser()->setFlash('notice', 'Your subscription request has been received. To complete your registration, please check your email and follow the instructions that have been sent to you.');
                $this->redirect('obituaries', $obituary);
            }
        }
        $this->form = $form;
        $this->obituary = $obituary;
    }

    public function executeConfirmObituaryUpdatesSubscription(sfWebRequest $request)
    {
        $obituary = $this->getRoute()->getObject();
        $ou_subscription = Doctrine_Core::getTable('PlgObituarySubscribe')->createQuery()
                ->andWhere('hash = ?', $request->getParameter('hash'))->fetchOne();
        if ($ou_subscription) {
            $ou_subscription['enabled'] = 1;
            $ou_subscription->save();
            $this->getUser()->setFlash('notice', 'Thank you. Your subscription has been confirmed. You will recieve an email when someone leaves a candle here.');
        }
        $this->redirect('obituaries', $obituary);
    }

    public function executePrint(sfWebRequest $request)
    {
        if ($this->config['candles_policy']) {
            if ($request->getParameter('order')) {
                $this->order = $request->getParameter('order') == 'desc' ? 'desc' : 'asc';
                $this->getUser()->setAttribute('candles_order', $this->order);
            }
            else {
                $this->order = $this->getUser()->getAttribute('candles_order', 'asc');
            }
            $this->candles = Doctrine_Core::getTable('PlgObituaryCandle')->createQuery()
                    ->where('obituary_id = ? AND state = 0', $this->obituary['obituary_id'])
                    ->orderBy('timestamp ' . $this->order)
                    ->execute();
        }
        else {
            $this->candles = array();
        }
        if (file_exists(sfConfig::get('sf_web_dir') . "/obits/themes/$this->theme/printSuccess.php")) {
            $this->setTemplate(sfConfig::get('sf_web_dir') . "/obits/themes/$this->theme/print");
        }
        $this->setLayout(sfConfig::get('sf_web_dir') . "/obits/themes/$this->theme/layout_print");
    }

    public function executeAddMedia(sfWebRequest $request) {
        $this->redirectUnless($this->config['media'], 'obituaries', $this->obituary);
/*
        $this->getResponse()->addJavascript('/gyCaptchaPlugin/js/gyCaptcha.js');
        $this->getResponse()->addStylesheet('/gyCaptchaPlugin/css/gyCaptcha.css');
*/
        $form = new ObituariesAddMediaForm();
        if ($request->getMethod() == 'POST') {
            $form->bind($request->getParameter($form->getName()), $request->getFiles($form->getName()));
            $obituary = $this->getRoute()->getObject();
            if ($form->isValid() && $obituary->getConfig('media') && !$obituary->getConfig('disable_media')) {
                $user = $this->getUser();
                // save files stored in session
                $files = $user->getAttribute('files', array());
                if (count($files)) {
                    $domain = Doctrine_Core::getTable('SmsDomain')->createQuery()->addWhere('domain_id = ?', $obituary->getDomainId())->fetchOne();
                    verseMediaUtil::saveFiles($domain, $obituary, $files);
                }
                $user->getAttributeHolder()->remove('files');
                $this->getUser()->setFlash('notice', 'Your file(s) have been uploaded successfully. Moderator will review and publish them shortly.');
                $this->redirect('obituaries_viewmedia', $obituary);
            }
        }
        $this->form = $form;
    }

    public function executeViewMedia(sfWebRequest $request) {
        $this->redirectUnless($this->config['media'] && !$this->config['disable_media'], 'obituaries', $this->obituary);
        
        $this->order = $request->getParameter('order') == 'desc' ? 'desc' : 'asc';
        $this->photos = Doctrine_Core::getTable('PlgObituaryImage')->createQuery()
                ->andWhere('obituary_id = ? AND state = 0', $this->obituary['obituary_id'])
                ->orderBy('id ' . $this->order)
                ->execute();
        $this->movies = Doctrine_Core::getTable('PlgObituaryMovie')->createQuery()
                ->andWhere('obituary_id = ? AND state = 0', $this->obituary['obituary_id'])
                ->orderBy('id ' . $this->order)
                ->execute();
        if (file_exists(sfConfig::get('sf_web_dir') . "/obits/themes/$this->theme/ViewMediaSuccess.php")) {
            $this->setTemplate(sfConfig::get('sf_web_dir') . "/obits/themes/$this->theme/ViewMedia");
        }
    }

    public function executeUploadMedia(sfWebRequest $request) {
        if (isset($_FILES)) {
            $user = $this->getUser();
            $obituary = $this->getRoute()->getObject();
            $domain_name = Doctrine_Core::getTable('SmsDomain')->createQuery()->addWhere('domain_id = ?', $obituary->getDomainId())->fetchOne()->getDomainName();
            $file = verseMediaUtil::uploadFile($domain_name, $user, $_FILES);
            if($file) {
                echo $file->getThumbnail().'<a class="uploadedItemRemove" filename="'.$file->getFullName().'" href="javascript:void(0)" onclick="return removefile(this)"><img src="/img/b_remove.png"/></a><input type="text" class="uploadedItemCaption emptyCaption" value="'.VerseMediaUtil::EMPTY_CAPTION.'" filename="'.$file->getFullName().'" />';
            }
            else {
                echo "an error occured";
            }
        }
        $this->setLayout(false);
        return sfView::NONE;
    }

    public function executeGetUploadedFiles(sfWebRequest $request) {
        $files = verseMediaUtil::getThumbnailAndCaptionForFiles($this->getUser()->getAttribute('files', array()));
        echo json_encode($files, JSON_FORCE_OBJECT);
        $this->setLayout(false);
        return sfView::NONE;
    }

    public function executeRemoveFile(sfWebRequest $request) {
        $user = $this->getUser();
        $files = $user->getAttribute('files', array());
        if (isset($_POST['filename']) && count($files)) {
            $file = new verseMediaFile($files[$_POST['filename']]['path']);
            $user->getAttributeHolder()->remove('files');
            unset($files[$_POST['filename']]);
            $user->setAttribute('files', $files);
            $file->delete();
        }
        $this->setLayout(false);
        return sfView::NONE;
    }

    public function executeRemoveUploadedFiles(sfWebRequest $request) {
        $user = $this->getUser();
        $files = $user->getAttribute('files', array());
        foreach ($files as $file) {
            $media_file = new verseMediaFile($file['path']);
            $media_file->delete();
        }
        $user->getAttributeHolder()->remove('files');
        $this->setLayout(false);
        return sfView::NONE;
    }

    public function executeSetFileCaption(sfWebRequest $request) {
        if(isset($_POST['filename']) && isset($_POST['caption'])) {
            $user = $this->getUser();
            $files = $user->getAttribute('files', array());

            $filename = $_POST['filename'];
            $caption = $_POST['caption'];

            if ($caption != VerseMediaUtil::EMPTY_CAPTION) {
                $files[$filename]['caption'] = $caption;
            } else {
                $files[$filename]['caption'] = '';
            }

            $user->getAttributeHolder()->remove('files');
            $user->setAttribute('files', $files);
        }

        $this->setLayout(false);
        return sfView::NONE;
    }
}
