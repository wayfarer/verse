CREATE TABLE `sms_domain_link_list_parent_domain` (
  `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `group_id` INT(10) UNSIGNED NOT NULL,
  `domain_id` INT(10) UNSIGNED NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_sms_domain` (`domain_id`),
  KEY `FK_sms_domain_link_group` (`group_id`)
);