ALTER TABLE plg_obituary_image ADD caption VARCHAR(255) NOT NULL DEFAULT '' AFTER domain_id;
ALTER TABLE plg_obituary_movie ADD caption VARCHAR(255) NOT NULL DEFAULT '' AFTER domain_id;