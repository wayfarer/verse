/*
SQLyog Enterprise - MySQL GUI v8.14 
MySQL - 5.0.67-community-nt : Database - verse
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
/*Table structure for table `plg_obituary_image` */

DROP TABLE IF EXISTS `plg_obituary_image`;

CREATE TABLE `plg_obituary_image` (
  `id` int(11) NOT NULL auto_increment,
  `obituary_id` int(11) NOT NULL,
  `domain_id` int(11) NOT NULL,
  `path` varchar(255) NOT NULL default '',
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

/*Table structure for table `plg_obituary_movie` */

DROP TABLE IF EXISTS `plg_obituary_movie`;

CREATE TABLE `plg_obituary_movie` (
  `id` int(11) NOT NULL auto_increment,
  `obituary_id` int(11) NOT NULL,
  `domain_id` int(11) NOT NULL,
  `local` tinyint(4) NOT NULL default '0',
  `path` varchar(255) NOT NULL default '',
  `thumbnail` varchar(255) NOT NULL default '',
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
