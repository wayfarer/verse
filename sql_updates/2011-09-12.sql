ALTER TABLE plg_obituary_image ADD origfilename VARCHAR(255) NOT NULL DEFAULT '' AFTER path;
ALTER TABLE plg_obituary_movie ADD origfilename VARCHAR(255) NOT NULL DEFAULT '' AFTER path;