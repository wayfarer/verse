ALTER TABLE plg_product_order add order_domain_name varchar(255) NOT NULL DEFAULT '';

CREATE TABLE `sms_domain_link_product` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `domain_id` INT(10) UNSIGNED NOT NULL,
  `product_id` INT(10) UNSIGNED NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_sms_domain` (`domain_id`),
  KEY `FK_plg_product` (`product_id`)
) ENGINE=INNODB;
