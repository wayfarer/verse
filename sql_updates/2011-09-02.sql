ALTER TABLE `sms_domain` ENGINE=INNODB ROW_FORMAT=DEFAULT CHARSET=utf8;
CREATE TABLE `sms_domain_link_group` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `is_shared_obits` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `is_shared_pages` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `is_shared_members` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `is_shared_lists` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `is_shared_products` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `is_shared_movieclips` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `is_shared_events` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `is_shared_structure` tinyint(3) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
CREATE TABLE `sms_domain_link` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `group_id` int(10) unsigned NOT NULL,
  `domain_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_sms_domain` (`domain_id`),
  KEY `FK_sms_domain_link_group` (`group_id`),
  CONSTRAINT `FK_sms_domain_link_group` FOREIGN KEY (`group_id`) REFERENCES `sms_domain_link_group` (`id`),
  CONSTRAINT `FK_sms_domain` FOREIGN KEY (`domain_id`) REFERENCES `sms_domain` (`domain_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
