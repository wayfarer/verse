ALTER TABLE plg_obituary CHANGE visitation_date visitation_time VARCHAR(255) NULL;

ALTER TABLE plg_obituary ADD visitation_date INT(10) UNSIGNED NULL AFTER service_type;