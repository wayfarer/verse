ALTER TABLE sms_domain_link ADD FOREIGN KEY FK_sms_domain_link_group (group_id) REFERENCES sms_domain_link_group (id) ON DELETE CASCADE;

ALTER TABLE sms_domain_link_page ADD FOREIGN KEY FK_sms_domain_link_group (group_id) REFERENCES sms_domain_link_group (id) ON DELETE CASCADE;