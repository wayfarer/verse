ALTER TABLE `sms_domain_link_group` ADD `group_name` varchar(255) NOT NULL DEFAULT '' AFTER `id`;

CREATE TABLE `sms_domain_link_page` (
  `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `group_id` INT(10) UNSIGNED NOT NULL,
  `page_id` INT(10) UNSIGNED NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_cms_page` (`page_id`),
  KEY `FK_sms_domain_link_group` (`group_id`)
);