CREATE TABLE `sms_domain_link_group_config` (
  `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `group_id` INT(10) UNSIGNED NOT NULL,
  `param` VARCHAR(32) NOT NULL DEFAULT '',
  `value` BLOB NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=INNODB;

ALTER TABLE sms_domain_link_group_config ADD FOREIGN KEY FK_sms_domain_link_group (group_id) REFERENCES sms_domain_link_group (id) ON DELETE CASCADE;