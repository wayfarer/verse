set :application, "Verse SMS"
set :domain,      "ws4.twintierstech.net"
set :deploy_to,   "/home/ttt-ws-production"

set :repository,  "ssh://git@bitbucket.org/wayfarer/verse.git"
set :scm,         :git
set :shared_files,      ["symfony/config/databases.yml"]
set :shared_children,   ["logs", "symfony/log", "www/logs", "www/files", "www/vendor"]

role :web,        domain                         # Your HTTP server, Apache/etc
role :app,        domain                         # This may be the same as your `Web` server
role :db,         domain, :primary => true       # This is where Rails migrations will run

set  :use_sudo,       false
set  :keep_releases,  3
set  :user,           "wayfarer"
# ssh_options[:forward_agent] = true
set  :branch, "v1.2.0"
