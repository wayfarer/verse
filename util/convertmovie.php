<?php
require("config.inc.php");
require("util.inc.php");
require_once("DB.php");
require_once("Log.php");

// init logging mechanism
$conf = array('title' => 'Log');
$logger = &Log::factory('file', '/home/ttt-ws-production/util/convertmovie.log', 'versesms');
//$logger = &Log::factory('file', 'c:/sites/home/localhost/www/verse/util/convertmovie.log', 'versesms');

$db_con = createobject("db_versesms");
$db = $db_con->connect();

if($argc==2) {
  $id = intval($argv[1]);
  $query = "SELECT id, path, domain_name FROM plg_obituary_movie INNER JOIN sms_domain USING(domain_id) WHERE id='$id'";
  $ret = $db->getRow($query, DB_FETCHMODE_ASSOC);

  if(!DB::isError($ret) && $ret) {
    extract($ret);
    // get file information
    $output = "";
    exec("/usr/bin/ffmpeg -i ../www/files/$domain_name/$path 2>&1", $output);
    //exec("c:/ffmpeg/ffmpeg -i ../www/files/$domain_name/$path 2>&1", $output);
    $output = implode("\n", $output);
    //echo $output, "\n\n";
    //if(preg_match("/Video: (\w+).*?(\d{2,5})x(\d{2,5}).*?(\d+\.\d+) tb\(r\)/mis", $output, $matches)) {
    if(preg_match("/Video: (\w+).*?(\d{2,5})x(\d{2,5})/mis", $output, $matches)) {
      $format = $matches[1];
      $width = $matches[2];
      $height = $matches[3];
    }
    else {
      $query = "UPDATE plg_obituary_movie SET status=3 WHERE id='$id'";
      $db->query($query);
      $logger->warning("Cannot determine videofile data, id='$id', file='$domain_name/$path'! output='$output'\n");
      exit;
    }


    // determine destination
    $pathinfo = pathinfo($path);
    $fn = $pathinfo['filename'];
    $fullfn = $pathinfo['basename'];
    $filename = "../www/files/$domain_name/movies/$fn.flv";
    $shortfn = "movies/$fn.flv";
    $extension = strtolower($pathinfo['extension']);
//    echo $filename,"\n";

    if($extension == 'flv') { // some FLV files with FPS=1000 are converted incorrectly
      $logger->info("no conversion required for flv file $shortfn (id=$id)\n");
    }
    else {
      // convert if no exceptions
      $output = "";
      exec("/usr/bin/ffmpeg -i ../www/files/$domain_name/$path -ar 22050 -y $filename 2>&1", $output);
      //exec("c:/ffmpeg/ffmpeg -i ../www/files/$domain_name/$path -ar 22050 -y $filename 2>&1", $output);
      $output = implode("\n", $output);
      $logger->info($output."\n");
      unlink("../www/files/$domain_name/$path");
    }
    $query = "UPDATE plg_obituary_movie SET path='$shortfn', status=1 WHERE id='$id'";
    $db->query($query);
  }
  else {
    $logger->info("movie_id='$id', nothing to do");
  }
}
