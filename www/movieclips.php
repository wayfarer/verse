<?php
 // obsolete
// backend movieclips management
include("inc/verse.inc.php"); //main header - initializes Verse environment

/*  if(count($_POST)) {
    $logger->info(print_r($_POST, true));
    $logger->info(print_r($_FILES, true));
    $logger->info(print_r($_SESSION, true));
  } */
if ($user->have_role(ROLE_MOVIECLIPS_MANAGEMENT)) {
    $action = @$_POST["action"];
    switch ($action) {
        case "list":
            $rep = createobject("report_ajax", array($db, "plg_movieclip", REP_WITH_PAGENATOR, true, 20, 'width="750px" style="background-color: #DDEEFF"'));

            $rep->add_field("ID", REP_STRING_TEMPLATE, '{movieclip_id}', REP_ORDERABLE, 'align="center"');
            $rep->add_field("Title", REP_STRING_TEMPLATE, '{title}', REP_ORDERABLE, 'align="center"');
            $rep->add_field("Created", REP_STRING_TEMPLATE, '{FROM_UNIXTIME(#timestamp#,"' . DB_DATETIME_FORMAT . '")}', REP_ORDERABLE, 'align="center"');
            $rep->add_field("Status", REP_CALLBACK, 'status({status})', REP_UNORDERABLE, 'align="center"');
            $rep->add_field("", REP_STRING_TEMPLATE, href_js_action("preview", "movieclip_preview({movieclip_id|1})"), REP_UNORDERABLE, 'align="center"');
            $rep->add_field("", REP_STRING_TEMPLATE, href_js_action("edit", "new MovieclipEditor({movieclip_id|1})"), REP_UNORDERABLE, 'align="center"');
            $rep->add_field("", REP_STRING_TEMPLATE, href_js_action("delete", "delete_movieclip({movieclip_id|1})"), REP_UNORDERABLE, 'align="center"');
            // for superadmin
            if (!$user->data["domain_id"]) {
                $rep->add_field("Size", REP_CALLBACK, 'converted({filename})', REP_UNORDERABLE, 'align="center"');
                $rep->add_field("Orig.filename", REP_CALLBACK, 'orig({origfilename},{tmpfilename})', REP_UNORDERABLE, 'align="center"');
            }

            $rep->add_filter("", REP_INVISIBLE_FILTER, "domain_id", "=", $domain_id);

            $rep->html_attributes('width="750px"');

            if (count($_POST) < 3) {
                $rep->order_by("movieclip_id", 1);
            }
            $rep->handle_events($_POST);

//        echo $rep->query;

            $html = $rep->make_report();
            echo $html;
            break;
        case "load":
            $movieclip_id = intval(@$_POST["id"]);
            $query = "SELECT title, protected, password FROM plg_movieclip WHERE movieclip_id='$movieclip_id' AND domain_id='$domain_id'";
            $ret = $db->getRow($query, DB_FETCHMODE_ASSOC);
            header("X-JSON:" . make_json_response($ret));
            break;
        case "save":
            $logger->info("save movieclip called");
            $movieclip_id = intval(@$_POST["movieclip_id"]);
            $file_present = false;
            $logger->info(print_r($_FILES, true));
            if ($_FILES["movieclip"]["tmp_name"]) {
                // generate filename
                ensure_dir("files/$domain_name/tmp");
                $filename = tempnam("tmp", "");
                $filename = substr($filename, 1);
                $file_present = move_uploaded_file($_FILES["movieclip"]["tmp_name"], "files/$domain_name/$filename");
                $logger->info("uploaded tmp file $filename");
            }
            if ($movieclip_id || $file_present) {
                $p = array();
                $p["title"] = in(@$_POST["title"]);
                $p["protected"] = (@$_POST["protected"] ? 1 : 0);
                $p["password"] = in(@$_POST["password"]);
                if ($file_present) {
                    $p["tmpfilename"] = $filename;
                    $p["origfilename"] = in($_FILES["movieclip"]["name"]);
                    $logger->info("saving file " . $p["origfilename"]);
                }
                if ($movieclip_id) {
                    if ($file_present) {
                        // remove old file
                        $query = "SELECT filename FROM plg_movieclip WHERE movieclip_id='$movieclip_id' AND domain_id='$domain_id'";
                        $oldfn = $db->getOne($query);
                        if (!DB::isError($oldfn) && $oldfn) {
                            unlink("files/$domain_name/$oldfn");
                        }
                        $p["status"] = 2;
                    }
                    $query = "UPDATE plg_movieclip SET " . make_set_clause($p) . " WHERE movieclip_id='$movieclip_id' AND domain_id='$domain_id'";
                    $db->query($query);
                }
                else {
                    $p["domain_id"] = $domain_id;
                    $p["status"] = 2;
                    $query = "INSERT plg_movieclip SET " . make_set_clause($p) . ", timestamp=UNIX_TIMESTAMP(now())";
                    $ret = $db->query($query);
                    $p = array();
                    $movieclip_id = $p["movieclip_id"] = $db->getOne("SELECT last_insert_id()");
                    $p["obituary_id"] = $obituary_id;
                    $query = "INSERT plg_obituary_movieclip SET " . make_set_clause($p);
                    $db->query($query);
                }
                if ($file_present) {
                    // run conversion script
                    exec("php ../util/convertmc.php $movieclip_id");
                }
            }
            echo "<script>window.parent.document.dialog_window.onsave();</script>";
//        echo $query;
//        echo "done!";
            break;
        case "delete":
            $movieclip_id = intval(@$_POST["id"]);
            // remove file first
            $query = "SELECT filename FROM plg_movieclip WHERE movieclip_id='$movieclip_id' AND domain_id='$domain_id'";
            $filename = $db->getOne($query);
            unlink("files/$domain_name/$filename");
            $query = "DELETE FROM plg_movieclip WHERE movieclip_id='$movieclip_id' AND domain_id='$domain_id'";
            $db->query($query);
            $query = "DELETE FROM plg_obituary_movieclip WHERE movieclip_id='$movieclip_id'";
            $db->query($query);
            break;
        default:
            $smarty->assign("session_id", session_id());
            $smarty->display("movieclips.tpl");
    }
}
else {
    header("Location: login.php");
}

function ensure_dir($dir) {
    if (!is_dir($dir)) mkdir($dir);
}

function status($status) {
    $ret = "unknown";
    switch ($status) {
        case 1:
            $ret = "ready";
            break;
        case 2:
            $ret = "converting";
            break;
        case 3:
            $ret = "invalid file";
            break;
    }
    return $ret;
}

function converted($filename) {
    global $domain_name;
    return '<div title="' . $filename . '">' . intval(@filesize("files/$domain_name/$filename") / 1024) . 'Kb</div>';
}

function orig($filename, $tmpfilename) {
    global $domain_name;
    return '<div title="' . intval(@filesize("files/$domain_name/$tmpfilename") / 1024) . 'Kb">' . $filename . '</div>';
}