<?php
 // frontend wall
include("inc/verse.inc.php"); //main header - initializes Verse environment

$theuser = createobject("user", array($db, "cms_access_user"));

$wall_id = intval(@$_GET["w"]);
$thread_id = intval(@$_GET["t"]);
$action = @$_GET["action"];

switch ($action) {
    case "create_thread":
        if ($theuser->logged()) {
            $p = array();
            $p["wall_id"] = intval($_POST["wall_id"]);
            $p["title"] = in($_POST["title"]);
            $p["domain_id"] = $domain_id;
            $p["user_id"] = $theuser->data["user_id"];
            // create the thread
            $query = "INSERT cms_wall_thread SET " . make_set_clause($p) . ", created_at=now()";
            $ret = $db->query($query);
            $thread_id = $db->getOne("SELECT last_insert_id()");
            // create the first message in the thread
            $p = array();
            $p["domain_id"] = $domain_id;
            $p["thread_id"] = $thread_id;
            $p["lft"] = 1;
            $p["rgt"] = 2;
            $p["user_id"] = $theuser->data["user_id"];
            $p["name"] = $theuser->data["name"];

            $p["message"] = in(_filter_url(filter_xss($_POST["message"])));

            $query = "INSERT cms_wall_message SET " . make_set_clause($p) . ", created_at=now()";
            $db->query($query);
            echo "ok";
        }
        break;
    case "post_message":
        $p = array();
        $p["domain_id"] = $domain_id;
        $p["thread_id"] = intval($_POST["thread_id"]);
        if ($p["thread_id"]) {
            if (trim($_POST["name"])) {
                $p["name"] = in($_POST["name"]);
            }
            if (check_email($_POST["email"])) {
                $p["email"] = in($_POST["email"]);
            }
            if ($theuser->logged()) {
                $p["user_id"] = $theuser->data["user_id"];
            }

            $p["message"] = in(_filter_url(filter_xss($_POST["message"])));

            $thread_id = intval($_POST["thread_id"]);
            $reply_to_id = intval($_POST["reply_to_id"]);
            if ($reply_to_id) {
                // find lft
                $query = "SELECT rgt FROM cms_wall_message WHERE wall_message_id='$reply_to_id' AND thread_id='$thread_id' AND domain_id='$domain_id'";
                $rgt = $db->getOne($query);
                // move upper elements in interval higher
                $query = "UPDATE cms_wall_message SET rgt=rgt+2 WHERE rgt >= $rgt";
                $db->query($query);
                $query = "UPDATE cms_wall_message SET lft=lft+2 WHERE lft > $rgt";
                $db->query($query);
            }
            else {
                // find max
                $query = "SELECT max(rgt) FROM cms_wall_message WHERE thread_id='$thread_id' AND domain_id='$domain_id'";
                $rgt = $db->getOne($query);
                $rgt++;
            }

            $p["lft"] = $rgt;
            $p["rgt"] = $rgt + 1;
            $query = "INSERT cms_wall_message SET " . make_set_clause($p) . ", created_at=now()";
            $ret = $db->query($query);
        }
        echo "ok";
        break;
    case "delete_message":
        if ($theuser->logged()) {
            $message_id = intval($_POST["message_id"]);
            $user_id = $theuser->data["user_id"];
            if ($theuser->data["privileges"] == 1) {
                // delete message because the user is admin
                $query = "DELETE FROM cms_wall_message WHERE wall_message_id='$message_id' AND domain_id='$domain_id'";
            }
            else {
                // delete message if current user owns it
                $query = "DELETE FROM cms_wall_message WHERE wall_message_id='$message_id' AND user_id='$user_id' AND domain_id='$domain_id'";
            }
            $db->query($query);
        }
        echo $query;
        break;
    case "delete_thread":
        $thread_id = intval($_POST["thread_id"]);
        $user_id = $theuser->data["user_id"];

        if ($theuser->data["privileges"] == 1) {
            // allow admin to delete any threads
            $ret = 1;
        }
        else {
            // check if user owns thread
            $query = "SELECT 1 FROM cms_wall_thread WHERE thread_id='$thread_id' AND user_id='$user_id' AND domain_id='$domain_id'";
            $ret = $db->getOne($query);
        }
        if ($ret) {
            // delete messages
            $query = "DELETE FROM cms_wall_message WHERE thread_id='$thread_id' AND domain_id='$domain_id'";
            $db->query($query);
            // delete thread
            $query = "DELETE FROM cms_wall_thread WHERE thread_id='$thread_id' AND domain_id='$domain_id'";
            $db->query($query);
        }
        break;
    default:
        if ($wall_id) {
            if (!$thread_id) {
                // show threads
                $query = "SELECT thread_id, title, count(wall_message_id) msg_cnt, max(m.created_at) created_at_last, t.user_id
                    FROM cms_wall_thread t LEFT JOIN cms_wall_message m USING(thread_id)
                    WHERE wall_id='$wall_id' AND t.domain_id='$domain_id' 
                    GROUP BY thread_id
                    ORDER BY max(m.created_at) DESC";
                $threads = $db->getAll($query, DB_FETCHMODE_ASSOC);
                if ($theuser->logged()) {
                    $smarty->assign("user_id", $theuser->data["user_id"]);
                }
                else {
                    $smarty->assign("user_id", 0);
                }
                $smarty->assign("wall_id", $wall_id);
                $smarty->assign("threads", $threads);
                $smarty->assign("user", $theuser->data);
                $smarty->display("wall_threads.tpl");
            }
            else {
                // thread title
                $query = "SELECT title FROM cms_wall_thread WHERE thread_id='$thread_id' AND domain_id='$domain_id'";
                $title = $db->getOne($query);
                // show thread contents (messages)
                $query = "SELECT node.wall_message_id, node.message, node.created_at, COUNT(parent.wall_message_id) level, node.user_id, node.name
                    FROM cms_wall_message AS node,
                    cms_wall_message AS parent
                    WHERE node.lft >= parent.lft AND node.lft <= parent.rgt
                    AND node.is_published=1 AND parent.is_published=1 AND node.thread_id='$thread_id' AND parent.thread_id='$thread_id' AND node.domain_id='$domain_id' AND parent.domain_id='$domain_id'
                    GROUP BY node.wall_message_id
                    ORDER BY node.lft";
                $messages = $db->getAll($query, DB_FETCHMODE_ASSOC);

                if ($theuser->logged()) {
                    $smarty->assign("user_id", $theuser->data["user_id"]);
                    $smarty->assign("user", $theuser->data);
                }
                else {
                    $smarty->assign("user_id", 0);
                }
                $smarty->assign("title", $title);
                $smarty->assign("wall_id", $wall_id);
                $smarty->assign("thread_id", $thread_id);
                $smarty->assign("messages", $messages);
                $smarty->display("wall_messages.tpl");
            }
        }
        else {
            echo "nothing to do";
        }
}
