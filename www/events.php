<?php
 // obsolete
// backend events management
include("inc/verse.inc.php"); //main header - initializes Verse environment

if ($user->have_role(ROLE_OBITUARIES_MANAGEMENT)) {
    $smarty->display("events.tpl");
}
else {
    header("Location: login.php");
}