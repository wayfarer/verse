<?php
/**
 * Created by PhpStorm.
 * User: Vladimir Droznik
 * Date: 10.12.2010
 */

include("inc/verse.inc.php"); //main header - initializes Verse environment

$export_path = "exports/$domain_name";

$what = array('site' => 1, 'node' => 1, 'page' => 1, 'obit_config');

$data = array();
if (isset($what['site'])) {
    $query = "SELECT * FROM cms_site WHERE domain_id='$domain_id'";
    $data['site'] = $db->getRow($query, DB_FETCHMODE_ASSOC);
}
if (isset($what['node'])) {
    $query = "SELECT * FROM cms_node WHERE domain_id='$domain_id'";
    $data['node'] = $db->getAll($query, DB_FETCHMODE_ASSOC);
}
if (isset($what['page'])) {
    $query = "SELECT * FROM cms_page WHERE domain_id='$domain_id'";
    $data['page'] = $db->getAll($query, DB_FETCHMODE_ASSOC);
}
if (isset($what['obit_config'])) {
    $query = "SELECT * FROM plg_obituary_config WHERE domain_id='$domain_id'";
    $data['obit_config'] = $db->getAll($query, DB_FETCHMODE_ASSOC);
}

file_put_contents($export_path, serialize($data));

echo "Export results:<br>";
foreach ($data as $key => $value) {
    echo $key . ": " . count($value) . " entries<br>";
}
echo "data written to $export_path<br>";
echo "done";
