<?php
 // remote obituaries embedding
include("inc/verse.inc.php"); //main header - initializes Verse environment

// for IE to allow to set session cookie within IFRAME
header('P3P:CP="IDC DSP COR ADM DEVi TAIi PSA PSD IVAi IVDi CONi HIS OUR IND CNT"');

$page = arg(0);
if (!$page) {
    // resolve obituary_search page for the domain and fetch it
    $query = "SELECT data, page_type, no_header, restricted FROM cms_page WHERE page_type=3 AND domain_id='$domain_id'";
    $cnt = $db->getRow($query, DB_FETCHMODE_ASSOC);
    if (DB::isError($cnt) || !$cnt) {
        // default obituary_search
        $cnt = array("data" => serialize(array("content1" => "{obituary_search style=\"style1\"}")), "page_type" => 3, "no_header" => 0, "restricted" => 0);
    }
} elseif(($page == 'online-obituary' || $page == 'obituary_view') && is_numeric(arg(1))) {
    // Redirect old obituary links without slug
    $query = "SELECT * FROM plg_obituary WHERE obituary_id=" . arg(1) . " AND domain_id=$domain_id";
    $obituary = $db->getRow($query, DB_FETCHMODE_ASSOC);

    if(!is_null($obituary)) {
        $slug = get_obituary_slug($obituary);

        header("HTTP/1.1 301 Moved Permanently");
        header("Location: /$page/$slug/".$obituary['obituary_id']);
        exit();
    }
} elseif($page == 'online-obituary' && arg(1) && arg(2)) {
    // handle with symfony 1
    ob_start();
    // special processing using symfony
    include_once($_SERVER['DOCUMENT_ROOT'] . '/../symfony/config/ProjectConfiguration.class.php');
    $configuration = ProjectConfiguration::getApplicationConfiguration('frontend', 'prod', false);
    sfContext::createInstance($configuration)->dispatch();
    $content = ob_get_contents();
    ob_end_clean();
    $content = str_replace(array('<head>', '</head>', '<body>', '</body>'), '', $content);
} else {
    // fetch content
    $query = "SELECT data, page_type, no_header, restricted FROM cms_page WHERE internal_name='$page' AND domain_id='$domain_id'";
    $cnt = $db->getRow($query, DB_FETCHMODE_ASSOC);
    if (DB::isError($cnt) || !$cnt) {
        // default obituary_search
        $cnt = array("data" => serialize(array("content1" => "{obituary_view style=\"stylenew\"}")), "page_type" => 4, "no_header" => 0, "restricted" => 0);
    }
}

if(!$page || $page != 'online-obituary' ) {
    // process content with chosen plugin
    include "cnt_plugins_map.php";
    $content_type = $plugins_map[$cnt["page_type"]];
    $plugin_name = "cnt_plugin_$content_type.php";
    if (!file_exists($plugin_name)) {
        $content_type = "static";
        $plugin_name = "cnt_plugin_$content_type.php";
    }
    include $plugin_name;
    $state = array_merge($_GET, $_POST);

    if($content_type == 'obit_view') {
        $id = array_pop(explode('/', $_REQUEST['p']));
        if(is_numeric($id)) {
            $state['id'] = $id;
        }
    }

    unset($state["p"]);
    $content = process_content(@unserialize($cnt["data"]), $state);
}

// fetch CSS
$query = "SELECT css FROM cms_site WHERE domain_id='$domain_id'";
$site = $db->getRow($query, DB_FETCHMODE_ASSOC);

$smarty->assign("site", $site);
$smarty->assign("content", $content);
$smarty->display("integrate.tpl");
