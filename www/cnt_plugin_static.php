<?php
 // static page plugin
include "cnt_plugin_main.php";

function process_content($content, $state) {
    if (isset($_SESSION['content_switcher']) && $content[$_SESSION['content_switcher']] != '') {
        // use choosen content
        return process_tags($content[$_SESSION['content_switcher']]);
    } else {
        // use content1 as default content
        return process_tags($content["content1"]);
    }
}
