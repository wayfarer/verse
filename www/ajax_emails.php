<?php
 // obsolete
// emails management, ajax handling actions
include("inc/verse.inc.php"); //main header - initializes Verse environment
include("inc/mailbox.inc.php");

if ($user->have_role(ROLE_USER_MANAGEMENT)) {
    $_POST = utf8_to_latin($_POST);
    $action = $_POST["action"];

    switch ($action) {
        case "list":
            $rep = createobject("report_ajax", array($db, "plg_mailbox", REP_WITH_PAGENATOR, true, 50, 'width="600px" style="background-color: #DDEEFF"'));

            $rep->add_field("Email", REP_STRING_TEMPLATE, "{name}@$domain_name", REP_ORDERABLE, 'align="left"');
            $rep->add_field("Type", REP_FUNCTIONCALL_TEMPLATE, 'email_type({type},{forward})', REP_ORDERABLE, 'align="center"');
            $rep->add_field("", REP_STRING_TEMPLATE, href_js_action("setup", "new EmailEditor({mailbox_id})"), REP_UNORDERABLE, 'align="center"');
            $rep->add_field("", REP_STRING_TEMPLATE, href_js_action("delete", "delete_mailbox({mailbox_id})"), REP_UNORDERABLE, 'align="center"');
            // for superadmin
            if (!$user->data["domain_id"]) {
                $rep->add_field("", REP_STRING_TEMPLATE, href_js_action("cyrus status", "check_mailbox({mailbox_id})"), REP_UNORDERABLE, 'align="center"');
            }

            $rep->add_filter("", REP_INVISIBLE_FILTER, "domain_id", "=", $domain_id);

            $rep->html_attributes('class="sortable" id="uid" width="600px"');

            if (count($_POST) < 3) {
                $rep->order_by("name", 0);
            }
            $rep->handle_events($_POST);

            $html = $rep->make_report();

            echo $html;
            break;
        case "load":
            $mailbox_id = intval(@$_POST["id"]);
            $query = "SELECT type, name, forward FROM plg_mailbox WHERE mailbox_id='$mailbox_id' AND domain_id='$domain_id'";
            $ret = $db->getRow($query, DB_FETCHMODE_ASSOC);
            $ret["forward"] = implode("\n", explode(",", $ret["forward"]));
            header("X-JSON:" . make_json_response($ret));
            break;
        case "save":
            $mailbox_id = intval(@$_POST["id"]);
            $p["type"] = intval(@$_POST["type"]);
            $password = in(@$_POST["mail_pass"]);
            // preporcess $forward
            $forwards = preg_split('/[\r\n]+/', @$_POST["forward"], -1, PREG_SPLIT_NO_EMPTY);
//      var_dump($forwards);
//			$forwards = explode("\n", in(@$_POST["forward"]));
            foreach ($forwards as $i => $forward) {
                if (!trim($forward)) unset($forwards[$i]);
            }
            $p["forward"] = implode(",", in($forwards));

            if ($mailbox_id) {
                // get old values to check if changes
                $query = "SELECT type, name, forward, login FROM plg_mailbox WHERE mailbox_id='$mailbox_id' AND domain_id='$domain_id'";
                $mailbox = $db->getRow($query, DB_FETCHMODE_ASSOC);
                if ($mailbox["type"] != $p["type"] || $mailbox["forward"] != $p["forward"]) {
                    // update mailbox
                    $query = "UPDATE plg_mailbox SET " . make_set_clause($p) . " WHERE mailbox_id='$mailbox_id' AND domain_id='$domain_id'";
                    $db->query($query);
                    generate_sendmail_files();
                }
                if ($password) {
                    update_mailbox_password($mailbox["login"], $password);
                }
            }
            else {
                $query = "SELECT postfix FROM sms_domain WHERE domain_id='$domain_id'";
                $postfix = $db->getOne($query);
                $p["name"] = in(@$_POST["name"]);
                $p["login"] = $p["name"] . "-" . $postfix;
                // create new
                $query = "INSERT plg_mailbox SET " . make_set_clause($p) . ", domain_id='$domain_id'";
                $db->query($query);
                add_mailbox($p["name"], $password);
                generate_sendmail_files();
            }
            break;
        case "delete":
            $mailbox_id = intval(@$_POST["id"]);
            // check if this is mailbox for current domain
            $query = "SELECT 1 FROM plg_mailbox WHERE mailbox_id='$mailbox_id' AND domain_id='$domain_id'";
            $allow = $db->getOne($query);
            if ($allow) {
                remove_mailbox($mailbox_id);
                generate_sendmail_files();
            }
            break;
        case "cyrus":
            $mailbox_id = intval(@$_POST["id"]);
            $query = "SELECT login FROM plg_mailbox WHERE mailbox_id='$mailbox_id' AND domain_id='$domain_id'";
            $login = $db->getOne($query);
            // check IMAP mail box (cyrus)
            $h = fsockopen("localhost", 143);
            fwrite($h, "VCMS LOGIN cyrus source\n");
            fwrite($h, "VCMS INFO user.$login\n");
            $reply = "";
//		    while ($fragment = fgets($h, 256)) {
//		       $reply += $fragment;
//		    }
            $fragment = fgets($h, 256);
            $reply .= $fragment;
            $fragment = fgets($h, 256);
            $reply .= $fragment;
            $fragment = fgets($h, 256);
            $reply .= $fragment;
            fclose($h);
            echo "Cyrus reply: $reply";
            break;
        case 'load_domain_email':
            $query = "SELECT email FROM sms_domain WHERE domain_id='$domain_id'";
            $ret = $db->getRow($query, DB_FETCHMODE_ASSOC);
            header("X-JSON:" . make_json_response($ret));
            break;
        case 'save_domain_email':
            $p['email'] = @$_POST['email'];
            $query = "UPDATE sms_domain SET " . make_set_clause($p) . " WHERE domain_id='$domain_id'";
            $db->query($query);
            break;

    }
}


function email_type($mailbox_type, $forward) {
    if ($mailbox_type == 0) {
        return "mailbox";
    }
    else {
        return "forward: " . implode("\n", explode(",", $forward));
    }
}
