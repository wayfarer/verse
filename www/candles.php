<?php
 // obsolete
// backend candles management
include("inc/verse.inc.php"); //main header - initializes Verse environment

if ($user->have_role(ROLE_OBITUARIES_MANAGEMENT)) {
    // $_POST = utf8_to_latin($_POST);
    // check if obituary exists
    $obituary_id = intval(@$_GET["id"]);
    $query = "SELECT 1 FROM plg_obituary WHERE obituary_id='$obituary_id' AND domain_id='$domain_id'";
    $ret = $db->getOne($query);
    if (!$ret) {
        header("Location: obituaries.php");
        exit;
    }

    $state = intval(@$_GET["state"]);
    switch ($state) {
        case 1:
            $action = "list_candles_notapproved";
            break;
        case 2:
            $action = "list_candles_hidden";
            break;
        default:
            $action = "list_candles";
    }

    $smarty->assign("obituary_id", $obituary_id);
    $smarty->assign("action", $action);
    $smarty->display("candles.tpl");
}
else {
    header("Location: login.php");
}