<?php
 // backend candle reports
include("inc/verse.inc.php"); //main header - initializes Verse environment

if ($user->is_super()) {
    include_once('inc/open_flash_chart_object.php');
    include_once('inc/open-flash-chart.php');
    //  	$_POST = utf8_to_latin($_POST);

    $action = @$_GET["action"];
    switch ($action) { // first pass
        case "cpd":
        case "cbw":
        case "cbh":
            // preprocess params
            $period_start = $_GET["begin_date"];
            $period_end = $_GET["end_date"];
            if (!$period_start) {
                $period_start = date("Y-m-d", time() - 2592000);
                $period_end = date("Y-m-d");
            }
            $rep_domain_id = intval(@$_GET["domain_id"]);
            if ($rep_domain_id) {
                // fetch domain name
                $query = "SELECT domain_name FROM sms_domain WHERE domain_id='$rep_domain_id'";
                $rep_domain_name = $db->getOne($query);
            }
            else {
                $rep_domain_name = "All domains";
            }
            break;
    }

    switch ($action) { // second pass
        case "cpd": // counts per day
            if ($rep_domain_id) {
                $domain_clause = " INNER JOIN plg_obituary USING(obituary_id)";
                $domain_where = " domain_id='$rep_domain_id' AND";
            }
            else {
                $domain_clause = "";
                $domain_where = "";
            }
            $query = "SELECT DATE_FORMAT(timestamp, '%Y-%m-%d, %a') date, count(*) cnt FROM plg_obituary_candle$domain_clause WHERE$domain_where timestamp>'$period_start' AND timestamp<='$period_end 23:59:59' GROUP BY TO_DAYS(timestamp)";
            $data = $db->getAll($query, DB_FETCHMODE_ASSOC);
            echo draw_report("Candles count per day ($period_start - $period_end)", "candles count", $rep_domain_name, $data, 1);
            break;
        case "cbw":
            if ($rep_domain_id) {
                $domain_clause = " INNER JOIN plg_obituary USING(obituary_id)";
                $domain_where = " domain_id='$rep_domain_id' AND";
            }
            else {
                $domain_clause = "";
                $domain_where = "";
            }
            $query = "SELECT DATE_FORMAT(timestamp, '%W') date, count(*) cnt FROM plg_obituary_candle$domain_clause WHERE$domain_where timestamp>'$period_start' AND timestamp<='$period_end 23:59:59' GROUP BY WEEKDAY(timestamp)";
            $data = $db->getAll($query, DB_FETCHMODE_ASSOC);
            echo draw_report("Candles count by weekday ($period_start - $period_end)", "candles count", $rep_domain_name, $data);
            break;
        case "cbh":
            if ($rep_domain_id) {
                $domain_clause = " INNER JOIN plg_obituary USING(obituary_id)";
                $domain_where = " domain_id='$rep_domain_id' AND";
            }
            else {
                $domain_clause = "";
                $domain_where = "";
            }
            $query = "SELECT HOUR(timestamp) date, count(*) cnt FROM plg_obituary_candle$domain_clause WHERE$domain_where timestamp>'$period_start' AND timestamp<='$period_end 23:59:59' GROUP BY HOUR(timestamp)";
            $data = $db->getAll($query, DB_FETCHMODE_ASSOC);
            echo draw_report("Candles count by hour ($period_start - $period_end)", "candles count", $rep_domain_name, $data);
            break;
        default:
            // fetch domains
            $query = "SELECT domain_id, domain_name FROM sms_domain WHERE mode<2 AND alias_domain_id=0 ORDER BY domain_name";
            $domains = $db->getAssoc($query);
            $domains = array(0 => "All domains") + $domains;
            $ofc_object = open_flash_chart_object_str(600, 280, 'candle_report.php?action=cpd'); // candles per day
            $ofc_object2 = open_flash_chart_object_str(600, 280, 'candle_report.php?action=cbw'); // candles by weekday
            $ofc_object3 = open_flash_chart_object_str(600, 280, 'candle_report.php?action=cbh'); // candles by server hour

            $period_start = date("Y-m-d", time() - 2592000);
            $period_end = date("Y-m-d");

            $smarty->assign("domains", $domains);
            $smarty->assign("ofc_object", $ofc_object);
            $smarty->assign("ofc_object2", $ofc_object2);
            $smarty->assign("ofc_object3", $ofc_object3);
            $smarty->assign("begin_date", $period_start);
            $smarty->assign("end_date", $period_end);
            $smarty->display("candle_report.tpl");
            break;
    }
}
else {
    header("Location: login.php");
}

function draw_report($title, $hint, $y_legend, $data, $x_orient = 0) {
    $counts = array();
    $labels = array();
    $max = 0;
    foreach ($data as $row) {
        $counts[] = $row["cnt"];
        $labels[] = $row["date"];
        if ($row["cnt"] > $max) $max = $row["cnt"];
    }

    $bar = new bar_outline(75, '#63A8DF', '#2378AF');
    $bar->key($hint, 10);
    $bar->data = $counts;

    $g = new graph();
    $g->bg_colour = '#EDFAFF';
    $g->x_axis_colour('#808080', '#808080');
    $g->y_axis_colour('#808080', '#808080');
    $g->title($title, '{font-size: 18px;}');

    $g->data_sets[] = $bar;
    $g->set_x_labels($labels);
    $g->set_x_label_style(10, '#2378AF', $x_orient); // size, color, orient

    $g->set_y_max($max);

    $g->set_y_legend($y_legend, 12, '#2378AF');

    return $g->render();
}
