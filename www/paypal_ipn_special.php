<?php

require("inc/config.inc.php");
require("inc/util.inc.php");
require_once("pear/DB.php");
require_once("pear/Log.php");

// init logging mechanism
$conf = array('title' => 'Log');
$logger = &Log::factory('file', 'logs/paypal_ipn.log', 'versesms');

$logger->info(print_r($_POST, true));

// write IPN packet raw data
$h = fopen('logs/raw/' . date("Ymd-His") . '.ipn', "w");
fwrite($h, serialize($_POST));
fclose($h);

// connect to DB
$db_con = createobject("db_versesms");
$db = $db_con->connect();
if (DB::isError($db)) {
    $logger->err($db->getMessage() . "\n" . $db->getUserInfo());

    header("HTTP/1.0 500 Internal Server Error");
    exit;
}

if (isset($_POST["domain_id"])) {
    // check for transaction data
    //	if($_SESSION["total"] != @$_POST["mc_gross"] || $_SESSION["business"] != @$_POST["receiver_email"]) {

    $status = @$_POST["payment_status"];
    // insert order
    $p["user_data"] = in(serialize($_POST));
    $p["status"] = $status;
    $p["total"] = $_POST["mc_gross"];
    $p["domain_id"] = $_POST["domain_id"];
    $p["payer"] = $_POST["payer_email"];
    $p["affiliate_id"] = 0;
    $p["affiliate_from"] = "";
    $p["timestamp"] = date("Y-m-d H:i:s", strtotime($_POST["payment_date"]));
    $query = "INSERT plg_product_order SET " . make_set_clause($p);
    $logger->info($query);
    $ret = $db->query($query);
    if (DB::isError($ret)) {
        $logger->err($ret->getMessage() . "\n" . $ret->getUserInfo());
    }
    else {
        $logger->info("DB query success");
    }

    // add domain name to $p to use in mailing
    /*  if($_SESSION["domain_name"]) {
     $p["domain_name"] = $_SESSION["domain_name"];
   }
   else {
     $query = "SELECT domain_name FROM sms_domain WHERE domain_id='".$_SESSION["domain_id"]."'";
     $p["domain_name"] = $db->getOne($query);
   }
   $p["user_data"] = $_POST;
   $mailto = $_SESSION["business"];
   if(check_email($mailto)) {
     send_order_notification($mailto, $p);
   }
   // notification to default domain's email if it is different
   $query = "SELECT email FROM sms_domain WHERE domain_id='".$_SESSION["domain_id"]."'";
   $domain_email = $db->getOne($query);
   if($domain_email!==$mailto && check_email($domain_email)) {
     send_order_notification($domain_email, $p);
   }

     unset($_SESSION["cart"]);
     unset($_SESSION["order_id"]);
     unset($_SESSION["total"]);
     unset($_SESSION["business"]); */
}
else {
    $logger->err("paypal IPN special packet: domain_id not found!");
}

function dump_session() {
    $session_id = session_id();
    $h = fopen('logs/raw/' . $session_id . '.ses', "w");
    fwrite($h, serialize($_SESSION));
    fclose($h);
}

function send_order_notification($mailto, $p) {
    global $logger;

    $data = $p["user_data"];

    $subject = $p["domain_name"] . ": new order notification";
    $body_template = <<<ORDERDETAILS
Order status: %s

User details:
%s

Order details:
%s

Shipping: $%s
Taxes:    $%s
Total:    $%s


Always yours, {$p["domain_name"]}'s notification robot.

ORDERDETAILS;
    $fields = array("Name" => "address_name", "Street" => "address_street", "City" => "address_city", "State" => "address_state", "ZIP" => "address_zip", "Country" => "address_country", "Email" => "payer_email", "Pending reason" => "pending_reason", "Memo" => "memo");
    $user_details = "";
    foreach ($fields as $title => $field) {
        $user_details .= "$title: " . $data[$field] . "\n";
    }

    $order_details = "";
    $i = 1;
    while (isset($data["item_name$i"])) {
        $order_details .= $data["item_name$i"] . " x " . $data["quantity$i"] . " $" . $data["mc_gross_$i"] . "\n";
        $i++;
    }

    $body = sprintf($body_template, $p["status"], $user_details, $order_details, $data["mc_shipping"], $data["tax"], $data["mc_gross"]);

    $robot_email = "robot@twintierstech.net";
    $ret = mail($mailto, $subject, $body, "From: $robot_email");
    if ($ret) {
        $status = "sent";
    }
    else {
        $status = "failed to send";
    }

    $logger->info("status: $status, mailing: $mailto, $subject, $body");
}
