<?php
 // obsolete
// backend products management
include("inc/verse.inc.php"); //main header - initializes Verse environment

if ($user->logged()) {
    if (!isset($_GET["print"])) {
        $query = "SELECT category_id, name FROM plg_product_category WHERE domain_id='$domain_id'";
        $product_categories = $db->getAssoc($query);
        $query = "SELECT ecommerce_enabled FROM sms_domain WHERE domain_id='$domain_id'";
        $ecommerce_enabled = $db->getOne($query);
        // fetch existing pages for ecommerce config dialog
        $query = "SELECT internal_name, internal_name FROM cms_page WHERE domain_id='$domain_id' ORDER BY internal_name";
        $pages = $db->getAssoc($query);

        $smarty->assign("product_categories", $product_categories);
        $smarty->assign("ecommerce_enabled", $ecommerce_enabled);
        $smarty->assign("affiliates_enabled", ($domain->have_feature("affiliates_enabled") && $user->have_role(ROLE_AFFILIATES)));
        $smarty->assign("pages", $pages);
        $smarty->display("products.tpl");
    }
    else {
        if (isset($_GET["p"])) {
            $product_id = intval($_GET["p"]);
            $query = "SELECT image, name, price, description FROM plg_product WHERE product_id='$product_id' AND domain_id='$domain_id'";
            $ret = $db->getAll($query, DB_FETCHMODE_ASSOC);
            $smarty->assign("products", $ret);
            $smarty->display("products_print.tpl");
        }
        else {
            $rep = createobject("report_ajax", array($db, "plg_product", REP_NO_PAGENATOR, true, 0, 'width="700px" style="background-color: #DDEEFF"'));
            $rep->add_table("plg_product_category", REP_INNER_JOIN, "category_id");

            $rep->add_field("Product", REP_STRING_TEMPLATE, '{name|1}', REP_UNORDERABLE, 'align="center"');
            $rep->add_field("Description", REP_CALLBACK, 'strip_tags({description})', REP_UNORDERABLE, 'align="center"');
            $rep->add_field("Price", REP_STRING_TEMPLATE, '${price}', REP_UNORDERABLE, 'align="center"');

            $rep->add_filter("", REP_INVISIBLE_FILTER, "t1.domain_id", "=", $domain_id);

            $rep->html_attributes('width="700px"');

            $html = $rep->make_report();

            $html .= '<script>window.print();</script>';
            echo $html;
        }
    }
}
else {
    header("Location: login.php");
}
