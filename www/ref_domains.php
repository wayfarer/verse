<?php
  set_time_limit(0);

if (!count($_POST)) {
    display_form();
}
else {
    $domain = $_POST["domain"];
    $nofilter = intval(@$_POST["nofilter"]);
    $data = get_referring_domains($domain, $nofilter);

    // print results
    echo "Analyzed domain: <b>", $domain, "</b><br>";
    echo "Search results (referring pages): <b>", $data["results_parsed"], "</b><br>";
    echo "Referring domains count: <b>", count($data["domains"]), "</b><br>";
    echo "<hr>";

    uasort($data["domains"], count_cmp);
    foreach ($data["domains"] as $domain => $data) {
        echo "<b>$domain (", count($data), " pages)</b><br>";
        foreach ($data as $link) {
            echo "<li><a href=\"$link\" target=\"_blank\">$link</a></li>";
        }
    }
    echo "done.";
}

function count_cmp($a, $b) {
    $ca = count($a);
    $cb = count($b);
    if ($ca == $cb) {
        return 0;
    }
    return ($ca < $cb) ? 1 : -1;
}

function display_form() {
    echo <<<ref
<form method="post">
  domain to analyze:<br>
  <input type="text" name="domain"><br>
  <input type="checkbox" name="nofilter" value="1"> also show similar result pages<br>
  <input type="submit" value="analyze!">
</form>
ref;
}

function get_referring_domains($domain, $nofilter) {
    $google_rpp = 100;
    $max_pages = 10;
    $results = array();
    $pages_parsed = 0;
    $last_results_count = $google_rpp;

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

    while ($pages_parsed < $max_pages && $last_results_count == $google_rpp) {
        $start = $pages_parsed * $google_rpp;
        $url = "http://www.google.com/search?hl=en&q=link%3A$domain&num=$google_rpp&start=$start";
        if ($nofilter) $url .= "&filter=0";

        curl_setopt($ch, CURLOPT_URL, $url);
        if ($pages_parsed > 0) sleep(1); // 1sec pause to be friendly to google server, do not flood it with requests
        $content = curl_exec($ch);
        if (curl_errno($ch)) {
            echo "An error occured, " . curl_error($ch), "<br>url=$url<br>";
            return null;
        }

        // parse content
        $last_results_count = preg_match_all("/<h2 class=r><a href=\"(.*?)\"/mis", $content, $matches);
        foreach ($matches[1] as $link) {
            $parts = parse_url($link);
            $domain_name = $parts["host"];
            $results["domains"][$domain_name][] = $link;
        }
        $pages_parsed++;
    }
    $results["results_parsed"] = ($pages_parsed - 1) * $google_rpp + $last_results_count;
    curl_close($ch);
    return $results;
}
