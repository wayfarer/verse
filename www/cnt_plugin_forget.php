<?php
 // site members, plugin for forget password page
include "cnt_plugin_main.php";

function process_content($content, $state) {
    global $domain_id, $domain_name, $db;

    // validation
    $error_count = 0;
    if (!empty($state)) {
        $emailto = $state["email"];

        // validate params - find a user
        $query = "SELECT user_id FROM cms_access_user WHERE email='" . in($emailto) . "' AND domain_id='$domain_id'";
        $user_id = $db->getOne($query);

        if (!$user_id) {
            $error_count++;
            verse_set_message("Your email is not found on our records");
        }
    }

    if (empty($state) || $error_count > 0) { // show mailing form by default
        $ret = process_tags($content["content1"]);
        if ($ret) {
            // user entered registration form
            // add form tag only if not there
            if (strpos($ret, "<form") === false) {
                $ret = '<form method="post">' . $ret . '</form>';
            }
        }
        else {
            // default registration form
            $ret = file_get_contents("templates/helpers/forgetpassword_form.html");
        }
        return $ret;
    }
    else { // mail posted fields
        if (!isset($state["debug"])) {
            if ($user_id) {
                $query = "SELECT email FROM sms_domain WHERE domain_id='" . $GLOBALS["domain_id"] . "'";
                $emailfrom = $GLOBALS["db"]->getOne($query);

                $emailfromname = $GLOBALS["domain_name"];
                $subject = "$domain_name: forget password request";
                $text = file_get_contents("templates/helpers/forgetpassword_email.txt");

                $hash = substr(md5(uniqid(rand())), 0, 16);
                $login_url = "http://$domain_name/subscribe.php?action=forget-password&id=$hash";
                $text = str_replace(array("{login_url}"), array($login_url), $text);

                $query = "UPDATE cms_access_user SET hash='$hash' WHERE user_id='$user_id' AND domain_id='$domain_id'";
                $ret = $db->query($query);

                send_email($emailto, $subject, $emailfrom, $emailfromname, $text);
            }
        }
        if ($content["content2"]) return process_tags($content["content2"]);
        else return "<div>Please, check your e-mail to access members area</div>";
    }
}

function send_email($emailto, $subject, $emailfrom, $emailfromname, $text) {
    global $domain_name;

    return mail($emailto, $subject, $text, "From: $emailfromname <$emailfrom>");
}
  