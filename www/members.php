<?php
 // obsolete
// backend site members management
include("inc/verse.inc.php"); //main header - initializes Verse environment

if ($user->have_role(ROLE_MEMBERS_MANAGEMENT)) {
    /*		$query ="SELECT postfix FROM sms_domain WHERE domain_id='$domain_id'";
         $postfix = $db->getOne($query);
         $smarty->assign("domain_name", $domain_name);
         $smarty->assign("postfix", $postfix); */

    $action = @$_POST["action"];
    switch ($action) {
        default:
            $smarty->display("members.tpl");
            break;
        case "list":
            $rep = createobject("report_ajax", array($db, "cms_access_user", REP_WITH_PAGENATOR, true, 100, 'width="750px" style="background-color: #DDEEFF"'));

            $rep->add_field("name", REP_STRING_TEMPLATE, "{name}", REP_ORDERABLE, 'align="center"');
            $rep->add_field("login", REP_STRING_TEMPLATE, "{login}", REP_ORDERABLE, 'align="center"');
            $rep->add_field("email", REP_STRING_TEMPLATE, "{email}", REP_ORDERABLE, 'align="center"');
            $rep->add_field("created", REP_STRING_TEMPLATE, "{created_at}", REP_ORDERABLE, 'align="center"');
            $rep->add_field("status", REP_CALLBACK, "status({enabled})", REP_ORDERABLE, 'align="center"');
            $rep->add_field("privileges", REP_CALLBACK, "privileges({privileges})", REP_ORDERABLE, 'align="center"');

            $rep->add_field("", REP_STRING_TEMPLATE, href_js_action("edit", "new MemberEditor({user_id})"), REP_UNORDERABLE);
            $rep->add_field("", REP_CALLBACK, "approve({user_id},{enabled})", REP_UNORDERABLE);
            $rep->add_field("", REP_CALLBACK, "block({user_id},{enabled})", REP_UNORDERABLE);
            $rep->add_field("", REP_STRING_TEMPLATE, href_js_action("delete", "delete_member({user_id})"), REP_UNORDERABLE);

            $rep->add_filter("", REP_INVISIBLE_FILTER, "domain_id", "=", $domain_id);

//				$rep->add_filter("", REP_INVISIBLE_FILTER, "enabled", ">", 0);

            $rep->html_attributes('width="750px"');

            // default sort
            if (count($_POST) < 3) {
                $rep->order_by("name", 0);
            }

            $rep->handle_events($_POST);
            $html = $rep->make_report();
            echo $html;
            break;
        case "load":
            $user_id = intval(@$_POST["id"]);
            $query = "SELECT login ulogin, name, email, privileges moderator FROM cms_access_user WHERE user_id='$user_id' AND domain_id='$domain_id'";
            $ret = $db->getRow($query, DB_FETCHMODE_ASSOC);
            header("X-JSON:" . make_json_response($ret));
            break;
        case "save":
            $user_id = intval(@$_POST["id"]);
            $p = array();
            $p["login"] = in(@$_POST["ulogin"]);
            $p["name"] = in(@$_POST["name"]);
            $p["email"] = in(@$_POST["email"]);
            $p["privileges"] = @$_POST["moderator"] ? 1 : 0;
            $password = in(@$_POST["password"]);
            if (strlen($password)) {
                $p["password"] = $password;
            }

            if (!$p["email"]) $p["email"] = null;

            if ($user_id) {
                $query = "UPDATE cms_access_user SET " . make_set_clause($p) . " WHERE user_id='$user_id' AND domain_id='$domain_id'";
                $db->query($query);
            }
            else {
                $p["domain_id"] = $domain_id;
                $p["hash"] = "admin";
                $p["enabled"] = 1;
                $query = "INSERT cms_access_user SET " . make_set_clause($p) . ", created_at=now()";
                $ret = $db->query($query);
                //  				$query = "SELECT last_insert_id()";
                //  				$user_id = $db->getOne($query);
            }
            break;
        case "delete":
            $user_id = intval(@$_POST["id"]);
            $query = "DELETE FROM cms_access_user WHERE user_id='$user_id' AND domain_id='$domain_id'";
            $db->query($query);
            break;
        case "approve":
            $user_id = intval(@$_POST["id"]);
            $query = "UPDATE cms_access_user SET enabled=1 WHERE user_id='$user_id' AND domain_id='$domain_id'";
            $db->query($query);
            if ($db->affectedRows()) {
                send_approve_confirmation($user_id);
            }
            break;
        case "block":
            $user_id = intval(@$_POST["id"]);
            $query = "UPDATE cms_access_user SET enabled=2 WHERE user_id='$user_id' AND domain_id='$domain_id'";
            $db->query($query);
            if ($db->affectedRows()) {
                send_block_information($user_id);
            }
            break;
        case "unblock":
            $user_id = intval(@$_POST["id"]);
            $query = "UPDATE cms_access_user SET enabled=1 WHERE user_id='$user_id' AND domain_id='$domain_id'";
            $db->query($query);
            if ($db->affectedRows()) {
                send_unblock_information($user_id);
            }
            break;
        case "load_config":
            $query = "SELECT param, value FROM cms_config WHERE param='member_signup_notification_email' AND domain_id='$domain_id'";
            $ret = $db->getAssoc($query);
            header("X-JSON:" . make_json_response($ret));
            break;
        case "save_config":
            $member_signup_notification_email = in(@$_POST["member_signup_notification_email"]);
            $query = "REPLACE cms_config SET domain_id='$domain_id', param='member_signup_notification_email', value='$member_signup_notification_email'";
            $db->query($query);
            break;
    }
}
else {
    header("Location: login.php");
}

function approve($user_id, $status) {
    if ($status == 3 || $status == 0) {
        return href_js_action("approve", "approve_member($user_id)");
    }
    else {
        return "";
    }
}

function status($status) {
    switch ($status) {
        case 1:
            return '<span style="color:#0A0">active</span>';
            break;
        case 2:
            return 'blocked';
            break;
        case 3:
            return '<span style="color:#E00">new</span>';
            break;
        case 0:
            return '<span style="color:#999">unconfirmed</span>';
            break;
    }
}

function block($user_id, $status) {
    if ($status != 2) {
        return href_js_action("block", "block_member($user_id)");
    }
    else {
        return href_js_action("unblock", "unblock_member($user_id)");
    }
}

function send_approve_confirmation($user_id) {
    send_information($user_id, "approve");
}

function send_block_information($user_id) {
    send_information($user_id, "block");
}

function send_unblock_information($user_id) {
    send_information($user_id, "unblock");
}

function send_information($user_id, $mode) {
    global $domain_id, $domain_name, $db;

    switch ($mode) {
        case "approve":
            $subject = "$domain_name, your account has been approved";
            $text = file_get_contents("templates/helpers/approved_email.txt");
            break;
        case "block":
            $subject = "$domain_name, your account has been blocked";
            $text = file_get_contents("templates/helpers/blocked_email.txt");
            break;
        case "unblock":
            $subject = "$domain_name, your account has been unblocked";
            $text = file_get_contents("templates/helpers/unblocked_email.txt");
            break;
    }

    $query = "SELECT email FROM cms_access_user WHERE user_id='$user_id' AND domain_id='$domain_id'";
    $mailto = $db->getOne($query);

    // fetch default email for the domain
    $query = "SELECT email FROM sms_domain WHERE domain_id='" . $GLOBALS["domain_id"] . "'";
    $emailfrom = $db->getOne($query);
    $emailfromname = $domain_name;
    $text = str_replace(array("{domain_name}"), array($domain_name), $text);

    send_email($mailto, $subject, $emailfrom, $emailfromname, $text);
}

function send_email($emailto, $subject, $emailfrom, $emailfromname, $text) {
    return mail($emailto, $subject, $text, "From: $emailfromname <$emailfrom>");
}

function privileges($privileges) {
    switch ($privileges) {
        case 1:
            return '<span style="color:#555">moderator</span>';
            break;
        case 0:
            return '<span style="color:#555">user</span>';
            break;
    }
}
  