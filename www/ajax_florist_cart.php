<?php

include("inc/verse.inc.php");

$action = $_POST['action'];

switch ($action) {
    case "cart_add":
        $product_code = $_POST["product_code"];
        $cart_count = intval($_POST["cart_count"]);
        @$_SESSION["florist_cart"]["$product_code"] += $cart_count;
        if(@$_POST["update_list"] == 1) {
            print_cart_list();
            break;
        }
    // pass to next case to show item count
    case "cart_show":
        $item_count = @array_sum(@$_SESSION["florist_cart"]);
        if ($item_count) {
            echo "<b>$item_count</b> item(s)";
        } else {
            echo "empty";
        }
        break;
    case "cart_remove_inplace":
        $product_code = $_POST["product_code"];
        $cart_count = intval($_POST["cart_count"]);
        if (@$_SESSION["florist_cart"]["$product_code"] > 0) {
            $_SESSION["florist_cart"]["$product_code"] -= $cart_count;
        }
        if ($_SESSION["florist_cart"]["$product_code"] <= 0) {
            unset($_SESSION["florist_cart"]["$product_code"]);
        }
        $item_count = @array_sum(@$_SESSION["florist_cart"]);
        if ($item_count) {
            echo "<b>$item_count</b> item(s)";
        }
        else {
            echo "empty";
        }
        break;
    case "cart_remove":
        $product_code = $_POST["product_code"];
        $cart_count = intval($_POST["cart_count"]);
        if (@$_SESSION["florist_cart"]["$product_code"] > 0) {
            $_SESSION["florist_cart"]["$product_code"] -= $cart_count;
        }
        if ($_SESSION["florist_cart"]["$product_code"] <= 0) {
            unset($_SESSION["florist_cart"]["$product_code"]);
        }
    // pass to next case to show cart
    case "cart_list":
        print_cart_list();
        break;
    case "get_delivery_dates":
        get_delivery_dates(@$_POST['zip']);
        break;
}

function print_cart_list() {
    global $smarty;

    if (@array_sum(@$_SESSION["florist_cart"])) {
        require_once($_SERVER['DOCUMENT_ROOT'] . '/../util/soap/nusoap.php');
        include_once($_SERVER['DOCUMENT_ROOT'] . '/../symfony/config/ProjectConfiguration.class.php');
        $configuration = ProjectConfiguration::getApplicationConfiguration('frontend', 'prod', false);

        $server_url = sfConfig::get('app_florist_one_soap');
        $key = sfConfig::get('app_florist_one_key');
        $pass = sfConfig::get('app_florist_one_pass');

        $client = new nusoap_client($server_url, true);

        $total = 0;
        $products = array();

        foreach($_SESSION["florist_cart"] as $code=>$count) {
            $result = $client->call('getProduct2013', array("APIKey" => $key, "APIPassword" => $pass, "itemCode" => $code));
            if($result && !$result['errors']) {
                $product = $result['product'];
                $products[] = array(
                    'code' => $product['code'],
                    'name' => $product['name'],
                    'image' => $product['thumbnail'] ? $product['thumbnail'] : $product['small'],
                    'price' => $product['price'],
                    'count' => $count,
                    'amount' => $product['price']*$count
                );

                $total += $product['price']*$count;
            }
        }

        $smarty->assign("products", $products);
        $smarty->assign("total", $total);

        $html = $smarty->fetch("cnt_plugin_florist_cart_list.tpl");
    } else {
        $html = "Shopping cart is empty";
    }
    echo $html;
}

function get_delivery_dates($zip) {
    $html = '<option value="">--Please, fill in "Zip" field--</option>';
    if($zip) {
        require_once($_SERVER['DOCUMENT_ROOT'] . '/../util/soap/nusoap.php');
        include_once($_SERVER['DOCUMENT_ROOT'] . '/../symfony/config/ProjectConfiguration.class.php');
        $configuration = ProjectConfiguration::getApplicationConfiguration('frontend', 'prod', false);

        $server_url = sfConfig::get('app_florist_one_soap');
        $key = sfConfig::get('app_florist_one_key');
        $pass = sfConfig::get('app_florist_one_pass');

        $client = new nusoap_client($server_url, true);
        $result = $client->call('getDeliveryDates', array("APIKey" => $key, "APIPassword" => $pass, "zipcode" => $zip));
        if($result && !$result['errors']) {
            $html = '';
            foreach($result['dates'] as $date) {
                $html .= '<option value="'.$date.'">'.date("l, F d, Y", strtotime($date)).'</option>';
            }
        }
    }

    echo $html;
}

