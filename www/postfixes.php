<?php
    include("inc/verse.inc.php"); //main header - initializes Verse environment

if ($user->logged()) {
    $query = "SELECT postfix, domain_name FROM sms_domain WHERE mode < 2 AND alias_domain_id=0 ORDER BY postfix";
    $ret = $db->getAll($query, DB_FETCHMODE_ASSOC);

    foreach ($ret as $domain) {
        echo "<b>" . $domain["postfix"] . "</b> - " . $domain["domain_name"] . "<br>";
    }
}
else {
    header("Location: login.php");
}
