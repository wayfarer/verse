<?php
 // frontend ajax product display feature
include("inc/verse.inc.php"); //main header - initializes Verse environment
include("inc/imageutil.inc.php");

//	$_POST = utf8_to_latin($_REQUEST);
$action = $_POST["action"];
$elpp = @$_POST["elpp"] ? $_POST["elpp"] : 10;
$pass = @$_POST["pass"]; // uniq id to display multiple lists on one page
$nopagenator = @$_POST["nopg"];
$deforder = @$_POST["deforder"];

switch ($action) {
    case "list":
        //$query = "SELECT ecommerce_enabled, price_enabled FROM sms_domain WHERE domain_id='$domain_id'";
        //$options = $db->getRow($query, DB_FETCHMODE_ASSOC);
        $p = @$_POST["p"];
        $cat = @$_POST["cat"];

        $options = array("ecommerce_enabled" => $domain->get("ecommerce_enabled"), "price_enabled" => $domain->get("price_enabled"));
        if ($cat && $cat != "all") {
            // fetch category settings
            $query = "SELECT ecommerce_enabled, price_enabled FROM plg_product_category WHERE category_id='" . in($cat) . "' AND domain_id='$domain_id'";
            $cat_options = $db->getRow($query, DB_FETCHMODE_ASSOC);
            if ($cat_options["ecommerce_enabled"] > 0) {
                $options["ecommerce_enabled"] = $cat_options["ecommerce_enabled"];
            }
            if ($cat_options["price_enabled"] > 0) {
                $options["price_enabled"] = $cat_options["price_enabled"];
            }
        }

        $rep = createobject("report", array($db, "plg_product", $nopagenator ? REP_NO_PAGENATOR
                                            : REP_WITH_PAGENATOR, true, $elpp, 'id="products_pagenator" style="background-color: #DDDDDD"', $pass));

        $rep->add_field("", REP_FUNCTIONCALL_TEMPLATE, 'image({image})', REP_UNORDERABLE, 'align="center" width="20%"');
        $rep->add_field("Product", REP_STRING_TEMPLATE, '<a href="?p=' . $p . '&id={product_id}">{name}</a>', REP_ORDERABLE, 'align="center" width="15%"');
        if ($options["ecommerce_enabled"] == 1 || $options["price_enabled"] == 1) {
            $rep->add_field("Price", REP_STRING_TEMPLATE, '<b>${price}</b>', REP_ORDERABLE, 'align="center" width="10%"');
            $rep->add_field("Description", REP_STRING_TEMPLATE, '{description}', REP_ORDERABLE, 'align="center" width="45%"');
        }
        else {
            $rep->add_field("Description", REP_STRING_TEMPLATE, '{description}', REP_ORDERABLE, 'align="center" width="65%"');
        }
        if ($options["ecommerce_enabled"] == 1) {
            $rep->add_field("", REP_FUNCTIONCALL_TEMPLATE, 'cart({product_id})', REP_UNORDERABLE, 'align="center" width="10%"');
        }

        $rep->add_filter("", REP_INVISIBLE_FILTER, "domain_id", "=", $domain_id);
        // filter options
/*		    $query = "SELECT category_id, name FROM plg_product_category WHERE domain_id='$domain_id'";
		    $category_id_options = $db->getAssoc($query);
		    if(!$cat) {
		    	$cat = array_keys($category_id_options);
		    	$cat = $cat[0];
		    }
//		    $rep->add_filter("Product type", REP_SELECT_FILTER, "category_id", "=", $cat);
*/
        if ($cat && $cat != "all") {
            $rep->add_filter("Product type", REP_INVISIBLE_FILTER, "category_id", "=", $cat);
        }

        $rep->add_filter("", REP_INVISIBLE_FILTER, "enabled", "=", 1);

        // order
        if ($deforder) {
            $vals = explode(",", $deforder);
            $rep->order_by($vals[0], ($vals[1] == "desc") ? 1 : 0);
        }

        $rep->html_attributes('id="products_report"');

        $rep->handle_events($_POST);

        $html = $rep->make_report();

//      echo '<div style="display:none">', $rep->query, '</div>';

        echo str_replace('\n', "<br>", html_entity_decode($html));
        break;
    case "cart_add":
        $item_id = intval($_POST["id"]);
        $cart_count = intval($_POST["cart_count"]);
        @$_SESSION["cart"][$item_id] += $cart_count;
        // clear order_id if set
        unset($_SESSION["order_id"]);
        if(isset($_POST["update_list"]) && $_POST["update_list"] == 1) {
            print_cart_list();
            break;
        }
    // pass to next case to show item count
    case "cart_show":
        $item_count = @array_sum(@$_SESSION["cart"]);
        if ($item_count) {
            echo "<b>$item_count</b> item(s)";
        }
        else {
            echo "empty";
        }
        break;
    case "cart_remove_inplace":
        $item_id = intval($_POST["id"]);
        $cart_count = intval($_POST["cart_count"]);
        if (@$_SESSION["cart"][$item_id] > 0) {
            $_SESSION["cart"][$item_id] -= $cart_count;
        }
        if ($_SESSION["cart"][$item_id] <= 0) {
            unset($_SESSION["cart"][$item_id]);
        }
        $item_count = @array_sum(@$_SESSION["cart"]);
        if ($item_count) {
            echo "<b>$item_count</b> item(s)";
        }
        else {
            echo "empty";
        }
        unset($_SESSION["order_id"]);
        break;
    case "cart_remove":
        $item_id = intval($_POST["id"]);
        $cart_count = intval($_POST["cart_count"]);
        if (@$_SESSION["cart"][$item_id] > 0) {
            $_SESSION["cart"][$item_id] -= $cart_count;
        }
        if ($_SESSION["cart"][$item_id] <= 0) {
            unset($_SESSION["cart"][$item_id]);
        }
        unset($_SESSION["order_id"]);
    // pass to next case to show cart
    case "cart_list":
        print_cart_list();
        break;
}

function print_cart_list() {
    global $domain, $domain_id, $db, $smarty;

    if (@array_sum(@$_SESSION["cart"])) {
        $query = "SELECT product_id, p.domain_id, p.name, price, image, additional_fields, field1enabled, field1title, field1type, field2enabled, field2title, field2type FROM plg_product p LEFT JOIN plg_product_category c USING(category_id) WHERE product_id IN(" . implode(",", array_keys($_SESSION["cart"])) . ")";
        $products = $db->getAssoc($query, false, null, DB_FETCHMODE_ASSOC);
        $data = array();
        $total = 0;
        foreach ($products as $product_id => $vals) {
            $products[$product_id]["count"] = $_SESSION["cart"][$product_id];
            $products[$product_id]["amount"] = $vals["price"] * $products[$product_id]["count"];
            $products[$product_id]["image"] = image($vals["image"], $vals["domain_id"]);
            $products[$product_id]['product_id'] = $product_id;
            $total += $products[$product_id]["amount"];
        }

        $product_owner_domain_id = $domain->getParentProductDomainId();

        $query = "SELECT paypal_checkout FROM sms_domain WHERE domain_id='$product_owner_domain_id'";
        $paypal_checkout = $db->getOne($query);

        // get config
        $query = "SELECT param, value FROM plg_product_config WHERE domain_id='$product_owner_domain_id'";
        $config = $db->getAssoc($query);

        $_SESSION["business"] = $config["paypal_business"]; // store business name for validating IPN
        $_SESSION["product_owner_domain_id"] = $product_owner_domain_id;
        if ($product_owner_domain_id != $domain_id) {
            $_SESSION["product_owner_domain_name"] = $db->getOne("SELECT domain_name FROM sms_domain WHERE domain_id = '$product_owner_domain_id'");
        }
        $smarty->assign("paypal_checkout", $paypal_checkout);
        $smarty->assign("cart", $products);
        $smarty->assign("total", $total);
        $smarty->assign("txn", session_id());
        $smarty->assign("config", $config);
        $html = $smarty->fetch("cnt_plugin_cart_list.tpl");
    } else {
        $html = "Shopping cart is empty";
    }
    echo $html;
}

function image($image, $product_domain_id = null) {
    global $domain_id, $db;
    
    if (!strlen($image)) {
        $image = "image/products/noimage-tn.jpg";
    }
    else {
        //$image = str_replace(".", "-tn.", $image);
        if(!is_null($product_domain_id) && $product_domain_id != $domain_id) {
            $product_domain_name = $db->getOne("SELECT domain_name FROM sms_domain WHERE domain_id = '$product_domain_id'");
            $image = $product_domain_name.leading_slash($image);
        }
        $image = thumbnail($image, 100);
    }

    return '<img src="'.leading_slash($image).'">';
}

function cart($product_id) {
    $ret = '<a href="#" onclick="cart(' . $product_id . ');return false;">add to cart</a>';
    $ret .= '<br><small id="p' . $product_id . '" count="' . intval(@$_SESSION["cart"][$product_id]) . '">';
    if (isset($_SESSION["cart"][$product_id])) {
        $ret .= $_SESSION["cart"][$product_id] . " item(s) in cart</small>";
        $ret .= '<br><div id="r' . $product_id . '">';
    }
    else {
        $ret .= "</small>";
        $ret .= '<br><div style="display: none" id="r' . $product_id . '">';
    }
    $ret .= '<a href="#" onclick="cart_remove(' . $product_id . ');return false;">remove</a>';
    $ret .= '<br><a href="?p=cart">review cart</a>';
    $ret .= '</div>';
    return $ret;
}
