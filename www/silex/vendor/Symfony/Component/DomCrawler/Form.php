<?php
namespace Symfony\Component\DomCrawler;
use Symfony\Component\DomCrawler\Field\FormField;
class Form extends Link implements \ArrayAccess
{
    private $button;
    private $fields;
    public function __construct(\DOMNode $node, $currentUri, $method = null)
    {
        parent::__construct($node, $currentUri, $method);
        $this->initialize();
    }
    public function getFormNode()
    {
        return $this->node;
    }
    public function setValues(array $values)
    {
        foreach ($values as $name => $value) {
            $this[$name] = $value;
        }
        return $this;
    }
    public function getValues()
    {
        $values = array();
        foreach ($this->fields as $name => $field) {
            if ($field->isDisabled()) {
                continue;
            }
            if (!$field instanceof Field\FileFormField && $field->hasValue()) {
                $values[$name] = $field->getValue();
            }
        }
        return $values;
    }
    public function getFiles()
    {
        if (!in_array($this->getMethod(), array('POST', 'PUT', 'DELETE'))) {
            return array();
        }
        $files = array();
        foreach ($this->fields as $name => $field) {
            if ($field->isDisabled()) {
                continue;
            }
            if ($field instanceof Field\FileFormField) {
                $files[$name] = $field->getValue();
            }
        }
        return $files;
    }
    public function getPhpValues()
    {
        $qs = http_build_query($this->getValues());
        parse_str($qs, $values);
        return $values;
    }
    public function getPhpFiles()
    {
        $qs = http_build_query($this->getFiles());
        parse_str($qs, $values);
        return $values;
    }
    public function getUri()
    {
        $uri = parent::getUri();
        if (!in_array($this->getMethod(), array('POST', 'PUT', 'DELETE')) && $queryString = http_build_query($this->getValues(), null, '&')) {
            $sep = false === strpos($uri, '?') ? '?' : '&';
            $uri .= $sep.$queryString;
        }
        return $uri;
    }
    protected function getRawUri()
    {
        return $this->node->getAttribute('action');
    }
    public function getMethod()
    {
        if (null !== $this->method) {
            return $this->method;
        }
        return $this->node->getAttribute('method') ? strtoupper($this->node->getAttribute('method')) : 'GET';
    }
    public function has($name)
    {
        return isset($this->fields[$name]);
    }
    public function remove($name)
    {
        unset($this->fields[$name]);
    }
    public function get($name)
    {
        if (!$this->has($name)) {
            throw new \InvalidArgumentException(sprintf('The form has no "%s" field', $name));
        }
        return $this->fields[$name];
    }
    public function set(Field\FormField $field)
    {
        $this->fields[$field->getName()] = $field;
    }
    public function all()
    {
        return $this->fields;
    }
    private function initialize()
    {
        $this->fields = array();
        $document = new \DOMDocument('1.0', 'UTF-8');
        $node = $document->importNode($this->node, true);
        $button = $document->importNode($this->button, true);
        $root = $document->appendChild($document->createElement('_root'));
        $root->appendChild($node);
        $root->appendChild($button);
        $xpath = new \DOMXPath($document);
        foreach ($xpath->query('descendant::input | descendant::textarea | descendant::select', $root) as $node) {
            if (!$node->hasAttribute('name')) {
                continue;
            }
            $nodeName = $node->nodeName;
            if ($node === $button) {
                $this->set(new Field\InputFormField($node));
            } elseif ('select' == $nodeName || 'input' == $nodeName && 'checkbox' == $node->getAttribute('type')) {
                $this->set(new Field\ChoiceFormField($node));
            } elseif ('input' == $nodeName && 'radio' == $node->getAttribute('type')) {
                if ($this->has($node->getAttribute('name'))) {
                    $this->get($node->getAttribute('name'))->addChoice($node);
                } else {
                    $this->set(new Field\ChoiceFormField($node));
                }
            } elseif ('input' == $nodeName && 'file' == $node->getAttribute('type')) {
                $this->set(new Field\FileFormField($node));
            } elseif ('input' == $nodeName && !in_array($node->getAttribute('type'), array('submit', 'button', 'image'))) {
                $this->set(new Field\InputFormField($node));
            } elseif ('textarea' == $nodeName) {
                $this->set(new Field\TextareaFormField($node));
            }
        }
    }
    public function offsetExists($name)
    {
        return $this->has($name);
    }
    public function offsetGet($name)
    {
        if (!$this->has($name)) {
            throw new \InvalidArgumentException(sprintf('The form field "%s" does not exist', $name));
        }
        return $this->fields[$name];
    }
    public function offsetSet($name, $value)
    {
        if (!$this->has($name)) {
            throw new \InvalidArgumentException(sprintf('The form field "%s" does not exist', $name));
        }
        $this->fields[$name]->setValue($value);
    }
    public function offsetUnset($name)
    {
        $this->remove($name);
    }
    protected function setNode(\DOMNode $node)
    {
        $this->button = $node;
        if ('button' == $node->nodeName || ('input' == $node->nodeName && in_array($node->getAttribute('type'), array('submit', 'button', 'image')))) {
            do {
                                if (null === $node = $node->parentNode) {
                    throw new \LogicException('The selected node does not have a form ancestor.');
                }
            } while ('form' != $node->nodeName);
        } elseif('form' != $node->nodeName) {
            throw new \LogicException(sprintf('Unable to submit on a "%s" tag.', $node->nodeName));
        }
        $this->node = $node;
    }
}
