<?php
namespace Symfony\Component\HttpKernel\CacheWarmer;
interface CacheWarmerInterface
{
    function warmUp($cacheDir);
    function isOptional();
}
