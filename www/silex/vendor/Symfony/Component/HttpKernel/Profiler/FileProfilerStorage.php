<?php
namespace Symfony\Component\HttpKernel\Profiler;
class FileProfilerStorage implements ProfilerStorageInterface
{
    private $folder;
    public function __construct($dsn)
    {
        if (0 !== strpos($dsn, 'file:')) {
            throw new \InvalidArgumentException("FileStorage DSN must start with file:");
        }
        $this->folder = substr($dsn, 5);
        if (!is_dir($this->folder)) {
            mkdir($this->folder);
        }
    }
    public function find($ip, $url, $limit)
    {
        $file = $this->getIndexFilename();
        if (!file_exists($file)) {
            return array();
        }
        $file = fopen($file, 'r');
        fseek($file, 0, SEEK_END);
        $result = array();
        while ($limit > 0) {
            $line = $this->readLineFromFile($file);
            if (false === $line) {
                break;
            }
            if ($line === "") {
                continue;
            }
            list($csvToken, $csvIp, $csvUrl, $csvTime, $csvParent) = str_getcsv($line);
            if ($ip && false === strpos($csvIp, $ip) || $url && false === strpos($csvUrl, $url)) {
                continue;
            }
            $row = array(
                'token'  => $csvToken,
                'ip'     => $csvIp,
                'url'    => $csvUrl,
                'time'   => $csvTime,
                'parent' => $csvParent
            );
            $result[] = $row;
            $limit--;
        }
        fclose($file);
        return $result;
    }
    public function purge()
    {
        $flags = \FilesystemIterator::SKIP_DOTS;
        $iterator = new \RecursiveDirectoryIterator($this->folder, $flags);
        $iterator = new \RecursiveIteratorIterator($iterator, \RecursiveIteratorIterator::CHILD_FIRST);
        foreach ($iterator as $file) {
            if (is_file($file)) {
                unlink($file);
            } else {
                rmdir($file);
            }
        }
    }
    public function read($token)
    {
        $file = $this->getFilename($token);
        if (!file_exists($file)) {
            return null;
        }
        return unserialize(file_get_contents($file));
    }
    public function write(Profile $profile)
    {
        $file = $this->getFilename($profile->getToken());
        $exists = file_exists($file);
                $dir = dirname($file);
        if (!is_dir($dir)) {
            mkdir($dir, 0777, true);
        }
                file_put_contents($file, serialize($profile));
                $file = fopen($this->getIndexFilename(), 'a');
        fputcsv($file, array(
            $profile->getToken(),
            $profile->getIp(),
            $profile->getUrl(),
            $profile->getTime(),
            $profile->getParent() ? $profile->getParent()->getToken() : null
        ));
        fclose($file);
        return ! $exists;
    }
    protected function getFilename($token)
    {
                $folderA = substr($token, -2, 2);
        $folderB = substr($token, -4, 2);
        return $this->folder.'/'.$folderA.'/'.$folderB.'/'.$token;
    }
    protected function getIndexFilename()
    {
        return $this->folder.'/'.'index.csv';
    }
    protected function readLineFromFile($file)
    {
        if (ftell($file) === 0) {
            return false;
        }
        fseek($file, -1, SEEK_CUR);
        $str = '';
        while (true) {
            $char = fgetc($file);
            if ($char === "\n") {
                                fseek($file, -1, SEEK_CUR);
                break;
            }
            $str = $char . $str;
            if (ftell($file) === 1) {
                                fseek($file, -1, SEEK_CUR);
                break;
            }
            fseek($file, -2, SEEK_CUR);
        }
        return $str === "" ? $this->readLineFromFile($file) : $str;
    }
}
