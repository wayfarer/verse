<?php
namespace Symfony\Component\HttpKernel\Profiler;
use Symfony\Component\HttpKernel\DataCollector\DataCollectorInterface;
class Profile implements \Serializable
{
    private $token;
    private $collectors;
    private $ip;
    private $url;
    private $time;
    private $parent;
    private $children;
    public function __construct($token)
    {
        $this->token = $token;
        $this->collectors = array();
    }
    public function setToken($token)
    {
        $this->token = $token;
    }
    public function getToken()
    {
        return $this->token;
    }
    public function setParent(Profile $parent)
    {
        $this->parent = $parent;
    }
    public function getParent()
    {
        return $this->parent;
    }
    public function getIp()
    {
        return $this->ip;
    }
    public function setIp($ip)
    {
        $this->ip = $ip;
    }
    public function getUrl()
    {
        return $this->url;
    }
    public function setUrl($url)
    {
        $this->url = $url;
    }
    public function getTime()
    {
        return $this->time;
    }
    public function setTime($time)
    {
        $this->time = $time;
    }
    public function getChildren()
    {
        return $this->children;
    }
    public function setChildren(array $children)
    {
        $this->children = array();
        foreach ($children as $child) {
            $this->addChild($child);
        }
    }
    public function addChild(Profile $child)
    {
        $this->children[] = $child;
    }
    public function getCollector($name)
    {
        if (!isset($this->collectors[$name])) {
            throw new \InvalidArgumentException(sprintf('Collector "%s" does not exist.', $name));
        }
        return $this->collectors[$name];
    }
    public function getCollectors()
    {
        return $this->collectors;
    }
    public function setCollectors(array $collectors)
    {
        $this->collectors = array();
        foreach ($collectors as $collector) {
            $this->addCollector($collector);
        }
    }
    public function addCollector(DataCollectorInterface $collector)
    {
        $this->collectors[$collector->getName()] = $collector;
    }
    public function hasCollector($name)
    {
        return isset($this->collectors[$name]);
    }
    public function serialize()
    {
        return serialize(array($this->token, $this->parent, $this->children, $this->collectors, $this->ip, $this->url, $this->time));
    }
    public function unserialize($data)
    {
        list($this->token, $this->parent, $this->children, $this->collectors, $this->ip, $this->url, $this->time) = unserialize($data);
    }
}
