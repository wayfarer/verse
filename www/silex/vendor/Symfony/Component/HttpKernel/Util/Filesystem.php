<?php
namespace Symfony\Component\HttpKernel\Util;
class Filesystem
{
    public function copy($originFile, $targetFile, $override = false)
    {
        $this->mkdir(dirname($targetFile));
        if (!$override && is_file($targetFile)) {
            $doCopy = filemtime($originFile) > filemtime($targetFile);
        } else {
            $doCopy = true;
        }
        if ($doCopy) {
            copy($originFile, $targetFile);
        }
    }
    public function mkdir($dirs, $mode = 0777)
    {
        $ret = true;
        foreach ($this->toIterator($dirs) as $dir) {
            if (is_dir($dir)) {
                continue;
            }
            $ret = @mkdir($dir, $mode, true) && $ret;
        }
        return $ret;
    }
    public function touch($files)
    {
        foreach ($this->toIterator($files) as $file) {
            touch($file);
        }
    }
    public function remove($files)
    {
        $files = iterator_to_array($this->toIterator($files));
        $files = array_reverse($files);
        foreach ($files as $file) {
            if (!file_exists($file)) {
                continue;
            }
            if (is_dir($file) && !is_link($file)) {
                $this->remove(new \FilesystemIterator($file));
                rmdir($file);
            } else {
                unlink($file);
            }
        }
    }
    public function chmod($files, $mode, $umask = 0000)
    {
        $currentUmask = umask();
        umask($umask);
        foreach ($this->toIterator($files) as $file) {
            chmod($file, $mode);
        }
        umask($currentUmask);
    }
    public function rename($origin, $target)
    {
                if (is_readable($target)) {
            throw new \RuntimeException(sprintf('Cannot rename because the target "%" already exist.', $target));
        }
        rename($origin, $target);
    }
    public function symlink($originDir, $targetDir, $copyOnWindows = false)
    {
        if (!function_exists('symlink') && $copyOnWindows) {
            $this->mirror($originDir, $targetDir);
            return;
        }
        $ok = false;
        if (is_link($targetDir)) {
            if (readlink($targetDir) != $originDir) {
                unlink($targetDir);
            } else {
                $ok = true;
            }
        }
        if (!$ok) {
            symlink($originDir, $targetDir);
        }
    }
    public function makePathRelative($endPath, $startPath)
    {
                $offset = 0;
        while ($startPath[$offset] === $endPath[$offset]) {
            $offset++;
        }
                $depth = substr_count(substr($startPath, $offset), DIRECTORY_SEPARATOR) + 1;
                $traverser = str_repeat('../', $depth);
                return $traverser.substr($endPath, $offset);
    }
    public function mirror($originDir, $targetDir, \Traversable $iterator = null, $options = array())
    {
        $copyOnWindows = false;
        if (isset($options['copy_on_windows']) && !function_exists('symlink')) {
            $copyOnWindows = $options['copy_on_windows'];
        }
        if (null === $iterator) {
            $flags = $copyOnWindows ? \FilesystemIterator::SKIP_DOTS | \FilesystemIterator::FOLLOW_SYMLINKS : \FilesystemIterator::SKIP_DOTS;
            $iterator = new \RecursiveIteratorIterator(new \RecursiveDirectoryIterator($originDir, $flags), \RecursiveIteratorIterator::SELF_FIRST);
        }
        if ('/' === substr($targetDir, -1) || '\\' === substr($targetDir, -1)) {
            $targetDir = substr($targetDir, 0, -1);
        }
        if ('/' === substr($originDir, -1) || '\\' === substr($originDir, -1)) {
            $originDir = substr($originDir, 0, -1);
        }
        foreach ($iterator as $file) {
            $target = $targetDir.'/'.str_replace($originDir.DIRECTORY_SEPARATOR, '', $file->getPathname());
            if (is_dir($file)) {
                $this->mkdir($target);
            } else if (is_file($file) || ($copyOnWindows && is_link($file))) {
                $this->copy($file, $target, isset($options['override']) ? $options['override'] : false);
            } else if (is_link($file)) {
                $this->symlink($file, $target);
            } else {
                throw new \RuntimeException(sprintf('Unable to guess "%s" file type.', $file));
            }
        }
    }
    public function isAbsolutePath($file)
    {
        if ($file[0] == '/' || $file[0] == '\\'
            || (strlen($file) > 3 && ctype_alpha($file[0])
                && $file[1] == ':'
                && ($file[2] == '\\' || $file[2] == '/')
            )
        ) {
            return true;
        }
        return false;
    }
    private function toIterator($files)
    {
        if (!$files instanceof \Traversable) {
            $files = new \ArrayObject(is_array($files) ? $files : array($files));
        }
        return $files;
    }
}
