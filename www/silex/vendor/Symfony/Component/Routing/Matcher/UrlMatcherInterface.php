<?php
namespace Symfony\Component\Routing\Matcher;
use Symfony\Component\Routing\RequestContextAwareInterface;
interface UrlMatcherInterface extends RequestContextAwareInterface
{
    function match($pathinfo);
}
