<?php
namespace Symfony\Component\Routing\Matcher;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
abstract class RedirectableUrlMatcher extends UrlMatcher implements RedirectableUrlMatcherInterface
{
    private $trailingSlashTest = false;
    public function match($pathinfo)
    {
        try {
            $parameters = parent::match($pathinfo);
        } catch (ResourceNotFoundException $e) {
            if ('/' === substr($pathinfo, -1)) {
                throw $e;
            }
                        $this->trailingSlashTest = true;
            return $this->match($pathinfo.'/');
        }
        if ($this->trailingSlashTest) {
            $this->trailingSlashTest = false;
            return $this->redirect($pathinfo, null);
        }
        return $parameters;
    }
}
