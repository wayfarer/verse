<?php
namespace Symfony\Component\Routing\Generator;
use Symfony\Component\Routing\RequestContextAwareInterface;
interface UrlGeneratorInterface extends RequestContextAwareInterface
{
    function generate($name, $parameters = array(), $absolute = false);
}
