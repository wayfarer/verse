<?php
namespace Symfony\Component\HttpFoundation;
use Symfony\Component\HttpFoundation\SessionStorage\SessionStorageInterface;
class Session implements \Serializable
{
    protected $storage;
    protected $started;
    protected $attributes;
    protected $flashes;
    protected $oldFlashes;
    protected $closed;
    public function __construct(SessionStorageInterface $storage)
    {
        $this->storage = $storage;
        $this->flashes = array();
        $this->oldFlashes = array();
        $this->attributes = array();
        $this->started = false;
        $this->closed = false;
    }
    public function start()
    {
        if (true === $this->started) {
            return;
        }
        $this->storage->start();
        $attributes = $this->storage->read('_symfony2');
        if (isset($attributes['attributes'])) {
            $this->attributes = $attributes['attributes'];
            $this->flashes = $attributes['flashes'];
                        $this->oldFlashes = $this->flashes;
        }
        $this->started = true;
    }
    public function has($name)
    {
        return array_key_exists($name, $this->attributes);
    }
    public function get($name, $default = null)
    {
        return array_key_exists($name, $this->attributes) ? $this->attributes[$name] : $default;
    }
    public function set($name, $value)
    {
        if (false === $this->started) {
            $this->start();
        }
        $this->attributes[$name] = $value;
    }
    public function all()
    {
        return $this->attributes;
    }
    public function replace(array $attributes)
    {
        if (false === $this->started) {
            $this->start();
        }
        $this->attributes = $attributes;
    }
    public function remove($name)
    {
        if (false === $this->started) {
            $this->start();
        }
        if (array_key_exists($name, $this->attributes)) {
            unset($this->attributes[$name]);
        }
    }
    public function clear()
    {
        if (false === $this->started) {
            $this->start();
        }
        $this->attributes = array();
        $this->flashes = array();
    }
    public function invalidate()
    {
        $this->clear();
        $this->storage->regenerate(true);
    }
    public function migrate()
    {
        $this->storage->regenerate();
    }
    public function getId()
    {
        if (false === $this->started) {
            $this->start();
        }
        return $this->storage->getId();
    }
    public function getFlashes()
    {
        return $this->flashes;
    }
    public function setFlashes($values)
    {
        if (false === $this->started) {
            $this->start();
        }
        $this->flashes = $values;
        $this->oldFlashes = array();
    }
    public function getFlash($name, $default = null)
    {
        return array_key_exists($name, $this->flashes) ? $this->flashes[$name] : $default;
    }
    public function setFlash($name, $value)
    {
        if (false === $this->started) {
            $this->start();
        }
        $this->flashes[$name] = $value;
        unset($this->oldFlashes[$name]);
    }
    public function hasFlash($name)
    {
        if (false === $this->started) {
            $this->start();
        }
        return array_key_exists($name, $this->flashes);
    }
    public function removeFlash($name)
    {
        if (false === $this->started) {
            $this->start();
        }
        unset($this->flashes[$name]);
    }
    public function clearFlashes()
    {
        if (false === $this->started) {
            $this->start();
        }
        $this->flashes = array();
        $this->oldFlashes = array();
    }
    public function save()
    {
        if (false === $this->started) {
            $this->start();
        }
        $this->flashes = array_diff_key($this->flashes, $this->oldFlashes);
        $this->storage->write('_symfony2', array(
            'attributes' => $this->attributes,
            'flashes'    => $this->flashes,
        ));
    }
    public function close()
    {
        $this->closed = true;
    }
    public function __destruct()
    {
        echo "!!!";
        exit;
        if (true === $this->started && !$this->closed) {
            $this->save();
        }
    }
    public function serialize()
    {
        return serialize($this->storage);
    }
    public function unserialize($serialized)
    {
        $this->storage = unserialize($serialized);
        $this->attributes = array();
        $this->started = false;
    }
}
