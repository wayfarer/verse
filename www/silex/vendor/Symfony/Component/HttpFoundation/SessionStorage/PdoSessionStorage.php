<?php
namespace Symfony\Component\HttpFoundation\SessionStorage;
class PdoSessionStorage extends NativeSessionStorage
{
    private $db;
    private $dbOptions;
    public function __construct(\PDO $db, array $options = array(), array $dbOptions = array())
    {
        if (!array_key_exists('db_table', $dbOptions)) {
            throw new \InvalidArgumentException('You must provide the "db_table" option for a PdoSessionStorage.');
        }
        $this->db = $db;
        $this->dbOptions = array_merge(array(
            'db_id_col'   => 'sess_id',
            'db_data_col' => 'sess_data',
            'db_time_col' => 'sess_time',
        ), $dbOptions);
        parent::__construct($options);
    }
    public function start()
    {
        if (self::$sessionStarted) {
            return;
        }
                session_set_save_handler(
            array($this, 'sessionOpen'),
            array($this, 'sessionClose'),
            array($this, 'sessionRead'),
            array($this, 'sessionWrite'),
            array($this, 'sessionDestroy'),
            array($this, 'sessionGC')
        );
        parent::start();
    }
    public function sessionOpen($path = null, $name = null)
    {
        return true;
    }
    public function sessionClose()
    {
                return true;
    }
    public function sessionDestroy($id)
    {
                $dbTable  = $this->dbOptions['db_table'];
        $dbIdCol = $this->dbOptions['db_id_col'];
                $sql = "DELETE FROM $dbTable WHERE $dbIdCol = :id";
        try {
            $stmt = $this->db->prepare($sql);
            $stmt->bindParam(':id', $id, \PDO::PARAM_STR);
            $stmt->execute();
        } catch (\PDOException $e) {
            throw new \RuntimeException(sprintf('PDOException was thrown when trying to manipulate session data: %s', $e->getMessage()), 0, $e);
        }
        return true;
    }
    public function sessionGC($lifetime)
    {
                $dbTable    = $this->dbOptions['db_table'];
        $dbTimeCol = $this->dbOptions['db_time_col'];
                $sql = "DELETE FROM $dbTable WHERE $dbTimeCol < (:time - $lifetime)";
        try {
            $this->db->query($sql);
            $stmt = $this->db->prepare($sql);
            $stmt->bindValue(':time', time(), \PDO::PARAM_INT);
            $stmt->execute();
        } catch (\PDOException $e) {
            throw new \RuntimeException(sprintf('PDOException was thrown when trying to manipulate session data: %s', $e->getMessage()), 0, $e);
        }
        return true;
    }
    public function sessionRead($id)
    {
                $dbTable    = $this->dbOptions['db_table'];
        $dbDataCol = $this->dbOptions['db_data_col'];
        $dbIdCol   = $this->dbOptions['db_id_col'];
        try {
            $sql = "SELECT $dbDataCol FROM $dbTable WHERE $dbIdCol = :id";
            $stmt = $this->db->prepare($sql);
            $stmt->bindParam(':id', $id, \PDO::PARAM_STR, 255);
            $stmt->execute();
                                    $sessionRows = $stmt->fetchAll(\PDO::FETCH_NUM);
            if (count($sessionRows) == 1) {
                return $sessionRows[0][0];
            }
                        $this->createNewSession($id);
            return '';
        } catch (\PDOException $e) {
            throw new \RuntimeException(sprintf('PDOException was thrown when trying to manipulate session data: %s', $e->getMessage()), 0, $e);
        }
    }
    public function sessionWrite($id, $data)
    {
                $dbTable   = $this->dbOptions['db_table'];
        $dbDataCol = $this->dbOptions['db_data_col'];
        $dbIdCol   = $this->dbOptions['db_id_col'];
        $dbTimeCol = $this->dbOptions['db_time_col'];
        $sql = ('mysql' === $this->db->getAttribute(\PDO::ATTR_DRIVER_NAME))
            ? "INSERT INTO $dbTable ($dbIdCol, $dbDataCol, $dbTimeCol) VALUES (:id, :data, :time) "
              ."ON DUPLICATE KEY UPDATE $dbDataCol = VALUES($dbDataCol), $dbTimeCol = CASE WHEN $dbTimeCol = :time THEN (VALUES($dbTimeCol) + 1) ELSE VALUES($dbTimeCol) END"
            : "UPDATE $dbTable SET $dbDataCol = :data, $dbTimeCol = :time WHERE $dbIdCol = :id";
        try {
            $stmt = $this->db->prepare($sql);
            $stmt->bindParam(':id', $id, \PDO::PARAM_STR);
            $stmt->bindParam(':data', $data, \PDO::PARAM_STR);
            $stmt->bindValue(':time', time(), \PDO::PARAM_INT);
            $stmt->execute();
            if (!$stmt->rowCount()) {
                                                $this->createNewSession($id, $data);
            }
        } catch (\PDOException $e) {
            throw new \RuntimeException(sprintf('PDOException was thrown when trying to manipulate session data: %s', $e->getMessage()), 0, $e);
        }
        return true;
    }
    private function createNewSession($id, $data = '')
    {
                $dbTable    = $this->dbOptions['db_table'];
        $dbDataCol = $this->dbOptions['db_data_col'];
        $dbIdCol   = $this->dbOptions['db_id_col'];
        $dbTimeCol = $this->dbOptions['db_time_col'];
        $sql = "INSERT INTO $dbTable ($dbIdCol, $dbDataCol, $dbTimeCol) VALUES (:id, :data, :time)";
        $stmt = $this->db->prepare($sql);
        $stmt->bindParam(':id', $id, \PDO::PARAM_STR);
        $stmt->bindParam(':data', $data, \PDO::PARAM_STR);
        $stmt->bindValue(':time', time(), \PDO::PARAM_INT);
        $stmt->execute();
        return true;
    }
}
