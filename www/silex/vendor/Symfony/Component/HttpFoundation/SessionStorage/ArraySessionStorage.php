<?php
namespace Symfony\Component\HttpFoundation\SessionStorage;
class ArraySessionStorage implements SessionStorageInterface
{
    private $data = array();
    public function read($key, $default = null)
    {
        return array_key_exists($key, $this->data) ? $this->data[$key] : $default;
    }
    public function regenerate($destroy = false)
    {
        if ($destroy) {
            $this->data = array();
        }
        return true;
    }
    public function remove($key)
    {
        unset($this->data[$key]);
    }
    public function start()
    {
    }
    public function getId()
    {
    }
    public function write($key, $data)
    {
        $this->data[$key] = $data;
    }
}
