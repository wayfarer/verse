<?php
namespace Symfony\Component\HttpFoundation\SessionStorage;
interface SessionStorageInterface
{
    function start();
    function getId();
    function read($key);
    function remove($key);
    function write($key, $data);
    function regenerate($destroy = false);
}
