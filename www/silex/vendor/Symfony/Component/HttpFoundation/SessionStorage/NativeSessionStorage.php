<?php
namespace Symfony\Component\HttpFoundation\SessionStorage;
class NativeSessionStorage implements SessionStorageInterface
{
    static protected $sessionIdRegenerated = false;
    static protected $sessionStarted       = false;
    protected $options;
    public function __construct(array $options = array())
    {
        $cookieDefaults = session_get_cookie_params();
        $this->options = array_merge(array(
            'lifetime' => $cookieDefaults['lifetime'],
            'path'     => $cookieDefaults['path'],
            'domain'   => $cookieDefaults['domain'],
            'secure'   => $cookieDefaults['secure'],
            'httponly' => isset($cookieDefaults['httponly']) ? $cookieDefaults['httponly'] : false,
        ), $options);
                if (isset($this->options['name'])) {
            session_name($this->options['name']);
        }
    }
    public function start()
    {
        if (self::$sessionStarted) {
            return;
        }
        session_set_cookie_params(
            $this->options['lifetime'],
            $this->options['path'],
            $this->options['domain'],
            $this->options['secure'],
            $this->options['httponly']
        );
                session_cache_limiter(false);
        if (!ini_get('session.use_cookies') && isset($this->options['id']) && $this->options['id'] && $this->options['id'] != session_id()) {
            session_id($this->options['id']);
        }
        session_start();
        self::$sessionStarted = true;
    }
    public function getId()
    {
        if (!self::$sessionStarted) {
            throw new \RuntimeException('The session must be started before reading its ID');
        }
        return session_id();
    }
    public function read($key, $default = null)
    {
        return array_key_exists($key, $_SESSION) ? $_SESSION[$key] : $default;
    }
    public function remove($key)
    {
        $retval = null;
        if (isset($_SESSION[$key])) {
            $retval = $_SESSION[$key];
            unset($_SESSION[$key]);
        }
        return $retval;
    }
    public function write($key, $data)
    {
        $_SESSION[$key] = $data;
    }
    public function regenerate($destroy = false)
    {
        if (self::$sessionIdRegenerated) {
            return;
        }
        session_regenerate_id($destroy);
        self::$sessionIdRegenerated = true;
    }
}
