<?php
namespace Symfony\Component\BrowserKit;
class CookieJar
{
    protected $cookieJar = array();
    public function set(Cookie $cookie)
    {
        $this->cookieJar[$cookie->getName()] = $cookie;
    }
    public function get($name)
    {
        $this->flushExpiredCookies();
        return isset($this->cookieJar[$name]) ? $this->cookieJar[$name] : null;
    }
    public function expire($name)
    {
        unset($this->cookieJar[$name]);
    }
    public function clear()
    {
        $this->cookieJar = array();
    }
    public function updateFromResponse(Response $response, $uri = null)
    {
        foreach ($response->getHeader('Set-Cookie', false) as $cookie) {
            $this->set(Cookie::fromString($cookie, $uri));
        }
    }
    public function all()
    {
        $this->flushExpiredCookies();
        return $this->cookieJar;
    }
    public function allValues($uri, $returnsRawValue = false)
    {
        $this->flushExpiredCookies();
        $parts = array_replace(array('path' => '/'), parse_url($uri));
        $cookies = array();
        foreach ($this->cookieJar as $cookie) {
            if ($cookie->getDomain()) {
                $domain = ltrim($cookie->getDomain(), '.');
                if ($domain != substr($parts['host'], -strlen($domain))) {
                    continue;
                }
            }
            if ($cookie->getPath() != substr($parts['path'], 0, strlen($cookie->getPath()))) {
                continue;
            }
            if ($cookie->isSecure() && 'https' != $parts['scheme']) {
                continue;
            }
            $cookies[$cookie->getName()] = $returnsRawValue ? $cookie->getRawValue() : $cookie->getValue();
        }
        return $cookies;
    }
    public function allRawValues($uri)
    {
        return $this->allValues($uri, true);
    }
    public function flushExpiredCookies()
    {
        $cookies = $this->cookieJar;
        foreach ($cookies as $name => $cookie) {
            if ($cookie->isExpired()) {
                unset($this->cookieJar[$name]);
            }
        }
    }
}
