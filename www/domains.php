<?php
 // obsolete
// domains management
include("inc/verse.inc.php"); //main header - initializes Verse environment
include("inc/ldap.inc.php");

if ($user->logged()) {
    //  $_POST = utf8_to_latin($_POST);
    // pass only global admin to domain list
    if ($user->data["domain_id"] == 0) {
        $action = @$_POST["action"];
        switch ($action) {
            case "list":
            case "list_uc":
            case "list_hidden":
                $rep = createobject("report_ajax", array($db, "sms_domain", REP_WITH_PAGENATOR, true, 100, 'width="900px" style="background-color: #DDEEFF"'));
                $rep->add_table("sms_domain", REP_LEFT_JOIN, array("alias_domain_id", "domain_id"));

                $rep->add_field("id", REP_STRING_TEMPLATE, '{domain_id|1}', REP_ORDERABLE, 'align="center"');
                $rep->add_field("name", REP_STRING_TEMPLATE, href("{domain_name|1}", "http://{domain_name|1}", 'target="_blank"'), REP_ORDERABLE, 'align="center"');
                $rep->add_field("", REP_FUNCTIONCALL_TEMPLATE, "edit({domain_name|1},{alias_domain_id|1})", REP_UNORDERABLE, 'align="center" width="5%"');
                $rep->add_field("", REP_STRING_TEMPLATE, href_js_action("properties", "new DomainEditor({domain_id|1})"), REP_UNORDERABLE, 'align="center" width="10%"');
                switch ($action) {
                    case "list":
                        $rep->add_field("", REP_FUNCTIONCALL_TEMPLATE, "products({domain_name|1},{alias_domain_id|1})", REP_UNORDERABLE, 'align="center" width="5%"');
                        $rep->add_field("", REP_FUNCTIONCALL_TEMPLATE, "obits({domain_name|1},{alias_domain_id|1})", REP_UNORDERABLE, 'align="center" width="5%"');
                        $rep->add_field("", REP_STRING_TEMPLATE, "<small>" . href("users", "http://{domain_name|1}/user_manager.php?z=" . session_id()) . "</small>", REP_UNORDERABLE, 'align="center" width="5%"');
                        $rep->add_field("", REP_STRING_TEMPLATE, "<small>" . href("emails", "http://{domain_name|1}/emails.php?z=" . session_id()) . "</small>", REP_UNORDERABLE, 'align="center" width="5%"');
                        $rep->add_field("", REP_STRING_TEMPLATE, href_js_action("to UC", "to_uc_domain({domain_id|1})"), REP_UNORDERABLE, 'align="center"');
                        // show only domains in production
                        $rep->add_filter("", REP_INVISIBLE_FILTER, "t1.mode", "=", 0);
                        break;
                    case "list_uc":
                        $rep->add_field("", REP_STRING_TEMPLATE, href_js_action("to production", "to_production_domain({domain_id|1})"), REP_UNORDERABLE, 'align="center"');
                        $rep->add_field("", REP_STRING_TEMPLATE, href_js_action("remove", "hide_domain({domain_id|1})"), REP_UNORDERABLE, 'align="center"');
                        // show under construction domains
                        $rep->add_filter("", REP_INVISIBLE_FILTER, "t1.mode", "=", 1);
                        break;
                    case "list_hidden":
                        $rep->add_field("", REP_STRING_TEMPLATE, href_js_action("restore", "unhide_domain({domain_id|1})"), REP_UNORDERABLE, 'align="center"');
                        //					$rep->add_field("", REP_STRING_TEMPLATE, href_js_action("delete", "delete_domain({domain_id})"), REP_UNORDERABLE, 'align="center"');
                        $rep->add_filter("", REP_INVISIBLE_FILTER, "t1.mode", "=", 2);
                        break;
                }
                $rep->add_field("parent", REP_STRING_TEMPLATE, href("{domain_name|2}", "http://{domain_name|2}", 'target="_blank"'), REP_ORDERABLE, 'align="center"');

                $rep->add_field("space", REP_CALLBACK, "space_consumed({space_consumed|1})", REP_ORDERABLE, 'align="center" width="5%"');
                $rep->add_field("created", REP_CALLBACK, "created_at({created_at|1})", REP_ORDERABLE, 'align="center" width="5%"');

                $rep->html_attributes('width="900px"');

                if (count($_POST) < 3) {
                    $rep->order_by("domain_name|1", 0);
                }

                $rep->handle_events($_POST);
                $html = $rep->make_report();
                //		    echo $rep->query;

                echo $html;
                break;
            case "load":
                $domain_id = intval(@$_POST["id"]);
                $query = "SELECT domain_name, email, enabled, postfix, email_enabled, ecommerce_enabled, paypal_checkout, alias_domain_id FROM sms_domain WHERE domain_id='$domain_id'";
                $ret = $db->getRow($query, DB_FETCHMODE_ASSOC);
                if ($ret["alias_domain_id"]) {
                    $ret["type"] = 1;
                }
                header("X-JSON:" . make_json_response($ret));
                break;
            case "save":
                $domain_id = intval(@$_POST["id"]);
                $p = array();
                $p["enabled"] = (@$_POST["enabled"] ? 1 : 0);
                $p["email"] = in(@$_POST["email"]);
                $p["domain_name"] = in(@$_POST["domain_name"]);
                $p["postfix"] = in(@$_POST["postfix"]);
                $p["email_enabled"] = (@$_POST["email_enabled"] ? 1 : 0);
                $p["ecommerce_enabled"] = (@$_POST["ecommerce_enabled"] ? 1 : 0);
                $p["paypal_checkout"] = (@$_POST["paypal_checkout"] ? 1 : 0);
                $type = intval(@$_POST["type"]);
                if ($type == 1) {
                    $p["alias_domain_id"] = intval(@$_POST["alias_domain_id"]);
                }
                else {
                    $p["alias_domain_id"] = 0;
                }
                if ($domain_id) {
                    // fetch old domain name and type for renaming
                    $query = "SELECT domain_name, alias_domain_id FROM sms_domain WHERE domain_id='$domain_id'";
                    $old = $db->getRow($query, DB_FETCHMODE_ASSOC);

                    if ($p["domain_name"] != $old["domain_name"]) {
                        // check if new domain name is unique
                        $query = "SELECT 1 FROM sms_domain WHERE domain_name='" . $p["domain_name"] . "'";
                        $invalid_domain_name = $db->getOne($query);
                    }
                    else {
                        $invalid_domain_name = false;
                    }
                    if (!$invalid_domain_name) {
                        if (($old["alias_domain_id"] != 0) == ($p["alias_domain_id"] != 0)) {
                            // rename directory or symlink if type not changed
                            if ($old["domain_name"] != $p["domain_name"]) {
                                rename("files/" . $old["domain_name"], "files/" . $p["domain_name"]);
                                update_ftp_pathes($domain_id, $p["domain_name"]);
                                // rename domain at LDAP
                                manage_ldap_email_domain($p["domain_name"], $p["email_enabled"], $old["domain_name"]);
                                // manage piwik logins (cause they includes domain name)
                                include_once('inc/piwik_adapter.inc.php');
                                piwik_rename_domain($domain_id, $old["domain_name"], $p["domain_name"]);
                            }
                        }
                        else {
                            if ($p["alias_domain_id"]) { // new name is alias and changed from normal domain
                                // remove original dir if empty and create symlink
                                if (!file_exists("files/" . $old["domain_name"]) || rmdir("files/" . $old["domain_name"])) {
                                    // get alias domain name
                                    $query = "SELECT domain_name FROM sms_domain WHERE domain_id='" . $p["alias_domain_id"] . "'";
                                    $alias_domain_name = $db->getOne($query);
                                    symlink("files/" . $alias_domain_name, "files/" . $p["domain_name"]);
                                    // remove cms_site record
                                    $query = "DELETE FROM cms_site WHERE domain_id='$domain_id'";
                                    $db->query($query);
                                }
                            }
                            else { // in case of switching from alias to normal we need to remove symlink and create directory
                                unlink("files/" . $old["domain_name"]);
                                create_dirs($p["domain_name"]);

                                $query = "INSERT cms_site SET domain_id='$domain_id'";
                                $db->query($query);
                            }
                        }
                        $query = "UPDATE sms_domain SET " . make_set_clause($p) . " WHERE domain_id='$domain_id'";
                        $ret = $db->query($query);
                        // enables/disable mail services
                        $ret = manage_ldap_email_domain($p["domain_name"], $p["email_enabled"]);
                        //var_dump($ret);
                    }
                    else {
                        // TODO: report error here
                        //					$query = "UPDATE sms_domain SET email='$email', enabled='$enabled', postfix='$postfix', email_enabled='$email_enabled', ecommerce_enabled='$ecommerce_enabled', alias_domain_id='$alias_domain_id' WHERE domain_id='$domain_id'";
                        //					$db->query($query);
                    }
                }
                else {
                    $p["mode"] = 1;
                    $query = "INSERT sms_domain SET " . make_set_clause($p) . ", created_at=now()";
                    $db->query($query);
                    //$logger->log($query);
                    $query = "SELECT last_insert_id()";
                    $domain_id = $db->getOne($query);
                    if ($type == 0) { // normal domain
                        $query = "INSERT cms_site SET domain_id='$domain_id'";
                        $db->query($query);
                        create_dirs($p["domain_name"]);
                        // enable captcha by default
                        $query = "INSERT plg_obituary_config SET domain_id='$domain_id', param='captcha', value='1'";
                        $db->query($query);
                    }
                    else { // alias domain
                        // get alias domain name
                        $query = "SELECT domain_name FROM sms_domain WHERE domain_id='" . $p["alias_domain_id"] . "'";
                        $alias_domain_name = $db->getOne($query);
                        symlink("files/" . $alias_domain_name, "files/" . $p["domain_name"]);
                    }
                }
                break;
            case "delete":
                //$domain_id = intval(@$_POST["id"]);
                //$query = "DELETE FROM sms_domain WHERE domain_id='$domain_id'";
                //$db->query($query);
                break;
            case "hide":
                $domain_id = intval(@$_POST["id"]);
                $query = "UPDATE sms_domain SET enabled=0, mode=2 WHERE domain_id='$domain_id'";
                $db->query($query);
                break;
            case "unhide":
            case "touc":
                $domain_id = intval(@$_POST["id"]);
                $query = "UPDATE sms_domain SET mode=1 WHERE domain_id='$domain_id'";
                $db->query($query);
                break;
            case "toproduction":
                $domain_id = intval(@$_POST["id"]);
                $query = "UPDATE sms_domain SET enabled=1, mode=0 WHERE domain_id='$domain_id'";
                $db->query($query);

                $query = "SELECT alias_domain_id FROM sms_domain WHERE domain_id='$domain_id'";
                $alias_domain_id = $db->getOne($query);

                if (!$alias_domain_id) {
                    include_once('inc/piwik_adapter.inc.php');
                    piwik_ensure_domain($domain_id);

                    //phpmv logic (create or update phpmv domain related to fsms's one)
                    // include_once('inc/phpmv_adapter.inc.php');
                    // phpmv_domain_set($domain_id);
                }
                break;
            default:
                $mode = intval(@$_GET["mode"]);
                switch ($mode) {
                    case 1:
                        $action = "list_uc";
                        break;
                    case 2:
                        $action = "list_hidden";
                        break;
                    default:
                        $action = "list";
                }
                $query = "SELECT domain_id, domain_name FROM sms_domain WHERE mode<2 AND alias_domain_id=0 ORDER BY domain_name";
                $domains = $db->getAssoc($query);

                $smarty->assign("domains", $domains);
                $smarty->assign("action", $action);

                // sites space last calculated
                $query = "SELECT value FROM cms_config WHERE domain_id='0' AND param='sites_space_consumed_last_calc'";
                $sites_space_consumed_last_calculated = $db->getOne($query);
                $smarty->assign("sites_space_consumed_last_calculated", $sites_space_consumed_last_calculated);

                $smarty->display("domains.tpl");
        }
    }
    else {
        // point user to his domain structure
        header("Location: structure.php");
    }
}
else {
    header("Location: login.php");
}

function edit($domain_name, $type) {
    if (!$type) {
        return href("edit", "http://$domain_name/structure.php?z=" . session_id());
    }
    else return "";
}

function products($domain_name, $type) {
    if (!$type) {
        return href("products", "http://$domain_name/products.php?z=" . session_id());
    }
    else return "";
}

function obits($domain_name, $type) {
    if (!$type) {
        return href("obits", "http://$domain_name/obituaries.php?z=" . session_id());
    }
    else return "";
}

function create_dirs($name) {
    if (mkdir("files/$name")) {
        mkdir("files/$name/exchange");
        mkdir("files/$name/file");
        mkdir("files/$name/image");
        mkdir("files/$name/movies");
        mkdir("files/$name/image/merchandise");
        mkdir("files/$name/image/obituaries");
        mkdir("files/$name/image/obituaries/scrap");
    }
}

function update_ftp_pathes($domain_id, $domain_name) {
    global $db, $logger;

    $logger->log("Updating FTPs for domain $domain_name=$domain_id");
    // select users which have FTP access
    $query = "SELECT role_params FROM cms_user_role INNER JOIN sms_user USING(user_id) WHERE domain_id='$domain_id' AND role_id=3"; // 3=FTP access
    $ret = $db->getCol($query);
    if (!DB::isError($ret)) {
        // if there are some users - process them
        if ($ret) {
            // connect to FTPDB
            $db_con = createobject("db_ftp");
            $dbf = $db_con->connect();

            foreach ($ret as $rps) {
                $rp = unserialize($rps);
                if (isset($rp["ftp_user_id"])) {
                    $ftp_user_id = $rp["ftp_user_id"];
                    // update FTP account with new path
                    $query = "UPDATE accounts SET homedir='/home/ttt-ws-production/www/files/$domain_name' WHERE user_id='$ftp_user_id'";
                    $dbf->query($query);
                }
            }
        }
    }
}

function space_consumed($space) {
    $result = "N/A";
    if (!is_null($space)) {
        $result = number_format($space / 1048576, 2) . "Mb";
    }
    return '<span style="color:#555">' . $result . '</span>';
}

function created_at($date) {
    $result = "N/A";
    if ($date) {
        $parts = explode(" ", $date);
        $result = $parts[0];
    }
    return '<span style="color:#555">' . $result . '</span>';
}

function manage_ldap_email_domain($domain_name, $enabled, $old_domain_name = null) {
    $baseDN = "o=domains,dc=twintierstech,dc=net";

    $ldap = verse_ldap_connect();

    if (PEAR::isError($ldap)) {
        // log error
        return false;
    }

    if ($old_domain_name) {
        $dsn = "domainName=$old_domain_name,$baseDN";
    }
    else {
        $dsn = "domainName=$domain_name,$baseDN";
    }

    if (verse_ldap_entry_exists($ldap, $dsn)) {
        //		echo "LDAP: modifying domain";
        // modify entry
        if ($old_domain_name) {
            // rename entry
            $newdsn = "domainName=$domain_name,$baseDN";
            $ldap->move($dsn, $newdsn);
            $dsn = $newdsn;
        }
        // modify enabled attribute
        $replace = array("accountStatus" => $enabled ? 'active' : "disabled");
        return $ldap->modify($dsn, array("replace" => $replace));
    }
    else {
        //    echo "LDAP: creating domain";
        // create domain at LDAP only if email enabled
        if ($enabled) {
            // create new entry
            $attributes = array(
                'objectClass' => 'mailDomain',
                'domainName' => $domain_name,
                'accountStatus' => 'active',
                'cn' => $domain_name,
                'enabledService' => array('mail', 'recipientbcc', 'senderbcc'),
                'mtaTransport' => 'dovecot'
            );
            // create domain entry
            $entry = Net_LDAP2_Entry::createFresh($dsn, $attributes);
            // Add the entry to the directory:
            $ret = $ldap->add($entry);

            if (PEAR::isError($ret)) {
                // log error
            }
            else {
                // create ou=Users and ou=Aliases entries under the domain
                $users_attributes = array(
                    'objectClass' => array('organizationalUnit', 'top'),
                    'ou' => "Users"
                );
                $aliases_attributes = array(
                    'objectClass' => array('organizationalUnit', 'top'),
                    'ou' => "Aliases"
                );

                $entry = Net_LDAP2_Entry::createFresh("ou=Users,$dsn", $users_attributes);
                $ret = $ldap->add($entry);
                if (PEAR::isError($ret)) {
                    // log error
                }

                $entry = Net_LDAP2_Entry::createFresh("ou=Aliases,$dsn", $aliases_attributes);
                $ret = $ldap->add($entry);
                if (PEAR::isError($ret)) {
                    // log error
                }
            }
        }
    }
    return true;
}
