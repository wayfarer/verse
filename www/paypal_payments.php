<?php
  include("inc/verse.inc.php"); //main header - initializes Verse environment

if ($user->logged()) {
    echo verse_format_messages();
    if (!isset($_GET["ipn"])) {
        $ipns = array();
        foreach (glob("logs/raw/*.ipn") as $ipn) {
            $serialized = file_get_contents($ipn);
            $data = unserialize($serialized);
            $sessionid = $data["custom"];
            $business = $data["business"];
            if (file_exists("logs/raw/$sessionid.ses")) {
                $sessiondata = unserialize(file_get_contents("logs/raw/$sessionid.ses"));
                $domain_id = $sessiondata["domain_id"];
            }
            else {
                $domain_id = "no data";
            }
            $ipns[] = array("name" => basename($ipn), "domain_id" => $domain_id, "business" => $business);
        }
        $smarty->assign("ipns", $ipns);
        $smarty->display("pp-ipn-list.tpl");
    }
    else {
        $ipn = $_GET["ipn"];
        if (!isset($_GET["execute"])) {
            // display IPN details
            $serialized = file_get_contents("logs/raw/$ipn");
            $data = unserialize($serialized);
            $smarty->assign("domain_name", $domain_name);
            $smarty->assign("ipn", $ipn);
            $smarty->assign("packet", print_r($data, true));
            $smarty->display("pp-ipn-packet.tpl");
        }
        else {
            // execute IPN
            $ipn_data = unserialize(file_get_contents("logs/raw/$ipn"));

            $ipn_data["domain_id"] = $domain_id;
            // prepare POST
            $fields = array();
            foreach ($ipn_data as $k => $v) {
                $fields[] = "$k=" . urlencode($v);
            }

            $post = implode("&", $fields);

            $ch = curl_init();
            $url = "http://ws1.twintierstech.net/paypal_ipn_special.php";
            //        $url = "http://w2.verse.home/paypal_ipn_special.php";

            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

            $result = curl_exec($ch);
            if (curl_errno($ch)) {
                $result = curl_error($ch);
            }

            verse_set_message($result);
            verse_set_message("IPN packet sent");
            header("Location: ?ipn=$ipn");
        }
    }
}
else {
    echo "ad";
}
