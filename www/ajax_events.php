<?php
 // obsolete
// events management, ajax handling actions
include("inc/verse.inc.php"); //main header - initializes Verse environment

if ($user->have_role(ROLE_OBITUARIES_MANAGEMENT)) {
    $_POST = utf8_to_latin($_POST);
    $action = $_POST["action"];

    switch ($action) {
        case "list":
            $rep = createobject("report_ajax", array($db, "plg_event", REP_WITH_PAGENATOR, true, 20, 'width="750px" style="background-color: #DDEEFF"'));

            $rep->add_field("Event time", REP_STRING_TEMPLATE, '{FROM_UNIXTIME(#timestamp#,"' . DB_DATETIME_FORMAT . '")}', REP_ORDERABLE, 'align="center"');
            $rep->add_field("Description", REP_STRING_TEMPLATE, '{description}', REP_UNORDERABLE, 'align="center"');
            $rep->add_field("", REP_STRING_TEMPLATE, href_js_action("properties", "new EventEditor({event_id})"), REP_UNORDERABLE, 'align="center"');
            $rep->add_field("", REP_STRING_TEMPLATE, href_js_action("delete", "delete_event({event_id})"), REP_UNORDERABLE, 'align="center"');

            $rep->add_filter("", REP_INVISIBLE_FILTER, "domain_id", "=", $domain_id);

            $rep->html_attributes('width="750px"');

            if (count($_POST) < 3) {
                $rep->order_by("timestamp", 1);
            }
            $rep->handle_events($_POST);

            $html = $rep->make_report();
//		    echo $rep->query;
            echo $html;
            break;
        case "load":
            $event_id = intval(@$_POST["id"]);
            $query = "SELECT FROM_UNIXTIME(timestamp, '%Y-%m-%d %h:%i %p') timestamp, description FROM plg_event WHERE event_id='$event_id' AND domain_id='$domain_id'";
            $ret = $db->getRow($query, DB_FETCHMODE_ASSOC);
            header("X-JSON:" . make_json_response($ret));
            break;
        case "save":
            $event_id = intval(@$_POST["id"]);
            $p = array();
            $p["timestamp"] = strtotime(@$_POST["timestamp"]);
            $p["description"] = in(@$_POST["description"]);
            if ($event_id) {
                $query = "UPDATE plg_event SET " . make_set_clause($p) . " WHERE event_id='$event_id' AND domain_id='$domain_id'";
            }
            else {
                $query = "INSERT plg_event SET " . make_set_clause($p) . ", domain_id='$domain_id'";
            }
            $db->query($query);
            echo "ok";
            break;
        case "delete":
            $event_id = intval(@$_POST["id"]);
            $query = "DELETE FROM plg_event WHERE event_id='$event_id' AND domain_id='$domain_id'";
            $db->query($query);
            echo "ok";
            break;
    }
}
