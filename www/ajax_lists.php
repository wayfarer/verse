<?php
 // obsolete
// lists management, ajax handling actions
include("inc/verse.inc.php"); //main header - initializes Verse environment

if ($user->have_role(ROLE_CONTENT_MANAGEMENT)) {
    $_POST = utf8_to_latin($_POST);
    $action = $_POST["action"];

    switch ($action) {
        case "list":
            $rep = createobject("report_ajax", array($db, "plg_list", REP_WITH_PAGENATOR, true, 50, 'width="650px" style="background-color: #DDEEFF"'));

            $rep->add_field("Name", REP_STRING_TEMPLATE, '{name}', REP_ORDERABLE, 'align="left"');
            $rep->add_field("Type", REP_STRING_TEMPLATE, '{type}', REP_ORDERABLE, 'align="center"');
            $rep->add_field("", REP_STRING_TEMPLATE, href_js_action("properties", "new ListEditor({list_id})"), REP_UNORDERABLE, 'align="center"');
            $rep->add_field("", REP_STRING_TEMPLATE, href_js_action("delete", "delete_list({list_id})"), REP_UNORDERABLE, 'align="center"');

            $rep->add_filter("", REP_INVISIBLE_FILTER, "domain_id", "=", $domain_id);
//		    $rep->add_filter("Type", REP_SELECT_FILTER, "type", "=");

            $rep->html_attributes('width="650px"');

            $rep->handle_events($_POST);

            $html = $rep->make_report();

            echo $html;
            break;
        case "load":
            $list_id = intval(@$_POST["id"]);
            $query = "SELECT type, name, company, address, address2, phone, fax, website, email, description FROM plg_list WHERE list_id='$list_id'";
            $ret = $db->getRow($query, DB_FETCHMODE_ASSOC);
            header("X-JSON:" . make_json_response($ret));
            break;
        case "save":
            $list_id = intval(@$_POST["id"]);
            $p = array();
            $p["type"] = in(@$_POST["type"]);
            $p["name"] = in(@$_POST["name"]);
            $p["company"] = in(@$_POST["company"]);
            $p["address"] = in(@$_POST["address"]);
            $p["address2"] = in(@$_POST["address2"]);
            $p["phone"] = in(@$_POST["phone"]);
            $p["fax"] = in(@$_POST["fax"]);
            $p["website"] = in(@$_POST["website"]);
            $p["email"] = in(@$_POST["email"]);
            $p["description"] = in(@$_POST["description"]);
            if ($list_id) {
                $query = "UPDATE plg_list SET " . make_set_clause($p) . " WHERE list_id='$list_id' AND domain_id='$domain_id'";
            }
            else {
                $query = "INSERT plg_list SET " . make_set_clause($p) . ", domain_id='$domain_id'";
            }
            $db->query($query);
            echo "ok";
            break;
        case "delete":
            $list_id = intval(@$_POST["id"]);
            $query = "DELETE FROM plg_list WHERE list_id='$list_id' AND domain_id='$domain_id'";
            $db->query($query);
            echo "ok";
            break;
    }
}
