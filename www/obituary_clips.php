<?
include("inc/verse.inc.php"); //main header - initializes Verse environment

if ($user->have_role(ROLE_OBITUARIES_MANAGEMENT)) {
    // check if obituary exists
    $obituary_id = intval(@$_GET["id"]);
    if (!$obituary_id) {
        $obituary_id = intval(@$_POST["obituary_id"]);
    }
    $query = "SELECT 1 FROM plg_obituary WHERE obituary_id='$obituary_id' AND domain_id='$domain_id'";
    $ret = $db->getOne($query);
    if (!$ret && !count($_POST)) {
        header("Location: obituaries.php");
        exit;
    }

    $action = @$_POST["action"];
    switch ($action) {
        case "list":
            $rep = createobject("report_ajax", array($db, "plg_movieclip", REP_WITH_PAGENATOR, true, 20, 'width="700px" style="background-color: #DDEEFF"'));
            $rep->add_table("plg_obituary_movieclip", REP_INNER_JOIN, "movieclip_id");

            $rep->add_field("Title", REP_STRING_TEMPLATE, '{title}', REP_ORDERABLE, 'align="center"');
            $rep->add_field("Created", REP_STRING_TEMPLATE, '{FROM_UNIXTIME(#timestamp#,"' . DB_DATETIME_FORMAT . '")}', REP_ORDERABLE, 'align="center"');
            $rep->add_field("Status", REP_CALLBACK, 'status({status})', REP_UNORDERABLE, 'align="center"');
            $rep->add_field("", REP_STRING_TEMPLATE, href_js_action("edit", "new MovieclipEditor({movieclip_id|1})"), REP_UNORDERABLE, 'align="center"');
            $rep->add_field("", REP_STRING_TEMPLATE, href_js_action("delete", "delete_movieclip({movieclip_id|1})"), REP_UNORDERABLE, 'align="center"');

            $rep->add_filter("", REP_INVISIBLE_FILTER, "obituary_id", "=", $obituary_id);
            $rep->add_filter("", REP_INVISIBLE_FILTER, "domain_id", "=", $domain_id);

            $rep->html_attributes('width="700px"');

            if (count($_POST) < 3) {
                $rep->order_by("timestamp", 1);
            }
            $rep->handle_events($_POST);

            $html = $rep->make_report();
            echo $html;
            break;
        case "load":
            $movieclip_id = intval(@$_POST["id"]);
            $query = "SELECT title, protected, password FROM plg_movieclip WHERE movieclip_id='$movieclip_id'";
            $ret = $db->getRow($query, DB_FETCHMODE_ASSOC);
            header("X-JSON:" . make_json_response($ret));
            break;
        case "save":
            $obituary_id = intval(@$_GET["id"]);
            if ($obituary_id) {
                $movieclip_id = intval(@$_POST["movieclip_id"]);
                $file_present = false;
                if ($_FILES["movieclip"]["tmp_name"]) {
                    // generate filename
                    ensure_dir("files/tmp");
                    $filename = tempnam("tmp", "");
                    $file_present = move_uploaded_file($_FILES["movieclip"]["tmp_name"], "files/$domain_name/$filename");
                }
                if ($movieclip_id || $file_present) {
                    $p = array();
                    $p["title"] = in(@$_POST["title"]);
                    $p["protected"] = (@$_POST["protected"] ? 1 : 0);
                    $p["password"] = in(@$_POST["password"]);
                    if ($file_present) {
                        $p["tmpfilename"] = $filename;
                        $p["origfilename"] = in($_FILES["movieclip"]["name"]);
                    }
                    if ($movieclip_id) {
                        if ($file_present) {
                            // remove old file
                            $query = "SELECT filename FROM plg_movieclip WHERE movieclip_id='$movieclip_id' AND domain_id='$domain_id'";
                            $oldfn = $db->getOne($query);
                            if (!DB::isError($oldfn) && $oldfn) {
                                unlink("files/$oldfn");
                            }
                            $p["status"] = 2;
                        }
                        $query = "UPDATE plg_movieclip SET " . make_set_clause($p) . " WHERE movieclip_id='$movieclip_id' AND domain_id='$domain_id'";
                        $db->query($query);
                    }
                    else {
                        $p["domain_id"] = $domain_id;
                        $p["status"] = 2;
                        $query = "INSERT plg_movieclip SET " . make_set_clause($p) . ", timestamp=UNIX_TIMESTAMP(now())";
                        $ret = $db->query($query);
                        $p = array();
                        $movieclip_id = $p["movieclip_id"] = $db->getOne("SELECT last_insert_id()");
                        $p["obituary_id"] = $obituary_id;
                        $query = "INSERT plg_obituary_movieclip SET " . make_set_clause($p);
                        $db->query($query);
                    }
                    if ($file_present) {
                        // run conversion script
                        exec("php ../util/convertmc.php $movieclip_id");
                    }
                }
            }
            echo "<script>window.parent.document.dialog_window.onsave();</script>";
            break;
        case "delete":
            $movieclip_id = intval(@$_POST["id"]);
            $query = "DELETE FROM plg_obituary_movieclip WHERE movieclip_id='$movieclip_id'";
            $db->query($query);
            $query = "DELETE FROM plg_movieclip WHERE movieclip_id='$movieclip_id' AND domain_id='$domain_id'";
            $db->query($query);
            break;
        default:
            $smarty->assign("obituary_id", $obituary_id);
            $smarty->display("obituary_clips.tpl");
    }
}
else {
    header("Location: login.php");
}

function ensure_dir($dir) {
    if (!is_dir($dir)) mkdir($dir);
}

function status($status) {
    $ret = "unknown";
    switch ($status) {
        case 1:
            $ret = "ready";
            break;
        case 2:
            $ret = "converting";
            break;
        case 3:
            $ret = "invalid file";
            break;
    }
    return $ret;
}
