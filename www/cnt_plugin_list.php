<?php
	// plugin for display list page
  include "cnt_plugin_main.php";

	function process_content($content, $state) {
		if(isset($state['id'])) {
			if($state['id']!='all') {
        $id = intval(@$state["id"]);
			}
			else {
				$id = 'all';
			}
		}
		// track pagetype
		$p = arg(0);
		$GLOBALS["smarty"]->assign("p", $p);
		if(!$id || $id=="all") {
      if($id!="all") {
        $cnt = process_tags($content["content1"], array("list"=>"mylist"));
      }
      else {
        $cnt = process_tags($content["content1"], array("list"=>"mylist"), "all");
      }
		}
		else {
			$cnt = process_tags($content["content2"], array("list"=>"mylist_item"), $id);
			// make some defaults to help to content makers :))
			if(!$cnt) {
        $cnt = process_tags($content["content1"], array("list"=>"mylist_item"), $id);
//				$cnt = mylist_item(array("user_params"=>$id));
			}
		}
		return $cnt;
	}

	function mylist($params) {
    global $domain_id, $db;

    if(isset($params['content'])) {
      $content_field = $params['content'];
    }
    else {
      $content_field = 'description';
    }

    if(@$params['user_params']!='all' && isset($params['selected'])) {
        $domain_ids = get_shared_list_domain_ids();
        if (is_numeric($params['selected'])) {
            // determine last N items for category
            $type = in($params['type']);
            $limit = intval($params['selected']);
            if (count($domain_ids)) {
                $query = "SELECT list_id FROM plg_list WHERE type='$type' AND domain_id IN (".implode(', ', $domain_ids).") ORDER BY list_id DESC LIMIT $limit";
            } else {
                $query = "SELECT list_id FROM plg_list WHERE type='$type' AND domain_id='$domain_id' ORDER BY list_id DESC LIMIT $limit";
            }
            $list_ids = $db->getAll($query, DB_FETCHMODE_ASSOC);
            return mylist_item(array_merge($params, array("user_params" => $list_ids)));
        } else {
            switch ($params['selected']) {
                case 'last':
                    // determine last item for category
                    $type = in($params['type']);
                    if (count($domain_ids)) {
                        $query = "SELECT max(list_id) FROM plg_list WHERE type='$type' AND domain_id IN (".implode(', ', $domain_ids).")";
                    } else {
                        $query = "SELECT max(list_id) FROM plg_list WHERE type='$type' AND domain_id='$domain_id'";
                    }
                    $list_id = $db->getOne($query);
                    return mylist_item(array_merge($params, array("user_params" => $list_id)));
                    break;
                case 'first':
                    // determine first item for category
                    $type = in($params['type']);
                    if (count($domain_ids)) {
                        $query = "SELECT min(list_id) FROM plg_list WHERE type='$type' AND domain_id IN (".implode(', ', $domain_ids).")";
                    } else {
                        $query = "SELECT min(list_id) FROM plg_list WHERE type='$type' AND domain_id='$domain_id'";
                    }
                    $list_id = $db->getOne($query);
                    return mylist_item(array_merge($params, array("user_params" => $list_id)));
                    break;
                case 'all':
                    $link = '';
                    if (isset($params['link'])) {
                        if (isset($params['linktitle'])) {
                            $linktitle = $params['linktitle'];
                        }
                        else {
                            $linktitle = "more";
                        }
                        $link = '<div class="verse-list-link"><a href="' . $params['link'] . '/{list_id}">' . $linktitle . '</a><div>';
                    }
                    if (isset($params['pagenator'])) {
                        $pagenator = REP_WITH_PAGENATOR;
                    }
                    else {
                        $pagenator = REP_NO_PAGENATOR;
                    }
                    if (isset($params['rpp'])) {
                        $rpp = $params['rpp'];
                    }
                    else {
                        $rpp = 25;
                    }

                    // show all items expanded
                    $rep = createobject("report", array($db, "plg_list", $pagenator, true, $rpp, 'class="list list_header"'));

                    $rep->add_field("", REP_STRING, "<strong>{name}</strong><br>{" . $content_field . "}$link", REP_ORDERABLE, 'align="left" style="padding-left: 40px"');

                    if (count($domain_ids)) {
                        $rep->add_filter("", REP_INVISIBLE_FILTER, "domain_id", "IN", $domain_ids);
                    } else {
                        $rep->add_filter("", REP_INVISIBLE_FILTER, "domain_id", "=", $domain_id);
                    }

                    if (substr($params["type"], -1) != '*') {
                        $rep->add_filter("", REP_INVISIBLE_FILTER, "type", "=", $params["type"]);
                    }
                    else {
                        $rep->add_filter("", REP_INVISIBLE_FILTER, "type", "LIKE", substr($params["type"], 0, -1) . '%');
                    }

                    if (isset($params['filter'])) {
                        $rep->add_filter("Search for", REP_CUSTOM_FILTER, 'mylist_search');
                    }

                    $rep->html_attributes('class="list list_content"');

                    // order
                    if (isset($params['deforder'])) {
                        $vals = explode(",", $params['deforder']);
                        $rep->order_by($vals[0], ($vals[1] == "desc") ? 1 : 0);
                    }
                    else {
                        $rep->order_by("name", 0);
                    }

                    $rep->handle_events($_GET);

                    $html = $rep->make_report();

                    /*          if(isset($params["listtitle"])) {
                      $html = '<h1>'.$params["listtitle"].'</h1>'.$html;
                    }*/

                    return str_replace('\n', "<br>", html_entity_decode($html));
                    break;
            }
        }
    }

    // show statically (without pager)
		if(true) {
      $rep = createobject("report", array($db, "plg_list", REP_NO_PAGENATOR, true, 100, 'class="list list_header"'));

			$rep->add_field("", REP_CALLBACK, 'item_link({name},{list_id})', REP_ORDERABLE, 'align="left" style="padding-left: 40px"');

          $domain_ids = get_shared_list_domain_ids();
          if (count($domain_ids)) {
            $rep->add_filter("", REP_INVISIBLE_FILTER, "domain_id", "IN", $domain_ids);
          } else {
            $rep->add_filter("", REP_INVISIBLE_FILTER, "domain_id", "=", $domain_id);
          }
          
		  $rep->add_filter("", REP_INVISIBLE_FILTER, "type", "=", $params["type"]);

		  $rep->html_attributes('class="list list_content"');

	    // order
	    if(isset($params['deforder'])) {
	      $vals = explode(",",$params['deforder']);
	      $rep->order_by($vals[0], ($vals[1]=="desc")?1:0);
	    }
      else {
  	    $rep->order_by("name", 0);
      }

      $rep->handle_events($_GET);

		  $html = $rep->make_report();

		  if(isset($params["listtitle"])) {
		  	$html = '<h1>'.$params["listtitle"].'</h1>'.$html;
		  }

      return str_replace('\n',"<br>",html_entity_decode($html));
    }
		else {
      // show dynamically (ajax pager)
      $GLOBALS["smarty"]->assign("list_type", $params["type"]);
			$html = $GLOBALS["smarty"]->fetch("cnt_plugin_list.tpl");
    }

		return $html;
	}

    function mylist_item($params) {
        global $domain_id, $db, $smarty;

        if (is_array(@$params["user_params"])) {
            $item_ids = array();
            foreach ($params["user_params"] as $user_param) {
                $item_ids[] = $user_param['list_id'];
            }
            $where = "list_id IN (".implode(', ', $item_ids).")";
            $moreheaders_where = "list_id NOT IN (".implode(', ', $item_ids).")";
        } else {
            $item_id = @$params["user_params"];
            $where = "list_id='$item_id'";
            $moreheaders_where = "list_id!='$item_id'";
        }

        if (isset($params['content'])) {
            $content_field = $params['content'];
        } else {
            $content_field = 'description';
        }

        if (isset($params['deforder'])) {
            $vals = explode(",", $params['deforder']);
            $order_by = " ORDER BY ".$vals[0]." ".((isset($vals[1]) && $vals[1] == "desc") ? "DESC" : "ASC");
        } else {
            $order_by = "";
        }

        $query = "SELECT name, company, address, address2, phone, fax, website, email, summary, description, type FROM plg_list WHERE " . $where;

        $domain_ids = get_shared_list_domain_ids();
        if (count($domain_ids)) {
            $and_where = " AND domain_id IN (" . implode(", ", $domain_ids) . ")";
        } else {
            $and_where = " AND domain_id='$domain_id'";
        }
        $query .= $and_where.$order_by;
        $data = $db->getAll($query, DB_FETCHMODE_ASSOC);

        if (isset($params['moreheaders'])) {
            $limit = $params['moreheaders'];
            $query = "SELECT list_id, name FROM plg_list WHERE $moreheaders_where AND type='{$data[0]['type']}'" . $and_where . " ORDER BY list_id DESC LIMIT $limit";
            $moreheaders = $db->getAll($query, DB_FETCHMODE_ASSOC);
            $smarty->assign('moreheaders', $moreheaders);
            if (count($moreheaders) == $limit) {
                $smarty->assign("moreheaderslink", 1);
            }
        }
        if (isset($params['moreheaderstitle'])) {
            $moreheaderstitle = $params['moreheaderstitle'];
            $smarty->assign('moreheaderstitle', $moreheaderstitle);
        }

        // back link
        if (isset($params['back'])) {
            $smarty->assign("p", $params['back']);
        }

        // show item, currently one form for all types
        $data = str_replace(array('\n', '�'), " ", $data);
        $smarty->assign("data", $data);
        $smarty->assign("content_field", $content_field);
        return $smarty->fetch("cnt_plugin_list_view.tpl");
    }

	function item_link($name, $item_id) {
    return l($name, arg(0).'/'.$item_id);
	}

  function mylist_search($value) {
    $where_clause = "(name like '%$value%' or description like '%$value%')";
    return $where_clause;
  }

    function get_shared_list_domain_ids() {
        global $domain_id, $db;
        static $domain_ids;

        if(!$domain_ids) {
            $group_ids = $db->getCol("SELECT lg.id FROM sms_domain_link_group lg JOIN sms_domain_link l on l.group_id=lg.id WHERE lg.is_shared_lists='1' AND l.domain_id='$domain_id'");
            if (count($group_ids)) {
                //fetch list parent domains
                $domain_ids = $db->getCol("SELECT domain_id FROM sms_domain_link_list_parent_domain WHERE group_id IN (".implode(', ', $group_ids).")");
                if(count($domain_ids) && !in_array($domain_id, $domain_ids)) {
                    $domain_ids[] = $domain_id;
                }

                if(!count($domain_ids)) {
                    //fetch all domains from domain group
                    $domain_ids = $db->getCol("SELECT DISTINCT domain_id FROM sms_domain_link WHERE group_id IN (".implode(', ', $group_ids).")");
                }
            } else {
                $domain_ids = array();
            }
        }

        return $domain_ids;
    }
