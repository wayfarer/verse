<?php
// obsolete
include ("inc/verse.inc.php"); //main header - initializes Verse environment

if ($user->have_role(ROLE_WEBCASTING_MANAGEMENT)) {
    $action = @$_POST["action"];

    switch ($action) {
        case "list":
            $rep = createobject("report_ajax", array($db, "cms_webcasting", REP_WITH_PAGENATOR, true, 30, 'width="900px" style="background-color: #DDEEFF"'));
            $rep->add_table("plg_obituary", REP_LEFT_JOIN, "obituary_id");

            $rep->add_field("", REP_CALLBACK, 'user_action({status},{webcasting_id})', REP_UNORDERABLE, 'align="center"');
            $rep->add_field("webcasting title", REP_STRING_TEMPLATE, '{name}', REP_ORDERABLE, 'align="center"');
            $rep->add_field("obituary", REP_STRING_TEMPLATE, '{first_name} {middle_name} {last_name}', REP_ORDERABLE, 'align="center"');
            $rep->add_field("schedule", REP_CALLBACK, 'schedule({schedule_from},{schedule_to})', REP_ORDERABLE, 'align="center"');
            $rep->add_field("fact", REP_CALLBACK, 'fact({was_from},{was_to})', REP_ORDERABLE, 'align="center"');
            $rep->add_field("record", REP_CALLBACK, 'record({record})', REP_ORDERABLE, 'align="center"');
            $rep->add_field("status", REP_CALLBACK, 'status({status})', REP_ORDERABLE, 'align="center"');
            $rep->add_field("", REP_CALLBACK, 'edit({status},{webcasting_id})', REP_UNORDERABLE, 'align="center"');
            $rep->add_field("", REP_STRING_TEMPLATE, href_js_action("delete", "delete_webcasting({webcasting_id})"), REP_UNORDERABLE, 'align="center"');
            $rep->add_field("", REP_CALLBACK, 'notify({status},{webcasting_id})', REP_UNORDERABLE, 'align="center"');
            $rep->add_field("", REP_STRING_TEMPLATE, href("users >", "webcasting_users.php?id={webcasting_id}"), REP_UNORDERABLE, 'align="center"');

            $rep->add_filter("", REP_INVISIBLE_FILTER, "t1.domain_id", "=", $domain_id);

            $rep->html_attributes('width="900px"');

            if (count($_POST) < 3) {
                $rep->order_by("schedule_from", 1);
            }
            $rep->handle_events($_POST);

//      echo $rep->build_report_query();
            $html = $rep->make_report();

            echo $html;
            break;
        case "load":
            $webcasting_id = intval(@$_POST["id"]);
            $query = "SELECT obituary_id, name, schedule_from, schedule_to, record, status FROM cms_webcasting WHERE webcasting_id='$webcasting_id' AND domain_id='$domain_id'";
            $ret = $db->getRow($query, DB_FETCHMODE_ASSOC);
            header("X-JSON:" . make_json_response($ret));
            break;
        case "save":
            $webcasting_id = intval(@$_POST["id"]);
            $p = array();
            $p["domain_id"] = $domain_id;
            $p["obituary_id"] = intval(@$_POST["obituary_id"]);
            $p["name"] = in(@$_POST["name"]);
            if ($_POST["schedule_from"]) {
                $p["schedule_from"] = in(@$_POST["schedule_from"]);
            }
            else {
                $p["schedule_from"] = NULL;
            }
            if ($_POST["schedule_to"]) {
                $p["schedule_to"] = in(@$_POST["schedule_to"]);
            }
            else {
                $p["schedule_to"] = NULL;
            }
            $p["record"] = @$_POST["record"] ? 1 : 0;
            $p["status"] = @$_POST["enabled"] ? 1 : 0;
            if ($webcasting_id) {
                // check prev status
                $query = "SELECT status FROM cms_webcasting WHERE webcasting_id='$webcasting_id' AND domain_id='$domain_id'";
                $prev_status = $db->getOne($query);
                if ($prev_status > 1) $p["status"] = $prev_status;
                $query = "UPDATE cms_webcasting SET " . make_set_clause($p) . " WHERE webcasting_id='$webcasting_id'";
                $db->query($query);
            }
            else {
                $query = "INSERT cms_webcasting SET " . make_set_clause($p) . ", created=now()";
                $db->query($query);
            }
            break;
        case "load_config":
            $query = "SELECT param, value FROM cms_webcasting_config WHERE domain_id='$domain_id'";
            $ret = $db->getAssoc($query);
            if (!trim($ret["email_template"])) {
                $ret["email_template"] = file_get_contents("templates/helpers/webcasting_email.txt");
            }
            header("X-JSON:" . make_json_response($ret));
            break;
        case "save_config":
            $email_template = in(@$_POST["email_template"]);
            $email_template_subject = in(@$_POST["email_template_subject"]);
            $query = "REPLACE cms_webcasting_config SET domain_id='$domain_id', param='email_template', value='$email_template'";
            $db->query($query);
            $query = "REPLACE cms_webcasting_config SET domain_id='$domain_id', param='email_template_subject', value='$email_template_subject'";
            $db->query($query);
            break;
        case "load_user":
            $user_id = intval(@$_POST["id"]);
            $query = "SELECT login, name, enabled, active_from, active_to FROM cms_access_user WHERE user_id='$user_id' AND domain_id='$domain_id'";
            $ret = $db->getRow($query, DB_FETCHMODE_ASSOC);
            header("X-JSON:" . make_json_response($ret));
            echo "done";
            break;
        case "save_user":
            $user_id = intval(@$_POST["id"]);
            $p = array();
            $p["domain_id"] = $domain_id;
            $p["login"] = in(@$_POST["ulogin"]);
            $p["name"] = in(@$_POST["name"]);
            $p["enabled"] = @$_POST["enabled"] ? 1 : 0;
            if ($_POST["active_from"]) {
                $p["active_from"] = in(@$_POST["active_from"]);
            }
            else {
                $p["active_from"] = NULL;
            }
            if ($_POST["active_to"]) {
                $p["active_to"] = in(@$_POST["active_to"]);
            }
            else {
                $p["active_to"] = NULL;
            }
            $password = in(@$_POST["password"]);
            if ($user_id) {
                $query = "UPDATE cms_access_user SET " . make_set_clause($p);
                if (strlen($password)) {
                    $query .= ", password='$password'";
                }
                $query .= " WHERE user_id='$user_id'";
                $db->query($query);
            }
            else {
                $query = "INSERT cms_access_user SET " . make_set_clause($p) . ", password='$password'";
                $db->query($query);
                //				$query = "SELECT last_insert_id()";
                //				$user_id = $db->getOne($query);
            }
            echo "done";
            break;
        case "delete_user":
            $user_id = intval(@$_POST["id"]);
            $query = "DELETE FROM cms_access_user WHERE user_id='$user_id' AND domain_id='$domain_id'";
            $db->query($query);
            break;
        case "load_email":
            $webcasting_id = intval(@$_POST["id"]);
            // fetch users
            $query = "SELECT name, login, password, email FROM cms_access_user WHERE webcasting_id='$webcasting_id' AND enabled=1 AND domain_id='$domain_id'";
            $ret = $db->getAll($query, DB_FETCHMODE_ASSOC);
            // fetch config
            $query = "SELECT param, value FROM cms_webcasting_config WHERE domain_id='$domain_id'";
            $config = $db->getAssoc($query);
            $text_template = $config["email_template"];
            if (!trim($text_template)) {
                $text_template = file_get_contents("templates/helpers/webcasting_email.txt");
            }
            // fetch webcasting data
            $query = "SELECT name, schedule_from, schedule_to, first_name, middle_name, last_name FROM cms_webcasting wc LEFT JOIN plg_obituary USING(obituary_id) WHERE webcasting_id='$webcasting_id' AND wc.domain_id='$domain_id'";
            $webcasting = $db->getRow($query, DB_FETCHMODE_ASSOC);
            $name = $webcasting["first_name"] . " " . $webcasting["middle_name"] . " " . $webcasting["last_name"];
            if (!trim($name)) {
                $name = $webcasting["name"];
            }
            $schedule = date("D, M j, Y \a\\t g:i a", strtotime($webcasting["schedule_from"]));
            $page = "webcasting";
            $url = "http://$domain_name/?p=$page";
            $delta = 5;
            $text = str_replace(array("{name}", "{schedule}", "{url}", "{pre_login_delta}"), array($name, $schedule, $url, $delta), $text_template);
            $data = array();
            $emails = "";
            foreach ($ret as $user) {
                if ($emails) $emails .= ", ";
                $emails .= $user['name'] . " <" . $user['email'] . ">";
            }
            $data["emailto"] = $emails;
            $data["subject"] = $config["email_template_subject"];
            $data["email_text"] = $text;
            header("X-JSON:" . make_json_response($data));
            break;
        case "send_email":
            $webcasting_id = intval(@$_POST["id"]);
            $email_template = $_POST["email_text"];
            $subject = $_POST["subject"];
            // fetch users
            $query = "SELECT name, login, password, email FROM cms_access_user WHERE webcasting_id='$webcasting_id' AND enabled=1 AND domain_id='$domain_id'";
            $ret = $db->getAll($query, DB_FETCHMODE_ASSOC);
            foreach ($ret as $user) {
                $text = str_replace(array("{login}", "{password}"), array($user["login"], $user["password"]), $email_template);
                send_invitation(trim($user["email"]), $subject, $text);
            }
            break;
        case "start_now":
            $webcasting_id = intval(@$_POST["id"]);
            exec("../vlc.sh start", $output, $status);
            // 6 - manually started
            $query = "UPDATE cms_webcasting SET status=6, was_from=now() WHERE webcasting_id='$webcasting_id' AND domain_id='$domain_id'";
            $db->query($query);
            echo print_r($output) . "<br>$status";
            break;
        case "stop_now":
            $webcasting_id = intval(@$_POST["id"]);
            // 3 - manually started
            $query = "UPDATE cms_webcasting SET status=3, was_to=now() WHERE webcasting_id='$webcasting_id' AND domain_id='$domain_id'";
            $db->query($query);
            exec("../vlc.sh stop", $output, $status);
            echo print_r($output) . "<br>$status";
            break;
        case "restart_now":
            exec("../vlc.sh restart", $output, $status);
            echo print_r($output) . "<br>$status";
            break;
        default:
            // fetch obituaries with death_date in last 10 days (candidates for webcasting)
            $query = "SELECT obituary_id, first_name, middle_name, last_name, death_date FROM plg_obituary WHERE death_date>curdate()-interval 10 day AND domain_id='$domain_id' ORDER BY death_date DESC";
            $obit_rows = $db->getAll($query, DB_FETCHMODE_ASSOC);
            $obituaries = array("0" => "No obituary");
            foreach ($obit_rows as $obit_row) {
                $obituaries[$obit_row["obituary_id"]] = $obit_row["first_name"] . " " . $obit_row["middle_name"] . " " . $obit_row["last_name"] . " (" . $obit_row["death_date"] . ")";
            }

            $smarty->assign("obituaries", $obituaries);
            $smarty->display("webcasting.tpl");
    }
}
else {
    header("Location: login.php");
}

function send_invitation($emailto, $subj, $text) {
    global $domain_name;

    $emailfrom = "noreply@twintierstech.net";
    mail($emailto, $subj, $text, "From: $domain_name <$emailfrom>");
}

function record($record) {
    if ($record) {
        return "enabled";
    }
    else {
        return "disabled";
    }
}

function schedule($from, $to) {
    if (!$from) $from = "to be started manually";
    if (!$to) $to = "to be ended manually";
    return "$from -<br>$to";
}

function fact($from, $to) {
    return "$from - <br>$to";
}

function status($status) {
    $statuses = array("disabled", "scheduled", "completed (auto)", "completed (manually)", "cancelled", "in progress (auto)", "in progress (manually)");
    return $statuses[$status];
}

function user_action($status, $webcasting_id) {
    $action = "";
    if ($status == 1) {
        $action = href_js_action("RUN NOW", "new ActionDialog($webcasting_id, 1)");
    }
    if ($status == 5 || $status == 6) {
        $action = href_js_action("STOP NOW", "new ActionDialog($webcasting_id, 2)");
    }
    return $action;
}

function notify($status, $webcasting_id) {
    if ($status == 1) {
        return href_js_action("notify", "new NotifyDialog($webcasting_id)");
    }
    else {
        return "";
    }
}

function edit($status, $webcasting_id) {
    if (in_array($status, array(0, 1, 5, 6))) {
        return href_js_action("edit", "new WebcastingEditor($webcasting_id)");
    }
    else {
        return "";
    }
}
