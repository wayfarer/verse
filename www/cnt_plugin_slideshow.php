<?php
 // slideshow page plugin, for scrapbooks
include "cnt_plugin_main.php";

function process_content($content, $state) {
    $obituary_id = intval($state["id"]);
    // use content1 as default content
    return process_tags($content["content1"], array("slide" => "slide", "name" => "name"), $obituary_id);
}

function slide($params) {
    global $domain_id;
    $query = "SELECT image, description FROM plg_slide WHERE obituary_id='" . $params["user_params"] . "' ORDER BY CONVERT(SUBSTRING_INDEX(SUBSTRING_INDEX(image,'.',1),'-',-1), UNSIGNED)";
    $slides = $GLOBALS["db"]->getAll($query, DB_FETCHMODE_ASSOC);
    $GLOBALS["smarty"]->assign("slides", $slides);
    $html = $GLOBALS["smarty"]->fetch("cnt_plugin_slideshow.tpl");
    return $html;
}

function name($params) {
    $query = "SELECT first_name, middle_name, last_name FROM plg_obituary WHERE obituary_id='" . $params["user_params"] . "'";
    $ret = $GLOBALS["db"]->getRow($query, DB_FETCHMODE_ASSOC);
    return $ret["first_name"] . " " . $ret["middle_name"] . " " . $ret["last_name"];
}