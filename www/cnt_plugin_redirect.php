<?php
 // redirect page type plugin
include "cnt_plugin_main.php";

function process_content($content, $state) {
    $url = trim($content["content1"]);
    $url = filter_var($url, FILTER_VALIDATE_URL, FILTER_FLAG_SCHEME_REQUIRED);
    if ($url) {
        // redirect browser and stop processing
        header("Location: $url");
        exit;
    }
    else {
        return "Please put full valid URL to content1 of page content, ex: http://twintierstech.com";
    }
}
