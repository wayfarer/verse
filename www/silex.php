<?php

use Verse\Domain\Domain;

$domain_id = 65;

include "src/silex_bootstrap.php";

$obituaries = require __DIR__ . '/src/obituaries.php';

$_SESSION['theme'] = 'default';

$app['twig']->addFilter('paginate', new Twig_Filter_Function('paginate'));
$app['twig']->addFilter('order', new Twig_Filter_Function('order'));
$app['twig']->addFilter('page', new Twig_Filter_Function('page'));
$app['twig']->addFilter('truncate', new Twig_Filter_Function('truncate'));
$app['twig']->addFilter('thumbnail', new Twig_Filter_Function('thumbnail'));

$app->before(function() use ($app, $domain_id) {
    $domain = new Domain($app['db'], $domain_id);
    $app['request_context']->setParameter('domain', $domain);
});

$app->mount('/', $obituaries);

$app->run();
