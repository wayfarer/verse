<?
include("inc/verse.inc.php"); //main header - initializes Verse environment

$_POST = utf8_to_latin($_POST);
$action = @$_POST["action"];
$front_action = @$_POST["faction"];
// frontend actions (without authorization)
switch ($front_action) {
    case "list":
        $rep = createobject("report_ajax", array($db, "plg_funeral_director", REP_WITH_PAGENATOR, REP_WITH_HEADING, 50, 'class="pagenator" width="570px"'));

        $rep->add_field("Status", REP_STRING_TEMPLATE, '{status}', REP_ORDERABLE, '');
        $rep->add_field("Title", REP_STRING_TEMPLATE, '{title}', REP_ORDERABLE, '');
        $rep->add_field("First", REP_STRING_TEMPLATE, '{first_name}', REP_ORDERABLE, '');
        $rep->add_field("Last", REP_STRING_TEMPLATE, '{last_name}', REP_ORDERABLE, '');
        $rep->add_field("Firm", REP_STRING_TEMPLATE, '{firm}', REP_ORDERABLE);
        $rep->add_field("Address", REP_STRING_TEMPLATE, '{address}', REP_UNORDERABLE);
        $rep->add_field("City", REP_STRING_TEMPLATE, '{city}', REP_ORDERABLE);
        $rep->add_field("State", REP_STRING_TEMPLATE, '{state}', REP_ORDERABLE);
        $rep->add_field("Zip", REP_STRING_TEMPLATE, '{zip}', REP_ORDERABLE);
        $rep->add_field("Phone", REP_STRING_TEMPLATE, '{phone}', REP_ORDERABLE);
        $rep->add_field("Fax", REP_STRING_TEMPLATE, '{fax}', REP_ORDERABLE);
        $rep->add_field("County", REP_STRING_TEMPLATE, '{county}', REP_ORDERABLE);

        $rep->html_attributes('width="550px"');

        if (count($_POST) < 3) {
            $rep->order_by("last_name", 0);
        }

        $rep->handle_events($_POST);

        echo $rep->get_pagenator_html();
        $data = $rep->make_report_array();
        echo "<table>";
        echo "<tr valign=\"top\"><td>", href_js_action("Status", "order('status')"), "</td><td>First name</nobr></td><td>", href_js_action("Last name", "order('last_name')"), "</td><td>Firm, Address</td><td>Contact</td><td>", href_js_action("County", "order('county')"), "</td></tr>";
        foreach ($data as $line) {
            $line = $line["data"];
            echo "<tr valign=\"top\"><td><b>" . $line["Status"] . "</b></td>";
            echo "<td>" . $line["Title"] . " " . $line["First"] . "</td>";
            echo "<td>", $line["Last"], "</td>";
            echo "<td>";
            if ($line["Firm"]) {
                echo "<b>", $line["Firm"], "</b><br>";
            }
            ;
            echo $line["Address"], "<br>", $line["City"], ", ", $line["State"], " ", $line["Zip"], "</td>";
            echo "<td>Phone: ", $line["Phone"], "<br>Fax: ", $line["Fax"], "</td>";
            echo "<td align=\"center\">", $line["County"], "</td>";
            echo "</tr>";
        }
        echo "</table>";
        echo $rep->get_pagenator_html();
        break;
    // do nothing by default
}

if (!$front_action) {
    // backend actions (only with valid authrization)
    if ($user->have_role(ROLE_CONTENT_MANAGEMENT)) {
        switch ($action) {
            case "list":
                $rep = createobject("report_ajax", array($db, "plg_funeral_director", REP_WITH_PAGENATOR, REP_WITH_HEADING, 50, 'width="600px" style="background-color: #DDEEFF"'));

                $rep->add_field("First", REP_STRING_TEMPLATE, '{first_name}', REP_ORDERABLE);
                $rep->add_field("Last", REP_STRING_TEMPLATE, '{last_name}', REP_ORDERABLE);
                $rep->add_field("Firm", REP_STRING_TEMPLATE, '{firm}', REP_ORDERABLE);
                $rep->add_field("County", REP_STRING_TEMPLATE, '{county}', REP_ORDERABLE, 'align="center"');
                $rep->add_field("", REP_STRING_TEMPLATE, href_js_action("edit", "new FDEditor({director_id})"), REP_UNORDERABLE, 'align="center"');
                $rep->add_field("", REP_STRING_TEMPLATE, href_js_action("delete", "del({director_id})"), REP_UNORDERABLE, 'align="center"');

                $rep->html_attributes('width="600px"');

                if (count($_POST) < 3) {
                    $rep->order_by("last_name", 0);
                }

                $rep->handle_events($_POST);

                echo $rep->make_report();
                break;
            case "load":
                $director_id = intval(@$_POST["id"]);
                $query = "SELECT status, title, first_name, last_name, firm, address, city, state, zip zipcode, phone, fax, county FROM plg_funeral_director WHERE director_id='$director_id'";
                $ret = $db->getRow($query, DB_FETCHMODE_ASSOC);
                header("X-JSON:" . make_json_response($ret));
                break;
            case "save":
                $director_id = intval(@$_POST["id"]);
                $p = array();
                $p["status"] = in(@$_POST["status"]);
                $p["title"] = in(@$_POST["title"]);
                $p["first_name"] = in(@$_POST["first_name"]);
                $p["last_name"] = in(@$_POST["last_name"]);
                $p["firm"] = in(@$_POST["firm"]);
                $p["address"] = in(@$_POST["address"]);
                $p["city"] = in(@$_POST["city"]);
                $p["state"] = in(@$_POST["state"]);
                $p["zip"] = in(@$_POST["zipcode"]);
                $p["phone"] = in(@$_POST["phone"]);
                $p["fax"] = in(@$_POST["fax"]);
                $p["county"] = in(@$_POST["county"]);

                if ($director_id) {
                    $query = "UPDATE plg_funeral_director SET " . make_set_clause($p) . " WHERE director_id='$director_id'";
                }
                else {
                    $query = "INSERT plg_funeral_director SET " . make_set_clause($p);
                }
                $db->query($query);
                break;
            case "delete":
                $director_id = intval(@$_POST["id"]);
                $query = "DELETE FROM plg_funeral_director WHERE director_id='$director_id'";
                $db->query($query);
                break;
            default:
                $smarty->display("funeral_directors.tpl");
                break;
        }
    }
}