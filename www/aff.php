<?php
 // obsolete
// affiliates management
include("inc/verse.inc.php"); //main header - initializes Verse environment

//  $savant = createobject("Savant");

if ($domain->have_feature("affiliates_enabled") && $user->have_role(ROLE_AFFILIATES)) {
    $action = @$_POST["action"];
    switch ($action) {
        case "list":
            error_reporting(E_ALL);
            $rep = createobject("report_ajax", array($db, "cms_affiliate", REP_WITH_PAGENATOR, true, 50, 'width="600px" style="background-color: #DDEEFF"'));
            ;
            $rep->add_table("cms_page", REP_INNER_JOIN, "page_id");

            $rep->add_field("Title", REP_STRING_TEMPLATE, "{title}", REP_ORDERABLE, 'align="left"');
            $rep->add_field("Link", REP_STRING_TEMPLATE, "http://$domain_name/?ref={ref}", REP_UNORDERABLE, 'align="left"');
            $rep->add_field("Entry page", REP_STRING_TEMPLATE, "{internal_name}", REP_UNORDERABLE, 'align="center"');
            $rep->add_field("", REP_STRING_TEMPLATE, href_js_action("edit", "new AffiliateEditor({affiliate_id})"), REP_UNORDERABLE, 'align="center"');
            $rep->add_field("", REP_STRING_TEMPLATE, href_js_action("delete", "delete_affiliate({affiliate_id})"), REP_UNORDERABLE, 'align="center"');

            $rep->add_filter("", REP_INVISIBLE_FILTER, "t1.domain_id", "=", $domain_id);

            $rep->html_attributes('width="600px"');

            if (count($_POST) < 3) {
                $rep->order_by("affiliate_id", 1);
            }
            $rep->handle_events($_POST);

            $html = $rep->make_report();

            echo $html;
            break;
        case "load":
            $affiliate_id = intval(@$_POST["id"]);
            $query = "SELECT ref, title, page_id FROM cms_affiliate WHERE affiliate_id='$affiliate_id'";
            $ret = $db->getRow($query, DB_FETCHMODE_ASSOC);
            header("X-JSON:" . make_json_response($ret));
            break;
        case "save":
            $affiliate_id = intval(@$_POST["id"]);
            $p = array();
            $p["title"] = in(@$_POST["title"]);
            $p["page_id"] = intval(@$_POST["page_id"]);
            $p["ref"] = in(@$_POST["ref"]);
            if ($affiliate_id) {
                $query = "UPDATE cms_affiliate SET " . make_set_clause($p) . " WHERE affiliate_id='$affiliate_id' AND domain_id='$domain_id'";
            }
            else {
                $query = "INSERT cms_affiliate SET " . make_set_clause($p) . ", domain_id='$domain_id'";
            }
            $db->query($query);
            echo $query, "ok";
            break;
        case "delete":
            $affiliate_id = intval(@$_POST["id"]);
            $query = "DELETE FROM cms_affiliate WHERE affiliate_id='$affiliate_id' AND domain_id='$domain_id'";
            $db->query($query);
            break;
        default:
//      $savant->assign("var", "this is test of Savant");
//      $savant->display("affiliates.tpl.php"); 
            $query = "SELECT ecommerce_enabled FROM sms_domain WHERE domain_id='$domain_id'";
            $ecommerce_enabled = $db->getOne($query);
            $smarty->assign("ecommerce_enabled", $ecommerce_enabled);
            // fetch existing pages for ecommerce config dialog
            $query = "SELECT page_id, internal_name FROM cms_page WHERE domain_id='$domain_id' ORDER BY internal_name";
            $pages = $db->getAssoc($query);
            $smarty->assign("pages", $pages);
            $smarty->assign("affiliates_enabled", 1);
            $smarty->display("affiliates.tpl");
    }
}
else {
    header("Location: login.php");
}
