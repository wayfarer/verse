<?php
 // script for transferring session data from remote server
$allowed_ips = array('66.135.56.26', '216.55.169.51');
if (in_array($_SERVER['REMOTE_ADDR'], $allowed_ips)) {
    if (isset($_POST['session_id']) && isset($_POST['session_data'])) {
        session_id($_POST['session_id']);
        session_start();
        $_SESSION = unserialize($_POST['session_data']);
        vlog("session data set for session: " . $_POST['session_id']);
    }
    else {
        vlog("Error! Invalid data passed: " . print_r($_POST, 1));
    }
}
else {
    vlog("Warning! Unauthorized access from " . $_SERVER['REMOTE_ADDR']);
}

function vlog($message) {
    $logfile = "logs/gate_session.log";
    $logline = "[" . date("Y-m-d H:i:s") . "] $message\n";
    file_put_contents($logfile, $logline, FILE_APPEND);
}
