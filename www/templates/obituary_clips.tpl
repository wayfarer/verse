{include file="adm_header.tpl" menu="1"}
{include file="window.tpl"}
{include file="contextmenu.tpl"}
{include file="util.tpl"}

<h1>Obituary Movie Clips</h1>
<div class="submenu"><a href="obituaries.php">&laquo; back to obituaries</a></div>
<div class="actions"><a href="#" onclick="new MovieclipEditor(0); return false;">new movieclip</a></div>
<div id="movieclips"><img src="img/loader-big.gif"></div>
<script type="text/javascript">
{include file="populate_controls.tpl"}
{literal}
	var MovieclipEditor = Class.create();
	MovieclipEditor.prototype = {
		initialize: function(movieclip_id) {
			this.movieclip_id = movieclip_id;
			// create dialog fields in form
      // iframe ajax-style upload method
			var f = '\
					<form target="upload_iframe" method="post" enctype="multipart/form-data">\
					<label for="title">Title</label><input type="text" id="title" name="title" class="txt2"><br>\
  				<label for="movieclip">Video File</label><input type="file" id="movieclip" name="movieclip"><br>\
  				<label for="protected">Protected</label><input type="checkbox" id="protected" name="protected" class="chk"><br>\
  				<label for="password">Password</label><input type="text" id="password" name="password" class="txt"><br>\
          <input type="hidden" name="action" value="save">\
          <input type="hidden" name="movieclip_id" value="'+movieclip_id+'">\
          <div class="alphacube_buttons">\
          <input type="submit" class="ok_button" value="Ok">\
          <input type="button" class="cancel_button" onclick="Dialog.cancelCallback()" value="Cancel">\
          </div>\
					</form>\
          <iframe name="upload_iframe" style="display:none">\
				';
			this.wnd = Dialog.confirm(f, {windowParameters: {className:"alphacube", width:520, title:"Movieclip", showEffect: Element.show, hideEffect: Element.hide},
  								      ok: this.onok.bind(this) });
      // for iframe to access current window for closing
      document.dialog_window = this;
			this.form = this.wnd.getContent().getElementsByTagName("FORM")[0];
  		this.form.onsubmit = this.onsubmit.bind(this);

			Form.focusFirstElement(this.form);

			if(movieclip_id) {
				// load data
				new Ajax.Request('obituary_clips.php', {parameters:'action=load&id='+movieclip_id, onSuccess:this.onload.bind(this)}); // TODO: handle AJAX errors here
			}
			else {
				// defaults
				this.wnd.hideloader();
			}
		},

		onload: function(t, json) {
			jslog.debug(t.getResponseHeader("X-JSON"));
			populate_controls(this.form, json);
			this.wnd.hideloader();
		},

		onsubmit: function() {
			this.onok();
			return true; // UNUSUAL: continue submitting
		},

		onsave: function() {
			update_movieclips();
			this.wnd.hide();
		},
		
		onok: function() {
			this.wnd.showloader();
		}
	}

	function movieclip_deleted(t)
	{
		update_movieclips();
	}
	
	function delete_movieclip(movieclip_id)
	{
		if(confirm("Are you sure you want to delete movieclip?")) {
			new Ajax.Request('obituary_clips.php', {parameters:'action=delete&id='+movieclip_id, onSuccess:movieclip_deleted});
		}
	}
{/literal}	
	function page(start)
	{ldelim}
		new Ajax.Updater('movieclips', 'obituary_clips.php', {ldelim}parameters: "action=list&obituary_id={$obituary_id}&start="+start});  // add AJAX error handling here
	}
	function order(field)
	{ldelim}
		new Ajax.Updater('movieclips', 'obituary_clips.php', {ldelim}parameters: "action=list&obituary_id={$obituary_id}&order="+field});  // add AJAX error handling here
	}
	function update_movieclips()
	{ldelim}
		new Ajax.Updater('movieclips', 'obituary_clips.php', {ldelim}parameters: "action=list&obituary_id={$obituary_id}"});  // add AJAX error handling here
	}	
	update_movieclips();
</script>	

{include file="adm_footer.tpl"}
