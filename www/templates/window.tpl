<style type="text/css">@import url(css/window.css);</style>
<script type="text/javascript" src="js/window.js"></script>	
{literal}
<script>
Window.prototype.initialize2 = Window.prototype.initialize;
Window.prototype.initialize = function() {
	arguments[1]["title"] = "<img src=\"img/loader.gif\" align=\"left\">"+arguments[1]["title"];
	this.initialize2(arguments[0], arguments[1]);
}

Window.prototype.hideloader = function() {
	Element.setStyle(this.topbar.getElementsByTagName("IMG")[0], {visibility: "hidden"});
};

Window.prototype.showloader = function() {
	Element.setStyle(this.topbar.getElementsByTagName("IMG")[0], {visibility: "visible"});
};
Dialog.confirm2 = Dialog.confirm;
Dialog.confirm = function(content, params) {
    if(!params.windowParameters) {
    	params = {windowParameters: {className:"alphacube", width:params.width||null, title:params.title||null, showEffect: Element.show, hideEffect: Element.hide}, ok: params.ok||null }
    }
	return Dialog.confirm2(content, params);
}
</script>
{/literal}
