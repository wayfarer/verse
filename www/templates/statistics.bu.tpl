{include file="adm_header.tpl" menu="1"}
<h1>Website statistics</h1>
{if $token_auth}
<object style="visibility: visible;width:500px" id="VisitsSummarygetEvolutionGraphChart_swf" data="http://twintierstech.net/piwik/libs/open-flash-chart/open-flash-chart.swf?piwik=0.5.4" bgcolor="#FFFFFF" type="application/x-shockwave-flash" height="150" width="100%">
<param value="always" name="allowScriptAccess">
<param value="transparent" name="wmode">
{capture name=datafile}
http://twintierstech.net/piwik/index.php?module=VisitsSummary&action=getEvolutionGraph&columns[]=nb_visits&idSite=1&period=day&date=2009-12-17,2010-01-15&viewDataTable=generateDataChartEvolution&id=VisitsSummarygetEvolutionGraphChart_swf&token_auth={$token_auth}
{/capture}
<param value="data-file={$smarty.capture.datafile|urlencode}" name="flashvars">
</object>
{/if}
<div id="widgetIframe">
<iframe src="https://twintierstech.net/piwik/index.php?module=Widgetize&action=iframe&moduleToWidgetize=Dashboard&actionToWidgetize=index&idSite=1&period=week&date=yesterday&token_auth={$token_auth}" frameborder="0" marginheight="0" marginwidth="0" width="100%" height="100%"></iframe>
</div>
<h2><a href="phpmv_login.php">phpMyVisites (old statistics module)</a></h2>
<h2><a href="piwik_login.php">Piwik (new statistics module)</a></h2>
