{include file="adm_header.tpl" menu="1"}
{include file="window.tpl"}
{include file="contextmenu.tpl"}
{include file="util.tpl"}

<h1>Ecommerce :: Orders</h1>
<div class="submenu"><a href="products.php">Products</a> | <a href="products_categories.php">Categories</a>{if $ecommerce_enabled} | <a href="products_orders.php">Orders</a>{/if}{if $affiliates_enabled} | <a href="aff.php">Affiliates</a>{/if}</div>
<div id="orders"><img src="img/loader-big.gif"></div>
<script type="text/javascript">
{literal}	
	var OrderView = Class.create();
	OrderView.prototype = {
		initialize: function(order_id) {
			this.order_id = order_id;
			var f = '<div class="info"></div>';
			this.wnd = Dialog.alert(f, {windowParameters: {className:"alphacube", width:520, height: 450, title:"Order review", showEffect: Element.show, hideEffect: Element.hide} });
			this.info = this.wnd.getContent().getElementsByTagName("DIV")[0];

			if(order_id) {
				// load data
				this.json = null;
				new Ajax.Request('ajax_products.php', {parameters:'action=load_order&id='+order_id, onSuccess:this.onload.bind(this)}); // TODO: handle AJAX errors here
			}
			else {
				// defaults
				this.wnd.hideloader();
			}
		},

		onload: function(t, json) {
			jslog.debug(t.getResponseHeader("X-JSON"));
			this.info.innerHTML = t.responseText;
			this.wnd.hideloader();
		}
	}

	function page(start)
	{
		new Ajax.Updater('orders', 'ajax_products.php', {parameters: "action=list_orders&start="+start});
	}
	function order(field)
	{
		new Ajax.Updater('orders', 'ajax_products.php', {parameters: "action=list_orders&order="+field});
	}
	function update_orders()
	{
		new Ajax.Updater('orders', 'ajax_products.php', {parameters: "action=list_orders"});
	}	
	update_orders();
</script>	
{/literal}

{include file="adm_footer.tpl"}
