<table border="0" cellpadding="0" cellspacing="0" class="events_calendar" style="border-collapse: collapse">
{foreach from=$data item=item key=day}
    {if $weekday>0}
    	{assign var="advance" value="0"}
    {else}
    	{assign var="advance" value="1"}
    {/if}
	<tr class="{cycle values="even,odd" advance=$advance}" valign="top"><td width="5%" align="center"><b>{$day}</b></td><td width="15%">{$item.day_name}</td><td>{$item.content}</td></tr>
	{if $weekday++ > 5}
		{assign var="weekday" value="0"}
	{/if}
{/foreach}
</table>
