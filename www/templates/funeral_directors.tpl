{include file="adm_header.tpl" menu="1"}
{include file="window.tpl"}
{include file="util.tpl"}

<h1>Funeral Directors</h1>
<div class="actions"><a href="#" onclick="new FDEditor(0); return false;">new funeral director</a></div>
<div id="fd"><img src="img/loader-big.gif"></div>
<script type="text/javascript">
{include file="populate_controls.tpl"}
{literal}
	var FDEditor = Class.create();
	FDEditor.prototype = {
		initialize: function(fd_id) {
			this.fd_id = fd_id;
			// create form
			var f = '\
					<form>\
					<label for="status">Status</label><input type="text" id="status" name="status" class="small"><br>\
					<label for="title">Title</label><input type="text" id="title" name="title" class="small"><br>\
					<label for="first_name">First Name</label><input type="text" id="first_name" name="first_name" class="txt"><br>\
					<label for="last_name">Last Name</label><input type="text" id="last_name" name="last_name" class="txt"><br>\
					<label for="firm">Firm</label><input type="text" id="firm" name="firm" class="txt2"><br>\
					<label for="address">Address</label><input type="text" id="address" name="address" class="txt2"><br>\
					<label for="city">City</label><input type="text" id="city" name="city" class="txt2"><br>\
					<label for="state">State</label><input type="text" id="state" name="state" class="small"><br>\
					<label for="zip">ZIP</label><input type="text" id="zip" name="zipcode" class="small"><br>\
					<label for="phone">Phone</label><input type="text" id="phone" name="phone" class="phone"><br>\
					<label for="fax">Fax</label><input type="text" id="fax" name="fax" class="phone"><br>\
					<label for="county">County</label><input type="text" id="county" name="county" class="small"><br>\
					</form>\
				';
			this.wnd = Dialog.confirm(f, {windowParameters: {className:"alphacube", width:500, height:280, title:"Edit Funeral Director", showEffect: Element.show, hideEffect: Element.hide},
									      ok: this.onok.bind(this) });
			this.form = this.wnd.getContent().getElementsByTagName("FORM")[0];
			
			Form.focusFirstElement(this.form);
			if(fd_id) {
				// load data
				new Ajax.Request('funeral_directors.php', {parameters:'action=load&id='+fd_id, onSuccess:this.onload.bind(this)});
			}
			else {
				this.wnd.hideloader();
			}
		},

		onload: function(t, json) {
			jslog.debug(t.getResponseHeader("X-JSON"));
			populate_controls(this.form, json);
			this.wnd.hideloader();
		},

		onsave: function(t) {
			jslog.debug(t.responseText);
			this.wnd.hide();
		},

		onok: function() {
			this.wnd.showloader();
			new Ajax.Request('funeral_directors.php', {parameters:'action=save&id='+this.fd_id+"&"+Form.serialize(this.form), onSuccess:this.onsave.bind(this)});
		}
	};

	function fd_changed(t) {
		jslog.debug(t.responseText);
		update_fd();
	}	
	function del(fd_id) {
		if(confirm("Are you sure you want to delete Funeral Director item?")) {
			new Ajax.Request('funeral_directors.php', {parameters:'action=delete&id='+fd_id, onSuccess:fd_changed});
		}
	}
{/literal}	
	function page(start) {ldelim}
		new Ajax.Updater('fd', 'funeral_directors.php', {ldelim}parameters: "action=list&start="+start}); 
	}
	function order(field) {ldelim}
		new Ajax.Updater('fd', 'funeral_directors.php', {ldelim}parameters: "action=list&order="+field}); 
	}
	function update_fd() {ldelim}
		new Ajax.Updater('fd', 'funeral_directors.php', {ldelim}parameters: "action=list"});
	}	
	update_fd();
</script>	

{include file="adm_footer.tpl"}
