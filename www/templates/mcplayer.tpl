<html>
<head>
<title>{$movieclip.title}</title>
</head>
<body style="margin:0">
{if $movieclip.allowed}
<div id="movie"></div>
<script type="text/javascript" src="js/swfobject.js"></script>
<script type="text/javascript">
//<![CDATA[
var so = new SWFObject('tools/mcplayer.swf','player',"{$movieclip.width|default:460}","{$movieclip.height|default:375}","9");
        so.addVariable('movieURL','/{$movieclip.domain_name}/{$movieclip.filename|escape:"url"}');
        so.addVariable('movieTitle','{$movieclip.title|escape:"quotes"}');
        so.addVariable('movieAuthor','{$movieclip.name|default:' '|escape:"quotes"}');
        {if !$movieclip.nodate}
        so.addVariable('movieDate','{$movieclip.timestamp|urlencode}');
        {else}
        so.addVariable('movieDate',' ');
        {/if}
        so.addParam('allowFullScreen', 'true');
        so.addParam('wmode', 'transparent');
        so.write('movie');
//]]>
</script>
{else}
  <center>
  <div class="message">
    <h2>This movieclip is password protected</h2>
    Please, enter the password to see it.
  </div>
  {if $movieclip.authfail}
    <div class="error">Password incorrect</div>
  {/if}
  <br><br>
  <form method="post">
    <input type="password" name="mcpass">
    <input type="submit" value="Ok">
  </form>
  </center>
{/if}
</body>
</html>