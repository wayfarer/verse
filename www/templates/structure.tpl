{include file="adm_header.tpl" menu="1"}
{include file="window.tpl"}
{include file="contextmenu.tpl"}
{include file="util.tpl"}

{literal}
<style>
    #ace-header-html, #ace-footer, #ace-css, #ace-content1, #ace-content2 {
        overflow:  hidden;
        position: relative;
        text-align: left;
    }

    #ace-footer, #ace-css {
        width: 100%;
    }

    #content_pages .page {
        float: left;
        margin-right: 10px;
    }

	#left {
		float: left;
		border-right: 1px solid;
		margin-right: 10px;
		width: 426px !important;
		width: 427px;
	}

	#right {
		float: left;
		border-right: 1px solid;
		margin-right: 10px;
	}

	#right2 {
		float: left;
	}

	div.structure {
		clear: both;
	}

	div.structure a {
		text-decoration: none;
	}

	div.structure a:hover {
		color: #FF0000;
	}

	div.node {
		clear: both;
		height: 20px;
/*		border: 1px solid #EDFAFF; */
		border: 1px solid #BDCACC;
		margin-bottom: 1px;
		margin-right: 5px;
		overflow: hidden;
	}

	div.node_accept {
		border: 1px solid #0000FF;
	}

	div.page_accept {
		border: 1px solid #FF0000;
	}

	div.node_add {
		font-size: 12px;
		padding-top: 3px;
		padding-left: 3px;
		float: left;
		width: 11px !important;
		width: 13px;
	}

	div.node_depth {
		float: left;
		width: 35px;
		background-color: #DDEAEF;
		padding: 0 2px;
	}

	div.node_name {
		float: left;
		width: 200px;
		margin: 0 0 0 10px;
	}

	div.node_content {
		position: absolute;
		left: 277px !important;
		left: 276px;
		width: 120px;
		border-left: 1px solid #BDCACC;
	}

	div.node_content span {
		padding-left: 3px;
	}

	div.node_controls {
		position: absolute;
		left: 380px;
		padding: 0 3px;
		background-color: #DDEAEF;
	}

	div.page {
		background-color: #DDEAEF;
		border-top: 1px solid #BCCACF;
		border-bottom: 1px solid #BCCACF;
		width: 110px !important;
		width: 120px;
		padding: 0 5px;
		cursor: default;
		margin: 1px 0;
		overflow: hidden;
	}

    div.page-1 {
        background-color: #FFF8C5;
    }

    div.page-2 {
        background-color: #C7E792;
    }

    div.page-3 {
        background-color: #AFAEF6;
    }
    
    div.page-4 {
        background-color: #EB9EA6;
    }

    div.page-5 {
        background-color: #8B97A7;
    }

	div.node_delete {
		border-left: 1px solid #BDCACC;
		position: absolute;
		left: 397px;
		font-size: 12px;
		padding-top: 3px;
		padding-left: 4px;
	}

	div.structure_controls {
		margin-top: 7px;
		margin-bottom: 7px;
	}
	label.inline {
		display: inline;
		float: none;
	}

    .rewrite-properties {
        font-weight: bold;
        margin-bottom: 10px;
    }
</style>
{/literal}

<div id="left">
<h1>Site structure <small>(<a href="index.php" target="_blank">preview</a>) 
{if $uc_mode}
<a href="structure_uc.php">UC pages &raquo;</a><br>
<a href="http://dev.{$smarty.session.domain_name}/index.php" target="_blank">preview release</a>
{/if}
</small></h1>
<div class="actions"><a href="#" onclick="new NodeDialog(null, 1); return false;">new node</a></div>
<form id="structure_controls">
<div class="structure_controls">
<span class="preloader" style="float: right; margin-right: 7px"><img src="img/loader.gif" width="16" height="16"></span>
<input type="button" value="normalize" onclick="structure.normalize_tree()" title="used to normalize the site structure in way it has tree structure">
<input type="button" value="save" onclick="structure.save()" title="used to save the site structure after node order or node depth have changed">
<input type="button" value="revert" onclick="structure.initialize()" title="used to cancel changes of the site structure">
</div>
<div id="structure" class="structure"></div>
<div class="structure_controls">
<input type="button" value="normalize" onclick="structure.normalize_tree()" title="used to normalize the site structure in way it has tree structure">
<input type="button" value="save" onclick="structure.save()" title="used to save the site structure after node order or node depth have changed">
<input type="button" value="revert" onclick="structure.initialize()" title="used to cancel changes of the site structure">
</div>
</form>
</div>

<div id="right">
	<h1>Content pages</h1>
    <span class="preloader" style="float: right; margin-right: 7px"><img src="img/loader.gif" width="16" height="16"></span>
	<div class="actions"><a href="#" onclick="new ContentPropsDialog(0, 0); return false;">new page</a> | <a href="history.php">history</a></div>
	<div id="content_pages"></div>
</div>

<div id="right2">
	<h1>Site Properties</h1>
    {$overwrite_properties_message}
	Site properties
	<ul>
		<li><a href="#" onclick="new GeneralPropsDialog(); return false">general</a></li>
		<li><a href="#" onclick="new HeaderDialog(); return false">header</a> (<a href="#" onclick="new HeaderWysiwygDialog(); return false">wysiwyg</a>)</li>
		<li><a href="#" onclick="new FooterDialog(); return false">footer</a> (<a href="#" onclick="new FooterWysiwygDialog(); return false">wysiwyg</a>)</li>
		<li><a href="#" onclick="new CSSDialog(); return false">css</a></li>
        <li><a href="/backend.php/google_stats">google analytics</a></li>
	</ul>
	Menu styles
	<ul>
		<li><a href="#" onclick="new MainMenuDialog(); return false">main menu</a></li>
		<li><a href="#" onclick="new PopupMenuDialog(); return false">popup menu</a></li>
	</ul>
	Obituaries
	<ul>
		<li><a href="#" onclick="new ObituariesConfig(); return false">config</a></li>
  	<li><a href="#" onclick="new ObituaryTheme(); return false">theme</a></li>
  	<li><a href="#" onclick="new MusicDialog(); return false">music</a></li>
	</ul>
</div>

{if !$is_ie }
    <script type="text/javascript" src="js/nicEdit.js"></script>
    <script type="text/javascript" src="js/ace/ace.js" charset="utf-8"></script>
    <script type="text/javascript" src="js/ace/mode-css.js" charset="utf-8"></script>
    <script type="text/javascript" src="js/ace/mode-html.js" charset="utf-8"></script>
    <script type="text/javascript" src="js/ace/theme-crimson_editor.js" charset="utf-8"></script>
    <script type="text/javascript" src="js/cookies.js" charset="utf-8"></script>
{/if}
<script type="text/javascript">
	var node_counter = 1;
    var domain_id = {$smarty.session.domain_id};
    if(navigator.userAgent.indexOf("MSIE") == -1)
        var is_ie = false;
    else
        var is_ie = true;
{include file="populate_controls.tpl"}
{literal}
	var NodeDialog = Class.create();
	NodeDialog.prototype = {
		initialize: function(node, create) {
			// create window
//			this.wnd = new WindowClass("Node/Menu Item properties", 400, 300, [this.node_onok.bind(this), this.node_oncancel.bind(this)]);
			this.node = node;
			this.create = create;
			// create form
			// create dialog fields in window
			var f = '<form>\
					<label for="display_name">Title</label><input type="text" id="display_name" name="display_name" class="txt2"><br>\
					<label for="menu_type">Menu Item type</label><input type="radio" name="menu_type" id="menu_type" value="1">Text<input type="radio" name="menu_type" value="2">Static image<input type="radio" name="menu_type" value="3">Rollover image<br>\
					<label for="image">Item image</label><input type="text" id="image" name="image" class="file"> <input type="button" name="browse_i" value="browse"><br>\
					<label for="himage">Rollover image</label><input type="text" id="himage" name="himage" class="file"> <input type="button" name="browse_hi" value="browse"><br>\
					<label for="width">Width</label><input type="text" id="width" name="width" class="txt"><br>\
					<label for="height">Height</label><input type="text" id="height" name="height" class="txt"><br>\
					<label for="newwindow">Open in new window</label><input type="checkbox" id="newwindow" name="newwindow" value="1"><br>\
          </form>\
				';
			this.wnd = Dialog.confirm(f, {windowParameters: {className:"alphacube", width:450, title:"Node/Menu Item properties", showEffect: Element.show, hideEffect: Element.hide},
									      ok: this.onok.bind(this) });
			//alert();
			//alert(this.wnd.getContent().findChildElements);
			//this.form = this.wnd.getContent().getElementsByTagName("FORM")[0];
			this.form = Selector.findChildElements(this.wnd.getContent(), ["form"])[0];
			this.form.onsubmit = this.onsubmit.bind(this);

			// create and bind filebrowsers
			this.sfb = new ServerFileBrowser(this.onimageselect.bind(this), "Images:/", "Images");
			this.form.elements["browse_i"].onclick=this.sfb.open.bind(this.sfb);

			this.sfb2 = new ServerFileBrowser(this.onhoverimageselect.bind(this), "Images:/", "Images");
			this.form.elements["browse_hi"].onclick=this.sfb.open.bind(this.sfb2);

			Form.focusFirstElement(this.form);
			if(node && !create) {
//				new Ajax.Request('ajax_structure.php', {parameters:'action=load_node&id='+node_id, onSuccess:this.node_onload.bind(this)}); // TODO: handle AJAX errors here
				// read node info from given node
				this.form.elements["display_name"].value=node.getAttribute("title");
				this.form.elements["menu_type"][node.getAttribute("menu_type")-1].checked = true;
				this.form.elements["image"].value=node.getAttribute("image");
				this.form.elements["himage"].value=node.getAttribute("himage");
				this.form.elements["width"].value=node.getAttribute("width");
				this.form.elements["height"].value=node.getAttribute("height");
        this.form.elements["newwindow"].checked=node.getAttribute("newwindow")=="1"?true:false;
				this.wnd.hideloader();
			}
			else {
				// defaults for new node
				this.form.elements["display_name"].value="node"+node_counter++;
				this.form.elements["menu_type"][1].checked = true;
				this.form.elements["width"].value="auto";
				this.form.elements["height"].value="auto";
				this.wnd.hideloader();
			}
		},

		onimageselect: function(url) {
			this.form.elements["image"].value = url;
		},

		onhoverimageselect: function(url) {
			this.form.elements["himage"].value = url;
		},
		
		onsubmit: function() {
			this.onok();
			return false;
		},

		onok: function() {
			// store data to node
			var node;
			if(this.create) {
				node = create_node();
				// uniq ids required to sortable work correctly
				node.setAttribute("id", "id_"+parseInt(Math.random()*100000));
				node.setAttribute("node_id", 0);
                node.setAttribute("depth", 0);//escape "null"
			}
			else {
				node = this.node;
			}

			// set offset as clicked node has, if it was
			if(this.node) {
				var depth = this.node.getAttribute("depth");
				node.setAttribute("depth", depth);
				structure.set_padding(node, depth);
			}
			node.setAttribute("title", this.form.elements["display_name"].value);
			for(var i=0; i<this.form.elements["menu_type"].length; i++) {
				if(this.form.elements["menu_type"][i].checked) {
					node.setAttribute("menu_type", i+1);
					break;
				}
			}
			node.setAttribute("image", this.form.elements["image"].value);
			node.setAttribute("himage", this.form.elements["himage"].value);
			node.setAttribute("width", this.form.elements["width"].value);
			node.setAttribute("height", this.form.elements["height"].value);
      node.setAttribute("newwindow", this.form.elements["newwindow"].checked?1:0);
			// set visible part
//			var node_name = document.getElementsByClassName("node_name", node);
      var node_name = $(node).childElements();
			node_name[2].innerHTML = this.form.elements["display_name"].value;

			if(this.create) {
				var where = null;
				if(this.node) {
					where = this.node.nextSibling;
					if(where && where.nodeName=="#text") where = where.nextSibling;
				}
				var struct = $("structure");
				struct.insertBefore(node, where);
				Sortable.create(struct, {tag: 'div', constraint: false, onUpdate: structure.onnodeorder.bind(structure)});
				// parse all elements and make droppable (sortable.destroy destroys prev)
				$A(struct.childNodes).each( function(node) {
			    	if(node.nodeName=="DIV") {
			    		Droppables.add(node, {accept: 'page', hoverclass: 'page_accept', onDrop: structure.onpagedrop.bind(structure)});
			    	}
				} );

			}
			Form.enable("structure_controls");
			//this.wnd.hide();
		}
	};

	function create_node() {
		var el = Builder.node('div', {className:'node'}, 
					[Builder.node('div', {className:'node_add'}, [Builder.node('a', {href:'#', onclick:'new NodeDialog(this.parentNode.parentNode, 1); return false;'}, '+')] ),
					 Builder.node('div', {className:'node_depth'}, [Builder.node('a', {href:'#', onclick:'structure.move_right(this);return false'}, '>'),Builder.node('a', {href:'#', onclick:'structure.move_left(this);return false'}, '<')]),
					 Builder.node('div', {className:'node_name'}),
					 Builder.node('div', {className:'node_content'}, [ Builder.node('span', {}, 'empty') ] ),
					 Builder.node('div', {className:'node_delete'}, [Builder.node('a', {href:'#', onclick:'structure.remove_node(this.parentNode.parentNode);return false'}, 'X')])
					]
				);
		return el;
	}

	var SiteStructure = Class.create();
	SiteStructure.prototype = {
		initialize: function(container) {
			Form.disable("structure_controls");
			if(container) {
				this.container = container;
				// this.loader = document.getElementsByClassName("preloader", $("structure_controls"))[0];
				this.loader = $$("#structure_controls .preloader")[0];
			}
			this.showloader();
			new Ajax.Request('ajax_structure.php', {parameters: "action=list", onComplete: this.onload.bind(this)});  // add AJAX error handling here
		},

		onload: function(t) {
//			jslog.debug(t.responseText);
			this.container.innerHTML = "";
			var struct = eval(t.responseText);
			var self = this;
            var is_shared_pages = {/literal}{$smarty.session.is_shared_pages}{literal};
            var classes = {/literal}{$smarty.session.classes|@json_encode}{literal};
			$H(struct).each(
				function(node) {
					// create node
                    var el = Builder.node('div', {className:'node', id:'id_'+node[1].node_id, node_id:node[1].node_id, depth:node[1].depth, title:node[1].title, image:node[1].image?node[1].image:"", himage:node[1].himage?node[1].himage:"", menu_type:node[1].menu_type?node[1].menu_type:2, width:node[1].width?node[1].width:"", height:node[1].height?node[1].height:"", newwindow: node[1].newwindow=="1"?1:0},
								[Builder.node('div', {className:'node_add'}, [Builder.node('a', {href:'#', onclick:'new NodeDialog(this.parentNode.parentNode, 1); return false;'}, '+')] ),
								 Builder.node('div', {className:'node_depth'}, [Builder.node('a', {href:'#', onclick:'structure.move_right(this);return false'}, '>'),Builder.node('a', {href:'#', onclick:'structure.move_left(this);return false'}, '<')]),
								 Builder.node('div', {className:'node_name', style:'padding-left:'+(node[1].depth*20)+'px'}, node[1].title?node[1].title:"notitle"),
								 Builder.node('div', {className:'node_content'}, [
								 	(node[1].page_id!=0)?Builder.node('div', {className:(is_shared_pages!=1)?'page':'page'+classes[node[1].domain_id], page_id:node[1].page_id, domain_id:node[1].domain_id, style:'position: relative; top:-2px'}, node[1].page_name):Builder.node('span', {}, 'empty') ] ),
								 Builder.node('div', {className:'node_delete'}, [Builder.node('a', {href:'#', onclick:'structure.remove_node(this.parentNode.parentNode);return false'}, 'X')])
								]
							);
					self.container.appendChild(el);
					Droppables.add(el, {accept: 'page', hoverclass: 'page_accept', onDrop: self.onpagedrop.bind(self)});
				}
			);

			// ought to use "self", cause inside each "this" pointer has other context
//			var self = this;
//			$A(this.container.childNodes).each(
//				function(node) {
//			    	if(node.nodeName=="DIV") {
//			    		self.set_padding(node, node.getAttribute("depth"));
//			    		// add drop area for page
//			    		Droppables.add(node, {accept: 'page', hoverclass: 'page_accept', onDrop: self.onpagedrop.bind(self)});
//			    	}
//			    }
//			);
			Sortable.create(this.container, {tag: 'div', constraint: false, onUpdate: this.onnodeorder.bind(this)});
			this.hideloader();
		},

		hideloader: function() {
			this.loader.style.display = "none";
		},

		showloader: function() {
			this.loader.style.display = "block";
		},

		onnodeorder: function() {			
			Form.enable("structure_controls");
		},

		onsave: function(t) {
			jslog.debug(t.responseText);
			this.initialize();
		},

		save: function() {
			Form.disable("structure_controls");
			this.showloader();
			var param_str = this.serialize();
			obj = new Ajax.Request('ajax_structure.php', {parameters:'action=save&data='+escape(param_str), onSuccess:this.onsave.bind(this)});
		},

    	serialize: function() {
	    	var ret = "";
			$A($("structure").childNodes).each(
				function(node) {
			    	if(node.nodeName=="DIV") {
				    	ret+=node.getAttribute("node_id")+"|"+node.getAttribute("depth")+"|"+
				    	     node.getAttribute("title")+"|"+node.getAttribute("menu_type")+"|"+
				    	     node.getAttribute("image")+"|"+node.getAttribute("himage")+"|"+
				    	     node.getAttribute("width")+"|"+node.getAttribute("height")+"|"+
                   node.getAttribute("newwindow")+"\n";
			    	}
				}
			);
			return ret;
	    },

    	normalize_tree: function() {
    		var self = this;
			var avail_depth = 0;
			$A(this.container.childNodes).each(
				function(node) {
			    	if(node.nodeName=="DIV") {
			    		var node_depth = node.getAttribute("depth");
			    		if(node_depth>avail_depth) {
			    			node_depth = avail_depth;
			    			node.setAttribute("depth", node_depth);
			    			self.set_padding(node, node_depth);
			    		}
			    		avail_depth = Number(node_depth)+1;
			    	}
				}
			);
	    },

	    updated: function() {
	    	this.hideloader();
	    },
	    
	    remove_node: function(node_elem) {
			if(confirm("Are you sure you want to delete node?")) {
				var node_id = node_elem.getAttribute("node_id");
				if(node_id>0) {
					this.showloader();
					// request to delete node on server
					new Ajax.Request('ajax_structure.php', {parameters:'action=delete_node&id='+node_id, onComplete: this.updated.bind(this)});
				}
				$("structure").removeChild(node_elem);
			}
	    },

		move_up: function(elem) {
			var node_elem = elem.parentNode.parentNode;
			var prev = node_elem.previousSibling;
			if(prev && prev.nodeName=="#text") prev = prev.previousSibling;
			if(prev) this.container.insertBefore(node_elem, prev);
		},

		move_down: function(elem) {
			var node_elem = elem.parentNode.parentNode;
			var next = node_elem.nextSibling;
			if(next && next.nodeName=="#text") next = next.nextSibling;
			if(next) this.container.insertBefore(node_elem, next.nextSibling);
		},

		set_padding: function(elem, depth) {
      //var pad_elem = document.getElementsByClassName("node_name", elem);
      var pad_elem = $(elem).childElements();
			pad_elem[2].style.paddingLeft = (depth*20)+"px";
		},
	
		move_right: function(elem) {
			var node_elem = elem.parentNode.parentNode;
			var depth = node_elem.getAttribute("depth");
			depth++;
			node_elem.setAttribute("depth", depth);
			this.set_padding(node_elem, depth);
			Form.enable("structure_controls");
		},

		move_left: function(elem) {
			var node_elem = elem.parentNode.parentNode;
			var depth = node_elem.getAttribute("depth");
			if(depth>0) depth--;
			node_elem.setAttribute("depth", depth);
			this.set_padding(node_elem, depth);
			Form.enable("structure_controls");
		},
		
		onpagedrop: function(src, tg) {
			var node_id = tg.getAttribute("node_id");
			if(node_id>0) {
				this.showloader();
				var page_id = src.getAttribute("page_id"),
                    domain_id = src.getAttribute("domain_id");
				// store assoc to DB
				new Ajax.Request('ajax_structure.php', {parameters:'action=set_node_content&node_id='+node_id+'&page_id='+page_id, onComplete: this.content_set.bind(this)});
				// make visual changes
//				var cnt_elem = document.getElementsByClassName("node_content", tg)[0];
				var cnt_elem = $(tg).childElements()[3];
				var clone = Builder.node("DIV", {className:src.getAttribute("class")}, src.innerHTML);
				clone.style.position = "relative";
				clone.style.top = "-2px";
                clone.setAttribute('page_id', page_id);
                clone.setAttribute('domain_id', domain_id);
				cnt_elem.innerHTML = "";
				cnt_elem.appendChild(clone);
			}
			else {
				setTimeout('alert("Please, save Structure Tree before assigning content to newly created nodes")', 10);
			}
		},

		content_set: function(t) {
			this.hideloader();
			jslog.debug(t.responseText);
		},

		clear_node_content: function(node) {
			// clear assoc in DB
			this.showloader();
			new Ajax.Request('ajax_structure.php', {parameters:'action=set_node_content&node_id='+node.getAttribute("node_id")+'&page_id=0', onComplete: this.updated.bind(this)});
			// make visual changes
			$(node).childElements()[3].innerHTML = "<span>empty</span>";
		}
	};

	function page_ondrop_revert(elem) 
	{
		Position.absolutize(elem);
		elem.style.left = elem.offsetOrig[0];
		elem.style.top = elem.offsetOrig[1];
		Position.relativize(elem);
		return false;
	}
	
    function onpagesorder()
    {
		var order="";
		$A($("content_pages").childNodes).each(
			function(page) {
		    	if(page.nodeName=="DIV") {
		    		order+=page.getAttribute("page_id")+";";
		    	}
		    }
		);
		// save order
		new Ajax.Request('ajax_structure.php', {parameters: "action=update_pages_ord&ord="+order});
	}

	function content_pages_onload()
	{
/*		$A($("content_pages").childNodes).each(
			function(page) {
		    	if(page.nodeName=="DIV") {
					page.offsetOrig = Position.cumulativeOffset(page);
					new Draggable(page, {ghosting:true, revert:page_ondrop_revert});
		    	}
		    }
		); */
        resize_content_pages();
		Sortable.create($("content_pages"), {tag: 'div', constraint: false, onUpdate: onpagesorder});
		//document.getElementsByClassName("preloader", $("right"))[0].style.display = "none";
		$$("#right .preloader")[0].style.display = "none";
	}

    function resize_content_pages() {
        var sumHeight = 0;
        $("content_pages").setStyle({width: '150px'});
        $A($$("#content_pages .page")).each(
            function(page) {
                if (sumHeight >= 500) {
                    var new_width = $("content_pages").getWidth() + page.getWidth() + parseInt(page.getStyle('margin-right'));
                    $("content_pages").setStyle({width: new_width+'px'});
                    sumHeight = 0;
                }
                sumHeight += page.getHeight() + parseInt(page.getStyle('margin-top')) + parseInt(page.getStyle('margin-bottom'))
                            + parseInt(page.getStyle('border-top-width')) + parseInt(page.getStyle('border-bottom-width'));
            }
        );
    }

	function window_oncancel(wnd)
	{
		wnd.close();
	}

	function ajax_onerror(t)
	{
	    alert('Error ' + t.status + ' -- ' + t.statusText);
	}


	var content_page_counter = 1;
	var ContentPropsDialog = Class.create();
	ContentPropsDialog.prototype = {
		initialize: function(page_id, page_domain_id) {
			this.page_id = page_id;
            this.page_domain_id = page_domain_id;
{/literal}
{capture name=page_type_opts}
	{html_options options=$page_types}
{/capture}
			var f = '<form>\
					<label for="int_name">Internal name</label><input type="text" id="int_name" name="int_name" class="txt"><br>\
			  	<label for="page_type">Page type</label><select id="page_type" name="page_type">{$smarty.capture.page_type_opts|strip}</select><br>\
					<label for="no_header">No headers</label><input type="checkbox" id="no_header" name="no_header" class="chk"><br>\
  				<label for="no_leftmenu">No left menu</label><input type="checkbox" id="no_leftmenu" name="no_leftmenu" class="chk"><br>\
					<label for="restricted">Restricted access</label><input type="checkbox" id="restricted" name="restricted" class="chk"><br>\
          <label for="title">Page title</label><input type="text" id="title" name="title" class="txt3"><br>\
					<table>\
					<tr><td>\
					<label for="description">Page description</label><br><textarea id="desciption" name="description" cols="40" rows="2"></textarea><br>\
					<input type="checkbox" name="desc_no_global" id="desc_no_global"><label for="desc_no_global" class="in">Do not add Global site description</label>\
					</td><td>\
					<label for="keywords">Page keywords</label><br><textarea id="keywords" name="keywords" cols="40" rows="2"></textarea><br>\
					<small>Comma separated</small><br>\
					<input type="checkbox" name="keywords_no_global" id="keywords_no_global"><label for="keywords_no_global" class="in">Do not add Global site keywords</label>\
					</td></tr></table>\
          <label>Custom header:</label><input type="radio" value="0" id="none" checked="checked" name="custom_header"><label class="inline" for="none">None</label><br>\
					<label>&nbsp;</label><input type="radio" id="image" name="custom_header" value="2" class="radio"><label class="inline" for="image">Image</label> <input type="text" name="custom_header_image" class="file"> <input type="button" name="browse" value="browse"><br>\
          <label>&nbsp;</label><input type="radio" id="html" name="custom_header" value="1" class="radio"><label class="inline" for="html">HTML</label><br><textarea name="custom_header_html" style="margin-left: 110px" cols="72" rows="6"></textarea>\
					</form>\
				';
{literal}
            if (this.page_domain_id == domain_id || this.page_id == 0) {
			    this.wnd = Dialog.confirm(f, {windowParameters: {className:"alphacube", width:760, title:"Page Properties", showEffect: Element.show, hideEffect: Element.hide},
									        ok: this.onok.bind(this) });
            }
//			this.form = this.wnd.getContent().getElementsByTagName("FORM")[0];
      this.form = Selector.findChildElements(this.wnd.getContent(), ["form"])[0];
			this.form.onsubmit = this.onsubmit.bind(this);

      this.sfb = new ServerFileBrowser(this.onfileselect.bind(this), "Images:/", "Images");
      this.form.elements["browse"].onclick=this.sfb.open.bind(this.sfb);
			
			Form.focusFirstElement(this.form);
			if(page_id) {
				new Ajax.Request('ajax_structure.php', {parameters:'action=load_content_props&id='+page_id, onSuccess:this.onload.bind(this)}); // TODO: handle AJAX errors here
			}
			else {
				this.form.int_name.value="page"+content_page_counter++;
				this.wnd.hideloader();
			}
		},
		
		onload: function(t, json) {
			jslog.debug(t.getResponseHeader("X-JSON"));
			populate_controls(this.form, json);
			this.wnd.hideloader();
		},

		onsave: function(t) {
			jslog.debug(t.responseText);
			load_content_pages();
			// we need to update node in structure
			// TODO: load structure or update
			// load_structure(); 
			this.wnd.hide();
		},

    onfileselect: function(url) {
      this.form.elements["custom_header_image"].value = url;
    },

		onsubmit: function() {
			this.onok();
			return false;
		},

		onok: function() {
			this.wnd.showloader();
			new Ajax.Request('ajax_structure.php', {parameters:'action=save_content_props&id='+this.page_id+'&'+Form.serialize(this.form), onSuccess:this.onsave.bind(this)}); // TODO: handle AJAX errors here
		}
	};
	
	var content_page_content = null;
  var ce_dialog = null;
	var ContentEditDialog = Class.create();
	ContentEditDialog.prototype = {
		initialize: function(page_id, page_domain_id) {
			// create window
			var width = document.body.clientWidth-80;
			var height = document.body.clientHeight-130;
//			this.wnd = new WindowClass("Page editor", width, height, [this.onok.bind(this), this.oncancel.bind(this)]);
			this.page_id = page_id;
            this.page_domain_id = page_domain_id;

//			fck.style.height = height-110;

//			this.form = Builder.node("FORM");
			// create dialog fields in form
			var f = '\
					<form>\
			        <label>Content HTML</label><br>\
			        <div class="tabber">\
						<div class="tabbertab"><h2>Main content</h2><div id="ckplace"></div></div>\
						<div class="tabbertab"><h2>Second content</h2></div>\
					</div>\
					<input type="hidden" name="content1">\
					<input type="hidden" name="content2">\
					</form>\
				';
            if (this.page_domain_id == domain_id) {
			    this.wnd = Dialog.confirm(f, {windowParameters: {className:"alphacube", width:width, height:height, title:"Page editor", showEffect: Element.show, hideEffect: Element.hide},
									        ok: this.onok.bind(this), cancel: this.oncancel.bind(this) });
            }

      this.form = Selector.findChildElements(this.wnd.getContent(), ["form"])[0];

      ckeditor = CKEDITOR.appendTo('ckplace', {width: width-18, height: height-190});
			CKFinder.SetupCKEditor(ckeditor, "/ckfinder/");

			var tabberOptions = {
			  'onClick': function(argsObj) {
			    var t = argsObj.tabber;
			    var id = t.id;
			    var i = argsObj.index;
			    var e = argsObj.event;
			    t.navClearActive(t.currentTab);
				// get fckeditor instance
//				var editor = FCKeditorAPI.GetInstance('fckeditor');
				// save current text to hidden field
				t.form.elements["content"+(t.currentTab+1)].value = ckeditor.getData();
				// load new text from hidden field for selected tab
				ckeditor.setData(t.form.elements["content"+(i+1)].value);
				// for mozilla to fix vertical size of iframe
//				if(!IsIE) {
//					if(window.frames["fckeditor___Frame"].onresize) {
//						window.frames["fckeditor___Frame"].onresize();
//					}
//				}
			    t.currentTab = i;
			    t.navSetActive(i);
			    return false;
			  }
			};
			this.form.onsubmit = this.onsubmit.bind(this);
			tabberAutomatic(tabberOptions);
			this.tabber=this.form.getElementsByTagName("div")[0].tabber;

			this.tabber.form = this.form;
			this.tabber.currentTab = 0;

			// clear editor area only if IE, mozilla reloads IFRAME every time
/*			if(IsIE) {
				var editor = FCKeditorAPI.GetInstance('fckeditor');
				if(editor && editor.Status==FCK_STATUS_COMPLETE) { 
					editor.SetHTML("");
				}
			}
						
			this.fckeditor = fckeditor; */
			// load data
			new Ajax.Request('ajax_structure.php', {parameters:'action=load_page&id='+page_id, onSuccess:this.onload.bind(this)});
		},

		onload: function(t, json) {
			jslog.debug(t.responseText);
			json = eval(t.responseText);
			var content1 = "";
			var content2 = "";
			if(json.content1) { content1 = json.content1 };
			if(json.content2) { content2 = json.content2 };
			// store contents to hidden fields
			this.form.elements["content1"].value = content1;
			this.form.elements["content2"].value = content2;

			// Get the editor instance that we want to interact with.
      ckeditor.setData(content1); 
      this.wnd.hideloader();

/*			var oEditor=null;
      var self = this;
			Try.these (
        function() { oEditor = FCKeditorAPI.GetInstance('fckeditor'); }
	    );
			if(oEditor && oEditor.Status==FCK_STATUS_COMPLETE) { 
				// Insert the desired HTML.
			    jslog.debug("onload editor content setting");
			    Try.these(
					function() { 
						oEditor.SetHTML(content1); 
            self.wnd.hideloader();
					},
          function() { 
            content_page_content = content1 
            ce_dialog = self;
          }
        );
			}
			else { // store data, FCK onload event will handle it
				content_page_content = content1;
        ce_dialog = this;
			  jslog.debug("editor not ready, storing data");
        // for IE, push tabber, without this FCKEditor Onready not occured
        if(IsIE) {
          setTimeout("FCKeditor_OnComplete()", 1000);
        }
			} */		
		},

		onsubmit: function() {
			this.onok();
			return false;
		},

		onsave: function(t) {
			jslog.debug(t.responseText);
      // destroy CKEditor
			ckeditor.destroy();
      ckeditor = null;			
			this.wnd.hide();
		},
		
		oncancel: function() {
      ckeditor.destroy();
      ckeditor = null;
		},

		onok: function() {
			this.wnd.showloader();
			// store content from editor to hidden field
//			var editor = FCKeditorAPI.GetInstance('fckeditor');
			this.form.elements["content"+(this.tabber.currentTab+1)].value = ckeditor.getData();
			new Ajax.Request('ajax_structure.php', {parameters:'action=save_content&id='+this.page_id+"&"+Form.serialize(this.form), onSuccess:this.onsave.bind(this)}); // TODO: handle AJAX errors here
		}
	};
	
/*	function FCKeditor_OnComplete(instance)
	{
	  jslog.debug("FCK OnComplete");
		if(content_page_content) {
      if(!instance) instance = FCKeditorAPI.GetInstance('fckeditor');
      // set 1st tab as active (for case, when user pushed second)
  		ce_dialog.tabber.navClearActive(ce_dialog.tabber.currentTab);
  		ce_dialog.tabber.currentTab = 0;
      ce_dialog.tabber.navSetActive(0);
			instance.SetHTML(content_page_content);
      ce_dialog.wnd.hideloader();
			content_page_content = null;
		  jslog.debug("FCK onload content set");
		}
	} */

	var ContentEditHTMLDialog = Class.create();
	ContentEditHTMLDialog.prototype = {
		initialize: function(page_id, page_domain_id) {
			// create window
			var width = document.body.clientWidth-80;
			var height = document.body.clientHeight-130;
//			this.wnd = new WindowClass("Page editor", width, height, [this.onok.bind(this), this.oncancel.bind(this)]);
			this.page_id = page_id;
            this.page_domain_id = page_domain_id;
			// create form

            if(is_ie) {
                var f = '\
					<form>\
			            <label>Content HTML</label><br>\
			            <div class="tabber">\
                            <div class="tabbertab"><h2>Main content</h2><textarea name="content1" id="content1" style="width:'+(width-23)+'px; height:'+(height-120)+'px; margin: 0;"></textarea></div>\
                            <div class="tabbertab"><h2>Second content</h2><textarea name="content2" id="content2" style="width:'+(width-23)+'px; height:'+(height-120)+'px; margin: 0;"></textarea></div>\
					    </div>\
					</form>\
				';
            } else {
			    var f = '\
					<form>\
			            <label>Content HTML</label><br>\
			            <div class="tabber">\
                            <div class="tabbertab"><h2>Main content</h2><textarea name="content1" id="content1" style="display: none;"></textarea><div id="ace-content1" style="width:'+(width-23)+'px; height:'+(height-120)+'px;"></div></div>\
                            <div class="tabbertab"><h2>Second content</h2><textarea name="content2" id="content2" style="display: none;"></textarea><div id="ace-content2" style="width:'+(width-23)+'px; height:'+(height-120)+'px;"></div></div>\
					    </div>\
					</form>\
				';
            }

            if (this.page_domain_id == domain_id) {
                this.wnd = Dialog.confirm(f, {windowParameters: {className:"alphacube", width:width, height:height, title:"General Properties", showEffect: Element.show, hideEffect: Element.hide},
									        ok: this.onok.bind(this) });
            }
			this.form = this.wnd.getContent().getElementsByTagName("FORM")[0];
            this.form.elements["content1"].onkeydown = textarea_tab;
			this.form.elements["content2"].onkeydown = textarea_tab;

            // call tabber to make tabs
            if(is_ie) {
			    tabberAutomatic();
            } else {
                var HtmlMode = require("ace/mode/html").Mode;

                window.editor1 = ace.edit("ace-content1");
                window.editor1.setTheme("ace/theme/crimson_editor");
                window.editor1.getSession().setMode(new HtmlMode());

                window.editor2 = ace.edit("ace-content2");
                window.editor2.setTheme("ace/theme/crimson_editor");
                window.editor2.getSession().setMode(new HtmlMode());

                setTimeout(function() { tabberAutomatic(); }, 500);
            }

			Form.focusFirstElement(this.form);
			// load data
			new Ajax.Request('ajax_structure.php', {parameters:'action=load_page&id='+page_id, onSuccess:this.onload.bind(this)});
		},

		onload: function(t, json) {
			jslog.debug(t.responseText);
			json = eval(t.responseText);
			populate_controls(this.form, json);

            if(!is_ie) {
                window.editor1.getSession().setValue(this.form.elements['content1'].value);
                var line1 = Cookies['ace_content1_'+this.page_id+'_line'];
                if(line1 != null) {
                    window.editor1.gotoLine(line1);
                } else {
                    window.editor1.gotoLine(1);
                }

                window.editor2.getSession().setValue(this.form.elements['content2'].value);
                var line2 = Cookies['ace_content2_'+this.page_id+'_line'];
                if(line2 != null) {
                    window.editor2.gotoLine(line2);
                } else {
                    window.editor2.gotoLine(1);
                }
            }
            this.wnd.hideloader();
		},

		onsave: function(t) {
			jslog.debug(t.responseText);
			this.wnd.hide();
		},

		onok: function() {
			this.wnd.showloader();
            if(!is_ie) {
                this.form.elements['content1'].value = window.editor1.getSession().getValue();
                Cookies.create('ace_content1_'+this.page_id+'_line', window.editor1.getCursorPosition().row+1, 0);

                this.form.elements['content2'].value = window.editor2.getSession().getValue();
                Cookies.create('ace_content2_'+this.page_id+'_line', window.editor2.getCursorPosition().row+1, 0);
            }
			new Ajax.Request('ajax_structure.php', {parameters:'action=save_content&id='+this.page_id+"&"+Form.serialize(this.form), onSuccess:this.onsave.bind(this)}); // TODO: handle AJAX errors here
		}
	};

	var GeneralPropsDialog = Class.create();
	GeneralPropsDialog.prototype = {
		initialize: function() {
			// create dialog fields in form
			var f = '\
					<form>\
					<label for="title">Title</label><input type="text" id="title" name="title" class="txt2"><br>\
					<label for="width">Width (with px!)</label><input type="text" id="width" name="width" class="small"><br>\
			  		<label for="layout_id">Layout</label><select id="layout_id" name="layout_id"></select><br>\
			  		<label for="align">Align</label><input type="radio" name="align" id="align" value="1">align left<input type="radio" name="align" value="2">align center<br>\
					<label for="bgcolor">Backgr.color</label><input type="text" id="bgcolor" name="bgcolor" class="color"><br>\
					<label for="bgimage">Backgr.image</label><input type="text" id="bgimage" name="bgimage" class="file"><input type="button" name="browse" value="browse"><br>\
					<!-- <label for="date_format">Date Format</label><input type="text" id="date_format" name="date_format" class="txt"><br> -->\
					<label for="description">Main description</label><textarea id="desciption" name="description" cols="44" rows="2"></textarea><br>\
					<label>&nbsp;</label><small>Added after page description, can be disabled per page (see individual page properties)</small><br>\
					<label for="keywords">Main keywords</label><textarea id="keywords" name="keywords" cols="44" rows="2"></textarea><br>\
					<label>&nbsp;</label><small>Added after page keywords, can be disabled per page. Comma separated</small><br>\
					<label for="headhtml">Additional head code</label><textarea id="headhtml" name="headhtml" cols="44" rows="2"></textarea><br>\
					<label>&nbsp;</label><small>Any HTML code to add to head section, added for every site page</small><br>\
					</form>\
				';
			this.wnd = Dialog.confirm(f, {windowParameters: {className:"alphacube", width:540, title:"General Properties", showEffect: Element.show, hideEffect: Element.hide},
									      ok: this.onok.bind(this) });
			this.form = this.wnd.getContent().getElementsByTagName("FORM")[0];

			this.form.onsubmit = this.onsubmit.bind(this);
			// create and bind filebrowser
			this.sfb = new ServerFileBrowser(this.onfileselect.bind(this), "Images:/", "Images");
			this.form.elements["browse"].onclick=this.sfb.open.bind(this.sfb);

			Form.focusFirstElement(this.form);
			// load layouts
			this.layoutsloaded = false;
			new Ajax.Request('ajax_structure.php', {parameters:'action=load_layouts', onSuccess:this.onlayoutsload.bind(this)}); // TODO: handle AJAX errors here
			// load data
			new Ajax.Request('ajax_structure.php', {parameters:'action=load_site_general', onSuccess:this.onload.bind(this)}); // TODO: handle AJAX errors here
		},

		onfileselect: function(url) {
			this.form.elements["bgimage"].value = url;
		},

		onload: function(t, json) {
			if(t) {
				jslog.debug(t.getResponseHeader("X-JSON"));
			}
			if(this.layoutsloaded) {
				// prevent second population
				this.layoutsloaded = false;
				populate_controls(this.form, json);
				this.wnd.hideloader();
			}
			else {
				this.json = json;
				jslog.debug("onload waiting for onlayoutsload");
			}
		},

		onlayoutsload: function(t, json) {
			jslog.debug(t.getResponseHeader("X-JSON"));
			var form = this.form;
			$H(json).each(function(elem) {
				form.elements["layout_id"].options.add(new Option(elem.value, elem.key));
			} );
			this.layoutsloaded = true;
			if(this.json) {
				jslog.debug("onlayoutsload passing control to waiting onload");
				this.onload(null, this.json);
			}
			jslog.debug("onlayoutsload done");
		},

		onsubmit: function() {
			this.onok();
			return false;
		},

		onsave: function(t) {
			jslog.debug(t.responseText);
			this.wnd.hide();
		},
		
		onok: function() {
			this.wnd.showloader();
			new Ajax.Request('ajax_structure.php', {parameters:'action=save_site_general&'+Form.serialize(this.form), onSuccess:this.onsave.bind(this)}); // TODO: handle AJAX errors here
		}
	}

	var HeaderDialog = Class.create();
	HeaderDialog.prototype = {
		initialize: function() {
			// create dialog fields in form
            if(is_ie) {
                var f = '\
					<form>\
			        <label>Header type:</label><input type="radio" id="image" name="header_mode" value="0" class="radio" checked="checked"><label class="inline" for="image">image</label> <input type="text" name="header_image" class="file"> <input type="button" name="browse" value="browse"><br>\
			        <label>&nbsp;</label><input type="radio" id="html" name="header_mode" value="1" class="radio"><label class="inline" for="html">HTML</label><br><textarea name="header_html" style="margin-left: 110px" cols="64" rows="12"></textarea>\
					</form>\
				';
            } else {
			    var f = '\
					<form>\
			        <label>Header type:</label><input type="radio" id="image" name="header_mode" value="0" class="radio" checked="checked"><label class="inline" for="image">image</label> <input type="text" name="header_image" class="file"> <input type="button" name="browse" value="browse"><br>\
			        <label>&nbsp;</label><input type="radio" id="html" name="header_mode" value="1" class="radio"><label class="inline" for="html">HTML</label><br><textarea name="header_html" id="header_html" style="display: none; margin-left: 110px;" cols="64" rows="12"></textarea>\
					</form>\
					<div id="ace-header-html" style="height: 200px; margin-left: 110px;"></div>\
                ';
            }
			this.wnd = Dialog.confirm(f, {windowParameters: {className:"alphacube", width:675, title:"Site Header", showEffect: Element.show, hideEffect: Element.hide},
									      ok: this.onok.bind(this) });
			this.form = this.wnd.getContent().getElementsByTagName("FORM")[0];
			this.form.onsubmit = this.onsubmit.bind(this);
			// create and bind filebrowser
			this.sfb = new ServerFileBrowser(this.onfileselect.bind(this), "Images:/", "Images");
			this.form.elements["browse"].onclick=this.sfb.open.bind(this.sfb);

			// load data
			new Ajax.Request('ajax_structure.php', {parameters:'action=load_site_header', onSuccess:this.onload.bind(this)}); // TODO: handle AJAX errors here
			Form.focusFirstElement(this.form);
		},

		onfileselect: function(url) {
			this.form.elements["header_image"].value = url;
		},

		onload: function(t, json) {
			jslog.debug(t.getResponseHeader("X-JSON"));
			populate_controls(this.form, json);

            if(!is_ie) {
                this.editor = ace.edit("ace-header-html");
                this.editor.setTheme("ace/theme/crimson_editor");
                var HtmlMode = require("ace/mode/html").Mode;
                this.editor.getSession().setMode(new HtmlMode());
                this.editor.getSession().setValue(this.form.elements['header_html'].value);

                var line = Cookies['ace_header_line'];
                if(line != null) {
                    this.editor.gotoLine(line);
                } else {
                    this.editor.gotoLine(1);
                }
            }

            this.wnd.hideloader();
		},

		onsave: function(t) {
			jslog.debug(t.responseText);
			this.wnd.hide();
		},
		
		onsubmit: function() {
			this.onok();
			return false;
		},

		onok: function() {
			this.wnd.showloader();
            if(!is_ie) {
                this.form.elements['header_html'].value = this.editor.getSession().getValue();
                Cookies.create('ace_header_line', this.editor.getCursorPosition().row+1, 0);
            }
			new Ajax.Request('ajax_structure.php', {parameters:'action=save_site_header&'+Form.serialize(this.form), onSuccess:this.onsave.bind(this)}); // TODO: handle AJAX errors here
		}
	}

  var HeaderWysiwygDialog = Class.create();
	HeaderWysiwygDialog.prototype = {
		initialize: function() {
			// create dialog fields in form
			var f = '\
					<form>\
			        <label>Header type:</label><input type="radio" id="image" name="header_mode" value="0" class="radio" checked="checked"><label class="inline" for="image">image</label> <input type="text" name="header_image" class="file"> <input type="button" name="browse" value="browse"><br>\
			        <label>&nbsp;</label><input type="radio" id="html" name="header_mode" value="1" class="radio"><label class="inline" for="html">HTML</label><br><textarea name="header_html" id="header_html" style="width: 660px; height: 270px"></textarea>\
					</form>\
				';
			this.wnd = Dialog.confirm(f, {windowParameters: {className:"alphacube", width:675, height:400, title:"Site Header", showEffect: Element.show, hideEffect: Element.hide},
									      ok: this.onok.bind(this) });
			this.form = this.wnd.getContent().getElementsByTagName("FORM")[0];
			this.form.onsubmit = this.onsubmit.bind(this);
			// create and bind filebrowser
			this.sfb = new ServerFileBrowser(this.onfileselect.bind(this), "Images:/", "Images");
			this.form.elements["browse"].onclick=this.sfb.open.bind(this.sfb);

      var myNicEditor = new nicEditor({buttonList:['bold','italic','underline','strikethrough','subscript','superscript','ol','ul','hr','link','unlink','fontFormat','xhtml'], iconsPath: "/img/nicEditorIcons.gif", maxHeight: 270}).panelInstance('header_html');

			// load data
			new Ajax.Request('ajax_structure.php', {parameters:'action=load_site_header', onSuccess:this.onload.bind(this)}); // TODO: handle AJAX errors here
			Form.focusFirstElement(this.form);
		},

		onfileselect: function(url) {
			this.form.elements["header_image"].value = url;
		},

		onload: function(t, json) {
			jslog.debug(t.getResponseHeader("X-JSON"));
			populate_controls(this.form, json);
      nicEditors.findEditor("header_html").setContent(this.form.elements["header_html"].value);
			this.wnd.hideloader();
		},

		onsave: function(t) {
			jslog.debug(t.responseText);
			this.wnd.hide();
		},
		
		onsubmit: function() {
			this.onok();
			return false;
		},

		onok: function() {
			this.wnd.showloader();
      nicEditors.findEditor("header_html").saveContent();
			new Ajax.Request('ajax_structure.php', {parameters:'action=save_site_header&'+Form.serialize(this.form), onSuccess:this.onsave.bind(this)}); // TODO: handle AJAX errors here
		}
	}
	
  var FooterDialog = Class.create();
	FooterDialog.prototype = {
		initialize: function() {
			// create dialog fields in form
            if(is_ie) {
                var f = '\
					<form>\
					<label for="footer">Footer HTML</label><br><textarea name="footer" id="footer" cols="80" rows="15"></textarea>\
					</form>\
				';
            } else {
			    var f = '\
					<form>\
					<label for="footer">Footer HTML</label><br><textarea name="footer" id="footer" cols="80" rows="15" style="display: none;"></textarea>\
					</form>\
					<div id="ace-footer" style="height: 240px;"></div>\
                ';
            }
			this.wnd = Dialog.confirm(f, {windowParameters: {className:"alphacube", width:675, title:"Site Footer", showEffect: Element.show, hideEffect: Element.hide},
									      ok: this.onok.bind(this) });
			this.form = this.wnd.getContent().getElementsByTagName("FORM")[0];

			this.form.elements["footer"].onkeydown = textarea_tab;
			this.form.onsubmit = this.onsubmit.bind(this);

			Form.focusFirstElement(this.form);
			// load data
			new Ajax.Request('ajax_structure.php', {parameters:'action=load_site_footer', onSuccess:this.onload.bind(this)}); // TODO: handle AJAX errors here
		},

		onload: function(t, json) {
			jslog.debug(t.getResponseHeader("X-JSON"));
			populate_controls(this.form, json);

            if(!is_ie) {
                this.editor = ace.edit("ace-footer");
                this.editor.setTheme("ace/theme/crimson_editor");
                var HtmlMode = require("ace/mode/html").Mode;
                this.editor.getSession().setMode(new HtmlMode());
                this.editor.getSession().setValue(this.form.elements['footer'].value);

                var line = Cookies['ace_footer_line'];
                if(line != null) {
                    this.editor.gotoLine(line);
                } else {
                    this.editor.gotoLine(1);
                }
            }

            this.wnd.hideloader();
		},

		onsave: function(t) {
			jslog.debug(t.responseText);
			this.wnd.hide();
		},
		
		onsubmit: function() {
			this.onok();
			return false;
		},

		onok: function() {
			this.wnd.showloader();
            if(!is_ie) {
                this.form.elements['footer'].value = this.editor.getSession().getValue();
                Cookies.create('ace_footer_line', this.editor.getCursorPosition().row+1, 0);
            }
			new Ajax.Request('ajax_structure.php', {parameters:'action=save_site_footer&'+Form.serialize(this.form), onSuccess:this.onsave.bind(this)}); // TODO: handle AJAX errors here
		}
	}

  var FooterWysiwygDialog = Class.create();
	FooterWysiwygDialog.prototype = {
		initialize: function() {
			// create dialog fields in form
			var f = '\
					<form>\
					<label for="footer">Footer HTML</label><br><textarea name="footer" id="footer" style="width: 660px; height: 270px"></textarea>\
					</form>\
				';
			this.wnd = Dialog.confirm(f, {windowParameters: {className:"alphacube", width:670, height: 370, title:"Site Footer", showEffect: Element.show, hideEffect: Element.hide},
									      ok: this.onok.bind(this) });
			this.form = this.wnd.getContent().getElementsByTagName("FORM")[0];

			this.form.elements["footer"].onkeydown = textarea_tab;
			this.form.onsubmit = this.onsubmit.bind(this);

      var myNicEditor = new nicEditor({buttonList:['bold','italic','underline','strikethrough','subscript','superscript','ol','ul','hr','link','unlink','fontFormat','xhtml'], iconsPath: "/img/nicEditorIcons.gif", maxHeight: 270}).panelInstance('footer');

//			Form.focusFirstElement(this.form);
			// load data
			new Ajax.Request('ajax_structure.php', {parameters:'action=load_site_footer', onSuccess:this.onload.bind(this)}); // TODO: handle AJAX errors here
		},

		onload: function(t, json) {
			jslog.debug(t.getResponseHeader("X-JSON"));
			populate_controls(this.form, json);
      nicEditors.findEditor("footer").setContent(this.form.elements["footer"].value);
			this.wnd.hideloader();
		},

		onsave: function(t) {
			jslog.debug(t.responseText);
			this.wnd.hide();
		},
		
		onsubmit: function() {
			this.onok();
			return false;
		},

		onok: function() {
			this.wnd.showloader();
      nicEditors.findEditor("footer").saveContent();
			new Ajax.Request('ajax_structure.php', {parameters:'action=save_site_footer&'+Form.serialize(this.form), onSuccess:this.onsave.bind(this)}); // TODO: handle AJAX errors here
		}
	}

	var CSSDialog = Class.create();
	CSSDialog.prototype = {
		initialize: function() {
			// create dialog fields in form
            if(is_ie) {
                var f = '\
					<form>\
					<label for="css">CSS Styles</label><br><textarea name="css" id="css" cols="80" rows="25"></textarea>\
					</form>\
				';
            } else {
			    var f = '\
					<form>\
					<label for="css">CSS Styles</label><br><textarea name="css" id="css" cols="80" rows="25" style="display: none;"></textarea>\
                    </form>\
                    <div id="ace-css" style="height: 400px;"></div>\
                    ';
            }
			this.wnd = Dialog.confirm(f, {title:"CSS Styles", width:675, ok: this.onok.bind(this)});
			this.form = this.wnd.getContent().getElementsByTagName("FORM")[0];
			this.form.elements["css"].onkeydown = textarea_tab;
			Form.focusFirstElement(this.form);
			// load data
			new Ajax.Request('ajax_structure.php', {parameters:'action=load_site_css', onSuccess:this.onload.bind(this)}); // TODO: handle AJAX errors here
		},

		onload: function(t, json) {
			jslog.debug(t.getResponseHeader("X-JSON"));
			populate_controls(this.form, json);

            if(!is_ie) {
                this.editor = ace.edit("ace-css");
                this.editor.setTheme("ace/theme/crimson_editor");
                var CssMode = require("ace/mode/css").Mode;
                this.editor.getSession().setMode(new CssMode());
                this.editor.getSession().setValue(this.form.elements['css'].value);

                var line = Cookies['ace_css_line'];
                if(line != null) {
                    this.editor.gotoLine(line);
                } else {
                    this.editor.gotoLine(1);
                }
            }

            this.wnd.hideloader();
		},

		onsave: function(t) {
			jslog.debug(t.responseText);
			this.wnd.hide();
		},
		
		onok: function() {
			this.wnd.showloader();
            if(!is_ie) {
                this.form.elements['css'].value = this.editor.getSession().getValue();
                Cookies.create('ace_css_line', this.editor.getCursorPosition().row+1, 0);
            }
			new Ajax.Request('ajax_structure.php', {parameters:'action=save_site_css&'+Form.serialize(this.form), onSuccess:this.onsave.bind(this)}); // TODO: handle AJAX errors here
		}
	}

  var MusicDialog = Class.create();
	MusicDialog.prototype = {
    initialize: function() {
    // create dialog fields in form
{/literal}
{capture name=music_files}
	{html_options options=$music_files}
{/capture}
			var f = '\
					<form>\
			        <label>Obituaries music:</label><input type="radio" id="off" name="site_music_state" value="0" class="radio" checked="checked"><label class="inline" for="off">off</label><br>\
			        <label>&nbsp;</label><input type="radio" id="on" name="site_music_state" value="1" class="radio"><label class="inline" for="on">on</label><br>\
              <label>Composition:</label><select id="site_music_file" name="site_music_file">{$smarty.capture.music_files|strip|escape:"quotes"}</select>\
              <label>&nbsp;</label><div id="player">MediaPlayer</div>\
					</form>\
				';
{literal}
			this.wnd = Dialog.confirm(f, {windowParameters: {className:"alphacube", width:400, title:"Music", showEffect: Element.show, hideEffect: Element.hide},
									      ok: this.onok.bind(this) });
			this.form = this.wnd.getContent().getElementsByTagName("FORM")[0];
			this.form.onsubmit = this.onsubmit.bind(this);
  		this.form.elements["site_music_file"].onchange=this.onlisten.bind(this);
//  		this.form.elements["listen"].onclick=this.onlisten.bind(this);
  
      // write player
      var so = new SWFObject('tools/mediaplayer.swf','mpl','220','20','8');
      so.addParam('allowscriptaccess','always');
      so.addParam('allowfullscreen','false');
      so.addVariable('height','20');
      so.addVariable('width','220');
      so.addVariable('showdigits','false');
      so.addVariable('showstop','true');
      so.addVariable('repeat','false');
      so.addVariable('type','mp3');
      so.addVariable("enablejs", "true");
      so.write('player');
			// load data
			new Ajax.Request('ajax_structure.php', {parameters:'action=load_site_music', onSuccess:this.onload.bind(this)});
			Form.focusFirstElement(this.form);
		},

		onload: function(t, json) {
			jslog.debug(t.getResponseHeader("X-JSON"));
			populate_controls(this.form, json);
			this.wnd.hideloader();
      this.onlisten();
		},

		onsave: function(t) {
			jslog.debug(t.responseText);
			this.wnd.hide();
		},

    onlisten: function() {
      var filename = this.form.elements["site_music_file"].options[this.form.elements["site_music_file"].selectedIndex].value;
      $("mpl").sendEvent("stop");
      if(filename) {
        $("mpl").loadFile({file:filename});
        $("mpl").sendEvent("playpause");
      }
    },
		
		onsubmit: function() {
			this.onok();
			return false;
		},

		onok: function() {
			this.wnd.showloader();
			new Ajax.Request('ajax_structure.php', {parameters:'action=save_site_music&'+Form.serialize(this.form), onSuccess:this.onsave.bind(this)}); // TODO: handle AJAX errors here
		}
	}

	var MainMenuDialog = Class.create();
	MainMenuDialog.prototype = {
		initialize: function() {
			// create dialog fields in form
			var f = '\
					<form>\
					<label for="width">Menu width</label><input type="text" id="width" name="width" class="txt"><br>\
					<label for="height">Elem height</label><input type="text" id="height" name="height" class="txt"><br>\
					<label for="color">Color (FG/BG)</label><input type="text" id="color" name="color" class="color"><input type="button" value=" .. " onclick="showColorPicker(this,$(\'color\'))">/<input type="text" id="bgcolor" name="bgcolor" class="color"><input type="button" value=" .. " onclick="showColorPicker(this,$(\'bgcolor\'))"><br>\
					<label for="hvcolor">Hover Color</label><input type="text" id="hvcolor" name="hvcolor" class="color"><input type="button" value=" .. " onclick="showColorPicker(this,$(\'hvcolor\'))">/<input type="text" id="hvbgcolor" name="hvbgcolor" class="color"><input type="button" value=" .. " onclick="showColorPicker(this,$(\'hvbgcolor\'))"><br>\
					<label for="font">Font<br></label><input type="text" id="font" name="font" class="font"><span style="font: 12px times">* italic small-caps bold size family</span><br>\
					<label for="prespacer">Pre spacer</label><input type="text" id="prespacer" name="prespacer" class="file"><input type="button" name="browse_pre" value="browse"><br>\
					<label for="postspacer">Post spacer</label><input type="text" id="postspacer" name="postspacer" class="file"><input type="button" name="browse_post" value="browse"><br>\
					</form>\
				';
			this.wnd = Dialog.confirm(f, {title:"Main menu properties", width:500, ok: this.onok.bind(this)});
			this.form = this.wnd.getContent().getElementsByTagName("FORM")[0];
			this.sfb = new ServerFileBrowser(this.onprespacerselect.bind(this), "Images:/", "Images");
			this.form.elements["browse_pre"].onclick=this.sfb.open.bind(this.sfb);
			this.sfb2 = new ServerFileBrowser(this.onpostspacerselect.bind(this), "Images:/", "Images");
			this.form.elements["browse_post"].onclick=this.sfb2.open.bind(this.sfb2);

			Form.focusFirstElement(this.form);
			// load data
			new Ajax.Request('ajax_structure.php', {parameters:'action=load_site_mainmenu', onSuccess:this.onload.bind(this)}); // TODO: handle AJAX errors here
		},

		onload: function(t, json) {
			jslog.debug(t.getResponseHeader("X-JSON"));
			jslog.debug(t.responseText);
			populate_controls(this.form, json);
			this.wnd.hideloader();
		},

		onprespacerselect: function(url) {
			this.form.elements["prespacer"].value = url;
		},

		onpostspacerselect: function(url) {
			this.form.elements["postspacer"].value = url;
		},

		onsave: function(t) {
			jslog.debug(t.responseText);
			this.wnd.hide();
		},
		
		onok: function() {
			this.wnd.showloader();
			new Ajax.Request('ajax_structure.php', {parameters:'action=save_site_mainmenu&'+Form.serialize(this.form), onSuccess:this.onsave.bind(this)}); // TODO: handle AJAX errors here
		}
	}
	
	var PopupMenuDialog = Class.create();
	PopupMenuDialog.prototype = {
		initialize: function() {
			// create dialog fields in form
			var f = '\
					<form>\
					<label for="width">Menu width</label><input type="text" id="width" name="width" class="txt"><br>\
					<label for="height">Elem height</label><input type="text" id="height" name="height" class="txt"><br>\
					<label for="color">Color (FG/BG)</label><input type="text" id="color" name="color" class="color"><input type="button" value=" .. " onclick="showColorPicker(this,$(\'color\'))">/<input type="text" id="bgcolor" name="bgcolor" class="color"><input type="button" value=" .. " onclick="showColorPicker(this,$(\'bgcolor\'))"><br>\
					<label for="hvcolor">Hover Color</label><input type="text" id="hvcolor" name="hvcolor" class="color"><input type="button" value=" .. " onclick="showColorPicker(this,$(\'hvcolor\'))">/<input type="text" id="hvbgcolor" name="hvbgcolor" class="color"><input type="button" value=" .. " onclick="showColorPicker(this,$(\'hvbgcolor\'))"><br>\
					<label for="font">Font<br></label><input type="text" id="font" name="font" class="font"><span style="font: 12px times">* italic small-caps bold size family</span><br>\
					<label for="border">Border</label><input type="checkbox" id="border" name="border" class="chk"><br>\
					<label for="prespacer">Pre spacer</label><input type="text" id="prespacer" name="prespacer" class="file"><input type="button" name="browse_pre" value="browse"><br>\
					<label for="postspacer">Post spacer</label><input type="text" id="postspacer" name="postspacer" class="file"><input type="button" name="browse_post" value="browse"><br>\
					</form>\
				';
			this.wnd = Dialog.confirm(f, {title:"Popup menu properties", width:500, ok: this.onok.bind(this)});
			this.form = this.wnd.getContent().getElementsByTagName("FORM")[0];
			this.sfb = new ServerFileBrowser(this.onprespacerselect.bind(this), "Images:/", "Images");
			this.form.elements["browse_pre"].onclick=this.sfb.open.bind(this.sfb);
			this.sfb2 = new ServerFileBrowser(this.onpostspacerselect.bind(this), "Images:/", "Images");
			this.form.elements["browse_post"].onclick=this.sfb2.open.bind(this.sfb2);

			Form.focusFirstElement(this.form);
			// load data
			new Ajax.Request('ajax_structure.php', {parameters:'action=load_site_popupmenu', onSuccess:this.onload.bind(this)}); // TODO: handle AJAX errors here
		},

		onload: function(t, json) {
			jslog.debug(t.getResponseHeader("X-JSON"));
			jslog.debug(t.responseText);
			populate_controls(this.form, json);
			this.wnd.hideloader();
		},

		onprespacerselect: function(url) {
			this.form.elements["prespacer"].value = url;
		},

		onpostspacerselect: function(url) {
			this.form.elements["postspacer"].value = url;
		},

		onsave: function(t) {
			jslog.debug(t.responseText);
			this.wnd.hide();
		},
		
		onok: function() {
			this.wnd.showloader();
			new Ajax.Request('ajax_structure.php', {parameters:'action=save_site_popupmenu&'+Form.serialize(this.form), onSuccess:this.onsave.bind(this)}); // TODO: handle AJAX errors here
		}
	}

// Stuff

// menu handlers
	function on_content_page_properties(param) {
		new ContentPropsDialog(param.page_id, param.domain_id);
	}

	function on_content_page_edit(param) {
		new ContentEditDialog(param.page_id, param.domain_id);
	}

	function on_content_page_edit_html(param) {
		new ContentEditHTMLDialog(param.page_id, param.domain_id);
	}

	function on_content_page_delete(param) {
		if(domain_id==param.domain_id && confirm("Are you sure you want to delete content page?")) {
			new Ajax.Request('ajax_structure.php', {parameters: "action=delete_page&id="+param.page_id});
			load_content_pages();
		}
	}

	function on_node_properties(param) {
		new NodeDialog(param.node, 0);
	}

	function structure_updated(t) {
		jslog.debug(t.responseText);
		structure.initialize();
	}
	
	function on_node_clear(param) {
		structure.clear_node_content(param.node);
	}
	
	function on_node_delete(param) {
		structure.remove_node(param.node);
	}
	
	function handle_context_menu(e) {
		if (!e) var e = window.event;
		var tg = (e.target) ? e.target : e.srcElement;

		var posx = 0;
		var posy = 0;

		if (e.pageX || e.pageY)
		{
			posx = e.pageX;
			posy = e.pageY;
		}
		else if (e.clientX || e.clientY)
		{
			posx = e.clientX + document.body.scrollLeft;
			posy = e.clientY + document.body.scrollTop;
		}

		switch(tg.className) {
			case "page":
            case "page page-0":
            case "page page-1":
            case "page page-2":
            case "page page-3":
            case "page page-4":
            case "page page-5":
				if(tg.parentNode.className == "node_content" && domain_id == tg.getAttribute("domain_id")) {
				    mixed_menu.popup(posx, posy, {page_id: tg.getAttribute("page_id"), domain_id: tg.getAttribute("domain_id"), node: tg.parentNode.parentNode});
				}
                if(tg.parentNode.className == "node_content" && domain_id != tg.getAttribute("domain_id")) {
                    node_menu.popup(posx, posy, {node: tg.parentNode.parentNode});
                }
				if(tg.parentNode.className != "node_content" && domain_id == tg.getAttribute("domain_id")) {
                    content_page_menu.popup(posx, posy, {page_id: tg.getAttribute("page_id"), domain_id: tg.getAttribute("domain_id")});
				}
				return false;
			break;
			case "node_name":
				node_menu.popup(e.clientX, e.clientY, {node: tg.parentNode});
				return false;
			break;
		}
		return true;
	}

	function handle_double_click(e)
	{
		if (!e) var e = window.event;
		var tg = (e.target) ? e.target : e.srcElement;
		switch(tg.className) {
			case "page":
				if(tg.parentNode.className=="node_content") {
					on_node_properties({node: tg.parentNode.parentNode});
				} else {
				    on_content_page_properties({page_id: tg.getAttribute("page_id"), domain_id: tg.getAttribute("domain_id")});
				}
				return false;
			break;
			case "node_name":
				on_node_properties({node: tg.parentNode});
				return false;
			break;
		}
		return true;
	}

	function onmousedown(e)
	{
		var posx = 0;
		var posy = 0;
		if (!e) var e = window.event;

//		e.cancelBubble = true;
//		if (e.stopPropagation) e.stopPropagation();

		if (e.pageX || e.pageY)
		{
			posx = e.pageX;
			posy = e.pageY;
		}
		else if (e.clientX || e.clientY)
		{
			posx = e.clientX + document.body.scrollLeft;
			posy = e.clientY + document.body.scrollTop;
		}
		// posx and posy contain the mouse position relative to the document
		if($("popupmenu")) {
			if(!Position.within($("popupmenu"), posx, posy)) {
				node_menu.hide();
				content_page_menu.hide();
				mixed_menu.hide();
			}
		}
	}
	
	function load_content_pages() {
    new Ajax.Updater('content_pages', 'ajax_structure.php', {parameters: "action=list_pages", onComplete: content_pages_onload});
  }

	var page_actions = [["Page Properties", "on_content_page_properties"],["Edit Wysiwyg", "on_content_page_edit"],["Edit HTML", "on_content_page_edit_html"], ["Delete Page", "on_content_page_delete"]];
	var node_actions = [["Node Properties", on_node_properties],["Clear Content", "on_node_clear"], ["Delete Node", "on_node_delete"]];
	var content_page_menu = new ContextMenu(page_actions);
	var node_menu = new ContextMenu(node_actions);
	var mixed_menu = new ContextMenu(node_actions.concat(["-"].concat(page_actions)));

	// add custom menus for content pages
	document.oncontextmenu=handle_context_menu;
	document.ondblclick=handle_double_click;
	Event.observe(document, "mousedown", onmousedown, true);

	var structure = new SiteStructure($("structure"));
	load_content_pages();

	var IsIE = navigator.userAgent.toLowerCase().indexOf('msie')!=-1;
  var ckeditor;

/*	var fckeditor = new FCKeditor('fckeditor');
	fckeditor.BasePath = "js/fckeditor/";
	fckeditor.Height = document.body.clientHeight-240;
	document.write(fckeditor.CreateHTML());
	var fck = $("fck"); */
    
{/literal}
{include file="obit_config.tpl"}
</script>	

{include file="adm_footer.tpl"}
