{literal}
	var EcommerceConfig = Class.create();
	EcommerceConfig.prototype = {
		initialize: function() {
{/literal}			
{capture name=pages}
	{html_options options=$pages}
{/capture}
			var f = '<form>\
					  <label for="paypal_business">Paypal account</label><input type="text" id="paypal_business" name="paypal_business" class="txt2"><br>\
					  <label>&nbsp;</label><small>Paypal account to receive payments to</small><br><br>\
					  <label for="return_page">Return page</label><select id="return_page" name="return_page">{$smarty.capture.pages|strip|escape:"quotes"}</select><br><br>\
					 </form>\
				';
{literal}
			this.wnd = Dialog.confirm(f, {windowParameters: {className:"alphacube", width:430, title:"Ecommerce configuration", showEffect: Element.show, hideEffect: Element.hide},
									      ok: this.onok.bind(this) });
			this.form = this.wnd.getContent().getElementsByTagName("FORM")[0];
			Form.focusFirstElement(this.form);
			// load data
			new Ajax.Request('ajax_products.php', {parameters:'action=load_config', onSuccess:this.onload.bind(this)});
		},

		onload: function(t, json) {
			jslog.debug(t.getResponseHeader("X-JSON"));
			var form = this.form;
			populate_controls(this.form, json);
			this.wnd.hideloader();
		},

		onsave: function(t) {
			jslog.debug(t.responseText);
			this.wnd.hide();
	    },
		
		onok: function() {
			this.wnd.showloader();
			new Ajax.Request('ajax_products.php', {parameters:'action=save_config&'+Form.serialize(this.form), onSuccess:this.onsave.bind(this)});
		}
	}
{/literal}
