<div class="content-switcher-container">
    <div class="content-switcher-content-1{if !isset($smarty.session.content_switcher) || $smarty.session.content_switcher == 'content1'} active{/if}"><a href="?content_switcher=1">{$params.content1}</a></div>
    <div class="content-switcher-content-2{if $smarty.session.content_switcher == 'content2'} active{/if}"><a href="?content_switcher=2">{$params.content2}</a></div>
</div>