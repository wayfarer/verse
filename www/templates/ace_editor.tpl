<link rel="stylesheet" href="/css/jquery-ui.css" type="text/css" />
<script type="text/javascript" src="/js/ace/ace.js" charset="utf-8"></script>
<script type="text/javascript" src="/js/ace/mode-css.js" charset="utf-8"></script>
<script type="text/javascript" src="/js/ace/theme-crimson_editor.js" charset="utf-8"></script>
<script type="text/javascript" src="/js/jquery-ui-1.8.16.resizable.min.js"></script>
<script type="text/javascript">
{literal}
jQuery(function() {
    var align = $('#edit-buttons').attr('align');
    if(align != '1') {
        var left = ($(window).width()-$('div.container').width())/2;
    }

    var controller;
    if($('#edit-buttons').attr('uc') == undefined) {
        controller = 'ajax_structure.php';
    } else {
        controller = 'structure_uc.php';
    }

    var load_editor = function() {
        var resize_editor = function() {
            var editor_cotainer = $('#editor-container');
            $('#editor').width(editor_cotainer.width()).height(editor_cotainer.height()-90);
            editor.resize();
        }

        var content = '',
            domain_name = '{/literal}{$domain_name}{literal}';

        var Mode = require("ace/mode/css").Mode;
        editor = ace.edit("editor");
        editor.setTheme("ace/theme/crimson_editor");
        editor.getSession().setMode(new Mode());

        $.post('http://'+domain_name+'/'+controller, {action: "load_site_css_frontend"}, function(data) {
            content = data;
            editor.getSession().setValue(data);
            editor.gotoLine(1);
        });

        $('#save-button').click(function() {
            $.post('http://'+domain_name+'/'+controller, {action: "save_site_css", css: editor.getSession().getValue()}, function() {
                content = editor.getSession().getValue();
                $('#editor-container > h3').after('<div id="editor-message">Changes have been saved successfully.</div>');
                $('#editor-message').animate({opacity: 0}, 5000, function() { $(this).remove(); });
            });
        });

        $('#close-button').click(function() {
            if(content == editor.getSession().getValue() || confirm("Changes haven't been saved. Do you want to close editor?")) {
                $('#site-css').html(content);
                if(align == '1') {
                    $('#editor-container').animate({width: '0'}, 1000, function() {
                        $('#editor-container').remove();
                        $('div.container').css('float', '');
                        $('#edit-buttons').show();
                    });
                } else {
                    $('#editor-container').animate({width: '0'}, {duration: 1000,
                        complete: function() {
                            $('#editor-container').remove();
                            $('#edit-buttons').show();
                        },
                        step: function() {
                            if($('div.container').css('float') != 'none' && $(this).width() <= left-parseInt($(this).css('margin-right'))) {
                                $('div.container').css('float', '');
                            }
                        }
                    });
                }
            }
        });

        $('#reload-button').click(function() {
            $('.content').fadeOut('fast');
            $('#site-css').html(editor.getSession().getValue());
            $('.content').fadeIn('fast');
        });

        $(window).bind('beforeunload', function() {
            if(content != editor.getSession().getValue()) {
                return "Changes haven't been saved. Do you want to leave this page?";
            }
        });

        $(window).resize(function() {
            $('#editor-container').height($(window).height());
            resize_editor();
        });

        $('#editor-container').resize(function() {
            resize_editor();
        });
    };

    $('#edit-buttons .editor-button').click(function() {
        if(align == '1') {
            $('div.container').css('float', 'left');
        }
        $(document.body).prepend('<div id="editor-container" class="ui-resizable ui-widget-content">\
                                    <h3 class="ui-widget-header">'+$(this).attr('value')+'</h3>\
                                    <div id="editor"></div>\
                                    <input type="button" id="save-button" class="editor-button" value="Save">\
                                    <input type="button" id="close-button" class="editor-button" value="Close">\
                                    <input type="button" id="reload-button" class="editor-button" value="Reload Page">\
                                  </div>');
        $('#editor').height($('#editor-container').height()-90);

        if(align == '1') {
            $('#editor-container').animate({width: '400'}, 1000, function() {
                $(this).resizable();
                load_editor();
            });
        } else {
            $('#editor-container').animate({width: '400'}, {duration: 1000,
                complete: function() {
                    $(this).resizable();
                    load_editor();
                },
                step: function() {
                    if($('div.container').css('float') == 'none' && $(this).width() >= left-parseInt($(this).css('margin-right'))) {
                        $('div.container').css('float', 'left');
                    }
                }
            });
        }
        $(this).parent().hide();
    });

    if(align == '1') {
        $('#edit-buttons').css('left', $('div.container').width()+10);
    } else {
        $('#edit-buttons').css('left', ($(window).width()-$('div.container').width())/2+$('div.container').width()+10);
    }
});
{/literal}
</script>