<script type="text/javascript" src="js/prototype.js"></script>
<h1>Shopping cart</h1>
<div id="cart"><img src="img/loader-big.gif"></div>
<br>
<a href="javascript: history.go(-1)">&laquo; back</a>
<script>
{literal}
    function remove_item(product_code) {
        new Ajax.Updater('cart', 'ajax_florist_cart.php', {parameters: "action=cart_remove&product_code="+product_code+"&cart_count="+get_cart_count(product_code)});
    }

    function add_item(product_code) {
        new Ajax.Updater('cart', 'ajax_florist_cart.php', {parameters: "action=cart_add&product_code="+product_code+"&cart_count="+get_cart_count(product_code)+"&update_list=1"});
    }

    function get_cart_count(product_code) {
        return Math.abs(parseInt($("cart-count-"+product_code).value));
    }

    function update_florist_cart_list() {
        new Ajax.Updater('cart', 'ajax_florist_cart.php', {parameters: "action=cart_list"});
    }

    update_florist_cart_list();
{/literal}
</script>
