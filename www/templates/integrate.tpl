<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<style type="text/css">@import url(calendar-brown.css);</style>
<script type="text/javascript" src="js/calendar.js"></script>
<script type="text/javascript" src="js/calendar-en.js"></script>
<script type="text/javascript" src="js/calendar-setup.js"></script>  
{literal}
<style>
img {border: 0}
a {text-decoration: none}
a:hover {text-decoration: underline}
{/literal}
{$site.css}
</style>
</head>

<body>
{$content}
<br>
<a href="javascript:history.go(-1)">&laquo; back</a>
</body>
</html>