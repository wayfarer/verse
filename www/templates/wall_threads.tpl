{if $threads}
<table cellspacing="0" cellpadding="0">
{foreach from=$threads item=thread}
<tr><td class="thread {cycle values="even,odd"}">
<span class="thread_info">{$thread.msg_cnt} message(s), last: {$thread.created_at_last|date_format:"%a, %b %e, %Y %H:%M"}</span>
<a href="#" onclick="open_thread({$wall_id},{$thread.thread_id}); return false;">{$thread.title}</a>
<div class="thread_footer">
{if ($user_id === $thread.user_id) or ($user.privileges == 1) }
<a href="#" onclick="delete_thread({$thread.thread_id}); return false;">delete discussion</a>
{/if}	
</div>
</td></tr>
{/foreach}
</table>
{else}
<div class="empty">
There are no discussions yet.
</div>
{/if}

{if $user_id}
<div class="reply">
<a href="#" onclick="$('#create-discussion-form').slideToggle('fast'); return false;">Create new discussion</a>
<form id="create-discussion-form" style="display:none">
<input type="hidden" name="wall_id" value="{$wall_id}">
<label>Title:</label><input type="text" name="title"><br>
<label>Message:</label>
<textarea style="width: 400px; height: 200px;" name="message"></textarea><br>
<label></label><input type="button" value="Create" onclick="create_thread(this.form)">
</div>
</form>
{/if}