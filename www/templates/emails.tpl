{include file="adm_header.tpl" menu="1"}
{include file="window.tpl"}
{include file="contextmenu.tpl"}
{include file="util.tpl"}

<h1>Email manager</h1>
<div class="actions">
	<a href="#" onclick="new EmailEditor(0); return false;">new email</a> / 
  <a href="#" onclick="new EmailAliasEditor(0); return false;">new email alias</a> / 
	<a href="#" onclick="new EmailConfig(); return false;">email config</a></div>
{literal}
<style>
	label.inline {
		display: inline;
		float: none;
	}
</style>
{/literal}
<script type="text/javascript">
{include file="populate_controls.tpl"}
{literal}
	var EmailEditor = Class.create();
	EmailEditor.prototype = {
		initialize: function(mailbox_id) {
			// create window
			this.mailbox_id = mailbox_id;
{/literal}
{capture name=domains_opts}
	{html_options options=$domain_list}
{/capture}
			var f = '<form>\
			         <label for="username">Email:</label><input type="text" name="username" id="username" disabled="disabled" class="txt" style="font-weight:bold; color: #000; font-size:12px"> <b>@{$postfix}</b><br>\
               <label for="cn">Full name <small>(Visible at Roundcube address book)</small>:</label><input type="text" name="cn" id="cn" class="txt"><br>\
               <label for="email_pass">Password:</label><input type="password" name="email_pass" id="email_pass"><br>\
					</form>\
				';
{literal}
			this.wnd = Dialog.confirm(f, {windowParameters: {className:"alphacube", width:540, title:"Email", showEffect: Element.show, hideEffect: Element.hide},
									      ok: this.onok.bind(this) });
			this.form = this.wnd.getContent().getElementsByTagName("FORM")[0];
			this.form.onsubmit = this.onsubmit.bind(this);
			if(mailbox_id) {
				// load data
				new Ajax.Request('emails.php', {parameters:'action=load_email&id='+mailbox_id, onSuccess:this.onload.bind(this)});
			}
			else {
				// enable name field for new user
				this.form.username.disabled = false;
				this.wnd.hideloader();
			}
		},

		onload: function(t, json) {
			jslog.debug(t.getResponseHeader("X-JSON"));
			populate_controls(this.form, json);
			this.wnd.hideloader();
		},

		onsubmit: function() {
			this.onok();
			return false;
		},

		onsave: function(t) {
			jslog.debug(t.responseText);
			update_emails();
			this.wnd.hide();
		},
		
		onok: function() {
			this.wnd.showloader();
			new Ajax.Request('emails.php', {parameters:'action=save_email&id='+this.mailbox_id+"&"+Form.serialize(this.form), onSuccess:this.onsave.bind(this)}); // TODO: handle AJAX errors here
		}
	}

  var EmailAliasEditor = Class.create();
  EmailAliasEditor.prototype = {
    initialize: function(mailbox_id) {
      // create window
      this.mailbox_id = mailbox_id;
{/literal}
      var f = '<form>\
               <label for="username">Alias email:</label><input type="text" name="username" id="username" disabled="disabled" class="txt" style="font-weight:bold; color: #000; font-size:12px"> <b>@{$postfix}</b><br>\
               <label for="aliases">Redirect to addresses (one per line)</label><textarea name="aliases" id="aliases" cols="35" rows="3"></textarea><br>\
          </form>\
        ';
{literal}
      this.wnd = Dialog.confirm(f, {windowParameters: {className:"alphacube", width:540, title:"Email", showEffect: Element.show, hideEffect: Element.hide},
                        ok: this.onok.bind(this) });
      this.form = this.wnd.getContent().getElementsByTagName("FORM")[0];
      this.form.onsubmit = this.onsubmit.bind(this);
      if(mailbox_id) {
        // load data
        new Ajax.Request('emails.php', {parameters:'action=load_alias&id='+mailbox_id, onSuccess:this.onload.bind(this)});
      }
      else {
        // enable name field for new user
        this.form.username.disabled = false;
        this.wnd.hideloader();
      }
    },

    onload: function(t, json) {
      jslog.debug(t.getResponseHeader("X-JSON"));
      populate_controls(this.form, json);
      this.wnd.hideloader();
    },

    onsubmit: function() {
      this.onok();
      return false;
    },

    onsave: function(t) {
      jslog.debug(t.responseText);
      update_emails();
      this.wnd.hide();
    },
    
    onok: function() {
      this.wnd.showloader();
      new Ajax.Request('emails.php', {parameters:'action=save_alias&id='+this.mailbox_id+"&"+Form.serialize(this.form), onSuccess:this.onsave.bind(this)}); // TODO: handle AJAX errors here
    }
  }
	
	var EmailConfig = Class.create();
	EmailConfig.prototype = {
		initialize: function() {
			var f = '<form>\
			         <label for="email">Default email:</label><input type="text" name="email" id="email" class="txt2"/>\
					 </form>\
				';
			this.wnd = Dialog.confirm(f, {windowParameters: {className:"alphacube", width:540, title:"Configure Default Email", showEffect: Element.show, hideEffect: Element.hide},
									      ok: this.onok.bind(this) });
			this.form = this.wnd.getContent().getElementsByTagName("FORM")[0];
			this.form.onsubmit = this.onsubmit.bind(this);
			// load data
			new Ajax.Request('ajax_emails.php', {parameters:'action=load_domain_email', onSuccess:this.onload.bind(this)});
		},

		onload: function(t, json) {
			jslog.debug(t.getResponseHeader("X-JSON"));
			populate_controls(this.form, json);
			this.wnd.hideloader();
		},

		onsubmit: function() {
			this.onok();
			return false;
		},

		onsave: function(t) {
			jslog.debug(t.responseText);
			update_emails();
			this.wnd.hide();
		},
		
		onok: function() {
			this.wnd.showloader();
			new Ajax.Request('ajax_emails.php', {parameters:'action=save_domain_email&'+Form.serialize(this.form), onSuccess:this.onsave.bind(this)});
		}
	}
	
	function mailbox_deleted(t) {
		update_emails();
	}	
	function delete_mailbox(mailbox_id) {
		if(confirm("Are you sure you want to permamenty delete mailbox?\nAll correspondence on server will be lost!")) {
			new Ajax.Request('emails.php', {parameters:'action=delete_email&id='+mailbox_id, onSuccess:mailbox_deleted}); // TODO: handle AJAX errors here
		}
	}
  function delete_alias(mailbox_id) {
    if(confirm("Are you sure you want to permamenty delete alias?")) {
      new Ajax.Request('emails.php', {parameters:'action=delete_alias&id='+mailbox_id, onSuccess:mailbox_deleted}); // TODO: handle AJAX errors here
    }
  }
	
	function update_emails() {
    document.location = document.location;
	}	
</script>	
{/literal}

<div id="emails">
<table width="600px" cellspacing="0" cellpadding="0" border="0" style="background-color: rgb(221, 238, 255);"><tbody><tr/><tr><td align="left"> << <</td><td align="center">{if $entries_count}1{else}0{/if} - {$entries_count}</td><td align="right">> >> </td></tr></tbody></table>
<table width="600px">
{foreach from=$data key=dn item=item}
{if in_array("mailUser", $item.objectClass)}
  <tr><td><strong>{$item.mail.0}</strong></td><td>mailbox</td><td><a onclick="new EmailEditor('{$item.mail.0}'); return false;" href="#">setup</a></td><td><a onclick="delete_mailbox('{$item.mail.0}'); return false;" href="#">delete</a></td></tr>
{else}
  <tr><td>{$item.mail.0}</td><td>alias: {foreach from=$item.mailForwardingAddress item=mfw}{$mfw}<br>{/foreach}</td><td><a onclick="new EmailAliasEditor('{$item.mail.0}'); return false;" href="#">setup</a></td><td><a onclick="delete_alias('{$item.mail.0}'); return false;" href="#">delete</a></td></tr>
{/if}		
{/foreach}
</table>
</div>

{include file="adm_footer.tpl"}
