<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<!-- {$smarty.const.VERSE_SERVER_NAME} -->
<html>
<head>
<meta name="encoding" content="text/html; charset=UTF-8" />  
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
{literal}
<style>
img {border: 0}
a {text-decoration: none}
a:hover {text-decoration: underline}
{/literal}
{$site.css}
</style>
</head>

<body>
{if $member}
  {include file="member_controls.tpl"}
{/if}
{$messages}
{$content}
</body>
</html>
