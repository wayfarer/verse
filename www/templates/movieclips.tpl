{include file="adm_header.tpl" menu="1"}
{include file="window.tpl"}
{include file="contextmenu.tpl"}
{include file="util.tpl"}

<h1>Movie Clips</h1>
<div class="actions"><a href="#" onclick="new MovieclipEditor(0); return false;">new movieclip</a></div>
<div id="movieclips"><img src="img/loader-big.gif"></div>
<br>
<small style="color:#999"><b>Note!</b> Upload system uses Adobe Flash plugin to show uploading progress, please update to latest version if you can't see the upload progress bar while uploading. <a href="http://get.adobe.com/flashplayer/" target="_blank">Click here</a> to update Flash plugin.</small>
<script type="text/javascript" src="js/swfupload.js"></script>
<script type="text/javascript">
{include file="populate_controls.tpl"}
{literal}
  var swf_upload_control;
	var MovieclipEditor = Class.create();
	MovieclipEditor.prototype = {
		initialize: function(movieclip_id) {
			this.movieclip_id = movieclip_id;
			// create dialog fields in form
      // iframe ajax-style upload method
//  						<label>&nbsp;</label><input type="text" id="txtFileName" disabled="true" style="border: 1px solid #888; background-color: #FFFFFF; color: #000" /><input id="btnBrowse" type="button" value="Browse..." xonclick="fileBrowse.apply(swf_upload_control)" /> (100 MB max)\
			var f = '\
          <form target="upload_iframe" method="post" enctype="multipart/form-data">\
					<label for="title">Title</label><input type="text" id="title" name="title" class="txt2"><br>\
  				<div id="flashUI">\
						<div>\
							<label>&nbsp;</label><input type="text" id="txtFileName" disabled="true" style="border: 1px solid #888; background-color: #FFFFFF; color: #000" /> <div id="btn_swfupload"></div> (100 MB max)\
						</div>\
						<div class="progress">\
              <div class="bar" id="progressbar"></div>\
						</div>\
					</div>\
					<div id="degradedUI">\
						<label>&nbsp;</label><input type="file" name="movieclip" id="resume_degraded" /> (100 MB max)\
					</div>\
  				<label for="protected">Protected</label><input type="checkbox" id="protected" name="protected" class="chk"><br>\
  				<label for="password">Password</label><input type="text" id="password" name="password" class="txt"><br>\
          <input type="hidden" name="action" value="save">\
          <input type="hidden" name="movieclip_id" value="'+movieclip_id+'">\
{/literal}<input type="hidden" name="z" value="{$session_id}">\{literal}
          <div class="alphacube_buttons">\
          <input type="submit" class="ok_button" value="Ok">\
          <input type="button" class="cancel_button" onclick="Dialog.cancelCallback()" value="Cancel">\
          </div>\
					</form>\
          <iframe name="upload_iframe" style="display:none">\
      ';
			this.wnd = Dialog.confirm(f, {windowParameters: {className:"alphacube", width:520, height:170, title:"Movieclip", showEffect: Element.show, hideEffect: Element.hide},
  								      ok: this.onok.bind(this) });
      // for iframe to access current window for closing
      document.dialog_window = this;
			this.form = this.wnd.getContent().getElementsByTagName("FORM")[0];
  		this.form.onsubmit = this.onsubmit.bind(this);

			Form.focusFirstElement(this.form);

      swf_upload_control = new SWFUpload({
  			// Flash Settings
				flash_url : "tools/swfupload.swf",

  			// Backend settings
				upload_url: "movieclips.php",	// Relative to the document
				file_post_name: "movieclip",

  			// Flash file settings
				file_size_limit : "102400",	// 100 MB
				file_types : "*.avi;*.mpg;*.flv;*.mov;*.wmv;*.jpg",
				file_types_description : "Video files (*.avi;*.mpg;*.flv;*.mov)",
				file_upload_limit : "0", // Even though I only want one file I want the user to be able to try again if an upload fails
				file_queue_limit : "1", // this isn't needed because the upload_limit will automatically place a queue limit
				
				file_queued_handler : this.file_queued.bind(this),
				
				upload_progress_handler : this.upload_progress.bind(this),
				upload_complete_handler : this.upload_complete.bind(this),
        swfupload_loaded_handler: this.swf_upload_ready.bind(this),
//				upload_success_handler: this.upload_success.bind(this),
//				upload_error_handler: this.upload_error.bind(this),

        button_placeholder_id : "btn_swfupload",
        button_image_url : "/img/xpbutton.png",
        button_width: 61,
        button_height: 22,

        button_action : SWFUpload.BUTTON_ACTION.SELECT_FILES,
				
				prevent_swf_caching: false

				// UI settings
//				ui_container_id : "flashUI",
//				degraded_container_id : "degradedUI"
				// Debug settings
//			  ,debug: true
			});

			if(movieclip_id) {
				// load data
				new Ajax.Request('movieclips.php', {parameters:'action=load&id='+movieclip_id, onSuccess:this.onload.bind(this)}); // TODO: handle AJAX errors here
			}
			else {
				// defaults
				this.wnd.hideloader();
			}
		},

		onload: function(t, json) {
			jslog.debug(t.getResponseHeader("X-JSON"));
			populate_controls(this.form, json);
			this.wnd.hideloader();
		},

		onsubmit: function() {
      var stats = swf_upload_control.getStats();
      if(stats.files_queued) {
        swf_upload_control.setPostParams(serialize_elements(Form.getElements(this.form)));
        swf_upload_control.startUpload();
        return false; // stop processing, upload_complete callback will close dialog
      }
      else {
  			this.onok();
  			return true; // UNUSUAL: continue submitting IFrame form
      }
		},

		onsave: function() {
			update_movieclips();
      try {
  			this.wnd.hide();
      }
      catch(e) {};
		},
		
		onok: function() {
			this.wnd.showloader();
		},
    
    swf_upload_ready: function() {
      $("degradedUI").style.display = "none";
      $("flashUI").style.display = "block";
    },
    
    file_queued: function(fileObj) {
      $("txtFileName").value = fileObj.name;
    },
    
    upload_progress: function(fileObj, bytesLoaded, bytesTotal) {
//      try {
        var total = 220;
        var part = bytesLoaded / bytesTotal;
        var percent = Math.ceil(part*100);
        var pbar = $("progressbar");
        pbar.style.width = total * (bytesLoaded / bytesTotal);
        pbar.innerHTML = percent+"%";
        // if(percent==100) this.upload_complete();
      //}
//      catch(e) {};
    },

    upload_complete: function(fileObj, server_data) {
      this.onsave();
    },
		
		upload_success: function(fileObj, server_data, response_received) {
//		  alert("Upload success: "+server_data);
		},
		
		upload_error: function(fileObj, error_code, message) {
//		  alert("Upload error: "+error_code+"; "+message);
		}
	}

  function serialize_elements(elements) {
		var data = elements.inject({}, function(result, element) {
      if (!element.disabled && element.name) {
        var key = element.name, value = element.value;
				if(element.type=="checkbox") {
				  value = element.checked?"1":"0";
				}
        if (value != null) {
         	if (key in result) {
            if (result[key].constructor != Array) result[key] = [result[key]];
            result[key].push(value);
          }
          else result[key] = value;
        }
      }
      return result;
    });
    return data;
  }

/*  function fileBrowse() {
  	$("txtFileName").value = "";
  	this.cancelUpload();
  	this.selectFile();
  } */

	function movieclip_deleted(t) {
		update_movieclips();
	}

  function movieclips_updated(t,t2) {
//    alert(t2);
  }

  function openwin(url, width, height) {
    var width = width || 500;
    var height = height || 500;
    var left = (screen.width-width)/2;
    var top = (screen.height-height)/2;
    win=window.open(url, null, config="scrollbars=no,resizable=no,toolbar=no,location=no,menubar=no,width="+width+",height="+height+",top="+top+",left="+left+"");
    win.focus();
  }

  function movieclip_preview(movieclip_id) {
    openwin('mcplayer.php?id='+movieclip_id+'&detectflash=false',460,375);
  }
	
	function delete_movieclip(movieclip_id) {
		if(confirm("Are you sure you want to delete movieclip?")) {
			new Ajax.Request('movieclips.php', {parameters:'action=delete&id='+movieclip_id, onSuccess:movieclip_deleted});
		}
	}
{/literal}	
	function page(start)
	{ldelim}
		new Ajax.Updater('movieclips', 'movieclips.php', {ldelim}parameters: "action=list&start="+start});  // add AJAX error handling here
	}
	function order(field)
	{ldelim}
		new Ajax.Updater('movieclips', 'movieclips.php', {ldelim}parameters: "action=list&order="+field});  // add AJAX error handling here
	}
	function update_movieclips()
	{ldelim}
//		new Ajax.Updater('movieclips', 'movieclips.php', {ldelim}parameters: "action=list", onException:movieclips_updated});  // add AJAX error handling here
		new Ajax.Updater('movieclips', 'movieclips.php', {ldelim}parameters: "action=list"}); 
	}	
	update_movieclips();
</script>	

{include file="adm_footer.tpl"}
