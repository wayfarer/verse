<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<!-- {$smarty.const.VERSE_SERVER_NAME} -->
<html>
<head>
<title>{$site.title}</title>
<meta name="encoding" content="text/html; charset=UTF-8" />  
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
{if $site.props.description}
<meta name="description" content="{$site.props.description}">
{/if}
{if $site.props.keywords}
<meta name="keywords" content="{$site.props.keywords}">
{/if}
{if $site.props.headhtml}
{$site.props.headhtml}
{/if}
<link rel="stylesheet" href="/css/menutop.css" type="text/css" />
<link rel="stylesheet" href="/css/general.css" type="text/css" />
<script type="text/javascript" src="/js/jquery-1.4.2.min.js"></script>
{if $page_type eq 3}
<link rel="stylesheet" href="/calendar-brown.css" type="text/css" />
<script type="text/javascript" src="/js/calendar.js"></script>
<script type="text/javascript" src="/js/calendar-en.js"></script>
<script type="text/javascript" src="/js/calendar-setup.js"></script>
{/if}
{if $allow_edit}
    {include file="ace_editor.tpl"}
{/if}
<style>
body {ldelim}
{if $site.props.bgcolor}
	background-color: {$site.props.bgcolor};
{/if}
{if $site.props.align==2}
	text-align: center;
{/if}
{if $site.props.bgimage}
  background-image: url({$site.props.bgimage|replace:" ":"%20"|leading_slash});
{/if}
}
div.container {ldelim}
	width: {$site.props.width};
{if $site.props.align==2}
	margin: 0 auto;
{/if}
	background-color: {$site.props.bgcolor};
	text-align: left;
}
table.container {ldelim}
	width: {$site.props.width};
}

td.left_submenu_container {ldelim}
    width: 150px;
}

div.left_submenu {ldelim}
    padding-right: 20px;
}
div.left_submenu a.parent-item {ldelim}
    font-weight: bold;
    padding-bottom: 5px;
}

div.left_submenu ul.submenu,
div.left_submenu ul.item-submenu {ldelim}
    list-style: none;
    margin: 0;
    padding: 0;
}

div.left_submenu ul.submenu li {ldelim}
    padding: 5px 0 0 10px;
}

div.main_menu ul {ldelim}
	margin: 0; 
	padding: 0; 
	list-style-type: none;
}

div.main_menu ul li {ldelim}
	position: relative;
	float: left;
	margin: 0;
	width: {$site.main_menu.height};
}

div.main_menu ul li a {ldelim}
	display: block;
}

div.main_menu {ldelim}
    {if $site.main_menu.font}
	font: {$site.main_menu.font};
    {/if}
}

div.main_menu ul.popup_menu {ldelim}
	width: {$site.popup_menu.width};
    {if $site.popup_menu.border}
	border: 1px solid black;
	{/if}
    {if $site.popup_menu.font}
	{if $site.popup_menu.font=="bold"}
	font-weight: bold;
	{else}
	font: {$site.popup_menu.font};
	{/if}
    {/if}
}

div.main_menu ul.popup_menu li {ldelim}
	float: none;
	height: {$site.popup_menu.height};
}

div.main_menu ul.popup_menu li a {ldelim}
	display: block;
	width: auto !important;
	width: 100%;
	height: 100%;
}

div.main_menu a.rollover {ldelim}
	color: {$site.main_menu.color};
	background-color: {$site.main_menu.bgcolor};
	text-decoration: none;
	padding-left: 2px;
}

div.main_menu a.rollover:hover {ldelim}
	color: {$site.main_menu.hvcolor};
	background-color: {$site.main_menu.hvbgcolor};
	text-decoration: none;
} 

div.main_menu ul.popup_menu li a.rollover {ldelim}
	color: {$site.popup_menu.color};
	background-color: {$site.popup_menu.bgcolor};
	text-decoration: none;
	padding-left: 2px;
}

div.main_menu ul.popup_menu li a.rollover:hover {ldelim}
	color: {$site.popup_menu.hvcolor};
	background-color: {$site.popup_menu.hvbgcolor};
	text-decoration: none;
}
</style>
<style type="text/css" id="site-css">
    {$site.css}
</style>
{literal}
<!--[if IE]>
<style type="text/css" media="screen">
body {behavior: url(/js/csshover.htc);} 
</style>
<![endif]-->
{/literal}
</head>
<body>
<div class="container">
    {include file="layout_header.tpl"}
    <table cellspacing="0" cellpadding="0" class="container">
        <tr>
            <td valign="top" height="{$site.main_menu.height}" class="main_menu_container">
                <div class="main_menu">
                    {if $site.main_menu.prespacer}
                        <div style="float: left">
                            <img src="{$site.main_menu.prespacer|leading_slash}">
                        </div>
                    {/if}
                    <ul>
                        {section name=node loop=$structure}
                          {if $structure[node].depth<2}
                            <li class="main-menu-item
                                {if $smarty.section.node.index == 0}
                                    menu-item-first
                                {/if}
                                {if $smarty.section.node.index == sizeof($structure)-1}
                                    menu-item-last
                                {/if}
                                {if $structure[node].active == 1}
                                    active
                                {/if}"
                                {if $structure[node].width && $structure[node].width != "default"}
                                    style="width: {$structure[node].width}"
                                {/if}
                            >
                                {if $structure[node].menu_type==1 || $site.menu_type==1}{* text node *}
                                    <a href={if $structure[node].internal_name}"/{$structure[node].internal_name}"
                                            {else}"" onclick="return false;"
                                            {/if}
                                            class="rollover"
                                            {if $structure[node].newwindow==1}
                                                target="_blank"
                                            {/if}
                                    >
                                        {$structure[node].display_name}
                                    </a>
                                {elseif $structure[node].menu_type==2 || $site.menu_type==2}{* static image node *}
                                    <a href={if $structure[node].internal_name}"/{$structure[node].internal_name}"{else}""
                                                onclick="return false;"
                                            {/if}
                                            title="{$structure[node].display_name}"
                                            {if $structure[node].newwindow==1}
                                                target="_blank"
                                            {/if}
                                    >
                                        <img src="{$structure[node].image|leading_slash}">
                                    </a>
                                {else}{* rollover node *}
                                    <a href={if $structure[node].internal_name}"/{$structure[node].internal_name}"
                                            {else}
                                                "" onclick="return false;"
                                            {/if}
                                            title="{$structure[node].display_name}"
                                            {if $structure[node].newwindow==1}
                                                target="_blank"
                                            {/if}
                                    >
                                        <img src="{$structure[node].image|leading_slash}" onmouseover="this.src='{$structure[node].himage|leading_slash}'"
                                                onmouseout="this.src='{$structure[node].image|leading_slash}'">
                                    </a>
                                    <script>
                                        // preload hover image
                                        {counter assign='cntr'}
                                        var myimage{$cntr} = new Image();
                                        myimage{$cntr}.src = "{$structure[node].himage|leading_slash}";
                                    </script>
	                            {/if}
                                {if isset($structure[node.index_next])}
                                    {if $structure[node].depth==$structure[node.index_next].depth || $structure[node.index_next].depth>1}
                                        </li>
                                    {elseif $structure[node].depth<$structure[node.index_next].depth}
	                                    <ul class="popup_menu">
	                                        {if $site.popup_menu.prespacer}
		                                        <img src="{$site.popup_menu.prespacer|leading_slash}">
                                            {/if}
                                    {else}
                                        {section name=i loop=$structure[node].depth-$structure[node.index_next].depth}
	                                        </li>
	                                        {if $site.popup_menu.postspacer}
		                                        <img src="{$site.popup_menu.postspacer|leading_slash}">
	                                        {/if}
	                                        </ul>
	                                    {/section}
	                                    </li>
                                    {/if}
                                {else}
                                    </li>
                                    {if $structure[node].depth > 0}
                                        </ul>
                                        </li>
                                    {/if}
                                {/if}
                          {elseif $structure[node].depth>$structure[node.index_next].depth && $structure[node.index_next].depth==0}
                            </ul>
                          {/if}
                        {/section}
                    </ul>
                    {if $site.main_menu.postspacer}
                        <img src="{$site.main_menu.postspacer|leading_slash}" style="float:right">
                    {/if}
                </div>
            </td>
        </tr>
        <tr>
            <td valign="top" class="content_container">
              <table cellspacing="0" cellpadding="0" >
                  <tr>
                    {if $show}
                        <td valign="top" class="left_submenu_container">
                            <div class="left_submenu">
                                {if $structure[$start_index].menu_type==1 || $site.menu_type==1}{* text node *}
                                    <a href={if $structure[$start_index].internal_name}"/{$structure[$start_index].internal_name}"
                                            {else}"" onclick="return false;"
                                            {/if}
                                            class="rollover parent-item"
                                            {if $structure[node].newwindow==1}
                                                target="_blank"
                                            {/if}
                                    >
                                        {$structure[$start_index].display_name}
                                    </a>
                                {elseif $structure[$start_index].menu_type==2 || $site.menu_type==2}{* static image node *}
                                    <a href={if $structure[$start_index].internal_name}"/{$structure[$start_index].internal_name}"{else}""
                                                onclick="return false;"
                                            {/if}
                                            title="{$structure[$start_index].display_name}"
                                            {if $structure[$start_index].newwindow==1}
                                                target="_blank"
                                            {/if}
                                    >
                                        <img src="{$structure[$start_index].image|leading_slash}">
                                    </a>
                                {else}{* rollover node *}
                                    <a href={if $structure[$start_index].internal_name}"/{$structure[$start_index].internal_name}"
                                            {else}
                                                "" onclick="return false;"
                                            {/if}
                                            title="{$structure[$start_index].display_name}"
                                            {if $structure[$start_index].newwindow==1}
                                                target="_blank"
                                            {/if}
                                    >
                                        <img src="{$structure[$start_index].image|leading_slash}" onmouseover="this.src='{$structure[$start_index].himage|leading_slash}'"
                                                 onmouseout="this.src='{$structure[$start_index].image|leading_slash}'">
                                    </a>
                                    <script>
                                        // preload hover image
                                        {counter assign='cntr'}
                                        var myimage{$cntr} = new Image();
                                        myimage{$cntr}.src = "{$structure[$start_index].himage|leading_slash}";
                                    </script>
	                            {/if}
                                <ul class="submenu">
                                    {section name=link start=$start_index+1 loop=$structure step=1}
                                        {if ($structure[link].depth == 1)}
                                            <li class="submenu-item
                                                {if $smarty.section.link.index == $start_index+1}
                                                    first
                                                {/if}
                                                {if $structure[link].active == 1}
                                                    active
                                                {/if}
                                                {if $structure[link.index_next].depth == 0}
                                                    last
                                                {/if}
                                                "
                                            >
                                                {if $structure[link].menu_type==1 || $site.menu_type==1}{* text node *}
                                                    <a href={if $structure[link].internal_name}"/{$structure[link].internal_name}"
                                                            {else}
                                                                "" onclick="return false;"
                                                            {/if}
                                                            class="rollover"
                                                            {if $structure[link].newwindow==1}
                                                                target="_blank"
                                                            {/if}
                                                    >
                                                        {$structure[link].display_name}
                                                     </a>
                                                {elseif $structure[link].menu_type==2 || $site.menu_type==2}{* static image node *}
                                                    <a href={if $structure[link].internal_name}"/{$structure[link].internal_name}"{else}""
                                                                onclick="return false;"
                                                            {/if}
                                                            title="{$structure[link].display_name}"
                                                            {if $structure[link].newwindow==1}
                                                                target="_blank"
                                                            {/if}
                                                    >
                                                        <img src="{$structure[link].image|leading_slash}">
                                                    </a>
                                                {else}{* rollover node *}
                                                    <a href={if $structure[link].internal_name}"/{$structure[link].internal_name}"
                                                            {else}
                                                                "" onclick="return false;"
                                                            {/if}
                                                            title="{$structure[link].display_name}"
                                                            {if $structure[link].newwindow==1}
                                                                target="_blank"
                                                            {/if}
                                                    >
                                                        <img src="{$structure[link].image|leading_slash}" onmouseover="this.src='{$structure[link].himage|leading_slash}'"
                                                                onmouseout="this.src='{$structure[link].image|leading_slash}'">
                                                    </a>
                                                    <script>
                                                        // preload hover image
                                                        {counter assign='cntr'}
                                                        var myimage{$cntr} = new Image();
                                                        myimage{$cntr}.src = "{$structure[link].himage|leading_slash}";
                                                    </script>
	                                            {/if}

                                            {if $structure[link].active == 1 && $structure[link.index_next].depth == 2 && !isset($show_item_submenu)}
                                                {assign var="show_item_submenu" value=true}
                                                <ul class="item-submenu">
                                            {else}
                                                </li>
                                            {/if}
                                        {/if}
                                        {if $structure[link].depth == 2 && isset($show_item_submenu) && $show_item_submenu}
                                            <li class="item-submenu-item
                                                {if $structure[link].depth > $structure[link.index_prev].depth}
                                                    first
                                                {/if}
                                                {if $structure[link].active == 1}
                                                    active
                                                {/if}
                                                {if $structure[link.index_next].depth > $structure[link].depth}
                                                    last
                                                {/if}
                                                "
                                            >
                                                {if $structure[link].menu_type==1 || $site.menu_type==1}{* text node *}
                                                    <a href={if $structure[link].internal_name}"/{$structure[link].internal_name}"
                                                            {else}
                                                                "" onclick="return false;"
                                                            {/if}
                                                            class="rollover"
                                                            {if $structure[link].newwindow==1}
                                                                target="_blank"
                                                            {/if}
                                                    >
                                                        {$structure[link].display_name}
                                                    </a>
                                                {elseif $structure[link].menu_type==2 || $site.menu_type==2}{* static image node *}
                                                    <a href={if $structure[link].internal_name}"/{$structure[link].internal_name}"{else}""
                                                                onclick="return false;"
                                                            {/if}
                                                            title="{$structure[link].display_name}"
                                                            {if $structure[link].newwindow==1}
                                                                target="_blank"
                                                            {/if}
                                                    >
                                                        <img src="{$structure[link].image|leading_slash}">
                                                    </a>
                                                {else}{* rollover node *}
                                                    <a href={if $structure[link].internal_name}"/{$structure[link].internal_name}"
                                                            {else}
                                                                "" onclick="return false;"
                                                            {/if}
                                                            title="{$structure[link].display_name}"
                                                            {if $structure[link].newwindow==1}
                                                                target="_blank"
                                                            {/if}
                                                    >
                                                        <img src="{$structure[link].image|leading_slash}" onmouseover="this.src='{$structure[link].himage|leading_slash}'"
                                                                                                          onmouseout="this.src='{$structure[link].image|leading_slash}'">
                                                    </a>
                                                    <script>
                                                        // preload hover image
                                                        {counter assign='cntr'}
                                                        var myimage{$cntr} = new Image();
                                                        myimage{$cntr}.src = "{$structure[link].himage|leading_slash}";
                                                    </script>
                                                {/if}

                                            </li>
                                            {if $structure[link.index_next].depth < $structure[link].depth && isset($show_item_submenu) && $show_item_submenu}
                                                </ul>
                                                </li>
                                                {assign var="show_item_submenu" value=false}
                                            {/if}
                                        {/if}
                                        {if $structure[link.index_next].depth == 0}
                                            {php}break;{/php}
                                        {/if}
                                    {/section}
                                </ul>
                            </div>
                        </td>
                    {/if}
                    <td>
                        <div class="content">
                            {if $member}
                                {include file="member_controls.tpl"}
                            {/if}
                            {$messages}
                            {$content}
                        </div>
                    </td>
                  </tr>
              </table>
            </td>
        </tr>
    </table>
    <div class="footer">
        {$site.footer}
    </div>
</div>
{if $allow_edit}
    <div id="edit-buttons" align="{$site.props.align}">
        <input type="button" id="edit-css-button" class="editor-button" value="Edit CSS" editor="css">
    </div>
{/if}
</body>
</html>