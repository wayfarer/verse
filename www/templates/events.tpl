{include file="adm_header.tpl" menu="1"}
{include file="window.tpl"}
{include file="contextmenu.tpl"}
{include file="util.tpl"}

<h1>Events management</h1>
<div class="actions"><a href="#" onclick="new EventEditor(0); return false;">new event</a></div>
<div id="events"><img src="img/loader-big.gif"></div>
{literal}
<style>
	input.txt {
		width: 300px;
	}
</style>
<script type="text/javascript" src="js/nicEdit.js"></script>
<script type="text/javascript">
	var EventEditor = Class.create();
	EventEditor.prototype = {
		initialize: function(event_id) {
			// create window
//			this.wnd = new WindowClass("Event", 500, 250, [this.onok.bind(this), this.oncancel.bind(this)]);
			this.event_id = event_id;
			// create form
//			this.form = Builder.node("FORM");
			// create dialog fields in form
			var f = '<form>\
					<label for="timestamp">Event Time</label><input type="text" id="timestamp" name="timestamp" class="datetime"><input type="button" name="timestamp_b" id="timestamp_b" value=".."><br>\
          <label for="description">Description</label><div class="wysiwyg"><textarea name="description" id="description" style="width: 520px; height: 200px"></textarea></div><br>\
					</form>\
				';
			this.wnd = Dialog.confirm(f, {windowParameters: {className:"alphacube", width:670, height:320, title:"Event", showEffect: Element.show, hideEffect: Element.hide},
									      ok: this.onok.bind(this) });
			this.form = this.wnd.getContent().getElementsByTagName("FORM")[0];
			this.form.onsubmit = this.onsubmit.bind(this);

      var myNicEditor = new nicEditor({buttonList:['bold','italic','underline','strikethrough','subscript','superscript','ol','ul','hr','link','unlink','fontFormat','xhtml'], iconsPath: "/img/nicEditorIcons.gif", maxHeight: 200}).panelInstance('description');
			
      Calendar.setup( {
				inputField : "timestamp",
				ifFormat : "%Y-%m-%d %I:%M %p",
				button : "timestamp_b",
				showsTime: true,
				timeFormat: 12,
				step: 1
			}
			);
			Form.focusFirstElement(this.form);
			if(event_id) {
				// load data
				new Ajax.Request('ajax_events.php', {parameters:'action=load&id='+event_id, onSuccess:this.onload.bind(this)}); // TODO: handle AJAX errors here
			}
			else {
				// defaults
				var d = new Date();
				this.form.elements["timestamp"].value = d.print("%Y-%m-%d 12:00 PM");
				this.wnd.hideloader();
			}
		},

		onload: function(t, json) {
			jslog.debug(t.getResponseHeader("X-JSON"));
			var form = this.form;
			$H(json).each(function(elem) {
					if(form.elements[elem.key].type=="text" || form.elements[elem.key].type=="textarea") {
						form.elements[elem.key].value = elem.value;
					}
					else { // treat as radios
						$A(form.elements[elem.key]).each(function(re) {
								if(re.value==elem.value) {
									re.checked = true;
								}
							}
						);
					}
				}
			);
      nicEditors.findEditor("description").setContent(this.form.elements["description"].value);
			this.wnd.hideloader();
		},

		onsubmit: function() {
			this.onok();
			return false;
		},

		onsave: function(t) {
			jslog.debug(t.responseText);
			update_events();
			this.wnd.hide();
		},
		
		onok: function() {
			this.wnd.showloader();
      nicEditors.findEditor("description").saveContent();
			new Ajax.Request('ajax_events.php', {parameters:'action=save&id='+this.event_id+"&"+Form.serialize(this.form), onSuccess:this.onsave.bind(this)}); // TODO: handle AJAX errors here
		}
	}

	function event_deleted(t)
	{
		update_events();
	}
	
	function delete_event(event_id)
	{
		if(confirm("Are you sure you want to delete event?")) {
			new Ajax.Request('ajax_events.php', {parameters:'action=delete&id='+event_id, onSuccess:event_deleted}); // TODO: handle AJAX errors here
		}
	}
	
	function page(start)
	{
		new Ajax.Updater('events', 'ajax_events.php', {parameters: "action=list&start="+start});  // add AJAX error handling here
	}
	function order(field)
	{
		new Ajax.Updater('events', 'ajax_events.php', {parameters: "action=list&order="+field});  // add AJAX error handling here
	}
	function update_events()
	{
		new Ajax.Updater('events', 'ajax_events.php', {parameters: "action=list"});  // add AJAX error handling here
	}	
	update_events();
</script>	
{/literal}

{include file="adm_footer.tpl"}
