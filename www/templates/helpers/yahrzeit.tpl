<script src="js/yahrzeit.js"></script>
<style>
{literal}
label {
  float: none;
  display: inline;
}
{/literal}
</style>
<form>
  <label for="greg_day">Day: </label>
  <select name="greg_day" id="greg_day">
    <script>
      document.write(generate_day_options());
    </script>    
  </select>
  <label for="greg_month">Month: </label>
  <select name="greg_month" id="greg_month">
    <script>
      document.write(generate_month_options());
    </script>    
  </select>
  <label for="greg_year">Year: </label>
  <select name="greg_year" id="greg_year">
    <script>
      var today = new Date();
      document.write(generate_year_options(1900, today.getFullYear()));
    </script>    
  </select><br>
  <label for="after_sunset">Death occured after sunset?</label><input type="checkbox" name="after_sunset" id="after_sunset"><br>
  <input type="button" onclick="calculate_yahrzeit(this, 'yahrzeit'{if $to_year}, {$to_year}{/if})" value="Calculate">
</form>
<div id="yahrzeit"></div>
