<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en">
<head>
<title>{$site.title}</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
{if $site.props.description}
<meta name="description" content="{$site.props.description}">
{/if}
{if $site.props.keywords}
<meta name="keywords" content="{$site.props.keywords}">
{/if}
{if $site.props.headhtml}
{$site.props.headhtml}
{/if}
<style type="text/css">@import url(css/uc.css);</style>
<link rel="stylesheet" href="/css/general.css" type="text/css" />
<script type="text/javascript" src="/js/jquery-1.4.2.min.js"></script>
{if $page_type eq 3}
<style type="text/css">@import url(calendar-brown.css);</style>
<script type="text/javascript" src="js/calendar.js"></script>
<script type="text/javascript" src="js/calendar-en.js"></script>
<script type="text/javascript" src="js/calendar-setup.js"></script>
{/if}
{if $allow_edit}
    {include file="ace_editor.tpl"}
{/if}
<style>
body {ldelim}
{if $site.props.bgcolor}
	background-color: {$site.props.bgcolor};
{/if}
{if $site.props.align==2}
	text-align: center;
{/if}
{if $site.props.bgimage}
	background-image: url({$site.props.bgimage});
{/if}
}
div.container {ldelim}
  width: {$site.props.width};
{if $site.props.align==2}
  margin: 0 auto;
{/if}
  text-align: left;
}
table.container {ldelim}
	width: {$site.props.width};
}
{literal}
body {
  margin: 0;
}
img {
  border: 0;
}
div.content {
  margin-top: 10px;
}
div.content div {
  margin: 0 auto;
}
div.content table {
  margin: 0 auto;
}
div.uc_menu {
  text-align: center;
}
{/literal}
</style>
<style type="text/css" id="site-css">
    {$site.css}
</style>
</head>
<body>
<div class="container">
  {include file="layout_header.tpl"}
  <div class="uc_menu">
    {$uc_menu}
  </div>
  <div class="content">
    {$content}
  </div>
  <div class="footer">
    {$site.footer}
  </div>
</div>
{if $allow_edit}
    <div id="edit-buttons" align="{$site.props.align}" uc>
        <input type="button" id="edit-css-button" class="editor-button" value="Edit CSS" editor="css">
    </div>
{/if}
</body>
</html>
