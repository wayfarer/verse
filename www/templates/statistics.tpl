{include file="adm_header.tpl" menu="1"}
<link rel="stylesheet" type="text/css" href="/css/jquery-ui.css" />
<h1>Website statistics</h1>
<div style="float:right">
<a href="#more_options">Click here for pre-2010 statistics and more statistics options</a>
</div>
{include file="js_global_variables.tpl"}
{include file="statistics_period_select.tpl"}
{if $token_auth}
<div id="widgetIframe">
{capture name=dashboard_url}index.php?module=Widgetize&action=iframe&moduleToWidgetize=Dashboard&actionToWidgetize=index&idSite={if $user_issuper}1{else}{$smarty.session.domain_id}{/if}&period={$period}&date={$date}{/capture}
<iframe src="https://twintierstech.net/piwik/index.php?module=VerseIntegration&action=logme_token&login={$login}.{$smarty.session.domain_name}&token_auth={$token_auth}&url={$smarty.capture.dashboard_url|urlencode}" frameborder="0" marginheight="0" marginwidth="0" width="100%" height="1300"></iframe>
</div>
{/if}
<br/>
<a name="more_options"></a>
<h3>We are upgrading our statistics services!</h3>
<p>We have started to collect statistics using a new software called Piwik since January 1, 2010. 
<em>(You can see it in action above)</em></p>

<p>We are still collecting stats using the old software (phpMyVisites), 
but we are planning to stop collecting new statistics with phpMyVisities 
(though you will still <strong>be able to access your old historical data</strong> in phpMyVisites).</p>

<p>With this system, if you would like to compare statistics from 2009 and earlier with statistics from 2010 
and on you will not be able to do so using one statistics system (you will need to compare the numbers 
from each program separately). If you would like to be able to continue to use phpMyVisites to collect 
current statistics so that you may continue to compare numbers from before 2010 with the numbers after 2010 
in the same graph, we can enable that for you.</p> 

<p>If you would like to us to enable phpMyVisites to continue to collect data for you, please check the box 
below and we will contact you regading this.</p>
 
<strong><label for="keep_phpmv">Contact me about continuing phpMyVisites data collection:</label> <input type="checkbox" name="keep_phpmv" id="keep_phpmv" onclick="keep_phpmv()"{if $keep_phpmv} checked{/if}/></strong><br/><br/>
Now you can:<br/>
<h2><a href="phpmv_login.php">go to phpMyVisites (statistics for pre 2010 periods)</a></h2>
<h2><a href="piwik_login.php">go to full Piwik UI (statistics since Jan 1st, 2010)</a></h2>
<br/><br/><br/><br/>
{literal}
<script>
  function keep_phpmv() {
	$.post("statistics.php", {action: "keep_phpmv", keep_phpmv: $("#keep_phpmv").attr('checked')?1:0});
	$("#keep_phpmv").after(" saved!");
  }
</script>
{/literal}
