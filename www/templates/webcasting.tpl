{include file="adm_header.tpl" menu="1"}
{include file="window.tpl"}
{include file="util.tpl"}

<h1>Webcasting management</h1>
<div class="actions"><a href="#" onclick="new WebcastingEditor(0); return false;">new webcasting</a> / <a href="#" onclick="new WebcastingConfig(); return false;">webcasting config</a></div>
<div id="webcastings"><img src="img/loader-big.gif"></div>
<script type="text/javascript">
{include file="populate_controls.tpl"}
{literal}
	var WebcastingEditor = Class.create();
	WebcastingEditor.prototype = {
		initialize: function(webcasting_id) {
			// create window
			this.webcasting_id = webcasting_id;
{/literal}
{capture name=obituaries}
	{html_options options=$obituaries}
{/capture}
			var f = '<form>\
          <label for="obituary_id">Obituary:</label><select id="obituary_id" name="obituary_id">{$smarty.capture.obituaries|strip|escape:"quotes"}</select><br>\
					<label for="name">Title</label><input type="text" id="name" name="name" class="txt2"><br><br>\
					<label for="schedule_from">Schedule From</label><input type="text" id="schedule_from" name="schedule_from" class="txt"> <input type="button" id="schedule_from_b" value=" .. "><br>\
					<label>&nbsp;</label><small>* leave empty to start it manually</small><br>\
					<label for="schedule_to">Schedule Until</label><input type="text" id="schedule_to" name="schedule_to" class="txt"> <input type="button" id="schedule_to_b" value=" .. "><br>\
					<label>&nbsp;</label><small>* leave empty to end it manually</small><br><br>\
					<label for="record">Record it?</label><input type="checkbox" id="record" name="record" checked="1"><br><br>\
					<label for="enabled">Enabled</label><input type="checkbox" id="enabled" name="enabled" checked="1">\
					</form>\
				';
{literal}
			this.wnd = Dialog.confirm(f, {windowParameters: {className:"alphacube", width:500, title:"Webcasting", showEffect: Element.show, hideEffect: Element.hide},
									      ok: this.onok.bind(this) });
			this.form = this.wnd.getContent().getElementsByTagName("FORM")[0];
			this.form.onsubmit = this.onsubmit.bind(this);
			this.form.obituary_id.onchange = this.onchange.bind(this);

			Calendar.setup( {
				inputField : "schedule_from",
				ifFormat : "%Y-%m-%d %H:%M:00",
				button : "schedule_from_b",
				step: 1,
				showsTime: true
			} );

			Calendar.setup( {
				inputField : "schedule_to",
				ifFormat : "%Y-%m-%d %H:%M:00",
				button : "schedule_to_b",
				step: 1,
				showsTime: true
			} );

			Form.focusFirstElement(this.form);
			if(webcasting_id) {
				// load data
				new Ajax.Request('webcasting.php', {parameters:'action=load&id='+webcasting_id, onSuccess:this.onload.bind(this)}); // TODO: handle AJAX errors here
			}
			else {
				this.wnd.hideloader();
			}
		},

		onload: function(t, json) {
			jslog.debug(t.getResponseHeader("X-JSON"));
  		jslog.debug(t.responseText);
			populate_controls(this.form, json);
			if(json.status<=1) {
				this.form["enabled"].checked = parseInt(json.status);
			}
			else {
				this.form["enabled"].disabled = true;			
			}
			this.wnd.hideloader();
		},

		onsubmit: function() {
			this.onok();
			return false;
		},

		onsave: function(t) {
			jslog.debug(t.responseText);
			update_webcastings();
			this.wnd.hide();
		},
		
		onok: function() {
			this.wnd.showloader();
			new Ajax.Request('webcasting.php', {parameters:'action=save&id='+this.webcasting_id+"&"+Form.serialize(this.form), onSuccess:this.onsave.bind(this)}); // TODO: handle AJAX errors here
		},
		
		onchange: function() {
			var id = this.form.obituary_id.options[this.form.obituary_id.selectedIndex].value;
			if(id>0) {
				var text = this.form.obituary_id.options[this.form.obituary_id.selectedIndex].text;
				var datepos = text.indexOf("(");
				if(datepos) {
					text = text.substr(0, datepos-1);
				}
				this.form.name.value = text+" webcasting";
			}
		}
	}

	var NotifyDialog = Class.create();
	NotifyDialog.prototype = {
		initialize: function(webcasting_id) {
			// create window
			this.webcasting_id = webcasting_id;
{/literal}
			var f = '<form>\
					<label for="emailto">To:</label><input type="text" id="emailto" name="emailto" class="txt3" readonly="1"><br>\
					<label for="subject">Subject:</label><input type="text" id="subject" name="subject" class="txt3"><br>\
					<label for="emailtext">Email text:</label><textarea name="email_text" id="email_text" style="width: 640px; height:250px"></textarea><br>\
					</form>\
				';
{literal}
			this.wnd = Dialog.confirm(f, {windowParameters: {className:"alphacube", width:660, title:"Webcasting notify e-mail", showEffect: Element.show, hideEffect: Element.hide},
									      ok: this.onok.bind(this) });
			this.form = this.wnd.getContent().getElementsByTagName("FORM")[0];
			this.form.onsubmit = this.onsubmit.bind(this);

			Form.focusFirstElement(this.form);
			
			// load data
			new Ajax.Request('webcasting.php', {parameters:'action=load_email&id='+webcasting_id, onSuccess:this.onload.bind(this)}); // TODO: handle AJAX errors here
		},

		onload: function(t, json) {
			jslog.debug(t.getResponseHeader("X-JSON"));
  		jslog.debug(t.responseText);
			populate_controls(this.form, json);
			this.wnd.hideloader();
		},

		onsubmit: function() {
			this.onok();
			return false;
		},

		onsave: function(t) {
			jslog.debug(t.responseText);
			this.wnd.hide();
		},
		
		onok: function() {
			this.wnd.showloader();
			new Ajax.Request('webcasting.php', {parameters:'action=send_email&id='+this.webcasting_id+"&"+Form.serialize(this.form), onSuccess:this.onsave.bind(this)}); // TODO: handle AJAX errors here
		}
	}

	var WebcastingConfig = Class.create();
	WebcastingConfig.prototype = {
		initialize: function() {
			// create window
{/literal}
			var f = '<form>\
					Notification e-mail template<br><br>\
					<label for="email_template_subject">Subject</label><input type="text" id="email_template_subject" name="email_template_subject" class="txt2"><br><br>\
					<label for="email_template">E-mail template:</label><textarea name="email_template" id="email_template" style="width: 600px; height:300px"></textarea><br>\
					</form>\
				';
{literal}
			this.wnd = Dialog.confirm(f, {windowParameters: {className:"alphacube", width:620, title:"Webcasting Config", showEffect: Element.show, hideEffect: Element.hide},
									      ok: this.onok.bind(this) });
			this.form = this.wnd.getContent().getElementsByTagName("FORM")[0];
			this.form.onsubmit = this.onsubmit.bind(this);

			Form.focusFirstElement(this.form);
			
			// load data
			new Ajax.Request('webcasting.php', {parameters:'action=load_config', onSuccess:this.onload.bind(this)}); // TODO: handle AJAX errors here
		},

		onload: function(t, json) {
			jslog.debug(t.getResponseHeader("X-JSON"));
  		jslog.debug(t.responseText);
			populate_controls(this.form, json);
			this.wnd.hideloader();
		},

		onsubmit: function() {
			this.onok();
			return false;
		},

		onsave: function(t) {
			jslog.debug(t.responseText);
			this.wnd.hide();
		},
		
		onok: function() {
			this.wnd.showloader();
			new Ajax.Request('webcasting.php', {parameters:'action=save_config&'+Form.serialize(this.form), onSuccess:this.onsave.bind(this)}); // TODO: handle AJAX errors here
		}
	}

	var ActionDialog = Class.create();
	ActionDialog.prototype = {
		initialize: function(webcasting_id, action) {
			// create window
			this.webcasting_id = webcasting_id;			
			this.action = action;
			if(action==1) {
				this.action_string = "start";
			} else if(action==2) {
				this.action_string = "stop";
			}
			else if(action==3) {
				this.action_string="restart";
			}
{/literal}
			var f = '<form>\
						Do you like to '+this.action_string+' webcasting now?\
						<input type="button" name="action" id="action">\
					</form>\
				';
{literal}
			this.wnd = Dialog.alert(f, {windowParameters: {className:"alphacube", width:300, title:"Please, confirm", showEffect: Element.show, hideEffect: Element.hide},
									      ok: this.onok.bind(this) });
			this.form = this.wnd.getContent().getElementsByTagName("FORM")[0];
			this.form.action.onclick = this.onexecute.bind(this);
			
			this.wnd.hideloader();
		},

		onsuccess: function(t) {
			jslog.debug(t.responseText);		
			this.wnd.hide();
			update_webcastings();
		},
		
		onexecute: function(t) {
			this.wnd.showloader();
			new Ajax.Request('webcasting.php', {parameters:'action='+this.action_string+'_now&id='+this.webcasting_id, onSuccess:this.onsuccess.bind(this)}); // TODO: handle AJAX errors here
		},
		
		onok: function() {
			this.wnd.hide();
		}
	}

	function update_webcastings()
	{
		new Ajax.Updater('webcastings', 'webcasting.php', {parameters: "action=list"});  // add AJAX error handling here
	}	

	function delete_user(user_id)
	{
		if(confirm("Are you sure you want to delete user?")) {
			new Ajax.Request('webcasting.php', {parameters:'action=delete&id='+user_id, onSuccess:user_deleted}); // TODO: handle AJAX errors here
		}
	}
	function user_deleted(t)
	{
		update_users();
	}
	
	update_webcastings();
</script>	
{/literal}

{include file="adm_footer.tpl"}
	