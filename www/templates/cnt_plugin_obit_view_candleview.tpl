{if $notapproved}
<div style="color: #008000">
Thank you for submitting your thoughts! Your message may not be immediately available for public viewing, but be sure that the family will receive it.
</div>
<br>
{/if}
{if $justpublished}
<div style="color: #008000">
The candle has been published.
</div>
<br>
{/if}
<div style="color: #D0822E; xfont-weight: bold">
	Thoughts from {$candle.name}<br>{$candle.timestamp}
</div>
<hr size="1">
<div>
	{$candle.thoughts}
</div>
<div style="margin-top: 20px">
<a href="/obituary_view/{$candle.obituary_id}">&laquo; Back</a>
</div>
