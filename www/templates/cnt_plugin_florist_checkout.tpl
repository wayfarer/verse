<style>
    div.error {ldelim}
        color: #F00000;
        margin-bottom: 10px;
    }
    label {ldelim}
        margin-right: 5px;
    }
</style>
<h1>Checkout</h1>
{if !$empty}
    {if !$thanks}
        {if $checkout_form}
            {if $errors}
                <div class="error">
                    {foreach from=$errors item=error}
                        {$error}<br/>
                    {/foreach}
                </div>
            {/if}
            <div style="margin: 10px 0; font-weight: bold">
                Please fill out the form below to make your order. Thank you!
            </div>
            <form id="florist-checkout-form" method="post">
                <input id="checkout_form" name="checkout_form" type="hidden" value="1">
                <input id="selected_recipient_state" name="selected_recipient_state" type="hidden" value="{$form.recipient_state|default:""}">
                <input id="selected_recipient_country" name="selected_recipient_country" type="hidden" value="{$form.recipient_country|default:""}">
                <input id="selected_delivery_date" name="selected_delivery_date" type="hidden" value="{$form.delivery_date|default:""}">
                <input id="selected_customer_state" name="selected_customer_state" type="hidden" value="{$form.customer_state|default:""}">
                <input id="selected_customer_country" name="selected_customer_country" type="hidden" value="{$form.customer_country|default:""}">
                <input id="selected_card_type" name="selected_card_type" type="hidden" value="{$form.card_type|default:""}">
                <input id="selected_card_expiration_month" name="selected_card_expiration_month" type="hidden" value="{$form.card_expiration_month|default:""}">
                <input id="selected_card_expiration_year" name="selected_card_expiration_year" type="hidden" value="{$form.card_expiration_year|default:""}">

                <fieldset>
                    <legend>Card Info</legend>
                    <label for="card_message">Card Message<small>*</small></label><textarea id="card_message" name="card_message" class="length-limited-200">{$form.card_message|default:""}</textarea><br/>
                    <label for="special_instructions">Special Instructions</label><textarea id="special_instructions" name="special_instructions" class="length-limited-200">{$form.special_instructions|default:""}</textarea><br/>
                </fieldset>

                <fieldset>
                    <legend>Recipient Info</legend>
                    <label for="recipient_name">Name<small>*</small></label><input id="recipient_name" class="length-limited-50" type="text" name="recipient_name" value="{$form.recipient_name|default:""}" maxlength="50"><br/>
                    <label for="recipient_institution">Institution</label><input id="recipient_institution" class="length-limited-50" type="text" name="recipient_institution" value="{$form.recipient_institution|default:""}" maxlength="50"><br/>
                    <label for="recipient_address1">Address1<small>*</small></label><input id="recipient_address1" class="length-limited-30" type="text" name="recipient_address1" value="{$form.recipient_address1|default:""}" maxlength="30"><br/>
                    <label for="recipient_address2">Address2</label><input id="recipient_address2" type="text" class="length-limited-30" name="recipient_address2" value="{$form.recipient_address2|default:""}" maxlength="30"><br/>
                    <label for="recipient_city">City<small>*</small></label><input id="recipient_city" class="length-limited-30" type="text" name="recipient_city" value="{$form.recipient_city|default:""}" maxlength="30"><br/>
                    <label for="recipient_state">State<small>*</small></label><select id="recipient_state" name="recipient_state">{include file="florist_checkout_states.tpl"}</select><br/>
                    <label for="recipient_country">Country<small>*</small></label><select id="recipient_country" name="recipient_country">{include file="florist_checkout_countries.tpl"}</select><br/>
                    <label for="recipient_zip">Zip<small>*</small></label><input id="recipient_zip" type="text" name="recipient_zip" value="{$form.recipient_zip|default:""}"><br/>
                    <label for="recipient_phone">Phone<small>*</small></label><input id="recipient_phone" type="text" name="recipient_phone" value="{$form.recipient_phone|default:""}"><br/>

                    <label for="delivery_date">Delivery Date<small>*</small></label>
                    <select id="delivery_date" name="delivery_date">
                        <option value="">--Please, fill in "Zip" field--</option>
                    </select><br/>
                </fieldset>

                <fieldset>
                    <legend>Customer Info</legend>
                    <label for="customer_name">Name<small>*</small></label><input id="customer_name" class="length-limited-50" type="text" name="customer_name" value="{$form.customer_name|default:""}" maxlength="50"><br/>
                    <label for="customer_address1 ">Address1<small>*</small></label><input id="customer_address1" class="length-limited-30" type="text" name="customer_address1" value="{$form.customer_address1|default:""}" maxlength="30"><br/>
                    <label for="customer_address2">Address2</label><input id="customer_address2" class="length-limited-30" type="text" name="customer_address2" value="{$form.customer_address2|default:""}" maxlength="30"><br/>
                    <label for="customer_city">City<small>*</small></label><input id="customer_city" class="length-limited-30" type="text" name="customer_city" value="{$form.customer_city|default:""}" maxlength="30"><br/>
                    <label for="customer_state">State<small>*</small></label><select id="customer_state" name="customer_state">{include file="florist_checkout_states.tpl"}</select><br/>
                    <label for="customer_country">Country<small>*</small></label><select id="customer_country" name="customer_country">{include file="florist_checkout_countries.tpl"}</select><br/>
                    <label for="customer_zip">Zip<small>*</small></label><input id="customer_zip" type="text" name="customer_zip" value="{$form.customer_zip|default:""}"><br/>
                    <label for="customer_phone">Phone<small>*</small></label><input id="customer_phone" type="text" name="customer_phone" value="{$form.customer_phone|default:""}"><br/>
                    <label for="customer_email">Email<small>*</small></label><input id="customer_email" class="length-limited-100" type="text" name="customer_email" value="{$form.customer_email|default:""}" maxlength="100"><br/>

                    <label for="card_type">Credit Card<small>*</small></label>
                    <select id="card_type" name="card_type">
                        <option value="">--Select Card--</option>
                        <option value="AX">American Express</option>
                        <option value="VI">Visa</option>
                        <option value="MC">MasterCard</option>
                        <option value="DI">Discover</option>
                    </select><br/>
                    <label for="card_number">Card Number<small>*</small></label><input id="card_number" type="text" name="card_number" value="{$form.card_number|default:""}"><br/>
                    <label for="card_expiration_month">Expiration Date<small>*</small></label>
                    <select id="card_expiration_month" name="card_expiration_month">
                        <option value="">MM</option>
                        <option value="01">01</option>
                        <option value="02">02</option>
                        <option value="03">03</option>
                        <option value="04">04</option>
                        <option value="05">05</option>
                        <option value="06">06</option>
                        <option value="07">07</option>
                        <option value="08">08</option>
                        <option value="09">09</option>
                        <option value="10">10</option>
                        <option value="11">11</option>
                        <option value="12">12</option>
                    </select>
                    <select id="card_expiration_year" name="card_expiration_year">
                        <option value="">YYYY</option>
                        {foreach from=$years item=year }
                            <option value="{$year}">20{$year}</option>
                        {/foreach}
                    </select><br/>
                    <label for="card_sec_code">Security Code<small>*</small></label><input id="card_sec_code" type="text" name="card_sec_code" value="{$form.card_sec_code|default:""}"><br/>
                </fieldset>

                <br/>
                <center>
                    <input type="submit" value="continue">
                    <input type="button" value="review cart" onclick="document.location='?p=flowercart'">
                </center>
                <br/>
            </form>

            <script type="text/javascript" src="/js/jquery.inputlimiter.1.2.2.min.js"></script>
            <script>
                {literal}
                jQuery(function() {
                    var first_run = true;

                    $('#recipient_zip').change(function() {
                        $.post('/ajax_florist_cart.php', {action: 'get_delivery_dates', zip: $('#recipient_zip').val()}, function(data) {
                            $('#delivery_date option').remove();
                            $('#delivery_date').append(data);

                            if(first_run) {
                                $('#delivery_date').val($('#selected_delivery_date').val());
                            }
                        });
                    });

                    $('#recipient_zip').change();

                    $('#recipient_state').val($('#selected_recipient_state').val());
                    $('#recipient_country').val($('#selected_recipient_country').val());
                    $('#customer_state').val($('#selected_customer_state').val());
                    $('#customer_country').val($('#selected_customer_country').val());
                    $('#card_type').val($('#selected_card_type').val());
                    $('#card_expiration_month').val($('#selected_card_expiration_month').val());
                    $('#card_expiration_year').val($('#selected_card_expiration_year').val());

                    $('.length-limited-30').inputlimiter({limit: 30, limitTextShow: false});
                    $('.length-limited-50').inputlimiter({limit: 50, limitTextShow: false});
                    $('.length-limited-100').inputlimiter({limit: 100, limitTextShow: false});
                    $('.length-limited-200').inputlimiter({limit: 200, limitTextShow: false});
                });
                {/literal}
            </script>
        {elseif $checkout_info }
            <table id="florist-checkout-info-table">
                <tr>
                    <td class="florist-header" colspan="2">Card Info</td>
                </tr>
                <tr>
                    <td class="florist-left">Card Message</td>
                    <td>{$form.card_message|default:""}</td>
                </tr>
                <tr>
                    <td class="florist-left">Special Instructions</td>
                    <td>{$form.special_instructions|default:""}</td>
                </tr>

                <tr>
                    <td class="florist-header" colspan="2">Recipient Info</td>
                </tr>
                <tr>
                    <td class="florist-left">Name</td>
                    <td>{$form.recipient_name|default:""}</td>
                </tr>
                <tr>
                    <td class="florist-left">Institution</td>
                    <td>{$form.recipient_institution|default:""}</td>
                </tr>
                <tr>
                    <td class="florist-left">Address1</td>
                    <td>{$form.recipient_address1|default:""}</td>
                </tr>
                <tr>
                    <td class="florist-left">Address2</td>
                    <td>{$form.recipient_address2|default:""}</td>
                </tr>
                <tr>
                    <td class="florist-left">City</td>
                    <td>{$form.recipient_city|default:""}</td>
                </tr>
                <tr>
                    <td class="florist-left">State</td>
                    <td>{$form.recipient_state|default:""}</td>
                </tr>
                <tr>
                    <td class="florist-left">Country</td>
                    <td>{$form.recipient_country|default:""}</td>
                </tr>
                <tr>
                    <td class="florist-left">Zip</td>
                    <td>{$form.recipient_zip|default:""}</td>
                </tr>
                <tr>
                    <td class="florist-left">Phone</td>
                    <td>{$form.recipient_phone|default:""}</td>
                </tr>

                <tr>
                    <td class="florist-header" colspan="2">Customer Info</td>
                </tr>
                <tr>
                    <td class="florist-left">Name</td>
                    <td>{$form.customer_name|default:""}</td>
                </tr>
                <tr>
                    <td class="florist-left">Address1</td>
                    <td>{$form.customer_address1|default:""}</td>
                </tr>
                <tr>
                    <td class="florist-left">Address2</td>
                    <td>{$form.customer_address2|default:""}</td>
                </tr>
                <tr>
                    <td class="florist-left">City</td>
                    <td>{$form.customer_city|default:""}</td>
                </tr>
                <tr>
                    <td class="florist-left">State</td>
                    <td>{$form.customer_state|default:""}</td>
                </tr>
                <tr>
                    <td class="florist-left">Country</td>
                    <td>{$form.customer_country|default:""}</td>
                </tr>
                <tr>
                    <td class="florist-left">Zip</td>
                    <td>{$form.customer_zip|default:""}</td>
                </tr>
                <tr>
                    <td class="florist-left">Phone</td>
                    <td>{$form.customer_phone|default:""}</td>
                </tr>
                <tr>
                    <td class="florist-left">Email</td>
                    <td>{$form.customer_email|default:""}</td>
                </tr>
                <tr>
                    <td class="florist-left">Credit Card</td>
                    <td>{$form.card_type|default:""}</td>
                </tr>
                <tr>
                    <td class="florist-left">Card Number</td>
                    <td>{$form.card_number|default:""}</td>
                </tr>
                <tr>
                    <td class="florist-left">Expiration Date</td>
                    <td>{$form.card_expiration_month|default:""}/{$form.card_expiration_year|default:""}</td>
                </tr>
                <tr>
                    <td class="florist-left">Security Code</td>
                    <td>{$form.card_sec_code|default:""}</td>
                </tr>
            </table>

            <form id="florist-checkout-info-form" method="post">
                <input id="checkout_info" name="checkout_info" type="hidden" value="1">
                {foreach from=$form key=name item=value}
                    <input id="{$name}" name="{$name}" type="hidden" value="{$value}">
                {/foreach}

                <center>
                    <input type="submit" value="continue">
                    <input type="button" value="change entered information" onclick="document.location='?p=flowercheckout'">
                </center>
            </form>
        {else}
            <table width="100%">
                <tr>
                    <th>product</th>
                    <th>price</th>
                    <th>quantity</th>
                    <th>amount</th>
                </tr>
                {foreach from=$products key=pid item=product}
                    <tr align="center">
                        <td>
                            {$product.name}
                        </td>
                        <td>
                            ${$product.price|number_format:2}
                        </td>
                        <td>
                            {$product.count}
                        </td>
                        <td>
                            <b>${$product.amount|number_format:2}</b>
                        </td>
                    </tr>
                {/foreach}
                <tr>
                    <td></td>
                    <td></td>
                    <td align="right">
                        <b>Total:</b>
                    </td>
                    <td align="center">
                        <b>${$order_info.sub_total|number_format:2}</b>
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td></td>
                    <td align="right">
                        <b>Affiliate service charge:</b>
                    </td>
                    <td align="center">
                        <b>${$order_info.affiliate_service_charge|number_format:2}</b>
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td></td>
                    <td align="right">
                        <b>Affiliate tax:</b>
                    </td>
                    <td align="center">
                        <b>${$order_info.affiliate_tax|number_format:2}</b>
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td></td>
                    <td align="right">
                        <b>FloristOne service charge:</b>
                    </td>
                    <td align="center">
                        <b>${$order_info.florist_one_service_charge|number_format:2}</b>
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td></td>
                    <td align="right">
                        <b>FloristOne tax:</b>
                    </td>
                    <td align="center">
                        <b>${$order_info.florist_one_tax|number_format:2}</b>
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td></td>
                    <td align="right">
                        <b>Order total:</b>
                    </td>
                    <td align="center">
                        <b>${$order_info.order_total|number_format:2}</b>
                    </td>
                </tr>
            </table>

            <form method="post">
                {foreach from=$form key=name item=value}
                    <input id="{$name}" name="{$name}" type="hidden" value='{$value}'>
                {/foreach}

                <center>
                    <input type="submit" value="submit">
                    <input type="button" value="review cart" onclick="document.location='?p=flowercart'">
                </center>
            </form>
        {/if}
    {else}
        <h3>Success!</h3>
        Your order has been placed. Thank you!<br/><br/>
        <b>Order number:</b> {$order_number}<br/>
        <b>Order total:</b> ${$order_total|number_format:2}
        <br/><br/>
    {/if}
{else}
    Shopping card is empty.
    <br/><br/>
    <a href="javascript: history.go(-1)">&laquo; back</a>
{/if}
