{include file="adm_header.tpl" menu="1"}
{include file="window.tpl"}
{include file="util.tpl"}
{literal}
<style>
label.cb {
  width: auto !important;
  float: none;
  text-align: left;
  padding-left: 27px;
}
input.cb {
  float: left;
}
input.cb-padded {
  margin-left: 25px;
}
div.page {
  float: left;
  width: 200px;
}
</style>
{/literal}
<h1>Site copy tool</h1>
{if $denied}
Site is in production mode. Copying denied
{else}
<form id="form">
Copy site elements from: <br>
<select name="domain_id" id="domain_id" onchange="pages_onclick()">
{html_options options=$domains}
</select>
to: <b>{$smarty.session.domain_name}</b>
<fieldset>
<legend>Elements</legend>
<div><input type="checkbox" name="general" id="general" class="cb"><label for="general" class="cb">Site general properties (layout, title, menu styles etc)</label></div>
<div><input type="checkbox" name="header" id="header" class="cb"><label for="header" class="cb">Site header</label></div>
<div><input type="checkbox" name="footer" id="footer" class="cb"><label for="footer" class="cb">Site footer</label></div>
<div><input type="checkbox" name="css" id="css" class="cb"><label for="css" class="cb">CSS</label></div>
<div><input type="checkbox" name="obit" id="obit" class="cb"><label for="obit" class="cb">Obituary config</label></div>
<small>Warning! Any existing data from above elements will be replaced by copied data</small>
<br>
<br>
<div><input type="checkbox" name="structure" id="structure" class="cb"><label for="structure" class="cb">Site structure (note: no images are copied)</label></div>
<div><input type="checkbox" name="remove_structure" id="remove_structure" class="cb cb-padded"><label for="remove_structure" class="cb">Remove existing site nodes</label></div>
<div><input type="checkbox" name="pages" id="pages" class="cb" onclick="pages_onclick()"><label for="pages" class="cb">Pages (selected pages, note!: currently no images are copied)</label></div>
<div><input type="checkbox" name="remove_pages" id="remove_pages" class="cb cb-padded"><label for="remove_pages" class="cb">Remove existing site pages</label></div>
<br>
</fieldset>
<fieldset>
<legend>Pages</legend>
<div><input type="checkbox" id="all" class="cb" onclick="select_all(this)"><label for="all" class="cb">All</label></div>
<div id="pholder"></div>
</fieldset>
<fieldset>
<legend>Other elements</legend>
<div><input type="checkbox" name="list_items" id="list_items" class="cb"><label for="list_items" class="cb">List items</label></div>
<div><input type="checkbox" name="remove_list_items" id="remove_list_items" class="cb cb-padded"><label for="remove_list_items" class="cb">Remove existing list items</label></div>

<div><input type="checkbox" name="products" id="products" class="cb"><label for="products" class="cb">Products & categories (with images)</label></div>
<div><input type="checkbox" name="remove_products" id="remove_products" class="cb cb-padded"><label for="remove_products" class="cb">Remove existing products & categories</label></div>
</fieldset>
</form>
<input type="button" value="Copy!" onclick="make_copy()">
<script type="text/javascript">
{include file="populate_controls.tpl"}
{literal}

var CopyDialog = Class.create();
CopyDialog.prototype = {
	initialize: function() {
		// create dialog fields in form
		var f = '\
        <form>\
          <br>You are about to copy information from one site to other.<br><br>\
          <b>Please, note!</b> Any existing information for copied elements will be replaced by newly copied.<br><br>\
					All requested information will be erased. This cannot be undone!<br><br>\
          <center>\
          <input type="button" value="I agree, start copying!" name="ok">\
          <input type="button" value="Cancel" name="cancel">\
          </center>\
        </form>\
			';
		this.wnd = Dialog.info(f, {windowParameters: {className:"alphacube", width:400, title:"Copy confirmation", showEffect: Element.show, hideEffect: Element.hide} });

    this.form = this.wnd.getContent().getElementsByTagName("FORM")[0];
    this.form.elements["cancel"].onclick=this.oncancel.bind(this);
    this.form.elements["ok"].onclick=this.onok.bind(this);
  	this.wnd.hideloader();
	},

	onok: function() {
		this.wnd.showloader();
    Dialog.setInfoMessage("<br><br><br><b>Copying! Please wait...</b>");
		new Ajax.Request('copy.php', {parameters:'action=copy&'+Form.serialize($("form")), onSuccess:this.ondone.bind(this)}); 
	},
  oncancel: function() {
    this.wnd.hide();
  },
  ondone: function(t, json) {
    jslog.debug(t.responseText);
    Dialog.setInfoMessage('<br><br><br><b>Done!</b><br><br><br><form><center><input type="button" value=" Ok " name="ok"></center></form>');
    this.form = this.wnd.getContent().getElementsByTagName("FORM")[0];
    this.form.elements["ok"].onclick=this.oncancel.bind(this);
  	this.wnd.hideloader();
  }
}

function make_copy() {
  // alert(Form.serialize($("form")));
  new CopyDialog();
}
function select_all(src) {
  var elems = Form.getInputs($("pholder"), 'checkbox');
  var checked = src.checked;
  $A(elems).each( function(elem) {
    elem.checked = checked;
  } );
}
function pages_onclick() {
  $("pholder").innerHTML = "";
  if($("pages").checked) {
    load_pages($("domain_id").options[$("domain_id").selectedIndex].value);
  }
}
function load_pages(domain_id) {
  new Ajax.Request('copy.php', {parameters:'action=list_pages&domain_id='+domain_id, onSuccess:pages_onload});
}
function pages_onload(t, json) {
  var html = "";
  jslog.debug(t.responseText);
  json = eval(t.responseText);
  $H(json).each(function(elem) {
    // alert(elem.value.name);
    html += '<div class="page"><input type="checkbox" id="page'+elem.value.page_id+'" name="page_id[]" value="'+elem.value.page_id+'" class="cb"><label for="page'+elem.value.page_id+'" class="cb">'+elem.value.name+'</label></div>';
  } );
  $("pholder").innerHTML = html;
}
</script>	
{/literal}
{/if}
{include file="adm_footer.tpl"}
