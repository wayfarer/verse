{include file="adm_header.tpl" menu="1"}
{include file="window.tpl"}
{include file="contextmenu.tpl"}
{include file="util.tpl"}

<h1>Ecommerce :: Affiliates</h1>
<div class="submenu"><a href="products.php">Products</a> | <a href="products_categories.php">Categories</a>{if $ecommerce_enabled} | <a href="products_orders.php">Orders</a>{/if}{if $affiliates_enabled} | <a href="aff.php">Affiliates</a>{/if}</div>
<div class="actions"><a href="#" onclick="new AffiliateEditor(0); return false;">new affiliate</a></div>
<div id="affiliates"><img src="img/loader-big.gif"></div>
<script type="text/javascript">
{include file="populate_controls.tpl"}
{literal}
  var AffiliateEditor = Class.create();
  AffiliateEditor.prototype = {
    initialize: function(affiliate_id) {
      this.affiliate_id = affiliate_id;
{/literal}
{capture name=pages}
  {html_options options=$pages}
{/capture}
      var f = '\
          <form>\
          <label for="title">Title</label><input type="text" id="title" name="title" class="txt3"><br>\
          <label for="page_id">Entry page</label><select id="page_id" name="page_id">{$smarty.capture.pages|strip|escape:"quotes"}</select><br><br>\
          <label for="ref">Reference</label><input type="text" id="ref" name="ref"><br>\
          </form>\
        ';
{literal}
      this.wnd = Dialog.confirm(f, {windowParameters: {className:"alphacube", width:520, title:"Affiliate", showEffect: Element.show, hideEffect: Element.hide},
                        ok: this.onok.bind(this) });
      this.form = this.wnd.getContent().getElementsByTagName("FORM")[0];
      this.form.onsubmit = this.onsubmit.bind(this);

      Form.focusFirstElement(this.form);

      if(affiliate_id) {
        // load data
        this.json = null;
        new Ajax.Request('aff.php', {parameters:'action=load&id='+affiliate_id, onSuccess:this.onload.bind(this)}); // TODO: handle AJAX errors here
      }
      else {
        // defaults
        // fill ref field with random 8 symbols
        this.form.elements['ref'].value = random_string(8);
        this.wnd.hideloader();
      }
    },

    onload: function(t, json) {
      jslog.debug(t.getResponseHeader("X-JSON"));
      populate_controls(this.form, json);
      this.wnd.hideloader();
    },

    onsubmit: function() {
      this.onok();
      return false;
    },

    onsave: function(t) {
      jslog.debug(t.responseText);
      update_affiliates();
      this.wnd.hide();
    },
    
    onok: function() {
      this.wnd.showloader();
      new Ajax.Request('aff.php', {parameters:'action=save&id='+this.affiliate_id+"&"+Form.serialize(this.form), onSuccess:this.onsave.bind(this)}); // TODO: handle AJAX errors here
    }
  }

  function random_string(length) {
    chars = "abcdefghijklmnopqrstuvwxyz1234567890";
    pass = "";
    for(x=0;x<length;x++)
    {
      i = Math.floor(Math.random() * 36);
      pass += chars.charAt(i);
    }
    return pass;
  }

  function affiliate_deleted(t) {
    update_affiliates();
  }
  
  function delete_affiliate(affiliate_id) {
    if(confirm("Are you sure you want to delete affiliate?")) {
      new Ajax.Request('aff.php', {parameters:'action=delete&id='+affiliate_id, onSuccess:affiliate_deleted}); // TODO: handle AJAX errors here
    }
  }
  
  function page(start) {
    new Ajax.Updater('affiliates', 'aff.php', {parameters: "action=list&start="+start});  // add AJAX error handling here
  }
  function order(field) {
    new Ajax.Updater('affiliates', 'aff.php', {parameters: "action=list&order="+field});  // add AJAX error handling here
  }
  function update_affiliates() {                         
    new Ajax.Updater('affiliates', 'aff.php', {parameters: "action=list"});  // add AJAX error handling here
  } 
  update_affiliates();
{/literal}
</script> 

{include file="adm_footer.tpl"}
