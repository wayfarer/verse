{if $page_type eq 4 && $site.props.site_music_state && $site.props.site_music_file}
<script type="text/javascript" src="/js/cookies.js"></script>
<script type="text/javascript" src="/js/swfobject.js"></script>
<div class="mp3player" style="position: fixed; top: 0; right: 0;">
<div id="player">Mediaplayer will be here if scripting enabled</div>
<script type="text/javascript">
  var music_start_time = 0;
	// get playing state from cookie  
  var so = new SWFObject('/tools/player.swf','mpl','170','20','8');
  so.addParam('allowscriptaccess','always');
  so.addParam('allowfullscreen','false');
  so.addVariable('height','20');
  so.addVariable('width','170');

  if(Cookies["music_start_time_{$obituary_id}"] != 'undefined') {ldelim}
      so.addVariable('start',Cookies["music_start_time_{$obituary_id}"]);
  }

  {if strpos($site.props.site_music_file, "/") !== false}
  so.addVariable('file',"/{$site.props.site_music_file}");
  {else}
  so.addVariable('file',"/assets/{$site.props.site_music_file}");
  {/if}
	if(Cookies["music_stopped"]!="1") {ldelim}
	  so.addVariable('autostart','true');
	}
  so.addVariable('repeat','false');
  so.addVariable('type','sound');
  so.write('player');

	function playerReady() {ldelim}
		// add listeners
		document.getElementById("mpl").addControllerListener("ITEM","oLoad");
		document.getElementById("mpl").addControllerListener("PLAY","oPlay");
		document.getElementById("mpl").addControllerListener("MUTE","oMute");
	}	
	function oMute(param) {ldelim}
		if(param.state) {ldelim}
			Cookies.create("music_stopped", "1", 365);
		}
		else {ldelim}
			Cookies.create("music_stopped", "0", 365);
		}
	}
	function oPlay(player) {ldelim}
		if(player.state) {ldelim}
			Cookies.create("music_stopped", "0", 365);			
		}
		else {ldelim}
			Cookies.create("music_stopped", "1", 365);
		}
	}
	function oStop(player) {ldelim}
		Cookies.create("music_stopped", "1", 365);
	}
	function oLoad() {ldelim}
        if(Cookies["music_stopped"] == 'undefined') {ldelim}
		    Cookies.create("music_stopped", "0", 365);
        }
		document.getElementById("mpl").addControllerListener("STOP","oStop");
        document.getElementById("mpl").addModelListener("TIME","timeListener");
	}
    function timeListener(player) {ldelim}
        music_start_time = player.position;
    }
  function sendEvent(typ, prm) {ldelim} 
    document.getElementById("mpl").sendEvent(typ, prm);
  }
  function isie() {ldelim}
    return (navigator.appName && navigator.appName.indexOf("Microsoft") != -1);
  }
  var active_element;
  if(isie()) {ldelim}
   active_element = document.activeElement; // for dumb IE, to detect when window loses focus
  }
  var onblur_handler = function() {ldelim}
    if(isie() && active_element != document.activeElement) {ldelim}
      active_element = document.activeElement;
      return;
    }
    // active element is not changed - means that window lost focus
    if(Cookies["music_stopped"]!="1") {ldelim}
	    sendEvent('play', false);
	    Cookies.create("music_stopped", "2", 365);
	}
  };
  if (isie()) {ldelim}
    document.onfocusout = onblur_handler;
  }
  else {ldelim}
    window.onblur = onblur_handler;
  }
  window.onfocus = function() {ldelim}
	  if(Cookies["music_stopped"]!="1") {ldelim}
	    sendEvent('play', true);
	  }
  }
  window.onunload = function() {ldelim}
      Cookies.create('music_start_time_{$obituary_id}', music_start_time, 0);
  }
</script>
</div>
{/if}
<div class="header">
{if $site.header_mode}
{$site.header}
{else}
<a href="http://{$domain_name}">
{if $site.header[0] !== "/"}<img src="/{$site.header}">{else}<img src="{$site.header}">{/if}
</a>{/if}</div>
