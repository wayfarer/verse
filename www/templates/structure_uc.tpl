{include file="adm_header.tpl" menu="1"}
{include file="window.tpl"}
{include file="contextmenu.tpl"}
{include file="util.tpl"}

{literal}
<style>
    #ace-header-html, #ace-footer, #ace-css, #ace-content1, #ace-content2 {
        overflow:  hidden;
        position: relative;
        text-align: left;
    }

    #ace-footer, #ace-css {
        width: 100%;
    }

	#left {
		float: left;
		border-right: 1px solid;
		margin-right: 10px;
		width: 426px !important;
		width: 427px;
	}

	#right {
		float: left;
		width: 150px;
		border-right: 1px solid;
		margin-right: 10px;
	}

	#right2 {
		float: left;
	}

	div.structure {
		clear: both;
	}

	div.structure a {
		text-decoration: none;
	}

	div.structure a:hover {
		color: #FF0000;
	}

	div.node {
		clear: both;
		height: 20px;
/*		border: 1px solid #EDFAFF; */
		border: 1px solid #BDCACC;
		margin-bottom: 1px;
		margin-right: 5px;
		overflow: hidden;
	}

	div.node_accept {
		border: 1px solid #0000FF;
	}

	div.page_accept {
		border: 1px solid #FF0000;
	}

	div.node_add {
		font-size: 12px;
		padding-top: 3px;
		padding-left: 3px;
		float: left;
		width: 11px !important;
		width: 13px;
	}

	div.node_depth {
		float: left;
		width: 35px;
		background-color: #DDEAEF;
		padding: 0 2px;
	}

	div.node_name {
		float: left;
		width: 200px;
		margin: 0 0 0 10px;
	}

	div.node_content {
		position: absolute;
		left: 277px !important;
		left: 276px;
		width: 120px;
		border-left: 1px solid #BDCACC;
	}

	div.node_content span {
		padding-left: 3px;
	}

	div.node_controls {
		position: absolute;
		left: 380px;
		padding: 0 3px;
		background-color: #DDEAEF;
	}

	div.page {
		background-color: #DDEAEF;
		border-top: 1px solid #BCCACF;
		border-bottom: 1px solid #BCCACF;
		width: 110px !important;
		width: 120px;
		padding: 0 5px;
		cursor: default;
		margin: 1px 0;
		overflow: hidden;
	}

	div.node_delete {
		border-left: 1px solid #BDCACC;
		position: absolute;
		left: 397px;
		font-size: 12px;
		padding-top: 3px;
		padding-left: 4px;
	}

	div.structure_controls {
		margin-top: 7px;
		margin-bottom: 7px;
	}
	label.inline {
		display: inline;
		float: none;
	}
</style>
{/literal}

<div id="left">
<h1>Site structure<br>(under construction pages) <small><a href="structure.php">&laquo; main pages</a></small><br>
<small><a href="index.php" target="_blank">preview</a> | <a href="http://dev.{$smarty.session.domain_name}/index.php" target="_blank">preview release</a></small>
</h1>
  <span class="preloader" style="float: right; margin-right: 7px"><img src="img/loader.gif" width="16" height="16"></span>
	<div class="actions"><a href="#" onclick="new ContentPropsDialog(0); return false;">new page</a></div>
	<div id="content_pages"></div>
</div>

<div id="right2">
	<h1>Under construction mode<br>Properties</h1>
	<ul>
  	<li><a href="#" onclick="new GeneralPropsDialog(); return false">general</a></li>
		<li><a href="#" onclick="new HeaderDialog(); return false">header</a></li>
  	<li><a href="#" onclick="new FooterDialog(); return false">footer</a></li>
		<li><a href="#" onclick="new CSSDialog(); return false">css</a></li>
	</ul>
</div>

{if !$is_ie }
    <script type="text/javascript" src="js/nicEdit.js"></script>
    <script type="text/javascript" src="js/ace/ace.js" charset="utf-8"></script>
    <script type="text/javascript" src="js/ace/mode-css.js" charset="utf-8"></script>
    <script type="text/javascript" src="js/ace/mode-html.js" charset="utf-8"></script>
    <script type="text/javascript" src="js/ace/theme-crimson_editor.js" charset="utf-8"></script>
    <script type="text/javascript" src="js/cookies.js" charset="utf-8"></script>
{/if}
<script type="text/javascript">
	var node_counter = 1;
    if(navigator.userAgent.indexOf("MSIE") == -1)
        var is_ie = false;
    else
        var is_ie = true;
{include file="populate_controls.tpl"}
{literal}

	function content_pages_onload()
	{
		document.getElementsByClassName("preloader", $("right"))[0].style.display = "none";
    if(!$("content_pages").innerHTML) {
      $("content_pages").innerHTML = '<a href="#" onclick="create_splash(); return false;">create splash page only</a><br><a href="#" onclick="create_splash(1); return false;">create splash, obituaries, calendar, direction and contact us pages</a>';
    }
    else {
    	Sortable.create($("content_pages"), {tag: 'div', constraint: false, onUpdate: onpagesorder});
    }
	}

  function create_splash(extra) {
    var extra = extra | 0;
  	document.getElementsByClassName("preloader", $("right"))[0].style.display = "block";
    new Ajax.Updater('content_pages', 'structure_uc.php', {parameters: "action=create_splash&extra="+extra, onComplete: content_pages_onload});
  }

  function onpagesorder() {
		var order="";
		$A($("content_pages").childNodes).each(
			function(page) {
		    	if(page.nodeName=="DIV") {
		    		order+=page.getAttribute("page_id")+";";
		    	}
		    }
		);
		// save order
		new Ajax.Request('structure_uc.php', {parameters: "action=update_pages_ord&ord="+order});
	}

	function window_oncancel(wnd)
	{
		wnd.close();
	}

	function ajax_onerror(t)
	{
	    alert('Error ' + t.status + ' -- ' + t.statusText);
	}


	var content_page_counter = 1;
	var ContentPropsDialog = Class.create();
	ContentPropsDialog.prototype = {
		initialize: function(page_id) {
			this.page_id = page_id;
{/literal}
{capture name=page_type_opts}
	{html_options options=$page_types}
{/capture}
			var f = '<form>\
					<label for="int_name">Internal name</label><input type="text" id="int_name" name="int_name" class="txt"><br>\
			  		<label for="page_type">Page type</label><select id="page_type" name="page_type">{$smarty.capture.page_type_opts|strip}</select><br>\
					<label for="no_header">No headers</label><input type="checkbox" id="no_header" name="no_header" class="chk"><br>\
					<label for="restricted">Restricted access</label><input type="checkbox" id="restricted" name="restricted" class="chk"><br>\
					<br>\
					<label for="title">Page title</label><input type="text" id="title" name="title" class="txt3"><br>\
					<label for="description">Page description</label><textarea id="desciption" name="description" cols="44" rows="2"></textarea><br>\
					<label>&nbsp;</label><input type="checkbox" name="desc_no_global" id="desc_no_global"><label for="desc_no_global" class="in">Do not add Global site description</label><br>\
					<label for="keywords">Page keywords</label><textarea id="keywords" name="keywords" cols="44" rows="2"></textarea><br>\
					<label>&nbsp;</label><small>Comma separated</small><br>\
					<label>&nbsp;</label><input type="checkbox" name="keywords_no_global" id="keywords_no_global"><label for="keywords_no_global" class="in">Do not add Global site keywords</label><br>\
					</form>\
				';
{literal}
			this.wnd = Dialog.confirm(f, {windowParameters: {className:"alphacube", width:540, title:"Page Properties", showEffect: Element.show, hideEffect: Element.hide},
									      ok: this.onok.bind(this) });
			this.form = this.wnd.getContent().getElementsByTagName("FORM")[0];
			this.form.onsubmit = this.onsubmit.bind(this);
			Form.focusFirstElement(this.form);
			if(page_id) {
				new Ajax.Request('structure_uc.php', {parameters:'action=load_page_props&id='+page_id, onSuccess:this.onload.bind(this)}); // TODO: handle AJAX errors here
			}
			else {
				this.form.int_name.value="page"+content_page_counter++;
				this.wnd.hideloader();
			}
		},
		
		onload: function(t, json) {
			jslog.debug(t.getResponseHeader("X-JSON"));
			populate_controls(this.form, json);
			this.wnd.hideloader();
		},

		onsave: function(t) {
			jslog.debug(t.responseText);
			load_content_pages();
			// we need to update node in structure
			// TODO: load structure or update
			// load_structure(); 
			this.wnd.hide();
		},

		onsubmit: function() {
			this.onok();
			return false;
		},

		onok: function() {
			this.wnd.showloader();
			new Ajax.Request('structure_uc.php', {parameters:'action=save_page_props&id='+this.page_id+'&'+Form.serialize(this.form), onSuccess:this.onsave.bind(this)}); // TODO: handle AJAX errors here
		}
	};
	
	var content_page_content = null;
  var ce_dialog = null;
	var ContentEditDialog = Class.create();
	ContentEditDialog.prototype = {
		initialize: function(page_id) {
			// create window
			var width = document.body.clientWidth-80;
			var height = document.body.clientHeight-130;
//			this.wnd = new WindowClass("Page editor", width, height, [this.onok.bind(this), this.oncancel.bind(this)]);
			this.page_id = page_id;

//			fck.style.height = height-110;

//			this.form = Builder.node("FORM");
			// create dialog fields in form
			var f = '\
					<form>\
			        <label>Content HTML</label><br>\
			        <div class="tabber">\
						<div class="tabbertab"><h2>Main content</h2><div class="fckplace"></div></div>\
						<div class="tabbertab"><h2>Second content</h2></div>\
					</div>\
					<input type="hidden" name="content1">\
					<input type="hidden" name="content2">\
					</form>\
				';
			this.wnd = Dialog.confirm(f, {windowParameters: {className:"alphacube", width:width, height:height, title:"Page editor", showEffect: Element.show, hideEffect: Element.hide},
									      ok: this.onok.bind(this) });
			this.form = this.wnd.getContent().getElementsByTagName("FORM")[0];

			var place = document.getElementsByClassName("fckplace", this.form);
			place[0].appendChild(fck);
			fck.style.display = "block";

			var tabberOptions = {
			  'onClick': function(argsObj) {
			    var t = argsObj.tabber;
			    var id = t.id;
			    var i = argsObj.index;
			    var e = argsObj.event;
			    t.navClearActive(t.currentTab);
				// get fckeditor instance
				var editor = FCKeditorAPI.GetInstance('fckeditor');
				// save current text to hidden field
				t.form.elements["content"+(t.currentTab+1)].value = editor.GetXHTML(true);
				// load new text from hidden field for selected tab
				editor.SetHTML(t.form.elements["content"+(i+1)].value);
				// for mozilla to fix vertical size of iframe
//				if(!IsIE) {
//					if(window.frames["fckeditor___Frame"].onresize) {
//						window.frames["fckeditor___Frame"].onresize();
//					}
//				}
			    t.currentTab = i;
			    t.navSetActive(i);
			    return false;
			  }
			};
			this.form.onsubmit = this.onsubmit.bind(this);
			tabberAutomatic(tabberOptions);
			this.tabber=this.form.getElementsByTagName("div")[0].tabber;

			this.tabber.form = this.form;
			this.tabber.currentTab = 0;

			// clear editor area only if IE, mozilla reloads IFRAME every time
			if(IsIE) {
				var editor = FCKeditorAPI.GetInstance('fckeditor');
				if(editor && editor.Status==FCK_STATUS_COMPLETE) { 
					editor.SetHTML("");
				}
			}
						
			this.fckeditor = fckeditor;
			// load data
			new Ajax.Request('structure_uc.php', {parameters:'action=load_page&id='+page_id, onSuccess:this.onload.bind(this)});
		},

		onload: function(t, json) {
			jslog.debug(t.responseText);
			json = eval(t.responseText);
			var content1 = "";
			var content2 = "";
			if(json.content1) { content1 = json.content1 };
			if(json.content2) { content2 = json.content2 };
			// store contents to hidden fields
			this.form.elements["content1"].value = content1;
			this.form.elements["content2"].value = content2;

			// Get the editor instance that we want to interact with.
			var oEditor=null;
      var self = this;
			Try.these (
        function() { oEditor = FCKeditorAPI.GetInstance('fckeditor'); }
	    );
			if(oEditor && oEditor.Status==FCK_STATUS_COMPLETE) { 
				// Insert the desired HTML.
			    jslog.debug("onload editor content setting");
			    Try.these(
					function() { 
						oEditor.SetHTML(content1); 
            self.wnd.hideloader();
					},
          function() { 
            content_page_content = content1 
            ce_dialog = self;
          }
        );
			}
			else { // store data, FCK onload event will handle it
				content_page_content = content1;
        ce_dialog = this;
			  jslog.debug("editor not ready, storing data");
        // for IE, push tabber, without this FCKEditor Onready not occured
        if(IsIE) {
          setTimeout("FCKeditor_OnComplete()", 1000);
        }
			}		
		},

		onsubmit: function() {
			this.onok();
			return false;
		},

		onsave: function(t) {
			jslog.debug(t.responseText);
			this.wnd.hide();
		},

		onok: function() {
			this.wnd.showloader();
			// store content from editor to hidden field
			var editor = FCKeditorAPI.GetInstance('fckeditor');
			this.form.elements["content"+(this.tabber.currentTab+1)].value = editor.GetXHTML(true);
			new Ajax.Request('structure_uc.php', {parameters:'action=save_page&id='+this.page_id+"&"+Form.serialize(this.form), onSuccess:this.onsave.bind(this)}); // TODO: handle AJAX errors here
		}
	};
	
	function FCKeditor_OnComplete(instance)
	{
	  jslog.debug("FCK OnComplete");
		if(content_page_content) {
      if(!instance) instance = FCKeditorAPI.GetInstance('fckeditor');
      // set 1st tab as active (for case, when user pushed second)
  		ce_dialog.tabber.navClearActive(ce_dialog.tabber.currentTab);
  		ce_dialog.tabber.currentTab = 0;
      ce_dialog.tabber.navSetActive(0);
			instance.SetHTML(content_page_content);
      ce_dialog.wnd.hideloader();
			content_page_content = null;
		  jslog.debug("FCK onload content set");
		}
	}

	var ContentEditHTMLDialog = Class.create();
	ContentEditHTMLDialog.prototype = {
		initialize: function(page_id) {
			// create window
			var width = document.body.clientWidth-80;
			var height = document.body.clientHeight-130;
//			this.wnd = new WindowClass("Page editor", width, height, [this.onok.bind(this), this.oncancel.bind(this)]);
			this.page_id = page_id;
			// create form
            if(is_ie) {
			var f = '\
					<form>\
			            <label>Content HTML</label><br>\
			            <div class="tabber">\
                            <div class="tabbertab"><h2>Main content</h2><textarea name="content1" id="content1" style="width:'+(width-23)+'px; height:'+(height-120)+'px; margin: 0;"></textarea></div>\
                            <div class="tabbertab"><h2>Second content</h2><textarea name="content2" id="content2" style="width:'+(width-23)+'px; height:'+(height-120)+'px; margin: 0;"></textarea></div>\
					    </div>\
					</form>\
				';
            } else {
                var f = '\
					<form>\
			            <label>Content HTML</label><br>\
			            <div class="tabber">\
                            <div class="tabbertab"><h2>Main content</h2><textarea name="content1" id="content1" style="display: none;"></textarea><div id="ace-content1" style="width:'+(width-23)+'px; height:'+(height-120)+'px;"></div></div>\
                            <div class="tabbertab"><h2>Second content</h2><textarea name="content2" id="content2" style="display: none;"></textarea><div id="ace-content2" style="width:'+(width-23)+'px; height:'+(height-120)+'px;"></div></div>\
					    </div>\
					</form>\
				';
            }
			this.wnd = Dialog.confirm(f, {windowParameters: {className:"alphacube", width:width, height:height, title:"General Properties", showEffect: Element.show, hideEffect: Element.hide},
									      ok: this.onok.bind(this) });
			this.form = this.wnd.getContent().getElementsByTagName("FORM")[0];
			
			this.form.elements["content1"].onkeydown = textarea_tab;
			this.form.elements["content2"].onkeydown = textarea_tab;

            // call tabber to make tabs
            if(is_ie) {
			    tabberAutomatic();
            } else {
                var HtmlMode = require("ace/mode/html").Mode;

                this.editor1 = ace.edit("ace-content1");
                this.editor1.setTheme("ace/theme/crimson_editor");
                this.editor1.getSession().setMode(new HtmlMode());

                this.editor2 = ace.edit("ace-content2");
                this.editor2.setTheme("ace/theme/crimson_editor");
                this.editor2.getSession().setMode(new HtmlMode());

                setTimeout(function() { tabberAutomatic(); }, 500);
            }
            
			Form.focusFirstElement(this.form);
			// load data
			new Ajax.Request('structure_uc.php', {parameters:'action=load_page&id='+page_id, onSuccess:this.onload.bind(this)});
		},

		onload: function(t, json) {
			jslog.debug(t.responseText);
			json = eval(t.responseText);
			populate_controls(this.form, json);

            if(!is_ie) {
                this.editor1.getSession().setValue(this.form.elements['content1'].value);
                var line1 = Cookies['ace_content1_'+this.page_id+'_uc_line'];
                if(line1 != null) {
                    this.editor1.gotoLine(line1);
                } else {
                    this.editor1.gotoLine(1);
                }

                this.editor2.getSession().setValue(this.form.elements['content2'].value);
                var line2 = Cookies['ace_content2_'+this.page_id+'_uc_line'];
                if(line2 != null) {
                    this.editor2.gotoLine(line2);
                } else {
                    this.editor2.gotoLine(1);
                }
            }

			this.wnd.hideloader();
		},

		onsave: function(t) {
			jslog.debug(t.responseText);
			this.wnd.hide();
		},

		onok: function() {
			this.wnd.showloader();
            if(!is_ie) {
                this.form.elements['content1'].value = this.editor1.getSession().getValue();
                Cookies.create('ace_content1_'+this.page_id+'_uc_line', this.editor1.getCursorPosition().row+1, 0);

                this.form.elements['content2'].value = this.editor2.getSession().getValue();
                Cookies.create('ace_content2_'+this.page_id+'_uc_line', this.editor2.getCursorPosition().row+1, 0);
            }
			new Ajax.Request('structure_uc.php', {parameters:'action=save_page&id='+this.page_id+"&"+Form.serialize(this.form), onSuccess:this.onsave.bind(this)}); // TODO: handle AJAX errors here
		}
	};

  var GeneralPropsDialog = Class.create();
	GeneralPropsDialog.prototype = {
		initialize: function() {
			// create dialog fields in form
			var f = '\
					<form>\
					<label for="title">Title</label><input type="text" id="title" name="title" class="txt2"><br>\
					<label for="width">Width (with px!)</label><input type="text" id="width" name="width" class="small"><br>\
			  	<label for="align">Align</label><input type="radio" name="align" id="align" value="1">align left<input type="radio" name="align" value="2">align center<br>\
					<label for="bgcolor">Backgr.color</label><input type="text" id="bgcolor" name="bgcolor" class="color"><br>\
					<label for="bgimage">Backgr.image</label><input type="text" id="bgimage" name="bgimage" class="file"><input type="button" name="browse" value="browse"><br>\
					<label for="description">Main description</label><textarea id="desciption" name="description" cols="44" rows="2"></textarea><br>\
					<label>&nbsp;</label><small>Added after page description, can be disabled per page (see individual page properties)</small><br>\
					<label for="keywords">Main keywords</label><textarea id="keywords" name="keywords" cols="44" rows="2"></textarea><br>\
					<label>&nbsp;</label><small>Added after page keywords, can be disabled per page. Comma separated</small><br>\
					<label for="headhtml">Additional head code</label><textarea id="headhtml" name="headhtml" cols="44" rows="2"></textarea><br>\
					<label>&nbsp;</label><small>Any HTML code to add to head section, added for every site page</small><br>\
					</form>\
				';
			this.wnd = Dialog.confirm(f, {windowParameters: {className:"alphacube", width:540, title:"General Properties", showEffect: Element.show, hideEffect: Element.hide},
									      ok: this.onok.bind(this) });
			this.form = this.wnd.getContent().getElementsByTagName("FORM")[0];

			this.form.onsubmit = this.onsubmit.bind(this);
			// create and bind filebrowser
			this.sfb = new ServerFileBrowser(this.onfileselect.bind(this));
			this.form.elements["browse"].onclick=this.sfb.open.bind(this.sfb);

			Form.focusFirstElement(this.form);
			// load layouts
			//this.layoutsloaded = false;
			//new Ajax.Request('structure_uc.php', {parameters:'action=load_layouts', onSuccess:this.onlayoutsload.bind(this)}); // TODO: handle AJAX errors here
			// load data
			new Ajax.Request('structure_uc.php', {parameters:'action=load_site_general', onSuccess:this.onload.bind(this)}); // TODO: handle AJAX errors here
		},

		onfileselect: function(url) {
			this.form.elements["bgimage"].value = url;
		},

		onload: function(t, json) {
			if(t) {
				jslog.debug(t.getResponseHeader("X-JSON"));
			}
			populate_controls(this.form, json);
			this.wnd.hideloader();
		},

		onsubmit: function() {
			this.onok();
			return false;
		},

		onsave: function(t) {
			jslog.debug(t.responseText);
			this.wnd.hide();
		},
		
		onok: function() {
			this.wnd.showloader();
			new Ajax.Request('structure_uc.php', {parameters:'action=save_site_general&'+Form.serialize(this.form), onSuccess:this.onsave.bind(this)}); // TODO: handle AJAX errors here
		}
	}

	var HeaderDialog = Class.create();
	HeaderDialog.prototype = {
		initialize: function() {
			// create dialog fields in form
            if(is_ie) {
			    var f = '\
					<form>\
			        <label>Header type:</label><input type="radio" id="image" name="header_mode" value="0" class="radio" checked="checked"><label class="inline" for="image">image</label> <input type="text" name="header_image" class="file"> <input type="button" name="browse" value="browse"><br>\
			        <label>&nbsp;</label><input type="radio" id="html" name="header_mode" value="1" class="radio"><label class="inline" for="html">HTML</label><br><textarea name="header_html" style="margin-left: 110px" cols="64" rows="12"></textarea>\
					</form>\
				';
            } else {
                var f = '\
					<form>\
			        <label>Header type:</label><input type="radio" id="image" name="header_mode" value="0" class="radio" checked="checked"><label class="inline" for="image">image</label> <input type="text" name="header_image" class="file"> <input type="button" name="browse" value="browse"><br>\
			        <label>&nbsp;</label><input type="radio" id="html" name="header_mode" value="1" class="radio"><label class="inline" for="html">HTML</label><br><textarea name="header_html" id="header_html" style="display: none; margin-left: 110px;" cols="64" rows="12"></textarea>\
					</form>\
					<div id="ace-header-html" style="height: 200px; margin-left: 110px;"></div>\
                ';
            }
			this.wnd = Dialog.confirm(f, {windowParameters: {className:"alphacube", width:675, title:"Site Header", showEffect: Element.show, hideEffect: Element.hide},
									      ok: this.onok.bind(this) });
			this.form = this.wnd.getContent().getElementsByTagName("FORM")[0];
			this.form.onsubmit = this.onsubmit.bind(this);
			// create and bind filebrowser
			this.sfb = new ServerFileBrowser(this.onfileselect.bind(this));
			this.form.elements["browse"].onclick=this.sfb.open.bind(this.sfb);

			// load data
			new Ajax.Request('structure_uc.php', {parameters:'action=load_site_header', onSuccess:this.onload.bind(this)}); // TODO: handle AJAX errors here
			Form.focusFirstElement(this.form);
		},

		onfileselect: function(url) {
			this.form.elements["header_image"].value = url;
		},

		onload: function(t, json) {
			jslog.debug(t.getResponseHeader("X-JSON"));
			populate_controls(this.form, json);

            if(!is_ie) {
                this.editor = ace.edit("ace-header-html");
                this.editor.setTheme("ace/theme/crimson_editor");
                var HtmlMode = require("ace/mode/html").Mode;
                this.editor.getSession().setMode(new HtmlMode());
                this.editor.getSession().setValue(this.form.elements['header_html'].value);

                var line = Cookies['ace_header_uc_line'];
                if(line != null) {
                    this.editor.gotoLine(line);
                } else {
                    this.editor.gotoLine(1);
                }
            }

			this.wnd.hideloader();
		},

		onsave: function(t) {
			jslog.debug(t.responseText);
			this.wnd.hide();
		},
		
		onsubmit: function() {
			this.onok();
			return false;
		},

		onok: function() {
			this.wnd.showloader();
            if(!is_ie) {
                this.form.elements['header_html'].value = this.editor.getSession().getValue();
                Cookies.create('ace_header_uc_line', this.editor.getCursorPosition().row+1, 0);
            }
			new Ajax.Request('structure_uc.php', {parameters:'action=save_site_header&'+Form.serialize(this.form), onSuccess:this.onsave.bind(this)}); // TODO: handle AJAX errors here
		}
	}
	
	var FooterDialog = Class.create();
	FooterDialog.prototype = {
		initialize: function() {
			// create dialog fields in form
            if(is_ie) {
			    var f = '\
					<form>\
					<label for="footer">Footer HTML</label><br><textarea name="footer" id="footer" cols="80" rows="15"></textarea>\
					</form>\
				';
            } else {
                var f = '\
					<form>\
					<label for="footer">Footer HTML</label><br><textarea name="footer" id="footer" cols="80" rows="15" style="display: none;"></textarea>\
					</form>\
					<div id="ace-footer" style="height: 240px;"></div>\
                ';
            }
			this.wnd = Dialog.confirm(f, {windowParameters: {className:"alphacube", width:675, title:"Site Footer", showEffect: Element.show, hideEffect: Element.hide},
									      ok: this.onok.bind(this) });
			this.form = this.wnd.getContent().getElementsByTagName("FORM")[0];

			this.form.elements["footer"].onkeydown = textarea_tab;
			this.form.onsubmit = this.onsubmit.bind(this);

			Form.focusFirstElement(this.form);
			// load data
			new Ajax.Request('structure_uc.php', {parameters:'action=load_site_footer', onSuccess:this.onload.bind(this)}); // TODO: handle AJAX errors here
		},

		onload: function(t, json) {
			jslog.debug(t.getResponseHeader("X-JSON"));
			populate_controls(this.form, json);

            if(!is_ie) {
                this.editor = ace.edit("ace-footer");
                this.editor.setTheme("ace/theme/crimson_editor");
                var HtmlMode = require("ace/mode/html").Mode;
                this.editor.getSession().setMode(new HtmlMode());
                this.editor.getSession().setValue(this.form.elements['footer'].value);

                var line = Cookies['ace_footer_uc_line'];
                if(line != null) {
                    this.editor.gotoLine(line);
                } else {
                    this.editor.gotoLine(1);
                }
            }

			this.wnd.hideloader();
		},

		onsave: function(t) {
			jslog.debug(t.responseText);
			this.wnd.hide();
		},
		
		onsubmit: function() {
			this.onok();
			return false;
		},

		onok: function() {
			this.wnd.showloader();
            if(!is_ie) {
                this.form.elements['footer'].value = this.editor.getSession().getValue();
                Cookies.create('ace_footer_uc_line', this.editor.getCursorPosition().row+1, 0);
            }
			new Ajax.Request('structure_uc.php', {parameters:'action=save_site_footer&'+Form.serialize(this.form), onSuccess:this.onsave.bind(this)}); // TODO: handle AJAX errors here
		}
	}

	var CSSDialog = Class.create();
	CSSDialog.prototype = {
		initialize: function() {
			// create dialog fields in form
            if(is_ie) {
			    var f = '\
					<form>\
					<label for="css">CSS Styles</label><br><textarea name="css" id="css" cols="80" rows="25"></textarea>\
					</form>\
				';
            } else {
                var f = '\
					<form>\
					<label for="css">CSS Styles</label><br><textarea name="css" id="css" cols="80" rows="25" style="display: none;"></textarea>\
                    </form>\
                    <div id="ace-css" style="height: 400px;"></div>\
                    ';
            }
			this.wnd = Dialog.confirm(f, {title:"CSS Styles", width:675, ok: this.onok.bind(this)});
			this.form = this.wnd.getContent().getElementsByTagName("FORM")[0];
			this.form.elements["css"].onkeydown = textarea_tab;
			Form.focusFirstElement(this.form);
			// load data
			new Ajax.Request('structure_uc.php', {parameters:'action=load_site_css', onSuccess:this.onload.bind(this)}); // TODO: handle AJAX errors here
		},

		onload: function(t, json) {
			jslog.debug(t.getResponseHeader("X-JSON"));
			populate_controls(this.form, json);

            if(!is_ie) {
                this.editor = ace.edit("ace-css");
                this.editor.setTheme("ace/theme/crimson_editor");
                var CssMode = require("ace/mode/css").Mode;
                this.editor.getSession().setMode(new CssMode());
                this.editor.getSession().setValue(this.form.elements['css'].value);

                var line = Cookies['ace_css_uc_line'];
                if(line != null) {
                    this.editor.gotoLine(line);
                } else {
                    this.editor.gotoLine(1);
                }
            }

			this.wnd.hideloader();
		},

		onsave: function(t) {
			jslog.debug(t.responseText);
			this.wnd.hide();
		},
		
		onok: function() {
			this.wnd.showloader();
            if(!is_ie) {
                this.form.elements['css'].value = this.editor.getSession().getValue();
                Cookies.create('ace_css_uc_line', this.editor.getCursorPosition().row+1, 0);
            }
			new Ajax.Request('structure_uc.php', {parameters:'action=save_site_css&'+Form.serialize(this.form), onSuccess:this.onsave.bind(this)}); // TODO: handle AJAX errors here
		}
	}

// Stuff

// menu handlers
	function on_content_page_properties(param) {
		new ContentPropsDialog(param.page_id);
	}

	function on_content_page_edit(param) {
		new ContentEditDialog(param.page_id);
	}

	function on_content_page_edit_html(param) {
		new ContentEditHTMLDialog(param.page_id);
	}

	function on_content_page_delete(param) {
		if(confirm("Are you sure you want to delete content page?")) {
			new Ajax.Request('structure_uc.php', {parameters: "action=delete_page&id="+param.page_id});
			load_content_pages();
		}
	}
		
	function handle_context_menu(e) {
		if (!e) var e = window.event;
		var tg = (e.target) ? e.target : e.srcElement;

		var posx = 0;
		var posy = 0;

		if (e.pageX || e.pageY)
		{
			posx = e.pageX;
			posy = e.pageY;
		}
		else if (e.clientX || e.clientY)
		{
			posx = e.clientX + document.body.scrollLeft;
			posy = e.clientY + document.body.scrollTop;
		}

		switch(tg.className) {
			case "page":
				content_page_menu.popup(posx, posy, {page_id: tg.getAttribute("page_id")});
				return false;
			break;
		}
		return true;
	}

	function handle_double_click(e)
	{
		if (!e) var e = window.event;
		var tg = (e.target) ? e.target : e.srcElement;
		switch(tg.className) {
			case "page":
				on_content_page_properties({page_id: tg.getAttribute("page_id")});
				return false;
			break;
		}
		return true;
	}

	function onmousedown(e)
	{
		var posx = 0;
		var posy = 0;
		if (!e) var e = window.event;

//		e.cancelBubble = true;
//		if (e.stopPropagation) e.stopPropagation();

		if (e.pageX || e.pageY)
		{
			posx = e.pageX;
			posy = e.pageY;
		}
		else if (e.clientX || e.clientY)
		{
			posx = e.clientX + document.body.scrollLeft;
			posy = e.clientY + document.body.scrollTop;
		}
		// posx and posy contain the mouse position relative to the document
		if($("popupmenu")) {
			if(!Position.within($("popupmenu"), posx, posy)) {
				content_page_menu.hide();
			}
		}
	}
	
	function load_content_pages() {
    new Ajax.Updater('content_pages', 'structure_uc.php', {parameters: "action=list_pages", onComplete: content_pages_onload});
  }

	var page_actions = [["Page Properties", "on_content_page_properties"],["Edit Wysiwyg", "on_content_page_edit"],["Edit HTML", "on_content_page_edit_html"], ["Delete Page", "on_content_page_delete"]];
	var content_page_menu = new ContextMenu(page_actions);

	// add custom menus for content pages
	document.oncontextmenu=handle_context_menu;
	document.ondblclick=handle_double_click;
	Event.observe(document, "mousedown", onmousedown, true);

	load_content_pages();

	var IsIE = navigator.userAgent.toLowerCase().indexOf('msie')!=-1;

	var fckeditor = new FCKeditor('fckeditor');
	fckeditor.BasePath = "js/fckeditor/";
	fckeditor.Height = document.body.clientHeight-240;
	document.write(fckeditor.CreateHTML());
	var fck = $("fck");
{/literal}
{include file="obit_config.tpl"}
</script>	

{include file="adm_footer.tpl"}
