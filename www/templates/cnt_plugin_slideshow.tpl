<script type="text/javascript" src="/js/prototype.js"></script>
<script type="text/javascript" src="/js/effects.js"></script>
<script type="text/javascript" src="/js/slideshow.js"></script>
<div id="slideshow"></div>
<script>
	var imgs = Array({section name=slide loop=$slides}"{$slides[slide].image|leading_slash}"{if $slides[slide.index_next]},{/if}{/section});
	var slideshow = new SlideShow({ldelim}images:imgs, container:$("slideshow")});
	slideshow.run();
</script>
