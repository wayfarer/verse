{include file="adm_header.tpl" menu="1"}
{include file="window.tpl"}
{include file="contextmenu.tpl"}
{include file="util.tpl"}

<h1>Lists management</h1>
<div class="actions"><a href="#" onclick="new ListEditor(0); return false;">new list</a></div>
<div id="lists"><img src="img/loader-big.gif"></div>
{literal}
<style>
	input.txt {
		width: 300px;
	}
</style>
<script type="text/javascript" src="js/nicEdit.js"></script>
<script type="text/javascript">
	var ListEditor = Class.create();
	ListEditor.prototype = {
		initialize: function(list_id) {
			// create window
//			this.wnd = new WindowClass("List Item", 680, 400, [this.onok.bind(this), this.oncancel.bind(this)]);
			this.list_id = list_id;
			// create form
//			this.form = Builder.node("FORM");
			// create dialog fields in form
			var f = '<form>\
					<label for="name">Name</label><input type="text" id="name" name="name" class="txt"><br>\
					<label for="type">Type</label><input type="text" id="type" name="type" class="txt"><br>\
<!--					<label for="company">Company</label><input type="text" id="company" name="company" class="txt"><br>\
					<label for="address">Address</label><input type="text" id="address" name="address" class="txt"><br>\
					<label for="address2">Address2</label><input type="text" id="address2" name="address2" class="txt"><br>\
					<label for="phone">Phone</label><input type="text" id="phone" name="phone" class="txt"><br>\
					<label for="fax">Fax</label><input type="text" id="fax" name="fax" class="txt"><br>\
					<label for="website">Website</label><input type="text" id="website" name="website" class="txt"><br>\
					<label for="email">Email</label><input type="text" id="email" name="email" class="txt"><br>-->\
          <label for="description">Description</label><div class="wysiwyg"><textarea name="description" id="description" style="width: 520px; height: 200px"></textarea></div><br>\
					</form>\
				';
			this.wnd = Dialog.confirm(f, {windowParameters: {className:"alphacube", width:670, height: 350, title:"List Item", showEffect: Element.show, hideEffect: Element.hide},
									      ok: this.onok.bind(this) });
			this.form = this.wnd.getContent().getElementsByTagName("FORM")[0];
			this.form.onsubmit = this.onsubmit.bind(this);

      var myNicEditor = new nicEditor({buttonList:['bold','italic','underline','strikethrough','subscript','superscript','ol','ul','hr','link','unlink','fontFormat','xhtml'], iconsPath: "/img/nicEditorIcons.gif", maxHeight: 200}).panelInstance('description');

			Form.focusFirstElement(this.form);

			if(list_id) {
				// load data
				this.json = null;
				new Ajax.Request('ajax_lists.php', {parameters:'action=load&id='+list_id, onSuccess:this.onload.bind(this)}); // TODO: handle AJAX errors here
			}
			else {
				// defaults
				this.wnd.hideloader();
			}
		},

		onload: function(t, json) {
			jslog.debug(t.getResponseHeader("X-JSON"));
			var form = this.form;
			$H(json).each(function(elem) {
				if(form.elements[elem.key]) {
					if(form.elements[elem.key].type=="text" || form.elements[elem.key].type=="textarea") {
						form.elements[elem.key].value = elem.value;
					}
					else { // treat as select
						for(var i=0; i<form.elements[elem.key].length; i++) {
							if(form.elements[elem.key].options[i].value==elem.value) {
								form.elements[elem.key].selectedIndex = i;
								break;
							}
						}
					}
				}
			} );
      nicEditors.findEditor("description").setContent(this.form.elements["description"].value);
			this.wnd.hideloader();
		},

		onsubmit: function() {
			this.onok();
			return false;
		},

		onsave: function(t) {
			jslog.debug(t.responseText);
			update_lists();
      this.wnd.hide();
		},
		
		onok: function() {
			this.wnd.showloader();
      nicEditors.findEditor("description").saveContent();
			new Ajax.Request('ajax_lists.php', {parameters:'action=save&id='+this.list_id+"&"+Form.serialize(this.form), onSuccess:this.onsave.bind(this)}); // TODO: handle AJAX errors here
		}
	}

	function list_deleted(t)
	{
		update_lists();
	}
	
	function delete_list(list_id)
	{
		if(confirm("Are you sure you want to delete list item?")) {
			new Ajax.Request('ajax_lists.php', {parameters:'action=delete&id='+list_id, onSuccess:list_deleted}); // TODO: handle AJAX errors here
		}
	}
	
	function page(start)
	{
		new Ajax.Updater('lists', 'ajax_lists.php', {parameters: "action=list&start="+start});  // add AJAX error handling here
	}
	function order(field)
	{
		new Ajax.Updater('lists', 'ajax_lists.php', {parameters: "action=list&order="+field});  // add AJAX error handling here
	}
	function update_lists()
	{
		new Ajax.Updater('lists', 'ajax_lists.php', {parameters: "action=list"});  // add AJAX error handling here
	}	
	update_lists();
</script>	
{/literal}

{include file="adm_footer.tpl"}
