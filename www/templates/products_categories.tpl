{include file="adm_header.tpl" menu="1"}
{include file="window.tpl"}
{include file="contextmenu.tpl"}
{include file="util.tpl"}

<h1>Ecommerce :: Products categories</h1>
<div class="submenu"><a href="products.php">Products</a> | <a href="products_categories.php">Categories</a>{if $ecommerce_enabled} | <a href="products_orders.php">Orders</a>{/if}{if $affiliates_enabled} | <a href="aff.php">Affiliates</a>{/if}</div>
<div class="actions"><a href="#" onclick="new CategoryEditor(0); return false;">new category</a></div>
<div id="categories"><img src="img/loader-big.gif"></div>
<script type="text/javascript">
{include file="populate_controls.tpl"}
{literal}
  var CategoryEditor = Class.create();
  CategoryEditor.prototype = {
    initialize: function(category_id) {
      // create window
//      this.wnd = new WindowClass("Category", 500, 290, [this.onok.bind(this), this.oncancel.bind(this)]);
      this.category_id = category_id;
      // create form
//      this.form = Builder.node("FORM");
      // create dialog fields in form
//          <input type="radio" id="afdefault" name="additional_fields" value="0"><label class="inline" for="afdefault">default <small>(user can access additional fields by clicking a link)</small></label><br>\
      var f = '\
          <form>\
          <label for="name">Category</label><input type="text" id="name" name="name" class="txt2"><br>\
          <label>Ecommerce</label><input type="radio" id="default" name="ecommerce_enabled" value="0" checked="1"><label class="inline" for="default">default</label><input type="radio" id="on" name="ecommerce_enabled" value="1"><label class="inline" for="on">on</label><input type="radio" id="off" name="ecommerce_enabled" value="2"><label class="inline" for="off">off</label><br>\
          <label>Price enabled</label><input type="radio" id="default" name="price_enabled" value="0" checked="1"><label class="inline" for="default">default</label><input type="radio" id="on" name="price_enabled" value="1"><label class="inline" for="on">on</label><input type="radio" id="off" name="price_enabled" value="2"><label class="inline" for="off">off</label><br><br>\
					<label>Additional fields for category products</label>\
            <div style="float:left">\
              <input type="radio" id="afoff" name="additional_fields" value="0" checked="1"><label class="inline" for="afoff">off <small>(no additional fields available)</small></label><br>\
              <input type="radio" id="afon" name="additional_fields" value="1"><label class="inline" for="afon">on <small>(additional fields are shown to the user)</small></label>\
						</div><br><br>\
					<label>Field 1</label><input type="checkbox" id="field1enabled" name="field1enabled" value="1" checked="1"><label class="inline" for="field1enabled">enabled</label> <label class="inline" for="field1title">Title</label><input type="text" name="field1title" id="field1title" class="txt" value="Your comments about item for the merchant"> <label class="inline" for="field1type">Type</label><select name="field1type" id="field1type"><option value="1">Textarea</option><option value="2">Text field</option></select><br>\
          <label>Field 2</label><input type="checkbox" id="field2enabled" name="field2enabled" value="1"><label class="inline" for="field2enabled">enabled</label> <label class="inline" for="field2title">Title</label><input type="text" name="field2title" id="field2title" class="txt"> <label class="inline" for="field2type">Type</label><select name="field2type" id="field2type"><option value="1">Textarea</option><option value="2">Text field</option></select><br>\
          </form>\
        ';
      this.wnd = Dialog.confirm(f, {windowParameters: {className:"alphacube", width:600, title:"Category", showEffect: Element.show, hideEffect: Element.hide},
                        ok: this.onok.bind(this) });
      this.form = this.wnd.getContent().getElementsByTagName("FORM")[0];
      this.form.onsubmit = this.onsubmit.bind(this);

      Form.focusFirstElement(this.form);

      if(category_id) {
        // load data
        this.json = null;
        new Ajax.Request('ajax_products.php', {parameters:'action=load_category&id='+category_id, onSuccess:this.onload.bind(this)}); // TODO: handle AJAX errors here
      }
      else {
        // defaults
        this.wnd.hideloader();
      }
    },

    onload: function(t, json) {
      jslog.debug(t.getResponseHeader("X-JSON"));
      populate_controls(this.form, json);
      this.wnd.hideloader();
    },

    onsubmit: function() {
      this.onok();
      return false;
    },

    onsave: function(t) {
      jslog.debug(t.responseText);
      update_categories();
      this.wnd.hide();
    },
    
    onok: function() {
      this.wnd.showloader();
      new Ajax.Request('ajax_products.php', {parameters:'action=save_category&id='+this.category_id+"&"+Form.serialize(this.form), onSuccess:this.onsave.bind(this)}); // TODO: handle AJAX errors here
    }
  }

  function category_deleted(t)
  {
    update_categories();
  }
  
  function delete_category(category_id)
  {
    if(confirm("Are you sure you want to delete category?")) {
      new Ajax.Request('ajax_products.php', {parameters:'action=delete_category&id='+category_id, onSuccess:category_deleted}); // TODO: handle AJAX errors here
    }
  }
  
  function page(start)
  {
    new Ajax.Updater('categories', 'ajax_products.php', {parameters: "action=list_categories&start="+start});  // add AJAX error handling here
  }
  function order(field)
  {
    new Ajax.Updater('categories', 'ajax_products.php', {parameters: "action=list_categories&order="+field});  // add AJAX error handling here
  }
  function update_categories()
  {
    new Ajax.Updater('categories', 'ajax_products.php', {parameters: "action=list_categories"});  // add AJAX error handling here
  } 
  update_categories();
</script> 
{/literal}

{include file="adm_footer.tpl"}
