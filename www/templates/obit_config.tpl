{literal}
	var ObituariesConfig = Class.create();
	ObituariesConfig.prototype = {
		initialize: function() {
{/literal}			
{capture name=pages}
	{html_options options=$pages}
{/capture}
			var f = '<form>\
					  <label>Candles policy</label><input type="radio" id="on" name="candles_policy" value="on"><label class="inline" for="on">on</label><input type="radio" id="moderated" name="candles_policy" value="moderated"><label class="inline" for="moderated">moderated</label><input type="radio" id="off" name="candles_policy" value="off"><label class="inline" for="off">off</label><br><br>\
					  <label for="captcha">CAPTCHA</label><input type="checkbox" id="captcha" name="captcha" class="chk"><br><br>\
  				  <label for="flowers">Flowers</label><input type="checkbox" id="flowers" name="flowers" class="chk"> <label class="inline" for="flowers_page">flowers page:</label><select id="flowers_page" name="flowers_page_id">{$smarty.capture.pages|strip|escape:"quotes"}</select><br><br>\
					  <label for="light_candle">Light a candle txt</label><input type="text" id="light_candle" name="light_candle" class="txt2"><br>\
					  <label>&nbsp;</label><small>Standard: Light a candle by adding your thoughts and memories</small><br><hr size="1">\
					  <label for="qe_enabled">Quick Edit On</label><input type="checkbox" id="qe_enabled" name="quick_edit_enabled" class="chk"><br>\
					  <label for="qe_codeword">Codeword</label><input type="text" id="qe_codeword" name="quick_edit_codeword" class="txt"><br>\
					  <label for="rc_codeword">Remove Candle</label><input type="text" id="rc_codeword" name="remove_candle_codeword" class="txt">\
					 </form>\
				';
{literal}
			this.wnd = Dialog.confirm(f, {windowParameters: {className:"alphacube", width:430, title:"Obituaries configuration", showEffect: Element.show, hideEffect: Element.hide},
									      ok: this.onok.bind(this) });
			this.form = this.wnd.getContent().getElementsByTagName("FORM")[0];
			Form.focusFirstElement(this.form);
			// load data
			new Ajax.Request('obituaries.php', {parameters:'action=load_config', onSuccess:this.onload.bind(this)}); // TODO: handle AJAX errors here
		},

		onload: function(t, json) {
			jslog.debug(t.getResponseHeader("X-JSON"));
			var form = this.form;
			populate_controls(this.form, json);
			this.wnd.hideloader();
		},

		onsave: function(t) {
			jslog.debug(t.responseText);
			this.wnd.hide();
	    },
		
		onok: function() {
			this.wnd.showloader();
			new Ajax.Request('obituaries.php', {parameters:'action=save_config&'+Form.serialize(this.form), onSuccess:this.onsave.bind(this)}); // TODO: handle AJAX errors here
		}
	}

  var ObituaryTheme = Class.create();
	ObituaryTheme.prototype = {
		initialize: function() {
{/literal}			
{capture name=border_styles}
	{html_options options=$border_styles}
{/capture}
{capture name=theme_images}
	{html_options options=$theme_images}
{/capture}
			var f = '<form>\
  				  <label for="theme_frame">Frame style</label> <select id="theme_frame" name="theme_frame">{$smarty.capture.border_styles|strip|escape:"quotes"}</select><br>\
            <label for="theme_frameoutborder">Frame style overrides:</label><br>\
            <label for="theme_width">Frame width X height</label><input type="text" id="theme_width" name="theme_width" class="small">px X <input type="text" id="theme_height" name="theme_height" class="small">px <br>\
            <label for="theme_frameoutbordersize">Outer border</label><input type="text" id="theme_frameoutbordersize" name="theme_frameoutbordersize" class="small">px <input type="text" id="theme_frameoutbordercolor" name="theme_frameoutbordercolor" class="color"><input type="button" value=" .. " onclick="showColorPicker(this,$(\'theme_frameoutbordercolor\'))"><br>\
            <label for="theme_frameinbordersize">Inner border</label><input type="text" id="theme_frameinbordersize" name="theme_frameinbordersize" class="small">px <input type="text" id="theme_frameinbordercolor" name="theme_frameinbordercolor" class="color"><input type="button" value=" .. " onclick="showColorPicker(this,$(\'theme_frameinbordercolor\'))"><br><br>\
            <label for="theme_bgcolor">Background color</label><input type="text" id="theme_bgcolor" name="theme_bgcolor" class="color"><input type="button" value=" .. " onclick="showColorPicker(this,$(\'theme_bgcolor\'))"><br>\
  				  <label for="theme_image">Theme image</label> <select id="theme_image" name="theme_image">{$smarty.capture.theme_images|strip|escape:"quotes"}</select><br>\
					 </form>\
				';
{literal}
			this.wnd = Dialog.confirm(f, {windowParameters: {className:"alphacube", width:430, title:"Obituaries theme", showEffect: Element.show, hideEffect: Element.hide},
									      ok: this.onok.bind(this) });
			this.form = this.wnd.getContent().getElementsByTagName("FORM")[0];
			Form.focusFirstElement(this.form);
			// load data
			new Ajax.Request('obituaries.php', {parameters:'action=load_theme', onSuccess:this.onload.bind(this)}); // TODO: handle AJAX errors here
		},

		onload: function(t, json) {
			jslog.debug(t.getResponseHeader("X-JSON"));
			var form = this.form;
			populate_controls(this.form, json);
			this.wnd.hideloader();
		},

		onsave: function(t) {
			jslog.debug(t.responseText);
			this.wnd.hide();
	  },
		
		onok: function() {
			this.wnd.showloader();
			new Ajax.Request('obituaries.php', {parameters:'action=save_theme&'+Form.serialize(this.form), onSuccess:this.onsave.bind(this)}); // TODO: handle AJAX errors here
		}
	}
{/literal}
