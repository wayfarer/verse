{literal}
<style>
#obit, #center { font: 10pt Tahoma; color: #000; width: 100%; margin-bottom: 10px }
#center td { padding: 5px }
#obit td.left { width: 33px; background-image: url(/img/obit01-left.jpg) }
#obit td.right { width: 30px; background-image: url(/img/obit01-right.jpg) }
#obit td.center { background-image: url(/img/obit01-bg.jpg) }
#obit div.center { text-align: center; }
#obit td.topcenter { background-image: url(/img/obit01-topbg.jpg) }
#obit td.bottomcenter { background-image: url(/img/obit01-bottombg.jpg) }
#obit div.name {
	padding: 20px 70px 0 70px;	
  text-align: left;
}
#obit div.date {
	padding-top: 10px;
	padding-left: 70px;
  text-align: left;
}
#obit label {
	width: 150px;
	display: block;
	float: left;
}
#obit div.submit {
  padding: 10px 70px 20px 120px;
  text-align: left;
}
#obit input {
	margin-bottom: 5px;
	*margin-bottom: 0;
}
#obit input.dateb {
	width: 25px;
}
#obit h3 {
	color: #5B1313;
	font: bold 1.1em Tahoma;
	letter-spacing: 0.01;
}
#obit input.search {
	width: 110px;
	height: 25px;
	background-image: url(/img/jc_obitbutton.png);
	color: #FFF;
	border: 0;
}
</style>

<table cellpadding="0" cellspacing="0" id="obit">
<tr><td width="33"><img src="/img/obit01-topleft.jpg"></td><td class="topcenter"></td><td width="30"><img src="/img/obit01-topright.jpg"></td></tr>
<tr><td class="left"></td><td valign="top" class="center">
<div class="center">
    <div class="name">
		<h3>Search by Name</h3>
		<label for="first_name">First Name:</label><input type="text" name="first_name" id="first_name"><br>
		<label for="last_name">Last Name:</label><input type="text" name="last_name" id="last_name"><br>
		<label for="home_place">Home City:</label><input type="text" name="city" id="home_place"><br>
		<label for="phrase">Phrase in Memorial's text:</label><input type="text" name="phrase" id="phrase">
	</div>
	<div class="date">
		<h3>Search by Date Range</h3>
		<label for="from_date">From date:</label><input id="from_date" name="from_date" type="text" size="12"><input type="button" class="dateb" id="from_date_b" value=".."><br>
		<label for="to_date">To date:</label><input id="to_date" name="to_date" type="text" size="12"><input type="button" class="dateb" id="to_date_b" value=".."><br>
	</div>
	<div class="submit">
		<input type="submit" value="Search" class="search">
	</div>
</div>
</td><td class="right"></td></tr>
<tr><td width="33"><img src="/img/obit01-bottomleft.jpg"></td><td class="bottomcenter"></td><td width="30"><img src="/img/obit01-bottomright.jpg"></td></tr>
</table>
{/literal}
