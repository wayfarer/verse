<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<body>
<style>
#obit {ldelim}
  width: 100%;
  font: 12px Tahoma;
  {if $config.theme_frame.outbordersize}
  border: {$config.theme_frame.outbordersize}px solid {$config.theme_frame.outbordercolor};
  {/if}
}
#obit h2 {ldelim}
  font-size: 15px;
  color: #7C181D;
  margin: 0;
}
#obit td.right {ldelim}
  {if $config.theme_image neq "noimage"}
  padding: 0px 0px 10px 10px;
  {else}
  padding: 25px 0px 10px 10px;
  {/if}
}
#obit div.inner {ldelim}
  {if $config.theme_frame.inbordersize}
  border: {$config.theme_frame.inbordersize}px solid {$config.theme_frame.inbordercolor};
  {/if}
  margin: 0px 30px 30px 30px;
}
{literal}
#obit table {
  font: 13px Tahoma;
  width: 100%;
  border-spacing: 0;
  border: 0;
}
#obit table.inner {
  width: auto;
}
#obit td.left {
  width: 220px;
}

#obit div.photo {
  text-align: center;
  padding: 10px 0 20px 0;
}
#obit div.info {
  margin-bottom: 20px;

}
#obit div.info p.info {
  margin: 0;
  line-height: 1.4em;
}
#obit div.outtop {
  width: 100%;
  text-align: center;
}
#obit div.obitcontent {

}
#obit div.candle {
  margin: 5px 0 20px 0;
}
#obit div.candle div.from {
  color: #0876B5;
  font-size: smaller;
  padding-bottom: 3px;
}

td {
  vertical-align: top;
}
img {
  border: 0;
}
h1 {
  font: 21px Arial;
}
div.splitter {
  height: 8px;
  width: 100%;
  font-size: 0px;
  padding-top: 3px;
}
</style>
{/literal}

<div id="obit" style="page-break-after: always">
<div class="outtop"><h1>{$data.first_name} {$data.middle_name} {$data.last_name}</h1></div>
<div class="inner">
<table class="inner" cellspacing="0">
<tr><td class="left">
    <div class="photo">
      {ldelim}photo}
    </div>
  </td>
  <td class="right">
    {if $config.theme_image neq "noimage"}
    <img src="/{$config.theme_image}" width="100%">
    {/if}
    <div style="padding-right: 15px">
    <div class="info">
      <h2>Funeral information</h2>
      <div class="splitter">
        <img src="/img/splitter_print.gif">
      </div>
      
      <table cellspacing="0">
        <tr>
          <td><strong>Home:</strong> {$data.home_place}</td>
          <td><strong>Place of Birth:</strong> {$data.birth_place}</td>
        </tr>
        <tr>    
          <td><strong>Date of Death:</strong> {$data.death_date}</td>
          <td><strong>Birthdate:</strong> {$data.birth_date}</td>
        </tr>
        <tr>
          <td></td>
          <td>
            {if $data.age > 0}
            <strong>Age:</strong> {$data.age}
            {/if}
          </td>
        </tr>
      </table>
      <div class="splitter">
        <img src="/img/splitter_print.gif">
      </div>

{if $smarty.session.domain_id eq 535}
      {if $data.visitation_date}
      <p class="info"><strong>Viewing:</strong> {$data.visitation_date}</p>
      {/if}
      <p class="info"><strong>Service information:</strong> {$data.service_date} {$data.service_place}</p>
{else}
      <p class="info"><strong>Service information:</strong> {$data.service_date} {$data.service_place}</p>
      {if $data.visitation_date}
      <p class="info"><strong>Visitation:</strong> {$data.visitation_date}</p>
      {/if}
{/if}
      {if $data.final_disposition}
      <p class="info"><strong>Interment:</strong> {$data.final_disposition}</p>
      {/if}

      <div class="splitter">
        <img src="/img/splitter_print.gif">
      </div>
    </div>
    <div class="obitcontent">
      {ldelim}obit_content}
    </div>
    </div>
  </td>
</tr></table>
</div>
</div>
</body>
</html>
