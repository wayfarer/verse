{literal}
<style>
#obit { font: 10pt Tahoma; color: #000; background-color: #FFF}
#obit td.content { padding: 4px; }
#obit td.left { width: 25px; background-image: url(/img/obit02-left.jpg); }
#obit td.right { width: 25px; background-image: url(/img/obit02-right.jpg); }
#obit a { color: #000; }
</style>
<table cellpadding="0" cellspacing="0" id="obit" style="page-break-after: always">
<tr><td colspan="5"><img width="617" height="30" src="/img/obit02-top.jpg"></td></tr>
<tr valign="top"><td class="left"><img width="25" height="800" src="/img/obit02-left.jpg"></td><td width="272" class="content">{obituary_left_content}</td><td><img width="7" height="800" src="/img/obit02-center.jpg"></td><td width="272" class="content"><center><img src="/img/obit02-extra1.jpg"></center><br>{obituary_right_content}<br><br><center><img src="/img/obit02-extra2.jpg"></center></td><td class="right"><img width="25" height="800" src="/img/obit02-right.jpg"></td></tr>
<tr><td colspan="5"><img width="617" height="30" src="/img/obit02-bottom.jpg"></td></tr>
</table>
{/literal}