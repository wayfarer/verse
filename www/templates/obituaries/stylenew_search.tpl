<div align="center" id="os">
<table cellspacing="0" cellpadding="0" align="center" border="0">
    <tr align="center">
        <td valign="top" align="center"><br />
        <br />
        <div class="main_container">
        <div class="center">
        <div class="name">
        <h2>Search Obituaries</h2>
        <h3>Search by Name</h3>
        <label>First Name:</label><input id="os_first_name" name="first_name" type="text" /><br />
        <label>Last Name:</label><input id="os_last_name" name="last_name" type="text" /><br />
        <label>Home City:</label><input id="os_city" name="city" type="text" /><br />
        <label>Phrase in Memorial's text:</label><input id="os_phrase" name="phrase" type="text" /></div>
        <div class="date">
        <h3>Search by Date Range</h3>
        <label>From date:</label><input id="from_date" size="12" name="from_date" type="text" /><input class="#" id="from_date_b" type="button" value=".." /><br />
        <label>To date:</label><input id="to_date" size="12" name="to_date" type="text" /><input class="#" id="to_date_b" type="button" value=".." /><br />
        &nbsp;</div>
        <div class="submit"><input class="btn" type="submit" value="Search" /></div>
        </div>
        </div>
        <br />
        <p>{ldelim}last_obituaries}</p>
        </td>
    </tr>
</table>
</div>
