{literal}
<style>
#obit { font: 10pt Tahoma; color: #000; width: 617px }
#obit td.cntleft { padding: 4px; padding-bottom: 60px; background: #FFF url(img/obit02-extra4.jpg) no-repeat left bottom }
#obit td.cntright { padding: 4px; background: #FFF url(img/obit02-extra3.jpg) no-repeat right bottom }
#obit td.top { 
	height: 30px; 
	background-image: url(img/obit02-top.png);
	*background-image: none;
	filter:progid:DXImageTransform.Microsoft.AlphaImageLoader(enabled=true, sizingMethod=scale src="img/obit02-top.png");
}
#obit td.left { 
	width: 25px; 
	background-image: url(img/obit02-left.png);
	*background-image: none;
	filter:progid:DXImageTransform.Microsoft.AlphaImageLoader(enabled=true, sizingMethod=scale src="img/obit02-left.png");
}
#obit td.right { 
	width: 25px; 
	background-image: url(img/obit02-right.png);
	*background-image: none;
	filter:progid:DXImageTransform.Microsoft.AlphaImageLoader(enabled=true, sizingMethod=scale src="img/obit02-right.png");
}
#obit td.bottom { 
	height: 40px; 
	background-image: url(img/obit02-bottom.png);
	*background-image: none;
	filter:progid:DXImageTransform.Microsoft.AlphaImageLoader(enabled=true, sizingMethod=scale src="img/obit02-bottom.png");
}
#obit td.center { 
	width: 7px;
	background-image: url(img/obit02-center.jpg);
}
#obit a { color: #000; }
#obit td.cntright a {
  text-decoration: underline;
}
#obit td.cntright a:hover {
  color: #00D;
}
</style>
<table cellpadding="0" cellspacing="0" id="obit">
<tr><td colspan="5" class="top"></td></tr>
<tr valign="top"><td class="left"></td><td width="272" class="cntleft">{obituary_left_content_jc}</td><td class="center"></td><td width="272" class="cntright"><center><img src="img/obit02-extra1.jpg"></center><br>{obituary_right_content_jc}<br><br><center><img src="img/obit02-extra2.jpg"></center></td><td class="right"></td></tr>
<tr><td colspan="5" class="bottom"></td></tr>
</table>
{/literal}