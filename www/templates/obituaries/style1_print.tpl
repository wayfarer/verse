{literal}
<style>
#obit, #center { font: 10pt Tahoma; color: #000; width: 528px }
#center td { padding: 5px }
#obit td.left { width: 33px; background-image: url(/img/obit01-left.jpg) }
#obit td.right { width: 30px; background-image: url(/img/obit01-right.jpg) }
#obit div.name { font: bold 15px Tahoma; color: #733636 }
#candle { background-color: #E5E0CE; border: 1px solid #A79A6C; width: 100%; font: 8pt Tahoma; color: #A43114; padding: 8px }
#candle img { border: 0 }
#candle a { color: #C96F0E; text-decoration: none; }
</style>

<table cellpadding="0" cellspacing="0" id="obit">
<tr><td colspan="3"><img src="/img/obit01-top.jpg"></td></tr>
<tr><td class="left"><img src="/img/obit01-left.jpg" width="33" height="800"></td><td valign="top">
<table cellpadding="0" cellspacing="0" id="center">
<tr><td valign="top">{photo}<div class="name">{name}</div>

</td><td>
<img src="/img/obit01-extra1.gif">
<div>{funeral_info}</div>
<br>
<img src="/img/obit01-extra2.gif">
<div>{biography}</div>
</td></tr>
</table>
</td><td class="right"><img src="/img/obit01-right.jpg" width="30" height="800"></td></tr>
<tr><td colspan="3"><img src="/img/obit01-bottom.jpg"></td></tr>
</table>
{/literal}