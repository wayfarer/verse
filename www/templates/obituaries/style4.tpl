{literal}
<style>
#obit { width: 100%; font: 10pt Tahoma; color: #000 }
#obit td { vertical-align: top }
#obit td.topcenter { padding: 5px; border-top: 1px solid #EBEBEB; text-align: center; vertical-align: middle }
#obit td.info { padding: 5px; background-color: #E5E5E5; border: 1px solid #EBEBEB }
#obit td.bio { padding: 5px; border: 1px solid #EBEBEB; border-top: 0 }
#obit div.bio { margin-bottom: 20px }
#obit div.name { font: bold 15px Tahoma; color: #AD8414 }
#candle { border: 1px solid #EBEBEB; width: 100%; font: 12px Tahoma; color: #A43114; padding: 8px }
#candle div.lightacandle img { border: 0; float: left; margin-right: 10px }
#candle a { color: #C96F0E; text-decoration: none; }
#candle div.lightacandle { width: 150px; float: left; margin-right: 10px }
#candle hr { display: none; }
#obit td.topleft { 
	background: url(/img/obit04-topleft.png) no-repeat;
	*background-image: none;
	filter:progid:DXImageTransform.Microsoft.AlphaImageLoader(enabled=true, sizingMethod=crop src="/img/obit04-topleft.png");
}
#obit td.topright { 
	background: url(/img/obit04-topright.png) no-repeat;
	*background-image: none;
	filter:progid:DXImageTransform.Microsoft.AlphaImageLoader(enabled=true, sizingMethod=crop src="/img/obit04-topright.png");
}
</style>
<style media="print">
#candle { display: none }
a.print { display: none }
</style>
<table cellpadding="0" cellspacing="0" id="obit">
<tr height="183">
<td>
<table cellpadding="0" cellspacing="0" width="100%">
<tr><td width="170" class="topleft"><div style="height:183px"></div></td>
<td class="topcenter">
{/literal}
{if !$smarty.get.print}
  <a class="print" href="{ldelim}print_link}" target="_blank"><img style="vertical-align:middle" src="/img/print.gif"></a>&nbsp;
{if $config.candles_policy neq "off"}
  <a href="{ldelim}all_candles_link}" title="{if $smarty.session.domain_id neq 154}view all candles{else}view all messages{/if}"><img src="/img/{if $smarty.session.domain_id neq 154}candlesall.gif{else}messagesall.jpg{/if}" style="vertical-align:middle"></a>
{/if}
  {if $config.flowers_page_id}
    <a href="/{$config.flowers_page}" title="Send Flowers"><img src="/img/sendflowers.gif" style="vertical-align:middle"></a>
  {/if}
{include file="cnt_plugin_obit_movieclips.tpl"}
{/if}
{literal}
</td>
<td class="topcenter">{photo}
<div class="name">{name}</div>
</td><td width="100" class="topright"></td></tr>
</table>
</td>
</tr>
<tr><td class="info"><img src="/img/obit04-extra1.gif"><div>{funeral_info}</div></td></tr>
<tr><td class="bio"><img src="/img/obit04-extra2.gif"><div class="bio">{biography}</div>
<!-- <table cellpadding="0" cellspacing="0" id="candle">
<tr><td width="150"><a href="{light_candle_link}"><img src="img/candle_big.gif" align="left" style="margin-right: 10px">Light a candle by<br>adding your<br>thoughts and memories</a></td><td valign="top"><div>Select a message to display:</div>{candles}</td></tr>
</table> -->
<table cellpadding="0" cellspacing="0" id="candle">
  <tr><td>{candles}</td></tr>
</table>
</td></tr>
</table>
{/literal}
