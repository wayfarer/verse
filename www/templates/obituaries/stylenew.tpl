<style>
#obit {ldelim}
  width: 100%;
  position: relative;
  font: 12px Tahoma;
  z-index: 1;
  background: {$config.theme_bgcolor};
}
#obit div.in {ldelim}
  {if $config.theme_frame.outbordersize}
  border: {$config.theme_frame.outbordersize}px solid {$config.theme_frame.outbordercolor};
  {/if}
}
#obit div.topleft {ldelim}
  position: absolute;
  {if $config.theme_frame.topleftimg}
  background: url(/{$config.theme_frame.topleftimg}) top left no-repeat;
  {/if}
  width: 50%;
  height: 50%;
  z-index: -1;
  top: 0;
  left: 0;
}
#obit div.topright {ldelim}
  position: absolute;
  top: 0;
  right: 0;
  {if $config.theme_frame.toprightimg}
  background: url(/{$config.theme_frame.toprightimg}) top right no-repeat;
  {/if}
  width: 50%;
  height: 50%;
  z-index: -1;
}
#obit div.bottomleft {ldelim}
  position: absolute;
  left: 0;
  bottom: 0;
  {if $config.theme_frame.bottomleftimg}
  background: url(/{$config.theme_frame.bottomleftimg}) bottom left no-repeat;
  {/if}
  width: 50%;
  height: 50%;
  z-index: -1;
}
#obit div.bottomright {ldelim}
  position: absolute;
  bottom: 0;
  right: 0;
  {if $config.theme_frame.bottomrightimg}
  background: url(/{$config.theme_frame.bottomrightimg}) bottom right no-repeat;
  {/if}
  width: 50%;
  height: 50%;
  z-index: -1;
}
#obit div.outtop {ldelim}
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  height: {$config.theme_frame.height}px;
  text-align: center;
  {if $config.theme_frame.outtopimg}
	background: {$config.theme_frame.bgcolor} url(/{$config.theme_frame.outtopimg}) left {$config.theme_frame.outtopimgoffset}px repeat-x;
	{else}
  background: {$config.theme_frame.bgcolor};
	{/if}
  z-index: -2;
}
#obit div.outtop h1 {ldelim}
  padding: {$config.theme_frame.titletopoffset}px 0 0 0;
  color: {$config.theme_frame.titlecolor};
}
#obit div.outtop h1 span {ldelim}
  background-color: {$config.theme_frame.titlebgcolor};
  padding: 15px;
}
#obit h2 {ldelim}
  font-size: 15px;
  color: #7C181D;
  margin: 0;
}

#obit div.outleft {ldelim}
  position: absolute;
  left: 0;
  top: 0;
  height: 100%;
  {if $config.theme_frame.outleftimg}
  background: {$config.theme_frame.bgcolor} url(/{$config.theme_frame.outleftimg}) {$config.theme_frame.outleftimgoffset}px top repeat-y;
	{else}
  background: {$config.theme_frame.bgcolor};
	{/if}
  padding: 0 13px 0 38px;
  z-index: -2;
}
#obit div.outright {ldelim}
  position: absolute;
  top: 0;
  right: 0;
  height: 100%;
  {if $config.theme_frame.outrightimg}
  background: {$config.theme_frame.bgcolor} url(/{$config.theme_frame.outrightimg}) {$config.theme_frame.outrightimgoffset}px top repeat-y;
	{else}
  background: {$config.theme_frame.bgcolor};
	{/if}
  padding: 0 38px 0 13px;
  z-index: -2;
}
#obit div.outbottom {ldelim}
  position: absolute;
  bottom: 0;
  left: 0;
  width: 100%;
  {if $config.theme_frame.outbottomimg}
  background: {$config.theme_frame.bgcolor} url(/{$config.theme_frame.outbottomimg}) left {$config.theme_frame.outbottomimgoffset}px repeat-x;
	{else}
  background: {$config.theme_frame.bgcolor};
	{/if}
  xpadding: 15px 0 55px 0;
  height: {$config.theme_frame.height}px;
  z-index: -2;
  font-size: 0;
}
#obit td.right {ldelim}
  {if $config.theme_image neq "noimage"}
  background: url(/{$config.theme_image}) top right no-repeat;
  {/if}
  padding: 80px 15px 10px 10px;
}

#obit div.candles {ldelim}
  padding-bottom: 150px;
  {if $config.theme_allcandlesbottomimage}
  background: url(/{$config.theme_allcandlesbottomimage}) bottom left no-repeat;
	{/if}
}
#obit div.lightacandleform {ldelim}
  {if $config.theme_lightacandlethemeimage}
  background: url(/{$config.theme_lightacandlethemeimage}) -184px 95px no-repeat;
	{/if}
}
#obit div.inner {ldelim}
  padding-bottom: 5px;
  {if $config.theme_frame.inbordersize}
  border: {$config.theme_frame.inbordersize}px solid {$config.theme_frame.inbordercolor};
  {/if}
  margin: {$config.theme_frame.height}px {$config.theme_frame.width}px;
}
{literal}
#obit table {
  font: 13px Tahoma;
  width: 100%;
  border-spacing: 0;
  border: 0;
}
#obit a {
  font-family: Tahoma;
}
#obit td.left {
  width: 220px;
}

#obit div.menu div.item {
  padding-left: 55px;
  height: 65px;
  margin-left: 15px;
}
#obit div.menu div.item a {
  color: #222;
  text-decoration: none;
}
#obit div.menu div.item a:hover {
  text-decoration: underline;
}
#obit div.item div.title {
  font-weight: bold;
  color: #0875B3;
}
#obit div.item div.desc {
  font-size: 11px;
}
#obit div.lightacandle {
  background: url(/img/ob_lightacandle2.jpg) no-repeat;
}
#obit div.viewallcandles {
  background: url(/img/ob_viewallcandles2.jpg) no-repeat;
}
#obit div.sendaflower {
  background: url(/img/ob_sendflowers2.jpg) no-repeat;
}
#obit div.subscribe {
  background: url(/img/ob_subscribe2.jpg) no-repeat;
}

#obit div.photo {
  text-align: center;
  padding: 50px 0 20px 0;
}
#obit div.info {
  margin-bottom: 20px;
  position: relative;
}
#obit div.info p.info {
  margin: 0;
  line-height: 1.4em;
}
#obit div.info div.print {
  position: absolute;
  right: 0;
  top: -8px;
  font-weight: bold;
}
#obit div.info div.print a {
  display: block;
  color: #0875B3;
  padding: 5px 0 6px 32px;
  background: url(/img/ob_print.gif) no-repeat;
}

#obit div.obitcontent {
  position: relative;
}
#obit div.obitcontent div.actions {
  position: absolute;
  right: 0;
  top: -14px;
  font-weight: bold;
}
#obit div.obitcontent div.actions a {
  display: block;
  color: #0875B3;
  padding: 15px 0 6px 47px;
  float: left;
}
#obit div.obitcontent div.actions a.photo {
  background: url(/img/ob_photobg.gif) no-repeat;
}
#obit div.obitcontent div.actions a.video {
  background: url(/img/ob_videobg.gif) no-repeat;
}

#obit div.candle {
  margin: 5px 0 20px 0;
}
#obit div.candle div.from {
  color: #0876B5;
  font-size: smaller;
  padding-bottom: 3px;
}

td {
  vertical-align: top;
}
</style>
<!--[if lte IE 6]>
<style>
#obit {
  height: 100px;
}
</style>
<![endif]-->
<script>
openwin = function(url, width, height) {
  var width = width || 770;
  var height = height || 570;
  var left = (screen.width-width)/2;
  var top = (screen.height-height)/2;
  win=window.open(url, null, config="scrollbars=no,resizable=no,toolbar=no,location=no,menubar=no,width="+width+",height="+height+",top="+top+",left="+left+"");
  win.focus();
}
</script>
{/literal}
<div id="obit">
<div class="in">
<div class="topleft"></div>
<div class="topright"></div>
<div class="bottomleft"></div>
<div class="bottomright"></div>
<div class="outtop"><h1><span>{$data.title} {$data.first_name} {$data.middle_name} {$data.last_name} {$data.suffix}</span></h1></div>
<div class="outright"></div>
<div class="outbottom"></div>
<div class="outleft"></div>
<div class="inner">
<table class="inner" cellspacing="0">
<tr><td class="left">
    <div class="photo">
      {ldelim}photo}
    </div>
    <div class="menu">
      {if $config.candles_policy neq "off"}
      <div class="item lightacandle">
        <a href="{ldelim}light_candle_link}">
          <div class="title">Light a candle</div>
          <div class="desc">Add your thoughts and memories</div>
        </a>
      </div>
      <div class="item viewallcandles">
        <a href="{ldelim}all_candles_link}">
          <div class="title">View all candles ({$data.candles_count})</div>
          <div class="desc">Read other memories</div>
        </a>
      </div>
      {/if}
      {if $config.flowers_page}
      <div class="item sendaflower">
        <a href="/{$config.flowers_page}">
{if $smarty.session.domain_id eq 509}
          <div class="title">Send a Gift</div>
          <div class="desc">Send a condolence gift or flowers</div>
{else}
          <div class="title">Send flowers</div>
          <div class="desc">Send flowers using our service</div>
{/if}
        </a>
      </div>
      {/if}
      {if ($config.subscribe_policy neq "off") and ($config.candles_policy neq "off")}
      <div class="item subscribe">
        <a href="{ldelim}subscribe_link}">
          <div class="title">Subscribe</div>
          <div class="desc">Receive an email when new candles are posted to this obituary</div>
        </a>
      </div>
      {/if}
    </div>
  </td>
  <td class="right">
    <div class="info">
      <div class="print">
        <a href="{ldelim}print_link}" target="_blank">Print</a>
      </div>
      <h2>Funeral information</h2>
      <div class="splitter"></div>
      <table cellspacing="0">
        <tr>
          <td><strong>Home:</strong> {$data.home_place}</td>
          <td><strong>Place of Birth:</strong> {$data.birth_place}</td>
        </tr>
        <tr>
          <td><strong>Date of Death:</strong> {$data.death_date}</td>
          <td><strong>Birthdate:</strong> {$data.birth_date}</td>
        </tr>
        <tr>
          <td></td>
          <td>
            {if $data.age > 0}
            <strong>Age:</strong> {$data.age}
            {/if}
          </td>
        </tr>
      </table>
      <div class="splitter"></div>
{if $smarty.session.domain_id eq 535}
      {if $data.visitation_date}
      <p class="info"><strong>Viewing:</strong> {$data.visitation_date}</p>
      {/if}
      <p class="info"><strong>{$obituary_service_types[$data.service_type]} Service:</strong> {$data.service_date} {$data.service_place}</p>
{else}
      {if $data.service_date}
      <p class="info"><strong>{$obituary_service_types[$data.service_type]} Service:</strong> {$data.service_date} {$data.service_place}</p>
      {/if}
      {if $data.visitation_date}
      <p class="info"><strong>Visitation:</strong> {$data.visitation_date}</p>
      {/if}
{/if}
      {if $data.final_disposition}
      <p class="info"><strong>Interment:</strong> {$data.final_disposition}</p>
      {/if}
      <div class="splitter"></div>
{if $data.scrapbookhere}
  <a href="#" title="view photoalbum" onclick="openwin('/{$data.scrapbookpage}/{$config.obituary_id}'); return false;"><img src="/img/album.gif" alt="view photoalbum" style="vertical-align:middle">click to view photoalbum</a><br>
{/if}
{foreach from=$data.movieclips item=movieclip}
  <a href="#" title="view movie" style="padding-top: 6px; display: block" onclick="openwin('/mcplayer.php?id={$movieclip.movieclip_id}&detectflash=false',475,390); return false;"><img src="/img/movieclip.gif" alt="view movie" style="float:left;margin-top:-6px">{$movieclip.title}</a><br>
{/foreach}
    </div>
    <div class="obitcontent">
      <div class="actions">
<!--        <a class="photo" href="#">Photo</a>
        <a class="video" href="#">Video</a> -->
      </div>
      {ldelim}obit_content}
    </div>
  </td>
</tr></table>
</div>
</div>
</div>