{literal}
<style>
#obit, #center { font: 10pt Tahoma; color: #000 }
#center td { padding: 5px }
#obit td.left { width: 33px; background-image: url(/img/obit01-left.jpg) }
#obit td.right { width: 30px; background-image: url(/img/obit01-right.jpg) }
#obit td.center { background-image: url(/img/obit01-bg.jpg) }
#obit td.topcenter { background-image: url(/img/obit01-topbg.jpg) }
#obit td.bottomcenter { background-image: url(/img/obit01-bottombg.jpg) }
#obit div.name { font: bold 15px Tahoma; color: #733636 }
#candles { background-color: #E5E0CE; border: 1px solid #A79A6C; font: 8pt Tahoma; color: #A43114; padding: 8px }
#candles img { border: 0 }
#candles a { color: #C96F0E; text-decoration: none; }
</style>

<table cellpadding="0" cellspacing="0" id="obit">
<tr><td width="33"><img src="/img/obit01-topleft.jpg"></td><td class="topcenter"></td><td width="30"><img src="/img/obit01-topright.jpg"></td></tr>
<tr><td class="left"><img src="/img/obit01-left.jpg"></td><td valign="top" class="center">
<table cellpadding="0" cellspacing="0" id="center">
<tr><td valign="top">{photo}<div class="name">{name}</div><br>
<a href="{print_link}" align="center"><img border="0" src="/img/print.gif"></a>
{/literal}
{if $config.candles_policy neq "off"}
<a href="{ldelim}all_candles_link}" title="{if $smarty.session.domain_id neq 154}view all candles{else}view all messages{/if}"><img src="/img/{if $smarty.session.domain_id neq 154}candlesall.gif{else}messagesall.jpg{/if}"></a>
{/if}
<br>
{include file="cnt_plugin_obit_movieclips.tpl"}
{literal}
<br>
{candles}
</td><td>
<img src="/img/obit01-extra1.gif">
<div>{funeral_info}</div>
<br>
<img src="/img/obit01-extra2.gif">
<div>{biography}</div>
</td></tr>
</table>
</td><td class="right"><img src="/img/obit01-right.jpg"></td></tr>
<tr><td width="33"><img src="/img/obit01-bottomleft.jpg"></td><td class="bottomcenter"></td><td width="30"><img src="/img/obit01-bottomright.jpg"></td></tr>
</table>
{/literal}