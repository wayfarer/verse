{include file="adm_header.tpl" menu="1"}
{include file="window.tpl"}
{include file="contextmenu.tpl"}
{include file="util.tpl"}

<h1>Email manager</h1>
<div class="actions"><a href="#" onclick="new EmailEditor(0); return false;">new email</a> / <a href="#" onclick="new EmailConfig(); return false;">email config</a></div>
<div id="emails"><img src="img/loader-big.gif"></div>
{literal}
<style>
	label.inline {
		display: inline;
		float: none;
	}
</style>
{/literal}
<script type="text/javascript">
{include file="populate_controls.tpl"}
{literal}
	var EmailEditor = Class.create();
	EmailEditor.prototype = {
		initialize: function(mailbox_id) {
			// create window
			this.mailbox_id = mailbox_id;
{/literal}
{capture name=domains_opts}
	{html_options options=$domain_list}
{/capture}
			var f = '<form>\
			         <label for="name">login:</label><input type="text" name="name" id="name" disabled="disabled"> <b>-{$postfix}</b><br>\
			         <label>type:</label><input type="radio" name="type" value="0" id="mailbox" class="radio" checked="checked"><label class="inline" for="mailbox">mailbox</label> set pass:<input type="password" name="mail_pass"><br>\
			         <label>&nbsp;</label><input type="radio" name="type" value="1" id="frw" class="radio"><label class="inline" for="frw">forward</label> addresses (one per line):<br><textarea name="forward" style="margin-left: 180px" cols="35" rows="3"></textarea><br>\
					</form>\
				';
{literal}
			this.wnd = Dialog.confirm(f, {windowParameters: {className:"alphacube", width:540, title:"Email", showEffect: Element.show, hideEffect: Element.hide},
									      ok: this.onok.bind(this) });
			this.form = this.wnd.getContent().getElementsByTagName("FORM")[0];
			this.form.onsubmit = this.onsubmit.bind(this);
			if(mailbox_id) {
				// load data
				new Ajax.Request('ajax_emails.php', {parameters:'action=load&id='+mailbox_id, onSuccess:this.onload.bind(this)});
			}
			else {
				// enable name field for new user
				this.form.name.disabled = false;
				this.wnd.hideloader();
			}
		},

		onload: function(t, json) {
			jslog.debug(t.getResponseHeader("X-JSON"));
			populate_controls(this.form, json);
			this.wnd.hideloader();
		},

		onsubmit: function() {
			this.onok();
			return false;
		},

		onsave: function(t) {
			jslog.debug(t.responseText);
			update_emails();
			this.wnd.hide();
		},
		
		onok: function() {
			this.wnd.showloader();
			new Ajax.Request('ajax_emails.php', {parameters:'action=save&id='+this.mailbox_id+"&"+Form.serialize(this.form), onSuccess:this.onsave.bind(this)}); // TODO: handle AJAX errors here
		}
	}
	
	var EmailConfig = Class.create();
	EmailConfig.prototype = {
		initialize: function() {
			var f = '<form>\
			         <label for="email">Default email:</label><input type="text" name="email" id="email" class="txt2"/>\
					 </form>\
				';
			this.wnd = Dialog.confirm(f, {windowParameters: {className:"alphacube", width:540, title:"Configure Default Email", showEffect: Element.show, hideEffect: Element.hide},
									      ok: this.onok.bind(this) });
			this.form = this.wnd.getContent().getElementsByTagName("FORM")[0];
			this.form.onsubmit = this.onsubmit.bind(this);
			// load data
			new Ajax.Request('ajax_emails.php', {parameters:'action=load_domain_email', onSuccess:this.onload.bind(this)});
		},

		onload: function(t, json) {
			jslog.debug(t.getResponseHeader("X-JSON"));
			populate_controls(this.form, json);
			this.wnd.hideloader();
		},

		onsubmit: function() {
			this.onok();
			return false;
		},

		onsave: function(t) {
			jslog.debug(t.responseText);
			update_emails();
			this.wnd.hide();
		},
		
		onok: function() {
			this.wnd.showloader();
			new Ajax.Request('ajax_emails.php', {parameters:'action=save_domain_email&'+Form.serialize(this.form), onSuccess:this.onsave.bind(this)});
		}
	}
	
	function mailbox_deleted(t) {
		update_emails();
	}	
	function delete_mailbox(mailbox_id) {
		if(confirm("Are you sure you want to permamenty delete mailbox?\nAll correspondence on server will be lost!")) {
			new Ajax.Request('ajax_emails.php', {parameters:'action=delete&id='+mailbox_id, onSuccess:mailbox_deleted}); // TODO: handle AJAX errors here
		}
	}

	function mailbox_status(t) {
		alert(t.responseText);
	}	
	function check_mailbox(mailbox_id) {
		new Ajax.Request('ajax_emails.php', {parameters:'action=cyrus&id='+mailbox_id, onSuccess:mailbox_status}); // TODO: handle AJAX errors here
	}

	function page(start)
	{
		new Ajax.Updater('emails', 'ajax_emails.php', {parameters: "action=list&start="+start});  // add AJAX error handling here
	}
	function order(field)
	{
		new Ajax.Updater('emails', 'ajax_emails.php', {parameters: "action=list&order="+field});  // add AJAX error handling here
	}
	function update_emails()
	{
		new Ajax.Updater('emails', 'ajax_emails.php', {parameters: "action=list"});  // add AJAX error handling here
	}	
	update_emails();
</script>	
{/literal}

{include file="adm_footer.tpl"}
