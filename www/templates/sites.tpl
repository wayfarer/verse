{include file="adm_header.tpl" menu="1"}
{include file="window.tpl"}

{literal}
<script type="text/javascript">
	var SiteEditor = Class.create();
	SiteEditor.prototype = {
		initialize: function(site_id) {
			// create window
			this.wnd = new WindowClass("Site properties", 300, 200, [this.onok.bind(this), this.oncancel.bind(this)]);
			this.site_id = site_id;
			// create form
			this.form = Builder.node("FORM");
			// create dialog fields in form
			this.form.innerHTML = '\
					  <label for="title">Site name</label><input type="text" id="title" name="title" class="txt"><br>\
					  <label for="domain_id">Site domain</label><select id="domain_id" name="domain_id"></select><br>\
				';
			this.wnd.content.appendChild(this.form);
			this.wnd.show();
			Form.focusFirstElement(this.form);

			this.domainsloaded = false;
			// load domains
			new Ajax.Request('ajax_sites.php', {parameters:'action=load_domains', onSuccess:this.ondomainsload.bind(this)}); // TODO: handle AJAX errors here
			if(site_id) {
				// load data
				new Ajax.Request('ajax_sites.php', {parameters:'action=load&id='+site_id, onSuccess:this.onload.bind(this)}); // TODO: handle AJAX errors here
			}
			else {
				this.wnd.hideloader();
			}
		},

		onload: function(t, json) {
			if(t) {
				jslog.debug(t.getResponseHeader("X-JSON"));
			}
			if(this.domainsloaded) {
				var form = this.form;
				$H(json).each(function(elem) {
					if(form.elements[elem.key].type=="text" || form.elements[elem.key].type=="textarea") {
						form.elements[elem.key].value = elem.value;
					}
					else { // treat as select
						for(var i=0; i<form.elements[elem.key].length; i++) {
							if(form.elements[elem.key].options[i].value==elem.value) {
								form.elements[elem.key].selectedIndex = i;
								break;
							}
						}
					}
				} );
				this.wnd.hideloader();
			}
			else {
				this.json = json;
				jslog.debug("onload waiting for ondomainsload");
			}
		},

		ondomainsload: function(t, json) {
			jslog.debug(t.getResponseHeader("X-JSON"));
			var form = this.form;
			$H(json).each(function(elem) {
				form.elements["domain_id"].options.add(new Option(elem.value, elem.key));
			} );
			this.domainsloaded = true;
			if(this.json) {
				jslog.debug("ondomainsload passing control to waiting onload");
				this.onload(null, this.json);
			}
			jslog.debug("ondomainsload done");
		},  

		onsave: function(t) {
			jslog.debug(t.responseText);
			this.wnd.close();
			this.wnd = null;
			update_site_list();
		},
		
		onok: function() {
			this.wnd.showloader();
			new Ajax.Request('ajax_sites.php', {parameters:'action=save&id='+this.site_id+"&"+Form.serialize(this.form), onSuccess:this.onsave.bind(this)}); // TODO: handle AJAX errors here
		},

		oncancel: function() {
			this.wnd.close();
			this.wnd = null;
		}
	}

	function site_deleted(t)
	{
		update_site_list();
	}

	function delete_site(site_id)
	{
		if(confirm("Are you sure you want to delete site?")) {
			new Ajax.Request('ajax_sites.php', {parameters:'action=delete&id='+site_id, onSuccess:site_deleted}); // TODO: handle AJAX errors here
		}
	}
</script>	
{/literal}

<h1>Sites management</h1>
<div class="actions"><a href="#" onclick="new SiteEditor(0); return false;">create site</a></div>
<div id="site_list"></div>
{literal}
<script type="text/javascript">
	function update_site_list()
	{
		new Ajax.Updater('site_list', 'ajax_sites.php', {parameters: "action=list"});  // add AJAX error handling here
	}	
	update_site_list();
</script>	
{/literal}

{include file="adm_footer.tpl"}
