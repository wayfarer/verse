<style>
{literal}
div.obit_view_title {
	font-weight: bold;
	float: left;
	padding-right: 4px;
}
{/literal}
</style>
<div class="funeral_info">
<div class="obit_view_title">Home: </div>
<div class="obit_view_cnt">{$data.home_place|default:"&nbsp;"}</div>
<div class="obit_view_title">Date of Death: </div>
<div class="obit_view_cnt">{$data.death_date|default:"&nbsp;"}</div>
{if $data.age > 0}
<div style="float: right; margin-right: 10px">
  <div class="obit_view_title">Age: </div>
  <div class="obit_view_cnt">{$data.age}</div>
</div>
{/if}
<div class="obit_view_title">Birthdate: </div>
<div class="obit_view_cnt">{$data.birth_date|default:"&nbsp;"}</div>
<div class="obit_view_title">Place of Birth: </div>
<div class="obit_view_cnt">{$data.birth_place|default:"&nbsp;"}</div>
<div class="obit_view_title">Service Information: </div>
<div class="obit_view_cnt">{$data.service_date|default:"&nbsp;"} {$data.service_place|default:"&nbsp;"}</div>
<div class="obit_view_title">Visitation: </div>
<div class="obit_view_cnt">{$data.visitation_date|default:"&nbsp;"}</div>
<!-- <div class="obit_view_title">Visitation Place: </div>
<div class="obit_view_cnt">{$data.visitation_place|default:"&nbsp;"}</div> -->
{if $data.final_disposition}
<div class="obit_view_title">{if $smarty.session.domain_id neq 57}Interment{else}Final Disposition{/if}: </div>
<div class="obit_view_cnt">{$data.final_disposition}</div>
{/if}
</div>