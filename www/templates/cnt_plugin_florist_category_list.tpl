<div class="florist-category-list">
    {foreach from=$categories item=category}
        <div class="florist-category">
            <div class="thumbnail">
                <a title="View Flowers for {$category.title}" href="/{$flowers_page}/{$category.link}">
                    <img alt="View Flowers for {$category.title}" src="{$category.img}">
                </a>
            </div>
            <div class="name">{$category.title}</div>
        </div>
    {/foreach}
</div>