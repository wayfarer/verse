{php}
	$this->assign("local_admin", $GLOBALS["user"]->data["domain_id"]);
	$this->assign("roles", @array_flip($GLOBALS["user"]->data["roles"]));
{/php}
<html>
<head>
  <title>Twin Tiers Technologies CMS</title>
  <meta http-equiv="Content-Type" content="text/html; charset=utf8">
{if $smarty.session.domain_id == 0}
  <link rel="stylesheet" href="adm_versesms.css" type="text/css">
{else}
  <link rel="stylesheet" href="adm_versesms_new.css" type="text/css">
{/if}
  <link rel="stylesheet" href="tabber.css" type="text/css">
  <link rel="stylesheet" href="js_color_picker_v2.css" type="text/css">

  <link href="css/default.css" rel="stylesheet" type="text/css">
  <link href="css/alphacube.css" rel="stylesheet" type="text/css">

  <style type="text/css">@import url(calendar.css);</style>
  <script type="text/javascript" src="js/calendar.js"></script>
  <script type="text/javascript" src="js/calendar-en.js"></script>
  <script type="text/javascript" src="js/calendar-setup.js"></script>

  <script type="text/javascript" src="js/prototype.js"></script>
  <script type="text/javascript" src="js/scriptaculous.js"></script>
  <script type="text/javascript" src="js/jslog.js"></script>
  <script type="text/javascript" src="ckeditor/ckeditor.js"></script>
  <script type="text/javascript" src="ckfinder/ckfinder.js"></script>
  <script type="text/javascript" src="js/color_functions.js"></script>
  <script type="text/javascript" src="js/js_color_picker_v2.js"></script>
  <script type="text/javascript" src="js/util.js"></script>
  <script type="text/javascript" src="js/swfobject.js"></script>
  <script type="text/javascript">
{literal}
	var tabberOptions = {
	  'manualStartup':true
	};
{/literal}
  </script>
  <script type="text/javascript" src="js/tabber.js"></script>
</head>

<body>
<div id="head">

</div>
<div id="menu">
{if $smarty.session.domain_id == 0}
	<div id="menu_in">
		<span style="float: left">
		{if isset($roles[2]) || !$local_admin}<a href="structure.php">Site Structure</a>{/if}
		{if isset($roles[5]) || !$local_admin}<a href="obituaries.php">Obituaries</a>{/if}
		{if isset($roles[5]) || !$local_admin}<a href="events.php">Events</a>{/if}
		{if isset($roles[6]) || !$local_admin}<a href="products.php">
		{if !$ecommerce}Products{else}E-Commerce{/if}
		</a>{/if}
		{if isset($roles[7])}<a href="sublists.php">Products selection</a>{/if}
		{if isset($roles[2]) || !$local_admin}<a href="lists.php">Lists</a>{/if}
		{if (isset($roles[2]) || !$local_admin) && $smarty.session.domain_id eq 53}<a href="funeral_directors.php">Funeral Directors</a>{/if}
  	{if isset($roles[9]) || !$local_admin}<a href="movieclips.php" class="endofgroup">Movie Clips</a>{/if}
		<span class="endofgroup"></span>
		{if isset($roles[1]) || !$local_admin}<a href="user_manager.php">Users</a>{/if}
		{if isset($roles[11]) || !$local_admin}<a href="members.php" class="endofgroup">Members</a>{/if}
		{if !$local_admin}
		<span class="endofgroup"></span><a href="domains.php">Domains</a>
        <span class="endofgroup"></span><a href="/backend.php/domain_group">Domain Groups</a>
		{/if}
		{if isset($roles[12]) || !$local_admin}<a href="emails.php">Email management</a>{/if}
		{if isset($roles[8]) || !$local_admin}<a href="statistics.php" class="endofgroup">Statistics</a>{/if}
		<span class="endofgroup"></span>
		{if (isset($roles[10]) || !$local_admin) && ($smarty.session.domain_id == 1 || $smarty.session.domain_id == 4)}<a href="webcasting.php">Webcasting</a>{/if}
		</span>
		<span style="float: right"><a href="logout.php">Logout</a></span>
	</div>
{else}
  <div id="menu_in">
    <span style="float: left">
    {if isset($roles[2]) || !$local_admin}<a href="structure.php" class="{if $smarty.server.SCRIPT_NAME=='/structure.php'}current{/if}">Site Structure</a>{/if}
    {if isset($roles[5]) || !$local_admin}<a href="backend.php/obituaries">Obituaries</a>{/if}
    {if isset($roles[5]) || !$local_admin}<a href="backend.php/events">Events</a>{/if}
    {if isset($roles[6]) || !$local_admin}<a href="backend.php/products">
    {if !$ecommerce}Products{else}Ecommerce{/if}
    </a>{/if}
    {if isset($roles[7])}<a href="sublists.php">Products selection</a>{/if}
    {if isset($roles[2]) || !$local_admin}<a href="backend.php/lists">Lists</a>{/if}
    {if (isset($roles[2]) || !$local_admin) && $smarty.session.domain_id eq 53}<a href="funeral_directors.php">Funeral Directors</a>{/if}
    {if isset($roles[9]) || !$local_admin}<a href="backend.php/movieclips" class="endofgroup">Movie Clips</a>{/if}
    <span class="endofgroup"></span>
    {if isset($roles[1]) || !$local_admin}<a href="backend.php/sf_guard_user">Users</a>{/if}
    {if isset($roles[11]) || !$local_admin}<a href="backend.php/members" class="endofgroup">Members</a>{/if}
    <span class="endofgroup"></span>
    {if isset($roles[8]) || !$local_admin}<a href="backend.php/statistics" class="endofgroup{if $smarty.server.SCRIPT_NAME=='/statistics.php'} current{/if}">Statistics</a>{/if}
    {if !$local_admin}
    <span class="endofgroup"></span><a href="backend.php/domains" class="{if $smarty.server.SCRIPT_NAME=='/domains.php'}current{/if}">Domains</a>
    <span class="endofgroup"></span><a href="/backend.php/domain_group">Domain Groups</a>
    {/if}
    </span>
    <span style="float: right"><a href="backend.php/logout">Logout</a></span>
  </div>
{/if}
</div>
<!--<div id="menu">
	<div id="menu_in">
		<a href="ml_issues.php">Issues</a><a href="ml_issue_edit.php">Last Issue</a><a href="ml_headers.php">Headers</a>
	</div>
</div> -->
<div id="content">
