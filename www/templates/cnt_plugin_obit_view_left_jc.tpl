{literal}
<style>
div.obit_view_title {
	font-weight: bold;
	float: left;
	padding-right: 4px;
}
a.candle {
	color: #D0822E;
}
div.obit_title {
	margin-top: 15px;
	margin-bottom: 10px;
	font: bold 15px Tahoma;
	text-align: center;
}
div.funeral_info {
	margin-bottom: 20px;
}
</style>
<style media="print">
div.noprint {
	display: none;
}
</style>
{/literal}
{if $data.image}
<div class="obit_view_cnt" style="text-align: center"><img src="{$data.image}"></div>
{/if}
<div style="text-align: center; font-weight: bold">{$data.first_name} {$data.middle_name} {$data.last_name}</div>
{if !$smarty.get.print}
<div style="text-align: left; margin-top: 10px">
<script>
{literal}
function openwin(url, width, height) {
  var width = width || 500;
  var height = height || 500;
  var left = (screen.width-width)/2;
  var top = (screen.height-height)/2;
  win=window.open(url, null, config="scrollbars=no,resizable=no,toolbar=no,location=no,menubar=no,width="+width+",height="+height+",top="+top+",left="+left+"");
  win.focus();
}
{/literal}
</script>
{if $scrapbookhere}
<a href="#" title="View Photoalbum" onclick="openwin('?p={$scrapbookpage}&id={$config.obituary_id}'); return false;"><img src="img/album.gif" alt="View Photoalbum" hspace="5">View photo tribute</a><br>
{/if}
{include file="cnt_plugin_obit_movieclips.tpl"}
<a href="?p=obituary_view&id={$data.obituary_id}&print=1" title="Print Obituary"><img src="img/print.gif" hspace="5" style="vertical-align:middle">Print web obituary</a><br>
{if $config.candles_policy neq "off" && $data.candles}
<a href="?p=obituary_view&id={$data.obituary_id}&candle=all" title="View All Candles"><img src="img/candlesall.gif" style="vertical-align:middle">View all candles</a>
{/if}
{if $config.flowers_page_id}
<a href="?p={$config.flowers_page}" title="Send Flowers"><img src="img/sendflowers.gif" style="vertical-align:middle">Send Flowers</a>
{/if}
</div>
{/if}
<div class="obit_title">Funeral Information</div>
<div class="funeral_info">
<div class="obit_view_title">Visitation: </div>
<div class="obit_view_cnt">{$data.visitation_date|default:"&nbsp;"}</div>
<div class="obit_view_title">Service Information: </div>
<div class="obit_view_cnt">{$data.service_date|default:"&nbsp;"} {$data.service_place|default:"&nbsp;"}</div>
<div class="obit_view_title">Interment: </div>
<div class="obit_view_cnt">{$data.final_disposition|default:"&nbsp;"}</div>
</div>

<br>
{include file="cnt_plugin_obit_view_candles.tpl"}
{if $config.quick_edit_enabled && !$smarty.get.print}
<br>
<form method="post">
	<input type="password" name="return" size="2"><input type="submit" value="">
</form>
{if $smarty.session.obit_edit}
  <script type="text/javascript" src="js/jslog.js"></script>
  <script type="text/javascript" src="js/prototype.js"></script>
  <script type="text/javascript" src="js/scriptaculous.js"></script>
{include file="window.tpl"}
{include file="util.tpl"}
<style type="text/css">@import url(css/default.css);</style>
<style type="text/css">@import url(css/alphacube.css);</style>
{literal}
  <script>
	var ObituaryEditor = Class.create();
	ObituaryEditor.prototype = {
		initialize: function(obituary_id) {
			// create window
//			this.wnd = new WindowClass("Obituary", 690, 510, [this.onok.bind(this), this.oncancel.bind(this)]);
			this.obituary_id = obituary_id;
			// create form
//			this.form = Builder.node("FORM");
			// create dialog fields in form
			var f = '<form>\
					<label for="first_name">First/Middle/Last</label><input type="text" id="first_name" name="first_name" size="19"> <input type="text" id="middle_name" name="middle_name" size="7"> <input type="text" id="last_name" name="last_name" size="19"><br>\
					<label for="home_place">Home Place</label><input type="text" id="home_place" name="home_place" class="txt2"><br>\
					<label for="death_date">Death Date</label><input type="text" id="death_date" name="death_date" class="date"><xinput type="button" id="death_date_b" value=" .. "> <small>(yyyy-mm-dd)</small><br>\
					<label for="birth_date">Birth Date</label><input type="text" id="birth_date" name="birth_date" class="date"><xinput type="button" id="birth_date_b" value=" .. "> <small>(yyyy-mm-dd)</small><br>\
					<label for="birth_place">Birth Place</label><input type="text" id="birth_place" name="birth_place" class="txt2"><br>\
					<label for="service_date">Service Date</label><input type="text" id="service_date" name="service_date" class="datetime"><xinput type="button" id="service_date_b" value=" .. "> <small>(yyyy-mm-dd hh:mm am)</small><br>\
					<label for="service_place">Service Place</label><input type="text" id="service_place" name="service_place" class="txt2"> <input type="checkbox" name="add_calendar_sp" id="add_calendar_sp"><label for="add_calendar_sp" class="in">add to calendar</label><br>\
					<label for="visitation_date">Visitation Date</label><input type="text" id="visitation_date" name="visitation_date" class="txt2"><br>\
					<label for="visitation_place">Visitation Place</label><input type="text" id="visitation_place" name="visitation_place" class="txt2"> <input type="checkbox" name="add_calendar_vp" id="add_calendar_vp"><label for="add_calendar_vp" class="in">add to calendar</label><br>\
					<label for="final_disposition">Final Disposition</label><input type="text" id="final_disposition" name="final_disposition" class="txt2"><br>\
					<label for="image">Image</label><input type="text" id="image" name="image" class="file"> <input type="button" name="browse_i" value="browse"><br>\
					<label for="obit_text">Obituary</label><textarea name="obit_text" id="obit_text" cols="62" rows="11"></textarea><br>\
					</form>\
				';
			this.wnd = Dialog.confirm(f, {windowParameters: {className:"alphacube", width:690, title:"Obituary", showEffect: Element.show, hideEffect: Element.hide},
									      ok: this.onok.bind(this) });
			this.form = this.wnd.getContent().getElementsByTagName("FORM")[0];
			this.sfb = new ServerFileBrowser(this.onimageselect.bind(this));
			this.form.elements["browse_i"].onclick=this.sfb.open.bind(this.sfb);

/*			Calendar.setup( {
				inputField : "death_date",
				ifFormat : "%Y-%m-%d",
				button : "death_date_b",
				step: 1
			} );
			Calendar.setup( {
				inputField : "birth_date",
				ifFormat : "%Y-%m-%d",
				button : "birth_date_b",
				step: 5
			} );
			Calendar.setup( {
				inputField : "service_date",
				ifFormat : "%Y-%m-%d %I:%M %p",
				button : "service_date_b",
				showsTime: true,
				timeFormat: 12,
				step: 1
			} ); */

			Form.focusFirstElement(this.form);
			if(obituary_id) {
				// load data
				new Ajax.Request('ajax_obituaries.php', {parameters:'action=load&id='+obituary_id, onSuccess:this.onload.bind(this)}); // TODO: handle AJAX errors here
			}
			else {
				this.wnd.hideloader();
			}
		},

		onimageselect: function(url) {
			this.form.elements["image"].value = url;
		},

		onload: function(t, json) {
			jslog.debug(t.getResponseHeader("X-JSON"));
			var form = this.form;
			$H(json).each(function(elem) {
					if(form.elements[elem.key].type=="text" || form.elements[elem.key].type=="textarea") {
						form.elements[elem.key].value = elem.value;
					}
					else { // treat as checkbox
						form.elements[elem.key].checked = parseInt(elem.value);
					}
				}
			);
			this.wnd.hideloader();
		},

		onsave: function(t) {
			jslog.debug(t.responseText);
			this.wnd.hide();
	    	// force browser to reload page
	    	document.location = document.location;
		},
		
		onok: function() {
			this.wnd.showloader();
			new Ajax.Request('ajax_obituaries.php', {parameters:'action=save&id='+this.obituary_id+"&"+Form.serialize(this.form), onSuccess:this.onsave.bind(this)}); // TODO: handle AJAX errors here
		}
	}
{/literal}
	function handle_onload() {ldelim}
		var obit = new ObituaryEditor({$config.obituary_id});
	}
	window.onload = handle_onload;
  </script>
{/if}
{if $smarty.session.remove_candle}
  <script type="text/javascript" src="js/prototype.js"></script>
  <script>
    function on_candle_removed(t) {ldelim}
    	// force browser to reload page
//    	alert(t.responseText);
    	document.location = document.location;
    }

	function handle_onload() {ldelim}
		if(confirm("Are you sure you want to delete this candle?")) {ldelim}
			// remove candle
			new Ajax.Request('ajax_plg_obituary.php', {ldelim}parameters:'action=delete_candle&obituary_id={$config.obituary_id}&candle_id={$smarty.session.remove_candle}', onSuccess:on_candle_removed}); // TODO: handle AJAX errors here
		}
	}
	window.onload = handle_onload;
  </script>	
{/if}
{/if}
