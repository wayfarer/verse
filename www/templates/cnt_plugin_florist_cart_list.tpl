<table width="100%">
    <tr>
        <th></th>
        <th>product</th>
        <th>price</th>
        <th>quantity</th>
        <th>amount</th>
    </tr>
    {foreach from=$products item=product}
        <tr align="center">
            <td>
                <img src="{$product.image}">
            </td>
            <td>
                {$product.name}
            </td>
            <td>
                ${$product.price|number_format:2}
            </td>
            <td>
                {$product.count}
            </td>
            <td>
                <b>${$product.amount|number_format:2}</b>
            </td>
            <td width="20%">
                <input type="text" class="cart-count" id="cart-count-{$product.code}" name="cart_count[]" title="Item(s) to add/remove from cart" value="1"><br/>
                <a href="#" onclick="add_item('{$product.code}'); return false;">update cart</a><br/>
                <a href="#" onclick="remove_item('{$product.code}'); return false;">remove item(s)</a>
            </td>
        </tr>
    {/foreach}
    <tr>
        <td></td>
        <td></td>
        <td></td>
        <td align="right">
            <b>Total:</b>
        </td>
        <td align="center">
            <b>${$total|number_format:2}</b>
        </td>
    </tr>
    <tr>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td align="center">
            <input type="button" value="checkout" onclick="document.location='?p=flowercheckout'">
        </td>
    </tr>
</table>