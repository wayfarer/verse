<script type="text/javascript" src="/js/prototype.js"></script>
<script type="text/javascript" src="/js/cart.js"></script>
<h1>{$product.name}</h1>
{if $product.image}
<img src="{$product.image|leading_slash}"><br>
{/if}
{$product.manufacturer}<br>
{if $price}
<b>${$product.price|number_format:2}</b><br>
{/if}
{$product.description}
<br><br>
{if $ecommerce}
    <input type="text" class="cart-count-product" id="cart-count-{$product.product_id}" name="cart_count[]" title="Item(s) to add/remove from cart" value="1"><br>
	<a href="#" onclick="cart({$product.product_id});return false">add to cart</a><br>
	<small id="p{$product.product_id}" count="{$product.count}">
	{if $product.count}
		{$product.count} item(s) in cart
		</small><br>
		<div id="r{$product.product_id}">
	{else}
		</small><br>
		<div style="display: none" id="r{$product.product_id}">
	{/if}
	<a href="#" onclick="cart_remove({$product.product_id});return false;">remove</a><br>
	<a href="/cart">review cart</a>
	</div>
{/if}
<br><br>
<a href="javascript: history.go(-1)">&laquo; back</a>
