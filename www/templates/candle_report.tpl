{include file="adm_header.tpl" menu="1"}

<h1>Candle reports</h1>

<div>
  <form id="filters">
    <label for="domain_id">Domain:</label><select id="domain_id" name="domain_id">{html_options options=$domains}</select><br>
    <label for="begin_date">Begin date:</label><input type="text" id="begin_date" name="begin_date" size="8" value="{$begin_date}"> <input type="button" id="begin_date_b" value=" .. ">
    <label for="end_date">End date:</label><input type="text" id="end_date" name="end_date" size="8" value="{$end_date}"> <input type="button" id="end_date_b" value=" .. ">
    <input type="button" value="Filter!" onclick="update_chart()">
  </form>
</div>

<div id="report">
  {$ofc_object}
  {$ofc_object2}
  {$ofc_object3}
</div>

{literal}
<script>	
<!--  
function update_chart() {
  var chart = $("chart");
  if(!chart) chart = $("ie_chart");
  var chart2 = $("chart_2");
  if(!chart2) chart2 = $("ie_chart_2");
  var chart3 = $("chart_3");
  if(!chart3) chart3 = $("ie_chart_3");

  // to load from a specific URL:
  // you may need to 'escape' (URL escape, i.e. percent escape) your URL if it has & in it
  chart.reload("candle_report.php?action=cpd&"+Form.serialize($("filters")));
  chart2.reload("candle_report.php?action=cbw&"+Form.serialize($("filters")));
  chart3.reload("candle_report.php?action=cbh&"+Form.serialize($("filters")));
}

Calendar.setup( {
	inputField : "begin_date",
	ifFormat : "%Y-%m-%d",
	button : "begin_date_b",
	step: 1
} );
Calendar.setup( {
	inputField : "end_date",
	ifFormat : "%Y-%m-%d",
	button : "end_date_b",
	step: 1
} );
-->
</script>	
{/literal}

{include file="adm_footer.tpl"}
