{include file="adm_header.tpl" menu="1"}
{include file="window.tpl"}
{include file="contextmenu.tpl"}
{include file="util.tpl"}

<h1>User rights management</h1>
<div class="actions"><a href="#" onclick="new UserEditor(0); return false;">new user</a></div>
<div id="usersdiv"></div>
{literal}
<style>
	input.txt {
		width: 200px;
	}
	label {
		width: 140px !important;
	}
</style>
{/literal}
<script type="text/javascript">
{include file="populate_controls.tpl"}
{literal}
	var UserEditor = Class.create();
	UserEditor.prototype = {
		initialize: function(user_id) {
			// create window
			this.user_id = user_id;
{/literal}
{capture name=domains_opts}
	{html_options options=$domain_list}
{/capture}
			var f = '<form>\
					<label for="login">Login</label><input type="text" id="login" name="ulogin" class="txt"><br>\
					<label for="password">Password</label><input type="password" id="password" name="password" class="txt"><br>\
					<label for="name">Name</label><input type="text" id="name" name="name" class="txt"><br>\
					<label for="enabled">Enabled</label><input type="checkbox" id="enabled" name="enabled"><br>\
					User roles:<br>\
					<label for="users">User management</label><input type="checkbox" id="users" name="users"><br>\
					<label for="cnt">Content management</label><input type="checkbox" id="cnt" name="content"><br>\
					<label for="obits">Obits management</label><input type="checkbox" id="obits" name="obits"><br>\
					<label for="products">Products management</label><input type="checkbox" id="products" name="products"><br>\
{if $smarty.session.domain_id eq 98}
					<label for="sublists">Products sublists</label><input type="checkbox" id="sublists" name="sublists"><br>\
{/if}
					<label for="phpmvstats">Website statistics</label><input type="checkbox" id="phpmvstats" name="phpmvstats"><br>\
  				<label for="movieclips">Movie Clips</label><input type="checkbox" id="movieclips" name="movieclips"><br>\
  				<label for="members">Members management</label><input type="checkbox" id="members" name="members"><br>\
  				<label for="email">Email management</label><input type="checkbox" id="email" name="email"><br>\
					<label for="ftp">FTP access</label><input type="checkbox" id="ftp" name="ftp"> set pass:<input type="password" name="ftp_pass" class="pass"><br>\
					<br><small>Note: login for FTP: <b><span class="username"></span>-{$postfix}</b></small><br>\
					<input type="hidden" name="users_old">\
					<input type="hidden" name="content_old">\
					<input type="hidden" name="obits_old">\
					<input type="hidden" name="products_old">\
					<input type="hidden" name="ftp_old">\
{if $smarty.session.domain_id eq 98}
					<input type="hidden" name="sublists_old">\
{/if}
					<input type="hidden" name="phpmvstats_old">\
  				<input type="hidden" name="movieclips_old">\
  				<input type="hidden" name="members_old">\
					</form>\
				';
{literal}
			this.wnd = Dialog.confirm(f, {windowParameters: {className:"alphacube", width:500, title:"User", showEffect: Element.show, hideEffect: Element.hide},
									      ok: this.onok.bind(this) });
			this.form = this.wnd.getContent().getElementsByTagName("FORM")[0];
			this.form.onsubmit = this.onsubmit.bind(this);
			this.form["login"].onblur = this.updateinfo.bind(this);
			Form.focusFirstElement(this.form);
			if(user_id) {
				// load data
				new Ajax.Request('ajax_users.php', {parameters:'action=load&id='+user_id, onSuccess:this.onload.bind(this)}); // TODO: handle AJAX errors here
			}
			else {
				this.wnd.hideloader();
			}
		},

		onload: function(t, json) {
{/literal}
			jslog.debug(t.getResponseHeader("X-JSON"));
			populate_controls(this.form, json);
			this.updateinfo();
			this.form["users_old"].value = this.form["users"].checked?1:0;
			this.form["content_old"].value = this.form["content"].checked?1:0;
			this.form["obits_old"].value = this.form["obits"].checked?1:0;
			this.form["products_old"].value = this.form["products"].checked?1:0;
			this.form["ftp_old"].value = this.form["ftp"].checked?1:0;
//			this.form["mail_old"].value = this.form["mail"].checked?1:0;
{if $smarty.session.domain_id eq 98}
			this.form["sublists_old"].value = this.form["sublists"].checked?1:0;
{/if}			
			this.form["phpmvstats_old"].value = this.form["phpmvstats"].checked?1:0;
  		this.form["movieclips_old"].value = this.form["movieclips"].checked?1:0;
  		this.form["members_old"].value = this.form["members"].checked?1:0;
			this.wnd.hideloader();
{literal}
		},

		onsubmit: function() {
			this.onok();
			return false;
		},

		onsave: function(t) {
			jslog.debug(t.responseText);
			update_users();
			this.wnd.hide();
		},
		
		onok: function() {
			this.wnd.showloader();
			new Ajax.Request('ajax_users.php', {parameters:'action=save&id='+this.user_id+"&"+Form.serialize(this.form), onSuccess:this.onsave.bind(this)}); // TODO: handle AJAX errors here
		},

		updateinfo: function() {
			var login = this.form["login"].value;
//			var name_elems = document.getElementsByClassName("username", this.form);
			var name_elems = $$(".username");
			name_elems[0].innerHTML=login;
//			name_elems[1].innerHTML=login;
//			name_elems[2].innerHTML=login;
		}
	}

	function user_deleted(t)
	{
		update_users();
	}
	
	function delete_user(user_id)
	{
		if(confirm("Are you sure you want to delete user?")) {
			new Ajax.Request('ajax_users.php', {parameters:'action=delete&id='+user_id, onSuccess:user_deleted}); // TODO: handle AJAX errors here
		}
	}
	
	function page(start)
	{
		new Ajax.Updater('usersdiv', 'ajax_users.php', {parameters: "action=list&start="+start});  // add AJAX error handling here
	}
	function order(field)
	{
		new Ajax.Updater('usersdiv', 'ajax_users.php', {parameters: "action=list&order="+field});  // add AJAX error handling here
	}
	function update_users()
	{
		new Ajax.Updater('usersdiv', 'ajax_users.php', {parameters: "action=list"});  // add AJAX error handling here
	}	
	update_users();
</script>	
{/literal}

{include file="adm_footer.tpl"}
