{include file="adm_header.tpl" menu="1"}
{include file="window.tpl"}

<script type="text/javascript">
{include file="populate_controls.tpl"}
{literal}
	var DomainEditor = Class.create();
	DomainEditor.prototype = {
		initialize: function(domain_id) {
			// create window
//			this.wnd = new WindowClass("Domain properties", 300, 200, [this.onok.bind(this), this.oncancel.bind(this)]);
			this.domain_id = domain_id;
			// create form
//			this.form = Builder.node("FORM");
			// create dialog fields in form
{/literal}
{capture name=domains}
	{html_options options=$domains}
{/capture}
			var f = '<form>\
					  <label for="domain_name">Domain name</label><input type="text" id="domain_name" name="domain_name" class="txt"><br>\
			          <label>Domain type</label><input type="radio" name="type" value="0" id="normal" class="radio" checked="checked"><label class="inline" for="normal">normal</label><br>\
			          <label>&nbsp;</label><input type="radio" name="type" value="1" id="alias" class="radio"><label class="inline" for="alias">alias</label><br><br>\
					  <div id="normal_controls">\
					  <label for="postfix">Postfix <small>(3 letters)</small></label><input type="text" id="postfix" name="postfix" class="txt"><br>\
					  <label for="email">Default e-mail</label><input type="text" id="email" name="email" class="txt"><br>\
					  <label for="enabled">Enabled</label><input type="checkbox" id="enabled" name="enabled" class="chk"><br>\
					  <label for="email_enabled">Email enabled</label><input type="checkbox" id="email_enabled" name="email_enabled" class="chk"><br>\
					  <label for="ecommerce_enabled">Ecommerce</label><input type="checkbox" id="ecommerce_enabled" name="ecommerce_enabled" class="chk">\
					  <label for="paypal_checkout">PayPal checkout</label><input type="checkbox" id="paypal_checkout" name="paypal_checkout" class="chk"><br>\
					  </div>\
					  <div id="alias_controls" style="display:none">\
					  <label for="alias_domain_id">Alias For</label><select id="alias_domain_id" name="alias_domain_id">{$smarty.capture.domains|strip}</select><br>\
					  </div>\
					  </form>\
				';
{literal}
			this.wnd = Dialog.confirm(f, {windowParameters: {className:"alphacube", width:450, title:"Domain Properties", showEffect: Element.show, hideEffect: Element.hide},
									      ok: this.onok.bind(this) });
			this.form = this.wnd.getContent().getElementsByTagName("FORM")[0];
			this.form.elements["type"][0].onclick = this.switch_type.bind(this);
			this.form.elements["type"][1].onclick = this.switch_type.bind(this);
			Form.focusFirstElement(this.form);
			if(domain_id) {
				// load data
				new Ajax.Request('domains.php', {parameters:'action=load&id='+domain_id, onSuccess:this.onload.bind(this)}); // TODO: handle AJAX errors here
//				this.form.elements["domain_name"].disabled = true;
			}
			else {
				this.wnd.hideloader();
			}
		},

		onload: function(t, json) {
			jslog.debug(t.getResponseHeader("X-JSON"));
			var form = this.form;
			populate_controls(this.form, json);
			this.switch_type();
			this.wnd.hideloader();
		},

		onsave: function(t) {
			jslog.debug(t.responseText);
			update_domain_list();
			this.wnd.hide();
		},
		
		onok: function() {
			this.wnd.showloader();
			new Ajax.Request('domains.php', {parameters:'action=save&id='+this.domain_id+"&"+Form.serialize(this.form), onSuccess:this.onsave.bind(this)}); // TODO: handle AJAX errors here
		},

		switch_type: function() {
			var normal = $("normal_controls"); // TODO: replace this with search inside form on init step
			var alias = $("alias_controls");
			if(this.form.elements["type"][0].checked) {
				normal.style.display = "block";
				alias.style.display = "none";
			}
			else {
				normal.style.display = "none";
				alias.style.display = "block";
			}
		}
	}

	function domain_deleted(t)
	{
		update_domain_list();
	}

	function delete_domain(domain_id)
	{
		if(confirm("Are you sure you want to PERMAMENTLY DELETE the domain?\nAll information WILL BE LOST!")) {
			new Ajax.Request('domains.php', {parameters:'action=delete&id='+domain_id, onSuccess:domain_deleted}); // TODO: handle AJAX errors here
		}
	}
	function hide_domain(domain_id)
	{
		if(confirm("Are you sure you want to remove domain?")) {
			new Ajax.Request('domains.php', {parameters:'action=hide&id='+domain_id, onSuccess:domain_deleted}); // TODO: handle AJAX errors here
		}
	}
	function unhide_domain(domain_id)
	{
		new Ajax.Request('domains.php', {parameters:'action=unhide&id='+domain_id, onSuccess:domain_deleted}); // TODO: handle AJAX errors here
	}
	function to_production_domain(domain_id)
	{
		new Ajax.Request('domains.php', {parameters:'action=toproduction&id='+domain_id, onSuccess:domain_deleted}); // TODO: handle AJAX errors here
	}
	function to_uc_domain(domain_id)
	{
		if(confirm("Are you sure you want to put domain to under construction mode?")) {
			new Ajax.Request('domains.php', {parameters:'action=touc&id='+domain_id, onSuccess:domain_deleted}); // TODO: handle AJAX errors here
		}
	}

{/literal}

	function update_domain_list()
	{ldelim}
		new Ajax.Updater('domain_list', 'domains.php', {ldelim}parameters: "action={$action}"});  // add AJAX error handling here
	}	

	function page(start)
	{ldelim}
		new Ajax.Updater('domain_list', 'domains.php', {ldelim}parameters: "action={$action}&start="+start});  // add AJAX error handling here
	}
	
	function order(field)
	{ldelim}
		new Ajax.Updater('domain_list', 'domains.php', {ldelim}parameters: "action={$action}&order="+field});  // add AJAX error handling here
	}
</script>	

<h1>Domain management : {if $action eq "list"}Production{elseif $action eq "list_uc"}Under construction{else}Removed{/if}</h1>
<div class="submenu"><a href="mx.php">MX records list</a> | <a href="candles_report_sites.php">Candle reports</a></div>
<div class="actions"><a href="#" onclick="new DomainEditor(0); return false;">create domain</a> | <a href="domains.php">production domains</a> | <a href="domains.php?mode=1">under construction domains</a> | <a href="domains.php?mode=2">removed domains</a></div>
<div id="domain_list"><img src="img/loader-big.gif"></div>
{literal}
<script type="text/javascript">
	update_domain_list();
</script>	
{/literal}
<br>
<small>* Note: Space consumed by domains are automatically calculated once per week on Mondays. If you want actual information right now, please click the following link: <a href="calc_spaces.php?all=1">Calculate domains spaces now</a><br>
* Last calculated: {$sites_space_consumed_last_calculated}</small>
<br><br>
{include file="adm_footer.tpl"}
