{include file="adm_header.tpl" menu="1"}
{include file="window.tpl"}
{include file="contextmenu.tpl"}
{include file="util.tpl"}

<h1>Obituaries management</h1>
<div class="submenu"><a href="candles_moderation.php">candles moderation queue</a></div>
<div class="actions"><a href="#" onclick="new ObituaryEditor(0); return false;">new obituary</a> / <a href="#" onclick="new ObituariesConfig(); return false;">obituaries config</a> / <a href="#" onclick="new UpdateSubscription(); return false;">new obituary notification</a></div>
<div id="obituaries"><img src="img/loader-big.gif"></div>
<script type="text/javascript" src="js/tiny_mce/tiny_mce.js"></script>
<script type="text/javascript">
{include file="populate_controls.tpl"}
{literal}
	var ObituaryEditor = Class.create();
	ObituaryEditor.prototype = {
		initialize: function(obituary_id) {
			// create window
//			this.wnd = new WindowClass("Obituary", 690, 510, [this.onok.bind(this), this.oncancel.bind(this)]);
			this.obituary_id = obituary_id;
			// create form
//			this.form = Builder.node("FORM");
			// create dialog fields in form
{/literal}
{capture name=service_types}
  {html_options options=$service_types}
{/capture}
			var f = '<form>\
  				<label for="obituary_case_id">Case ID</label><input type="text" id="obituary_case_id" name="obituary_case_id" class="txt"><br>\
					<label for="first_name">First/Middle/Last</label><input type="text" id="first_name" name="first_name" size="19"> <input type="text" id="middle_name" name="middle_name" size="7"> <input type="text" id="last_name" name="last_name" size="19"><br>\
 					<label for="home_place">Home Place</label><input type="text" id="home_place" name="home_place" class="txt2"><br>\
					<label for="death_date">Death Date</label><input type="text" id="death_date" name="death_date" class="date"><xinput type="button" id="death_date_b" value=" .. "> <small>(yyyy-mm-dd)</small><br>\
					<label for="birth_date">Birth Date</label><input type="text" id="birth_date" name="birth_date" class="date"><xinput type="button" id="birth_date_b" value=" .. "> <small>(yyyy-mm-dd)</small><br>\
					<label for="birth_place">Birth Place</label><input type="text" id="birth_place" name="birth_place" class="txt2"><br>\
					<label for="service_type">Service Type</label><select id="service_type" name="service_type">{$smarty.capture.service_types|strip}</select><br>\
					<label for="service_date">Service Date</label><input type="text" id="service_date" name="service_date" class="datetime"><xinput type="button" id="service_date_b" value=" .. "> <small>(yyyy-mm-dd)</small><br>\
					<label for="service_time">Service Time</label><input type="text" id="service_time" name="service_time" class="time" maxlength="128"> <small>(freeform)</small><br>\
					<label for="service_place">Service Place</label><input type="text" id="service_place" name="service_place" class="txt2"> <input type="checkbox" name="add_calendar_sp" id="add_calendar_sp"><label for="add_calendar_sp" class="in">add to calendar</label><br>\
					<label for="visitation_date">Visitation Date</label><input type="text" id="visitation_date" name="visitation_date" class="txt2"><br>\
					<label for="visitation_place">Visitation Place</label><input type="text" id="visitation_place" name="visitation_place" class="txt2"> <input type="checkbox" name="add_calendar_vp" id="add_calendar_vp"><label for="add_calendar_vp" class="in">add to calendar</label><br>\
					<label for="final_disposition">Final Disposition</label><input type="text" id="final_disposition" name="final_disposition" class="txt2"><br>\
					<label for="image">Image</label><input type="text" id="image" name="image" class="file"> <input type="button" name="browse_i" value="browse"><br>\
					<label for="obit_text">Obituary</label><div class="wysiwyg"><textarea name="obit_text" id="obit_text" style="width: 540px; height:180px"></textarea></div><br>\
					<label>Candles policy</label><input type="radio" id="on" name="candles_policy" value="on"><label class="inline" for="on">on</label><input type="radio" id="moderated" name="candles_policy" value="moderated"><label class="inline" for="moderated">moderated</label><input type="radio" id="off" name="candles_policy" value="off"><label class="inline" for="off">off</label><input type="radio" id="default" name="candles_policy" value=""><label class="inline" for="default">default</label><br>\
					</form>\
				';
{literal}        
			this.wnd = Dialog.confirm(f, {windowParameters: {className:"alphacube", width:690, title:"Obituary", showEffect: Element.show, hideEffect: Element.hide},
									      ok: this.onok.bind(this) });
			this.form = this.wnd.getContent().getElementsByTagName("FORM")[0];
			this.sfb = new ServerFileBrowser(this.onimageselect.bind(this), "Images:/obituaries/", "Images");
			this.form.elements["browse_i"].onclick=this.sfb.open.bind(this.sfb);

//      var myNicEditor = new nicEditor({buttonList:['bold','italic','underline','strikethrough','subscript','superscript','ol','ul','hr','link','unlink','fontFormat','xhtml'], iconsPath: "/img/nicEditorIcons.gif", maxHeight: 155, height: 165}).panelInstance('obit_text');

    	tinyMCE.init({
    		mode : "exact",
        elements : "obit_text",
    		theme : "advanced",
        theme_advanced_buttons1 : "bold,italic,underline,strikethrough,|,sub,sup,|,bullist,numlist,link,unlink,|,formatselect,|,undo,redo,|,code",
        theme_advanced_buttons2 : "",        
        theme_advanced_buttons3 : "",
        theme_advanced_toolbar_location : "top",
        theme_advanced_toolbar_align : "left",
        plugins: "paste,inlinepopups",
        paste_auto_cleanup_on_paste : true
    	});

/*			Calendar.setup( {
				inputField : "death_date",
				ifFormat : "%Y-%m-%d",
				button : "death_date_b",
				step: 1
			} );
			Calendar.setup( {
				inputField : "birth_date",
				ifFormat : "%Y-%m-%d",
				button : "birth_date_b",
				step: 5
			} );
			Calendar.setup( {
				inputField : "service_date",
				ifFormat : "%Y-%m-%d %I:%M %p",
				button : "service_date_b",
				showsTime: true,
				timeFormat: 12,
				step: 1
			} ); */

			Form.focusFirstElement(this.form);
			if(obituary_id) {
				// load data
				new Ajax.Request('obituaries.php', {parameters:'action=load&id='+obituary_id, onSuccess:this.onload.bind(this)}); // TODO: handle AJAX errors here
			}
			else {
				this.wnd.hideloader();
			}
		},

		onimageselect: function(url) {
			this.form.elements["image"].value = url;
		},

		onload: function(t, json) {
//			jslog.debug(t.getResponseHeader("X-JSON"));
//  		jslog.debug(json);
  		jslog.debug(t.responseText);
      json = eval(t.responseText);
			var form = this.form;
			populate_controls(this.form, json);
      tinyMCE.get('obit_text').load();
//      nicEditors.findEditor("obit_text").setContent(this.form.elements["obit_text"].value);
			this.wnd.hideloader();
		},

		onsave: function(t) {
			jslog.debug(t.responseText);
			update_obituaries();
			this.wnd.hide();
		},
		
		onok: function() {
			this.wnd.showloader();
//      nicEditors.findEditor("obit_text").saveContent();
      tinyMCE.get('obit_text').save();
			new Ajax.Request('obituaries.php', {parameters:'action=save&id='+this.obituary_id+"&"+Form.serialize(this.form), onSuccess:this.onsave.bind(this)}); // TODO: handle AJAX errors here
		}
	}

	var ScrapbookEditor = Class.create();
	ScrapbookEditor.prototype = {
		initialize: function(obituary_id) {
			// create window
//			this.wnd = new WindowClass("Scrapbook", 500, 420, [this.onok.bind(this), this.oncancel.bind(this)]);
			this.obituary_id = obituary_id;
			// create form
//			this.form = Builder.node("FORM");
			// create dialog fields in form
			var f = '<form>\
					<label for="images">Scrapbook Images</label><select size="15" class="scrap" name="images" id="images"></select><br>\
					<label for="description">Image Description</label><textarea name="description" id="description" cols="42" rows="5"></textarea><br>\
					<label>&nbsp;</label><input type="button" name="browse" value="add image">\
					<input type="button" name="delete" value="delete image(s)">\
					</form>\
				';
			this.wnd = Dialog.confirm(f, {windowParameters: {className:"alphacube", width:500, title:"Scrapbook", showEffect: Element.show, hideEffect: Element.hide},
									      ok: this.onok.bind(this) });
			this.form = this.wnd.getContent().getElementsByTagName("FORM")[0];
			this.sfb = new ServerFileBrowser(this.onimageselect.bind(this), "Images:/obituaries/scrap", "Images");
			this.form.elements["browse"].onclick=this.sfb.open.bind(this.sfb);
			this.form.elements["delete"].onclick=this.ondelete.bind(this);
			this.form.elements["images"].onmousedown=this.onbeforechange.bind(this);
			this.form.elements["images"].onchange=this.onchange.bind(this);
			this.currentIndex = this.form.elements["images"].selectedIndex;

			if(obituary_id) {
				// load data
				new Ajax.Request('obituaries.php', {parameters:'action=load_scrap&id='+obituary_id, onSuccess:this.onload.bind(this)}); // TODO: handle AJAX errors here
			}
			else {
				this.wnd.hideloader();
			}
		},

		onbeforechange: function() {
			if(this.currentIndex!=-1) {
				// store textarea text
			    this.form.elements["images"].options[this.currentIndex].setAttribute("description", this.form.elements["description"].value);
			}
		},

		onchange: function(p) {
			this.form.elements["description"].value = this.form.elements["images"].options[this.form.elements["images"].selectedIndex].getAttribute("description");
			this.currentIndex = this.form.elements["images"].selectedIndex;
//			alert(this.form.elements["images"].options[this.form.elements["images"].selectedIndex].getAttribute("description"));
		},

		onimageselect: function(url) {
			this.onbeforechange();			
			var opt = new Option(url);
			this.form.elements["images"].options.add(opt);
			this.form.elements["images"].selectedIndex = this.form.elements["images"].options.length-1;

			this.currentIndex = this.form.elements["images"].selectedIndex;			
			this.form.elements["images"].options[this.currentIndex].setAttribute("description", "");
			this.form.elements["description"].value="";
		},

		ondelete: function() {
			var i = this.form.elements["images"].selectedIndex;
			if(i!=-1) {
			    if(document.all) {
					this.form.elements["images"].options[i] = null;
				}
				else {
				    this.form.elements["images"].remove(i);
				}
				if(this.form.elements["images"].options.length) {
					if(!i)i=1;
					this.form.elements["images"].selectedIndex = i-1;
					this.onchange();
				}
				else {
					this.currentIndex = -1;
				}
			}
		},

		onload: function(t, json) {
			jslog.debug(t.getResponseHeader("X-JSON"));
			var form = this.form;
			$H(json).each(function(elem) {
					if(elem.key) {
						var opt = new Option(elem.key);
						opt.setAttribute("description", elem.value);
						form.elements["images"].options.add(opt);
					}
				}
			);
			if(form.elements["images"].options.length) {
				form.elements["images"].options.selectedIndex = 0;
				this.onchange();
			}
			this.wnd.hideloader();
		},

		onsave: function(t) {
			jslog.debug(t.responseText);
			this.wnd.hide();
		},
		
		onok: function() {
			this.wnd.showloader();
			this.onbeforechange();
			var i;
			var str = "";
			var elem = this.form.elements["images"];
			for(i=0; i<elem.options.length; i++) {
				if(str.length) str+="&";
				str+="img[]="+escape(elem.options[i].text)+"&desc[]="+escape(elem.options[i].getAttribute("description"));
			}
			new Ajax.Request('obituaries.php', {parameters:'action=save_scrap&id='+this.obituary_id+"&"+str, onSuccess:this.onsave.bind(this)}); // TODO: handle AJAX errors here
		}
	};

  var ObituaryMovieclips = Class.create();
	ObituaryMovieclips.prototype = {
		initialize: function(obituary_id) {
      this.obituary_id = obituary_id;
			var f = '<form>\
			         <label for="movieclip_ids">Movieclip ids:</label><input type="text" name="movieclip_ids" id="movieclip_ids" class="txt2"/><br>\
               <label>&nbsp;</label><small>Enter comma separated movieclip ids</small>\
					     </form>\
				';
			this.wnd = Dialog.confirm(f, {windowParameters: {className:"alphacube", width:540, title:"Obituary Movieclips", showEffect: Element.show, hideEffect: Element.hide},
									      ok: this.onok.bind(this) });
			this.form = this.wnd.getContent().getElementsByTagName("FORM")[0];
			this.form.onsubmit = this.onsubmit.bind(this);
			// load data
			new Ajax.Request('obituaries.php', {parameters:'action=load_movieclip_ids&id='+this.obituary_id, onSuccess:this.onload.bind(this)});
		},

		onload: function(t, json) {
			jslog.debug(t.getResponseHeader("X-JSON"));
			populate_controls(this.form, json);
			this.wnd.hideloader();
		},

		onsubmit: function() {
			this.onok();
			return false;
		},

		onsave: function(t) {
			jslog.debug(t.responseText);
			update_obituaries();
			this.wnd.hide();
		},
		
		onok: function() {
			this.wnd.showloader();
			new Ajax.Request('obituaries.php', {parameters:'action=save_movieclip_ids&id='+this.obituary_id+'&'+Form.serialize(this.form), onSuccess:this.onsave.bind(this)});
		}
	}

  var ObituarySubscriptions = Class.create();
	ObituarySubscriptions.prototype = {
		initialize: function(obituary_id) {
      this.obituary_id = obituary_id;
			var f = '<form>\
                  <label for="subscriptions">Subscriptions</label><select size="15" name="subscriptions" id="subscriptions" class="scrap"></select><br>\
                  <label>&nbsp;</label><input type="button" name="delete" value="delete subscription(s)"><br><br>\
                  <label>Add subscription:</label>\
    		                               <label for="name">Name:</label><input type="text" name="name" id="name" class="txt"/><br>\
    		          <label>&nbsp;</label><label for="email">E-mail:</label><input type="text" name="email" id="email" class="txt"/><br>\
          				<label>&nbsp;</label><label>&nbsp;</label><input type="button" name="add" value="add subscription">\
					     </form>\
				';
			this.wnd = Dialog.confirm(f, {windowParameters: {className:"alphacube", width:540, title:"Obituary Subscriptions", showEffect: Element.show, hideEffect: Element.hide},
									      ok: this.onok.bind(this) });
			this.form = this.wnd.getContent().getElementsByTagName("FORM")[0];
			this.form.onsubmit = this.onsubmit.bind(this);
  		this.form.elements["add"].onclick=this.onadd.bind(this);
  		this.form.elements["delete"].onclick=this.ondelete.bind(this);

      this.deleted = "";

			// load data
			new Ajax.Request('obituaries.php', {parameters:'action=load_subscriptions&id='+this.obituary_id, onSuccess:this.onload.bind(this)});
		},

		onload: function(t, json) {
			jslog.debug(t.getResponseHeader("X-JSON"));
      var self = this;
      $H(json).each(function(elem) {
    		var opt = new Option(elem.value.name+" / "+elem.value.email+" / "+elem.value.created);
        opt.setAttribute("name", elem.value.name);
        opt.setAttribute("email", elem.value.email);
  			self.form.elements["subscriptions"].options.add(opt);
      } );  		
			this.wnd.hideloader();
		},

		onsubmit: function() {
			this.onok();
			return false;
		},

		onsave: function(t) {
			jslog.debug(t.responseText);
			this.wnd.hide();
		},
		
		onok: function() {
			this.wnd.showloader();
  		var i;
			var str = "";
			var elem = this.form.elements["subscriptions"];
			for(i=0; i<elem.options.length; i++) {
        if(elem.options[i].getAttribute("isnew")) {
  			  // save only newly created entries (they have name attr)
        	if(str.length) str+="&";
  				str+="names[]="+escape(elem.options[i].getAttribute("name"))+"&emails[]="+escape(elem.options[i].getAttribute("email"));
        }
			}
      new Ajax.Request('obituaries.php', {parameters:'action=save_subscriptions&id='+this.obituary_id+'&r='+this.deleted+"&"+str, onSuccess:this.onsave.bind(this)});
		},

    onadd: function() {
      if(this.form.elements["name"].value && this.form.elements["email"].value) {
    		var opt = new Option(this.form.elements["name"].value+" / "+this.form.elements["email"].value);
        opt.setAttribute("name", this.form.elements["name"].value);
        opt.setAttribute("email", this.form.elements["email"].value);
        opt.setAttribute("isnew", 1);
  			this.form.elements["subscriptions"].options.add(opt);
  			this.form.elements["subscriptions"].selectedIndex = this.form.elements["subscriptions"].options.length-1;
        this.form.elements["name"].value = "";
        this.form.elements["email"].value = "";
      }
    },

    ondelete: function() {
  		var i = this.form.elements["subscriptions"].selectedIndex;

      if(!this.form.elements["subscriptions"].options[i].getAttribute("isnew")) {
        // remember deleted emails
        if(this.deleted) this.deleted+=",";
        this.deleted += this.form.elements["subscriptions"].options[i].getAttribute("email");
      }

			if(i!=-1) {
			    if(document.all) {
					this.form.elements["subscriptions"].options[i] = null;
				}
				else {
				    this.form.elements["subscriptions"].remove(i);
				}
				if(this.form.elements["subscriptions"].options.length) {
					if(!i)i=1;
					this.form.elements["subscriptions"].selectedIndex = i-1;
				}
				else {
					this.currentIndex = -1;
				}
			}
    }
	}

  var UpdateSubscription = Class.create();
	UpdateSubscription.prototype = {
		initialize: function() {
			var f = '<form>\
                  <label for="subscriptions">Subscriptions</label><select size="15" name="subscriptions" id="subscriptions" class="scrap"></select><br>\
                  <label>&nbsp;</label><input type="button" name="delete" value="delete subscription(s)"><br><br>\
                  <label>Add subscription:</label>\
    		                               <label for="name">Name:</label><input type="text" name="name" id="name" class="txt"/><br>\
    		          <label>&nbsp;</label><label for="email">E-mail:</label><input type="text" name="email" id="email" class="txt"/><br>\
          				<label>&nbsp;</label><label>&nbsp;</label><input type="button" name="add" value="add subscription">\
					     </form>\
				';
			this.wnd = Dialog.confirm(f, {windowParameters: {className:"alphacube", width:540, title:"New Obituary Notification Subscription", showEffect: Element.show, hideEffect: Element.hide},
									      ok: this.onok.bind(this) });
			this.form = this.wnd.getContent().getElementsByTagName("FORM")[0];
			this.form.onsubmit = this.onsubmit.bind(this);
  		this.form.elements["add"].onclick=this.onadd.bind(this);
  		this.form.elements["delete"].onclick=this.ondelete.bind(this);

      this.deleted = "";

			// load data
			new Ajax.Request('obituaries.php', {parameters:'action=load_newobits_subscription', onSuccess:this.onload.bind(this)});
		},

		onload: function(t) {
 			jslog.debug(t.responseText);
//			jslog.debug(t.getResponseHeader("X-JSON"));
      var json = eval(t.responseText);
      var self = this;
      $H(json).each(function(elem) {
    		var opt = new Option(elem.value.name+" / "+elem.value.email+" / "+elem.value.created+(elem.value.enabled=="0"?" / inactive":""));
        opt.setAttribute("name", elem.value.name);
        opt.setAttribute("email", elem.value.email);
  			self.form.elements["subscriptions"].options.add(opt);
      } );  		
			this.wnd.hideloader();
		},

		onsubmit: function() {
			this.onok();
			return false;
		},

		onsave: function(t) {
			jslog.debug(t.responseText);
			this.wnd.hide();
		},
		
		onok: function() {
			this.wnd.showloader();
  		var i;
			var str = "";
			var elem = this.form.elements["subscriptions"];
			for(i=0; i<elem.options.length; i++) {
        if(elem.options[i].getAttribute("isnew")) {
  			  // save only newly created entries (they have name attr)
        	if(str.length) str+="&";
  				str+="names[]="+escape(elem.options[i].getAttribute("name"))+"&emails[]="+escape(elem.options[i].getAttribute("email"));
        }
			}
      new Ajax.Request('obituaries.php', {parameters:'action=save_newobits_subscription'+'&r='+this.deleted+"&"+str, onSuccess:this.onsave.bind(this)});
		},

    onadd: function() {
      if(this.form.elements["name"].value && this.form.elements["email"].value) {
    		var opt = new Option(this.form.elements["name"].value+" / "+this.form.elements["email"].value);
        opt.setAttribute("name", this.form.elements["name"].value);
        opt.setAttribute("email", this.form.elements["email"].value);
        opt.setAttribute("isnew", 1);
  			this.form.elements["subscriptions"].options.add(opt);
  			this.form.elements["subscriptions"].selectedIndex = this.form.elements["subscriptions"].options.length-1;
        this.form.elements["name"].value = "";
        this.form.elements["email"].value = "";
      }
    },

    ondelete: function() {
  		var i = this.form.elements["subscriptions"].selectedIndex;

      if(!this.form.elements["subscriptions"].options[i].getAttribute("isnew")) {
        // remember deleted emails
        if(this.deleted) this.deleted+=",";
        this.deleted += this.form.elements["subscriptions"].options[i].getAttribute("email");
      }

			if(i!=-1) {
			  if(document.all) {
					this.form.elements["subscriptions"].options[i] = null;
				}
				else {
				    this.form.elements["subscriptions"].remove(i);
				}
				if(this.form.elements["subscriptions"].options.length) {
					if(!i)i=1;
					this.form.elements["subscriptions"].selectedIndex = i-1;
				}
				else {
					this.currentIndex = -1;
				}
			}
    }
	}
	
	function obituary_deleted(t)
	{
		update_obituaries();
	}
	
	function delete_obituary(obituary_id)
	{
		if(confirm("Are you sure you want to delete obituary?\nAll candle will be LOST!")) {
			new Ajax.Request('obituaries.php', {parameters:'action=delete&id='+obituary_id, onSuccess:obituary_deleted}); // TODO: handle AJAX errors here
		}
	}

	function page(start)
	{
		new Ajax.Updater('obituaries', 'obituaries.php', {parameters: "action=list&start="+start});  // add AJAX error handling here
	}
	function order(field)
	{
		new Ajax.Updater('obituaries', 'obituaries.php', {parameters: "action=list&order="+field});  // add AJAX error handling here
	}
	function update_obituaries()
	{
		new Ajax.Updater('obituaries', 'obituaries.php', {parameters: "action=list"});  // add AJAX error handling here
	}	
	update_obituaries();
{/literal}
{include file="obit_config.tpl"}
</script>	

{include file="adm_footer.tpl"}
