<h2>Candles</h2>
{if $sort eq 1}
<small>showing old to new; <a href="/obituary_view/{$slug}/{$obituary_id}?candle=all&sort=desc">show new to old</a></small>
{elseif $sort eq 2}
<small>showing new to old; <a href="/obituary_view/{$slug}/{$obituary_id}?candle=all">show old to new</a></small>
{/if}
<div class="splitter"></div>
<div class="candles">
{foreach from=$candles item=candle}
<div class="candle">
<div class="from"><a href="/obituary_view/{$slug}/{$candle.obituary_id}?candle={$candle.candle_id}"><b>{$candle.name}</b>, {$candle.timestamp}</a></div>
<div class="splitter2"></div>
{$candle.thoughts}
</div>
{/foreach}
<a href="javascript:history.go(-1)">&laquo; back</a>
</div>
