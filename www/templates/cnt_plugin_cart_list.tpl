{if $paypal_checkout != 1}
<table width="100%">
<tr><th></th><th>product</th><th>price</th><th>quantity</th><th>amount</th></tr>
{foreach from=$cart key=pid item=product}
	<tr align="center"><td>{$product.image}</td><td>{$product.name}</td><td>${$product.price|number_format:2}</td><td>{$product.count}</td><td><b>${$product.amount|number_format:2}</b></td><td width="20%"><input type="text" class="cart-count" id="cart-count-{$product.product_id}" name="cart_count[]" title="Item(s) to add/remove from cart" value="1"><a href="#" onclick="remove_item({$pid}); return false;">remove item(s)</a></td></tr>
{/foreach}
<tr><td></td><td></td><td></td><td align="right"><b>Total:</b></td><td align="center"><b>${$total|number_format:2}</b></td></tr>
<tr><td></td><td></td><td></td><td></td><td align="center"><input type="button" value="checkout" onclick="document.location='?p=checkout'"></td></tr>
</table>
{else}
<form action="https://www.paypal.com/us/cgi-bin/webscr" method="post" target="_top">
<input type="hidden" name="business" value="{$config.paypal_business}">
<input type="hidden" name="cmd" value="_cart">
<input type="hidden" name="upload" value="1">
<input type="hidden" name="currency_code" value="USD">
<input type="hidden" name="notify_url" value="https://ws1.twintierstech.net/paypal_ipn.php">
<input type="hidden" name="return" value="https://ws1.twintierstech.net/paypal.php">
<input type="hidden" name="rm" value="2">
<input type="hidden" name="custom" value="{$txn}">
<table width="100%">
<tr><th></th><th>product</th><th>price</th><th>quantity</th><th>amount</th></tr>
{foreach from=$cart key=pid item=product}
	<tr align="center"><td>{$product.image}</td><td>{$product.name}</td><td>${$product.price|number_format:2}</td><td>{$product.count}</td><td><b>${$product.amount|number_format:2}</b></td><td width="20%"><input type="text" class="cart-count" id="cart-count-{$product.product_id}" name="cart_count[]" title="Item(s) to add/remove from cart" value="1"><br/><a href="#" onclick="add_item({$product.product_id}); return false;">update cart</a><br/><a href="#" onclick="remove_item({$product.product_id}); return false;">remove item(s)</a><br/></td></tr>
    <tr><td colspan="6"><div class="custom_fields_holder"></div></td></tr>
{counter name="i" assign="i"}
{if $product.additional_fields eq 1}
  {if $product.field1enabled}
    {counter name="oi" assign="oi" start="0"}
    <tr><td colspan="6"><label class="cart_extrafield_label">{$product.field1title}</label>
      <input type="hidden" name="on{$oi}_{$i}" value="{$product.field1title}">
		  {if $product.field1type eq 1}
            <textarea name="os{$oi}_{$i}" style="width:70%" rows="4" maxlength="255" class="length-limited"></textarea>
		  {else}
			  <input type="text" name="os{$oi}_{$i}" style="width:70%" maxlength="255">
		  {/if}
		</td></tr>
	{/if}
  {if $product.field2enabled}
    {counter name="oi" assign="oi"}
    <tr><td colspan="6"><label class="cart_extrafield_label">{$product.field2title}</label>
      <input type="hidden" name="on{$oi}_{$i}" value="{$product.field2title}">
      {if $product.field2type eq 1}
        <textarea name="os{$oi}_{$i}" style="width:70%" rows="4" maxlength="255" class="length-limited"></textarea>
      {else}
        <input type="text" name="os{$oi}_{$i}" style="width:70%" maxlength="255">
      {/if}
    </td></tr>
  {/if}
{/if}	
<input type="hidden" name="item_number_{$i}" value="{$pid}">
<input type="hidden" name="quantity_{$i}" value="{$product.count}">
<input type="hidden" name="item_name_{$i}" value="{$product.name}">
<input type="hidden" name="amount_{$i}" value="{$product.price}">
{/foreach}
<tr><td></td><td></td><td></td><td align="right"><b>Total:</b></td><td align="center"><b>${$total|number_format:2}</b></td></tr>
<tr><td></td><td></td><td></td><td></td><td align="center">
<br>
<input type="submit" value="Proceed to checkout">
</td></tr>
</table>
</form>
{/if}
