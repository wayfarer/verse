{include file="florist_cart.tpl"}

<h1>{$product.name}</h1>
{if $product.image}
    <img src="{$product.image}"><br/>
{else}
    <img src="{$product.large}"><br/>
{/if}
{if $product.price}
    <b>${$product.price|number_format:2}</b><br/>
{/if}
{$product.description}
<br/><br/>
<input type="text" class="cart-count-product" id="cart-count-{$product.code}" name="cart_count[]" title="Item(s) to add/remove from cart" value="1"><br/>
<a href="#" onclick="florist_cart('{$product.code}');return false">add to cart</a><br/>
<small id="p{$product.code}" count="{$product.count}">
{if $product.count}
    {$product.count} item(s) in cart
    </small><br/>
    <div id="r{$product.code}">
{else}
    </small><br/>
    <div style="display: none" id="r{$product.code}">
{/if}
    <a href="#" onclick="florist_cart_remove('{$product.code}');return false;">remove</a><br/>
    <a href="/flowercart">review cart</a>
</div>

<br/><br/>
<a href="javascript: history.go(-1)">&laquo; back</a>