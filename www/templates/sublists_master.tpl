{include file="adm_header.tpl" menu="1"}

<h1>Products :: Master List</h1>
<div class="submenu"><a href="sublists.php">My List</a> | <a href="sublists_master.php">Master List</a></div>
<a href="sublists.php?print=1" target="_blank"><img src="img/printer.png"></a>
<div id="products"></div>
{literal}
<script type="text/javascript">
	function success(t)
	{
		update_products();
	}
	
	function add_mylist(product_id)
	{
		new Ajax.Request('ajax_sublists.php', {parameters:'action=add_mylist&id='+product_id, onSuccess:success}); // TODO: handle AJAX errors here
	}
	function remove_mylist(product_id)
	{
		new Ajax.Request('ajax_sublists.php', {parameters:'action=remove_mylist&id='+product_id, onSuccess:success}); // TODO: handle AJAX errors here
	}
	
	function page(start)
	{
		new Ajax.Updater('products', 'ajax_sublists.php', {parameters: "action=masterlist&start="+start});
	}
	function order(field)
	{
		new Ajax.Updater('products', 'ajax_sublists.php', {parameters: "action=masterlist&order="+field});
	}
	function update_products()
	{
		new Ajax.Updater('products', 'ajax_sublists.php', {parameters: "action=masterlist"}); 
	}	
	update_products();
</script>	
{/literal}

{include file="adm_footer.tpl"}
