<script>
function update_thread() {ldelim}
	$("#verse-wall").load("wall.php?w={$wall_id}&t={$thread_id}", hide_progress);
}
</script>
<h1>{$title}</h1>
<a class="menu" href="#" onclick="update_wall(); return false;">&lt;&lt; back to discussions list</a>
<table cellpadding="0" cellspacing="0">
{foreach from=$messages item=message}
<tr id="msg-{$message.wall_message_id}"><td class="post">
<div class="level level{$message.level}">
<div class="info">
<span style="float:right">{$message.created_at|date_format:"%a, %b %e, %Y %H:%M"}</span>
From: {$message.name|default:"Anonymous"}
</div>
<div class="message">
{$message.message|nl2br}
</div>
<div class="footer">
<a href="#" onclick="reply_to({$message.wall_message_id}); return false;">reply to this</a>
{if ($message.user_id == $user_id) or ($user.privileges == 1)}
 :: <a href="#" onclick="delete_message({$message.wall_message_id}); return false;">delete</a>
{/if}
</div>
</div>
<div class="reply-place level{$message.level}" id="reply{$message.wall_message_id}">

</div>
</td></tr>
{/foreach}
</table>
<a class="menu" href="#" onclick="update_wall(); return false;">&lt;&lt; back to discussions list</a>
<div class="reply">
<a href="#" onclick="reply_to(0); return false;">Post a message to the discussion</a>
<div id="reply0">
<form id="post-message-form">
<input type="hidden" name="thread_id" value="{$thread_id}">
<input type="hidden" name="reply_to_id" value="0">
<label>Name:</label>
{if $user.name}
<input type="text" name="name" value="{$user.name}" readonly="1">
{else}
<input type="text" name="name">
{/if}
<br>
<label>E-mail:</label>
{if $user.email}
<input type="text" name="email" value="{$user.email}" readonly="1">
{else}
<input type="text" name="email">
{/if}
<br>
<label>Message:</label>
<textarea style="width: 400px; height: 200px;" name="message"></textarea><br>
<label></label><input type="button" value="Post" onclick="post_message(this.form)">
</form>
</div>
</div>