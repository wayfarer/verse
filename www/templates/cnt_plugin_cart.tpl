<script type="text/javascript" src="js/prototype.js"></script>
<h1>Shopping cart</h1>
<div id="cart"><img src="img/loader-big.gif"></div>
<br>
<a href="javascript: history.go(-1)">&laquo; back</a>
<script>
	function remove_item(product_id)
	{ldelim}
		new Ajax.Updater('cart', 'ajax_plg_products.php', {ldelim}parameters: "action=cart_remove&id="+product_id+"&cart_count="+get_cart_count(product_id)});
	}

    function add_item(product_id)
    {ldelim}
        new Ajax.Updater('cart', 'ajax_plg_products.php', {ldelim}parameters: "action=cart_add&id="+product_id+"&cart_count="+get_cart_count(product_id)+"&update_list=1"});
    }

    function get_cart_count(product_id)
    {ldelim}
        return Math.abs(parseInt($("cart-count-"+product_id).value));
    }

    // shows custom fields below each item if present
    function process_custom_fields() {ldelim}
        var products = $$('.custom_fields_holder');
        for(var i=0; i<products.length; i++ ) {ldelim}
            var html = $('custom_fields').innerHTML;
            if(i>0) {ldelim}
                var j=0;
                while(html.indexOf('on'+j+'_1')!=-1) {ldelim}
                    html = html.replace('on'+j+'_1', 'on'+j+'_'+(i+1));
                    html = html.replace('os'+j+'_1', 'os'+j+'_'+(i+1));
                    j++;
                }
            }
            products[i].update(html);
        }
    }

    function update_cart_list()
    {ldelim}
	    new Ajax.Updater('cart', 'ajax_plg_products.php', {ldelim}parameters: "action=cart_list", onComplete: process_custom_fields});
    }

    update_cart_list();
</script>
<script type="text/javascript" src="js/jquery.inputlimiter.1.2.2.min.js"></script>
<script type="text/javascript">
    jQuery(function() {ldelim}
        jQuery('.length-limited').inputlimiter();
    });
</script>
{if isset($custom_fields)}
<div id="custom_fields" style="display:none">
    {$custom_fields}
</div>
{/if}
