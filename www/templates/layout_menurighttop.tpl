<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<!-- {$smarty.const.VERSE_SERVER_NAME} -->
<html xmlns="http://www.w3.org/1999/xhtml" lang="ru" xml:lang="ru">
<head>
<title>{$site.title}</title>
<meta name="encoding" content="text/html; charset=UTF-8" />  
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
{if $site.props.description}
<meta name="description" content="{$site.props.description}">
{/if}
{if $site.props.keywords}
<meta name="keywords" content="{$site.props.keywords}">
{/if}
{if $site.props.headhtml}
{$site.props.headhtml}
{/if}
<link rel="stylesheet" href="/css/menuleft.css" type="text/css" />
<link rel="stylesheet" href="/css/general.css" type="text/css" />
<script type="text/javascript" src="/js/jquery-1.4.2.min.js"></script>
{if $page_type eq 3}
<link rel="stylesheet" href="/calendar-brown.css" type="text/css" />
<script type="text/javascript" src="/js/calendar.js"></script>
<script type="text/javascript" src="/js/calendar-en.js"></script>
<script type="text/javascript" src="/js/calendar-setup.js"></script>
{/if}
{if $allow_edit}
    {include file="ace_editor.tpl"}
{/if}
<style>
body {ldelim}
{if $site.props.bgcolor}
	background-color: {$site.props.bgcolor};
{/if}
{if $site.props.align==2}
	text-align: center;
{/if}
{if $site.props.bgimage}
	background-image: url({$site.props.bgimage});
{/if}
}
div.container {ldelim}
	width: {$site.props.width};
{if $site.props.align==2}
	margin: 0 auto;
{/if}
	background-color: {$site.props.bgcolor};
	text-align: left;
}
table.container {ldelim}
	width: {$site.props.width};
}
div.main_menu ul {ldelim}
	margin: 0; 
	padding: 0; 
	list-style-type: none;
	width: {$site.main_menu.width};
}

div.main_menu ul li {ldelim}
	position: relative;
	display: block;
	margin: 0;
	height: {$site.main_menu.height};
	z-index: 2;
}

div.main_menu ul li a {ldelim}
	display: block;
	width: auto !important;
	width: 100%;
	xheight: {$site.main_menu.height};
	height: 100%;
}

div.main_menu {ldelim}
    {if $site.main_menu.font}
	font: {$site.main_menu.font};
    {/if}
}

div.main_menu ul.popup_menu {ldelim}
	width: {$site.popup_menu.width};
    {if $site.popup_menu.border}
	border: 1px solid black;
	{/if}
    {if $site.popup_menu.font}
	font: {$site.popup_menu.font};
    {/if}
}

div.main_menu ul.popup_menu li {ldelim}
	height: {$site.popup_menu.height};
}

div.main_menu ul.popup_menu li a {ldelim}
	display: block;
	width: auto !important;
	width: 100%;
	xheight: {$site.popup_menu.height};
	height: 100%;
	padding-left: 4px;
}

div.main_menu a.rollover {ldelim}
	color: {$site.main_menu.color};
	background-color: {$site.main_menu.bgcolor};
	text-decoration: none;
	padding-left: 2px;
}

div.main_menu a.rollover:hover {ldelim}
	color: {$site.main_menu.hvcolor};
	background-color: {$site.main_menu.hvbgcolor};
	text-decoration: none;
} 

div.main_menu ul.popup_menu li a.rollover {ldelim}
	color: {$site.popup_menu.color};
	background-color: {$site.popup_menu.bgcolor};
	text-decoration: none;
	padding-left: 2px;
}

div.main_menu ul.popup_menu li a.rollover:hover {ldelim}
	color: {$site.popup_menu.hvcolor};
	background-color: {$site.popup_menu.hvbgcolor};
	text-decoration: none;
} 

{literal}
body, form {
	margin: 0;
}

form {
	margin-bottom: 10px;
}

p, h1, h2 {
	margin: 0;
	margin-bottom: 5px;
	margin-top: 5px;
}

h1 {
	font-family: Arial;
	font-size: 21px;
}

h2 {
	font-family: Arial;
	font-size: 18px;
}

img {
	border: 0;
}

a {
	text-decoration: none;
	color: #5000FF;
}

a:hover {
	text-decoration: underline;
}

label {
	display: block;
	position: relative; /* fix for IE */
	float: left;
	text-align: right;
	width: 110px !important;
	width: 120px;
	padding-right: 10px;
	margin-bottom: 3px;
}

br {
	clear: left;
}

div.main_menu {
	float: left;
}

div.main_menu li ul {
	position: absolute; 
	left: 100%; 
	top: 0; 
	margin-left: 0 !important;
	margin-left: 1px;
}

/* popup stuff */
div.main_menu li ul,
div.main_menu li:hover ul ul
{ display: none; }

div.main_menu li:hover ul,
div.main_menu li:hover ul li:hover ul
{ display: block; }

/* IE UL display fix */
div.main_menu ul li 
{
	float: left; width: 100%;
}

div.content{
	padding: 0;
}
{/literal}
</style>
<style type="text/css" id="site-css">
    {$site.css}
</style>
{literal}
<!--[if IE]>
<style type="text/css" media="screen">
body {behavior: url(js/csshover.htc);} 
</style>
<![endif]-->
{/literal}
</head>
<body>
<div class="container">
<table cellspacing="0" cellpadding="0" class="container">
<tr><td valign="top">
{include file="layout_header.tpl"}
</td>
	<td valign="top" width="{$site.main_menu.width}" rowspan="2">
<div class="main_menu">
{if $site.main_menu.prespacer}
<img src="{$site.main_menu.prespacer}">{/if}<ul>
{section name=node loop=$structure}
<li{if $structure[node].height && $structure[node].height != "default"} style="height: {$structure[node].height}"{/if}>{if $structure[node].menu_type==1 || $site.menu_type==1}{* text node *}
	<a href={if $structure[node].internal_name}"/{$structure[node].internal_name}"{else}"" onclick="return false;"{/if} class="rollover"{if $structure[node].newwindow==1} target="_blank"{/if}>{$structure[node].display_name}</a>
	{elseif $structure[node].menu_type==2 || $site.menu_type==2}{* static image node *}
	<a href={if $structure[node].internal_name}"/{$structure[node].internal_name}"{else}"" onclick="return false;"{/if} title="{$structure[node].display_name}"{if $structure[node].newwindow==1} target="_blank"{/if}><img src="{$structure[node].image}"></a>
	{else}{* rollover node *}
	not implemented :O
	{/if}
{if isset($structure[node.index_next])}
{if $structure[node].depth==$structure[node.index_next].depth}
</li>
{elseif $structure[node].depth<$structure[node.index_next].depth}
	<ul class="popup_menu">
	{if $site.popup_menu.prespacer}
		<img src="{$site.popup_menu.prespacer}">
	{/if}	
{else}
	{section name=i loop=$structure[node].depth-$structure[node.index_next].depth}
	</li>
	{if $site.popup_menu.postspacer}
		<img src="{$site.popup_menu.postspacer}">
	{/if}	
	</ul>
	{/section}
	</li>
{/if}
{else}
</li>
{/if}
{/section}
</ul>
{if $site.main_menu.postspacer}
<img src="{$site.main_menu.postspacer}">
{/if}
</div>
</td></tr>
<tr><td valign="top">
<div class="content">
{$content}
</div>
</td></tr>
</table>
<div class="footer">
{$site.footer}
</div>
</div>
{if $allow_edit}
    <div id="edit-buttons" align="{$site.props.align}">
        <input type="button" id="edit-css-button" class="editor-button" value="Edit CSS" editor="css">
    </div>
{/if}
</body>
</html>