{include file="adm_header.tpl" menu="1"}
{include file="window.tpl"}
{include file="contextmenu.tpl"}
{include file="util.tpl"}

<h1>Candles moderation</h1>
<div class="submenu"><a href="backend.php/obituaries">&laquo; back to obituaries</a></div>
<div id="candles"><img src="img/loader-big.gif"></div>
<script type="text/javascript">
{literal}
	function candle_deleted(t)
	{
		update_candles();
	}

	function hide_candle(candle_id)
	{
		new Ajax.Request('obituaries.php', {parameters:'action=hide_candle&id='+candle_id, onSuccess:candle_deleted}); // TODO: handle AJAX errors here
	}
	function approve(candle_id)
	{
		new Ajax.Request('obituaries.php', {parameters:'action=approve_candle&id='+candle_id, onSuccess:candle_deleted}); // TODO: handle AJAX errors here
	}
{/literal}
	function page(start)
	{ldelim}
		new Ajax.Updater('candles', 'obituaries.php', {ldelim}parameters: "action=list_candles_notapproved&start="+start});  // add AJAX error handling here
	}
	function order(field)
	{ldelim}
		new Ajax.Updater('candles', 'obituaries.php', {ldelim}parameters: "action=list_candles_notapproved&order="+field});  // add AJAX error handling here
	}
	function update_candles()
	{ldelim}
		new Ajax.Updater('candles', 'obituaries.php', {ldelim}parameters: "action=list_candles_notapproved"});  // add AJAX error handling here
	}
	update_candles();
</script>

{include file="adm_footer.tpl"}
