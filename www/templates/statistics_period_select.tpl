<script type="text/javascript" src="/js/jquery-1.3.2.min.js"></script>
<script type="text/javascript" src="/js/piwik/jquery-ui.js"></script>
<script type="text/javascript" src="/js/piwik/calendar.js"></script>
<script type="text/javascript" src="/js/piwik/date.js"></script>

<div id="periodString">
	<span id="date"><img src='/img/icon-calendar.gif' style="vertical-align:middle" alt="" /> {$prettyDate}</span> -&nbsp;
	<span id="periods"> 
		<span id="currentPeriod">{$periodsNames.$period.singular}</span> 
		<span id="otherPeriods">
			{foreach from=$otherPeriods item=thisPeriod} | <a href='?period={$thisPeriod}'>{$periodsNames.$thisPeriod.singular}</a>{/foreach}
		</span>
	</span>
	<br/>
	<span id="datepicker"></span>
</div>

{literal}<script language="javascript">
$(document).ready(function() {
     // this will trigger to change only the period value on search query and hash string.
     $("#otherPeriods a").bind('click',function(e) {
        e.preventDefault();                            
        var request_URL = $(e.target).attr("href");
        var new_period = getUrlParamValue('period', request_URL);
        propagateNewPage('period='+new_period);
     });
});</script>
{/literal}

<div style="clear:both"></div>

