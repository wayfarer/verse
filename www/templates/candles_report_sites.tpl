{include file="adm_header.tpl" menu="1"}

<h1>Candle reports :: Compare domains</h1>
<div class="submenu"><a href="candles_report_sites.php">Compare domains</a> | <a href="candles_report_periods.php">Compare periods</a></div>
<div class="submenu">Detalization period: <a href="#" onclick="update_period('day'); return false;">day</a> <a href="#" onclick="update_period('week'); return false;">week</a> <a href="#" onclick="update_period('month'); return false;">month</a> <a href="#" onclick="update_period('year'); return false;">year</a></div>
<div>
  <form id="filters">
    <div class="controls">
    <label for="domain_id">Domain:</label> <select id="domain_id" name="domain_id">{html_options options=$domains}</select> <input type="button" value="Add / Reset" onclick="update_chart()"> <input type="button" value="Reset graph" onclick="reset_chart()">
    </div>
    <div class="controls">    
    <input type="button" value="<< Prev.period" onclick="update_date('prev')"> <label>Period ending by:</label> <input type="text" id="date" name="date" size="8" value="{$date}" onchange="update_date(this.value)"> <input type="button" id="date_b" value=" .. "> <input type="button" value="Next period >>" onclick="update_date('next')">
    </div>
<!--    <input type="button" value="Filter!" onclick="update_chart()"> -->
  </form>
</div>

<div id="report">
  {$ofc_object}
</div>

{literal}
<script>	
<!--  
function update_period(period) {
  var chart = $("chart");
  if(!chart) chart = $("ie_chart");

  // to load from a specific URL:
  // you may need to 'escape' (URL escape, i.e. percent escape) your URL if it has & in it
//  chart.reload("candles_report_sites.php?action=cpd&"+Form.serialize($("filters")));
  chart.reload("candles_report_sites.php?action=draw&period="+period);
}
function update_date(date) {
  var chart = $("chart");
  if(!chart) chart = $("ie_chart");
  chart.reload("candles_report_sites.php?action=draw&date="+date);
}
function update_chart() {
  var chart = $("chart");
  if(!chart) chart = $("ie_chart");
//  alert($("domain_id").options[$("domain_id").selectedIndex].value);
//  alert($("domain_id").value);
  chart.reload("candles_report_sites.php?action=draw&domain="+$("domain_id").value);
}
function reset_chart() {
  var chart = $("chart");
  if(!chart) chart = $("ie_chart");
  chart.reload("candles_report_sites.php?action=draw&domain=0&date=now&period=month");
}
function remove_domain(domain_id) {
  var chart = $("chart");
  if(!chart) chart = $("ie_chart");
  chart.reload("candles_report_sites.php?action=draw&rdomain="+domain_id);
}

Calendar.setup( {
	inputField : "date",
	ifFormat : "%Y-%m-%d",
	button : "date_b",
	step: 1
} );
-->
</script>	
{/literal}

{include file="adm_footer.tpl"}
