{include file="adm_header.tpl" menu="1"}
{include file="window.tpl"}
{include file="util.tpl"}

<h1>Site members</h1>
<div class="actions"><a href="#" onclick="new MemberEditor(0); return false;">new member</a> | <a href="#" onclick="new MembersConfig(0); return false;">settings</a></div>
<div id="members"><img src="img/loader-big.gif"></div>
<script type="text/javascript">
{include file="populate_controls.tpl"}
{literal}
  var MemberEditor = Class.create();
  MemberEditor.prototype = {
    initialize: function(member_id) {
      // create window
//      this.wnd = new WindowClass("Member", 500, 290, [this.onok.bind(this), this.oncancel.bind(this)]);
      this.member_id = member_id;
      // create form
      // create dialog fields in form
{/literal}
      var f = '\
          <form>\
          <label for="name">Name</label><input type="text" id="name" name="name" class="txt"><br>\
					<label for="ulogin">Login</label><input type="text" id="ulogin" name="ulogin" class="txt"><br>\
					<label for="password">Password <small>(leave blank to keep old password)</small></label><input type="password" id="password" name="password" class="txt"><br>\
          <label for="email">Email</label><input type="text" id="email" name="email" class="txt"><br>\
					<label for="moderator">Moderator</label><input type="checkbox" id="moderator" name="moderator"><br>\
          </form>\
        ';
{literal}
      this.wnd = Dialog.confirm(f, {windowParameters: {className:"alphacube", width:350, height:180, title:"Member", showEffect: Element.show, hideEffect: Element.hide},
                        ok: this.onok.bind(this) });
      this.form = this.wnd.getContent().getElementsByTagName("FORM")[0];
      this.form.onsubmit = this.onsubmit.bind(this);
      
      Form.focusFirstElement(this.form);

      if(member_id) {
        // load data
        new Ajax.Request('members.php', {parameters:'action=load&id='+member_id, onSuccess:this.onload.bind(this)}); // TODO: handle AJAX errors here
      }
      else {
        // defaults
        this.wnd.hideloader();
      }
    },

    onload: function(t, json) {
      jslog.debug(t.getResponseHeader("X-JSON"));
      populate_controls(this.form, json);
      this.wnd.hideloader();
    },

    onsubmit: function() {
      this.onok();
      return false;
    },

    onsave: function(t) {
      jslog.debug(t.responseText);
      update_members();
      this.wnd.hide();
    },
    
    onok: function() {
      this.wnd.showloader();
      // save content from wysiwyg to textarea
      new Ajax.Request('members.php', {parameters:'action=save&id='+this.member_id+"&"+Form.serialize(this.form), onSuccess:this.onsave.bind(this)}); // TODO: handle AJAX errors here
    }
  }

	var MembersConfig = Class.create();
	MembersConfig.prototype = {
		initialize: function() {
{/literal}
			var f = '<form>\
					  <label for="member_signup_notification_email">Signups notification e-mail</label><input type="text" id="member_signup_notification_email" name="member_signup_notification_email" class="txt2"><br>\
					 </form>\
				';
{literal}
			this.wnd = Dialog.confirm(f, {windowParameters: {className:"alphacube", width:430, title:"Members configuration", showEffect: Element.show, hideEffect: Element.hide},
									      ok: this.onok.bind(this) });
			this.form = this.wnd.getContent().getElementsByTagName("FORM")[0];
			Form.focusFirstElement(this.form);
			// load data
			new Ajax.Request('members.php', {parameters:'action=load_config', onSuccess:this.onload.bind(this)});
		},

		onload: function(t, json) {
			populate_controls(this.form, json);
			this.wnd.hideloader();
		},

		onsave: function(t) {
			this.wnd.hide();
    },
		
		onok: function() {
			this.wnd.showloader();
			new Ajax.Request('members.php', {parameters:'action=save_config&'+Form.serialize(this.form), onSuccess:this.onsave.bind(this)});
		}
	}


  function member_deleted(t)
  {
    update_members();
  }
  
  function delete_member(member_id) {
    if(confirm("Are you sure you want to delete member?")) {
      new Ajax.Request('members.php', {parameters:'action=delete&id='+member_id, onSuccess:member_deleted}); // TODO: handle AJAX errors here
    }
  }
  function approve_member(member_id) {
    if(confirm("Are you sure you want to approve member?")) {
      new Ajax.Request('members.php', {parameters:'action=approve&id='+member_id, onSuccess:member_deleted}); // TODO: handle AJAX errors here
    }    
  }
  function block_member(member_id) {
    if(confirm("Are you sure you want to block member?")) {
      new Ajax.Request('members.php', {parameters:'action=block&id='+member_id, onSuccess:member_deleted}); // TODO: handle AJAX errors here
    }    
  }
  function unblock_member(member_id) {
    if(confirm("Are you sure you want to unblock member?")) {
      new Ajax.Request('members.php', {parameters:'action=unblock&id='+member_id, onSuccess:member_deleted}); // TODO: handle AJAX errors here
    }    
  }
  
  function page(start)
  {
    new Ajax.Updater('members', 'members.php', {parameters: "action=list&start="+start});  // add AJAX error handling here
  }
  function order(field)
  {
    new Ajax.Updater('members', 'members.php', {parameters: "action=list&order="+field});  // add AJAX error handling here
  }
  function update_members()
  {
    new Ajax.Updater('members', 'members.php', {parameters: "action=list"});
  } 
  update_members();
{/literal}
</script> 

{include file="adm_footer.tpl"}
