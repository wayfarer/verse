<link media="screen" type="text/css" href="/css/highslide.css" rel="stylesheet">
<!--[if lte IE 6 ]>
    <link media="screen" type="text/css" href="/css/highslide-ie6.css" rel="stylesheet">
<![endif]-->
<script type="text/javascript" src="js/jquery.jcarousel.min.js"></script>
<script type="text/javascript" src="/js/highslide-with-gallery.js"></script>
<script type="text/javascript">
    hs.graphicsDir = '/css/graphics/';
    hs.align = 'center';
    hs.transitions = ['expand', 'crossfade'];
    hs.wrapperClassName = 'dark borderless floating-caption';
    hs.fadeInOut = true;
    hs.dimmingOpacity = .75;
    // Add the controlbar
    if (hs.addSlideshow) hs.addSlideshow({ldelim}
        interval: 5000,
        repeat: false,
        useControls: true,
        fixedControls: 'fit',
        overlayOptions: {ldelim}
            opacity: .6,
            position: 'bottom center',
            hideOnMouseOut: true
        }
    });
</script>
<script type="text/javascript">
    jQuery(function() {ldelim}
        $('.last-obituaries').jcarousel({ldelim}
            scroll: 1,
            size:5,
            animation: 1000,
            auto: 8,
            wrap: 'circular',
            itemFallbackDimension: 100
        });
        $('.jcarousel-prev, .jcarousel-next').css('top', $('.last-obituaries').height()/2-5);
        $('.last-obituaries').css('margin-left', ($('.last-obituaries-box').width()-545)/2);
    });
</script>
<h3>Recent Obits</h3>
<div class="last-obituaries-box">
<div class="last-obituaries">
    <ul>
        {foreach from=$obituaries item=obituary}
            <li><div class="last-obituaries-item"><a href="/{$obituary.image}" class="last-obituaries-image" onclick="return hs.expand(this, {ldelim} slideshowGroup: 'recent-obit-photo' })"><img src="/{$obituary.thumbnail}"></a><div class="last-obituaries-item-text"><div><a href="/online-obituary/{$obituary.obituary_slug}/{$obituary.obituary_id}">{$obituary.first_name} {$obituary.last_name}</a></div><div>{$obituary.death_date}</div></div></div></li>
        {/foreach}
    </ul>
</div>
</div>