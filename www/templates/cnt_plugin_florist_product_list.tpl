<a href="/{$flowers_page}" class="florist-all-categories-link">All categories</a>

{include file="florist_cart.tpl"}

<table id="products_pagenator" class="florist-pagenator" cellspacing="0" cellpadding="0" border="0">
    <tr>
        <td align="left">
            {if $prev > 0}
                <a href="/{$flowers_page}/{$category_link}?start=1"><<</a>
                <a href="/{$flowers_page}/{$category_link}?start={$prev}"><</a>
            {else}
                << <
            {/if}
        </td>
        <td align="center">{$position_text} : {$total}</td>
        <td align="right">
            {if $next < $total}
                <a href="/{$flowers_page}/{$category_link}?start={$next}">></a>
                <a href="/{$flowers_page}/{$category_link}?start={$last}">>></a>
            {else}
                > >>
            {/if}
        </td>
    </tr>
</table>

<table id="products_report" class="florist-report">
    <tr class="rep_header">
        <th></th>
        <th>
            Product
        </th>
        <th>
            Price
        </th>
        <th>
            Description
        </th>
        <th></th>
    </tr>
    {foreach from=$products item=product}
        <tr class="content">
            <td width="20%" align="center">
                {if $product.thumbnail}
                    <img src="{$product.thumbnail}">
                {else}
                    <img src="{$product.small}">
                {/if}
            </td>
            <td width="15%" align="center">
                <a href="/{$flowers_page}/{$category_link}/{$product.code}">{$product.name}</a>
            </td>
            <td width="10%" align="center">
                <strong>${$product.price}</strong>
            </td>
            <td width="45%" align="center">
                {$product.description}
            </td>
            <td width="10%" align="center">
                <input id="cart-count-{$product.code}" class="cart-count-product" type="text" value="1" title="Item(s) to add/remove from cart" name="cart_count[]">
                <br/>
                <a onclick="florist_cart('{$product.code}'); return false;" href="#">add to cart</a>
                <br/>
                {if $product.count}
                <small id="p{$product.code}" count="{$product.count}">{$product.count} item(s) in cart</small>
                {else}
                    <small id="p{$product.code}" count="0"></small>
                {/if}
                <br/>
                <div id="r{$product.code}"{if !$product.count} style="display: none"{/if}>
                    <a onclick="florist_cart_remove('{$product.code}'); return false;" href="#">remove</a>
                    <br/>
                    <a href="/flowercart">review cart</a>
                </div>
            </td>
        </tr>
    {/foreach}
</table>