<h1>{$page_title}</h1>
{foreach key=key item=link from=$links}
	{""|indent:$link.depth:"&nbsp;&nbsp;&nbsp;&nbsp;"}<a href={if $link.internal_name}"/{$link.internal_name}"{else}"/" onclick="return false;"{/if}>{$link.display_name}</a><br>
{/foreach}
