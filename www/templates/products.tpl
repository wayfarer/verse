{include file="adm_header.tpl" menu="1"}
{include file="window.tpl"}
{include file="contextmenu.tpl"}
{include file="util.tpl"}

<h1>Ecommerce :: Products</h1>
<div class="submenu"><a href="products.php">Products</a> | <a href="products_categories.php">Categories</a>{if $ecommerce_enabled} | <a href="products_orders.php">Orders</a>{/if}{if $affiliates_enabled} | <a href="aff.php">Affiliates</a>{/if}</div>
<div class="actions"><a href="#" onclick="new ProductEditor(0); return false;">new product</a> / <a href="#" onclick="new EcommerceConfig(); return false;">ecommerce config</a></div>
<a href="products.php?print=1" target="_blank"><img src="img/printer.png"></a>
<div id="products"><img src="img/loader-big.gif"></div>
<script type="text/javascript" src="js/nicEdit.js"></script>
<script type="text/javascript">
{include file="populate_controls.tpl"}
{literal}
  var ProductEditor = Class.create();
  ProductEditor.prototype = {
    initialize: function(product_id) {
      // create window
//      this.wnd = new WindowClass("Product", 500, 290, [this.onok.bind(this), this.oncancel.bind(this)]);
      this.product_id = product_id;
      // create form
//      this.form = Builder.node("FORM");
      // create dialog fields in form
{/literal}
{capture name=product_categories}
  {html_options options=$product_categories}
{/capture}
      var f = '\
          <form>\
          <label for="name">Product</label><input type="text" id="name" name="name" class="txt3"><br>\
          <label for="category_id">Category</label><select id="category_id" name="category_id">{$smarty.capture.product_categories|strip}</select><br>\
          <label for="price">Price</label><input type="text" id="price" name="price" class="price"><br>\
          <label for="quantity">Quantity</label><input type="text" id="quantity" name="quantity" class="price"><br>\
          <label for="image">Image</label><input type="text" id="image" name="image" class="file"> <input type="button" name="browse_i" value="browse"><br>\
          <label for="description">Description</label><div class="wysiwyg"><textarea name="description" id="description" style="width: 520px; height: 200px"></textarea></div><br>\
          <label for="sold">Sold times</label><input type="text" id="sold" name="sold" class="price"><br>\
          <label for="enabled">Enabled</label><input type="checkbox" id="enabled" name="enabled" class="chk"><br>\
          </form>\
        ';
{literal}
      this.wnd = Dialog.confirm(f, {windowParameters: {className:"alphacube", width:670, height:450, title:"Product", showEffect: Element.show, hideEffect: Element.hide},
                        ok: this.onok.bind(this) });
      this.form = this.wnd.getContent().getElementsByTagName("FORM")[0];
      this.form.onsubmit = this.onsubmit.bind(this);
      this.sfb = new ServerFileBrowser(this.onimageselect.bind(this),"Images:/merchandise", "Images");
      this.form.elements["browse_i"].onclick=this.sfb.open.bind(this.sfb);

      var myNicEditor = new nicEditor({buttonList:['bold','italic','underline','strikethrough','subscript','superscript','ol','ul','hr','link','unlink','fontFormat','xhtml'], iconsPath: "/img/nicEditorIcons.gif", maxHeight: 200}).panelInstance('description');
      
      Form.focusFirstElement(this.form);

      if(product_id) {
        // load data
        this.json = null;
        new Ajax.Request('ajax_products.php', {parameters:'action=load&id='+product_id, onSuccess:this.onload.bind(this)}); // TODO: handle AJAX errors here
      }
      else {
        // defaults
        this.wnd.hideloader();
      }
    },

    onload: function(t, json) {
      jslog.debug(t.getResponseHeader("X-JSON"));
      populate_controls(this.form, json);
      nicEditors.findEditor("description").setContent(this.form.elements["description"].value);
      this.wnd.hideloader();
    },

    onimageselect: function(url) {
      this.form.elements["image"].value = url;
    },

    onsubmit: function() {
      this.onok();
      return false;
    },

    onsave: function(t) {
      jslog.debug(t.responseText);
      update_products();
      this.wnd.hide();
    },
    
    onok: function() {
      this.wnd.showloader();
      // save content from wysiwyg to textarea
      nicEditors.findEditor("description").saveContent();
      new Ajax.Request('ajax_products.php', {parameters:'action=save&id='+this.product_id+"&"+Form.serialize(this.form), onSuccess:this.onsave.bind(this)}); // TODO: handle AJAX errors here
    }
  }



  function product_deleted(t)
  {
    update_products();
  }
  
  function delete_product(product_id)
  {
    if(confirm("Are you sure you want to delete product?")) {
      new Ajax.Request('ajax_products.php', {parameters:'action=delete&id='+product_id, onSuccess:product_deleted}); // TODO: handle AJAX errors here
    }
  }
  
  function page(start)
  {
    new Ajax.Updater('products', 'ajax_products.php', {parameters: "action=list&start="+start});  // add AJAX error handling here
  }
  function order(field)
  {
    new Ajax.Updater('products', 'ajax_products.php', {parameters: "action=list&order="+field});  // add AJAX error handling here
  }
  function update_products()
  {
    new Ajax.Updater('products', 'ajax_products.php', {parameters: "action=list"});  // add AJAX error handling here
  } 
  update_products();
{/literal}
{include file="ecommerce_config.tpl"}
</script> 

{include file="adm_footer.tpl"}
