<h2>A candle</h2>
<div class="splitter"></div>
{if $notapproved}
<div style="color: #008000">
Thank you for submitting your thoughts! Your message may not be immediately available for public viewing, but be sure that the family will receive it.
</div>
<br>
{/if}
{if $justpublished}
<div style="color: #008000">
The candle has been published.
</div>
<br>
{/if}
<div class="candles">
<div class="candle">
<div class="from"><a href="/obituary_view/{$candle.obituary_id}?candle={$candle.candle_id}"><b>{$candle.name}</b>, {$candle.timestamp}</a></div>
<div class="splitter2"></div>
{$candle.thoughts}
</div>
<div style="margin-top: 20px">
<a href="/{$smarty.get.p}?candle=all">&laquo; Back</a>
</div>
</div>