{foreach from=$data item=page}
<div class="page{$classes[$page.domain_id]}" page_id="{$page.page_id}" id="id_{$page.page_id}" {if isset($page.domain_name)}title="{$page.domain_name}"{/if} {if isset($page.domain_id)}domain_id="{$page.domain_id}"{/if}>
	{$page.name}
</div>
{/foreach}