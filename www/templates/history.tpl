{include file="adm_header.tpl" menu="1"}
{include file="window.tpl"}
{include file="util.tpl"}

<h1>Page edit history</h1>
<div class="submenu"><a href="structure.php">&laquo; back to site structure</a></div>
<div id="history"><img src="img/loader-big.gif"></div>
<script type="text/javascript">
{include file="populate_controls.tpl"}
{literal}
	var ContentEditHTMLDialog = Class.create();
	ContentEditHTMLDialog.prototype = {
		initialize: function(page_history_id) {
			// create window
			var width = document.body.clientWidth-80;
			var height = document.body.clientHeight-130;
			this.page_history_id = page_history_id;
			// create form
			var f = '\
					<form>\
			        <label>Content HTML</label><br>\
			        <div class="tabber">\
<div class="tabbertab"><h2>Main content</h2><textarea name="content1" id="content1" style="width:'+(width-23)+'px; height:'+(height-120)+'px; margin: 0;"></textarea></div>\
<div class="tabbertab"><h2>Second content</h2><textarea name="content2" id="content2" style="width:'+(width-23)+'px; height:'+(height-120)+'px; margin: 0;"></textarea></div>\
					</div>\
					</form>\
				';
			this.wnd = Dialog.confirm(f, {windowParameters: {className:"alphacube", width:width, height:height, title:"View history element", showEffect: Element.show, hideEffect: Element.hide},
									      ok: this.onok.bind(this) });
			this.form = this.wnd.getContent().getElementsByTagName("FORM")[0];
			
			this.form.elements["content1"].onkeydown = textarea_tab;
			this.form.elements["content2"].onkeydown = textarea_tab;
			// call tabber to make tabs
			tabberAutomatic();
			Form.focusFirstElement(this.form);
			// load data
			new Ajax.Request('history.php', {parameters:'action=load&id='+page_history_id, onSuccess:this.onload.bind(this)});
		},

		onload: function(t, json) {
			jslog.debug(t.responseText);
			json = eval(t.responseText);
			populate_controls(this.form, json);
			this.wnd.hideloader();
		},

		onsave: function(t) {
			jslog.debug(t.responseText);
			this.wnd.hide();
		},

		onok: function() {
			this.wnd.showloader();
		}
	};

	function history_changed(t) {
		jslog.debug(t.responseText);
		update_history();
	}	
	function restore(history_id) {
		new Ajax.Request('history.php', {parameters:'action=restore&id='+history_id, onSuccess:history_changed});
	}
	function del(history_id) {
		if(confirm("Are you sure you want to delete history item?")) {
			new Ajax.Request('history.php', {parameters:'action=delete&id='+history_id, onSuccess:history_changed});
		}
	}
	function view(history_id) {
		//alert("Feature coming soon!");
		new ContentEditHTMLDialog(history_id);
	}
{/literal}	
	function page(start) {ldelim}
		new Ajax.Updater('history', 'history.php', {ldelim}parameters: "action=list&start="+start}); 
	}
	function order(field) {ldelim}
		new Ajax.Updater('history', 'history.php', {ldelim}parameters: "action=list&order="+field}); 
	}
	function update_history() {ldelim}
		new Ajax.Updater('history', 'history.php', {ldelim}parameters: "action=list"});
	}	
	update_history();
</script>	

{include file="adm_footer.tpl"}
