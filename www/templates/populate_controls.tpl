{literal}
	function populate_controls(form, data)
	{
		$H(data).each(function(elem) {
			if(form.elements[elem.key]) {
				switch(form.elements[elem.key].type) {
					case "text":
					case "textarea":
						form.elements[elem.key].value = elem.value;
					break;
					case "checkbox":
						form.elements[elem.key].checked = parseInt(elem.value);
					break;
					case "select-one":
						for(var i=0; i<form.elements[elem.key].length; i++) {
							if(elem.value==form.elements[elem.key].options[i].value) {
								form.elements[elem.key].selectedIndex = i;
								break;
							}
						}
					break;
					default: // treat as radio
						$A(form.elements[elem.key]).each(function(re) {
								if(re.value==elem.value) {
									re.checked = true;
								}
							}
						);
					break;
				}
			}
			else {
				var el;
				if(el = $(elem.key)) {
					el.innerHTML = elem.value;
				}
			}
		} );
	}
{/literal}