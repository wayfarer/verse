{include file="adm_header.tpl" menu="1"}
{include file="window.tpl"}
{include file="contextmenu.tpl"}
{include file="util.tpl"}

<h1>Candles review</h1>
<div class="submenu"><a href="obituaries.php">&laquo; back to obituaries</a> | <a href="candles.php?id={$obituary_id}">active candles</a> | <a href="candles.php?id={$obituary_id}&state=2">hidden candles</a></div>
<div class="actions"><a href="#" onclick="new CandleEditor(0); return false;">new candle</a></div>
<div id="candles"><img src="img/loader-big.gif"></div>
<script type="text/javascript">
{include file="populate_controls.tpl"}
{literal}
	var CandleEditor = Class.create();
	CandleEditor.prototype = {
		initialize: function(candle_id) {
			// create window
//			this.wnd = new WindowClass("Candle", 500, 290, [this.onok.bind(this), this.oncancel.bind(this)]);
			this.candle_id = candle_id;
			// create form
//			this.form = Builder.node("FORM");
			// create dialog fields in form
			var f = '\
					<form>\
					<label for="timestamp">Time</label><input type="text" id="timestamp" name="timestamp" class="txt"><br>\
					<label for="name">Name</label><input type="text" id="name" name="name" class="txt2"><br>\
  				<label for="email">Email</label><input type="text" id="email" name="email" class="txt2"><br>\
					<label for="thoughts">Thoughts</label><textarea name="thoughts" id="thoughts" cols="42" rows="5"></textarea><br>\
					</form>\
				';
			this.wnd = Dialog.confirm(f, {windowParameters: {className:"alphacube", width:520, title:"Candle", showEffect: Element.show, hideEffect: Element.hide},
									      ok: this.onok.bind(this) });
			this.form = this.wnd.getContent().getElementsByTagName("FORM")[0];
			this.form.onsubmit = this.onsubmit.bind(this);

			Form.focusFirstElement(this.form);

			if(candle_id) {
				// load data
				this.json = null;
				new Ajax.Request('obituaries.php', {parameters:'action=load_candle&id='+candle_id, onSuccess:this.onload.bind(this)}); // TODO: handle AJAX errors here
			}
			else {
				// defaults
				this.wnd.hideloader();
			}
		},

		onload: function(t, json) {
			jslog.debug(t.getResponseHeader("X-JSON"));
			populate_controls(this.form, json);
			this.wnd.hideloader();
		},

		onsubmit: function() {
			this.onok();
			return false;
		},

		onsave: function(t) {
			jslog.debug(t.responseText);
			update_candles();
			this.wnd.hide();
		},
		
		onok: function() {
			this.wnd.showloader();
{/literal}
			new Ajax.Request('obituaries.php', {ldelim}parameters:'action=save_candle&oid={$obituary_id}&id='+this.candle_id+"&"+Form.serialize(this.form), onSuccess:this.onsave.bind(this)});
{literal}
		}
	}

	function candle_deleted(t)
	{
		update_candles();
	}
	
	function hide_candle(candle_id)
	{
		new Ajax.Request('obituaries.php', {parameters:'action=hide_candle&id='+candle_id, onSuccess:candle_deleted}); // TODO: handle AJAX errors here
	}
	function unhide_candle(candle_id)
	{
		new Ajax.Request('obituaries.php', {parameters:'action=unhide_candle&id='+candle_id, onSuccess:candle_deleted}); // TODO: handle AJAX errors here
	}
	function approve(candle_id)
	{
		new Ajax.Request('obituaries.php', {parameters:'action=approve_candle&id='+candle_id, onSuccess:candle_deleted}); // TODO: handle AJAX errors here
	}
	function delete_candle(candle_id)
	{
		if(confirm("Are you sure you want to delete candle?")) {
			new Ajax.Request('obituaries.php', {parameters:'action=delete_candle&id='+candle_id, onSuccess:candle_deleted}); // TODO: handle AJAX errors here
		}
	}
{/literal}	
	function page(start)
	{ldelim}
		new Ajax.Updater('candles', 'obituaries.php', {ldelim}parameters: "action={$action}&id={$obituary_id}&start="+start});  // add AJAX error handling here
	}
	function order(field)
	{ldelim}
		new Ajax.Updater('candles', 'obituaries.php', {ldelim}parameters: "action={$action}&id={$obituary_id}&order="+field});  // add AJAX error handling here
	}
	function update_candles()
	{ldelim}
		new Ajax.Updater('candles', 'obituaries.php', {ldelim}parameters: "action={$action}&id={$obituary_id}"});  // add AJAX error handling here
	}	
	update_candles();
</script>	

{include file="adm_footer.tpl"}
