<style>
{literal}
#lastobits div.name {
	float: left;
	width: 200px;
	text-align: center;
}
#lastobits div.deathdate {
	float: left;
	width: 100px;
}
{/literal}
</style>
{$cnt}
{literal}
<script>
    if($('#from_date').length) {
        Calendar.setup( {
            inputField : "from_date",
            ifFormat : "%Y-%m-%d",
            button : "from_date_b",
            step: 1,
            width: "200px"
        } );
    }
    if($('#to_date').length) {
        Calendar.setup( {
            inputField : "to_date",
            ifFormat : "%Y-%m-%d",
            button : "to_date_b",
            step: 1
        } );
    }
</script>
{/literal}
