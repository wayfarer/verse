{if $config.candles_policy neq "off"}
<div id="candles">
{if !$smarty.get.print}
<div class="lightacandle">
<a class="candle" href="/obituary_view/{$data.slug}/{$data.obituary_id}?candle=new"><img src="/img/{if $smarty.session.domain_id neq 154}candle_big.gif{else}guestbook.jpg{/if}" style="margin-bottom: {if $smarty.session.domain_id eq 154}20px{else}0{/if}"> {$config.light_candle|default:"Light a candle by adding your thoughts and memories"}</a>
</div>
{/if}
{if $data.candles}
<hr size="1">
{foreach from=$data.candles item=candle}
	<a href="/obituary_view/{$data.slug}/{$data.obituary_id}?candle={$candle.candle_id}"><img src="/img/{if $smarty.session.domain_id neq 154}candle.gif{else}gb_small.jpg{/if}" alt="{$candle.name}"></a>
{/foreach}
<hr size="1">
{else}
<br>
{/if}
</div>
{/if}
