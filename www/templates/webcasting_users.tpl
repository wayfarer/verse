{include file="adm_header.tpl" menu="1"}
{include file="window.tpl"}
{include file="util.tpl"}

<h1>Webcasting users / {$webcasting_title}</h1>
<div class="submenu"><a href="webcasting.php">&laquo; back to webcasting management</a></div>
<div class="actions"><a href="#" onclick="new UserEditor(0); return false;">new user</a></div>
<div id="usersdiv"><img src="img/loader-big.gif"></div>
<script type="text/javascript">
{include file="populate_controls.tpl"}
{literal}
	var UserEditor = Class.create();
	UserEditor.prototype = {
		initialize: function(user_id) {
			// create window
			this.user_id = user_id;
{/literal}
			var f = '<form>\
					<label for="login">Login</label><input type="text" id="login" name="ulogin" class="txt"><br>\
					<label for="password">Password</label><input type="text" id="password" name="password" class="txt"><br><br>\
					<label for="name">Name</label><input type="text" id="name" name="name" class="txt"><br>\
					<label for="email">E-mail</label><input type="text" id="email" name="email" class="txt"><br><br>\
					<label for="enabled">Enabled</label><input type="checkbox" id="enabled" name="enabled" checked="1"><br>\
					</form>\
				';
{literal}
			this.wnd = Dialog.confirm(f, {windowParameters: {className:"alphacube", width:500, title:"Webcasting user", showEffect: Element.show, hideEffect: Element.hide},
									      ok: this.onok.bind(this) });
			this.form = this.wnd.getContent().getElementsByTagName("FORM")[0];
			this.form.onsubmit = this.onsubmit.bind(this);

			Form.focusFirstElement(this.form);
			if(user_id) {
				// load data
				new Ajax.Request('webcasting_users.php', {parameters:'action=load&id='+user_id, onSuccess:this.onload.bind(this)}); // TODO: handle AJAX errors here
			}
			else {
				this.wnd.hideloader();
			}
		},

		onload: function(t, json) {
			jslog.debug(t.getResponseHeader("X-JSON"));
  		jslog.debug(t.responseText);
			populate_controls(this.form, json);
			this.wnd.hideloader();
		},

		onsubmit: function() {
			this.onok();
			return false;
		},

		onsave: function(t) {
			jslog.debug(t.responseText);
			update_users();
			this.wnd.hide();
		},
		
		onok: function() {
			this.wnd.showloader();
{/literal}
			new Ajax.Request('webcasting_users.php', {ldelim}parameters:'action=save&id='+this.user_id+"&webcasting_id={$webcasting_id}&"+Form.serialize(this.form), onSuccess:this.onsave.bind(this)}); // TODO: handle AJAX errors here
{literal}
		}
	}

	var NotifyDialog = Class.create();
	NotifyDialog.prototype = {
		initialize: function(user_id) {
			// create window
			this.user_id = user_id;
{/literal}
			var f = '<form>\
					<label for="emailto">Email(s)<br>(one per line):</label><textarea name="emailto" id="emailto" style="width: 300px; height:100px"></textarea><br>\
					<label for="emailtext">Email text:</label><textarea name="email_text" id="email_text" style="width: 540px; height:200px"></textarea><br>\
					</form>\
				';
{literal}
			this.wnd = Dialog.confirm(f, {windowParameters: {className:"alphacube", width:560, title:"Webcasting notify e-mail", showEffect: Element.show, hideEffect: Element.hide},
									      ok: this.onok.bind(this) });
			this.form = this.wnd.getContent().getElementsByTagName("FORM")[0];
			this.form.onsubmit = this.onsubmit.bind(this);

			Form.focusFirstElement(this.form);
			
			// load data
			new Ajax.Request('webcasting_users.php', {parameters:'action=load_email&id='+user_id, onSuccess:this.onload.bind(this)}); // TODO: handle AJAX errors here
		},

		onload: function(t, json) {
			jslog.debug(t.getResponseHeader("X-JSON"));
  		jslog.debug(t.responseText);
			populate_controls(this.form, json);
			this.wnd.hideloader();
		},

		onsubmit: function() {
			this.onok();
			return false;
		},

		onsave: function(t) {
			jslog.debug(t.responseText);
			update_users();
			this.wnd.hide();
		},
		
		onok: function() {
			this.wnd.showloader();
			new Ajax.Request('webcasting.php', {parameters:'action=send_email&id='+this.user_id+"&"+Form.serialize(this.form), onSuccess:this.onsave.bind(this)}); // TODO: handle AJAX errors here
		}
	}

	function update_users()
	{
{/literal}
		new Ajax.Updater('usersdiv', 'webcasting_users.php', {ldelim}parameters: "action=list&id={$webcasting_id}"});  // add AJAX error handling here
{literal}
	}	

	function delete_user(user_id)
	{
		if(confirm("Are you sure you want to delete user?")) {
			new Ajax.Request('webcasting_users.php', {parameters:'action=delete&id='+user_id, onSuccess:user_deleted}); // TODO: handle AJAX errors here
		}
	}
	function user_deleted(t)
	{
		update_users();
	}
	
	update_users();
</script>	
{/literal}

{include file="adm_footer.tpl"}
	