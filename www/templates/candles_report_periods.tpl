{include file="adm_header.tpl" menu="1"}

<h1>Candle reports :: Compare periods</h1>
<div class="submenu"><a href="candles_report_sites.php">Compare domains</a> | <a href="candles_report_periods.php">Compare periods</a></div>
<div class="submenu">Default detalization: <a href="#" onclick="update_period('day'); return false;">day</a> <a href="#" onclick="update_period('week'); return false;">week</a> <a href="#" onclick="update_period('month'); return false;">month</a> <a href="#" onclick="update_period('year'); return false;">year</a></div>
<div>
  <form id="filters">
    <div class="controls">
    <label for="domain_id">Domain:</label> <select id="domain_id" name="domain_id">{html_options options=$domains}</select> <input type="button" value="Add / Reset" onclick="update_chart()">
    </div>
    <div class="controls">
    <label>Date:</label> <input type="text" id="date" name="date" size="8" value="{$date}"> <input type="button" id="date_b" value=" .. "> <input type="button" value="Add day" onclick="update_date('day')"> <input type="button" value="Add week" onclick="update_date('week')"> <input type="button" value="Add month" onclick="update_date('month')"> <input type="button" value="Add year" onclick="update_date('year')"> | <input type="button" value="Reset periods" onclick="reset_chart()">
    </div>
  </form>
</div>

<div id="report">
  {$ofc_object}
</div>

{literal}
<script>	
<!--  
function update_period(period) {
  var chart = $("chart");
  if(!chart) chart = $("ie_chart");
  chart.reload("candles_report_periods.php?action=draw&date=reset&period="+period);
}
function update_date(period) {
  var chart = $("chart");
  if(!chart) chart = $("ie_chart");
  chart.reload("candles_report_periods.php?action=draw&period="+period+"&date="+$("date").value);
}
function update_chart() {
  var chart = $("chart");
  if(!chart) chart = $("ie_chart");
  chart.reload("candles_report_periods.php?action=draw&domain="+$("domain_id").value);
}
function reset_chart() {
  var chart = $("chart");
  if(!chart) chart = $("ie_chart");
  chart.reload("candles_report_periods.php?action=draw&date=reset");
}
function remove_period(period_id) {
  var chart = $("chart");
  if(!chart) chart = $("ie_chart");
  chart.reload("candles_report_periods.php?action=draw&rperiod="+period_id);
}

Calendar.setup( {
	inputField : "date",
	ifFormat : "%Y-%m-%d",
	button : "date_b",
	step: 1
} );
-->
</script>	
{/literal}

{include file="adm_footer.tpl"}
