{literal}
<script>
	var ContextMenu = Class.create();

	ContextMenu.prototype = {

	   initialize: function(actions) {
	   		this.menu = Builder.node('div', {className:"popupmenu", id:"popupmenu"});
	   		this.actions = actions;	   		
	   },

	   popup: function(x, y, param) {
	   	   	var self = this;
	   	   	var menu = this.menu;
	   	   	this.param = param;
	   	   	menu.innerHTML = "";
	   	   	menu.style.left = x;
	   	   	menu.style.top = y;
	   	   	this.actions.each(function(action, i) {
	   	   			var a;
	   				if(action[0]!="-") {
		   				menu.appendChild(Builder.node('div', {className:"menuitem"}, [a = Builder.node('a', {href: "#", handler_id: i}, action[0])] ));
		   				a.onclick = self.click.bind(self);
	   				}
	   				else {
		   				menu.appendChild(Builder.node('div', {className:"menuitem"}, [Builder.node('hr', {size:1, style: "margin: 5px 5px"})] ));
	   				}
	   			}
	   		);
	   		document.body.appendChild(menu);
	   },

	   click: function(e) {
	   		this.hide();
			if (!e) var e = window.event;
			var tg = (e.target) ? e.target : e.srcElement;
			// call handler
			var callback = eval(this.actions[tg.getAttribute("handler_id")][1]);
			callback(this.param);
			return false;
	   },

	   hide: function() {
	   		if(this.menu.innerHTML) {
				document.body.removeChild(this.menu);
				this.menu.innerHTML = "";
			}
	   }
	}
</script>
{/literal}