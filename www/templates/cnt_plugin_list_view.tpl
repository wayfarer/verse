{section name=node loop=$data}
    <h1 class="list_item_header">{$data[node].name}</h1>
    <div class="list_item_content">
        {$data[node][$content_field]}
    </div>
    <br><br>
{/section}
{if isset($moreheaders)}
  {if count($moreheaders)}
	  <h3>{$moreheaderstitle|default:"See also"}</h3>
	  {foreach from=$moreheaders item=currheader}
	    <a href="/{$p}/{$currheader.list_id}">{$currheader.name}</a><br/>
	  {/foreach}
	  {if isset($moreheaderslink)}
	    <br/><a href="/{$p}/all">more >></a>
	  {/if}
  {/if}
{else}
  {if $p != 'off'}
    <a href="/{$p}">&laquo; back</a>
  {/if}
{/if}