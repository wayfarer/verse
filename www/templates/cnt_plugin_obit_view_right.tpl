{if !$smarty.get.print}
{if $scrapbookhere}
<script>
{literal}
function openwin(url, width, height) {
  var width = width || 500;
  var height = height || 500;
  var left = (screen.width-width)/2;
  var top = (screen.height-height)/2;
  win=window.open(url, null, config="scrollbars=no,resizable=no,toolbar=no,location=no,menubar=no,width="+width+",height="+height+",top="+top+",left="+left+"");
  win.focus();
}
{/literal}
</script>
<div class="noprint" style="font-size:1.2em;font-weight:bold"><a href="#" onclick="openwin('/{$scrapbookpage}/{$config.obituary_id}'); return false;">View photo tribute</a></div>
{/if}
{literal}
<style>
label {
	margin-bottom: 0;
}
</style>
{/literal}
{/if}
{$obit_text}
