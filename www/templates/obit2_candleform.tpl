{literal}
<form method="post"><input type="hidden" name="candle" value="save">
<style>
label {
	margin-right: 3px;
}
div.memo {
	padding-bottom: 10px;
}
div.warning {
	text-align: center;
	font-size: 0.7em;
}
div.error {
	color: #F00000;
	margin-bottom: 10px;
}
#captcha {
	margin-top: 13px;
}
div.captcha {
	margin: 5px 0;
}
</style>
{/literal}

<div class="lightacandleform">
<h2>Light a candle</h2>
<div class="splitter"></div>
{if $errors}
<div class="error">
{foreach from=$errors item=error}
{$error}<br>
{/foreach}
</div>
{/if}
<label for="name">Name:</label><input type="text" name="name" id="name" value="{$input.name|default:""}"><br>
<label for="email">Email:</label><input type="text" name="email" id="email" value="{$input.email|default:""}"><br>
<label for="thoughts">Memories:</label><textarea name="thoughts" id="thoughts" cols="30" rows="10">{$input.thoughts|default:""}</textarea><br>
{if $config.captcha}
<label for="captcha">Please enter the letters from below. <b>All letters are capital letters</b>:</label><input type="text" name="captcha" id="captcha"><br>
<div class="captcha"><img src="/captcha.php"></div>
{/if}
<center><input type="submit" onclick="this.disabled=true;this.form.submit();" value="{if $smarty.session.domain_id neq 154}Light a candle{else}Send{/if}"></center>
</div>
</form>
