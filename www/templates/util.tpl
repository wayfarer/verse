<script>
{literal}
	var sfb = null;
	var ServerFileBrowser = Class.create();
	ServerFileBrowser.prototype = {
		// use FCKEditor's server browser for now
		initialize: function(callback, initfolder, only_type) {
			this.onSetUrl = callback;
      this.initfolder = initfolder;
			this.only_type = only_type;
		},

		open: function() {
		  var finder = new CKFinder() ;
		  // The path for the installation of CKFinder (default = "/ckfinder/").
		  finder.BasePath = '/ckfinder/' ;
		  
		  //Startup path in a form: "Type:/path/to/directory/"
		  finder.StartupPath = this.initfolder;
			if(this.only_type) finder.type = this.only_type;
		  
		  // Name of a function which is called when a file is selected in CKFinder.
			finder.SelectFunction = SetURL;
		  
		  // Additional data to be passed to the SelectFunction in a second argument.
		  // We'll use this feature to pass the Id of a field that will be updated.
		  // finder.SelectFunctionData = functionData ;
		  
		  // Name of a function which is called when a thumbnail is selected in CKFinder.
		  // finder.SelectThumbnailFunction = ShowThumbnails ;
		
		  // ugly global var
			// no way found to pass class' function as callback
			sfb = this;
		
		  // Launch CKFinder
		  finder.Popup() ;
    }
	}
		
  function SetURL(url) {
    if(sfb) {
      url = decodeURI(url);
		  sfb.onSetUrl(url);
		}
	}	
        
    function textarea_tab(evt) {
	    // Set desired tab- defaults to four space softtab
	    var tab = "    ";

		if (!evt) var evt = window.event;
		var t = (evt.target) ? evt.target : evt.srcElement;
    	
        var ss = t.selectionStart;
        var se = t.selectionEnd;

		if (document.selection) {
			var range = document.selection.createRange();
	        if (evt.keyCode == 9) {
				if (range.parentElement() == t) {
					var isCollapsed = range.text == '';
					range.text = tab;

					 if (!isCollapsed)  {
						range.moveStart('character', -tab.length);
						range.select();
					}
				}
				return false;
			}
			// calculate selection positions for IE
			// We'll use this as a 'dummy'
			var stored_range = range.duplicate();
			// Select all text
			stored_range.moveToElementText(t);
			// Now move 'dummy' end point to end point of original range
			stored_range.setEndPoint( 'EndToEnd', range );
			// Now we can calculate start and end points
			var ss = stored_range.text.length - range.text.length;
			var se = ss + range.text.length;
		}

        // Tab key - insert tab expansion
        if (evt.keyCode == 9) {
            evt.preventDefault();
            
            // Special case of multi line selection
            if (ss != se && t.value.slice(ss,se).indexOf("\n") != -1) {
                // In case selection was not of entire lines (e.g. selection begins in the middle of a line)
                // we ought to tab at the beginning as well as at the start of every following line.
                var pre = t.value.slice(0,ss);
                var sel = t.value.slice(ss,se).replace(/\n/g,"\n"+tab);
                var post = t.value.slice(se,t.value.length);
                t.value = pre.concat(tab).concat(sel).concat(post);
                
                t.selectionStart = ss + tab.length;
                t.selectionEnd = se + tab.length;
            }
            
            // "Normal" case (no selection or selection on one line only)
            else {
                t.value = t.value.slice(0,ss).concat(tab).concat(t.value.slice(ss,t.value.length));
                if (ss == se) {
                    t.selectionStart = t.selectionEnd = ss + tab.length;
                }
                else {
                    t.selectionStart = ss + tab.length;
                    t.selectionEnd = se + tab.length;
                }
            }
        }
        
        // Backspace key - delete preceding tab expansion, if exists
        else if (evt.keyCode==8 && t.value.slice(ss - tab.length,ss) == tab) {
        	if(!range) {
	            evt.preventDefault();
                t.value = t.value.slice(0,ss - tab.length).concat(t.value.slice(ss,t.value.length));
    	        t.selectionStart = t.selectionEnd = ss - tab.length;
    	    }
    	    else {
				range.moveStart('character', -tab.length);
				range.moveEnd('character', 0);
				range.select();
    	    }
        }
        
        // Delete key - delete following tab expansion, if exists
//        else if (evt.keyCode==46 && t.value.slice(se,se + tab.length) == tab) {
//            evt.preventDefault();
            
//            t.value = t.value.slice(0,ss).concat(t.value.slice(ss + tab.length,t.value.length));
//            t.selectionStart = t.selectionEnd = ss;
//        } 

        
        // Left/right arrow keys - move across the tab in one go
        else if (evt.keyCode == 37 && t.value.slice(ss - tab.length,ss) == tab) {
            if(!range) {
	            evt.preventDefault();
                t.selectionStart = t.selectionEnd = ss - tab.length;
            }
			else {
				range.moveStart('character', -tab.length);
				range.select();
			}
        }
        else if (evt.keyCode == 39 && t.value.slice(ss,ss + tab.length) == tab) {
            if(!range) {
	            evt.preventDefault();
	            t.selectionStart = t.selectionEnd = ss + tab.length;
	        }
			else {
				range.moveStart('character', tab.length-1);
				range.select();
			}
        }
        
    }
{/literal}
</script>