{include file="adm_header.tpl" menu="1"}
{include file="window.tpl"}
{include file="contextmenu.tpl"}
{include file="util.tpl"}

<h1>Products :: My list</h1>
<div class="submenu"><a href="sublists.php">My List</a> | <a href="sublists_master.php">Master List</a></div>
<div class="info">Your List URL: <a href="http://chinesecasketdistributor.com/?p=mylist&uid={$hash}" target="_blank">http://chinesecasketdistributor.com/?p=mylist&uid={$hash}</a></div>
<a href="sublists.php?print=1" target="_blank"><img src="img/printer.png" align="left"></a> print price list<br>
<div id="products"></div>
<script type="text/javascript">
{include file="populate_controls.tpl"}
{literal}
function checkbox_all(c) {
	var cb = Form.getInputs(c.form, "checkbox");
	cb.each(function(el) {
		el.checked = c.checked;
	});
} 

	var ProductEditor = Class.create();
	ProductEditor.prototype = {
		initialize: function(product_id) {
			// create window
//			this.wnd = new WindowClass("Product Price", 500, 290, [this.onok.bind(this), this.oncancel.bind(this)]);
			this.product_id = product_id;
			// create form
//			this.form = Builder.node("FORM");
			// create dialog fields in form
{/literal}
{capture name=product_categories}
	{html_options options=$product_categories}
{/capture}
			var f = '\
					<form>\
					<label>Title</label><input type="text" name="name" class="text" disabled><br>\
					<label for="price">Price</label><input type="text" id="price" name="price" class="price"><br>\
					</form>\
				';
{literal}
			this.wnd = Dialog.confirm(f, {windowParameters: {className:"alphacube", width:520, title:"Product", showEffect: Element.show, hideEffect: Element.hide},
									      ok: this.onok.bind(this) });
			this.form = this.wnd.getContent().getElementsByTagName("FORM")[0];
			this.form.onsubmit = this.onsubmit.bind(this);

			Form.focusFirstElement(this.form);

			if(product_id) {
				// load data
				this.json = null;
				new Ajax.Request('ajax_sublists.php', {parameters:'action=load&id='+product_id, onSuccess:this.onload.bind(this)}); // TODO: handle AJAX errors here
			}
			else {
				// defaults
				this.wnd.hideloader();
			}
		},

		onload: function(t, json) {
			jslog.debug(t.getResponseHeader("X-JSON"));
			populate_controls(this.form, json);
			this.wnd.hideloader();
		},

		onsubmit: function() {
			this.onok();
			return false;
		},

		onsave: function(t) {
			jslog.debug(t.responseText);
			update_products();
			this.wnd.hide();
		},
		
		onok: function() {
			this.wnd.showloader();
			new Ajax.Request('ajax_sublists.php', {parameters:'action=save&id='+this.product_id+"&"+Form.serialize(this.form), onSuccess:this.onsave.bind(this)}); // TODO: handle AJAX errors here
		}
	}

	function success(t)
	{
		update_products();
	}
	
	function remove_item(product_id)
	{
		if(confirm("Are you sure you want to remove product from your list?")) {
			new Ajax.Request('ajax_sublists.php', {parameters:'action=remove_mylist&id='+product_id, onSuccess:success}); // TODO: handle AJAX errors here
		}
	}
	
	function page(start)
	{
		new Ajax.Updater('products', 'ajax_sublists.php', {parameters: "action=list&start="+start});  // add AJAX error handling here
	}
	function order(field)
	{
		new Ajax.Updater('products', 'ajax_sublists.php', {parameters: "action=list&order="+field});  // add AJAX error handling here
	}
	function update_products()
	{
		new Ajax.Updater('products', 'ajax_sublists.php', {parameters: "action=list"});  // add AJAX error handling here
	}	
	update_products();
</script>	
{/literal}

{include file="adm_footer.tpl"}
