{include file="adm_header.tpl" menu="1"}

<h1>Production domains MX records</h1>
<div class="submenu"><a href="domains.php">&laquo; back to domain list</a></div>
<div id="mx"><img src="img/loader-big.gif"></div>
<script type="text/javascript">
	function page(start) {ldelim}
		new Ajax.Updater('mx', 'mx.php', {ldelim}parameters: "action=list&start="+start}); 
	}
	function order(field) {ldelim}
		new Ajax.Updater('mx', 'mx.php', {ldelim}parameters: "action=list&order="+field}); 
	}
	function update_mx() {ldelim}
		new Ajax.Updater('mx', 'mx.php', {ldelim}parameters: "action=list"});
	}	
	update_mx();
</script>	

{include file="adm_footer.tpl"}
