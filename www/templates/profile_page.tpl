<style>
  label {ldelim}
    width: 75px;
    margin-right: 10px;
  }
</style>
<div class="member-profile">
<h1>Member profile</h1>
<label>Name:</label> {$user.name}<br><br>
<label>Username:</label> {$user.login}<br><br>
<label>E-mail:</label> {$user.email}<br><br>
<a href="/{$smarty.get.p}?action=edit">Edit profile</a>
{if $profile_ref} 
 :: <a href="{$profile_ref}">Return to content</a>
{/if}
</div>