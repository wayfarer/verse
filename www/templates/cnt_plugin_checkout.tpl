<script type="text/javascript" src="js/prototype.js"></script>
<style>
div.error {ldelim}
	color: #F00000;
	margin-bottom: 10px;
}
label {ldelim}
	margin-right: 5px;
}
</style>
<h1>Checkout</h1>
{if !$thanks}
<table width="100%">
<tr><th>product</th><th>price</th><th>quantity</th><th>amount</th></tr>
{foreach from=$cart key=pid item=product}
	<tr align="center"><td>{$product.name}</td><td>${$product.price}</td><td>{$product.count}</td><td><b>${$product.amount}</b></td></tr>
{/foreach}
<tr><td></td><td></td><td align="right"><b>Total:</b></td><td align="center"><b>${$total}</b></td></tr>
</table>
{if $errors}
<div class="error">
{foreach from=$errors item=error}
{$error}<br>
{/foreach}
</div>
{/if}
<div style="margin: 10px 0; font-weight: bold">
Please fill out the form below to make your order. We will contact you regarding payment shortly. Thank you!
</div>
<form method="post">
<fieldset>
<legend>Personal info</legend>
<label for="email">E-mail<small>*</small></label><input id="email" type="text" name="email" value="{$input.email|default:""}"><br>
<label for="first">First Name<small>*</small></label><input id="first" type="text" name="firstname" value="{$input.firstname|default:""}"><br>
<label for="last">Last Name<small>*</small></label><input id="last" type="text" name="lastname" value="{$input.lastname|default:""}"><br>
<label for="company">Company</label><input id="company" type="text" name="company" value="{$input.company|default:""}"><br>
<label for="phone">Phone<small>*</small></label><input id="phone" type="text" name="phone" value="{$input.phone|default:""}"><br>
<label for="address1">Address 1</label><input id="address1" type="text" name="address1" value="{$input.address1|default:""}"><br>
<label for="address2">Address 2</label><input id="address2" type="text" name="address2" value="{$input.address2|default:""}"><br>
<label for="city">City</label><input id="city" type="text" name="city" value="{$input.city|default:""}"><br>
<label for="state">State/Province</label><input id="state" type="text" name="state" value="{$input.state|default:""}"><br>
<label for="zip">ZIP</label><input id="zip" type="text" name="zip" value="{$input.zip|default:""}"><br>
<label for="country">Country</label><input id="country" type="text" name="country" value="{$input.country|default:""}">
</fieldset>

<!-- <fieldset>
<legend>Delivery info</legend>
<small><font color="#CC0000">Skip if same as billing info</font><br><br></small>
<label for="first">First Name</label><input id="first" type="text" name="firstname"><br>
<label for="last">Last Name</label><input id="last" type="text" name="lastname"><br>
<label for="company">Company (opt)</label><input id="company" type="text" name="company"><br>
<label for="phone">Phone</label><input id="phone" type="text" name="phone"><br>
<label for="address1">Address 1</label><input id="address1" type="text" name="address1"><br>
<label for="address2">Address 2 (opt)</label><input id="address2" type="text" name="address2"><br>
<label for="city">City</label><input id="city" type="text" name="city"><br>
<label for="state">State/Province</label><input id="state" type="text" name="state"><br>
<label for="zip">ZIP</label><input id="zip" type="text" name="zip"><br>
<label for="country">Country</label><input id="country" type="text" name="country">
</fieldset> -->
<br>
<center>
<input type="submit" value="submit">
<input type="button" value="review cart" onclick="document.location='?p=cart'">
</center>
<br>
</form>
{else}
<h3>Success!</h3>
Your order has been placed. Thank you. We will contact you soon.
<br><br>
<!-- <a href="?p=products">&laquo; back</a> -->
{/if}
