<script type="text/javascript" src="/js/prototype.js"></script>
<script type="text/javascript">
{literal}
    function update_florist_cart_count() {
        new Ajax.Updater('cart', '/ajax_florist_cart.php', {parameters: "action=cart_show"});
    }

    function florist_cart(product_code) {
        $("cart").innerHTML = "loading...";
        var elem = $("p"+product_code),
            cart_count = Math.abs(parseInt($("cart-count-"+product_code).value));
        new Ajax.Updater('cart', '/ajax_florist_cart.php', {parameters: "action=cart_add&product_code="+product_code+"&cart_count="+cart_count});
        if(elem) {
            var count = parseInt(elem.getAttribute("count"));
            if(count==0) {
                $("r"+product_code).style.display = "block";
            }
            count+=cart_count;
            elem.setAttribute("count", count);
            elem.innerHTML = count+" item(s) in cart";
        }
    }

    function florist_cart_remove(product_code) {
        $("cart").innerHTML = "loading...";
        var elem = $("p"+product_code),
            cart_count = Math.abs(parseInt($("cart-count-"+product_code).value));
        new Ajax.Updater('cart', '/ajax_florist_cart.php', {parameters: "action=cart_remove_inplace&product_code="+product_code+"&cart_count="+cart_count});
        if(elem) {
            var count = elem.getAttribute("count");
            if(count>0) {
                count-=cart_count;
                elem.setAttribute("count", count);
                if(count>0) {
                    elem.innerHTML = count+" item(s) in cart";
                }
                else {
                    $("r"+product_code).style.display = "none";
                    elem.innerHTML = "";
                }
            }
        }
    }

    update_florist_cart_count();
{/literal}
</script>

<div class="cart" style="text-align: right">
    shopping cart (
    <a href="/flowercart">review</a>
    ):
    <span id="cart"></span>
</div>