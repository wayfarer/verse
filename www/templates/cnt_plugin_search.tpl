<form action="{$params.url}" id="cse-search-box">
  <div>
    <input type="hidden" name="cx" value="{$params.key}" />
    <input type="hidden" name="cof" value="FORID:10" />
    <input type="hidden" name="ie" value="UTF-8" />
    <input type="text" name="q" size="{$params.size}" />
    <input type="submit" name="sa" value="{$params.button_text}" />
  </div>
</form>