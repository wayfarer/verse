<?php
 // obsolete
// backend integration with PIWIK
include('inc/verse.inc.php'); //main header - initializes Verse environment

$piwik_url = "https://twintierstech.net/piwik/";
$piwik_login_tpl = "module=VerseIntegration&action=logme_token&login=%s&token_auth=%s";
if ($user->have_role(ROLE_STATISTICS_MANAGEMENT)) {
    if (!$user->is_super()) {
        // count stats visits
        $query = "UPDATE sms_stats_visits SET visits=visits+1, last_visit=now() WHERE domain_id='$domain_id' AND stat_system='piwik'";
        $db->query($query);
        if (!$db->affectedRows()) {
            $query = "INSERT sms_stats_visits SET domain_id='$domain_id', stat_system='piwik', visits=1, last_visit=now()";
            $db->query($query);
        }
    }

    $login = $user->data['login'] . '.' . $domain_name;
    $piwik_token_auth = $user->data['piwik_token_auth'];
    $piwik_login = sprintf($piwik_login_tpl, $login, $piwik_token_auth);
    header("Location: $piwik_url?$piwik_login");
}
else {
    header('Location: login.php');
}
