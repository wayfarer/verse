<?php
 // generates captcha image and stores the key in the session
// used for candles
include("inc/verse.inc.php"); //main header - initializes Verse environment

captchaimage();

function captchaimage() {

    // start session before!

    // ##### IMAGE SETTINGS
    $width = 200;
    $height = 100;


    // ##### SET UP IMAGE AND COLORS
    $image = imagecreatetruecolor($width, $height);
    imagesetthickness($image, 1);
    imagealphablending($image, true);
    $color_black = imagecolorallocatealpha($image, 0, 0, 0, 0);
    $color_black_semi = imagecolorallocatealpha($image, 0, 0, 0, 115);
    $color_white = imagecolorallocatealpha($image, 255, 255, 255, 0);
    imagefill($image, 0, 0, $color_white);
    imagecolortransparent($image, $color_white);


    // ##### BUILD RANDOM PASSWORD
    $acceptedCharsV = "AEIOUY";
    $acceptedCharsC = "BCDFGHJKLMNPQRSTVWXZ";
    $wordbuild = array(
        "cvcc", "ccvc", "ccvcc", "cvccc", // monosyllabic nominal stems
        "cvcvc", "cvcv", "cvccv", "ccvcv" // disyllabic nominal stems
    );
    $thisword = $wordbuild[mt_rand(0, sizeof($wordbuild) - 1)];
    $stringlength = strlen($thisword);
    for ($i = 0; $i < $stringlength; $i++) {
        if ($thisword[$i] == "c") {
            $password .= $acceptedCharsC{mt_rand(0, strlen($acceptedCharsC) - 1)};
        }
        if ($thisword[$i] == "v") {
            $password .= $acceptedCharsV{mt_rand(0, strlen($acceptedCharsV) - 1)};
        }
    }


    // ##### DRAW RANDOM LETTERS
    for ($i = 0; $i < 50; $i++) {
        $color = imagecolorallocatealpha($image, mt_rand(0, 255), mt_rand(0, 255), mt_rand(0, 255), 110);
        imagestring($image, mt_rand(1, 3), mt_rand(-$width * 0.25, $width * 1.25), mt_rand(-$height * 0.25, $height * 1.25),
                    $acceptedCharsC{mt_rand(0, strlen($acceptedCharsC) - 1)}, $color);
        imagestring($image, mt_rand(1, 3), mt_rand(-$width * 0.25, $width * 1.25), mt_rand(-$height * 0.25, $height * 1.25),
                    $acceptedCharsV{mt_rand(0, strlen($acceptedCharsV) - 1)}, $color);
    }


    // ##### DRAW PASSWORD
    for ($i = 0; $i < $stringlength; $i++) {
        $buffer = imagecreatetruecolor(40, 40);
        imagefill($buffer, 0, 0, $color_white);
        imagecolortransparent($buffer, $color_white);

        $buffer2 = imagecreatetruecolor(40, 40);
        imagefill($buffer2, 0, 0, $color_white);
        imagecolortransparent($buffer2, $color_white);

        $red = 0;
        $green = 0;
        $blue = 0;
        while ($red + $green + $blue < 400 || $red + $green + $blue > 450) {
            $red = mt_rand(0, 255);
            $green = mt_rand(0, 255);
            $blue = mt_rand(0, 255);
        }

        $color = imagecolorallocate($buffer, $red, $green, $blue);
        imagestring($buffer, 2, 0, 0, substr($password, $i, 1), $color);

        imagecopyresized($buffer2, $buffer, 2, -5, 0, 0, mt_rand(30, 40), mt_rand(30, 40), 10, 14);
        //  $buffer=imagerotate($buffer2,mt_rand(-25,25),$color_white);
        $buffer = imageRotateBicubic($buffer2, mt_rand(-25, 25));

        $xpos = $i / $stringlength * ($width - 30) + (($width - 30) / $stringlength / 2) + 5 + mt_rand(-8, 8);
        $ypos = (($height - 50) / 2) + 5 + mt_rand(-8, 8);

        imagecolortransparent($buffer, $color_white);

        imagecopymerge($image, $buffer, $xpos, $ypos, 0, 0, imagesx($buffer), imagesy($buffer), 100);
        imagedestroy($buffer);
        imagedestroy($buffer2);
    }


    // ##### DRAW ELLIPSES
    for ($i = 0; $i < 12; $i++) {
        $color = imagecolorallocatealpha($image, mt_rand(0, 200), mt_rand(0, 200), mt_rand(0, 200), 110);
        imagefilledellipse($image, mt_rand(0, $width), mt_rand(0, $height), mt_rand(10, 40), mt_rand(10, 40), $color);
    }


    // ##### DRAW LINES
    for ($i = 0; $i < 12; $i++) {
        $color = imagecolorallocatealpha($image, mt_rand(0, 200), mt_rand(0, 200), mt_rand(0, 200), 110);
        imagesetthickness($image, mt_rand(8, 20));
        imageline($image, mt_rand(-$width * 0.25, $width * 1.25), mt_rand(-$height * 0.25, $height * 1.25),
                  mt_rand(-$width * 0.25, $width * 1.25), mt_rand(-$height * 0.25, $height * 1.25), $color);
        imagesetthickness($image, 1);
    }

    /*
     // ##### WOBBLE HORIZONTALLY
     $sindivide=mt_rand(1,20);
     $sinwidth=mt_rand(1,$sindivide)/4;
     for ($i=0;$i<$height;$i++) {
      $buffer=imagecreatetruecolor($width,1);
      imagecopy($buffer,$image,0,0,0,$i,$width,1);
      imageline($image,0,$i,$width,$i,$color_white);
      imagecopy($image,$buffer,(sin($i/$sindivide)-.5)*2*$sinwidth,$i,0,0,$width,1);
      imagedestroy($buffer);
     }


     // ##### WOBBLE VERTICALLY
     $sindivide=mt_rand(1,20);
     $sinwidth=mt_rand(1,$sindivide)/4;
     for ($i=0;$i<$width;$i++) {
      $buffer=imagecreatetruecolor(1,$height);
      imagecopy($buffer,$image,0,0,$i,0,1,$height);
      imageline($image,$i,0,$i,$width,$color_white);
      imagecopy($image,$buffer,$i,(sin($i/$sindivide)-.5)*2*$sinwidth,0,0,1,$height);
      imagedestroy($buffer);
     }
    */

    // ##### GRADIENT BACKGROUND HORIZONTALLY
    $red_from = mt_rand(0, 255);
    $red_to = mt_rand(0, 255);
    $green_from = mt_rand(0, 255);
    $green_to = mt_rand(0, 255);
    $blue_from = mt_rand(0, 255);
    $blue_to = mt_rand(0, 255);

    for ($i = 0; $i < $height; $i++) {
        $color = imagecolorallocatealpha($image, $red_from + ($red_to - $red_from) / $height * $i, $green_from + ($green_to - $green_from) / $height * $i, $blue_from + ($blue_to - $blue_from) / $height * $i, 100);
        imageline($image, 0, $i, $width, $i, $color);
    }


    // ##### GRADIENT BACKGROUND VERTICALLY
    $red_from = mt_rand(0, 255);
    $red_to = mt_rand(0, 255);
    $green_from = mt_rand(0, 255);
    $green_to = mt_rand(0, 255);
    $blue_from = mt_rand(0, 255);
    $blue_to = mt_rand(0, 255);

    for ($i = 0; $i < $width; $i++) {
        $color = imagecolorallocatealpha($image, $red_from + ($red_to - $red_from) / $width * $i, $green_from + ($green_to - $green_from) / $width * $i, $blue_from + ($blue_to - $blue_from) / $width * $i, 100);
        imageline($image, $i, 0, $i, $height, $color);
    }


    // ##### TAG
    $color = imagecolorallocatealpha($image, 255, 255, 255, 90);
    imagefilledrectangle($image, 1, 1, 146, 8, $color);
    $color = imagecolorallocatealpha($image, 0, 0, 0, 100);
    // imagestring($image,1,2,1,"jhfCAPTCHA: find ".strlen($password)." characters",$color);

    // ##### STORE PASSWORD
    $_SESSION["captcha"] = $password;

    // ##### OUTPUT
    header('Content-Type: image/png');
    imagepng($image);
    imagedestroy($image);
}

function imageRotateBicubic($src_img, $angle, $bicubic = false) {

    // convert degrees to radians
    $angle = $angle + 180;
    $angle = deg2rad($angle);

    $src_x = imagesx($src_img);
    $src_y = imagesy($src_img);

    $center_x = floor($src_x / 2);
    $center_y = floor($src_y / 2);

    $rotate = imagecreatetruecolor($src_x, $src_y);
    imagealphablending($rotate, false);
    imagesavealpha($rotate, true);

    $cosangle = cos($angle);
    $sinangle = sin($angle);

    for ($y = 0; $y < $src_y; $y++) {
        for ($x = 0; $x < $src_x; $x++) {
            // rotate...
            $old_x = (($center_x - $x) * $cosangle + ($center_y - $y) * $sinangle)
                     + $center_x;
            $old_y = (($center_y - $y) * $cosangle - ($center_x - $x) * $sinangle)
                     + $center_y;

            if ($old_x >= 0 && $old_x < $src_x
                && $old_y >= 0 && $old_y < $src_y
            ) {
                if ($bicubic == true) {
                    $sY = $old_y + 1;
                    $siY = $old_y;
                    $siY2 = $old_y - 1;
                    $sX = $old_x + 1;
                    $siX = $old_x;
                    $siX2 = $old_x - 1;

                    $c1 = imagecolorsforindex($src_img, imagecolorat($src_img, $siX, $siY2));
                    $c2 = imagecolorsforindex($src_img, imagecolorat($src_img, $siX, $siY));
                    $c3 = imagecolorsforindex($src_img, imagecolorat($src_img, $siX2, $siY2));
                    $c4 = imagecolorsforindex($src_img, imagecolorat($src_img, $siX2, $siY));

                    $r = ($c1['red'] + $c2['red'] + $c3['red'] + $c4['red']) << 14;
                    $g = ($c1['green'] + $c2['green'] + $c3['green'] + $c4['green']) << 6;
                    $b = ($c1['blue'] + $c2['blue'] + $c3['blue'] + $c4['blue']) >> 2;
                    $a = ($c1['alpha'] + $c2['alpha'] + $c3['alpha'] + $c4['alpha']) >> 2;
                    $color = imagecolorallocatealpha($src_img, $r, $g, $b, $a);
                } else {
                    $color = imagecolorat($src_img, $old_x, $old_y);
                }
            } else {
                // this line sets the background colour
                $color = imagecolorallocatealpha($src_img, 255, 255, 255, 127);
            }
            imagesetpixel($rotate, $x, $y, $color);
        }
    }
    return $rotate;
}
