<?php
 // obsolete
include("inc/verse.inc.php"); //main header - initializes Verse environment
include("inc/mailbox.inc.php");

$logger = &Log::factory('file', 'logs/email_gate.log', 'versesms');
$logger->info("Call from " . $_SERVER["REMOTE_ADDR"]);

if (isset($_POST["action"]) && $_SERVER["REMOTE_ADDR"] == "66.135.56.26" && $_POST["action"] == "config") {
    // read config
    $data = unserialize($_POST["data"]);
    $logger->info("commands: " . $data["commands"]);
    // process commands first
    if ($data["commands"]) {
        $db_con = createobject("db_sasldb");
        $dbs = $db_con->connect();

        $commands = explode("\n", $data["commands"]);
        foreach ($commands as $commandline) {
            $command = explode("\t", $commandline);
            switch ($command[0]) {
                case "ADD":
                    // 1st - add username/pass to sasldb for authorization
                    $query = "INSERT accounts SET username='" . $command[1] . "', password='" . $command[2] . "'";
                    $dbs->query($query);
                    // 2nd - create IMAP mail box (cyrus)
                    $username = $command[1];
                    $h = fsockopen("localhost", 143);
                    fwrite($h, "VCMS LOGIN cyrus source\n");
                    fwrite($h, "VCMS CREATE user.$username\n");
                    fwrite($h, "VCMS SETACL user.$username cyrus lrswipcda\n");
                    fclose($h);
                    break;
                case "UPDATE":
                    $query = "UPDATE accounts SET password='" . $command[2] . "' WHERE username='" . $command[1] . "'";
                    $dbs->query($query);
                    break;
                case "REMOVE":
                    $query = "DELETE FROM accounts WHERE username = '" . $command[1] . "'";
                    $dbs->query($query);
                    // delete IMAP mail box (cyrus)
                    $username = $command[1];
                    $h = fsockopen("localhost", 143);
                    fwrite($h, "VCMS LOGIN cyrus source\n");
                    fwrite($h, "VCMS DELETE user.$username\n");
                    fclose($h);
                    break;
            }
        }
        $dbs->disconnect();
    }

    // write additions
    $additions = array();
    file_put_contents("../sendmail_config_remote/access", $data["access"]);
    file_put_contents("../sendmail_config_remote/aliases", $data["aliases"]);
    file_put_contents("../sendmail_config_remote/genericstable", $data["genericstable"]);
    file_put_contents("../sendmail_config_remote/local-host-names", $data["local-host-names"]);
    file_put_contents("../sendmail_config_remote/virtusertable", $data["virtusertable"]);

    generate_sendmail_files();

    echo "success";
}


function file_put_contents($n, $d) {
    $f = @fopen($n, "w");
    if (!$f) {
        return false;
    } else {
        fwrite($f, $d);
        fclose($f);
        return true;
    }
}
