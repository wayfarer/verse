<?php
 // MIMS integrations script
include("inc/verse.inc.php"); //main header - initializes Verse environment
include("inc/imageutil.inc.php");
include("inc/obituary.inc.php");

foreach (glob("files/$domain_name/exchange/*.xml") as $filename) {
    $logger->log("[MIMS] importing $filename", PEAR_LOG_INFO);

    $data = file_get_contents($filename);
    // remove symbols below 32, except 9 (tab), 10,13 (nl,br) - they are invalid for XML
    $remove = array();
    for ($i = 0; $i < 32; $i++) {
        if ($i <> 9 && $i <> 10 && $i <> 13) $remove[] = chr($i);
    }
    $data = str_replace($remove, " ", $data);

    // convert data to UTF-8 format, to support different non-ASCII symbols (for symbols above 122 (z) )
    $data = iconv("ISO-8859-1", "UTF-8", $data);

    $xo = simplexml_load_string($data, 'SimpleXMLElement', LIBXML_NOCDATA);

    import($xo, 'obituary');
    import($xo, 'scrapbook');
    import($xo, 'calendar');
    import($xo, 'memorial');
    import($xo, 'merchandise');
    import($xo, 'movieclip');

    $slash = strrpos($filename, "/");
    $bu_dir = substr($filename, 0, $slash) . "/backup";
    $bu_name = $bu_dir . "/" . substr($filename, $slash + 1);
    // ensure backup dir exists
    if (!file_exists($bu_dir)) {
        mkdir($bu_dir);
    }
    rename($filename, $bu_name);
}
// redirect browser to site's homepage
header("Location: index.php");


function import($xo, $type) {
    if (isset($xo->$type)) {
        foreach ($xo->$type as $entry) {
            call_user_func("process_$type", $entry);
        }
    }
}

function process_obituary($obituary) {
    global $db, $logger, $domain_id, $domain_name;

    $obit = array();
    $obituary_case_id = (string)$obituary["id"];
    $obit["obituary_case_id"] = $obituary_case_id;
    $obit["first_name"] = (string)$obituary->firstname;
    $obit["middle_name"] = (string)$obituary->middlename;
    $obit["last_name"] = (string)$obituary->lastname;
    $obit["title"] = (string)$obituary->title;
    $obit["suffix"] = (string)$obituary->suffix;
    $obit["home_place"] = str_replace("&apos;", "&#39;", (string)$obituary->home);
    $obit["death_date"] = (string)$obituary->deathdate;
    $obit["birth_place"] = str_replace("&apos;", "&#39;", (string)$obituary->birthplace);
    $obit["birth_date"] = (string)$obituary->birthdate;
    $obit["service_date"] = (string)$obituary->servicedate;
    $obit["service_time"] = (string)$obituary->servicetime;
    $obit["service_place"] = str_replace("&apos;", "&#39;", (string)$obituary->serviceplace);
    $obit['slug'] = get_obituary_slug($obit);

    $service_type = (string)$obituary->servicetype;
    if ($service_type) {
        $obit["service_type"] = $service_type;
    }

    $visitation_date1 = (string)$obituary->visitationdate1;
    $visitation_date2 = (string)$obituary->visitationdate2;
    $visitation_date3 = (string)$obituary->visitationdate3;
    $visitation_time1 = (string)$obituary->visitationtime1;
    $visitation_time2 = (string)$obituary->visitationtime2;
    $visitation_time3 = (string)$obituary->visitationtime3;
    $visitation_place1 = str_replace("&apos;", "&#39;", (string)$obituary->visitationplace1);
    $visitation_place2 = str_replace("&apos;", "&#39;", (string)$obituary->visitationplace2);
    $visitation_place3 = str_replace("&apos;", "&#39;", (string)$obituary->visitationplace3);
    $military_status = intval($obituary->militarystatus);
    $obit["final_disposition"] = str_replace("&apos;", "&#39;", (string)$obituary->finaldisposition);
    $obit["image"] = (string)$obituary->image;
    $obit["obit_text"] = str_replace("&apos;", "&#39;", trim($obituary->text));

    // process image
    $image = "files/$domain_name/image/" . $obit["image"];
    if (file_exists($image)) {
        resizejpg($image, 200); // resize to max width=200
    }

    if ($obit["death_date"]) {
        $obit["death_date"] = date("Y-m-d", strtotime($obit["death_date"]));
    }
    if ($obit["birth_date"]) {
        $obit["birth_date"] = date("Y-m-d", strtotime($obit["birth_date"]));
    }
    if ($obit["service_date"]) {
        $obit["service_date"] = strtotime($obit["service_date"]);
    }
    else {
        unset($obit["service_date"]);
    }

    if ($visitation_date1) {
        $obit["visitation_date"] .= strtotime($visitation_date1);
    }

    $obit["visitation_time"] = "";
    if ($visitation_time1) {
        $obit["visitation_time"] .= " " . $visitation_time1;
    }
    if ($visitation_place1) {
        $obit["visitation_time"] .= " at " . $visitation_place1;
    }

    if ($visitation_date2) {
        $obit["visitation_time"] .= "; " . date("l, F d, Y", strtotime($visitation_date2));
    }
    if ($visitation_time2) {
        $obit["visitation_time"] .= " " . $visitation_time2;
    }
    if ($visitation_place2) {
        $obit["visitation_time"] .= " at " . $visitation_place2;
    }

    if ($visitation_date3) {
        $obit["visitation_time"] .= "; " . date("l, F d, Y", strtotime($visitation_date3));
    }
    if ($visitation_time3) {
        $obit["visitation_time"] .= " " . $visitation_time3;
    }
    if ($visitation_place3) {
        $obit["visitation_time"] .= " at " . $visitation_place3;
    }

    // process URLs in obit text, convert them to links, if no <a tag, it means profi mode - manual links
    if (strpos($obit["obit_text"], "<a ") === false) {
        $obit["obit_text"] = preg_replace('/((\w+:\/\/)|(www\.))([^\s\)\<]+)/', ' <a href="http://\\3\\4" target="_blank">\\3\\4</a>', $obit["obit_text"]);
    }

    if ($obituary->finaldisposition_google_method && $obituary->finaldisposition_google_method == 'Address') {
        $obit['finaldisposition_google_method'] = $obituary->finaldisposition_google_method;
        if ($obituary->finaldisposition_google_addr) {
            $obit['finaldisposition_google_addr'] = urldecode($obituary->finaldisposition_google_addr);
        }
    }

    if ($obituary->service_google_method && $obituary->service_google_method == 'Address') {
        $obit['service_google_method'] = $obituary->service_google_method;
        if ($obituary->service_google_addr) {
            $obit['service_google_addr'] = urldecode($obituary->service_google_addr);
        }
    }

    // check if obituary exists
    $query = "SELECT obituary_id FROM plg_obituary WHERE obituary_case_id='$obituary_case_id' AND domain_id='$domain_id'";
    $obituary_id = $db->getOne($query);
    if ($obituary_id) {
        $query = "UPDATE plg_obituary SET " . make_set_clause_in($obit) . " WHERE obituary_id='$obituary_id' AND domain_id='$domain_id'";
        $ret = $db->query($query);
    }
    else {
        $obit["domain_id"] = $domain_id;
        $query = "INSERT plg_obituary SET " . make_set_clause_in($obit);
        $ret = $db->query($query);

        $obituary_id = $db->getOne("SELECT last_insert_id()");
        if ($obituary_id) {
            send_obituary_notification($obituary_id, $obit['slug'], $obit["first_name"] . " " . $obit["middle_name"] . " " . $obit["last_name"], $obit["birth_date"], $obit["death_date"]);

            $facebook_config = get_obituary_facebook_config($domain_id);
            if (!empty($facebook_config['facebook_user_access_token'])) {
                include_once($_SERVER['DOCUMENT_ROOT'] . '/../symfony/config/ProjectConfiguration.class.php');
                $configuration = ProjectConfiguration::getApplicationConfiguration('backend', 'prod', false);

                require_once("../symfony/lib/obituariesHelper.class.php");
                $facebook_message = "The online obituary for ".get_obituary_name($obit)." is available to view and leave online condolences. http://$domain_name/".get_obituary_view_page_name()."/$obituary_id";
                obituariesHelper::publishMessageOnFacebookWall($facebook_config['facebook_user_access_token'], @$facebook_config['facebook_page'], $facebook_message);
            }
        }
    }
    if ($obituary_id) {
        // insert or update military status
        $query = "REPLACE plg_obituary_config SET domain_id='$domain_id', obituary_id='$obituary_id', param='military_status', value='" . ($military_status != 0 ? 1 : 0) . "'";
        $db->query($query);
    }
    if (DB::isError($ret)) {
        $logger->log("[MIMS] DB error: case_id=" . $obituary_case_id . ": " . $ret->getMessage() . ";" . $ret->getUserInfo() . ";" . $query, PEAR_LOG_ERR);
    }
}

function process_scrapbook($scrapbook) {
    global $db, $logger, $domain_id, $domain_name;

    $obituary_case_id = (string)$scrapbook["id"];
    $query = "SELECT obituary_id FROM plg_obituary WHERE obituary_case_id='" . in($obituary_case_id) . "' AND domain_id='$domain_id'";
    $obituary_id = intval($db->getOne($query));
    $logger->log("[MIMS] importing scrapbook, case_id=$obituary_case_id; obituary_id=$obituary_id", PEAR_LOG_INFO);

    // clear scrapbook for obituary before import
    $query = "DELETE FROM plg_slide WHERE obituary_id='$obituary_id' AND domain_id='$domain_id'";
    $db->query($query);
    // save scrapbook entries
    foreach ($scrapbook->image as $image) {
        $image_file = (string)$image["file"];
        $image_file_withpath = "files/$domain_name/$image_file";
        $desc = trim($image);
        if (file_exists($image_file_withpath)) {
            resizejpg($image_file_withpath, 750); // resize to max width=750
        }
        $query = "INSERT plg_slide SET domain_id='$domain_id', obituary_id='$obituary_id', image='" . in($image_file) . "', description='" . in($desc) . "'";
        $db->query($query);
    }
}

function process_calendar($calendar) {
    global $db, $logger, $domain_id;

    $event_case_id = (string)$calendar["id"];
    $obituary_case_id = (string)$calendar["obit_id"];
    $event_type = (string)$calendar["type"];
    $time = (string)$calendar->time;
    $name = trim($calendar->name);
    $text = trim($calendar->text);

    if($time) {
        $time = strtotime($time);
    } else {
        $time = null;
    }

    if ($name) {
        $text = "<b>$name</b><br>" . $text;
    }

    // fetch obituary_id to associate with
    $obituary_id = NULL;
    if ($obituary_case_id) {
        $query = "SELECT obituary_id FROM plg_obituary WHERE obituary_case_id='" . in($obituary_case_id) . "' AND domain_id='$domain_id'";
        $obituary_id = intval($db->getOne($query));
    }

    $logger->log("[MIMS] importing calendar, event_case_id=$event_case_id, case_id=$obituary_case_id; obituary_id=$obituary_id", PEAR_LOG_INFO);

    // event_type = 3 - uploaded event
    $query = "REPLACE plg_event SET event_case_id='" . in($event_case_id) . "', domain_id='$domain_id', timestamp=$time, description='" . in($text) . "', event_type='3'";
    if ($obituary_id) {
        $query .= ", obituary_id='$obituary_id'";
    }
    $ret = $db->query($query);
    if (DB::isError($ret)) {
        $logger->log("[MIMS] DB error: event_case_id=" . $event_case_id . ": " . $ret->getMessage() . ";" . $ret->getUserInfo() . ";" . $query, PEAR_LOG_ERR);
    }
}

function process_memorial($memorial) {
    global $db, $logger, $domain_id;

    $p = array();
    $p["guid"] = (string)$memorial["id"];
    $company = (string)$memorial->company;
    $address = (string)$memorial->address;
    $address2 = (string)$memorial->address2;
    $city = (string)$memorial->city;
    $state = (string)$memorial->state;
    $zip = (string)$memorial->zip;
    $phone = (string)$memorial->phone;
    $fax = (string)$memorial->fax;
    $url = (string)$memorial->url;
    $email = (string)$memorial->email;
    $notes = (string)$memorial->notes;

    $p["name"] = $company;
    if ($address) {
        $p["description"] .= $address . "<br>";
    }
    if ($address2) {
        $p["description"] .= $address2 . "<br>";
    }
    if ($city) {
        $p["description"] .= $city;
    }
    if ($state) {
        $p["description"] .= ", " . $state;
    }
    if ($zip) {
        $p["description"] .= ", " . $zip . "<br>";
    }
    else {
        $p["description"] .= "<br>";
    }
    if ($phone) {
        $p["description"] .= "Phone: " . $phone . "<br>";
    }
    if ($fax) {
        $p["description"] .= "Fax: " . $fax . "<br>";
    }
    if ($url) {
        $p["description"] .= "Web: " . $url . "<br>";
    }
    if ($email) {
        $p["description"] .= "Email: " . $email . "<br>";
    }
    if ($notes) {
        $p["description"] .= $notes . "<br>";
    }
    $p["domain_id"] = $domain_id;
    $p["type"] = "memorials";
    $query = "REPLACE plg_list SET " . make_set_clause_in($p);
    $ret = $db->query($query);
    if (DB::isError($ret)) {
        $logger->log("[MIMS] DB error: memorial import: " . $ret->getMessage() . ";" . $ret->getUserInfo() . ";" . $query, PEAR_LOG_ERR);
    }
}

function process_merchandise($merch) {
    global $db, $logger, $domain_id, $domain_name;

    $p = array();
    $p["guid"] = (string)$merch["id"];
    $category = (string)$merch->category;
    $p["name"] = (string)$merch->name;
    $p["price"] = (string)$merch->price;
    $supplier = (string)$merch->supplier;
    $p["description"] = (string)$merch->description;
    $p["image"] = (string)$merch->image;

    // postprocess
    if ($supplier) {
        $p["description"] = $supplier . "<br>" . $p["description"];
    }

    // process image
    if ($p["image"]) {
        $p["image"] = "image/" . $p["image"];
        $image = "files/$domain_name/" . $p["image"];
        if (file_exists($image)) {
            $point = strrpos($image, ".");
            $tn = substr($image, 0, $point) . "-tn" . substr($image, $point);
            resizejpg($image, 100, $tn); // TN max width=100
        }
    }
    // find category
    $query = "SELECT category_id FROM plg_product_category WHERE name='" . in($category) . "' AND domain_id='$domain_id'";
    $category_id = $db->getOne($query);
    if (!$category_id) {
        // create new category
        $query = "INSERT plg_product_category SET name='" . in($category) . "', domain_id='$domain_id'";
        $db->query($query);
        $category_id = $db->getOne("SELECT last_insert_id()");
    }
    $p["category_id"] = $category_id;
    $p["enabled"] = 1;
    $p["timestamp"] = time();
    $p["domain_id"] = $domain_id;

    // store merchandise item
    $query = "REPLACE plg_product SET " . make_set_clause_in($p);
    $ret = $db->query($query);
    if (DB::isError($ret)) {
        $logger->log("[MIMS] DB error: merchandise import: " . $ret->getMessage() . ";" . $ret->getUserInfo() . ";" . $query, PEAR_LOG_ERR);
    }
}

function process_movieclip($movieclip) {
    global $db, $logger, $domain_id;
    static $count = 0;

    $p = array();
    $obituary_case_id = (string)$movieclip["id"];
    $p["filename"] = (string)$movieclip["filename"];
    $p["title"] = (string)$movieclip->name;
    $p["password"] = (string)$movieclip->password;
    $p["protected"] = intval($movieclip->protected);

    $p["tmpfilename"] = "mimsimported";
    $p["status"] = 1; // ready
    $p["timestamp"] = time();
    $p["domain_id"] = $domain_id;

    $p["filename"] = "movies/" . $p["filename"];

    // store video
    // remove existing DB entries of videos for obituary (do not remove files, they may be replaced with new ones)
    if ($count == 0) { // only on first step
        $query = "DELETE mc, obmc FROM plg_movieclip mc INNER JOIN plg_obituary_movieclip obmc USING(movieclip_id) INNER JOIN plg_obituary ob USING(obituary_id) WHERE obituary_case_id='$obituary_case_id' AND ob.domain_id='$domain_id'";
        $db->query($query);
        $logger->info("[MIMS] clear movieclips, $query");
    }

    // insert new video
    // store new movieclip
    $query = "INSERT plg_movieclip SET " . make_set_clause_in($p);
    $db->query($query);
    $logger->info("[MIMS] store movieclip, $query");
    if ($obituary_case_id) {
        $movieclip_id = $db->getOne("SELECT last_insert_id()");
        $query = "SELECT obituary_id FROM plg_obituary WHERE obituary_case_id='" . in($obituary_case_id) . "' AND domain_id='$domain_id'";
        $obituary_id = $db->getOne($query);
        if ($movieclip_id && $obituary_id) {
            $query = "INSERT plg_obituary_movieclip SET obituary_id='$obituary_id', movieclip_id='$movieclip_id'";
            $db->query($query);
        }
    }

    $count++; // next function call with know that it processing next movieclip (not first)

    // check if it exists
    /*    $query = "SELECT movieclip_id FROM plg_obituary_movieclip INNER JOIN plg_obituary USING(obituary_id) WHERE obituary_case_id='$obituary_case_id' AND domain_id='$domain_id'";
  $movieclip_id = $db->getOne($query);
  if ($movieclip_id) {
      // update movieclip data
      $query = "UPDATE plg_movieclip SET " . make_set_clause_in($p) . " WHERE movieclip_id='$movieclip_id' AND domain_id='$domain_id'";
      $db->query($query);
      $logger->info("[MIMS] updating movieclip, $query");
  }
  else { */
}

function make_set_clause_in($p) {
    $ret = "";
    foreach ($p as $key => $value) {
        $ret .= "$key='" . in($value) . "',";
    }
    return substr($ret, 0, -1);
}

