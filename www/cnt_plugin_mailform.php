<?php
 // plugin for emailing forms
include "cnt_plugin_main.php";

function process_content($content, $state) {
    global $domain_id, $domain_name, $db;

    if (empty($state)) { // show mailing form by default
        $ret = process_tags($content["content1"], array("captcha" => "captcha"), $state);
        if (isset($_SESSION["form_state"])) {
            // prefill form with user input values
            $patterns = array();
            $replacements = array();
            foreach ($_SESSION["form_state"] as $name => $value) {
                // prefill inputs
                $patterns[] = '/(\<input.*?name="' . $name . '".*?\>)/e';
                $replacements[] = "prefill_input('\\1', '$value')";
                // prefill textareas
                $patterns[] = '/(\<textarea.*?name="' . $name . '".*?\>.*?\<\/textarea\>)/e';
                $replacements[] = "prefill_textarea('\\1', '$value')";
            }

            $ret = preg_replace($patterns, $replacements, $ret);
            // clear form state
            unset($_SESSION["form_state"]);
        }

        // add form tag only if not there
        if (strpos($ret, "<form") === false) {
            $ret = '<form method="post">' . $ret . '</form>';
        }

        /*    OLD CAPTCHA auto-injecting code for all forms
        $captcha_code = file_get_contents("templates/helpers/captcha_code.html");
              // try to inject captcha right before submit button
        $rpls_made = 0;
              $ret = preg_replace('/(\<input.*?type="submit".*?\>)/', $captcha_code."\\1", $ret, 1, $rpls_made);
              if(!$rpls_made) {
          // injecting captcha before submit button failed
                  // inject it just before form ending
          $ret = str_replace("</form>", $captcha_code."</form>", $ret);
              } */
        return $ret;
    }
    else { // mail posted fields
        if (!isset($state["debug"]) && $_SERVER["REQUEST_METHOD"] == "POST") {
            // validation
            $error_count = 0;
            $captcha_present = (strpos($content["content1"], "{captcha}") !== false);
            // check if captcha present on the form
            if ($captcha_present) {
                if ($state["captcha"] != $_SESSION["captcha"] || !$_SESSION["captcha"]) {
                    $error_count++;
                    verse_set_message("Letters do not match, please try again", "error");
                }
                unset($_SESSION["captcha"]); // erase captcha to protect from subsequent posts with the same value
            }
            if (!$error_count) {
                $ip = $_SERVER['REMOTE_ADDR'];

                if (!is_block_ip($ip)) {
                    // it is unnecessary sending captcha in email
                    unset($state["captcha"]);

                    // fetch default email for the domain
                    $query = "SELECT email FROM sms_domain WHERE domain_id='" . $GLOBALS["domain_id"] . "'";
                    $email = $GLOBALS["db"]->getOne($query);
                    $result = "IP address: ".$ip."\n";
                    $replyto = "";
                    foreach ($state as $key => $value) {
                        $result .= "$key: $value\n";
                        if ($key == "email" && check_email($value)) {
                            $replyto = $value;
                        }
                    }
                    // determine subject
                    switch ($_GET["p"]) {
                        case "contact":
                        case "contactus":
                        case "contact_us":
                        case "onlinecontact":
                        case "contactform":
                        case "contactinfo":
                        case "contact_info":
                            $subject = "$domain_name: contact form message";
                            $hello = "Hello, this is the message from your website $domain_name.\nSomeone has filled contact form with next values:";
                            break;
                        case "preneed":
                        case "preplan":
                        case "planning_form":
                        case "planningform":
                            $subject = "$domain_name: preneed planning form";
                            $hello = "Hello, this is the message from your website $domain_name.\nSomeone has filled preneed form with next values:";
                            break;
                        case "request":
                            $subject = "$domain_name: request form message";
                            $hello = "Hello, this is the message from your website $domain_name.\nSomeone has filled request form with next values:";
                            break;
                        default:
                            $subject = "$domain_name: {$_GET["p"]} form message";
                            $hello = "Hello, this is the message from your website $domain_name.\nSomeone has filled the {$_GET["p"]} form with next values:";
                    }
                    $signature = "Sincerely yours, webform robot.";
                    $result = $hello . "\n\n" . $result . "\n\n" . $signature;

                    // special processing
                    if ($domain_id == 124 && isset($state['support_dept'])) {
                        switch ($state['support_dept']) {
                            case 'mims':
                                $email = 'mimssupport@twintierstech.com';
                                break;
                            case 'web':
                                $email = 'websupport@twintierstech.com';
                                break;
                            case 'motv':
                            case 'general':
                                $email = 'support@twintierstech.com';
                                break;
                        }
                    }
                    // end special

                    send_email($email, $subject, $result, $replyto);
                } else {
                    update_block_ip_count($ip);
                }
            }
            else {
                // there was validation errors - redisplay the form with error messages
                $_SESSION["form_state"] = $state;
                //          header("Location: /?p=".$_GET["p"]);
                header("Location: /" . $_GET["p"]);
                exit;
            }
        }
        if ($content["content2"]) return process_tags($content["content2"]);
        else return "<div>Thanks for contacting us</div>";
    }
}

function send_email($emailto, $subject, $text, $replyto) {
    $emailfrom = "noreply@twintierstech.net";
    if ($replyto) $replyto = "\r\nReply-To: $replyto";

    return mail($emailto, $subject, $text, "From: $emailfrom".$replyto);
}

function prefill_input($tag, $value) {
    //    echo "<!--$tag = $value -->\n";
    // handle XHTML closing tags
    $tag = stripslashes(str_replace("/>", ">", $tag));
    $tag = str_replace(">", " value=\"$value\">", $tag);
    //    echo "<!-- $tag -->\n";
    return $tag;
}

function prefill_textarea($tag, $value) {
    //    echo "<!--$tag = $value -->\n";
    $tag = stripslashes($tag);
    $tag = str_replace("</textarea>", "$value</textarea>", $tag);

    return $tag;
}

function captcha($params) {
    $captcha_code = file_get_contents("templates/helpers/captcha_code.html");
    return $captcha_code;
}
