﻿/*
Copyright (c) 2003-2011, CKSource - Frederico Knabben. All rights reserved.
For licensing, see LICENSE.html or http://ckeditor.com/license
*/

CKEDITOR.editorConfig = function( config )
{
	// Define changes to default configuration here. For example:
	config.language = 'en';
	// config.uiColor = '#AADC6E';
	config.protectedSource = [/\{.*?\}/gi]; // { } tags.
	//  config.protectedSource = [/\{[^\}]+\}/im]; // { } tags.
	//  config.protectedSource = [/\{[\s\S]*?\}/im]; // { } tags.
	config.removePlugins = 'save,print,scayt,wsc,smiley,pagebreak,about,find,newpage';
	config.skin = 'v2';
	config.toolbar = [
	['Source'],
	['Preview','Templates'],
	['Cut','Copy','Paste','PasteText','PasteFromWord'],
	['Undo','Redo'],
	['Format','RemoveFormat','Font','FontSize','Styles'],
	['Maximize'],
	'/',
	['ShowBlocks'],
	['Bold','Italic','Underline','Strike','Subscript','Superscript'],
	['NumberedList','BulletedList','Outdent','Indent','Blockquote'],
	['JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock'],
	['Link','Unlink','Anchor'],
	['Image','Flash','Table','HorizontalRule','SpecialChar'],
	['TextColor','BGColor']
	];
	config.height = "250px";
};
