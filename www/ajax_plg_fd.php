<?php
 // frontend ajax funeral directors list, for nsfda.org
include("inc/verse.inc.php"); //main header - initializes Verse environment

$rep = createobject("report_ajax", array($db, "plg_funeral_director", REP_WITH_PAGENATOR, REP_WITH_HEADING, 50, 'class="pagenator" width="570px"'));

$rep->add_field("Status", REP_STRING_TEMPLATE, '{status}', REP_ORDERABLE, '');
$rep->add_field("Title", REP_STRING_TEMPLATE, '{title}', REP_ORDERABLE, '');
$rep->add_field("First", REP_STRING_TEMPLATE, '{first_name}', REP_ORDERABLE, '');
$rep->add_field("Last", REP_STRING_TEMPLATE, '{last_name}', REP_ORDERABLE, '');
$rep->add_field("Firm", REP_STRING_TEMPLATE, '{firm}', REP_ORDERABLE);
$rep->add_field("Address", REP_STRING_TEMPLATE, '{address}', REP_UNORDERABLE);
$rep->add_field("City", REP_STRING_TEMPLATE, '{city}', REP_ORDERABLE);
$rep->add_field("State", REP_STRING_TEMPLATE, '{state}', REP_ORDERABLE);
$rep->add_field("Zip", REP_STRING_TEMPLATE, '{zip}', REP_ORDERABLE);
$rep->add_field("Phone", REP_STRING_TEMPLATE, '{phone}', REP_ORDERABLE);
$rep->add_field("Fax", REP_STRING_TEMPLATE, '{fax}', REP_ORDERABLE);
$rep->add_field("County", REP_STRING_TEMPLATE, '{county}', REP_ORDERABLE);

$rep->html_attributes('width="550px"');

if (!isset($_POST["order"])) {
    $rep->order_by("last_name", 0);
}

$rep->handle_events($_POST);

echo $rep->get_pagenator_html();
$data = $rep->make_report_array();
echo "<table>";
echo "<tr valign=\"top\"><td>", href_js_action("Status", "order('status')"), "</td><td>First name</nobr></td><td>", href_js_action("Last name", "order('last_name')"), "</td><td>Firm, Address</td><td>Contact</td><td>", href_js_action("County", "order('county')"), "</td></tr>";
foreach ($data as $line) {
    $line = $line["data"];
    echo "<tr valign=\"top\"><td><b>" . $line["Status"] . "</b></td>";
    echo "<td>" . $line["Title"] . " " . $line["First"] . "</td>";
    echo "<td>", $line["Last"], "</td>";
    echo "<td>";
    if ($line["Firm"]) {
        echo "<b>", $line["Firm"], "</b><br>";
    }
    ;
    echo $line["Address"], "<br>", $line["City"], ", ", $line["State"], " ", $line["Zip"], "</td>";
    echo "<td>Phone: ", $line["Phone"], "<br>Fax: ", $line["Fax"], "</td>";
    echo "<td align=\"center\">", $line["County"], "</td>";
    echo "</tr>";
}
echo "</table>";
echo $rep->get_pagenator_html();

//	echo $html;

function name($last, $first) {
    return $first . " " . $last;
}
