<?php
  chdir("..");
  include("inc/verse.inc.php");  //main header - initializes Verse environment

  $query = "SELECT user_id, login, password, enabled, domain_id, name, piwik_token_auth FROM sms_user";
  $users = $db->getAll($query, DB_FETCHMODE_ASSOC);

  $values = array();
  $pvalues = array();
  $perm_values = array();
  foreach($users as $user) {
    $query = "SELECT role_id, role_params FROM cms_user_role WHERE user_id='".$user['user_id']."'";
    $roles = $db->getAll($query, DB_FETCHMODE_ASSOC);
    $values[] = '('.implode(',', array('"'.$user['user_id'].'"', '"'.$user['login'].'"', '"mysql_old_password_hash"', '"'.$user['password'].'"', '"'.$user['enabled'].'"', '"2010-01-01"', '"2010-05-01"')).')';

    $user['ftp_user_id'] = 'NULL';
    foreach($roles as $role) {
      if($role['role_id']==4) continue; // ignore mailboxes
      $perm_values[] = '('.implode(',', array('"'.$user['user_id'].'"', '"'.$role['role_id'].'"', '"2010-01-01"', '"2010-05-01"')).')';
      if($role['role_id']==2) {
        // give list along with structure
        $perm_values[] = '('.implode(',', array('"'.$user['user_id'].'"', '"13"', '"2010-01-01"', '"2010-05-01"')).')';
      }
      if($role['role_id']==5) {
        // give movieclips along with obituaries
        $perm_values[] = '('.implode(',', array('"'.$user['user_id'].'"', '"9"', '"2010-01-01"', '"2010-05-01"')).')';
      }
      if($role['role_id']==3) {
        $role_params = @unserialize($role['role_params']);
        if($role_params) {
          $user['ftp_user_id'] ='"'.$role_params['ftp_user_id'].'"';
        }
      }
    }
    $pvalues[] = '('.implode(',', array('"'.$user['user_id'].'"', '"'.$user['name'].'"', '"'.$user['domain_id'].'"', $user['ftp_user_id'], '"'.$user['piwik_token_auth'].'"')).')';
  }
  $query = "INSERT IGNORE sf_guard_user (id, username, algorithm, password, is_active, created_at, updated_at) VALUES ".implode(',', $values);
  $db->query($query);
  echo "sf_guard_user: ", $db->affectedRows(), " rows affected<br>";
//  echo $query;

  $query = "INSERT IGNORE sf_guard_user_profile (user_id, name, domain_id, ftp_user_id, piwik_token_auth) VALUES ".implode(',', $pvalues);
  $db->query($query);
  echo "sf_guard_user_profile: ", $db->affectedRows(), " rows affected<br>";

  $query = "INSERT IGNORE sf_guard_user_permission (user_id, permission_id, created_at, updated_at) VALUES ".implode(',', $perm_values);
  $db->query($query);
  echo "sf_guard_user_permisson: ", $db->affectedRows(), " rows affected<br>";
//  echo $query;