<?php
  chdir('..');

  foreach(glob("files/*", GLOB_ONLYDIR) as $filename) {
    echo $filename,"<br>";
    $backupfiles = $filename."/exchange/backup/*";
    $i = 0;
    $border = time() - 3888000; // 45 days
    foreach(glob($backupfiles) as $backupfile) {
      if(filemtime($backupfile) < $border) {
        unlink($backupfile);
        $i++;
      }
    }
    echo $i, " files deleted<br>";
  }
