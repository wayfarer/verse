<?php
require("config.inc.php");
require("util.inc.php");
require_once("DB.php");
require_once("Log.php");

// init logging mechanism
$conf = array('title' => 'Log');
$logger = &Log::factory('file', '/home/ttt-ws-production/util/convertmc.log', 'versesms');

$db_con = createobject("db_versesms");
$db = $db_con->connect();

if($argc==2) {
  $movieclip_id = intval($argv[1]);
//  $query = "SELECT tmpfilename, domain_name FROM plg_movieclip INNER JOIN sms_domain USING(domain_id) WHERE movieclip_id='$movieclip_id' AND status=2"; 
  $query = "SELECT tmpfilename, domain_name, origfilename FROM plg_movieclip INNER JOIN sms_domain USING(domain_id) WHERE movieclip_id='$movieclip_id'"; 
  $ret = $db->getRow($query, DB_FETCHMODE_ASSOC);

  if(!DB::isError($ret) && $ret) {
    extract($ret);
    // get file information
    $output = "";
    exec("/usr/bin/ffmpeg -i ../www/files/$domain_name/$tmpfilename 2>&1", $output);
    $output = implode("\n", $output);
//    echo $output, "\n\n";
    if(preg_match("/Video: (\w+).*?(\d{2,5})x(\d{2,5}).*?(\d+\.\d+) tb\(r\)/mis", $output, $matches)) {
      $format = $matches[1];
      $width = $matches[2];
      $height = $matches[3];
      $fps = $matches[4];
    }
    else {
      $query = "UPDATE plg_movieclip SET status=3 WHERE movieclip_id='$movieclip_id'";
      $db->query($query);
      $logger->warning("Cannot determine videofile data, movieclip_id='$movieclip_id', origfilename='$origfilename'! output='$output'\n");
      exit;
    }

    
    // determine destination
    $fn = substr($tmpfilename, strrpos($tmpfilename,"/")+1);
    $filename = "../www/files/$domain_name/movies/$fn.flv";
    $shortfn = "movies/$fn.flv";
//    echo $filename,"\n";
  
    if(intval($fps)==1000) { // some FLV files with FPS=1000 are converted incorrectly
      // just copy src file to dest
      copy("../www/files/$domain_name/$tmpfilename", $filename);
      $logger->info("copied file $shortfn (movieclip_id=$movieclip_id) due to detected exception (fps=1000)\n");
    }
    else {
      // convert if no exceptions
      $output = "";
      exec("/usr/bin/ffmpeg -i ../www/files/$domain_name/$tmpfilename -ar 22050 -s 456x342 -y $filename 2>&1", $output);
      $output = implode("\n", $output);
      $logger->info($output."\n");
    }
    $query = "UPDATE plg_movieclip SET filename='$shortfn', status=1 WHERE movieclip_id='$movieclip_id'";
    $db->query($query);
  }
  else {
    $logger->info("movieclip_id='$movieclip_id', nothing to do");
  }
}
