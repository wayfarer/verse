<?php
  require("config.inc.php");
  require("util.inc.php");
  require_once("DB.php");
    
  $db_con = createobject("db_versesms");
  $db = $db_con->connect();
  if(DB::isError($db)) {
		echo "<b>error! ", $db->getMessage(), "</b><br>";        
    echo $db->getUserInfo(), "<br>";
    exit;
  }

  $domain_id = 512; // flynnfh.com

  ini_set("display_errors","1");

	$h = fopen("flynn/message.csv", "r");

	$case_id_to_obit_id = array();
  $cnt = 0;
  while(($row = fgetcsv($h, 65535, ";"))!==false) {
    $obituary_case_id = $row[0];
    // fetch obituary_id
    if(!$case_id_to_obit_id[$obituary_case_id]) {
      $query = "SELECT obituary_id FROM plg_obituary WHERE obituary_case_id='$obituary_case_id' AND domain_id='$domain_id'";
      $obituary_id = $db->getOne($query);
    }
    else {
      $obituary_id = $case_id_to_obit_id[$obituary_case_id];
    }
    if(!$obituary_id || DB::isError($obituary_id)) {
      echo "obituary not found for $obituary_case_id<br>";
    }
    else {
      // insert a candle
      $p["domain_id"] = $domain_id;
      $p["obituary_id"] = $obituary_id;
      $p["name"] = in($row[1]);
      $p["ip"] = "flynn_import";
      $p["thoughts"] = in(trim($row[6]));
      $p["timestamp"] = date("Y-m-d", strtotime($row[5]));
      $p["state"] = 1;      
      if($p["thoughts"]) {
        $query = "INSERT plg_obituary_candle SET ".make_set_clause($p);
        $ret = $db->query($query);
        if(DB::isError($ret)) {
        	echo "<b>error! ", $ret->getMessage(), "</b><br>";        
          echo $ret->getUserInfo(), "<br>";
        }     
        else {
          $cnt++;
        }
  
        if(!$db->affectedRows()) {
          echo "not inserted for $obituary_case_id<br>";
        }
      }
    }
  }
  echo "$cnt messages processed";
