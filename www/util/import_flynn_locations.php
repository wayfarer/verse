<?php
  require("config.inc.php");
  require("util.inc.php");
  require_once("DB.php");
    
  $db_con = createobject("db_versesms");
  $db = $db_con->connect();
  if(DB::isError($db)) {
		echo "<b>error! ", $db->getMessage(), "</b><br>";        
    echo $db->getUserInfo(), "<br>";
    exit;
  }

  $domain_id = 512; // flynnfh.com

  ini_set("display_errors","1");

	$h = fopen("flynn/locations.csv", "r");

	while(($row = fgetcsv($h, 65535, ";"))!==false) {
    switch($row[10]) {
      case "Cemetery":
        $obituary_case_id = in($row[0]);
        $final_disposition = in($row[1]);
        $query = "UPDATE plg_obituary SET final_disposition='$final_disposition' WHERE obituary_case_id='$obituary_case_id' AND domain_id='$domain_id'";
        $ret = $db->query($query);
        if(DB::isError($ret)) {
        	echo "<b>error! ", $ret->getMessage(), "</b><br>";        
          echo $ret->getUserInfo(), "<br>";
        }     
        if(!$db->affectedRows()) {
          echo $row[0], " -Cemetery NA<br>";
        }
      break;
      case "Service":
        $obituary_case_id = in($row[0]);
        $service_place = in($row[1]);
        $service_date = strtotime($row[2]);
        $service_time = in($row[3])." - ".in($row[4]);
        $query = "UPDATE plg_obituary SET service_date='$service_date', service_time='$service_time', service_place='$service_place' WHERE obituary_case_id='$obituary_case_id' AND domain_id='$domain_id'";
        $ret = $db->query($query);
        if(DB::isError($ret)) {
        	echo "<b>error! ", $ret->getMessage(), "</b><br>";        
          echo $ret->getUserInfo(), "<br>";
        }     
        if(!$db->affectedRows()) {
          echo $row[0], " -Service NA<br>";
        }
      break;
      case "Visitation":
        $obituary_case_id = in($row[0]);
        $visitation_place = in($row[1]);
        if($row[2]!="0000-00-00") {
          $visitation_date = in($row[2]);
          if($row[3]) {
            $visitation_date.=" ".$row[3]." - ".$row[4];
          }
          if($row[5]) {
            $visitation_date.="; ".$row[5];
          }
          if($row[6]) {
            $visitation_date.=" ".$row[6]." - ".$row[7];
          }
        }
        else {
          $visitation_date = "";
        }
        $query = "UPDATE plg_obituary SET visitation_date='$visitation_date', visitation_place='$visitation_place' WHERE obituary_case_id='$obituary_case_id' AND domain_id='$domain_id'";
        $ret = $db->query($query);
        if(DB::isError($ret)) {
        	echo "<b>error! ", $ret->getMessage(), "</b><br>";        
          echo $ret->getUserInfo(), "<br>";
        }     
        if(!$db->affectedRows()) {
          echo $row[0], " -Visitation NA<br>";
        }
      break;
      default:
        //echo "skip ".$row[10]."<br>";
    }
  }
    