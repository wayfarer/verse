<?php
  chdir("..");
	include("inc/verse.inc.php");  //main header - initializes Verse environment

  // fetch addresses
  $query = "SELECT email FROM sms_domain WHERE enabled=1 AND alias_domain_id=0";
  $emails = $db->getCol($query);

  $email_text = file_get_contents('util/send_email_to_default_addresses.txt');

  $emailfrom = "websupport@twintierstech.com";
  $subj = "Website Management Updates";
  $report = "";
  foreach($emails as $emailto) {
    if(trim($emailto)) {
      $ret = mail($emailto, $subj, $email_text, "From: $emailfrom", "-f$emailfrom");
      if($ret) {
        $report.="$emailto\n";
      }
      else {
        echo "$emailto - failed!<br>";
      }
    }
  }
  echo $report;
  echo "<br><br>";
  
  $report_to[] = "erin@twintierstech.com";
  $report_to[] = "brian@twintierstech.com";
  $report_to[] = "lance@twintierstech.com";
  $report_to[] = "sarah@twintierstech.com";
  $report_to[] = "vladimir@twintierstech.com";

  $subj = "Report: ".$subj;
  $email_text = "This is the mass mailing report, see below for the message text and the list of addresses the text was sent to\n\n---------\n\n".$email_text."\n----------------\n\nSent to next addresses:\n".$report;
  foreach($report_to as $emailto) {
    if(trim($emailto)) {
      $ret = mail($emailto, $subj, $email_text, "From: $emailfrom", "-f$emailfrom");
      echo "report sent to $emailto<br>";
    }
  }
