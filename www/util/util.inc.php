<?php

// prepares string or array of strings coming from browser for inserting to database query
function in($values) {
  if(is_array($values)) {
    foreach ($values as $key => $value) {
      $values[$key] = in($value);
    }
  }
  else if(!is_numeric($values) && !is_null($values) && !is_bool($values)) {
    if(get_magic_quotes_gpc()==1) {
      $values = stripslashes($values);
    }
    $values = mysql_real_escape_string($values);
  }
  return $values;
}

/*function in($str) {
	if(get_magic_quotes_gpc()==1)
		return $str;
	else
		return addslashes($str);
}*/

function out($str)
{
  if(is_array($str)) {
  	foreach($str as $key=>$value) {
  		$str[$key] = htmlspecialchars($value);
  	}
  }
  else
  	$str = htmlspecialchars($str);
  return $str;
}

function in_strip($str)
{
  if(get_magic_quotes_gpc()==1) {
	  if(is_array($str)) {
	  	foreach($str as $key=>$value) {
  			$str[$key] = stripslashes($value);
  		}
  	}	
  	else
  		$str = stripslashes($str);
  }
  return $str;
}

        // returns time in floating point format with microseconds since Unix Epoch
function getmicrotime() {
	list($usec, $sec) = explode(" ",microtime());
	return ((float)$usec + (float)$sec);
}

function check_email($email)
{
  $email = trim($email);
  if (strlen($email)==0) {
    return false;
  }
  if(!(ereg('^[-!#$%&\'*+\\./0-9=?A-Z^_`a-z{|}~]+@[-!#$%&\'*+\\/0-9=?A-Z^_`a-z{|}~]+\.[-!#$%&\'*+\\./0-9=?A-Z^_`a-z{|}~]+$', $email))) {
	return false;
  }
  return true;
}                                                                                            

function &createobject($classname, $args = array())
{
      if(file_exists("inc/".$classname.".class.php"))
              include_once("inc/".$classname.".class.php");
      else
              include_once($classname.".class.php");

      // extract pure class name from $classname (it can be path)
      $class = substr($classname, ($pos = strrpos($classname, "/"))?$pos+1:0);

      $args_count = count($args);
      if($args_count == 0) {
        $obj =& new $class;
      }
      else {
        $construct = "\$obj =& new $class(\$args[0]";
        for($i=1; $i<$args_count; $i++) {
                $construct.=", \$args[$i]";
        }
        $construct.=");";
        eval($construct);
      }
      return $obj;
}

function make_json_response($arr)
{
	$response = "";
	foreach($arr as $field=>$value) {
		if(!is_array($value)) {
			$response.="'$field':'".str_replace(array("\\","'"), array("\\\\","\'"), $value)."',";
		}
		else {
			$response.="'$field':".make_json_response($value).",";
		}
	}
	$response = "({".str_replace(array("\r","\n"), array("\\r","\\n"), substr($response,0,-1))."})";
	return $response;
}

function make_set_clause($p)
{
	$ret = "";
	foreach($p as $key=>$value) {
		if(!is_null($value)) {
			$ret.="$key='$value',";
		}
		else {
			$ret.="$key=NULL,";
		}
	}
	return substr($ret, 0, -1);
}

function utf8_to_latin($p) {
  if(is_array($p)) {
  	foreach($p as $key=>$value) {
  		$p[$key] = iconv('UTF-8', 'ISO-8859-1//TRANSLIT', $value);
  	}
  }
  else
  	$p = iconv('UTF-8', 'ISO-8859-1//TRANSLIT', $p);
  return $p;
}

function get_music_files() {
  global $domain_name;

  $music_files = array();
  // shared music
  foreach(glob("assets/*.mp3") as $filename) {
    $fn = substr($filename, 7, -4);
    $title = ucfirst($fn);
    $music_files[$filename] = $title;
  }
  // local music
  foreach(glob("files/$domain_name/mp3/*.mp3") as $filename) {
    $fn = substr($filename, strrpos($filename, "/")+1);
    $filename = "mp3/$fn";
    $title = ucfirst(substr($fn, 0, -4));
    $music_files[$filename] = $title;
  }
  return $music_files;
}

function page_exists($page) {
  global $domain_id, $db;
  $query = "SELECT count(*) FROM cms_page WHERE internal_name='".in($page)."' AND domain_id='$domain_id'";
  return $db->getOne($query);
}

function uc_settings_exist() {
  global $domain_id, $db;
  $query = "SELECT 1 FROM cms_site_uc WHERE domain_id='$domain_id'";
  return $db->getOne($query);
}

function url($page, $query) {
  $params = array();
  foreach($query as $key=>$value) {
    $params[] = urlencode($key)."=".urlencode($value);
  }
  return "?p=$page&".implode("&", $params);
}
