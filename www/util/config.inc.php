<?php
/**
 *  Config file.
 */
//  $pass_unknown_domain = true;
// security
  ini_set("register_globals", "0");

// directories
  define("PEAR_DIR", "pear/");
  define("SMARTY_DIR", "smarty/");
  define("SAVANT_DIR", "savant/");

  define("SMARTY_DEBUG", false);
  define("SMARTY_FORCE_RECOMPILE", false);

// error reporting & debug
  error_reporting(E_ALL & ~E_NOTICE);
//  error_reporting(E_ALL);
  ini_set("log_errors","1");
  ini_set("display_errors","1");
  ini_set("html_errors","0");
  ini_set("error_log","logs/php_errors.log");

// application
  define("SECURE_LOGIN_ONLY", 0);
  
  define("DEFAULT_PAGE", "index.php");
  define("REP_WITH_PAGENATOR", 1);
  define("REP_NO_PAGENATOR", 0);
  define("REP_WITH_HEADING", 1);
  define("REP_NO_HEADING", 0);

  define("DEFAULT_DATE_FORMAT", "Y-m-d");
  define("DB_DATE_FORMAT", "%Y-%m-%d");
  define("DB_DATETEXT_FORMAT", "%M %e, %Y");
  define("DB_DATETIME_FORMAT", "%Y-%m-%d %h:%i %p EST");
  define("DB_DATETIMELONG_FORMAT", "%W, %b %e, %Y at %h:%i %p EST");
  define("DB_DATETIMEAMERICAN_FORMAT", "%c-%e-%Y %l:%i %p EST");

  define("ROLE_USER_MANAGEMENT", 1);
  define("ROLE_CONTENT_MANAGEMENT", 2);
  define("ROLE_FTP", 3);
  define("ROLE_MAILBOX", 4);
  define("ROLE_OBITUARIES_MANAGEMENT", 5);
  define("ROLE_PRODUCTS_MANAGEMENT", 6);
  define("ROLE_SUBLISTS_MANAGEMENT", 7);
  define("ROLE_STATISTICS_MANAGEMENT", 8);
  define("ROLE_MOVIECLIPS_MANAGEMENT", 9);
  define("ROLE_WEBCASTING_MANAGEMENT", 10);
  
  define('PHPMV_PATH', 'http://twintierstech.net/phpmv2'); //no trailing slash needed
  define('PHPMV_BASE_URL', 'http://twintierstech.net/phpmv2'); //no trailing slash needed
  define('PHPMV_SU_LOGIN', 'admin'); 
  define('PHPMV_SU_PASSWORD', '7b503f2bb345cd69757b0bd1d31f4d15'); //ready md5-string goes here

// include path initialization
  if(substr(PHP_OS,0,3)=="WIN")
          $directory_separator = ";";
  else
          $directory_separator = ":";

  ini_set("include_path", ini_get("include_path").$directory_separator.PEAR_DIR.$directory_separator.SMARTY_DIR.$directory_separator.SAVANT_DIR.$directory_separator."../");
// support for scripts in subfolders, like for /admin/ folder or any else
  ini_set("include_path", ini_get("include_path").$directory_separator."../".PEAR_DIR.$directory_separator."../".SMARTY_DIR);
  
  // error handler function
  function myErrorHandler($errno, $errstr, $errfile, $errline)
  {
    $log_error = true;
    switch($errno) {
      case E_NOTICE:
        if(!(ini_get("error_reporting")&E_NOTICE)) {
          $log_error = false;  
        }
        else $type = "Notice";
      break;
      case E_WARNING:
        if(!(ini_get("error_reporting")&E_WARNING)) {
          $log_error = false;  
        }
        else $type = "Warning";
      break;
      case E_STRICT:
        if(!(ini_get("error_reporting")&E_STRICT)) {
          $log_error = false;  
        }
        else $type = "Strict";
      break;
      default:
        $type = $errno;  
    }
  
    if($log_error && ini_get('log_errors')) {
      error_log(sprintf("%s: %s in %s on line %d, ".$_SERVER["HTTP_HOST"], $type, $errstr, $errfile, $errline));
    }
  
    /* Don't execute PHP internal error handler */
    return true;
  }
  
  // set to the user defined error handler
  $old_error_handler = set_error_handler("myErrorHandler");
  
