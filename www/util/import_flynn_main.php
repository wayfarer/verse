<?php
  require("config.inc.php");
  require("util.inc.php");
  require_once("DB.php");
    
  $db_con = createobject("db_versesms");
  $db = $db_con->connect();
  if(DB::isError($db)) {
		echo "<b>error! ", $db->getMessage(), "</b><br>";        
    echo $db->getUserInfo(), "<br>";
    exit;
  }

  $domain_id = 512; // flynnfh.com

	$h = fopen("flynn/main.csv", "r");

	while(($ret = fgetcsv($h, 65535, ";"))!==false) {
    if($ret[0] && $ret[1]) {
      $p = array();
      $p["obituary_case_id"] = $ret[0];
      $p["first_name"] = $ret[1];
      $p["middle_name"] = $ret[2];
      $p["last_name"] = $ret[3];
      $p["home_place"] = $ret[4].", ".$ret[5].", ".$ret[6];
      $p["death_date"] = $ret[9];
      $p["birth_place"] = $ret[7];
      $p["birth_date"] = $ret[8];
      $p["final_disposition"] = "";
      $p["obit_text"] = "";
      $p["domain_id"] = $domain_id;
      $query = "INSERT plg_obituary SET ".make_set_clause_in($p);
      $ret = $db->query($query);
      if(DB::isError($ret)) {
  			echo "<b>error! ", $ret->getMessage(), "</b><br>";        
        echo $ret->getUserInfo(), "<br>";
      }
    }
  }

  function make_set_clause_in($p)
  {
  	$ret = "";
  	foreach($p as $key=>$value) {
  		$ret.="$key='".in($value)."',";
  	}
  	return substr($ret, 0, -1);
  }

  