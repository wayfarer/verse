<?php
  require("config.inc.php");
  require("util.inc.php");
  require_once("DB.php");
    
  $db_con = createobject("db_versesms");
  $db = $db_con->connect();
  if(DB::isError($db)) {
		echo "<b>error! ", $db->getMessage(), "</b><br>";        
    echo $db->getUserInfo(), "<br>";
    exit;
  }

  $domain_id = 512; // flynnfh.com

  ini_set("display_errors","1");

	$h = fopen("flynn/obit.csv", "r");

	while(($ret = fgetcsv($h, 65535, ";"))!==false) {
    // convert data to UTF-8 format, to support different non-ASCII symbols (for symbols above 122 (z) )
    $obit_text = nl2br(iconv("ISO-8859-1", "UTF-8", $ret[1]));
    if($obit_text) {
      $query = "UPDATE plg_obituary SET obit_text='".in($obit_text)."' WHERE domain_id='$domain_id' AND obituary_case_id='".in($ret[0])."'";
      $res = $db->query($query);
      if(DB::isError($res)) {
      	echo "<b>error! ", $res->getMessage(), "</b><br>";        
        echo $res->getUserInfo(), "<br>";
      }     
      if(!$db->affectedRows()) {
        echo $ret[0], " -NA<br>";
      }
    }
  }
  