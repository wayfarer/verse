<?php
  require("config.inc.php");
  require("util.inc.php");
  require_once("DB.php");

  error_reporting(E_ALL & ~E_NOTICE);
    
  $db_con2 = createobject("db_sasldb");
  $dbs = $db_con2->connect();
  if(DB::isError($dbs)) {
    echo "<b>error! ", $dbs->getMessage(), "</b><br>";        
    echo $dbs->getUserInfo(), "<br>";
    exit;
  }

  $query = "SELECT username, password FROM accounts";
	$accounts = $dbs->getAssoc($query);

	$emails_data = unserialize(file_get_contents("email_data.ws1"));
	
	echo "<b>", count($emails_data), " domains</b><br>";
  foreach($emails_data as $domain=>$emails) {
  	echo "<b>$domain</b> (", count($emails), ")<br>";
		foreach($emails as $i=>$email) {
			echo $email["name"],"@$domain - ", ($email["type"]==0?"mailbox":"forward (".$email["forward"].")"), " - ", $accounts[$email["login"]], "<br>";
			$emails_data[$domain][$i]["password"] = $accounts[$email["login"]]; 
		}
		echo "<br>";
  }

	file_put_contents("email_data_p.ws1", serialize($emails_data));
	
  function file_put_contents($filename, $data, $append = 0) {
      if(!$append) {
        $f = @fopen($filename, 'a');
      }
      else {
        $f = @fopen($filename, 'w');                
      }
      if (!$f) {
          return false;
      } else {
          $bytes = fwrite($f, $data);
          fclose($f);
          return $bytes;
      }
  }
