<?php
  chdir('..');

  include("inc/verse.inc.php");  //main header - initializes Verse environment

  // connect to DB
  $db_con = createobject("db_versesms_secondary");
  $db = $db_con->connect();
  
  $query = "SELECT domain_name FROM sms_domain";
  $domain_names = $db->getCol($query);

  file_put_contents('files/secondary_domains.txt', serialize($domain_names));

  echo count($domain_names), ' domains dumped';
