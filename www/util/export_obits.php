<?php
  chdir("..");
include("inc/verse.inc.php"); //main header - initializes Verse environment

$datefrom = "2000-03-22";

if ($user->logged()) {
  $query = "SELECT domain_name FROM sms_domain WHERE domain_id='$domain_id'";
  $domain_name = $db->getOne($query);

  $fields = array("obituary_id", "obituary_case_id", "first_name", "middle_name", "last_name", "home_place", "death_date", "birth_place", "birth_date", "service_date", "service_place", "visitation_date", "visitation_place", "final_disposition", "image", "obit_text");
  $candle_fields = array("candle_id", "candle_case_id", "obituary_id", "name", "thoughts", "timestamp");

  // special for scanlanfuneral.com
  //    $fields = array("obituary_id", "first_name", "middle_name", "last_name", "obit_text", "birth_date", "death_date", "image");
  //    $candle_fields = array("candle_id", "obituary_id", "name", "email", "ip", "thoughts", "timestamp");

  // determine obits with new candles and export them togeter with new obits
//  $query = "SELECT obituary_id FROM plg_obituary_candle WHERE timestamp>='$datefrom'";
//  $obit_ids = $db->getCol($query);
//  $obit_ids = array_unique($obit_ids);

//  echo "Obituaries to export: ", count($obit_ids),"<br>";
  if(!file_exists("files/$domain_name/file/oi")) {
    mkdir("files/$domain_name/file/oi");
  }

  // fetch obituaries
//  $query = "SELECT " . implode(",", $fields) . " FROM plg_obituary WHERE (death_date>='$datefrom' OR obituary_id IN (" . implode(",", $obit_ids) . ")) AND domain_id='$domain_id'";
  $query = "SELECT " . implode(",", $fields) . " FROM plg_obituary WHERE death_date>='$datefrom' AND domain_id='$domain_id' ORDER BY obituary_id";
  $ret = $db->query($query);
  $h_obits = fopen("files/$domain_name/file/obituaries.csv", "w");

  $obit_ids = array();

  // put header
  $line = implode(";", $fields) . "\r\n";
  fputs($h_obits, $line);
  while ($row = $ret->fetchRow(DB_FETCHMODE_ASSOC)) {
    foreach($row as &$item) {
      $item = trim($item);
    }
    /*foreach($row as $i=>$item) {
          if(strpos($row[$i],";") || strpos($row[$i],"\n")) {
            $row[$i] = str_replace('"', '""', $item);
            $row[$i] = '"'.$row[$i].'"';
          }
        }
        $line = implode(";", $row)."\r\n";
        fputs($h_obits, $line); */
/*    if($row['obituary_id']==10004546) {
      var_dump($row);
      exit;
    } */
    fputcsv($h_obits, $row, ';');
    $obit_ids[] = $row['obituary_id'];

    // process obit image
    if ($row['image']) {
      echo "!";
      // copy image with obit_id name
      $src = "files/$domain_name" . leading_slash($row['image']);
      if (!file_exists($src)) {
        $src = "files/$domain_name/image" . leading_slash($row['image']);
      }
      if (!file_exists($src)) {
        $src = "files/$domain_name/image/obituaries" . leading_slash($row['image']);
      }
      if (file_exists($src)) {
        $dst = "files/$domain_name/file/oi/" . $row['obituary_id'] . '.jpg';
        copy($src, $dst);
        echo "+";
      }
      else {
        echo $src, " not found<br>";
      }
    }
  }
  fclose($h_obits);
  echo "<br>Exported:", count($obit_ids), "<br>";
  var_dump($row);

  // fetch candles
  $query = "SELECT " . implode(",", $candle_fields) . " FROM plg_obituary_candle WHERE obituary_id IN (" . implode(",", $obit_ids) . ") AND timestamp>='$datefrom'";
  $ret = $db->query($query);
  $h_candles = fopen("files/$domain_name/file/candles.csv", "w");
  // put header
  $line = implode(";", $candle_fields) . "\r\n";
  fputs($h_candles, $line);
  while ($row = $ret->fetchRow()) {
    foreach($row as &$item) {
      $item = trim($item);
    }

    /*foreach ($row as $i => $item) {
      //  		    	$row[$i] = str_replace(array('"', "\r\n"), array('""', "\n"), $item);
      $row[$i] = str_replace('"', '""', $item);
      if (strpos($row[$i], ";") || strpos($row[$i], "\n")) {
        $row[$i] = str_replace('"', '""', $item);
        $row[$i] = '"' . $row[$i] . '"';
      }
    }
    $line = implode(";", $row) . "\r\n";
    fputs($h_candles, $line);*/
    fputcsv($h_candles, $row, ';');
  }
  fclose($h_candles);
  echo "done<br>";
  $link = "http://$domain_name/file/obituaries.csv";
  echo "<a href=\"$link\">$link</a><br>";
  $link = "http://$domain_name/file/candles.csv";
  echo "<a href=\"$link\">$link</a><br>";
  $link = "http://$domain_name/file/oi/";
  echo "<a href=\"$link\">$link</a><br>";
}
else {
  echo "access denied";
}
