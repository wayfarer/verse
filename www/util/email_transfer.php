<?php
  chdir("/home/ttt-ws-production");
//  file_put_contents("email_transfer.runs", date("Y-m-d H:i:s ")."starting..."."\n", FILE_APPEND);

  if(file_exists("sendmail_config/aliases") || file_exists("sendmail_config/commands")) {
    $data = array();
    $data["access"] = @file_get_contents("sendmail_config/access");
    $data["genericstable"] = @file_get_contents("sendmail_config/genericstable");
    $data["virtusertable"] = @file_get_contents("sendmail_config/virtusertable");
    $data["local-host-names"] = @file_get_contents("sendmail_config/local-host-names");
    $data["aliases"] = @file_get_contents("sendmail_config/aliases");
    $data["commands"] = @file_get_contents("sendmail_config/commands");

    $url = "http://twintierstech.net/email_gate.php";
    $post = "action=config&data=".urlencode(serialize($data));
    
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

  	$result = curl_exec($ch);
  	if(curl_errno($ch)) {
  		$result = curl_error($ch);
  	}
    file_put_contents("email_transfer.runs", date("d.m.Y H:i:s ").$result."\n", FILE_APPEND);
    // backup and remove files
    @rename("sendmail_config/access", "sendmail_config_backup/access");
    @rename("sendmail_config/genericstable", "sendmail_config_backup/genericstable");
    @rename("sendmail_config/virtusertable", "sendmail_config_backup/virtusertable");
    @rename("sendmail_config/local-host-names", "sendmail_config_backup/local-host-names");
    @rename("sendmail_config/aliases", "sendmail_config_backup/aliases");
    @rename("sendmail_config/commands", "sendmail_config_backup/commands");
  }
  else {
//    file_put_contents("email_transfer.runs", "nothing to do\n", FILE_APPEND);
  }
