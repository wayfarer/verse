<?php
chdir("..");
include("inc/verse.inc.php"); //main header - initializes Verse environment

if ($user->logged()) {
    $query = "SELECT domain_name FROM sms_domain WHERE domain_id='$domain_id'";
    $domain_name = $db->getOne($query);


    // Obituaries import

    $obit_fields = array(
        "obituary_case_id",
        "first_name",
        "middle_name",
        "last_name",
        "title",
        "suffix",
        "home_place",
        "death_date",
        "birth_date",
        "birth_place",
        "military_status",
        "service_type",
        "service_date",
        "service_time",
        "service_place",
        "service_to_events",
        "service_google_addr",
        "visitation_date",
        "visitation_time",
        "visitation_place",
        "visitation_to_events",
        "final_disposition",
        "finaldisposition_google_addr",
        "movieclips",
        "image",
        "obit_text",
        "candles_policy"
    );

    $obit_fields_order = array_flip($obit_fields);

    $config_fields = array(
        "military_status"
    );

    $event_fields = array(
        "service_to_events",
        "visitation_to_events"
    );

    $obits_file_path = "files/$domain_name/exchange/obituaries.csv";
    $case_id_to_obit_id = array();

    $obits_updated = array();

    if(file_exists($obits_file_path)) {
        $h_obits = fopen($obits_file_path, "r");
        while(($ret = fgetcsv($h_obits, 65535, ";")) !== false) {
            $p = array();
            foreach($obit_fields as $key=>$field) {
                if(!in_array($field, $config_fields) && !in_array($field, $event_fields) && $field != "movieclips") {
                    $p[$field] = @$ret[$key];
                }
            }
            $p["domain_id"] = $domain_id;

            if($p["death_date"]) {
                $p["death_date"] = date("Y-m-d", strtotime($p["death_date"]));
            }
            if($p["birth_date"]) {
                $p["birth_date"] = date("Y-m-d", strtotime($p["birth_date"]));
            }
            if($p["service_date"] && !is_numeric($p["service_date"])) {
                $p["service_date"] = strtotime($p["service_date"]);
            } elseif(!$p["service_date"]) {
                unset($p["service_date"]);
            }
            if($p["visitation_date"] && !is_numeric($p["visitation_date"])) {
                $p["visitation_date"] = strtotime($p["visitation_date"]);
            } elseif(!$p["visitation_date"]) {
                unset($p["visitation_date"]);
            }
            if($p["image"]) {
                $p["image"] = "/image/obituaries/".pathinfo($p["image"], PATHINFO_BASENAME);
            }

            $obituary_case_id = @$ret[$obit_fields_order["obituary_case_id"]];

            $obituary_update = false;

            // check if obituary exists
            $query = "SELECT obituary_id FROM plg_obituary WHERE obituary_case_id='$obituary_case_id' AND domain_id='$domain_id'";
            $obituary_id = $db->getOne($query);
            if ($obituary_id) {
                $query = "UPDATE plg_obituary SET " . make_set_clause_in($p) . " WHERE obituary_id='$obituary_id' AND domain_id='$domain_id'";
                $obituary_update = true;
            }
            else {
                $query = "INSERT plg_obituary SET ".make_set_clause_in($p);
            }
            $res = $db->query($query);
            if(DB::isError($res)) {
                echo "<b>error! ", $res->getMessage(), "</b><br/>";
                echo $res->getUserInfo(), "<br/>";
            }

            $query = "SELECT obituary_id FROM plg_obituary WHERE obituary_case_id='$obituary_case_id' AND domain_id='$domain_id'";
            $obituary_id = $db->getOne($query);
            if ($obituary_id) {
                $case_id_to_obit_id[$obituary_case_id] = $obituary_id;

                if($obituary_update) {
                    $db->query("DELETE FROM plg_obituary_movieclip WHERE obituary_id=$obituary_id");
                    $db->query("DELETE FROM plg_obituary_config WHERE obituary_id=$obituary_id AND domain_id=$domain_id");
                    $db->query("DELETE FROM plg_event WHERE obituary_id=$obituary_id AND domain_id=$domain_id");

                    $obits_updated[$obituary_case_id] = $obituary_id;
                }

                if(@$ret[$obit_fields_order["movieclips"]]) {
                    $movieclips = explode(",", $ret[$obit_fields_order["movieclips"]]);
                    foreach($movieclips as $movieclip) {
                        $query = "INSERT INTO plg_obituary_movieclip SET obituary_id=$obituary_id, movieclip_id=".intval(trim($movieclip));
                        $db->query($query);
                    }
                }

                foreach($config_fields as $config_field) {
                    if($ret[$obit_fields_order[$config_field]]) {
                        $query = "INSERT INTO plg_obituary_config SET domain_id=$domain_id, obituary_id=$obituary_id, param='$config_field', `value`='on'";
                        $db->query($query);
                    }
                }

                foreach($event_fields as $event_field) {
                    if($ret[$obit_fields_order[$event_field]]) {
                        switch($event_field) {
                            case "service_to_events":
                                $query = "INSERT INTO plg_event SET domain_id=$domain_id, obituary_id=$obituary_id, event_type=1, `timestamp`={$p["service_date"]}, description='".in($p["service_place"])."'";
                                break;
                            case "visitation_to_events":
                                $query = "INSERT INTO plg_event SET domain_id=$domain_id, obituary_id=$obituary_id, event_type=2, `timestamp`={$p["visitation_date"]}, description='".in($p["visitation_place"])."'";
                                break;
                            default:
                                break;
                        }
                    }

                    if(isset($query)) {
                        $db->query($query);
                    }
                }
            }
        }
        fclose($h_obits);
        rename($obits_file_path, "files/$domain_name/exchange/backup/obituaries_".date("d-m-Y_H-i").".csv");

        $updated_count = count($obits_updated);
        $imported_count = count($case_id_to_obit_id)-$updated_count;

        echo 'Updated '.$updated_count.' obituaries.<br/>';
        echo 'Imported '.$imported_count.' obituaries.<br/>';
    } else {
        echo $obits_file_path.' file not found.<br/>';
    }



    // Candles import

    $candle_fields = array(
        "obituary_case_id",
        "name",
        "email",
        "thoughts",
        "timestamp"
    );

    $candle_fields_order = array_flip($candle_fields);

    $candles_file_path = "files/$domain_name/exchange/candles.csv";
    if(file_exists($candles_file_path)) {
        $candles_count = 0;
        $candle_obituary_ids = array();

        $h_candles = fopen($candles_file_path, "r");
        while(($ret = fgetcsv($h_candles, 65535, ";")) !== false) {
            $candle_obituary_case_id = $ret[$candle_fields_order["obituary_case_id"]];

            if(!$case_id_to_obit_id[$candle_obituary_case_id]) {
                $query = "SELECT obituary_id FROM plg_obituary WHERE obituary_case_id='$candle_obituary_case_id' AND domain_id='$domain_id'";
                $candle_obituary_id = $db->getOne($query);
            } else {
                $candle_obituary_id = $case_id_to_obit_id[$candle_obituary_case_id];
            }

            if(!$candle_obituary_id || DB::isError($candle_obituary_id)) {
                echo "Obituary not found for $candle_obituary_case_id.<br/>";
            } else {
                $p = array();
                foreach($candle_fields as $key=>$field) {
                    if($field != "obituary_case_id" && $field != "timestamp") {
                        $p[$field] = $ret[$key];
                    }
                }
                $p["timestamp"] = date("Y-m-d", strtotime($ret[$candle_fields_order["timestamp"]]));
                $p["domain_id"] = $domain_id;
                $p["obituary_id"] = $candle_obituary_id;
                $p["state"] = 1;

                $query = "INSERT plg_obituary_candle SET ".make_set_clause_in($p);
                $res = $db->query($query);
                if(DB::isError($res)) {
                    echo "<b>error! ", $res->getMessage(), "</b><br/>";
                    echo $res->getUserInfo(), "<br/>";
                } else {
                    $candles_count++;
                    if(!in_array($candle_obituary_id, $candle_obituary_ids)) {
                        $candle_obituary_ids[] = $candle_obituary_id;
                    }
                }
            }
        }
        fclose($h_candles);
        rename($candles_file_path, "files/$domain_name/exchange/backup/candles_".date("d-m-Y_H-i").".csv");

        echo 'Imported '.$candles_count.' candles for '.count($candle_obituary_ids).' obituaries.<br/>';
    }  else {
        echo $candles_file_path.' file not found.<br/>';
    }

    echo "Done.<br/>";
} else {
    echo "Access denied.";
}

function make_set_clause_in($p) {
    $ret = "";
    foreach($p as $key=>$value) {
        $ret.="$key='".in($value)."',";
    }
    return substr($ret, 0, -1);
}
