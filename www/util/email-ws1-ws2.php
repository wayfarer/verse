<?php
  require("config.inc.php");
  require("util.inc.php");
  require_once("DB.php");

  error_reporting(E_ALL & ~E_NOTICE);
    
  $db_con = createobject("db_versesms");
  $db = $db_con->connect();
  if(DB::isError($db)) {
    echo "<b>error! ", $db->getMessage(), "</b><br>";        
    echo $db->getUserInfo(), "<br>";
    exit;
  }
	
	$query = "SELECT domain_name, name, login, type, forward FROM plg_mailbox INNER JOIN sms_domain d USING(domain_id) WHERE d.enabled=1 AND email_enabled=1 AND mode=0 AND alias_domain_id=0";
	$emails_data = $db->getAssoc($query, true, array(), DB_FETCHMODE_ASSOC, true);
	
	echo "<b>", count($emails_data), " domains</b><br>";
  foreach($emails_data as $domain=>$emails) {
  	echo "<b>$domain</b> (", count($emails), ")<br>";
		foreach($emails as $i=>$email) {
			echo $email["name"],"@$domain - ", ($email["type"]==0?"mailbox":"forward (".$email["forward"].")"), " - xxx", "<br>";
		}
		echo "<br>";
  }

	file_put_contents("email_data.ser", serialize($emails_data));
