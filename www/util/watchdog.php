<?php
  error_reporting(E_ALL);
  ini_set("display_errors", 1);

  chdir("/home/ttt-ws-production/www");  

  require_once 'inc/util.inc.php';
//	require_once 'lib/swiftmailer/swift_required.php';	

	$watchdirs = array("", "templates/");
	
	// read filesystem state
	
	foreach($watchdirs as $watchdir) {	  
		$statefile = "util/".slugify($watchdir).".state";
    $prevstate = unserialize(file_get_contents($statefile));
		$state = read_directory_state($watchdir);
		$diffs = array_diff_assoc($prevstate, $state);		
    $out = "";
		$diff_keys = array();
		if($diffs) {
			foreach($diffs as $key=>$diff) {
      	$diff_keys[$key] = $key;
      }
		}
    $diffs2 = array_diff_assoc($state, $prevstate);    
    if($diffs2) {
      foreach($diffs2 as $key=>$diff) {
        $diff_keys[$key] = $key;
      }
    }
		foreach($diff_keys as $key) {
      if(!isset($prevstate[$key])) {
       $out .= "N: $key: ".$state[$key]." bytes\n";      	
      }
			elseif(!isset($state[$key])) {
       $out .= "D: $key\n";       				
			}
			else {
			 $out .= "C: $key: ".$prevstate[$key]." bytes - ".$state[$key]." bytes\n";
			}
		}

/*    echo "===".nl2br($out)."===<br>";
		var_dump($diffs);
    var_dump($diffs2);
		echo "----<br>";
*/		
		if($out) {
      $out = "Server time: ".date("Y-m-d H:i")."\n\nchanged files:\n\n".$out;

			// send notification e-mail
/*      $transport = Swift_MailTransport::newInstance();
      $mailer = Swift_Mailer::newInstance($transport);

			//Create a message
			$message = Swift_Message::newInstance('watchdog@ws1: filesytem change detected')
			  ->setFrom(array('watchdog@ws1.twintierstech.net'))
			  ->setTo(array('vladimir@mail.by'))
			  ->setBody($out)
			  ;
			  
			//Send the message
			$result = $mailer->send($message);
*/			
			watchdog_log($out); 
			
      $emailfrom = "watchdog@twintierstech.net";
			$emailto = "vladimir@mail.by";
			$subj = "Watchdog@ws0: filesytem change detected";
      $ret = mail($emailto, $subj, $out, "From: $emailfrom", "-f$emailfrom $emailfrom");
		}
		
		file_put_contents($statefile, serialize($state));
	}
//  echo "<br><br>done.";   

	function read_directory_state($dir) {
		$state = array();
		foreach(glob($dir."*") as $filename) {
      $size = filesize($filename);
			$state[$filename] = $size;			
		}
		return $state;
	}

	function file_put_contents($filename, $data, $append = 0) {
	    if(!$append) {
        $f = @fopen($filename, 'a');
	    }
			else {
        $f = @fopen($filename, 'w');        				
			}
	    if (!$f) {
	        return false;
	    } else {
	        $bytes = fwrite($f, $data);
	        fclose($f);
	        return $bytes;
	    }
	}

  function watchdog_log($message) {
	  $logfile = "logs/watchdog.log";
	  $logline = "[".date("Y-m-d H:i:s")."] $message\n";
	  file_put_contents($logfile, $logline, 1);  	
  }
