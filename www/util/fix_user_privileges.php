<?php
// privilege USER_MANAGEMENT had been splitted into two privileges
// duplicate USER_MANAGEMENT privilege in DB
  require("config.inc.php");
  require("util.inc.php");
  require_once("DB.php");
    
  $db_con = createobject("db_versesms");
  $db = $db_con->connect();
  if(DB::isError($db)) {
		echo "<b>error! ", $db->getMessage(), "</b><br>";        
    echo $db->getUserInfo(), "<br>";
    exit;
  }

  ini_set("display_errors","1");

  // fetch existing users with USER_MANAGEMENT privileges
  $query = "SELECT user_id FROM cms_user_role WHERE role_id=1";
  $user_ids = $db->getCol($query);
  
  $pieces = array();
  foreach($user_ids as $user_id) {
    $pieces[] = "($user_id, 12, 'a:0:{}')";
  }

  $query = "INSERT cms_user_role (user_id, role_id, role_params) VALUES ".implode(",", $pieces);
  $db->query($query);

  echo $query,"<br>";

  echo $db->affectedRows(), " rows inserted";

  