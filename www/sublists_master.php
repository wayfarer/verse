<?php
 // sublists management, for chinesecasketdistributor.com
include("inc/verse.inc.php"); //main header - initializes Verse environment

if ($user->have_role(7)) { // create product sublists
    $smarty->display("sublists_master.tpl");
}
else {
    header("Location: login.php");
}