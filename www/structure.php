<?php
 // backend structure integration
include("inc/verse.inc.php"); //main header - initializes Verse environment
include("inc/obittheme.inc.php");

if ($user->have_role(ROLE_CONTENT_MANAGEMENT)) {
    // log usage
    //    $ulogger = &Log::factory('file', 'logs/structure_usage.log', 'versesms');
    //	  $ulogger->info($domain_name.' '.$user->logged_id);
    log_usage('structure', $domain_name, $user->logged_id);

    // get page types
    $query = "SELECT page_type_id page_type, page_type_name FROM cms_page_type";
    $page_types = $db->getAssoc($query);

    // fetch existing pages for obituaries config dialogg
    $query = "SELECT page_id, internal_name FROM cms_page WHERE domain_id='$domain_id' ORDER BY internal_name";
    $pages = $db->getAssoc($query);

    // prepare music files list
    $music_files = array_merge(array("" => "No music by default"), get_music_files());

    $group_ids = $db->getCol("SELECT lg.id FROM sms_domain_link_group lg JOIN sms_domain_link l ON lg.id=l.group_id WHERE is_shared_pages='1' AND domain_id='$domain_id'");
    if(count($group_ids)) {
        $_SESSION['is_shared_pages'] = 1;
        $_SESSION['group_ids'] = $group_ids;
        //add classes to pages from another domain
        $domain_ids = $db->getCol("SELECT DISTINCT d.domain_id FROM sms_domain d JOIN cms_page USING(domain_id) JOIN sms_domain_link_page USING(page_id) WHERE group_id IN (".implode(', ', $group_ids).")");
        $classes = array();
        $i = 1;
        foreach ($domain_ids as $d_id) {
            if ($d_id == $domain_id) {
                $classes[$d_id] = '';
            } else {
                $classes[$d_id] = ' page-'.$i;
                if ($i < 5) {
                    $i++;
                } else {
                    $i = 1;
                }
            }
        }
    } else {
        $_SESSION['is_shared_pages'] = 0;
        $classes[$domain_id] = ' page-0';
    }

    $_SESSION['classes'] = $classes;

    // check if structure is shared
    $shared_structure_group_ids = $db->getAll("SELECT lg.id, lg.group_name FROM sms_domain_link_group lg JOIN sms_domain_link l ON lg.id=l.group_id WHERE is_shared_structure='1' AND domain_id='$domain_id'", DB_FETCHMODE_ASSOC);
    if(count($shared_structure_group_ids)) {
        $parent_domain = $db->getRow("SELECT d.domain_id, d.domain_name FROM sms_domain_link JOIN sms_domain d USING(domain_id) WHERE group_id='".$shared_structure_group_ids[0]['id']."' ORDER BY id", DB_FETCHMODE_ASSOC);
        if($domain_id != $parent_domain['domain_id']) {
            $overwrite_properties_message = '<div class="rewrite-properties">This domain is in "'.$shared_structure_group_ids[0]['group_name'].'" domain group. Site properties are overwritten by properties of "'.$parent_domain['domain_name'].'"</div>';
        }
    }


    $smarty->assign("page_types", $page_types);
    $smarty->assign("pages", $pages);
    $smarty->assign("music_files", $music_files);
    $smarty->assign("uc_mode", ($domain->get("mode") > 0));

    $smarty->assign("border_styles", $theme_frames);
    $smarty->assign("theme_images", $theme_images);

    $smarty->assign("overwrite_properties_message", isset($overwrite_properties_message) ? $overwrite_properties_message : '');

    $smarty->display("structure.tpl");
}
else {
    header("Location: login.php");
}