<?php
 // backend candle reports
include("inc/verse.inc.php"); //main header - initializes Verse environment

if ($user->is_super()) {
    include_once('inc/open_flash_chart_object.php');
    include_once('inc/open-flash-chart.php');
    //  	$_POST = utf8_to_latin($_POST);

    $valid_periods = array("day", "week", "month", "year");

    $action = @$_GET["action"];
    $period = @$_GET["period"];
    $date = @$_GET["date"];
    $domain = @$_GET["domain"];
    $rperiod = @$_GET["rperiod"];

    // default period
    if (!isset($_SESSION["crp_period"])) {
        $_SESSION["crp_period"] = "month";
    }

    if ($period && in_array($period, $valid_periods) && $date == "reset") {
        $_SESSION["crp_period"] = $period;
    }

    // default periods
    if (!isset($_SESSION["crp_periods"]) || $date == "reset") {
        $ts = time();
        $i = 0;
        $_SESSION["crp_periods"] = array();
        while ($i++ < 3) {
            switch ($_SESSION["crp_period"]) {
                case "day":
                    $date_from = date("Y-m-d", $ts);
                    $date_to = $date_from;
                    break;
                case "week":
                    $date_from = date("Y-m-d", strtotime("last Monday", $ts));
                    $date_to = date("Y-m-d", strtotime("next Sunday", $ts));
                    break;
                case "month":
                    $date_from = date("Y-m-d", mktime(0, 0, 0, date("m", $ts), 1, date("Y", $ts)));
                    $date_to = date("Y-m-d", mktime(0, 0, 0, date("m", $ts) + 1, 0, date("Y", $ts)));
                    break;
                case "year":
                    $date_from = date("Y-m-d", mktime(0, 0, 0, 1, 1, date("Y", $ts)));
                    $date_to = date("Y-m-d", mktime(0, 0, 0, 12, 31, date("Y", $ts)));
                    break;
            }
            $_SESSION["crp_periods"] = array($date_from => array("from" => $date_from, "to" => $date_to, "period" => $_SESSION["crp_period"])) + $_SESSION["crp_periods"];
            // fetch prev period
            $ts = strtotime("-1 " . $_SESSION["crp_period"], $ts);
        }
    }

    // periods processing
    if ($period && in_array($period, $valid_periods) && $date && $date != "reset") {
        // add period
        switch ($period) {
            case "day":
                $date_from = $date;
                $date_to = $date;
                break;
            case "week":
                $ts = strtotime($date);
                $date_from = date("Y-m-d", strtotime("last Monday", $ts));
                $date_to = date("Y-m-d", strtotime("next Sunday", $ts));
                break;
            case "month":
                $ts = strtotime($date);
                $date_from = date("Y-m-d", mktime(0, 0, 0, date("m", $ts), 1, date("Y", $ts)));
                $date_to = date("Y-m-d", mktime(0, 0, 0, date("m", $ts) + 1, 0, date("Y", $ts)));
                break;
            case "year":
                $ts = strtotime($date);
                $date_from = date("Y-m-d", mktime(0, 0, 0, 1, 1, date("Y", $ts)));
                $date_to = date("Y-m-d", mktime(0, 0, 0, 12, 31, date("Y", $ts)));
                break;
        }
        if (!period_exists($date_from, $period)) {
            $_SESSION["crp_periods"][$date_from] = array("from" => $date_from, "to" => $date_to, "period" => $period);
        }
        ksort($_SESSION["crp_periods"]);
    }

    // domains processing
    if (!is_null($domain)) {
        if ($domain == 0) {
            $_SESSION["crp_domains"] = array();
        }
        else {
            if (isset($_SESSION["crp_domains"])) {
                $_SESSION["crp_domains"][] = $domain;
            }
            else {
                $_SESSION["crp_domains"] = array($domain);
            }
        }
    }
    if ($rperiod) {
        foreach ($_SESSION["crp_periods"] as $key => $value) {
            if ($key == $rperiod) {
                unset($_SESSION["crp_periods"][$key]);
                break;
            }
        }
    }

    switch ($action) {
        case "draw":
            // domains
            if (count($_SESSION["crp_domains"]) == 0) {
                // fetch top 7 candle domains for last 6 monthes
                $query = "SELECT domain_id, count(*) cnt FROM plg_obituary_candle WHERE timestamp>=DATE_SUB(CURDATE(), INTERVAL 6 MONTH) GROUP BY domain_id ORDER BY cnt DESC LIMIT 7";
                $ret = $db->getAll($query, DB_FETCHMODE_ASSOC);
                $domains = array();
                foreach ($ret as $row) {
                    $domains[] = $row["domain_id"];
                }
                $_SESSION["crp_domains"] = $domains;
            }
            else {
                $domains = $_SESSION["crp_domains"];
            }

            // fetch data for periods
            $candle_data = array();
            foreach ($_SESSION["crp_periods"] as $key => $period) {
                $query = "SELECT domain_id, count(*) cnt FROM plg_obituary_candle WHERE timestamp>'" . $period["from"] . "' AND timestamp<='" . $period["to"] . " 23:59:59' GROUP BY domain_id ORDER BY domain_id";
                $ret = $db->getAll($query, DB_FETCHMODE_ASSOC);
                foreach ($ret as $row) {
                    $candle_data[$key][$row["domain_id"]] = $row["cnt"];
                }
            }
//        var_dump($_SESSION["crp_periods"]);
//        exit;
            // fetch domain names - this is aspect
            $query = "SELECT domain_id, domain_name FROM sms_domain WHERE domain_id IN (" . implode(",", $domains) . ") ORDER BY domain_id";
            $aspect = $db->getAssoc($query);

            // prepare data sets
            $data = array();
            foreach ($_SESSION["crp_periods"] as $period_key => $period) {
                $period_data = array();
                foreach ($aspect as $aspect_key => $val) {
                    $period_data[] = $candle_data[$period_key][$aspect_key] ? $candle_data[$period_key][$aspect_key]
                            : 0;
                }
                $data[$period_key] = array("hint" => format_period($period), "data" => $period_data);
            }
//        var_dump($data);
//        exit;
            // x_labels orientation
            echo draw_cmp_report("Candles counts per periods by domains", "candle counts", $aspect, $data, 1);
            break;
        default:
            // fetch domains
            $query = "SELECT d.domain_id, domain_name, count(*) cnt FROM sms_domain d INNER JOIN plg_obituary_candle USING(domain_id) WHERE mode<2 AND alias_domain_id=0 GROUP BY domain_id ORDER BY domain_name";
            $ret = $db->getAll($query, DB_FETCHMODE_ASSOC);
            $domains = array(0 => "Top 7 domains (reset)");
            foreach ($ret as $row) {
                $domains[$row["domain_id"]] = $row["domain_name"] . " (" . $row["cnt"] . ")";
            }
            $ofc_object = open_flash_chart_object_str(750, 500, 'candles_report_periods.php?action=draw');

            $smarty->assign("domains", $domains);
            $smarty->assign("ofc_object", $ofc_object);
            $smarty->assign("date", date("Y-m-d"));
            $smarty->display("candles_report_periods.tpl");
            break;
    }
}
else {
    header("Location: login.php");
}

// $data - array of data arrays(hint=>hint, data=>array(data));
function draw_cmp_report($title, $y_legend, $x_labels, $data, $x_orient = 0) {
    $colors = array('#0066CC', '#9933CC', '#D83A45', '#639F45', '#C85817', '#243B3B', '#A9A83C');
    // create graph
    $g = new graph();
    $g->bg_colour = '#EDFAFF';
    $g->x_axis_colour('#808080', '#808080');
    $g->y_axis_colour('#808080', '#808080');
    $g->title($title, '{font-size: 18px;}');
    $g->set_x_labels($x_labels);
    $g->set_x_label_style(10, '#2378AF', $x_orient); // size, color, orient
    $g->set_y_legend($y_legend, 12, '#2378AF');
    $g->set_tool_tip("#x_label#<br>#key# - #val# candle(s)<br><br>Click to remove period from graph");
    // create and insert data
    $max = 0;
    $ci = 0;
    foreach ($data as $key => $values) {
        $hint = $values["hint"];
        $local_max = max($values["data"]);
        if ($local_max > $max) $max = $local_max;

        $bar = new bar(75, $colors[$ci++ % count($colors)]);
        $bar->key($hint, 10);
        //      $bar->data = $values["data"];
        foreach ($values["data"] as $value) {
            $bar->add_link($value, "javascript:remove_period('$key')");
        }
        $g->data_sets[] = $bar;
    }

    $g->set_y_max($max);
    return $g->render();
}

function format_period($period) {
    $ret = "";
    switch ($period["period"]) {
        case "day":
            $ret = $period["from"];
            break;
        case "week":
            $ts_from = strtotime($period["from"]);
            $ret = "Week " . date("F d, Y", $ts_from);
            break;
        case "month":
            $ts_from = strtotime($period["from"]);
            $ret = date("F Y", $ts_from);
            break;
        case "year":
            $ts_from = strtotime($period["from"]);
            $ret = date("Y", $ts_from);
            break;
    }
    return $ret;
}

function period_exists($date_from, $aperiod) {
    foreach ($_SESSION["crp_periods"] as $period) {
        if ($period["from"] == $date_from && $period["period"] == $aperiod) return true;
    }
    return false;
}