<?php

    $echo_error_message = true;

    $referer_domain_name = parse_url($_SERVER["HTTP_REFERER"], PHP_URL_HOST);

    $code = $_REQUEST['code'];
    if(!empty($code)) {
        include_once("inc/verse.inc.php");
        include_once($_SERVER['DOCUMENT_ROOT'] . '/../symfony/config/ProjectConfiguration.class.php');
        $configuration = ProjectConfiguration::getApplicationConfiguration('backend', 'prod', false);

        $token_url = "https://graph.facebook.com/oauth/access_token?client_id=".sfConfig::get('app_facebook_app_id')."&redirect_uri="
                    . urlencode('http://'.sfConfig::get('app_facebook_app_redirect_domain').'/facebook.php')
                    . "&client_secret=".sfConfig::get('app_facebook_app_secret')."&code=" . $code;

        $response = file_get_contents($token_url);
        $params = null;
        parse_str($response, $params);

        if(!empty($params["access_token"])) {
            $domain_id = $db->getOne("SELECT domain_id FROM sms_domain WHERE domain_name='$referer_domain_name'");

            $user_access_token = $db->getOne("SELECT value FROM plg_obituary_config WHERE domain_id=$domain_id AND obituary_id=0 AND param='facebook_user_access_token'");
            if($user_access_token) {
                $ret = $db->query("UPDATE plg_obituary_config SET value='".$params["access_token"]."' WHERE domain_id=$domain_id AND obituary_id=0 AND param='facebook_user_access_token'");
            } else {
                $ret = $db->query("INSERT INTO plg_obituary_config VALUES ($domain_id, 0, 'facebook_user_access_token', '".$params["access_token"]."')");
            }
            if($ret) {
                $echo_error_message = false;
            }
        }
    }

    if($echo_error_message) {
        echo "There are some errors. The application wasn't added to \"$referer_domain_name\".";
    } else {
        echo "The application was successfully added to \"$referer_domain_name\".";
    }
