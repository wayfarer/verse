<?php
 // frontend quick obituary edit feature
include("inc/verse.inc.php"); //main header - initializes Verse environment

if (@$_SESSION["obit_edit"]) {
    $_POST = utf8_to_latin($_POST);
    $action = $_POST["action"];

    switch ($action) {
        case "load":
            $obituary_id = intval(@$_POST["id"]);
            $query = "SELECT first_name, middle_name, last_name, home_place, DATE_FORMAT(death_date, '" . DB_DATE_FORMAT . "') death_date, birth_place, DATE_FORMAT(birth_date, '" . DB_DATE_FORMAT . "') birth_date, FROM_UNIXTIME(service_date, '" . DB_DATETIME_FORMAT . "') service_date, service_place, visitation_date, visitation_place, final_disposition, image, obit_text FROM plg_obituary WHERE obituary_id='$obituary_id' AND domain_id='$domain_id'";
            $ret = $db->getRow($query, DB_FETCHMODE_ASSOC);
            // check for associated events
            $query = "SELECT 1 FROM plg_event WHERE obituary_id='$obituary_id' AND event_type='1' AND domain_id='$domain_id'";
            $ret["add_calendar_sp"] = $db->getOne($query);
            $query = "SELECT 1 FROM plg_event WHERE obituary_id='$obituary_id' AND event_type='2' AND domain_id='$domain_id'";
            $ret["add_calendar_vp"] = $db->getOne($query);
            header("X-JSON:" . make_json_response($ret));
            break;
        case "save":
            $obituary_id = intval(@$_POST["id"]);
            $p = array();
            $p["first_name"] = in(substr(@$_POST["first_name"], 0, 32));
            $p["middle_name"] = in(substr(@$_POST["middle_name"], 0, 32));
            $p["last_name"] = in(substr(@$_POST["last_name"], 0, 32));
            $p["home_place"] = in(substr(@$_POST["home_place"], 0, 128));
            $p["death_date"] = in(@$_POST["death_date"]);
            $p["birth_date"] = in(@$_POST["birth_date"]);
            $p["birth_place"] = in(substr(@$_POST["birth_place"], 0, 128));
            if ($_POST["service_date"]) {
                $p["service_date"] = @strtotime($_POST["service_date"]);
            }
            $p["service_place"] = in(substr(@$_POST["service_place"], 0, 128));
            $p["visitation_date"] = in($_POST["visitation_date"]);
            $p["visitation_place"] = in(substr(@$_POST["visitation_place"], 0, 128));
            $p["final_disposition"] = in(substr(@$_POST["final_disposition"], 0, 128));
            $p["image"] = in(@$_POST["image"]);
            $p["obit_text"] = in(@$_POST["obit_text"]);
            if ($obituary_id) {
                $query = "UPDATE plg_obituary SET " . make_set_clause($p) . " WHERE obituary_id='$obituary_id' AND domain_id='$domain_id'";
                $db->query($query);
            }
            else {
                die;
            }
            // manage associated events events if asked
            if (isset($_POST["add_calendar_sp"])) {
                // add or replace service place
                $description = "Auto: Service for {$p['first_name']} {$p['middle_name']} {$p['last_name']}";
                $query = "REPLACE plg_event SET domain_id='$domain_id', obituary_id='$obituary_id', event_type='1', timestamp='" . $p["service_date"] . "', description='$description'";
            }
            else {
                // try to delete service place if event associated with obituary exists
                $query = "DELETE FROM plg_event WHERE domain_id='$domain_id' AND obituary_id='$obituary_id' AND event_type='1'";
            }
            $db->query($query);

            if (isset($_POST["add_calendar_vp"])) {
                // add visitation place
                $description = "Auto: Visitation for {$p['first_name']} {$p['middle_name']} {$p['last_name']}";
                $query = "REPLACE plg_event SET domain_id='$domain_id', obituary_id='$obituary_id', event_type='2', timestamp='" . $p["visitation_date"] . "', description='$description'";
            }
            else {
                // try to delete visitation place if event associated with obituary exists
                $query = "DELETE FROM plg_event WHERE domain_id='$domain_id' AND obituary_id='$obituary_id' AND event_type='2'";
            }
            $db->query($query);
            echo "ok";
            break;
    }
}

if (@$_SESSION["remove_candle"]) {
    $action = $_POST["action"];

    switch ($action) {
        case "delete_candle":
            $candle_id = intval(@$_POST["candle_id"]);
            $obituary_id = intval(@$_POST["obituary_id"]);
//			$query = "DELETE FROM plg_obituary_candle c INNER JOIN plg_obituary o USING(obituary_id) WHERE candle_id='$candle_id' AND c.obituary_id='$obituary_id' AND domain_id='$domain_id'";
            // TODO: add domain_id checking by MySQL means
            $query = "DELETE FROM plg_obituary_candle WHERE candle_id='$candle_id' AND obituary_id='$obituary_id'";
            $db->query($query);
            echo $query;
            break;
    }
}
