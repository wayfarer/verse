<?php
 // frontend videoplayer
include("inc/verse.inc.php"); //main header - initializes Verse environment

$movieclip_id = intval(@$_GET["id"]);
if (isset($_POST["mcpass"])) {
    $_SESSION["mc_passes"][$movieclip_id] = $_POST["mcpass"];
}

$query = "SELECT domain_name, filename, title, protected, password, FROM_UNIXTIME(timestamp,'" . DB_DATETIME_FORMAT . "') timestamp FROM plg_movieclip JOIN sms_domain USING(domain_id) WHERE movieclip_id='$movieclip_id'";
$movieclip = $db->getRow($query, DB_FETCHMODE_ASSOC);
if ($movieclip) {
    $allowed = !$movieclip["protected"];
    if ($movieclip["protected"] && $_SESSION["mc_passes"][$movieclip_id] == $movieclip["password"]) {
        $allowed = 1;
    }
    unset($movieclip["protected"]);
    unset($movieclip["password"]);
    $movieclip["allowed"] = $allowed;
    if (!$allowed && isset($_POST["mcpass"])) {
        $movieclip["authfail"] = true;
    }

    $smarty->assign("movieclip", $movieclip);
    $smarty->display("mcplayer.tpl");
}
