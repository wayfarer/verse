<?php
include_once __DIR__.'/../inc/imageutil.inc.php';

use Verse\Obituary\ObituarySearchCriterion;
use Verse\Obituary\ObituarySearchForm;
use Verse\Obituary\ObituarySearcher;
use Verse\Obituary\ObituaryHomePlaces;
use Verse\Paginator;
use Verse\PaginatorExtension;

$ap = new Silex\ControllerCollection();

$ap->match('/online-obituary', function (Silex\Application $app) {
//    $app['db.config']->setSQLLogger(new \Doctrine\DBAL\Logging\EchoSQLLogger());
    $domain = $app['request_context']->getParameter('domain');
    $obituarySearchCriterion = $app['session']->get('obituarySearchCriterion');
    if(!$obituarySearchCriterion) {
        $obituarySearchCriterion = new ObituarySearchCriterion($domain);
    }
    else {
        $obituarySearchCriterion->setDomain($domain);
    }
    $home_places_obj = new ObituaryHomePlaces($app['db'], $obituarySearchCriterion);
    $home_places = $home_places_obj->getAll();

    $form = $app['form.factory']->createBuilder(new ObituarySearchForm($home_places, $domain), $obituarySearchCriterion)
//                                ->addValidator($app['validator'])
                                ->getForm();

    if($app['request']->getMethod() == 'POST') {
        // validation won't work for some reason for now
        // waiting for official FormExtension release

        // TODO: validate for domain_id in group domain ids

        if(!$app['request']->get('reset_x')) {
            $form->bindRequest($app['request']);
            // $ret = $app['validator']->validate($obituarySearchCriterion);
    //        if($form->isValid()) {
                $app['session']->set('obituarySearchCriterion', $obituarySearchCriterion);
                $app['session']->save();
    //        }
        }
        else {
            $obituarySearchCriterion = new ObituarySearchCriterion($domain);
            $app['session']->set('obituarySearchCriterion', $obituarySearchCriterion);
            $app['session']->save();
            return $app->redirect($app['request']->getPathInfo());
        }
    }
    $paginator = null;
    //if($obituarySearchCriterion->notEmpty()) {
        $obitSearcher = new ObituarySearcher($app['db'], $obituarySearchCriterion, $app['request']->get('order'), $app['request']->get('dest'));
        $app['obitSearcher'] = $obitSearcher;
        $paginator = new Paginator($obitSearcher, $app['request']->get('page', 1), 10);
    //}

    return $app['twig']->render('obituaries.twig', array(
        'form' => $form->createView(),
        'paginator' => $paginator,
        'theme' => $_SESSION['theme']
    ));
});

function paginate(Paginator $paginator) {
    global $app;

    return $app['twig']->render('paginator.twig', array(
        'paginator' => $paginator
    ));
}

function order($link_text, $order_by) {
    global $app;

    $page = $app['request']->get('page');
    $order = $app['obitSearcher']->get('order');
    $order_dest = $app['obitSearcher']->get('order_dest');
    $class = '';

    if($order == $order_by) {
        if($order_dest=='desc') {
            $order_dest = '';
        }
        else {
            $order_dest = 'desc';
        }
        $class='class="obit-search-order-selected" ';
    }
    else {
        if($order_by=='death_date') {
            $order_dest = 'desc'; // default order for death date is descending
        }
        else {
            $order_dest = '';
        }
    }

    if($page) {
        $page="page=$page&";
    }

    if($order_dest) {
        $order_dest = '&dest='.$order_dest;
    }
    
    return "<a {$class}href=\"?{$page}order=$order_by$order_dest\">$link_text</a>";
}

function page($link_text, $page = null) {
    global $app;

    if(!$page) {
        $page = $link_text;
    }

    $order = $app['request']->get('order');
    $order_dest = $app['request']->get('dest');

    if($order) {
        $order = "&order=$order";
    }
    if($order_dest) {
        $order_dest = "&dest=$order_dest";
    }

    return "<a href=\"?page=$page$order$order_dest\">$link_text</a>";
}

function truncate($string, $symbols) {
    if(mb_strlen($string) > $symbols) {
        $string = mb_substr($string, 0, $symbols)."...";
    }
    return $string;
}

// stub to run without verse
if(!function_exists('leading_slash')) {
    function leading_slash($path) {
        return $path;
    }
}

return $ap;
