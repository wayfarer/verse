<?php

require_once __DIR__.'/../silex.phar';
require_once __DIR__.'/../config/config.php';

$app = new \Silex\Application();
$app['debug'] = true;
$app['autoloader']->registerNamespace('Verse', 'src');
$app['autoloader']->registerNamespace('Symfony', 'vendor');

$app->register(new Silex\Provider\SessionServiceProvider());

$app->register(new Silex\Provider\SymfonyBridgesServiceProvider(), array(
    'symfony_bridges.class_path' => __DIR__.'/../vendor',
));

$app->register(new Silex\Provider\DoctrineServiceProvider(), array(
    'db.options' => array(
        'dbname' => Config\DB\NAME,
        'host' => Config\DB\HOST,
        'user' => Config\DB\USER,
        'password' => Config\DB\PASS,
        'driver' => 'pdo_mysql'
    ),
    'db.dbal.class_path'    => __DIR__.'/../vendor/doctrine-dbal/lib',
    'db.common.class_path'  => __DIR__.'/../vendor/doctrine-common/lib'
));

$app->register(new Silex\Provider\ValidatorServiceProvider(), array(
    'validator.class_path'    => __DIR__.'/../vendor/Symfony/Component',
));

$app->register(new Silex\Provider\FormServiceProvider(), array(
    'form.class_path' => __DIR__.'/../vendor/Symfony/Component',
));

$app->register(new Silex\Provider\TwigServiceProvider(), array(
    'twig.path'       => __DIR__.'/../views',
    'twig.class_path' => __DIR__.'/../vendor/twig/lib',
    'twig.options'    => array(
//    'cache' => 'cache',
//    'debug' => true
    )
));
