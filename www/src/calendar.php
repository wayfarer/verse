<?php
use Verse\Calendar\CalendarEventSearcher;

$ap = new Silex\ControllerCollection();

$ap->match('/calendar', function (Silex\Application $app) {
    $domain = $app['request_context']->getParameter('domain');

    if (isset($_GET['t'])) {
        $now = $_GET['t'];
    } else {
        $now = time();
    }

    // fetch periods's events
    $calendar = new CalendarEventSearcher($app['db'], $domain);
    $data = $calendar->getData($now);
    
    return $app['twig']->render('calendar.twig', array('data' => $data));
});

return $ap;