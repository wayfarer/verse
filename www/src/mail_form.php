<?php

use Verse\MailForm\MailForm;

$ap = new Silex\ControllerCollection();

$ap->match('/{page}', function (Silex\Application $app, $page) {
    $domain = $app['request_context']->getParameter('domain');

    $form = $app['form.factory']->createBuilder(new MailForm())
//                                ->addValidator($app['validator'])
                                ->getForm();

    if($app['request']->getMethod() == 'POST') {
        $ips = array();
        $block_ips = $app['db']->fetchAll("SELECT ip FROM plg_block_ip");
        foreach ($block_ips as $block_ip) {
            $ips[] = $block_ip['ip'];
        }

        if (!in_array($_SERVER['REMOTE_ADDR'], $ips)) {
            $form->bindRequest($app['request']);

            // fetch default email for the domain
            $email = $app['db']->fetchColumn("SELECT email FROM sms_domain WHERE domain_id='" . $domain->getDomainId() . "'");

            $result = "IP address: ".$_SERVER['REMOTE_ADDR']."\n";
            $replyto = "";
            foreach ($form->getData() as $key => $value) {
                $result .= "$key: $value\n";
                if ($key == "email" && check_email($value)) {
                    $replyto = $value;
                }
            }

            $subject = $domain->getDomainName().": mail form message";
            $hello = "Hello, this is the message from your website ".$domain->getDomainName().".\nSomeone has filled mail form with next values:";
            $signature = "Sincerely yours, webform robot.";
            $result = $hello . "\n\n" . $result . "\n\n" . $signature;

            if ($replyto) $replyto = "\r\nReply-To: $replyto";

            mail($email, $subject, $result, "From: noreply@twintierstech.net".$replyto);
        }
        return "<div>Thanks for contacting us</div>";
    }

    return $app['twig']->render('mail_form.twig', array(
        'form' => $form->createView()
    ));
});

return $ap;