<?php

namespace Verse\Calendar;

use Doctrine\DBAL\Connection as DoctrineConnection;

class CalendarEventSearcher {
    protected   $db, $domain;

    public function __construct(DoctrineConnection $db, $domain) {
        $this->db = $db;
        $this->domain = $domain;
    }

    public function getData($now) {
        //$now = strtotime('01 February 2011');
        $period = $this->getPeriods($now);
        return array(
            'date' =>date("F Y", $now),
            'items' => $this->getItems($now),
            'next' => $period['end'] + 3600,
            'prev' => $period['begin'] - 3600
        );
    }

    public function getItems($now) {
        $events = $this->getEvents($now);

        $items = array();

        // cycle through each day of period
        $i = 0;
        $periods = $this->getPeriods($now);
        $timestamp = $periods['begin'];

        static $class;

        while ($timestamp < $periods['end']) {
            $day_begin = mktime(0, 0, 0, date("m", $timestamp), date("d", $timestamp), date("Y", $timestamp));
            $day_end = strtotime("23:59:59", $day_begin);
            $item_events = array();
            while ($i < count($events) && $events[$i]["timestamp"] >= $day_begin && $events[$i]["timestamp"] <= $day_end) {
                if ($events[$i]["first_name"] && strcasecmp(substr($events[$i]["description"], 0, 3), "<B>") && strcasecmp(substr($events[$i]["description"], 0, 8), "<STRONG>")) {
                    switch ($events[$i]["event_type"]) {
                        case 1:
                            $event_type = "Service";
                            $time = $events[$i]["service_time"];
                            break;
                        case 2:
                            $event_type = "Calling Hours";
                            $time = $events[$i]["visitation_time"];
                            break;
                    }
                    $item_events[] = array(
                        'name' => $events[$i]["first_name"] . " " . $events[$i]["middle_name"] . " " . $events[$i]["last_name"],
                        'type' => $event_type,
                        'time' => $time,
                        'description' => $events[$i]["description"],
                        'obituary_id' => $events[$i]["obituary_id"],
                        'obituary_slug' => get_obituary_slug($events[$i])
                    );
                }
                
                $i++;
            }

            if ($timestamp == $periods['begin']) {
                $class = 'odd';
            } elseif (date('w', $timestamp) == '1') {
                $class = ($class == 'odd') ? 'even' : 'odd';
            }
            $items[date("d", $timestamp)] = array("date" =>date("d", $timestamp), "weekday" => date("l", $timestamp), "events" => $item_events, "class" => $class);

            $timestamp = strtotime("next day", $timestamp);
        }
        
        return $items;
    }

    public function getEvents($now) {
        $periods = $this->getPeriods($now);
        $query = "SELECT timestamp, description, o.obituary_id, o.slug, first_name, middle_name, last_name, event_type, death_date, service_time, visitation_time FROM plg_event e LEFT JOIN plg_obituary o USING(obituary_id) WHERE timestamp>='".$periods['begin']."' AND timestamp<='".$periods['end']."' ".$this->getDomainWhere()." ORDER BY timestamp";
        $events = $this->db->fetchAll($query);

        return $events;
    }

    public function getPeriods($now) {
        return array(
            'begin' => mktime(0, 0, 0, date("m", $now), 1, date("Y", $now)),
            'end' => mktime(23, 59, 59, date("m", $now) + 1, 0, date("Y", $now))
        );
    }

    public function getDomainWhere() {
        $shared_obit_domain_ids = $this->domain->getSharedObitDomainIds();
        if (count($shared_obit_domain_ids)) {
            return "AND e.domain_id IN (".implode(", ", $shared_obit_domain_ids).")";
        } else {
            return "AND e.domain_id='".$this->domain->getDomainId()."'";
        }
    }
}