<?php
namespace Verse\MailForm;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;

class MailForm extends AbstractType {
    
    public function buildForm(FormBuilder $builder, array $options) {
        $builder->add('name', 'text', array('label' => 'Name'));
        $builder->add('company', 'text', array('label' => 'Company'));
        $builder->add('email', 'email', array('label' => 'Email'));
        $builder->add('phone', 'integer', array('label' => 'Phone'));
        $builder->add('message', 'textarea', array('label' => 'Message'));
    }

    public function getName() {
        return "mail_form_new";
    }
}

