<?php
namespace Verse\Domain;
 
class Domain {
    protected
        $db = null,
        $domain_id = null,
        $domain_name = null,
        $shared_obit_domain_ids = null,
        $shared_obit_domain_group_ids = null,
        $is_parent = null;

    function __construct($db, $domain_id) {
        $this->db = $db;
        $this->domain_id = $domain_id;
        $this->domain_name = $db->fetchColumn("SELECT domain_name FROM sms_domain WHERE domain_id=:domain_id", array('domain_id' => $domain_id));
        $this->shared_obit_domain_group_ids = $this->getSharedObitDomainGroupIds();
        $this->is_parent = $this->isParent();
    }

    public function getDomainId() {
        return $this->domain_id;
    }

    public function getDomainName() {
        return $this->domain_name;
    }

    public function getSharedObitDomainIds() {
        if(!$this->shared_obit_domain_ids) {
            $group_ids = $this->shared_obit_domain_group_ids;

            if ($group_ids) {
                // Parent domains which are shared to children
                $shared_parent_domain_ids = array();

                foreach($group_ids as $key => $group_id) {
                    $parent_domain_id = $this->getDomainGroupParentDomainId($group_id);
                    if($parent_domain_id != $this->domain_id) {
                        unset($group_ids[$key]);

                        $share_obits_from_parent = $this->db->fetchColumn("SELECT value FROM sms_domain_link_group_config WHERE group_id=:group_id AND param='share_obits_from_parent' LIMIT 1", array('group_id' => $group_id));
                        if($share_obits_from_parent) {
                            $shared_parent_domain_ids[] = $parent_domain_id;
                        }
                    }
                }

                if($group_ids) {
                    // Get all domains from domain groups where this domain is parent
                    $this->shared_obit_domain_ids = $this->db->executeQuery('SELECT DISTINCT domain_id FROM sms_domain_link WHERE group_id IN ('.implode(", ", $group_ids).')')->fetchAll(\PDO::FETCH_COLUMN);

                    // Add shared parent domains from other groups
                    $this->shared_obit_domain_ids = array_merge($this->shared_obit_domain_ids, $shared_parent_domain_ids);
                }

                // Add shared parent domains
                if(!$this->shared_obit_domain_ids) {
                    $shared_parent_domain_ids[] = $this->domain_id;
                    $this->shared_obit_domain_ids = $shared_parent_domain_ids;
                }
            }
            
            if(!$this->shared_obit_domain_ids) {
                $this->shared_obit_domain_ids = array($this->domain_id);
            }
        }
        return $this->shared_obit_domain_ids;
    }

    public function isParent() {
        $is_parent = $this->is_parent;

        if(is_null($is_parent)) {
            $is_parent = false;

            $group_ids = $this->shared_obit_domain_group_ids;
            if ($group_ids) {
                foreach($group_ids as $group_id) {
                    $parent_domain_id = $this->getDomainGroupParentDomainId($group_id);
                    if($parent_domain_id == $this->domain_id) {
                        $is_parent = true;
                        break;
                    }
                }
            }
        }

        return $is_parent;
    }

    protected function getDomainGroupParentDomainId($group_id) {
        return $this->db->fetchColumn("SELECT domain_id FROM sms_domain_link WHERE group_id=:group_id ORDER BY id LIMIT 1", array('group_id' => $group_id));
    }

    protected function getSharedObitDomainGroupIds() {
        return $this->db->executeQuery('SELECT lg.id FROM sms_domain_link l JOIN sms_domain_link_group lg ON lg.id=l.group_id WHERE lg.is_shared_obits="1" AND l.domain_id=:domain_id', array('domain_id'=>$this->domain_id))->fetchAll(\PDO::FETCH_COLUMN);
    }

    public function getSharedObitDomains() {
        $site_names = $this->db->executeQuery('SELECT domain_id, title FROM cms_site WHERE domain_id IN ('.implode(", ", $this->getSharedObitDomainIds()).')')->fetchAll(\PDO::FETCH_NAMED);

        $domains = array();
        foreach($site_names as $site) {
            $domains[$site['domain_id']] = $site['title'];
        }
        return $domains;
    }

    public function isSingle() {
        return count($this->getSharedObitDomainIds()) == 1;
    }
}
