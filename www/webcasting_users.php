<?php
// obsolete
include ("inc/verse.inc.php"); //main header - initializes Verse environment

if ($user->have_role(ROLE_WEBCASTING_MANAGEMENT)) {
    $action = @$_POST["action"];
    if (!$action) {
        $webcasting_id = @$_GET["id"];
        $query = "SELECT 1 FROM cms_webcasting WHERE webcasting_id='$webcasting_id' AND domain_id='$domain_id'";
        $ret = $db->getOne($query);
        if (!$ret) {
            header("Location: webcasting.php");
            exit;
        }
    }

    switch ($action) {
        case "list":
            $webcasting_id = intval(@$_POST["id"]);

            $rep = createobject("report_ajax", array($db, "cms_access_user", REP_WITH_PAGENATOR, true, 30, 'width="650px" style="background-color: #DDEEFF"'));

            $rep->add_field("name", REP_STRING_TEMPLATE, '{name}', REP_ORDERABLE, 'align="center"');
            $rep->add_field("login", REP_STRING_TEMPLATE, '{login}', REP_ORDERABLE, 'align="center"');
            $rep->add_field("password", REP_STRING_TEMPLATE, '{password}', REP_ORDERABLE, 'align="center"');
            $rep->add_field("email", REP_STRING_TEMPLATE, '{email}', REP_ORDERABLE, 'align="center"');
            $rep->add_field("status", REP_CALLBACK, 'enabled({enabled})', REP_ORDERABLE, 'align="center"');
            $rep->add_field("", REP_STRING_TEMPLATE, href_js_action("edit", "new UserEditor({user_id})"), REP_UNORDERABLE, 'align="center"');
            $rep->add_field("", REP_STRING_TEMPLATE, href_js_action("delete", "delete_user({user_id})"), REP_UNORDERABLE, 'align="center"');

            $rep->add_filter("", REP_INVISIBLE_FILTER, "domain_id", "=", $domain_id);
            $rep->add_filter("", REP_INVISIBLE_FILTER, "webcasting_id", "=", $webcasting_id);

            $rep->html_attributes('width="650px"');

            if (count($_POST) < 3) {
                $rep->order_by("login", 1);
            }
            $rep->handle_events($_POST);

            $html = $rep->make_report();

            echo $html;
            break;
        case "load":
            $user_id = intval(@$_POST["id"]);
            $query = "SELECT login, name, email, password, enabled FROM cms_access_user WHERE user_id='$user_id' AND domain_id='$domain_id'";
            $ret = $db->getRow($query, DB_FETCHMODE_ASSOC);
            header("X-JSON:" . make_json_response($ret));
            echo "done";
            break;
        case "save":
            $user_id = intval(@$_POST["id"]);
            $webcasting_id = intval(@$_POST["webcasting_id"]);

            $p = array();
            $p["domain_id"] = $domain_id;
            $p["login"] = in(@$_POST["ulogin"]);
            $p["name"] = in(@$_POST["name"]);
            $p["email"] = in(@$_POST["email"]);
            $p["webcasting_id"] = intval($webcasting_id);
            $p["enabled"] = @$_POST["enabled"] ? 1 : 0;
            $password = in(@$_POST["password"]);
            if ($user_id) {
                $query = "UPDATE cms_access_user SET " . make_set_clause($p);
                if (strlen($password)) {
                    $query .= ", password='$password'";
                }
                $query .= " WHERE user_id='$user_id'";
                $db->query($query);
            }
            else {
                $query = "INSERT cms_access_user SET " . make_set_clause($p) . ", password='$password'";
                $db->query($query);
                //				$query = "SELECT last_insert_id()";
                //				$user_id = $db->getOne($query);
            }
            echo "done";
            break;
        case "delete":
            $user_id = intval(@$_POST["id"]);
            $query = "DELETE FROM cms_access_user WHERE user_id='$user_id' AND domain_id='$domain_id'";
            $db->query($query);
            break;
        case "load_email":
            $user_id = intval(@$_POST["id"]);
            $query = "SELECT login, password, active_from, active_to FROM cms_access_user WHERE user_id='$user_id' AND domain_id='$domain_id'";
            $ret = $db->getRow($query, DB_FETCHMODE_ASSOC);
            $text = file_get_contents("templates/helpers/webcasting_email.txt");
            $page = "webcasting";
            $data = array();
            $data["email_text"] = sprintf($text, $domain_name, $page, $ret["login"], $ret["password"], $ret["active_from"], $ret["active_to"]);
            header("X-JSON:" . make_json_response($data));
            break;
        case "send_email":
            $user_id = intval(@$_POST["id"]);
            $emails_string = $_POST["emailto"];
            $text = $_POST["email_text"];
            $emails = explode("\n", $emails_string);
            foreach ($emails as $email) {
                send_invitation(trim($email), $text);
            }
            break;
        default:
            $query = "SELECT name FROM cms_webcasting WHERE webcasting_id='$webcasting_id' AND domain_id='$domain_id'";
            $webcasting_title = $db->getOne($query);
            $smarty->assign("webcasting_title", $webcasting_title);
            $smarty->assign("webcasting_id", $webcasting_id);
            $smarty->display("webcasting_users.tpl");
    }
}
else {
    header("Location: login.php");
}

function send_invitation($emailto, $text) {
    global $domain_name;

    $emailfrom = "noreply@twintierstech.net";
    $subj = "Invitation to webcasting from $domain_name";
    mail($emailto, $subj, $text, "From: $domain_name <$emailfrom>");
}

function record($record) {
    if ($record) {
        return "enabled";
    }
    else {
        return "disabled";
    }
}

function enabled($enabled) {
    if ($enabled) {
        return "enabled";
    }
    else {
        return "disabled";
    }
}

