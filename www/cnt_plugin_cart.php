<?php
 // plugin for shopping cart display
include "cnt_plugin_main.php";

function process_content($content, $state) {
    global $smarty;

    // fetch product names and prices
    if($content['content2']) {
        $smarty->assign('custom_fields', $content['content2']);
    }
    $html = $smarty->fetch("cnt_plugin_cart.tpl");
    return $html;
}
