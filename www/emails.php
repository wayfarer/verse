<?php
 // backend email management, LDAP
include("inc/verse.inc.php"); //main header - initializes Verse environment
require_once("Net/LDAP2.php");

if ($user->have_role(ROLE_EMAIL_MANAGEMENT)) {
    $action = @$_POST["action"];

    $myCacheConfig = array(
//		        'path'    => 'c:/temp/ldap/ttt.cache',
        'path' => '/tmp/ldap.cache',
        'max_age' => 1200
    );
    $myCacheObject = new Net_LDAP2_SimpleFileSchemaCache($myCacheConfig);

    // Connect using the configuration:
    $ldap = Net_LDAP2::connect($ldap_config);
    if (PEAR::isError($ldap)) {
        die('Could not connect to LDAP-server: ' . $ldap->getMessage());
    }
    $ldap->registerSchemaCache($myCacheObject);

    //    $domain_name = "christianlearningcenter.com";
    //    $domain_name = "twintierstech.com";
    $baseDN = "domainName=$domain_name,o=domains,dc=twintierstech,dc=net";
    switch ($action) {
        case "load_email":
            $email = @$_POST["id"];
            $entry = $ldap->getEntry("mail=$email,ou=Users,$baseDN", array("mail", "cn"));
            if (PEAR::isError($entry)) {
                echo 'Error: ' . $entry->getMessage();
                exit;
            }
            $ret = $entry->getValues();
            $ret["username"] = substr($ret["mail"], 0, strpos($ret["mail"], "@"));
            unset($ret["mail"]);
            header("X-JSON:" . make_json_response($ret));
            break;
        case "load_alias":
            $email = @$_POST["id"];
            $entry = $ldap->getEntry("mail=$email,ou=Aliases,$baseDN", array("mail", "mailForwardingAddress"));
            if (PEAR::isError($entry)) {
                echo 'Error: ' . $entry->getMessage();
                exit;
            }
            $ret = $entry->getValues();
            if (is_array($ret["mailForwardingAddress"])) {
                $ret["aliases"] = implode("\n", $ret["mailForwardingAddress"]);
            }
            else {
                $ret["aliases"] = $ret["mailForwardingAddress"];
            }
            $ret["username"] = substr($ret["mail"], 0, strpos($ret["mail"], "@"));
            unset($ret["mailForwardingAddress"], $ret["mail"]);
            header("X-JSON:" . make_json_response($ret));
            break;
        case "save_email":
            $email = $_POST["id"];
            if ($email !== "0") {
                if (check_email($email)) {
                    // 1st way, directly update entry by its DN, 1 LDAP request
                    $replace = array("cn" => $_POST["cn"]);
                    $email_pass = $_POST["email_pass"];
                    if ($email_pass) {
                        $replace["userPassword"] = make_ssha_password($email_pass);
                    }
                    $ret = $ldap->modify("mail=$email,ou=Users,$baseDN", array("replace" => $replace));

                    echo "mail=$email,ou=Users,$baseDN";
                    var_dump($replace);
                    var_dump($ret);

                    // 2nd way: fetch DN for modification, 2 LDAP requests
                    /*$entry = $ldap->getEntry("mail=$email,ou=Users,$baseDN", array("cn", "userPassword"));
                     if(!PEAR::isError($entry)) {
                       // modify entry
                       $replace = array("cn"=>@$_POST["cn"]);
                       $email_pass = @$_POST["email_pass"];
                       if($email_pass) {
                         $replace["userPassword"] = make_ssha_password($email_pass);
                       }
                       $entry->replace($replace);
                             }
                             */
                }
            }
            else {
                $username = $_POST["username"];
                $email = "$username@$domain_name";
                $email_pass = $_POST["email_pass"];
                $cn = $_POST["cn"];
                if (!$cn) $cn = $username;
                if (check_email($email)) {
                    $storagebasedir = "/home/vmail/vmail01";
                    // generate mail message store
                    $mailmessagestore = sprintf("%s/%s/%s/%s/%s-%s/", $domain_name, $username[0], substr($username, 0, 2), substr($username, 0, 3), $username, date("Y.m.d.H.i.s"));

                    $homedir = $storagebasedir . "/" . $mailmessagestore;

                    // create new entry
                    $attributes = array(
                        'objectClass' => array("inetOrgPerson", "mailUser", "shadowAccount"),
                        'cn' => $cn,
                        'mail' => $email,
                        'sn' => $username,
                        'uid' => $username,
                        'accountStatus' => "active",
                        'enabledService' => array('mail', 'smtp', 'pop3', 'imap', 'deliver', 'forward',
                                                  'senderbcc', 'recipientbcc', 'managesieve',
                                                  'displayedInGlobalAddressBook'),
                        'homeDirectory' => $homedir,
                        'mailMessageStore' => $mailmessagestore,
                        'mailQuota' => 104857600,
                        'memberOfGroup' => "",
                        'mtaTransport' => "dovecot",
                        'storageBaseDirectory' => $storagebasedir,
                        'userPassword' => make_ssha_password($email_pass)
                    );
                    $entry = Net_LDAP2_Entry::createFresh("mail=$email,ou=Users,$baseDN", $attributes);

                    // Add the entry to the directory:
                    $ldap->add($entry);
                }
            }
            break;
        case "save_alias":
            $email = $_POST["id"];
            $aliases = explode("\n", trim($_POST["aliases"]));
            foreach ($aliases as $i => $alias) {
                if (!check_email($alias)) {
                    unset($aliases[$i]);
                }
            }

            if ($email !== "0") {
                if (check_email($email)) {
                    // 1st way, directly update entry by its DN, 1 LDAP request
                    $replace = array("mailForwardingAddress" => $aliases);
                    $ret = $ldap->modify("mail=$email,ou=Aliases,$baseDN", array("replace" => $replace));
                }
            }
            else {
                $username = $_POST["username"];
                $email = "$username@$domain_name";
                if (check_email($email)) {
                    // create new entry
                    $attributes = array(
                        "objectClass" => array("mailAlias", "top"),
                        "mail" => $email,
                        "mailForwardingAddress" => $aliases,
                        "accountStatus" => "active",
                        "enabledService" => array("mail", "deliver")
                    );
                    $entry = Net_LDAP2_Entry::createFresh("mail=$email,ou=Aliases,$baseDN", $attributes);

                    // Add the entry to the directory:
                    $ret = $ldap->add($entry);
                    echo $ret;
                }
            }
            break;
        case "delete_email":
            $email = $_POST["id"];
            if (check_email($email)) {
                $ldap->delete("mail=$email,ou=Users,$baseDN");
            }
            break;
        case "delete_alias":
            $email = $_POST["id"];
            if (check_email($email)) {
                $ldap->delete("mail=$email,ou=Aliases,$baseDN");
            }
            break;
        default:
            $query = "SELECT postfix FROM sms_domain WHERE domain_id='$domain_id'";
            $postfix = $db->getOne($query);

            // list action
            $result = $ldap->search("domainName=$domain_name,o=domains,dc=twintierstech,dc=net", '(&(mail=*)(|(objectClass=mailAlias)(objectClass=mailUser)))', array("attributes" => array("mail", "objectClass", "mailForwardingAddress", "mailQuota")));
            if (PEAR::isError($result)) {
                die($result->getMessage());
            }

//        var_dump($result->sorted_as_struct());

            $smarty->assign("data", $result->sorted_as_struct(array('mail')));
            $smarty->assign("entries_count", $result->count());
            $smarty->assign("domain_name", $domain_name);
            $smarty->assign("postfix", $domain_name);
            $smarty->display("emails.tpl");
    }
}
else {
    header("Location: login.php");
}

function make_ssha_password($password) {
    mt_srand((double)microtime() * 1000000);
    $salt = pack("CCCC", mt_rand(), mt_rand(), mt_rand(), mt_rand());
    $hash = "{SSHA}" . base64_encode(pack("H*", sha1($password . $salt)) . $salt);
    return $hash;
}
