<?
// obsolete
include("inc/verse.inc.php"); //main header - initializes Verse environment

if ($user->logged()) {
    // pass only global admin to domain list
    if ($user->data["domain_id"] == 0) {

        $smarty->display("email_forwarding.tpl");
    }
        // point user to his domain structure
    else {
        header("Location: structure.php");
    }
}
else {
    header("Location: login.php");
}