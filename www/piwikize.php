<?php
 // initial piwik installation for all sites
include("inc/verse.inc.php"); //main header - initializes Verse environment

$token_auth = "7494d4b88b8e5e795c72cf2679f15f38";
$piwik_url = "https://twintierstech.net/piwik/";
$piwik_get_jscode_tpl = "?module=API&method=SitesManager.getJavascriptTag&idSite=%s&format=PHP&token_auth=$token_auth";
$piwik_add_domain_with_id_tpl = "?module=API&method=VerseIntegration.addSiteWithId&idSite=%d&siteName=%s&urls=%s&format=PHP&token_auth=$token_auth";

// add piwik tag to all production sites
if ($user->is_super()) {
    // get production domains
    $query = "SELECT d.domain_id, domain_name, title, footer FROM sms_domain d INNER JOIN cms_site s USING(domain_id) WHERE mode=0 AND alias_domain_id=0 ORDER BY d.domain_id";
    $domains = $db->getAll($query, DB_FETCHMODE_ASSOC);
    foreach ($domains as $domain) {
        // check if domain has piwik stats already
        if (strpos($domain["footer"], "<!-- Piwik -->") === false) {
            // add domain to piwik
            $title = $domain["title"];
            if (!$title) $title = $domain["domain_name"];
            $piwik_add_domain_with_id = sprintf($piwik_add_domain_with_id_tpl, $domain["domain_id"], urlencode($title), "http://" . $domain["domain_name"]);
            $result = unserialize(file_get_contents($piwik_url . $piwik_add_domain_with_id));
            echo $domain["domain_name"], ": adding to piwik: ";
            var_dump($result);
            echo "<br>";
            // get JS tracking code and add to the site's footer
            $piwik_get_jscode = sprintf($piwik_get_jscode_tpl, $domain["domain_id"]);
            $piwik_js_code = unserialize(file_get_contents($piwik_url . $piwik_get_jscode));

            $footer = $domain["footer"] . "\n" . $piwik_js_code;
            $query = "UPDATE cms_site SET footer = '" . in($footer) . "' WHERE domain_id='" . $domain["domain_id"] . "'";
            $db->query($query);

        }
    }

}
