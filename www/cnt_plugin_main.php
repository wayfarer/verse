<?php
 // main plugin file

include("../symfony/lib/VerseUtil.class.php");

// shared tags
$std_handle_map = array(
    "recent_obituaries" => "recent_obituaries",
    "link" => "link_proc",
    "cart" => "cart",
    "movieclip" => "movieclip",
    "uc_menu" => "uc_menu",
    "webcasting_player" => "webcasting_player",
    "wall" => "wall",
    "yahrzeit" => "yahrzeit",
    "subscribe_for_obits" => "subscribe_for_obits",
    "footer_links" => "footer_links",
    "member" => "member",
    "recent_obituaries_slider" => "recent_obituaries_slider",
    "content_switcher" => "content_switcher",
    "search" => "search",
    "search_results" => "search_results"
);

function process_tags($content, $handle_map = array(), $user_params = NULL) {
    $handles = array_merge($GLOBALS["std_handle_map"], $handle_map);
    foreach ($handles as $tag_name => $handler) {
        // search content for tag_name
        preg_match_all("/\{$tag_name\}|\{$tag_name\s[^\}]*\}/mi", $content, $matches, PREG_SET_ORDER);
        foreach ($matches as $match) {
            if ($match) {
                $arguments = array("user_params" => $user_params);
                //					var_dump($match[0]);
                //					echo "<br>";
                // parse for params
                preg_match_all("/\S+=\"[^\"]+\"/i", $match[0], $params);
                if ($params[0]) {
                    foreach ($params[0] as $arg_str) {
                        $arg = explode("=", $arg_str);
                        // remove quotes
                        $arg[1] = substr($arg[1], 1, -1);
                        $arguments[$arg[0]] = $arg[1];
                    }
                }
                $ret = call_user_func($handler, $arguments);
                $content = str_replace($match[0], $ret, $content);
            }
        }
    }
    return $content;
}

// global tag processors

function recent_obituaries($params) {
    $days = intval(@$params["days"]);
    $count = intval(@$params["count"]);
    $obit_view_page = @$params["page"];
    $format = @$params["format"];
    $order = @$params["order"];
    $target = @$params["target"];
    if (!$count && !$days) $count = 9;
    if (!$obit_view_page) $obit_view_page = get_obituary_view_page_name();
    if (!$format) $format = "<b>%first% %last%</b>";

    $order_options = array("death_date", "service_date");
    if (!in_array($order, $order_options)) $order = $order_options[0];

    $recent = '<div class="recent_obituaries">';
    $query = "SELECT obituary_id, slug, first_name, middle_name, last_name, suffix, home_place, DATE_FORMAT(death_date, '" . DB_DATETEXT_FORMAT . "') death, FROM_UNIXTIME(service_date, '" . DB_DATETIMEAMERICAN_FORMAT . "') service, FROM_UNIXTIME(visitation_date, '" . DB_DATETIMEAMERICAN_FORMAT . "') visitation, final_disposition interment, service_date FROM plg_obituary WHERE domain_id='" . $GLOBALS["domain_id"] . "'";
    if ($days) $query .= " AND to_days(now())-to_days(death_date)<=$days";
    $query .= " ORDER BY $order DESC";
    if ($count) $query .= " LIMIT $count";
    $ret = $GLOBALS["db"]->getAll($query, DB_FETCHMODE_ASSOC);
    if (!DB::isError($ret)) {
        $vars = array("%first%", "%last%", "%middle%", "%suffix%", "%home%", "%death%", "%service%", "%visitation%", "%interment%", "%service_short%");
        $attr_target = $target ? ' target="'.$target.'"' : '';
        foreach ($ret as $row) {
            //  	    $row["service"] = $row["service_date"]." ".$row["service_place"];
            if ($row['service_date']) {
                $service_short = date("l, F j, Y", $row["service_date"]);
            }
            else {
                $row['service'] = 'no service';
                $service_short = 'no service';
            }
            $row["suffix"] = ($row["suffix"]) ? ", ".$row["suffix"] : $row["suffix"];
            $subs = array($row["first_name"], $row["last_name"], $row["middle_name"], $row["suffix"], $row["home_place"], $row["death"], $row["service"], $row["visitation"], $row["interment"], $service_short);
            $text = str_replace($vars, $subs, $format);

            //			$recent.="<div style=\"float: left; width: 165px; padding-bottom: 5px; padding-right: 5px\"><a href=\"?p=$obit_view_page&id={$row['obituary_id']}\"><b>{$row['first_name']} {$row['last_name']}</b><br>of {$row['home_place']}</a></div>";
            $recent .= "<div class=\"item\"><div class=\"inner\"><a href=\"/$obit_view_page/".VerseUtil::getObituarySlug($row)."/{$row['obituary_id']}\"$attr_target>$text</a></div></div>";
        }
    }
    $recent .= '</div>';
    return $recent;
}

function link_proc($params) {
    $url = "/" . @$params["page"];
    if (isset($params['keepparams'])) {
        $params = arg();
        array_shift($params); // remove first element = page name
        $url .= '/' . implode('/', $params);
    }
    return $url;
}

//	function cart($params) {
function cart() {
    global $domain_id, $db;
    // ecommerce
    $query = "SELECT ecommerce_enabled FROM sms_domain WHERE domain_id='$domain_id'";
    $ecommerce = $db->getOne($query);
    $GLOBALS["smarty"]->assign("ecommerce", $ecommerce);
    $html = $GLOBALS["smarty"]->fetch("cnt_plugin_cartline.tpl");
    return $html;
}

function movieclip($params) {
    global $db, $smarty;

    $movieclip_id = @intval($params["id"]);
    $query = "SELECT filename, title, protected, password, FROM_UNIXTIME(timestamp,'" . DB_DATETIME_FORMAT . "') timestamp FROM plg_movieclip WHERE movieclip_id='$movieclip_id'";
    $movieclip = $db->getRow($query, DB_FETCHMODE_ASSOC);
    if (!DB::isError($movieclip) && $movieclip) {
        $movieclip["allowed"] = 1;
        if (isset($params['scale'])) {
            $movieclip['width'] = intval(460 * $params['scale'] / 100);
            $movieclip['height'] = intval(375 * $params['scale'] / 100);
        }
        if (isset($params['nodate'])) {
            $movieclip['nodate'] = $params['nodate'];
        }

        $smarty->assign("movieclip", $movieclip);
        $html = $smarty->fetch("mcplayer.tpl");
    }
    else {
        $html = "invalid movieclip selected (id=$movieclip_id)";
    }
    return $html;
}

function uc_menu() {
    global $db, $domain_id;
    $items = array();

    $query = "SELECT name, properties FROM cms_page_uc WHERE domain_id='$domain_id' ORDER BY ord";
    $entries = $db->getAll($query, DB_FETCHMODE_ASSOC);
    foreach ($entries as $entry) {
        $props = unserialize($entry["properties"]);
        $title = $props["title"];
        if ($title) {
            $page = $entry["name"];
            $items[] = "<a href=\"/$page\">$title</a>";
        }
    }
    if (count($items) > 1) {
        return implode(" | ", $items);
    }
    else return "";
}

function webcasting_player() {
    global $domain_id;
    $html = file_get_contents("templates/helpers/webcasting.html");
    return $html;
}

function wall($params) {
    global $db, $domain_id, $smarty;
    $wall_handle = @$params["name"];
    if (!$wall_handle) {
        $wall_handle = "default";
    }
    $query = "SELECT wall_id FROM cms_wall WHERE handle='" . in($wall_handle) . "' AND domain_id='$domain_id'";
    $wall_id = $db->getOne($query);
    if (!DB::isError($wall_id) && !$wall_id) {
        // create the wall
        $query = "INSERT cms_wall SET domain_id='$domain_id', handle='$wall_handle', created_at=now()";
        $db->query($query);
        $wall_id = $db->getOne("SELECT last_insert_id()");
    }
    $smarty->assign("wall_id", $wall_id);

    $theuser = createobject("user", array($db, "cms_access_user"));
    if ($theuser->logged()) {
        $smarty->assign("user", $theuser->data);
    }

    return $smarty->fetch("wall.tpl");
}

function yahrzeit($params) {
    global $smarty;

    if(!empty($params['mode']) && $params['mode'] == 'full') {
        $smarty->assign("to_year", date("Y", strtotime('+10 years')));
    }

    return $smarty->fetch("helpers/yahrzeit.tpl");
}

function subscribe_for_obits($params) {
    $html = file_get_contents("templates/helpers/subscribe_for_obits.html");
    return $html;
}

function footer_links($params) {
    global $db, $domain_id;

    $separator = isset($params["separator"]) ? $params["separator"] : " ";
    // fetch structure
    $query = "SELECT node_id, p.internal_name internal_name, depth, menu_type, display_name, params FROM cms_node n LEFT JOIN cms_page p USING(page_id) WHERE n.domain_id='$domain_id' AND depth=0 ORDER BY n.ord";
    $structure = $db->getAll($query, DB_FETCHMODE_ASSOC);

    $nodes = array();
    foreach ($structure as $i => $elem) {
        // stuff to show subtree for empty node
        // processing is on display stage
        if (!$structure[$i]["internal_name"]) {
            $structure[$i]["internal_name"] = slugify($structure[$i]["display_name"]) . "-" . $structure[$i]["node_id"];
        }
        $nodes[] = l($elem["display_name"], $structure[$i]["internal_name"], array("attributes" => array("class" => "footer-link")));
    }
    return '<div class="footer-links">' . implode($nodes, $separator) . '</div>';
}

function member($params) {
    global $theuser;

    if ($theuser->data['privileges'] == 2) {
        return $params['prefix'] . $theuser->data['name'] . $params['suffix'];
    }
    else {
        return "";
    }
}

function recent_obituaries_slider($params) {
    include('inc/imageutil.inc.php');
    global $db, $domain_id, $domain_name, $smarty;

    $obituaries = $db->getAll("SELECT obituary_id, slug, image, first_name, last_name, death_date FROM plg_obituary WHERE domain_id = $domain_id ORDER BY obituary_id DESC LIMIT 5", DB_FETCHMODE_ASSOC);
    $obituaries_to_tpl = array();
    foreach ($obituaries as $obituary) {
        if ($obituary['image'] && substr($obituary['image'], 0, 7) == '/image/') {
            $obituary['image'] = substr($obituary['image'], 7);
        }
        if ($obituary['image'] && (file_exists("files/$domain_name/image/" . $obituary['image']))) {
            $image = 'image/' . $obituary['image'];
            $thumbnail = thumbnail('image/' . $obituary['image'], 50);
        } else {
            $image = 'img/noimage-tn.jpg';
            $thumbnail = 'img/noimage-tn-thumbnail.jpg';
        }
        $obituaries_to_tpl[] = array(
            'obituary_id' => $obituary['obituary_id'],
            'obituary_slug' => VerseUtil::getObituarySlug($obituary),
            'first_name' => $obituary['first_name'],
            'last_name' => $obituary['last_name'],
            'death_date' => $obituary['death_date'],
            'image' => $image,
            'thumbnail' => $thumbnail
        );
    }
    $smarty->assign('obituaries', $obituaries_to_tpl);
    return $smarty->fetch("cnt_plugin_obit_last.tpl");
}

function content_switcher($params) {
    global $smarty;

    $params = array(
        "content1" => @$params["content1"],
        "content2" => @$params["content2"],
    );

    $smarty->assign('params', $params);
    return $smarty->fetch("cnt_plugin_content_switcher.tpl");
}

function search($params) {
    global $smarty;

    $params = array(
        "key" => @$params["key"],
        "url" => (isset($params["url"])) ? $params["url"] : "search-results",
        "button_text" => (isset($params["button_text"])) ? $params["button_text"] : "Search",
        "size" => (isset($params["size"])) ? $params["size"] : "30"
    );

    $smarty->assign('params', $params);
    return $smarty->fetch("cnt_plugin_search.tpl");
}

function search_results($params) {
    global $smarty;
    
    return $smarty->fetch("cnt_plugin_search_results.tpl");
}