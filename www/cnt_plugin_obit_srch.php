<?php
 // obituary search page plugin
include "cnt_plugin_main.php";

function process_content($content, $state) {
    global $db, $domain_id, $smarty;

    if (empty($state)) { // show search form
        // first step processing (external obituary templates)
        $cnt = process_tags($content["content1"], array("obituary_search" => "obituary_search"));
        if (!$cnt) $cnt = $content["content1"];

        $cnt = process_tags($cnt, array("last_obituaries" => "last_obituaries", "subscribe" => "subscribe_for_obits"));
        // add form tags
        $form_open = stripos($cnt, '<form');
        $form_close = stripos($cnt, '</form>');
        if ($form_open !== false) {
            if ($form_open > strlen($cnt) - $form_close) {
                // take part before existing form in <form> tags, because it bigger
                $cnt = '<form method="post">' . str_ireplace('<form', '</form><form', $cnt);
            }
            else {
                // take part after existing form in <form> tags
                $cnt = str_ireplace('</form>', '</form><form method="post">', $cnt) . '</form>';
            }
        }
        else {
            $cnt = '<form method="post">' . $cnt . '</form>';
        }
        $smarty->assign("cnt", $cnt);
        $html = $GLOBALS["smarty"]->fetch("cnt_plugin_obit_srch.tpl");
        return $html;
    }
    else if (strlen($state["email"])) { // subscribe process
        // check if subscriber exists
        $query = "SELECT email FROM plg_subscribe WHERE email='" . in($state["email"]) . "' AND domain_id='$domain_id'";
        $ret = $db->getOne($query);
        if ($ret) {
            $html = "Address $ret is already subscribed for new obituaries notification";
        }
        else {
            $hash = substr(md5(uniqid("")), 0, 16);
            $query = "INSERT plg_subscribe SET email='" . in($state["email"]) . "', name='" . in($state["name"]) . "', domain_id='$domain_id', hash='$hash', enabled=0, created=now()";
            $db->query($query);

            subscribe_send_confirmation($state["name"], $state["email"], $hash);
            $html = "Confirmation e-mail has been sent to address {$state["email"]}. To complete subscription please follow instructions in this e-mail";
        }
        return $html;
    }
    else { // do search and show results
        $domain_id = $GLOBALS["domain_id"];
        $first_name = in(@$state["first_name"]);
        $last_name = in(@$state["last_name"]);
        $home_place = in(@$state["city"]);
        $from_date = in(@$state["from_date"]);
        $to_date = in(@$state["to_date"]);
        $phrase = in(@$state["phrase"]);

        // this is for vaughncgreene.r.twintierstech.net
        if($domain_id == 739) {
            if(!$first_name && !$last_name && !$home_place && !$from_date && !$to_date && !$phrase) {
                return 'Please enter a search term in at least one box to get search results.';
            }
            if(!$first_name && !$last_name) {
                return 'Please enter first and last name to get search results.';
            }
            if(!$first_name) {
                return 'Please enter first name to get search results.';
            }
            if(!$last_name) {
                return 'Please enter last name to get search results.';
            }

            $and_where = "first_name = '$first_name' AND last_name = '$last_name'";
        } else {
            $and_where = "first_name LIKE '%$first_name%' AND last_name LIKE '%$last_name%'";
        }

        $query = "SELECT obituary_id, slug, first_name, middle_name, last_name, home_place, death_date FROM plg_obituary WHERE domain_id='$domain_id' AND ".$and_where." AND home_place LIKE '%$home_place%'";
        if ($from_date) {
            $query .= " AND death_date>='$from_date'";
        }
        if ($to_date) {
            $query .= " AND death_date<='$to_date'";
        }
        if ($phrase) {
            $query .= " AND obit_text LIKE '%$phrase%'";
        }
        //			$query.=" ORDER BY death_date DESC";
        $query .= " ORDER BY last_name";
        $ret = $GLOBALS["db"]->getAll($query, DB_FETCHMODE_ASSOC);

        $cnt = $content["content2"];
        if (!$cnt) {
            $cnt = "{obituaries_search_result}";
        }
        $cnt = process_tags($cnt, array("obituaries_search_result" => "obituaries_search_result"), $ret);
        return $cnt;
    }
}

function obituaries_search_result($params) {
    $obit_view_page = @$params["page"];
    if (!$obit_view_page) {
        $obit_view_page = get_obituary_view_page_name();
    }
    $result = "";
    foreach ($params["user_params"] as $row) {
        $result .= l('<strong>' . $row["first_name"] . ' ' . $row["last_name"] . '</strong> from ' . $row["home_place"], $obit_view_page . "/" . VerseUtil::getObituarySlug($row) . "/" . $row["obituary_id"], array("html" => true)) . "<br>";
        //			$result.='<a href="?p='.$obit_view_page.'&id='.$row["obituary_id"].'"><b>'.$row["first_name"].' '.$row["last_name"].'</b> from '.$row["home_place"].'</a><br>';
    }
    //		$result.="<br><b>".count($params["user_params"])."</b> obituar(y,ies) found";
    if (!count($params["user_params"])) {
        $result .= "No obituaries found";
    }
    return $result;
}

function last_obituaries($params) {
    $days = intval(@$params["days"]);
    $count = intval(@$params["count"]);
    $obit_view_page = @$params["page"];
    if (!$count) $count = 9;
    if (!$obit_view_page) $obit_view_page = get_obituary_view_page_name();

    $recent = '<div id="lastobits">';
    $query = "SELECT obituary_id, slug, first_name, last_name, death_date FROM plg_obituary WHERE domain_id='" . $GLOBALS["domain_id"] . "' ORDER BY death_date DESC LIMIT $count";
    $ret = $GLOBALS["db"]->getAll($query, DB_FETCHMODE_ASSOC);
    foreach ($ret as $row) {
        $recent .= "<div><div class=\"name\">{$row['first_name']} {$row['last_name']}</div><div class=\"deathdate\">{$row['death_date']}</div>" . l("View Service Information", $obit_view_page . "/" . VerseUtil::getObituarySlug($row) . "/" . $row['obituary_id']) . "</div>";
    }
    $recent .= "</div>";
    return $recent;
}

function obituary_search($params) {
    global $smarty;
    $style = "obituaries/" . $params["style"] . "_search.tpl";
    $search_tpl = "";
    if ($smarty->template_exists($style)) {
        $search_tpl = $smarty->fetch($style);
        if (strpos($search_tpl, '<form') === false) {
            //      	$search_tpl = '<form method="post">'.$search_tpl.'</form>';
        }
    }
    return $search_tpl;
}

function subscribe($params) {
    $html = file_get_contents("templates/helpers/subscribe.html");
    return $html;
}

function subscribe_send_confirmation($name, $email, $hash) {
    global $domain_name;

    $emailfrom = "noreply@twintierstech.net";
    $emailto = "$name <$email>";
    $subj = "Subscription for new obituaries confirmation";
    $text = "This email address has requested to receive new obituaries for site $domain_name. To complete the subscription and receive them, please click the following link: http://$domain_name/subscribe.php?action=obits&id=$hash\n\nIf you did not initiate this request, please ignore this email and you will not be subscribed.";
    mail($emailto, $subj, $text, "From: $domain_name subscription service <$emailfrom>");
}
