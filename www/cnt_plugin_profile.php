<?php
 // members, profile page plugin
include "cnt_plugin_main.php";

function process_content($content, $state) {
    global $domain_id, $domain_name, $db, $smarty, $theuser;
    $ret = "";

    switch ($state["action"]) {
        default:
            if (parse_url($_SERVER["HTTP_REFERER"], PHP_URL_PATH) != "/member_profile") {
                $_SESSION["profile_ref"] = $_SERVER["HTTP_REFERER"];
            }
            $smarty->assign("profile_ref", $_SESSION["profile_ref"]);
            $smarty->assign("user", $theuser->data);
            $ret = $smarty->fetch("profile_page.tpl");
            break;
        case "save":
            // validation
            $error_count = 0;
            if (strlen($state["name"]) < 2) {
                $error_count++;
                verse_set_message("Please, enter your name", "error");
            }
            if (strlen($state["login"]) < 2) {
                $error_count++;
                verse_set_message("Please, enter your username", "error");
            }
            if (strlen($state["email"]) && !check_email($state["email"])) {
                $error_count++;
                verse_set_message("Please, enter valid email address or leave it empty", "error");
            }
            if ($state["password"] != $state["password2"]) {
                $error_count++;
                verse_set_message("Passwords must be the same", "error");
            }
            if (!$error_count) {
                // success
                // update profile
                $p["name"] = in($state["name"]);
                $p["login"] = in($state["login"]);
                $p["email"] = in($state["email"]);
                if (strlen($state["password"])) {
                    $p["password"] = $state["password"];
                    $p["force_password_change"] = 0;
                }
                $user_id = $theuser->data["user_id"];
                if($theuser->is_shared()) {
                    $query = "UPDATE cms_access_user SET " . make_set_clause($p) . " WHERE user_id='$user_id' AND domain_id IN (".implode(', ', $theuser->get_domain_ids()).")";
                } else {
                    $query = "UPDATE cms_access_user SET " . make_set_clause($p) . " WHERE user_id='$user_id' AND domain_id='$domain_id'";
                }
                $db->query($query);
                // reload user object
                $theuser->update_login($p["login"]);
                $theuser->get_data(true);
                verse_set_message("Your profile has been updated");
                header("Location: /?p=" . $_GET["p"]);
                exit;
            }
            else {
                header("Location: /?p=" . $_GET["p"] . "&action=edit");
                exit;
            }
            break;
        case "edit":
            $smarty->assign("user", $theuser->data);
            $ret = $smarty->fetch("profile_form.tpl");
            break;
    }
    return $ret;
}
