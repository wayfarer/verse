<?php
 // backend candle reports
include("inc/verse.inc.php"); //main header - initializes Verse environment

if ($user->is_super()) {
    include_once('inc/open_flash_chart_object.php');
    include_once('inc/open-flash-chart.php');
    //  	$_POST = utf8_to_latin($_POST);

    $valid_periods = array("day", "week", "month", "year");

    $action = @$_GET["action"];
    $period = @$_GET["period"];
    $date = @$_GET["date"];
    $domain = @$_GET["domain"];
    $rdomain = @$_GET["rdomain"];

    // period processing
    if ($period && in_array($period, $valid_periods)) {
        $_SESSION["cr_period"] = $period;
        $_SESSION["cr_date_from"] = NULL; // to recalculate date range :)
    }
    if (!isset($_SESSION["cr_period"])) $_SESSION["cr_period"] = "month"; // default period

    // date range processing
    if ($date || !isset($_SESSION["cr_date_from"])) {
        // calculate new daterange
        if (!$date) {
            //        $date = $_SESSION["cr_date"]?$_SESSION["cr_date"]:date("Y-m-d");
            $date = date("Y-m-d");
        }
        else {
            if ($date == "prev") {
                if ($period != "year") {
                    $date = date("Y-m-d", strtotime("-10 " . $_SESSION["cr_period"], strtotime($_SESSION["cr_date"])));
                }
            }
            if ($date == "next") {
                if ($period != "year") {
                    $date = date("Y-m-d", strtotime("+10 " . $_SESSION["cr_period"], strtotime($_SESSION["cr_date"])));
                }
            }
            if ($date == "now") $date = date("Y-m-d");
        }
        $ts_to = strtotime($date);
        switch ($_SESSION["cr_period"]) {
            case "day":
                $ts_from = strtotime("-13 day", strtotime($date));
                break;
            case "week":
                $ts_from = strtotime("-11 week", $ts_to);
                // move to week begin and end
                $ts_from = strtotime("last Monday", $ts_from);
                $ts_to = strtotime("next Sunday", $ts_to);
                break;
            case "month":
                $ts_from = strtotime("-11 month", $ts_to);
                // move to month begin and end
                $ts_from = mktime(0, 0, 0, date("m", $ts_from), 1, date("Y", $ts_from));
                $ts_to = mktime(0, 0, 0, date("m", $ts_to) + 1, 0, date("Y", $ts_to));
                break;
            case "year":
                $ts_from = strtotime("-9 year", $ts_to);
                $ts_to = mktime(0, 0, 0, 12, 31, date("Y", $ts_to));
                break;
        }
        $_SESSION["cr_date_from"] = date("Y-m-d", $ts_from);
        $_SESSION["cr_date"] = date("Y-m-d", $ts_to);
    }

    // domains processing
    if (!is_null($domain)) {
        if ($domain == 0) {
            $_SESSION["cr_domains"] = array();
        }
        else {
            if (isset($_SESSION["cr_domains"])) {
                $_SESSION["cr_domains"][] = $domain;
            }
            else {
                $_SESSION["cr_domains"] = array($domain);
            }
        }
    }
    if ($rdomain) {
        foreach ($_SESSION["cr_domains"] as $key => $value) {
            if ($value == $rdomain) {
                unset($_SESSION["cr_domains"][$key]);
                break;
            }
        }
    }

    switch ($action) { // second pass
        case "draw":
            // domains
            if (count($_SESSION["cr_domains"]) == 0) {
                // fetch top 5 candle domains for examining period
                $query = "SELECT domain_id, count(*) cnt FROM plg_obituary_candle WHERE timestamp>'" . $_SESSION["cr_date_from"] . "' AND timestamp<='" . $_SESSION["cr_date"] . " 23:59:59' GROUP BY domain_id ORDER BY cnt DESC LIMIT 5";
                $ret = $db->getAll($query, DB_FETCHMODE_ASSOC);
                $domains = array();
                foreach ($ret as $row) {
                    $domains[] = $row["domain_id"];
                }
                $_SESSION["cr_domains"] = $domains;
            }
            else {
                $domains = $_SESSION["cr_domains"];
            }
//        var_dump(count($_SESSION["cr_domains"]));
//        exit;
//        $domains = array(4, 65, 69, 34, 131, 39, 8);
            // get date for examined period
            $period_operator = $_SESSION["cr_period"];
            if ($period_operator == "week") {
                $period_extra = ",3"; // use 3rd mode for week (1-53 week, with more than 3 days this year, or: The week number can be described by counting the Thursdays: week 12 contains the 12th Thursday of the year.)
            }
            else {
                $period_extra = "";
            }

            $candle_data = array();

            // check if special "all domains" present and fetch data for it
            if (in_array(-1, $domains)) {
                // fetch data for "all domains"
                $query = "SELECT $period_operator(timestamp$period_extra) period_value, count(*) cnt FROM plg_obituary_candle WHERE timestamp>'" . $_SESSION["cr_date_from"] . "' AND timestamp<='" . $_SESSION["cr_date"] . " 23:59:59' GROUP BY $period_operator(timestamp$period_extra) ORDER BY timestamp";
                $ret = $db->getAll($query, DB_FETCHMODE_ASSOC);
                // convert to assoc
                foreach ($ret as $row) {
                    $candle_data["-1"][$row["period_value"]] = $row["cnt"];
                }
                // remove -1 value
                foreach ($domains as $key => $value) {
                    if ($value == "-1") {
                        unset($domains[$key]);
                        break;
                    }
                }
            }
            // fetch data per domain
            $query = "SELECT domain_id, $period_operator(timestamp$period_extra) period_value, count(*) cnt FROM plg_obituary_candle WHERE timestamp>'" . $_SESSION["cr_date_from"] . "' AND timestamp<='" . $_SESSION["cr_date"] . " 23:59:59' AND domain_id IN (" . implode(",", $domains) . ") GROUP BY domain_id, $period_operator(timestamp$period_extra) ORDER BY timestamp";
            $ret = $db->getAll($query, DB_FETCHMODE_ASSOC);
            // convert to assoc
            foreach ($ret as $row) {
                $candle_data[$row["domain_id"]][$row["period_value"]] = $row["cnt"];
            }
//        var_dump($candle_data);
//        exit;
            // fetch domain names
            $query = "SELECT domain_id, domain_name FROM sms_domain WHERE domain_id IN (" . implode(",", $domains) . ")";
            $domain_names = $db->getAll($query, DB_FETCHMODE_ASSOC);
            $domain_names[] = array("domain_id" => "-1", "domain_name" => "All domains");

            // create aspect for selected period (i.e. create all period values and determine period_value <-> period_name assoc)
            $aspect = array();
            $ts_from = strtotime($_SESSION["cr_date_from"]);
            $ts_to = strtotime($_SESSION["cr_date"]);
            switch ($_SESSION["cr_period"]) {
                case "day":
                    do {
                        $period_value = date("j", $ts_from);
                        $period_name = date("Y-m-d", $ts_from);
                        $aspect[$period_value] = $period_name;
                        $ts_from = strtotime("+1 day", $ts_from);
                    }
                    while ($ts_from <= $ts_to);
                    break;
                case "week":
                    do {
                        $period_value = intval(date("W", $ts_from));
                        $period_name = date("F d", $ts_from) . " week";
                        $aspect[$period_value] = $period_name;
                        $ts_from = strtotime("+1 week", $ts_from);
                    }
                    while ($ts_from <= $ts_to);
                    break;
                case "month":
                    do {
                        $period_value = date("n", $ts_from);
                        $period_name = date("F", $ts_from);
                        $aspect[$period_value] = $period_name;
                        $ts_from = strtotime("+1 month", $ts_from);
                    }
                    while ($ts_from <= $ts_to);
                    break;
                case "year":
                    do {
                        $period_value = date("Y", $ts_from);
                        $period_name = date("Y", $ts_from);
                        $aspect[$period_value] = $period_name;
                        $ts_from = strtotime("+1 year", $ts_from);
                    }
                    while ($ts_from <= $ts_to);
                    break;
            }

            // prepare data sets
            $data = array();
            foreach ($domain_names as $row) {
                $domain_data = array();
                foreach ($aspect as $key => $val) {
                    $domain_data[] = $candle_data[$row["domain_id"]][$key] ? $candle_data[$row["domain_id"]][$key] : 0;
                }
                $data[$row["domain_id"]] = array("hint" => $row["domain_name"], "data" => $domain_data);
            }
            // x_labels orientation
            $orient = 0; // horiz
            switch ($_SESSION["cr_period"]) {
                case "day":
                case "week":
                    $orient = 1; // vert
                    break;
            }
            echo draw_cmp_report("Candles counts per domain by " . $_SESSION["cr_period"] . " for " . format_period($_SESSION["cr_date_from"], $_SESSION["cr_date"], $_SESSION["cr_period"]), "candle counts", $aspect, $data, $orient);
            break;
        default:
            // fetch domains
            $query = "SELECT d.domain_id, domain_name, count(*) cnt FROM sms_domain d INNER JOIN plg_obituary_candle USING(domain_id) WHERE mode<2 AND alias_domain_id=0 GROUP BY domain_id ORDER BY domain_name";
            $ret = $db->getAll($query, DB_FETCHMODE_ASSOC);
            $domains = array(0 => "Top 5 domains for period (reset)",
                             -1 => "All domains");
            foreach ($ret as $row) {
                $domains[$row["domain_id"]] = $row["domain_name"] . " (" . $row["cnt"] . ")";
            }
            $ofc_object = open_flash_chart_object_str(900, 350, 'candles_report_sites.php?action=draw');

            $smarty->assign("domains", $domains);
            $smarty->assign("ofc_object", $ofc_object);
            $smarty->assign("date", $_SESSION["cr_date"]);
            $smarty->display("candles_report_sites.tpl");
            break;
    }
}
else {
    header("Location: login.php");
}

// $data - array of data arrays(hint=>hint, data=>array(data));
function draw_cmp_report($title, $y_legend, $x_labels, $data, $x_orient = 0) {
    $colors = array('#0066CC', '#9933CC', '#D83A45', '#639F45', '#C85817', '#243B3B', '#A9A83C');
    // create graph
    $g = new graph();
    $g->bg_colour = '#EDFAFF';
    $g->x_axis_colour('#808080', '#808080');
    $g->y_axis_colour('#808080', '#808080');
    $g->title($title, '{font-size: 18px;}');
    $g->set_x_labels($x_labels);
    $g->set_x_label_style(10, '#2378AF', $x_orient); // size, color, orient
    $g->set_y_legend($y_legend, 12, '#2378AF');
    $g->set_tool_tip("#key#<br>#x_label# - #val# candle(s)<br><br>Click to remove domain from graph");
    // create and insert data
    $max = 0;
    $ci = 0;
    foreach ($data as $key => $values) {
        $hint = $values["hint"];
        $local_max = max($values["data"]);
        if ($local_max > $max) $max = $local_max;

        $bar = new bar(75, $colors[$ci++ % count($colors)]);
        $bar->key($hint, 10);
        //      $bar->data = $values["data"];
        foreach ($values["data"] as $value) {
            $bar->add_link($value, "javascript:remove_domain($key)");
        }
        $g->data_sets[] = $bar;
    }

    $g->set_y_max($max);
    return $g->render();
}

function format_period($date_from, $date_to, $period) {
    $ret = "";
    switch ($period) {
        case "day":
            $ret = $date_from . " - " . $date_to;
            break;
        case "week":
            $ts_from = strtotime($date_from);
            $ts_to = strtotime($date_to);
            $ret = "Week #" . date("W Y", $ts_from) . " - " . "Week #" . date("W Y", $ts_to);
            break;
        case "month":
            $ts_from = strtotime($date_from);
            $ts_to = strtotime($date_to);
            $ret = date("F Y", $ts_from) . " - " . date("F Y", $ts_to);
            break;
        case "year":
            $ts_from = strtotime($date_from);
            $ts_to = strtotime($date_to);
            $ret = date("Y", $ts_from) . " - " . date("Y", $ts_to);
            break;
    }
    return $ret;
}