<?php
 // plugin for secure emailing forms
include "cnt_plugin_main.php";

function process_content($content, $state) {
    global $domain_name, $domain_id, $db;

    if ($_SERVER["REQUEST_METHOD"] == "GET") { // show mailing form by default
        if (!$_GET["done"]) {
            $ret = process_tags($content["content1"], array("captcha" => "captcha"), $state);
            if (isset($_SESSION["form_state"])) {
                // prefill form with user input values
                $patterns = array();
                $replacements = array();
                foreach ($_SESSION["form_state"] as $name => $value) {
                    // prefill inputs
                    $patterns[] = '/(\<input.*?name="' . $name . '".*?\>)/e';
                    $replacements[] = "prefill_input('\\1', '$value')";
                    // prefill textareas
                    $patterns[] = '/(\<textarea.*?name="' . $name . '".*?\>.*?\<\/textarea\>)/e';
                    $replacements[] = "prefill_textarea('\\1', '$value')";
                }

                $ret = preg_replace($patterns, $replacements, $ret);
                // clear form state
                unset($_SESSION["form_state"]);
            }

            $ret = '<input type="hidden" name="z" value="' . session_id() . '">' . $ret;
            $ret = '<form method="post" action="' . VERSE_SERVER_SECURE_URL . '/' . arg(0) . '?from=' . $domain_name . '">' . $ret . '</form>';
            return $ret;
        }
        else {
            if ($content["content2"]) return process_tags($content["content2"]);
            else {
                if (!isset($thanks) && isset($_GET["thanks"])) {
                    $thanks = $_GET["thanks"];
                }
                if (!$thanks) $thanks = "Thanks for contacting us";
                return "<div>$thanks</div>";
            }
        }
    }
    else { // mail posted fields
        if (!$_GET["done"]) {
            if ($_SERVER["REQUEST_METHOD"] == "POST") {
                unset($state['z']);
                unset($state['from']);
                // init the same session as at originating domain (for captcha to work)
                // validation
                $error_count = 0;
                $captcha_present = (strpos($content["content1"], "{captcha}") !== false);
                // check if captcha present on the form
                if ($captcha_present) {
                    if ($state["captcha"] != $_SESSION["captcha"] || !$_SESSION["captcha"]) {
                        $error_count++;
                        verse_set_message("Letters do not match, please try again", "error");
                    }
                    unset($_SESSION["captcha"]); // erase captcha to protect from subsequent posts with the same value
                }
                if (!$error_count) {
                    $ip = $_SERVER['REMOTE_ADDR'];

                    if (!is_block_ip($ip)) {
                        // it is unnecessary sending captcha in email
                        unset($state["captcha"]);

                        // fetch default email for the domain
                        $query = "SELECT email FROM sms_domain WHERE domain_id='" . $domain_id . "'";
                        $email = $db->getOne($query);
                        $result = "IP address: ".$ip."\n";
                        $replyto = "";
                        $emailfromname = "From website";
                        foreach ($state as $key => $value) {
                            $result .= "$key: $value\n";
                            if ($key == "email" && check_email($value)) {
                                $replyto = $value;
                            }
                        }
                        // determine subject
                        switch ($_GET["p"]) {
                            case "contact":
                            case "contactus":
                            case "contact_us":
                            case "onlinecontact":
                            case "contactform":
                            case "contactinfo":
                            case "contact_info":
                                $subject = "$domain_name: contact form message";
                                $thanks = "Thanks for contacting us";
                                $hello = "Hello, this is the message from your website $domain_name.\nSomeone has filled contact form with next values:";
                                break;
                            case "preneed":
                            case "preplan":
                            case "planning_form":
                            case "planningform":
                            case "preplan_form":
                                $subject = "$domain_name: preened planning form";
                                $thanks = "Thank you for submitting our preneed planning form. If we have not contacted you in the next few business days please give us a call to make sure we have received your form.";
                                $hello = "Hello, this is the message from your website $domain_name.\nSomeone has filled preneed form with next values:";
                                break;
                            case "request":
                                $subject = "$domain_name: request form message";
                                $thanks = "Thanks for contacting us. We will get in touch with you soon.";
                                $hello = "Hello, this is the message from your website $domain_name.\nSomeone has filled request form with next values:";
                                break;
                            default:
                                $subject = "$domain_name: {$_GET["p"]} form message";
                                $thanks = "Thanks for contacting us";
                                $hello = "Hello, this is the message from your website $domain_name.\nSomeone has filled the {$_GET["p"]} form with next values:";
                        }
                        $signature = "Sincerely yours, webform robot.";

                        $result = $hello . "\n\n" . $result . "\n\n" . $signature;

                        send_email($email, $subject, $result, $replyto);
                    } else {
                        update_block_ip_count($ip);
                    }
                }
                else {
                    // there was validation errors - redisplay the form with error messages
                    $_SESSION["form_state"] = $state;
                    //          header("Location: /?p=".$_GET["p"]);
                    // redirect back to the site to display form with errors
                    header("Location: http://" . $_GET["from"] . "/" . arg(0));
                    exit;
                }
            }
        }

        if ($_SERVER["HTTPS"] && isset($_GET["from"])) {
            // redirect back to the site
            header("Location: http://" . $_GET["from"] . "/" . arg(0) . "?done=1&thanks=" . urlencode($thanks));
            exit;
        }
    }
}

/*  function send_email($emailto, $subject, $emailfrom, $emailfromname, $text) {
    global $domain_name;
    
    return mail($emailto, $subject, $text, "From: $emailfromname <$emailfrom>", "-f$emailfrom $emailto");
  }
*/

function send_email($emailto, $subject, $text, $replyto = null) {
    $emailfrom = "noreply@twintierstech.net";
    if ($replyto) $replyto = "\r\nReply-To: $replyto";

    return mail($emailto, $subject, $text, "From: $emailfrom".$replyto);
}

function prefill_input($tag, $value) {
    //    echo "<!--$tag = $value -->\n";
    // handle XHTML closing tags
    $tag = stripslashes(str_replace("/>", ">", $tag));
    $tag = str_replace(">", " value=\"$value\">", $tag);
    //    echo "<!-- $tag -->\n";
    return $tag;
}

function prefill_textarea($tag, $value) {
    //    echo "<!--$tag = $value -->\n";
    $tag = stripslashes($tag);
    $tag = str_replace("</textarea>", "$value</textarea>", $tag);

    return $tag;
}

function captcha($params) {
    $captcha_code = file_get_contents("templates/helpers/captcha_code.html");
    return $captcha_code;
}
  