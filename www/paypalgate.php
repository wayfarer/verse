<?php 
include("inc/verse.inc.php"); //main header - initializes Verse environment
// save order
if (!$_SESSION["order_id"]) {
    $query = "SELECT product_id, name, price FROM plg_product WHERE product_id IN(" . implode(",", array_keys($_SESSION["cart"])) . ")";
    $products = $GLOBALS["db"]->getAssoc($query, false, null, DB_FETCHMODE_ASSOC);
    // count order total
    $total = 0;
    foreach ($_SESSION["cart"] as $product_id => $count) {
        $total += $products[$product_id]["price"] * $count;
    }
    // get config
    $query = "SELECT param, value FROM plg_product_config WHERE domain_id='$domain_id'";
    $config = $db->getAssoc($query);
    // for checking after returning from PayPal gateway
    $_SESSION["total"] = $total;
    $_SESSION["business"] = $config["paypal_business"];

    $_SESSION["return_page"] = $config["return_page"];

    $p["domain_id"] = $domain_id;
    $p["total"] = $total;
    $p["status"] = "New";
    $query = "INSERT plg_product_order SET " . make_set_clause($p) . ", timestamp=now()";
    $db->query($query);
    $order_id = $db->getOne("SELECT last_insert_id()");
    // save items
    foreach ($_SESSION["cart"] as $product_id => $count) {
        $query = "INSERT plg_product_order_product SET order_id='$order_id', product_id='$product_id', count='$count'";
        $db->query($query);
    }
    $_SESSION["order_id"] = $order_id;
}
?>
Please be patient, your payment is being transferred to PayPal gateway...
<form action="https://www.paypal.com/us/cgi-bin/webscr" method="post">
    <input type="hidden" name="business" value="<?php echo $_SESSION["business"];?>">
    <input type="hidden" name="cmd" value="_cart">
    <input type="hidden" name="upload" value="1">
    <input type="hidden" name="currency_code" value="USD">
    <input type="hidden" name="return" value="https://twintierstech.net/paypal.php">
    <input type="hidden" name="rm" value="2">
    <?php foreach ($_POST as $key => $value): ?>
    <input type="hidden" name="<?php echo $key;?>" value="<?php echo $value;?>">
    <?php endforeach; ?>
</form>
<script>
    document.forms[0].submit();
</script>