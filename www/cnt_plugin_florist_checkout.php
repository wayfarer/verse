<?php
// plugin for checkout page
include "cnt_plugin_main.php";

function process_content($content, $state) {
    global $errors, $smarty;

    if(@$_SESSION["florist_cart"]) {
        if (count($_POST)) {
            require_once($_SERVER['DOCUMENT_ROOT'] . '/../util/soap/nusoap.php');
            include_once($_SERVER['DOCUMENT_ROOT'] . '/../symfony/config/ProjectConfiguration.class.php');
            $configuration = ProjectConfiguration::getApplicationConfiguration('frontend', 'prod', false);

            $server_url = sfConfig::get('app_florist_one_soap');
            $key = sfConfig::get('app_florist_one_key');
            $pass = sfConfig::get('app_florist_one_pass');

            $client = new nusoap_client($server_url, true);

            $card_types = array(
                'AX' => 'American Express',
                'VI' => 'Visa',
                'MC' => 'MasterCard',
                'DI' => 'Discover'
            );

            $form = $_POST;

            if(!empty($form['checkout_form'])) {
                $errors = validate_form($form);

                if(!isset($errors['recipient_zip']) && !isset($errors['delivery_date'])) {
                    $result = $client->call('checkDeliveryDate', array("APIKey" => $key, "APIPassword" => $pass, "zipcode" => $_POST['recipient_zip'], "date" => $_POST['delivery_date']));
                    if(!$result['errors'] && !$result['valid']) {
                        $errors['delivery_date'] = 'Invalid Delivery Date';
                    }
                }
            }

            if (empty($errors)) {
                if(!empty($form['checkout_form'])) {
                    unset($form['checkout_form']);
                    $form['card_type'] = $card_types[$form['card_type']];

                    $smarty->assign("form", $form);
                    $smarty->assign("checkout_info", true);
                } elseif(!empty($form['checkout_info'])) {
                    $products = array();
                    $products_to_display = array();

                    foreach($_SESSION["florist_cart"] as $code=>$count) {
                        $result = $client->call('getProduct2013', array("APIKey" => $key, "APIPassword" => $pass, "itemCode" => $code));
                        if($result && !$result['errors']) {
                            $product = $result['product'];

                            $products_to_display[] = array(
                                'name' => $product['name'],
                                'price' => $product['price'],
                                'count' => $count,
                                'amount' => $product['price']*$count
                            );

                            $i = 0;
                            while($i < $count) {
                                $products[] = array(
                                    'code' => $product['code'],
                                    'price' => $product['price']
                                );
                                $i++;
                            }
                        }
                    }

                    $result = $client->call('getTotal', array("APIKey" => $key, "APIPassword" => $pass, "recipientZip" => $_POST['recipient_zip'], "products" => $products));
                    if(!$result['errors']) {
                        $order_info = array(
                            'affiliate_service_charge' => $result['affiliateServiceCharge'],
                            'affiliate_tax' => $result['affiliateTax'],
                            'florist_one_service_charge' => $result['floristOneServiceCharge'],
                            'florist_one_tax' => $result['floristOneTax'],
                            'order_total' => $result['orderTotal'],
                            'service_charge_total' => $result['serviceChargeTotal'],
                            'sub_total' => $result['subTotal'],
                            'tax_total' => $result['taxTotal']
                        );
                        $smarty->assign("order_info", $order_info);
                        $form['total'] = $result['orderTotal'];
                    }

                    unset($form['checkout_form']);
                    unset($form['checkout_info']);
                    unset($form['selected_recipient_state']);
                    unset($form['selected_recipient_country']);
                    unset($form['selected_delivery_date']);
                    unset($form['selected_customer_state']);
                    unset($form['selected_customer_country']);
                    unset($form['selected_card_type']);
                    unset($form['selected_card_expiration_month']);
                    unset($form['selected_card_expiration_year']);

                    $form['products'] = serialize($products);

                    $smarty->assign("form", $form);
                    $smarty->assign("products", $products_to_display);

                } else {

                    $products = unserialize($form['products']);

                    $result = $client->call('placeOrder', array(
                        "APIKey" => $key,
                        "APIPassword" => $pass,
                        "recipient" => array(
                            "name" => @$form['recipient_name'],
                            "institution" => @$form['recipient_institution'],
                            "address1" => @$form['recipient_address1'],
                            "address2" => @$form['recipient_address2'],
                            "city" => @$form['recipient_city'],
                            "state" => @$form['recipient_state'],
                            "country" => @$form['recipient_country'],
                            "zip" => @$form['recipient_zip'],
                            "phone" => @$form['recipient_phone']
                        ),
                        "customer" => array(
                            "name" => @$form['customer_name'],
                            "address1" => @$form['customer_address1'],
                            "address2" => @$form['customer_address2'],
                            "city" => @$form['customer_city'],
                            "state" => @$form['customer_state'],
                            "country" => @$form['customer_country'],
                            "zip" => @$form['customer_zip'],
                            "phone" => @$form['customer_phone'],
                            "email" => @$form['customer_email'],
                        ),
                        "customerIP" => @$_SERVER['REMOTE_ADDR'] ? $_SERVER['REMOTE_ADDR'] : 0,
                        "ccInfo" => array(
                            "ccType" => @$form['card_type'],
                            "ccNum" => @$form['card_number'],
                            "ccExpMonth" => @$form['card_expiration_month'],
                            "ccExpYear" => @$form['card_expiration_year'],
                            "ccSecCode" => @$form['card_sec_code'],
                        ),
                        "products" => $products,
                        "cardMsg" => @$form['card_message'],
                        "specialInstructions" => @$form['special_instructions'],
                        "deliveryDate" => @$form['delivery_date'],
                        "orderTotal" => @$form['total']
                    ));

                    if(!$result['errors']) {
                        // clear cart
                        $_SESSION["florist_cart"] = array();
                        $smarty->assign("order_number", @$result['orderNumber']);
                        $smarty->assign("order_total", @$result['orderTotal']);
                        $smarty->assign("thanks", 1);
                    } else {
                        $errors['request_error'] = 'Error during request.';
                    }
                }
            } else { // validation errors
                $smarty->assign("errors", $errors);
                $smarty->assign("checkout_form", true);
                $smarty->assign("form", $form);
            }
        }

        if (!count($_POST) || count($errors)) {
            $years = array();
            $cur_year = intval(date("y"));
            $i = 0;
            while($i <= 15) {
                $years[] = $cur_year + $i;
                $i++;
            }

            $smarty->assign("years", $years);
            $smarty->assign("checkout_form", true);
        }
    } else {
        $smarty->assign("empty", true);
    }

    $html = $smarty->fetch("cnt_plugin_florist_checkout.tpl");
    return $html;
}

function validate_form($form) {
    $errors = array();

    $required_fields = array(
        'recipient_name' => 'Recipient Name',
        'recipient_address1' => 'Recipient Address1',
        'recipient_city' => 'Recipient City',
        'recipient_state' => 'Recipient State',
        'recipient_country' => 'Recipient Country',
        'recipient_zip' => 'Recipient Zip',
        'recipient_phone' => 'Recipient Phone',
        'delivery_date' => 'Delivery Date',
        'customer_name' => 'Customer Name',
        'customer_address1' => 'Customer Address1',
        'customer_city' => 'Customer City',
        'customer_state' => 'Customer State',
        'customer_country' => 'Customer Country',
        'customer_zip' => 'Customer Zip',
        'customer_phone' => 'Customer Phone',
        'customer_email' => 'Customer Email',
        'card_type' => 'Credit Card',
        'card_number' => 'Card Number',
        'card_expiration_month' => 'Card Expiration Month',
        'card_expiration_year' => 'Card Expiration Year',
        'card_sec_code' => 'Card Security Code',
        'card_message' => 'Card Message'
    );

    foreach($required_fields as $name=>$title) {
        if (!strlen($form[$name]) || (($name == 'recipient_state' || $name == 'customer_state') && ($form[$name] == 'u' || $form[$name] == 'c'))) {
            $errors[$name] = $title." required";
        }

        if(($name == 'recipient_phone' || $name == 'customer_phone') && !isset($errors[$name])) {
            preg_match_all("(\d+)", $form[$name], $matches);
            $digits = '';
            if($matches) {
                $digits = implode('', $matches[0]);
            }

            if(preg_match("([^0-9\-])", $form[$name]) || !strlen($digits) || strlen($digits) != 10) {
                $errors[$name] = $title.' can be 10  digits only or separated by dashes';
            }
        }

        if(($name == 'recipient_zip' || $name == 'customer_zip') && !isset($errors[$name]) && !preg_match("(^[0-9]{5}$)", $form[$name])) {
            $errors[$name] = 'Invalid '.$required_fields[$name];
        }
    }

    if(!isset($errors['customer_email']) && !preg_match("(^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$)", $form['customer_email'])) {
        $errors['customer_email'] = 'Invalid '.$required_fields['customer_email'];
    }

    if(!isset($errors['card_number']) && !isset($errors['card_type'])) {
        $invalid_card_number = false;
        switch($form['card_type']) {
            case 'AX':
                if(!preg_match("(^3[47][0-9]{13}$)", $form['card_number']))
                    $invalid_card_number = true;
                break;

            case 'VI':
                if(!preg_match("(^4[0-9]{12}(?:[0-9]{3})?$)", $form['card_number']))
                    $invalid_card_number = true;
                break;

            case 'MC':
                if(!preg_match("(^5[1-5][0-9]{14}$)", $form['card_number']))
                    $invalid_card_number = true;
                break;

            case 'DI':
                if(!preg_match("(^6(?:011|5[0-9]{2})[0-9]{12}$)", $form['card_number']))
                    $invalid_card_number = true;
                break;
        }

        if($invalid_card_number) {
            $errors['card_number'] = 'Invalid '.$required_fields['card_number'];
        }
    }

    if(!isset($errors['card_type']) && !isset($errors['card_sec_code']) && (($form['card_type'] == 'AX' && !preg_match("(^[0-9]{4}$)", $form['card_sec_code'])) || ($form['card_type'] != 'AX' && !preg_match("(^[0-9]{3}$)", $form['card_sec_code']))) ) {
        $errors['card_sec_code'] = 'Invalid '.$required_fields['card_sec_code'];
    }

    return $errors;
}
