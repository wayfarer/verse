<?php
/**
 * Page
 */

class Obituary extends Page {
  var $db,
      $domain_id,
      $data = NULL;

  function page(&$db, $domain_id, &$user, &$smarty) {
    $this->db = $db;
    $this->user = $user;
    $this->domain_id = $domain_id;
    $this->smarty = $smarty;
  }

  function draw() {

    $this->smarty->display("obituary.tpl");
  }

  function validate() {

  }

  function submit() {

  }

}
