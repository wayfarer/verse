<?php

/**
 * @desc Integrates VerseSMS and Piwik
 */

$token_auth = "7494d4b88b8e5e795c72cf2679f15f38";
$piwik_url = "https://twintierstech.net/piwik/";
$piwik_get_jscode_tpl = "?module=API&method=SitesManager.getJavascriptTag&idSite=%s&format=PHP&token_auth=$token_auth";
$piwik_add_domain_with_id_tpl = "?module=API&method=VerseIntegration.addSiteWithId&idSite=%d&siteName=%s&urls=%s&format=PHP&token_auth=$token_auth";

$piwik_add_user_tpl = "?module=API&method=UsersManager.addUser&userLogin=%s&password=%s&email=%s&alias=%s&format=PHP&token_auth=$token_auth";
$piwik_update_user_tpl = "?module=API&method=UsersManager.updateUser&userLogin=%s&password=%s&alias=%s&format=PHP&token_auth=$token_auth";
$piwik_set_user_access_tpl = "?module=API&method=UsersManager.setUserAccess&userLogin=%s&access=%s&idSites=%s&format=PHP&token_auth=$token_auth";
$piwik_user_exists_tpl = "?module=API&method=UsersManager.userExists&userLogin=%s&format=PHP&token_auth=$token_auth";
$piwik_get_token_auth_tpl = "?module=API&method=UsersManager.getTokenAuth&userLogin=%s&md5Password=%s&format=PHP&token_auth=$token_auth";
$piwik_delete_user_tpl = "?module=API&method=UsersManager.deleteUser&userLogin=%s&format=PHP&token_auth=$token_auth";

$piwik_logger = @Log::factory('file', 'logs/piwik_adapter.log', 'versesms');

function piwik_ensure_domain($domain_id) {
	global $db, $piwik_logger, $piwik_url, $piwik_get_jscode_tpl, $piwik_add_domain_with_id_tpl;

	$domain_id = intval($domain_id);
	if($domain_id) {
		// get domain information
		$query = "SELECT d.domain_id, domain_name, title, footer FROM sms_domain d INNER JOIN cms_site s USING(domain_id) WHERE d.domain_id='$domain_id'";
		$domain = $db->getRow($query, DB_FETCHMODE_ASSOC);
		// check if domain has piwik stats already
		if(strpos($domain["footer"], "<!-- Piwik -->")===false) {
			// add domain to piwik
			$title = $domain["title"];
			if(!$title) $title = $domain["domain_name"];
			$piwik_add_domain_with_id = sprintf($piwik_add_domain_with_id_tpl, $domain["domain_id"], urlencode($title), "http://".$domain["domain_name"]);
			$result = unserialize(file_get_contents($piwik_url.$piwik_add_domain_with_id));
			$piwik_logger->info($domain["domain_name"].": adding to piwik: ".print_r($result, 1));

			// get JS tracking code and add to the site's footer
			$piwik_get_jscode = sprintf($piwik_get_jscode_tpl, $domain["domain_id"]);
			$piwik_js_code = unserialize(file_get_contents($piwik_url.$piwik_get_jscode));

			$footer = $domain["footer"]."\n".$piwik_js_code;
			$query = "UPDATE cms_site SET footer = '".in($footer)."' WHERE domain_id='".$domain["domain_id"]."'";
			$db->query($query);
		}
	}
}

function piwik_rename_domain($domain_id, $old_domain_name, $new_domain_name) {
  global $db;

  // fetch users with statistics privilege
  $query = "SELECT u.user_id, login, name, domain_id FROM sms_user u INNER JOIN cms_user_role r USING (user_id) WHERE role_id=8 AND u.domain_id='$domain_id'";
  $users = $db->getAll($query, DB_FETCHMODE_ASSOC);

  foreach($users as $user) {
    $old_login = $user['login'].'.'.$old_domain_name;
    $new_login = $user['login'].'.'.$new_domain_name;

    piwik_delete_user_by_login($old_login);
    piwik_add_user_by_login($new_login, $user);
  }
}

function piwik_ensure_user($user_id, $password) {
	global $domain_id, $domain_name, $db, $piwik_logger, $piwik_url,
	$piwik_add_user_tpl, $piwik_update_user_tpl, $piwik_set_user_access_tpl, $piwik_user_exists_tpl, $piwik_get_token_auth_tpl;

	$user_id = intval($user_id);
	if(!$password) {
	  $password = substr(md5(uniqid(rand())),0,16);
	}
	$piwik_logger->info($domain_name.": piwik_ensure_user: ".$user_id);
	if($user_id) {
		// get user information
		$query = "SELECT user_id, login, name, password FROM sms_user WHERE user_id='$user_id' AND domain_id='$domain_id'";
		$user = $db->getRow($query, DB_FETCHMODE_ASSOC);
		$piwik_userlogin = $user['login'].".".$domain_name;
		$piwik_email = $user['login']."@".$domain_name;
		if(strpos($user['name'], 'pw:')!==false || strpos($user['name'], 'pass:')!==false) {
			$piwik_alias = $user['login'];
		}
		else {
			$piwik_alias = $user['name'];
		}

		// check user
		$piwik_user_exists = sprintf($piwik_user_exists_tpl, $piwik_userlogin);
		$user_exists = unserialize(file_get_contents($piwik_url.$piwik_user_exists));
		$piwik_logger->info("user exists: ".print_r($user_exists, 1));
		$piwik_logger->info($piwik_url.$piwik_user_exists);
		if($user_exists) {
			// update piwik users' password if set in order to retrieve token_auth below
			$piwik_update_user = sprintf($piwik_update_user_tpl, urlencode($piwik_userlogin), urlencode($password), urlencode($piwik_alias));
			$result = unserialize(file_get_contents($piwik_url.$piwik_update_user));
			$piwik_logger->info($domain_name.": update user $piwik_userlogin: ".print_r($result, 1));
		}
		else {
			if(!$password) $password = $user['password'];
			$piwik_add_user = sprintf($piwik_add_user_tpl, urlencode($piwik_userlogin), urlencode($password), urlencode($piwik_email), urlencode($piwik_alias));
			$result = unserialize(file_get_contents($piwik_url.$piwik_add_user));
			$piwik_logger->info($domain_name.": adding user $piwik_userlogin: ".print_r($result, 1));

			$piwik_set_user_access = sprintf($piwik_set_user_access_tpl, $piwik_userlogin, "view", $domain_id);
			$result = unserialize(file_get_contents($piwik_url.$piwik_set_user_access));
			$piwik_logger->info("set user access view: ".print_r($result, 1));
		}
		// retreive and remember the token_auth at local DB
		$piwik_get_token_auth = sprintf($piwik_get_token_auth_tpl, $piwik_userlogin, md5($password));
		$token_auth = unserialize(file_get_contents($piwik_url.$piwik_get_token_auth));
		$piwik_logger->info("token: ".print_r($token_auth, 1));
		if($token_auth) {
			$query = "UPDATE sms_user SET piwik_token_auth='".in($token_auth)."' WHERE user_id='".$user['user_id']."' AND domain_id='$domain_id'";
			$db->query($query);
			return $token_auth;
		}
	}
}

function piwik_ensure_user_v3(myUser $user) {
  global $piwik_url,
  $piwik_add_user_tpl, $piwik_update_user_tpl, $piwik_set_user_access_tpl, $piwik_user_exists_tpl, $piwik_get_token_auth_tpl;

  $piwik_logger = sfContext::getInstance()->getLogger();

  if(!$user->isSuperAdmin()) {
    $domain_name = Doctrine::getTable('SmsDomain')->getCurrent()->getDomainName();
    $password = substr(md5(uniqid(rand())),0,16);
    $piwik_logger->info($domain_name.": piwik_ensure_user_v3: ".$user->getUsername().'.'.$domain_name);

    $piwik_userlogin = $user->getUsername().'.'.$domain_name;
    $piwik_email = $user->getUsername()."@".$domain_name;
    if(strpos($user->getProfile()->getName(), 'pw:')!==false || strpos($user->getProfile()->getName(), 'pass:')!==false) {
      $piwik_alias = $user->getUsername();
    }
    else {
      $piwik_alias = $user->getProfile()->getName();
    }

    // check user
    $piwik_user_exists = sprintf($piwik_user_exists_tpl, $piwik_userlogin);
    $user_exists = unserialize(file_get_contents($piwik_url.$piwik_user_exists));
    $piwik_logger->info("user exists: ".print_r($user_exists, 1));
    $piwik_logger->info($piwik_url.$piwik_user_exists);
    if($user_exists) {
      // update piwik users' password if set
      /*if($password) {
        $piwik_update_user = sprintf($piwik_update_user_tpl, urlencode($piwik_userlogin), urlencode($password), urlencode($piwik_alias));
        $result = unserialize(file_get_contents($piwik_url.$piwik_update_user));
        $piwik_logger->info($domain_name.": update user $piwik_userlogin: ".print_r($result, 1));
      }
      else  return; // no further processing
      */
    }
    else {
      $piwik_add_user = sprintf($piwik_add_user_tpl, urlencode($piwik_userlogin), urlencode($password), urlencode($piwik_email), urlencode($piwik_alias));
      $result = unserialize(file_get_contents($piwik_url.$piwik_add_user));
      $piwik_logger->info($domain_name.": adding user $piwik_userlogin: ".print_r($result, 1));

      $piwik_set_user_access = sprintf($piwik_set_user_access_tpl, $piwik_userlogin, "view", Doctrine::getTable('SmsDomain')->getCurrent()->getDomainId());
      $result = unserialize(file_get_contents($piwik_url.$piwik_set_user_access));
      $piwik_logger->info("set user access view: ".print_r($result, 1));
    }
    // retreive and remember the token_auth at local DB
    $piwik_get_token_auth = sprintf($piwik_get_token_auth_tpl, $piwik_userlogin, md5($password));
    $token_auth = unserialize(file_get_contents($piwik_url.$piwik_get_token_auth));
    $piwik_logger->info("token: ".print_r($token_auth, 1));
  }
  else {
    sfContext::getInstance()->getLogger()->info('token for superadmin set');
    $token_auth = '7494d4b88b8e5e795c72cf2679f15f38';
  }
  if($token_auth) {
    $user->getProfile()->setPiwikTokenAuth($token_auth)->save();
//    $query = "UPDATE sms_user SET piwik_token_auth='".in($token_auth)."' WHERE user_id='".$user['user_id']."' AND domain_id='$domain_id'";
//    $db->query($query);
    return $token_auth;
  }
}

function piwik_delete_user($user_id) {
  global $domain_id, $domain_name, $db, $piwik_logger, $piwik_url, $piwik_delete_user_tpl;

  $user_id = intval($user_id);
  $piwik_logger->info($domain_name.": piwik_delete_user: ".$user_id);
  if($user_id) {
    // get user information
    $query = "SELECT login FROM sms_user WHERE user_id='$user_id' AND domain_id='$domain_id'";
    $user = $db->getRow($query, DB_FETCHMODE_ASSOC);
    $piwik_userlogin = $user['login'].".".$domain_name;

    $piwik_delete_user = sprintf($piwik_delete_user_tpl, urlencode($piwik_userlogin));
    $result = unserialize(file_get_contents($piwik_url.$piwik_delete_user));
    $piwik_logger->info($domain_name.": delete user $piwik_userlogin: ".print_r($result, 1));
  }
}

function piwik_delete_user_by_login($user_login) {
  global $db, $piwik_logger, $piwik_url, $piwik_delete_user_tpl;

  $piwik_logger->info("piwik_delete_user_by_login: ".$user_login);

  $piwik_delete_user = sprintf($piwik_delete_user_tpl, urlencode($user_login));
  $result = unserialize(file_get_contents($piwik_url.$piwik_delete_user));
  $piwik_logger->info("delete user $user_login: ".print_r($result, 1));
}

function piwik_add_user_by_login($login, $user) {
  global $db, $piwik_logger, $piwik_url, $piwik_add_user_tpl, $piwik_set_user_access_tpl, $piwik_get_token_auth_tpl;

  $piwik_logger->info("piwik_add_user_by_login: ".$login);

  // prepare needed stuff
  $password = substr(md5(uniqid(rand())),0,16);
  // transform login to email
  $piwik_email = $login;
  $piwik_email[strpos($piwik_email, '.')] = '@';
  // alias
  if(strpos($user['name'], 'pw:')!==false || strpos($user['name'], 'pass:')!==false) {
    $piwik_alias = $user['login'];
  }
  else {
    $piwik_alias = $user['name'];
  }

  $piwik_add_user = sprintf($piwik_add_user_tpl, urlencode($login), urlencode($password), urlencode($piwik_email), urlencode($piwik_alias));
  $result = unserialize(file_get_contents($piwik_url.$piwik_add_user));
  $piwik_logger->info("adding user $login: ".print_r($result, 1));

  $piwik_set_user_access = sprintf($piwik_set_user_access_tpl, $login, "view", $user['domain_id']);
  $result = unserialize(file_get_contents($piwik_url.$piwik_set_user_access));
  $piwik_logger->info("set user access view: ".print_r($result, 1));

  // retreive and remember the token_auth at local DB
  $piwik_get_token_auth = sprintf($piwik_get_token_auth_tpl, $login, md5($password));
  $token_auth = unserialize(file_get_contents($piwik_url.$piwik_get_token_auth));
  $piwik_logger->info("token: ".print_r($token_auth, 1));
  if($token_auth) {
    $query = "UPDATE sms_user SET piwik_token_auth='".in($token_auth)."' WHERE user_id='".$user['user_id']."' AND domain_id='".$user['domain_id']."'";
    $db->query($query);
  }
}
