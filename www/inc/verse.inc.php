<?php
// main include header
require("config.inc.php");
require("util.inc.php");

// hack to unserialize ObituarySearchCriterion from session correctly
if(file_exists("src/Verse/Obituary/ObituarySearchCriterion.php")) {
    require_once 'src/Verse/Obituary/ObituarySearchCriterion.php';
    require_once 'src/Verse/Domain/Domain.php';
}

require_once("DB.php");
require_once("Log.php");

$smarty = createobject("Smarty");
$smarty->debugging = SMARTY_DEBUG;
$smarty->force_compile = SMARTY_FORCE_RECOMPILE;

$default_page = DEFAULT_PAGE;

// init logging mechanism
$conf = array('title' => 'Log');
$logger = &Log::factory('file', 'logs/versesms.log', 'versesms');

if (isset($_GET["z"])) {
    //	$logger->log("[VSMS] sessionid=".$_GET["z"]."; host=".$_SERVER["HTTP_HOST"]."; from=".$_SERVER["HTTP_REFERER"], PEAR_LOG_INFO);
    session_id($_GET["z"]);
    session_start();

    // SECURITY:
    // need to check logged user are from this domain!

    // clear URL from session id, avoid session storing to bookmarks
    header("Location: " . $_SERVER["SCRIPT_NAME"]);
    exit;
}
if (isset($_POST["z"])) {
    session_id($_POST["z"]);
    $force_session_flag = true;

    // SECURITY:
    // need to check logged user are from this domain!
}

session_start();

// get domain name
$domain_name = $_SERVER["HTTP_HOST"];
// remove www. from begin if there
if (!strcasecmp(substr($domain_name, 0, 4), "www.")) {
    $domain_name = substr($domain_name, 4);
    // permament redirect (no www)
    header("HTTP/1.1 301 Moved Permanently");
    header("Location: http://$domain_name" . $_SERVER['REQUEST_URI']);
    exit;
}
// check for dev. at beginning, it indicates force production mode
$force_production = false;
if (!strcasecmp(substr($domain_name, 0, 4), "dev.")) {
    $domain_name = substr($domain_name, 4);
    $force_production = true;
}
// embedding, check for embed. , remove and set the flag
$embedded_mode = false;
if (!strcasecmp(substr($domain_name, 0, 6), "embed.")) {
    $domain_name = substr($domain_name, 6);
    $embedded_mode = true;
}

// determine DB to connect to
$dbname = 'db_versesms';
$secondary_domains = @unserialize(@file_get_contents('files/secondary_domains.txt'));
if (is_array($secondary_domains)) {
    if (in_array($domain_name, $secondary_domains)) {
        $dbname = 'db_versesms_secondary';
    }
}

// connect to DB
$db_con = createobject($dbname);
$db = $db_con->connect();

// $db->query("SET NAMES 'utf8'");
mysql_set_charset('utf8', $db->connection);
// $db->query("SET time_zone '-5:00'"); // EST, America/New York

if (DB::isError($db)) {
    //	echo $db->getMessage();
    //	echo "<br>";
    //	echo $db->getUserInfo();
}

// check for domain not changed
if (!isset($_SESSION["domain_id"]) || $_SESSION["domain_name"] != $domain_name || $_SESSION["domain_id"] == 0) {
    // domain have changed or not determined - do it
    // fetch domain_id for the domain
    $query = "SELECT domain_id FROM sms_domain WHERE domain_name='" . in($domain_name) . "' AND enabled=1";
    $domain_id = $db->getOne($query);
    if (!$domain_id) {
        // dev and test domains
        $alt_domain_name = '';
        if (!strcasecmp(substr($domain_name, -4), 'test')) {
            $alt_domain_name = substr($domain_name, 0, -4) . "com";
        }
        if (!strcasecmp(substr($domain_name, -3), 'dev')) {
            $alt_domain_name = substr($domain_name, 0, -3) . "com";
        }
        if ($alt_domain_name) {
            $query = "SELECT domain_id FROM sms_domain WHERE domain_name='" . in($alt_domain_name) . "' AND enabled=1";
            $domain_id = $db->getOne($query);
        }
    }
    if (!$domain_id) {
        if (!isset($pass_unknown_domain) && !isset($_SESSION["domain_id"])) {
            die(VERSE_SERVER_NAME . ": " . $domain_name . " is under maintenance. Please come back later");
        }
        else {
            $_SESSION["domain_id"] = 0;
            $_SESSION["domain_name"] = $domain_name;
        }
    }
    else {
        // aliases
        $query = "SELECT alias_domain_id FROM sms_domain WHERE domain_id='$domain_id'";
        $alias_domain_id = $db->getOne($query);
        if ($alias_domain_id) {
            $query = "SELECT domain_name FROM sms_domain WHERE domain_id='$alias_domain_id'";
            $domain_name = $db->getOne($query);
            // redirect
            header("HTTP/1.1 301 Moved Permanently");
            header("Location: http://$domain_name" . $_SERVER['REQUEST_URI']);
            exit;
            $domain_id = $alias_domain_id;
        }
        $_SESSION["domain_id"] = $domain_id;
        $_SESSION["domain_name"] = $domain_name;
    }
} else {
    $domain_id = $_SESSION["domain_id"];
    $domain_name = $_SESSION["domain_name"];
}

// process affiliates
if (isset($_GET["ref"])) {
    $query = "SELECT affiliate_id, page_id FROM cms_affiliate WHERE ref='" . in($_GET["ref"]) . "' AND domain_id='$domain_id'";
    $row = $db->getRow($query, DB_FETCHMODE_ASSOC);
    if (!DB::isError($row) && $row) {
        // store affiliate in session
        $_SESSION["affiliate_id"] = $row["affiliate_id"];
        $_SESSION["affiliate_from"] = $_SERVER["HTTP_REFERER"];
        $page_id = $row["page_id"];
        $query = "SELECT internal_name FROM cms_page WHERE page_id='$page_id' AND domain_id='$domain_id'";
        $page = $db->getOne($query);
        if (!DB::isError($page) && $page) {
            // redirect
            header("Location: ?p=$page");
            exit;
        }
    }
}

// id=111, special secure domain: secure.twintierstech.net
if ($domain_id == 111) {
    // pass only through secure protocol
    if (!$_SERVER["HTTPS"]) die("no ssl");

    $orig_domain = $_GET["from"];
    $query = "SELECT domain_id FROM sms_domain WHERE domain_name='" . in($orig_domain) . "'";
    $orig_domain_id = $db->getOne($query);

    // terminate if no data about originating domain
    if (!$orig_domain_id) die("no orig");

    // set context to originating domain
    $domain_id = $orig_domain_id;
    $domain_name = $orig_domain;

    // if session wasn't set forcible by passing session_id
    if (!isset($force_session_flag)) {
        // restart session and clear previous state
        session_regenerate_id(true);
        session_unset();
    }
}

$user = createobject("user", array($db, "sms_user"));

if (isset($_POST["login"])) {
    if ($user->login($_POST["login"], $_POST["password"])) {
        // check user domain
        if ($user->data["domain_id"] != 0 && $user->data["domain_id"] != $domain_id) {
            $user->logout();
        }
        else {
            // all ok, pass user
            /*      if($_SESSION["refering_page"]) {
             $parts = parse_url($_SESSION["refering_page"]);
             $_SESSION["refering_page"] = null;
             $retto = "";
             if($parts["path"]) $retto = $parts["path"];
             if($parts["query"]) $retto .= "?".$parts["query"];
             if($retto) {
               header("Location: $retto");
               exit;
             }
           } */
            // processing script will redirect
            //		    header("Location: admin.php");
            //			exit;
        }
    }
}

$domain = createobject("domain", array($db, $domain_id, $user));

// legacy compatibility
// set id parameter to second argument value
if (!isset($_GET["id"]) && arg(1)) {
    $_GET["id"] = arg(1);
}
