<?php

/**
 * @desc Plugs phpMyVisites(PMV) into FuneralSMS(FSMS)  
 */

/**
 * sets(creates) a PMV domain that will correspond to the specified one from FSMS
 */
function phpmv_domain_set($domain_id){
	//local connection
	global $db, $logger;
	//phpmv db connection
//	$db2 = _phpmv_db_connect(); if(!$db2) return 0;

  // collects and sends commands to remote script for DB processing via CURL
  $commands = array();


	$domain = _phpmv_load_fsms_domain($domain_id);

	//0. if domain is alias then do nothing (extra check..actually it should have been checked before the call)
	if((int)$domain->alias_domain_id) return 0;
	
	//1. check if relation to a PMV domain is already set in the local DB
	$query = "SELECT phpmv_domain_id FROM sms_domain_fsms2phpmv WHERE fsms_domain_id=$domain_id";
	$domain->phpmv_domain_id = (int)$db->getOne($query);
	
	//1.1 update PMV domain settings if the relation already exists
	if($domain->phpmv_domain_id){
		if($domain->title) {
			$title = $domain->title;
		}
		else {
			$title = $domain->fsms_domain_name;
		}

		$commands[] = array("update_domain"=>array("title"=>$title, "phpmv_domain_id"=>$domain->phpmv_domain_id, "url"=>$domain->fsms_domain_name));
    //$query = "UPDATE phpmv_site SET name='".in($title)."' WHERE idsite=$domain->phpmv_domain_id";
		//$db2->query($query);
		//$query = "UPDATE phpmv_site_url SET url='http://$domain->fsms_domain_name' WHERE idsite=$domain->phpmv_domain_id";
		//$db2->query($query);
	}
	//1.2 no relation exists
	else{
		/* $query = "SELECT idsite FROM phpmv_site_url WHERE url='http://$domain->fsms_domain_name'";
		$domain->phpmv_domain_id = (int)$db2->getOne($query);
		//1.2.1 same PMV domain exists but no relation in FSMS DB exists. Set the relation only 
		if($domain->phpmv_domain_id){
			$query = "INSERT sms_domain_fsms2phpmv SET fsms_domain_id=$domain->fsms_domain_id,phpmv_domain_id=$domain->phpmv_domain_id";
			$db->query($query);
		}
		//1.2.2 neither relation nor same PMV domain exists. Create PMV domain and set the relation
		else{ */
		// check for existance and create domain in PMV DB
		if($domain->title) {
			$title = $domain->title;
		}
		else {
			$title = $domain->fsms_domain_name;
		}
			
    //$query = "INSERT phpmv_site SET name='".in($title)."',logo='41.png',params_choice='all',params_names=''";
		//$db2->query($query);
		//$query = "SELECT last_insert_id()";	
		//$domain->phpmv_domain_id = (int)$db2->getOne($query);
		//$query = "INSERT phpmv_site_url SET idsite=$domain->phpmv_domain_id,url='http://$domain->fsms_domain_name'";
		//$db2->query($query);
  	$commands[] = array("insert_domain"=>array("title"=>$title, "url"=>$domain->fsms_domain_name));
			
    // SEE BELOW
    //save cross-domains relation in the local db
		//$query = "INSERT sms_domain_fsms2phpmv SET fsms_domain_id=$domain->fsms_domain_id,phpmv_domain_id=$domain->phpmv_domain_id";
		//$db->query($query);			
	}
	    
  // send commands  
  if($commands) {
    $url = "http://twintierstech.net/phpmv_gate.php";
    $post = "action=phpmv&data=".urlencode(serialize($commands));
  
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

    $result = curl_exec($ch);
    if(curl_errno($ch)) {
    	$result = curl_error($ch);
      $logger->err("domain_set: curl error: $result");
    }
    else {
      $result = unserialize($result);
      // process reply
      if(isset($result["insert_domain"])) {
        $domain->phpmv_domain_id = intval($result["insert_domain"]["phpmv_domain_id"]);
        //save cross-domains relation in the local db
  			$query = "INSERT sms_domain_fsms2phpmv SET fsms_domain_id='$domain->fsms_domain_id', phpmv_domain_id='$domain->phpmv_domain_id'";
  			$db->query($query);			
        $logger->info("domain_set: domain_id=$domain->fsms_domain_id, fsms_domain_id=$domain->phpmv_domain_id");
      }
    }
  }
	
  //set JS code into site footer (if no code there yet)
	$query = "SELECT footer FROM cms_site WHERE domain_id=$domain->fsms_domain_id";
	$footer = $db->getOne($query);
	if(strpos($footer, "<!-- phpmyvisites -->")===false) {
		$footer .= _phpmv_generate_js($domain->phpmv_domain_id);
		$query = "UPDATE cms_site SET footer='".in($footer)."' WHERE domain_id=$domain->fsms_domain_id";
		$db->query($query);	
	}

	//kill PMV domains cache
//	@unlink(PHPMV_PATH.'/config/site_urls.php');
//	@unlink(PHPMV_PATH.'/config/site_info.php');
	
	return 1;
}

/**
 * sets(creates) a PMV user that will correspond to the specified one from FSMS
 */
function phpmv_user_set($user_id, $domain_id){
	//local connection
	global $db, $logger, $domain;
//	$db2 = _phpmv_db_connect(); if(!$db2) return 0;
  $commands = array();

	$query = "SELECT fsms_uid FROM sms_user_fsms2phpmv WHERE fsms_uid=$user_id";
	//related user doesn't exist yet, set a new one
	if(!(int)$db->getOne($query)){
		$query = "SELECT phpmv_domain_id FROM sms_domain_fsms2phpmv WHERE fsms_domain_id=$domain_id";
		$phpmv_domain_id = (int)$db->getOne($query);
		if(!$phpmv_domain_id){
			$logger->err('An attempt to create PHPMV user for non-set in PHPMV domain(FSMS domain_id='.$domain_id.').');
			return 0;
		}

    // fetch user's data
    $query = "SELECT login, password FROM sms_user WHERE user_id='$user_id'";
    $ret = $db->getRow($query, DB_FETCHMODE_ASSOC);
		
		$user = new stdClass();
		$user->uid = $user_id;
    $user->login = $ret["login"]."-".$domain->data["postfix"];
    $user->password = $ret["password"];
//		$user->login = 'funeralsmsuser'.$user_id;
//		$user->password = md5($user->login.$domain_id.time());
		
    $commands[] = array("insert_user" => array("login"=>$user->login, "password"=>$user->password, "phpmv_domain_id"=>$phpmv_domain_id));

/*		//set user record
		$query = "INSERT phpmv_users SET login='$user->login',password='$user->password',email='',rss_hash='',date_registered=".time();
		$db2->query($query);
		//set user permission(2==view)
		$query = "INSERT phpmv_users_link_groups SET idsite=$phpmv_domain_id,idgroups=2,login='$user->login'";
		$db2->query($query); */
		
		//save cross-domains relation in the local db
		$query = "INSERT sms_user_fsms2phpmv SET fsms_uid=$user->uid,phpmv_login='$user->login',phpmv_password='$user->password',created=".time();
		$db->query($query);		
	}

  if($commands) {
    $url = "http://twintierstech.net/phpmv_gate.php";
    $post = "action=phpmv&data=".urlencode(serialize($commands));
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

    $result = curl_exec($ch);
    if(curl_errno($ch)) {
    	$result = curl_error($ch);
      $logger->err("user_set: curl error: $result");
    }
	}
	return 1;
}

/**
 * unsets(deletes) a PMV user that corresponds to the specified one from FSMS
 */
function phpmv_user_unset($user_id){
	//local connection
	global $db, $logger;
//	$db2 = _phpmv_db_connect(); if(!$db2) return 0;
  $commands = array();
		
	$query = "SELECT phpmv_login FROM sms_user_fsms2phpmv WHERE fsms_uid=$user_id";
	$phpmv_login = $db->getOne($query);
	if(!$phpmv_login){
		$logger->log('No PHPMV related user was found when trying to delete the one.');
		return 0;
	}
	
  $commands[] = array("delete_user" => array("login"=>$phpmv_login));
/*	//unset user record
	$query = "DELETE FROM phpmv_users WHERE login='$phpmv_login'";
	$db2->query($query);
	//unset user permissions
	$query = "DELETE FROM phpmv_users_link_groups WHERE login='$phpmv_login'";
	$db2->query($query); */
	
	//unset cross-domains relation from the local db
	$query = "DELETE FROM sms_user_fsms2phpmv WHERE fsms_uid='$user_id'";
  $db->query($query);

  if($commands) {
    $url = "http://twintierstech.net/phpmv_gate.php";
    $post = "action=phpmv&data=".urlencode(serialize($commands));
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

    $result = curl_exec($ch);
    if(curl_errno($ch)) {
    	$result = curl_error($ch);
      $logger->err("user_unset: curl error: $result");
    }
	}
	
	return 1;
}

/**
 * connect to PMV database
 * @return connection resource
 */
function _phpmv_db_connect(){
	global $logger;

	$db_con = createobject('db_phpmv');
	$db_phpmv = $db_con->connect();
	
	if(DB::isError($db_phpmv)){
		$logger->err('Fatal Error: Unable to connect to phpMyVisites database.'.get);
		return NULL;
	}
	
	return $db_phpmv;
}

/**
 * loads FSMS domain object
 * @return object
 */
function _phpmv_load_fsms_domain($domain_id){
	//local connection
	global $db;
	$query = "SELECT d.domain_id as fsms_domain_id,domain_name as fsms_domain_name,alias_domain_id, title FROM sms_domain d INNER JOIN cms_site USING(domain_id) WHERE d.domain_id=$domain_id";
	$res = $db->query($query);
	return $res->fetchRow(DB_FETCHMODE_OBJECT);
}

/**
 * generates PHPMV JS code
 * @return string
 */
function _phpmv_generate_js($phpmv_idsite){
	$output = '<!-- phpmyvisites -->
<a href="http://www.phpmyvisites.net/" title="Free web analytics, website statistics"
onclick="window.open(this.href);return(false);"><script type="text/javascript">
<!--
var a_vars = Array();
var pagename="";

var phpmyvisitesSite = '.$phpmv_idsite.';
var phpmyvisitesURL = "'.PHPMV_BASE_URL.'/phpmyvisites.php";
//-->
</script>
<script language="javascript" src="'.PHPMV_BASE_URL.'/phpmyvisites.js" type="text/javascript"></script>
<noscript>
<img src="'.PHPMV_BASE_URL.'/phpmyvisites.php" style="border:0" width="1" height="1"/>
</noscript></a>
<!-- /phpmyvisites -->';
	return $output;
}