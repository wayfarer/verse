<?php
// symfony utilities, including image utils
// include "../../symfony/lib/VerseUtil.class.php";

function thumbnail($imagepath, $max_size = 100) {
  	global $domain_id, $domain_name;
  	$app_thumbnails_dir = "_thumbs";

    $a_domain_name = $domain_name;

    if(!trim($imagepath)) return;
    if($imagepath[0]=='/') {
        $imagepath = substr($imagepath, 1);
    }
    if(strpos($imagepath, "/")!==false || strpos($imagepath, "\\")!==false) {
        $potential_domain = substr($imagepath, 0, strpos($imagepath, '/'));
        if(is_dir("files/$potential_domain")) {
            $a_domain_name = $potential_domain;
            $imagepath = substr($imagepath, strpos($imagepath, '/')+1);
        }
    }
    else {
        $imagepath = "image/$imagepath";
    }
    $uploadDir = "files/$a_domain_name";
    $full_imagepath = $uploadDir.leading_slash($imagepath);

    if(!file_exists($full_imagepath)) { // try to find it under image/ still not found
        $full_imagepath = $uploadDir.'/image'.leading_slash($imagepath);
    }

    if(file_exists($full_imagepath)) {
      $pathinfo = pathinfo($imagepath);
      $tnname = $pathinfo['dirname'].'/'.$pathinfo['filename'].'_'.$max_size.'.'.$pathinfo['extension'];
      $tnpath = $app_thumbnails_dir.leading_slash($tnname);
      if(!file_exists($uploadDir.$tnpath)) {
        // ensure destination directory
        $pathinfo = pathinfo($tnpath);
        if(!file_exists($uploadDir.'/'.$pathinfo['dirname'])) {
          // recursively create destination directory
          mkdir($uploadDir.'/'.$pathinfo['dirname'], 0755, true);
        }
        resizejpg($full_imagepath, $max_size, $uploadDir."/".$tnpath);
      }
      if($a_domain_name != $domain_name) {
          $tnpath = $a_domain_name.'/'.$tnpath;
      }
      return $tnpath;
    }
    else return $imagepath;
}

function resizejpg($filename, $new_width, $newfilename="") {
	global $logger;

  if(is_file($filename)) {
    list($width, $height, $type) = getimagesize($filename);
    // resize only to smaller size by width, or just copy the image
    if($width>$new_width) {
      // calculate new height according to width
      $percent = $new_width / $width;
      $new_height = $height * $percent;

      $image_p = imagecreatetruecolor($new_width, $new_height);
      switch($type) {
        case IMAGETYPE_JPEG:
          $image = imagecreatefromjpeg($filename);
        break;
        case IMAGETYPE_GIF:
          $image = imagecreatefromgif($filename);
        break;
        case IMAGETYPE_PNG:
          $image = imagecreatefrompng($filename);
        break;
      }
      if(!imagecopyresampled($image_p, $image, 0, 0, 0, 0, $new_width, $new_height, $width, $height)) {
        $logger->log("[IMAGEUTIL] invalid file given for resizing: $filename", PEAR_LOG_ERR);
      }
      // write new file
      if($newfilename) $filename = $newfilename;
      imagejpeg($image_p, $filename);
    }
    else {
        copy($filename, $newfilename);
    }
  }
  else {
      // log invalid filename for further investigation
      $trace=debug_backtrace(false);
      $caller=array_shift($trace);
      $caller_str = 'called from '.$caller['file'].':'.$caller['line'];
      $logger->log("[IMAGEUTIL] invalid file given for resizing: $filename, called from: $caller_str", PEAR_LOG_ERR);
  }
}
