<?php

// prepares string or array of strings coming from browser for inserting to database query
function in($values) {
  if(is_array($values)) {
    foreach ($values as $key => $value) {
      $values[$key] = in($value);
    }
  }
  else if(!is_numeric($values) && !is_null($values) && !is_bool($values)) {
    if(get_magic_quotes_gpc()==1) {
      $values = stripslashes($values);
    }
    $values = mysql_real_escape_string($values);
  }
  return $values;
}

/*function in($str) {
	if(get_magic_quotes_gpc()==1)
		return $str;
	else
		return addslashes($str);
}*/

function out($str)
{
  if(is_array($str)) {
  	foreach($str as $key=>$value) {
  		$str[$key] = htmlspecialchars($value);
  	}
  }
  else
  	$str = htmlspecialchars($str);
  return $str;
}

function in_strip($str)
{
  if(get_magic_quotes_gpc()==1) {
	  if(is_array($str)) {
	  	foreach($str as $key=>$value) {
  			$str[$key] = stripslashes($value);
  		}
  	}
  	else
  		$str = stripslashes($str);
  }
  return $str;
}

        // returns time in floating point format with microseconds since Unix Epoch
function getmicrotime() {
	list($usec, $sec) = explode(" ",microtime());
	return ((float)$usec + (float)$sec);
}

function check_email($email)
{
  $email = trim($email);
  if (strlen($email)==0) {
    return false;
  }
  if(!(ereg('^[-!#$%&\'*+\\./0-9=?A-Z^_`a-z{|}~]+@[-!#$%&\'*+\\/0-9=?A-Z^_`a-z{|}~]+\.[-!#$%&\'*+\\./0-9=?A-Z^_`a-z{|}~]+$', $email))) {
	return false;
  }
  return true;
}

function &createobject($classname, $args = array())
{
      if(file_exists("inc/".$classname.".class.php"))
              include_once("inc/".$classname.".class.php");
      else
              include_once($classname.".class.php");

      // extract pure class name from $classname (it can be path)
      $class = substr($classname, ($pos = strrpos($classname, "/"))?$pos+1:0);

      $args_count = count($args);
      if($args_count == 0) {
        $obj = new $class;
      }
      else {
        $construct = "\$obj = new $class(\$args[0]";
        for($i=1; $i<$args_count; $i++) {
                $construct.=", \$args[$i]";
        }
        $construct.=");";
        eval($construct);
      }
      return $obj;
}

function make_json_response($arr)
{
	$response = "";
	foreach($arr as $field=>$value) {
		if(!is_array($value)) {
			$response.="'$field':'".str_replace(array("\\","'"), array("\\\\","\'"), $value)."',";
		}
		else {
			$response.="'$field':".make_json_response($value).",";
		}
	}
	$response = "({".str_replace(array("\r","\n"), array("\\r","\\n"), substr($response,0,-1))."})";
	return $response;
}

function make_set_clause($p)
{
	$ret = "";
	foreach($p as $key=>$value) {
		if(!is_null($value)) {
			$ret.="$key='$value',";
		}
		else {
			$ret.="$key=NULL,";
		}
	}
	return substr($ret, 0, -1);
}

function utf8_to_latin($p) {
  if(is_array($p)) {
  	foreach($p as $key=>$value) {
  		$p[$key] = iconv('UTF-8', 'ISO-8859-1//TRANSLIT', $value);
  	}
  }
  else
  	$p = iconv('UTF-8', 'ISO-8859-1//TRANSLIT', $p);
  return $p;
}

function get_music_files() {
  global $domain_name;

  $music_files = array();
  // shared music
  foreach(glob("assets/*.mp3") as $filename) {
    $fn = substr($filename, 7, -4);
    $title = ucfirst($fn);
    $music_files[$filename] = $title;
  }
  // local music
  foreach(glob("files/$domain_name/mp3/*.mp3") as $filename) {
    $fn = substr($filename, strrpos($filename, "/")+1);
    $filename = "mp3/$fn";
    $title = ucfirst(substr($fn, 0, -4));
    $music_files[$filename] = $title;
  }
  return $music_files;
}

function page_exists($page) {
  global $domain_id, $db;
  $query = "SELECT count(*) FROM cms_page WHERE internal_name='".in($page)."' AND domain_id='$domain_id'";
  return $db->getOne($query);
}

function uc_settings_exist() {
  global $domain_id, $db;
  $query = "SELECT 1 FROM cms_site_uc WHERE domain_id='$domain_id'";
  return $db->getOne($query);
}

/* function url($page, $query) {
  $params = array();
  foreach($query as $key=>$value) {
    $params[] = urlencode($key)."=".urlencode($value);
  }
  return "?p=$page&".implode("&", $params);
} */

function verse_set_message($message = NULL, $type = 'status', $repeat = TRUE) {
  if ($message) {
    if (!isset($_SESSION['messages'])) {
      $_SESSION['messages'] = array();
    }

    if (!isset($_SESSION['messages'][$type])) {
      $_SESSION['messages'][$type] = array();
    }

    if ($repeat || !in_array($message, $_SESSION['messages'][$type])) {
      $_SESSION['messages'][$type][] = $message;
    }
  }

  // messages not set when DB connection fails, if DB sessions used
  return isset($_SESSION['messages']) ? $_SESSION['messages'] : NULL;
}

function verse_get_messages($type = NULL, $clear_queue = TRUE) {
  if ($messages = verse_set_message()) {
    if ($type) {
      if ($clear_queue) {
        unset($_SESSION['messages'][$type]);
      }
      if (isset($messages[$type])) {
        return array($type => $messages[$type]);
      }
    }
    else {
      if ($clear_queue) {
        unset($_SESSION['messages']);
      }
      return $messages;
    }
  }
  return array();
}

function verse_format_messages($display = NULL) {
  $output = '';
  foreach (verse_get_messages($display) as $type => $messages) {
    $output .= "<div class=\"messages $type\">\n";
    if (count($messages) > 1) {
      $output .= " <ul>\n";
      foreach ($messages as $message) {
        $output .= '  <li>'. $message ."</li>\n";
      }
      $output .= " </ul>\n";
    }
    else {
      $output .= $messages[0];
    }
    $output .= "</div>\n";
  }
  return $output;
}

/**
 * URL filter. Automatically converts text web addresses (URLs, e-mail addresses,
 * ftp links, etc.) into hyperlinks.
 */
function _filter_url($text) {
  // Pass length to regexp callback
  _filter_url_trim(NULL, 72);

  $text = ' '. $text .' ';

  // Match absolute URLs.
  $text = preg_replace_callback("`(<p>|<li>|<br\s*/?>|[ \n\r\t\(])((http://|https://|ftp://|mailto:|smb://|afp://|file://|gopher://|news://|ssl://|sslv2://|sslv3://|tls://|tcp://|udp://)([a-zA-Z0-9@:%_+*~#?&=.,/;-]*[a-zA-Z0-9@:%_+*~#&=/;-]))([.,?!]*?)(?=(</p>|</li>|<br\s*/?>|[ \n\r\t\)]))`i", '_filter_url_parse_full_links', $text);

  // Match e-mail addresses.
  $text = preg_replace("`(<p>|<li>|<br\s*/?>|[ \n\r\t\(])([A-Za-z0-9._-]+@[A-Za-z0-9._+-]+\.[A-Za-z]{2,4})([.,?!]*?)(?=(</p>|</li>|<br\s*/?>|[ \n\r\t\)]))`i", '\1<a href="mailto:\2">\2</a>\3', $text);

  // Match www domains/addresses.
  $text = preg_replace_callback("`(<p>|<li>|[ \n\r\t\(])(www\.[a-zA-Z0-9@:%_+*~#?&=.,/;-]*[a-zA-Z0-9@:%_+~#\&=/;-])([.,?!]*?)(?=(</p>|</li>|<br\s*/?>|[ \n\r\t\)]))`i", '_filter_url_parse_partial_links', $text);
  $text = substr($text, 1, -1);

  return $text;
}

/**
 * Make links out of absolute URLs.
 */
function _filter_url_parse_full_links($match) {
  $match[2] = decode_entities($match[2]);
  $caption = check_plain(_filter_url_trim($match[2]));
  $match[2] = check_url($match[2]);
  return $match[1] .'<a href="'. $match[2] .'" title="'. $match[2] .'" target="_blank">'. $caption .'</a>'. $match[5];
}

/**
 * Make links out of domain names starting with "www."
 */
function _filter_url_parse_partial_links($match) {
  $match[2] = decode_entities($match[2]);
  $caption = check_plain(_filter_url_trim($match[2]));
  $match[2] = check_plain($match[2]);
  return $match[1] .'<a href="http://'. $match[2] .'" title="'. $match[2] .'" target="_blank">'. $caption .'</a>'. $match[3];
}

/**
 * Shortens long URLs to http://www.example.com/long/url...
 */
function _filter_url_trim($text, $length = NULL) {
  static $_length;
  if ($length !== NULL) {
    $_length = $length;
  }

  // Use +3 for '...' string length.
  if (strlen($text) > $_length + 3) {
    $text = substr($text, 0, $_length) .'...';
  }

  return $text;
}

/**
 * Decode all HTML entities (including numerical ones) to regular UTF-8 bytes.
 * Double-escaped entities will only be decoded once ("&amp;lt;" becomes "&lt;", not "<").
 *
 * @param $text
 *   The text to decode entities in.
 * @param $exclude
 *   An array of characters which should not be decoded. For example,
 *   array('<', '&', '"'). This affects both named and numerical entities.
 */
function decode_entities($text, $exclude = array()) {
  static $table;
  // We store named entities in a table for quick processing.
  if (!isset($table)) {
    // Get all named HTML entities.
    $table = array_flip(get_html_translation_table(HTML_ENTITIES));
    // PHP gives us ISO-8859-1 data, we need UTF-8.
    $table = array_map('utf8_encode', $table);
    // Add apostrophe (XML)
    $table['&apos;'] = "'";
  }
  $newtable = array_diff($table, $exclude);

  // Use a regexp to select all entities in one pass, to avoid decoding double-escaped entities twice.
  return preg_replace('/&(#x?)?([A-Za-z0-9]+);/e', '_decode_entities("$1", "$2", "$0", $newtable, $exclude)', $text);
}

/**
 * Helper function for decode_entities
 */
function _decode_entities($prefix, $codepoint, $original, &$table, &$exclude) {
  // Named entity
  if (!$prefix) {
    if (isset($table[$original])) {
      return $table[$original];
    }
    else {
      return $original;
    }
  }
  // Hexadecimal numerical entity
  if ($prefix == '#x') {
    $codepoint = base_convert($codepoint, 16, 10);
  }
  // Decimal numerical entity (strip leading zeros to avoid PHP octal notation)
  else {
    $codepoint = preg_replace('/^0+/', '', $codepoint);
  }
  // Encode codepoint as UTF-8 bytes
  if ($codepoint < 0x80) {
    $str = chr($codepoint);
  }
  else if ($codepoint < 0x800) {
    $str = chr(0xC0 | ($codepoint >> 6))
         . chr(0x80 | ($codepoint & 0x3F));
  }
  else if ($codepoint < 0x10000) {
    $str = chr(0xE0 | ( $codepoint >> 12))
         . chr(0x80 | (($codepoint >> 6) & 0x3F))
         . chr(0x80 | ( $codepoint       & 0x3F));
  }
  else if ($codepoint < 0x200000) {
    $str = chr(0xF0 | ( $codepoint >> 18))
         . chr(0x80 | (($codepoint >> 12) & 0x3F))
         . chr(0x80 | (($codepoint >> 6)  & 0x3F))
         . chr(0x80 | ( $codepoint        & 0x3F));
  }
  // Check for excluded characters
  if (in_array($str, $exclude)) {
    return $original;
  }
  else {
    return $str;
  }
}

/**
 * Encode special characters in a plain-text string for display as HTML.
 *
 * Uses drupal_validate_utf8 to prevent cross site scripting attacks on
 * Internet Explorer 6.
 */
function check_plain($text) {
  return verse_validate_utf8($text) ? htmlspecialchars($text, ENT_QUOTES) : '';
}

/**
 * Checks whether a string is valid UTF-8.
 *
 * All functions designed to filter input should use drupal_validate_utf8
 * to ensure they operate on valid UTF-8 strings to prevent bypass of the
 * filter.
 *
 * When text containing an invalid UTF-8 lead byte (0xC0 - 0xFF) is presented
 * as UTF-8 to Internet Explorer 6, the program may misinterpret subsequent
 * bytes. When these subsequent bytes are HTML control characters such as
 * quotes or angle brackets, parts of the text that were deemed safe by filters
 * end up in locations that are potentially unsafe; An onerror attribute that
 * is outside of a tag, and thus deemed safe by a filter, can be interpreted
 * by the browser as if it were inside the tag.
 *
 * This function exploits preg_match behaviour (since PHP 4.3.5) when used
 * with the u modifier, as a fast way to find invalid UTF-8. When the matched
 * string contains an invalid byte sequence, it will fail silently.
 *
 * preg_match may not fail on 4 and 5 octet sequences, even though they
 * are not supported by the specification.
 *
 * The specific preg_match behaviour is present since PHP 4.3.5.
 *
 * @param $text
 *   The text to check.
 * @return
 *   TRUE if the text is valid UTF-8, FALSE if not.
 */
function verse_validate_utf8($text) {
  if (strlen($text) == 0) {
    return TRUE;
  }
  return (preg_match('/^./us', $text) == 1);
}

/**
 * Prepare a URL for use in an HTML attribute. Strips harmful protocols.
 */
function check_url($uri) {
  return filter_xss_bad_protocol($uri, FALSE);
}

/**
 * Processes an HTML attribute value and ensures it does not contain an URL
 * with a disallowed protocol (e.g. javascript:)
 *
 * @param $string
 *   The string with the attribute value.
 * @param $decode
 *   Whether to decode entities in the $string. Set to FALSE if the $string
 *   is in plain text, TRUE otherwise. Defaults to TRUE.
 * @return
 *   Cleaned up and HTML-escaped version of $string.
 */
function filter_xss_bad_protocol($string, $decode = TRUE) {
  static $allowed_protocols;
  if (!isset($allowed_protocols)) {
    $allowed_protocols = array_flip(array('javascript', 'http', 'https', 'ftp', 'news', 'nntp', 'telnet', 'mailto', 'irc', 'ssh', 'sftp', 'webcal', 'rtsp'));
  }

  // Get the plain text representation of the attribute value (i.e. its meaning).
  if ($decode) {
    $string = decode_entities($string);
  }

  // Iteratively remove any invalid protocol found.

  do {
    $before = $string;
    $colonpos = strpos($string, ':');
    if ($colonpos > 0) {
      // We found a colon, possibly a protocol. Verify.
      $protocol = substr($string, 0, $colonpos);
      // If a colon is preceded by a slash, question mark or hash, it cannot
      // possibly be part of the URL scheme. This must be a relative URL,
      // which inherits the (safe) protocol of the base document.
      if (preg_match('![/?#]!', $protocol)) {
        break;
      }
      // Per RFC2616, section 3.2.3 (URI Comparison) scheme comparison must be case-insensitive
      // Check if this is a disallowed protocol.
      if (!isset($allowed_protocols[strtolower($protocol)])) {
        $string = substr($string, $colonpos + 1);
      }
    }
  } while ($before != $string);
  return check_plain($string);
}

/**
 * Filters XSS. Based on kses by Ulf Harnhammar, see
 * http://sourceforge.net/projects/kses
 *
 * For examples of various XSS attacks, see:
 * http://ha.ckers.org/xss.html
 *
 * This code does four things:
 * - Removes characters and constructs that can trick browsers
 * - Makes sure all HTML entities are well-formed
 * - Makes sure all HTML tags and attributes are well-formed
 * - Makes sure no HTML tags contain URLs with a disallowed protocol (e.g. javascript:)
 *
 * @param $string
 *   The string with raw HTML in it. It will be stripped of everything that can cause
 *   an XSS attack.
 * @param $allowed_tags
 *   An array of allowed tags.
 */
function filter_xss($string, $allowed_tags = array('a', 'em', 'strong', 'cite', 'code', 'ul', 'ol', 'li', 'dl', 'dt', 'dd', 'br', 'p')) {
  // Only operate on valid UTF-8 strings. This is necessary to prevent cross
  // site scripting issues on Internet Explorer 6.
  if (!verse_validate_utf8($string)) {
    return '';
  }
  // Store the input format
  _filter_xss_split($allowed_tags, TRUE);
  // Remove NUL characters (ignored by some browsers)
  $string = str_replace(chr(0), '', $string);
  // Remove Netscape 4 JS entities
  $string = preg_replace('%&\s*\{[^}]*(\}\s*;?|$)%', '', $string);

  // Defuse all HTML entities
  $string = str_replace('&', '&amp;', $string);
  // Change back only well-formed entities in our whitelist
  // Named entities
  $string = preg_replace('/&amp;([A-Za-z][A-Za-z0-9]*;)/', '&\1', $string);
  // Decimal numeric entities
  $string = preg_replace('/&amp;#([0-9]+;)/', '&#\1', $string);
  // Hexadecimal numeric entities
  $string = preg_replace('/&amp;#[Xx]0*((?:[0-9A-Fa-f]{2})+;)/', '&#x\1', $string);

  return preg_replace_callback('%
    (
    <(?=[^a-zA-Z!/])  # a lone <
    |                 # or
    <[^>]*(>|$)       # a string that starts with a <, up until the > or the end of the string
    |                 # or
    >                 # just a >
    )%x', '_filter_xss_split', $string);
}

/**
 * Processes an HTML tag.
 *
 * @param @m
 *   An array with various meaning depending on the value of $store.
 *   If $store is TRUE then the array contains the allowed tags.
 *   If $store is FALSE then the array has one element, the HTML tag to process.
 * @param $store
 *   Whether to store $m.
 * @return
 *   If the element isn't allowed, an empty string. Otherwise, the cleaned up
 *   version of the HTML element.
 */
function _filter_xss_split($m, $store = FALSE) {
  static $allowed_html;

  if ($store) {
    $allowed_html = array_flip($m);
    return;
  }

  $string = $m[1];

  if (substr($string, 0, 1) != '<') {
    // We matched a lone ">" character
    return '&gt;';
  }
  else if (strlen($string) == 1) {
    // We matched a lone "<" character
    return '&lt;';
  }

  if (!preg_match('%^<\s*(/\s*)?([a-zA-Z0-9]+)([^>]*)>?$%', $string, $matches)) {
    // Seriously malformed
    return '';
  }

  $slash = trim($matches[1]);
  $elem = &$matches[2];
  $attrlist = &$matches[3];

  if (!isset($allowed_html[strtolower($elem)])) {
    // Disallowed HTML element
    return '';
  }

  if ($slash != '') {
    return "</$elem>";
  }

  // Is there a closing XHTML slash at the end of the attributes?
  // In PHP 5.1.0+ we could count the changes, currently we need a separate match
  $xhtml_slash = preg_match('%\s?/\s*$%', $attrlist) ? ' /' : '';
  $attrlist = preg_replace('%(\s?)/\s*$%', '\1', $attrlist);

  // Clean up attributes
  $attr2 = implode(' ', _filter_xss_attributes($attrlist));
  $attr2 = preg_replace('/[<>]/', '', $attr2);
  $attr2 = strlen($attr2) ? ' '. $attr2 : '';

  return "<$elem$attr2$xhtml_slash>";
}

/**
 * Processes a string of HTML attributes.
 *
 * @return
 *   Cleaned up version of the HTML attributes.
 */
function _filter_xss_attributes($attr) {
  $attrarr = array();
  $mode = 0;
  $attrname = '';

  while (strlen($attr) != 0) {
    // Was the last operation successful?
    $working = 0;

    switch ($mode) {
      case 0:
        // Attribute name, href for instance
        if (preg_match('/^([-a-zA-Z]+)/', $attr, $match)) {
          $attrname = strtolower($match[1]);
          $skip = ($attrname == 'style' || substr($attrname, 0, 2) == 'on');
          $working = $mode = 1;
          $attr = preg_replace('/^[-a-zA-Z]+/', '', $attr);
        }

        break;

      case 1:
        // Equals sign or valueless ("selected")
        if (preg_match('/^\s*=\s*/', $attr)) {
          $working = 1; $mode = 2;
          $attr = preg_replace('/^\s*=\s*/', '', $attr);
          break;
        }

        if (preg_match('/^\s+/', $attr)) {
          $working = 1; $mode = 0;
          if (!$skip) {
            $attrarr[] = $attrname;
          }
          $attr = preg_replace('/^\s+/', '', $attr);
        }

        break;

      case 2:
        // Attribute value, a URL after href= for instance
        if (preg_match('/^"([^"]*)"(\s+|$)/', $attr, $match)) {
          $thisval = filter_xss_bad_protocol($match[1]);

          if (!$skip) {
            $attrarr[] = "$attrname=\"$thisval\"";
          }
          $working = 1;
          $mode = 0;
          $attr = preg_replace('/^"[^"]*"(\s+|$)/', '', $attr);
          break;
        }

        if (preg_match("/^'([^']*)'(\s+|$)/", $attr, $match)) {
          $thisval = filter_xss_bad_protocol($match[1]);

          if (!$skip) {
            $attrarr[] = "$attrname='$thisval'";;
          }
          $working = 1; $mode = 0;
          $attr = preg_replace("/^'[^']*'(\s+|$)/", '', $attr);
          break;
        }

        if (preg_match("%^([^\s\"']+)(\s+|$)%", $attr, $match)) {
          $thisval = filter_xss_bad_protocol($match[1]);

          if (!$skip) {
            $attrarr[] = "$attrname=\"$thisval\"";
          }
          $working = 1; $mode = 0;
          $attr = preg_replace("%^[^\s\"']+(\s+|$)%", '', $attr);
        }

        break;
    }

    if ($working == 0) {
      // not well formed, remove and try again
      $attr = preg_replace('/
        ^
        (
        "[^"]*("|$)     # - a string that starts with a double quote, up until the next double quote or the end of the string
        |               # or
        \'[^\']*(\'|$)| # - a string that starts with a quote, up until the next quote or the end of the string
        |               # or
        \S              # - a non-whitespace character
        )*              # any number of the above three
        \s*             # any number of whitespaces
        /x', '', $attr);
      $mode = 0;
    }
  }

  // the attribute list ends with a valueless attribute like "selected"
  if ($mode == 1) {
    $attrarr[] = $attrname;
  }
  return $attrarr;
}

/**
 * Return a component of the current path.
*/

function arg($index = NULL, $path = NULL) {
  static $arguments;

  if (!isset($path)) {
    $path = $_GET['p'];
  }
  if (!isset($arguments[$path])) {
    $arguments[$path] = explode('/', $path);
  }
  if (!isset($index)) {
    return $arguments[$path];
  }
  if (isset($arguments[$path][$index])) {
    return $arguments[$path][$index];
  }
}

/**
 * Wrapper around urlencode() which avoids Apache quirks.
 *
 * Should be used when placing arbitrary data in an URL. Note that Drupal paths
 * are urlencoded() when passed through url() and do not require urlencoding()
 * of individual components.
 *
 * Notes:
 * - For esthetic reasons, we do not escape slashes. This also avoids a 'feature'
 *   in Apache where it 404s on any path containing '%2F'.
 * - mod_rewrite unescapes %-encoded ampersands, hashes, and slashes when clean
 *   URLs are used, which are interpreted as delimiters by PHP. These
 *   characters are double escaped so PHP will still see the encoded version.
 * - With clean URLs, Apache changes '//' to '/', so every second slash is
 *   double escaped.
 *
 * @param $text
 *   String to encode
 */
function verse_urlencode($text) {
    return str_replace(array('%2F', '%26', '%23', '//'),
                       array('/', '%2526', '%2523', '/%252F'),
                       rawurlencode($text));
}

/**
 * Parse an array into a valid urlencoded query string.
 *
 * @param $query
 *   The array to be processed e.g. $_GET.
 * @param $exclude
 *   The array filled with keys to be excluded. Use parent[child] to exclude
 *   nested items.
 * @param $parent
 *   Should not be passed, only used in recursive calls.
 * @return
 *   An urlencoded string which can be appended to/as the URL query string.
 */
function verse_query_string_encode($query, $exclude = array(), $parent = '') {
  $params = array();

  foreach ($query as $key => $value) {
    $key = verse_urlencode($key);
    if ($parent) {
      $key = $parent .'['. $key .']';
    }

    if (in_array($key, $exclude)) {
      continue;
    }

    if (is_array($value)) {
      $params[] = verse_query_string_encode($value, $exclude, $key);
    }
    else {
      $params[] = $key .'='. verse_urlencode($value);
    }
  }

  return implode('&', $params);
}

/**
 * Generate a URL from a path. Will also pass-through existing URLs.
 *
 * @param $path
 *   The Drupal path being linked to, such as "admin/content/node", or an
 *   existing URL like "http://drupal.org/".  The special path
 *   '<front>' may also be given and will generate the site's base URL.
 * @param $options
 *   An associative array of additional options, with the following keys:
 *   - 'query'
 *       A query string to append to the link, or an array of query key/value
 *       properties.
 *   - 'fragment'
 *       A fragment identifier (or named anchor) to append to the link.
 *       Do not include the '#' character.
 *   - 'absolute' (default FALSE)
 *       Whether to force the output to be an absolute link (beginning with
 *       http:). Useful for links that will be displayed outside the site, such
 *       as in an RSS feed.
 *   - 'alias' (default FALSE)
 *       Whether the given path is an alias already.
 *   - 'external'
 *       Whether the given path is an external URL.
 *   - 'language'
 *       An optional language object. Used to build the URL to link to and
 *       look up the proper alias for the link.
 *   - 'base_url'
 *       Only used internally, to modify the base URL when a language dependent
 *       URL requires so.
 *   - 'prefix'
 *       Only used internally, to modify the path when a language dependent URL
 *       requires so.
 * @return
 *   A string containing a URL to the given path.
 *
 * When creating links in modules, consider whether l() could be a better
 * alternative than url().
 */
function url($path = NULL, $options = array()) {
  // Merge in defaults.
  $options += array(
    'fragment' => '',
    'query' => '',
    'absolute' => FALSE,
    'alias' => FALSE,
    'prefix' => ''
  );
  if (!isset($options['external'])) {
    // Return an external link if $path contains an allowed absolute URL.
    // Only call the slow filter_xss_bad_protocol if $path contains a ':' before
    // any / ? or #.
    $colonpos = strpos($path, ':');
    $options['external'] = ($colonpos !== FALSE && !preg_match('![/?#]!', substr($path, 0, $colonpos)) && filter_xss_bad_protocol($path, FALSE) == check_plain($path));
  }

  if ($options['fragment']) {
    $options['fragment'] = '#'. $options['fragment'];
  }
  if (is_array($options['query'])) {
    $options['query'] = verse_query_string_encode($options['query']);
  }

  if ($options['external']) {
    // Split off the fragment.
    if (strpos($path, '#') !== FALSE) {
      list($path, $old_fragment) = explode('#', $path, 2);
      if (isset($old_fragment) && !$options['fragment']) {
        $options['fragment'] = '#'. $old_fragment;
      }
    }
    // Append the query.
    if ($options['query']) {
      $path .= (strpos($path, '?') !== FALSE ? '&' : '?') . $options['query'];
    }
    // Reassemble.
    return $path . $options['fragment'];
  }

//  global $base_url;
  static $script;

  if (!isset($script)) {
    // On some web servers, such as IIS, we can't omit "index.php". So, we
    // generate "index.php?q=foo" instead of "?q=foo" on anything that is not
    // Apache.
    $script = (strpos($_SERVER['SERVER_SOFTWARE'], 'Apache') === FALSE) ? 'index.php' : '';
  }

/*  if (!isset($options['base_url'])) {
    // The base_url might be rewritten from the language rewrite in domain mode.
    $options['base_url'] = $base_url;
  }
*/
  // Preserve the original path before aliasing.
  $options['base_url'] = "http://$domain_name";
  $original_path = $path;

  // The special path '<front>' links to the default front page.
/*  if ($path == '<front>') {
    $path = '';
  }
  elseif (!empty($path) && !$options['alias']) {
    $path = drupal_get_path_alias($path, isset($options['language']) ? $options['language']->language : '');
  }

  if (function_exists('custom_url_rewrite_outbound')) {
    // Modules may alter outbound links by reference.
    custom_url_rewrite_outbound($path, $options, $original_path);
  }
*/
  $base = $options['absolute'] ? $options['base_url'] .'/' : "/";
  $prefix = empty($path) ? rtrim($options['prefix'], '/') : $options['prefix'];
  $path = verse_urlencode($prefix . $path);

  // With Clean URLs.
  if ($options['query']) {
    return $base . $path .'?'. $options['query'] . $options['fragment'];
  }
  else {
    return $base . $path . $options['fragment'];
  }
}

/**
 * Format an attribute string to insert in a tag.
 *
 * @param $attributes
 *   An associative array of HTML attributes.
 * @return
 *   An HTML string ready for insertion in a tag.
 */
function verse_attributes($attributes = array()) {
  if (is_array($attributes)) {
    $t = '';
    foreach ($attributes as $key => $value) {
      $t .= " $key=".'"'. check_plain($value) .'"';
    }
    return $t;
  }
}

/**
 * Format an internal link.
 *
 * This function correctly handles aliased paths, and allows themes to highlight
 * links to the current page correctly, so all internal links output by modules
 * should be generated by this function if possible.
 *
 * @param $text
 *   The text to be enclosed with the anchor tag.
 * @param $path
 *   The Drupal path being linked to, such as "admin/content/node". Can be an
 *   external or internal URL.
 *     - If you provide the full URL, it will be considered an external URL.
 *     - If you provide only the path (e.g. "admin/content/node"), it is
 *       considered an internal link. In this case, it must be a system URL
 *       as the url() function will generate the alias.
 *     - If you provide '<front>', it generates a link to the site's
 *       base URL (again via the url() function).
 *     - If you provide a path, and 'alias' is set to TRUE (see below), it is
 *       used as is.
 * @param $options
 *   An associative array of additional options, with the following keys:
 *     - 'attributes'
 *       An associative array of HTML attributes to apply to the anchor tag.
 *     - 'query'
 *       A query string to append to the link, or an array of query key/value
 *       properties.
 *     - 'fragment'
 *       A fragment identifier (named anchor) to append to the link.
 *       Do not include the '#' character.
 *     - 'absolute' (default FALSE)
 *       Whether to force the output to be an absolute link (beginning with
 *       http:). Useful for links that will be displayed outside the site, such
 *       as in an RSS feed.
 *     - 'html' (default FALSE)
 *       Whether the title is HTML, or just plain-text. For example for making
 *       an image a link, this must be set to TRUE, or else you will see the
 *       escaped HTML.
 *     - 'alias' (default FALSE)
 *       Whether the given path is an alias already.
 * @return
 *   an HTML string containing a link to the given path.
 */
function l($text, $path, $options = array()) {
  global $language;

  // Merge in defaults.
  $options += array(
      'attributes' => array(),
      'html' => FALSE,
    );

  // Append active class.
/*  if (($path == $_GET['q'] || ($path == '<front>' && drupal_is_front_page())) &&
      (empty($options['language']) || $options['language']->language == $language->language)) {
    if (isset($options['attributes']['class'])) {
      $options['attributes']['class'] .= ' active';
    }
    else {
      $options['attributes']['class'] = 'active';
    }
  }
*/
  // Remove all HTML and PHP tags from a tooltip. For best performance, we act only
  // if a quick strpos() pre-check gave a suspicion (because strip_tags() is expensive).
  if (isset($options['attributes']['title']) && strpos($options['attributes']['title'], '<') !== FALSE) {
    $options['attributes']['title'] = strip_tags($options['attributes']['title']);
  }

  return '<a href="'. check_url(url($path, $options)) .'"'. verse_attributes($options['attributes']) .'>'. ($options['html'] ? $text : check_plain($text)) .'</a>';
}

function leading_slash($path) {
	$path = trim($path);
	if($path[0]!="/") $path = "/".$path;
	return $path;
}

function get_obituary_slug(array $obituary) {
    if(!empty($obituary['slug'])) {
        $slug = $obituary['slug'];
    } else {
        $slug = slugify(date("Y-m", strtotime(@$obituary['death_date'])).' '.@$obituary["first_name"].' '.@$obituary["middle_name"].' '.@$obituary["last_name"]);
    }

    return $slug;
}

function slugify($text) {
  // replace non letter or digits by -
  $text = preg_replace('~[^\\pL\d]+~u', '-', $text);

  // trim
  $text = trim($text, '-');

  // transliterate
  $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);

  // lowercase
  $text = strtolower($text);

  // remove unwanted characters
  $text = preg_replace('~[^-\w]+~', '', $text);

  if (empty($text))
  {
    return 'n-a';
  }

  return $text;
}

function log_usage($module, $domain, $user) {
  $ulogger = &Log::factory('file', "logs/{$module}_usage.log", 'usage');
  $ulogger->info($domain.' '.$user);
}

function get_obituary_view_page_name() {
	global $domain_id, $db;
	$query = "SELECT internal_name FROM cms_page WHERE page_type=4 AND domain_id=".$domain_id;
	$page_name = $db->getOne($query);
	if(DB::isError($page_name) || !$page_name) {
		$page_name = 'obituary_view';
	}
	return $page_name;
}

function get_obituary_music_file($domain_id, $obituary_id) {
  global $db;

  $music_file = '';
  $query = "SELECT obituary_id, value FROM plg_obituary_config WHERE param='obituary_music' AND domain_id='$domain_id'";
  $ret = $db->getAssoc($query);
  if(count($ret)) {
    if(isset($ret[$obituary_id])) {
      $music_file = $ret[$obituary_id];
    }
    else {
      $music_file = $ret[0];
    }
  }
  return $music_file;
}

function is_block_ip($ip) {
    global $domain_id, $db;
    $blocked = false;

    $query = "SELECT id FROM plg_block_ip WHERE domain_id = $domain_id AND ip = '$ip'";
    $block_ip_id = $db->getOne($query);

    if(!is_null($block_ip_id)) {
        $blocked = true;
    }

    return $blocked;
}

function update_block_ip_count($ip) {
    global $domain_id, $db;

    $query = "SELECT id, count FROM plg_block_ip WHERE domain_id = $domain_id AND ip = '$ip'";
    $block_ip_info = $db->getRow($query);
    if($block_ip_info) {
        $count = intval($block_ip_info[1]) + 1;
        $db->query("UPDATE plg_block_ip SET count = $count WHERE id = ".intval($block_ip_info[0]));
    }

    return true;
}