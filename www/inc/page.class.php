<?php
/**
 * Page
 */

class Page {
  var $db,
      $domain_id,
      $data = NULL;

  function page(&$db, &$smarty, $domain_id) {
    $this->db = $db;
    $this->smarty = $smarty;
    $this->domain_id = $domain_id;
  }

  function draw() {

  }

  function validate() {

  }

  function submit() {

  }

}
