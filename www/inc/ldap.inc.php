<?php
  require_once("Net/LDAP2.php");

  function verse_ldap_connect() {
    global $ldap_config;
		
		$myCacheConfig = array(
            'path'    => '/tmp/ldap.cache',
            'max_age' => 1200 
    );
    $myCacheObject = new Net_LDAP2_SimpleFileSchemaCache($myCacheConfig);

    // Connect using the configuration:
    $ldap = Net_LDAP2::connect($ldap_config);
		
		return $ldap;
	}

  function verse_ldap_entry_exists($ldap, $dsn) {
    $entry = $ldap->getEntry($dsn);
    return !PEAR::isError($entry);
  }
