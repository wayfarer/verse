<?php

function send_obituary_notification($obituary_id, $slug, $name, $birthdate, $deathdate) {
    global $db, $domain_id, $domain_name;

    $emailfrom = "noreply@twintierstech.net";
    $text_tpl = "An obituary for %s (%s - %s) has been published. To view this obituary click on the following link: http://$domain_name/" . get_obituary_view_page_name() . "/%s/%d\n\n----------\nIf you wish to unsubscribe from future email notifications for new obituaries, please click the following link: http://{$domain_name}/subscribe.php?action=unobits&id=%s";

    if(empty($slug)) {
        $slug = slugify(date("Y-m", strtotime($deathdate)).' '.$name);
    }

    if ($birthdate) {
        $birthdate = date("F j, Y", strtotime($birthdate));
    }
    else {
        $birthdate = "N/A";
    }
    if ($deathdate) {
        $deathdate = date("F j, Y", strtotime($deathdate));
        $deathdate = "N/A";
    }

    // fetch recipients
    $query = "SELECT id, email, hash FROM plg_subscribe WHERE enabled=1 and domain_id='$domain_id'";
    $subscribers = $db->getAll($query, DB_FETCHMODE_ASSOC);
    foreach ($subscribers as $subscriber) {
        if (send_to_subscriber($obituary_id, $subscriber["id"])) {
            $emailto = $subscriber["email"];
            $subj = "New obituary published";
            $text = sprintf($text_tpl, $name, $birthdate, $deathdate, $slug, $obituary_id, $subscriber["hash"]);
            mail($emailto, $subj, $text, "From: $domain_name subscription service <$emailfrom>");
        }
    }
}

function send_to_subscriber($obituary_id, $subscribe_id) {
    global $db;

    $keywords = $db->getCol("SELECT keyword FROM plg_keyword k JOIN plg_subscribe_to_keyword sk ON k.id = sk.keyword_id WHERE sk.subscribe_id = $subscribe_id");
    if (!count($keywords)) {
        return true;
    } else {
        $obituary = $db->getRow("SELECT home_place, birth_place, service_place, visitation_place, final_disposition, obit_text FROM plg_obituary WHERE obituary_id = $obituary_id", DB_FETCHMODE_ASSOC);
        foreach ($keywords as $keyword) {
            if (stripos($obituary['home_place'], $keyword) !== false || stripos($obituary['birth_place'], $keyword) !== false || stripos($obituary['birth_place'], $keyword) !== false ||
                stripos($obituary['service_place'], $keyword) !== false || stripos($obituary['visitation_place'], $keyword) !== false || stripos($obituary['final_disposition'], $keyword) !== false ||
                stripos($obituary['obit_text'], $keyword) !== false) {
                return true;
            }
        }
        return false;
    }
}

function get_obituary_name($obituary) {
    $name = $obituary["first_name"];
    if($obituary["middle_name"]) {
        $name.=" ".$obituary["middle_name"];
    }
    $name.=" ".$obituary["last_name"];
    if($obituary["title"]) {
        $name = $obituary["title"]." ".$name;
    }
    if($obituary["suffix"]) {
        $name = $name.", ".$obituary["suffix"];
    }
    return $name;
}

function get_obituary_facebook_config($domain_id) {
    global $db;

    $facebook_config = array();
    $facebook_config_fields = $db->getAll("SELECT param, value FROM plg_obituary_config WHERE domain_id=$domain_id AND (param='facebook_user_access_token' OR param='facebook_page')", DB_FETCHMODE_ASSOC);

    foreach($facebook_config_fields as $facebook_config_field) {
        $facebook_config[$facebook_config_field['param']] = $facebook_config_field['value'];
    }

    return $facebook_config;
}