<?
/**
 * User management class.
 */

class user
{
  var $db,
      $db_table, $mode,
      $logged_id = NULL,
      $data = NULL,
      $is_shared_members = 0,
      $domain_ids = array();

  function user(&$db, $db_table, $db_add = NULL)
  {
    global $domain_id;

    $this->db = $db;
    $this->db_table = $db_table;
    $this->mode = 0;

     // check if this domain in domain link group and members is shared
    $group_ids = $this->db->getCol("SELECT lg.id FROM sms_domain_link_group lg JOIN sms_domain_link l on l.group_id=lg.id WHERE lg.is_shared_members='1' AND l.domain_id='$domain_id'");
    if(count($group_ids)) {
        $this->is_shared_members = 1;
    }
    
    if ($this->is_shared_members) {
        $this->domain_ids = $this->db->getCol("SELECT DISTINCT domain_id FROM sms_domain_link WHERE group_id IN (".implode(', ', $group_ids).")");
    }

    if(isset($_SESSION["{$this->db_table}uid"])) {
       $this->logged_id = $_SESSION["{$this->db_table}uid"];
       $this->get_data();
    }
  }

  function login($login, $password, $is_plainpass = false)
  {
    global $domain_id;

    $_SESSION["{$this->db_table}uid"] = NULL;
    $query = "SELECT login FROM $this->db_table WHERE login='".in($login)."' AND enabled=1";
    if(!$is_plainpass) {
        $query .= " AND password=old_password('".in($password)."')";
    }
    else {
        $query .= " AND password='".in($password)."'";
    }

    // check if this domain in domain link group and members is shared
    if ($this->is_shared_members) {
        $and_where = " AND (domain_id IN (".implode(", ", $this->domain_ids).") OR domain_id=0)";
    } else {
        $and_where = " AND (domain_id='$domain_id' OR domain_id=0)";
    }
    $query .= $and_where;

    $result = $this->db->getOne($query);
    if(!DB::isError($result) && !is_null($result)) {
        $_SESSION["{$this->db_table}uid"] = $this->logged_id = $result;
        // load data to session
//	      $query = "SELECT user_id, domain_id, name FROM $this->db_table WHERE login='".$_SESSION["{$this->db_table}uid"]."' AND password=password('".in($password)."')";

        $query = "SELECT * FROM {$this->db_table} WHERE login='".$_SESSION["{$this->db_table}uid"]."'";
        if(!$is_plainpass) {
  	        $query .= " AND password=old_password('".in($password)."')";
        }
        else {
//  	      $query = "SELECT user_id, domain_id, name, email FROM {$this->db_table} WHERE login='".$_SESSION["{$this->db_table}uid"]."' AND password='".in($password)."'";
  	        $query .= " AND password='".in($password)."'";
        }
        $query .= $and_where;

    	$result = $this->db->getRow($query, DB_FETCHMODE_ASSOC);
    	$_SESSION["{$this->db_table}udata"] = $result;
    	$this->get_data(true);
        $this->db->query("UPDATE {$this->db_table} SET last_login = '".date("Y-m-d G:i:s")."' WHERE user_id='".$this->data['user_id']."'");
        return TRUE;
    }
    return FALSE;
  }

  function logout()
  {
        $_SESSION["{$this->db_table}uid"] = NULL;
        $this->logged_id = null;
        $this->mode = 0;
  }

  function logged()
  {
        return (is_null($this->logged_id)?FALSE:TRUE);
  }


  function update_login($newlogin) {
    $_SESSION["{$this->db_table}uid"] = $newlogin;
  }

  function get_data($force=false)
  {
    global $domain_id;

    if($force) {
      if ($this->is_shared_members) {
          $query = "SELECT * FROM $this->db_table WHERE login='".$_SESSION["{$this->db_table}uid"]."' AND (domain_id IN (".implode(", ", $this->domain_ids).") OR domain_id=0)";
      } else {
          $query = "SELECT * FROM $this->db_table WHERE login='".$_SESSION["{$this->db_table}uid"]."' AND (domain_id='$domain_id' OR domain_id=0)";
      }
      $result = $this->db->getRow($query, DB_FETCHMODE_ASSOC);
      $_SESSION["{$this->db_table}udata"] = $result;

      $query = "SELECT role_id FROM cms_user_role WHERE user_id=".$result["user_id"];
      $roles = $this->db->getCol($query);
      $_SESSION["{$this->db_table}udata"]["roles"] = $roles;
    }

/*        if (DB::isError($result)) {
                $err = new PEAR_Error($result->getMessage()."<br>".$result->getUserInfo(),0,PEAR_ERROR_DIE);
        } */
    $this->data = $_SESSION["{$this->db_table}udata"];

  }

  function set_data($param, $value) {
  	$_SESSION["{$this->db_table}udata"][$param] = $this->data[$param] = $value;
  }

  function user_exists($name)
  {
        $query = "SELECT login FROM $this->db_table WHERE login='".in($name)."'";
        $result = $this->db->getOne($query);
        return !is_null($result);
  }

  function have_role($role_id)
  {
//  	var_dump($this->data);
//  	exit;
	if($this->logged()) {
		if($this->data["domain_id"]!=0) {
			return in_array($role_id, $this->data["roles"]);
		}
		else {
			// all roles for global admin
			return true;
		}
	}
	else return false;
  }

  function is_super() {
    return ($this->logged() && $this->data["domain_id"]==0);
  }

  function is_shared() {
      if(count($this->domain_ids) && in_array($this->data["domain_id"], $this->domain_ids)) {
          return true;
      } else {
          return false;
      }
  }

  function get_domain_ids() {
      return $this->domain_ids;
  }
}
