<?php
/**
 * Domain management class.
 */

class domain
{
  var $db,
      $domain_id,
      $data = NULL;

  function domain(&$db, $domain_id, &$user) {
    $this->db = $db;
    $this->user = $user;
    $this->domain_id = $domain_id;
    $this->get_data();
  }

  function get_data() {
    $query = "SELECT domain_name, postfix, email_enabled, ecommerce_enabled, price_enabled, paypal_checkout, affiliates_enabled, mode FROM sms_domain WHERE domain_id='$this->domain_id'";
    $this->data = $this->db->getRow($query, DB_FETCHMODE_ASSOC);
  }

  function get($feature) {
    if(isset($this->data[$feature])) {
      return $this->data[$feature];
    }
    else return null;
  }

  function have_feature($feature) {
    return ($this->user->is_super()) || (isset($this->data[$feature]) && $this->data[$feature]);
  }

  public function getParentProductDomainId() {
      $parent_domain_id = $this->db->getOne("SELECT domain_id FROM sms_domain_link dl JOIN sms_domain_link_group lg ON dl.group_id=lg.id WHERE is_shared_products='1' AND
                                            lg.id=(SELECT g.id FROM sms_domain_link_group g JOIN sms_domain_link l ON g.id=l.group_id WHERE domain_id='$this->domain_id' AND is_shared_products='1')
                                            ORDER BY dl.id LIMIT 1");
      return !is_null($parent_domain_id) ? $parent_domain_id : $this->domain_id;
  }
}
