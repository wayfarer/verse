<?php
	function add_mailbox($name, $passwd)
	{
		global $domain_id, $db;
		// fetch domain_name
		$query = "SELECT domain_name, postfix FROM sms_domain WHERE domain_id='$domain_id'";
		$domain = $db->getRow($query, DB_FETCHMODE_ASSOC);
		$username = $name."-".$domain["postfix"];
		$email = $name."@".$domain["domain_name"];

		// 1st - add username/pass to sasldb for authorization
		$db_con = createobject("db_sasldb");
		$dbs = $db_con->connect();
		$query = "INSERT accounts SET username='$username', password='$passwd'";
		$dbs->query($query);
		$dbs->disconnect();
		
		// 2nd - create IMAP mail box (cyrus)
		$h = fsockopen("localhost", 143);
		fwrite($h, "VCMS LOGIN cyrus source\n");
		fwrite($h, "VCMS CREATE user.$username\n");
		fwrite($h, "VCMS SETACL user.$username cyrus lrswipcda\n");
		fclose($h);

		return $mailbox_id; 
	}

	function update_mailbox_password($login, $newpasswd) 
	{
		$db_con = createobject("db_sasldb");
		$dbs = $db_con->connect();
		$query = "UPDATE accounts SET password='$newpasswd' WHERE username='$login'";
		$dbs->query($query);
		$dbs->disconnect();
	}

	function remove_mailbox($mailbox_id)
	{
		global $db;

		// fetch username and delete user
		$query = "SELECT login FROM plg_mailbox WHERE mailbox_id='$mailbox_id'";
		$username = $db->getOne($query);
		if(!$username) return false;

		$db_con = createobject("db_sasldb");
		$dbs = $db_con->connect();
		$query = "DELETE FROM accounts WHERE username = '$username'";
		$dbs->query($query);
		$dbs->disconnect();

		// delete mailbox
		$query = "DELETE FROM plg_mailbox WHERE mailbox_id='$mailbox_id'";
		$db->query($query);

		// delete IMAP mail box (cyrus)
		$h = fsockopen("localhost", 143);
		fwrite($h, "VCMS LOGIN cyrus source\n");
		fwrite($h, "VCMS DELETE user.$username\n");
		fclose($h);
		return true;
	}	

	function generate_sendmail_files()
	{
		global $db, $logger;

		$query = "SELECT d.domain_id, name, type, login, forward FROM sms_domain d LEFT JOIN plg_mailbox m USING(domain_id) WHERE enabled=1 AND email_enabled=1";
		$boxes = $db->getAssoc($query, false, array(), DB_FETCHMODE_ASSOC, true);

		if(!$boxes || DB::isError($boxes)) {
			$logger->emerg("Generate Sendmail Files DB error");
			if(DB::isError($boxes)) {
				$logger->emerg($boxes->getMessage()." ".$boxes->getUserInfo());
			}
			return;
		}

		// remove empty records for domains with 0 emails
		foreach($boxes as $domain_id=>$box) {
			if(is_null($box[0]["name"])) {
				unset($boxes[$domain_id][0]);
			}
		}

		// fetch domain names
		$query = "SELECT domain_id, domain_name FROM sms_domain";
		$domains = $db->getAssoc($query);		
		if(!$domains || DB::isError($domains)) {
			$logger->emerg("Generate Sendmail Files DB error2");
			if(DB::isError($domains)) {
				$logger->emerg($domains->getMessage()." ".$domains->getUserInfo());
			}
			return;
		}

		$h = fopen("../sendmail_config/access", "w");
 		$h2 = fopen("../sendmail_config/genericstable", "w");
 		$h3 = fopen("../sendmail_config/virtusertable", "w");
 		$h4 = fopen("../sendmail_config/local-host-names", "w");
		// keep system aliases
		copy("../aliases_sys", "../sendmail_config/aliases");
		// open file for appending aliases
		$h5 = fopen("../sendmail_config/aliases", "a");

//$h = fopen("../access", "w");
//$h2 = fopen("../genericstable", "w");
//$h3 = fopen("../virtusertable", "w");
//$h4 = fopen("../local-host-names", "w");
// keep system aliases
//copy("../aliases_sys", "../aliases2");
// open file for appending aliases
//$h5 = fopen("../aliases2", "a");

		// /etc/mail/access file
		fwrite($h, "Connect:localhost.localdomain\tRELAY\nConnect:localhost\tRELAY\nConnect:127.0.0.1\tRELAY\nConnect:66.135.56.26\tRELAY\n\n");	
	 	// local-host-names
		fwrite($h4, "squirrelwebmail\n");

		fwrite($h5, "\n# Verse SMS aliases to implement forwarding to multiple emails\n");

 		foreach($boxes as $domain_id=>$mailboxes) {
			$domain_name = $domains[$domain_id];
			fwrite($h, "# domain $domain_name, ".count($mailboxes)." emails\n");
			foreach($mailboxes as $mailbox) {
				$email = $mailbox["name"]."@$domain_name";
				// /etc/mail/access file
				fwrite($h, "$email\t RELAY\n");			
		
		 		if($mailbox["type"]==0) { // mailbox
			 		// etc/mail/genericstable
			 		fwrite($h2, $mailbox["login"]."\t$email\n");

	 				// etc/mail/virtusertable
			 		fwrite($h3, "$email\t".$mailbox["login"]."\n");
			 	}
			 	else { // forward
			 		// process forward
			 		$forwards = explode(",", $mailbox["forward"]);
			 		// replace local address with aliases if so
			 		foreach($forwards as $i=>$forward) {
			 			$domain = substr($forward, strpos($forward,"@")+1);
			 			$query = "SELECT domain_id FROM sms_domain WHERE domain_name='$domain' AND email_enabled=1";
			 			$domain_id = $db->getOne($query);
			 			if($domain_id) {
			 				$name = substr($forward, 0, strpos($forward,"@"));
			 				$query = "SELECT login FROM plg_mailbox WHERE name='$name' AND domain_id='$domain_id'";
			 				$login = $db->getOne($query);
			 				if($login) {
			 					// replace email address with local alias
			 					$forwards[$i] = $login;
			 				}
			 			}
			 		}
			 		if(count($forwards)==1) {
		 				// etc/mail/virtusertable
				 		fwrite($h3, "$email\t".$forwards[0]."\n");									 			
			 		}
			 		else {
			 			// generate unique id
			 			$uid = $mailbox["name"]."0".$domain_name;
		 				// etc/mail/virtusertable
				 		fwrite($h3, "$email\t".$uid."\n");
				 		// etc/mail/aliases
				 		fwrite($h5, "$uid:\t".implode(", ",$forwards)."\n");
			 		}
			 	}
			}

			// /etc/mail/access file
	 		// reject emails to unknown (not listed) addresses
	 		fwrite($h, "@".$domain_name."\t REJECT\n\n");
		 	
		 	// etc/mail/genericstable
		 	// fine formatting
		 	if(count($mailboxes)) fwrite($h2, "\n");
		 	
 			// etc/mail/virtusertable
		 	if($domain_name!="twintierstech.net") {
			 	fwrite($h3, "@$domain_name\terror:nouser 550 User unknown\n");
		 	}
		 	else {
			 	fwrite($h3, "@$domain_name\troot\n");
		 	}
		 	fwrite($h3, "\n");
	
	 		// local-host-names
			fwrite($h4, "$domain_name\n");
 		}

    // write additions
    $add = file_get_contents("../sendmail_config_remote/access");
    if($add) {
      fwrite($h, "# data from remote server\n");
      fwrite($h, $add);
    }
    $add = file_get_contents("../sendmail_config_remote/genericstable");
    if($add) {
      fwrite($h2, $add);
    }
    $add = file_get_contents("../sendmail_config_remote/virtusertable");
    if($add) {
      fwrite($h3, $add);
    }
    $add = file_get_contents("../sendmail_config_remote/local-host-names");
    if($add) {
      fwrite($h4, $add);
    }
    $add = file_get_contents("../sendmail_config_remote/aliases");
    if($add) {
      fwrite($h5, "# data from remote server\n");
      fwrite($h5, $add);
    }

 		fclose($h);  // access
 		fclose($h2); // genericstable
 		fclose($h3); // virtusertable
 		fclose($h4); // local-host-names
 		fclose($h5); // aliases
 		$logger->info("Mailbox config generated, ".count($boxes)." domains");
	}
