<?php
  /** 
   * Class report - implements reports with pagenator and filters.
   * Class requires for session to be started if using pagenator, field sorting or html controls
   * @author Vladimir Droznik <vladimir@mail.by>
   * requires PHP 4.1.0 or above, because of using $_SESSION, $_POST etc.
   **/
  
  // constanst for using with class
  define("REP_INNER_JOIN", 1);
  define("REP_LEFT_JOIN", 2);

  define("REP_STRING_TEMPLATE", 0);
  define("REP_STRING", 0);
  define("REP_FUNCTIONCALL_TEMPLATE", 1);
  define("REP_CALLBACK", 1);
  define("REP_ACTION", 2);
  
  define("REP_CONTROL_FIELD", 8); // used to determine if field is control in this way:  if($field_type > REP_CONTROL_FIELD) field is control
  define("REP_CHECKBOX_FIELD", 9);
  define("REP_RADIO_FIELD", 10);
  
  define("REP_INVISIBLE_FILTER", 0);
  define("REP_TEXT_FILTER", 1);
  define("REP_CHECKBOX_FILTER", 2);
  define("REP_SELECT_FILTER", 3);
  define("REP_PERIOD_FILTER", 4);
  define("REP_CUSTOM_FILTER", 5);

  define("REP_SUBMIT_BUTTON", 0);
  
  define("REP_CUSTOM_ROW_TOP", 0);
  define("REP_CUSTOM_ROW_BOTTOM", 1000000);

  define("REP_UNORDERABLE", 0);
  define("REP_ORDERABLE", 1);
        
  // helper functions
  function href($value, $href, $params = NULL, $confirm_text = NULL)
  {
	  $html = "<a href=\"$href\"";
	  if($params) $html.=" ".$params;
	  if($confirm_text) $html.=" onclick=\"return confirm('$confirm_text');\"";
	  $html.=">$value</a>";
	  return $html;
  }

  function href_js_action($title, $action)
  {
	  $html = "<a href=\"#\" onclick=\"$action; return false;\">$title</a>";
	  return $html;
  }

  class report
  {
    var $db;         // db handles for internal use           
    var $uid;              // unique id for naming vars that stored in session 
    var $ruid;             // report unique id, used when more than 1 report on page
    var $html_attributes;
    var $rpp;              // records per page
    var $fields;           // report fields
    var $controls;  
    var $filters;  
    var $headers;
    var $custom_rows;
    var $db_tables;        // report database tables with join rules
    var $pagenator;        // pagenator usage flag
    var $heading;          // if report table has header
    
    var $grouping_field;
    var $group_fields;
    var $groupby_clause;
    var $custom_filter;
        
    var $order_field;      // field to order by
    var $order_dest;       // sorting direction
    var $start;            // record to start report from
    
    var $cfn;              // control fields number
    var $cft;              // control fields types

    /**
     * constructor
     * 
     * @param string $db_table - database table to use in report
     * @param integer $pagenator - is report uses pages flag
     * @param integer $rpp - records per page if pages are used
     * @return 
     **/
    function report(&$db, $db_table, $pagenator = 1, $heading = 1, $rpp = 100, $pagenator_style="", $ruid="sr")
    {
      $this->db = &$db; 
      $this->db_tables = array(array("name" => $db_table, 
                                     "join_type" => NULL,
                                     "join_column" => NULL, 
                                     "join_with" => NULL));
      $this->html_attributes = "";
      $this->rpp = $rpp;
      $this->fields = array();
      $this->controls = array();
      $this->filters = array();
      $this->headers = array();
      $this->custom_rows = array();
      $this->pagenator = $pagenator;
      $this->heading = $heading;
      $this->pagenator_style=$pagenator_style;
      
      $this->grouping_field = NULL;
      $this->group_fields = NULL;
      
      $this->uid = "rep".$db_table;
      $this->ruid = $ruid;
      $this->cfn = 0;  
      $this->cft = array();
//      $this->uv = time() - 5184000;
      $this->groupby_clause = "";
      $this->custom_filter = "";

      //if user calls us 1st time - initialize
      //else restore previous values from session
      if(!isset($_SESSION[$this->uid.$this->ruid."order_field"]))
      {
        $this->order_field = $_SESSION[$this->uid.$this->ruid."order_field"] = "";
        $this->order_dest = $_SESSION[$this->uid.$this->ruid."order_dest"] = 1;
        $this->start = $_SESSION[$this->uid.$this->ruid."start"] = 1;
      }
      else
      {
        $this->order_field = $_SESSION[$this->uid.$this->ruid."order_field"];
        $this->order_dest = $_SESSION[$this->uid.$this->ruid."order_dest"];
        $this->start = $_SESSION[$this->uid.$this->ruid."start"];
      }
    } 

    /**
    * adds attributes to use in main table
    **/
    function html_attributes($attributes)
    {
    	$this->html_attributes = " ".$attributes;
    }

    /**
     * adds database table for report with join rules
     * 
     * @access public
     * 
     * @param string $db_table - table name in database
     * @param integer $join_type how to join with second table
     *                                                       - REP_INNER_JOIN - inner join
     *                                                       - REP_LEFT_JOIN - left join
     * @param $join_column - (string or array[2] of strings) - name of join column or two names: this table and second table
     * @param integer $join_with - table id to join with (id = index in array db_tables), by default join with last added table
     * @return integer table_id
     **/
    function add_table($db_table, $join_type, $join_column, $join_with = 0)
    {
      //if(!is_array($join_column[0])) $join_column = array($join_column);
      $this->db_tables[] = array("name" => $db_table, 
                                 "join_type"  => $join_type, 
                                 "join_column" => $join_column, 
                                 "join_with" => $join_with);
      return sizeof($this->db_tables);
    }

    /**
     * adds field to report
     * 
     * @access public
     * 
     * @param string $field_caption - displayed field caption
     * @param integer $field_type - REP_STRING_TEMPLATE - string template like "something {somefield|sometableid}, and {someotherfield}"
     *                            - REP_FUNCTIONCALL_TEMPLATE - function call template like "userfunc({somefield|sometable}, {somefield2}, ...)"
     *                            - REP_CHECKBOX_FIELD - field is user defined control - checkbox
     *                            - REP_RADIO_FIELD - field is user defined control - checkbox
     * @param string $field_template - template for field;
     *                                                                 template example: "the value of {substance} is {sub_name} and id={id|1}";
     * @param integer $order_by - number of field in template (starting with 1) to order by when user clicked on field header,  (default: 0 - do not make header clickable)
     * @param string $html_attributes - html attributes will be used for all <td> tags for this column
     * @return 
     **/
    function add_field($field_caption, $field_type, $field_template, $order_by = 0, $html_attributes = NULL)
    {
      // extract patterns from template. patterns are like; {id}, {text|2}
      preg_match_all("/{[^}]+}/", $field_template, $patterns);

      // preprocess patterns
      $patterns = $patterns[0];
      $keys = array();
      foreach($patterns as $i=>$pattern) {
      $matches = array();
      $offset = 0;
      if(preg_match("/#(.*?)#/", $pattern, $matches, PREG_OFFSET_CAPTURE)) {
			$pattern = substr_replace($pattern, $matches[1][0], $matches[1][1]-1, strlen($matches[1][0])+2);
			$pattern = str_replace("}", " ".$matches[1][0]."}", $pattern);
			// change pattern in field_template 
			$field_template = str_replace($patterns[$i], $pattern, $field_template);
			$patterns[$i] = $pattern;
			$keys[$i] = $matches[1][0];
		}
      }

      $this->fields[] = array("caption" => $field_caption,
                              "type"  => $field_type,
                              "value" => $field_template,
                              "patterns" => $patterns,
                              "keys" => $keys,
                              "order_by" => $order_by,
                              "html_attributes" => $html_attributes);

      if($field_type > REP_CONTROL_FIELD) {
        if(!array_key_exists("udcv$this->cfn", $_SESSION))
        {
          switch($field_type)
          {
            case REP_CHECKBOX_FIELD:
              $_SESSION["udcv$this->cfn"] = array();
              $_SESSION["udcv_sent$this->cfn"] = array();
              break;
            default:
              $_SESSION["udcv$this->cfn"] = NULL;
              break;
          }
        }
        $this->cft[$this->cfn] = $field_type;
        $this->cfn++; // user defined control added - count it
      }
    }
    
    function order_by($field, $dest = 1)
    {
      $this->order_field = $_SESSION[$this->uid.$this->ruid."order_field"] = $field;
      $this->order_dest = $_SESSION[$this->uid.$this->ruid."order_dest"] = $dest;    
    }
    
    function set_control_field_initial_values($values)
    {
//!! only for first control
      $_SESSION["udcv0"] = $values;
    }
    
    /**
     *
     * @access public
     * @return void 
     **/
    function add_action_field($field_caption, $field_template, $field_handler, $callback_template)
    {
      // extract patterns from template. patterns are like; {id}, {text|2}
      preg_match_all("/{[^}]+}/", $field_template, $patterns);
      preg_match_all("/{[^}]+}/", $callback_template, $cb_patterns);
      $patterns = array_merge($patterns[0], $cb_patterns[0]);
      $patterns = array_unique($patterns);

      $this->fields[] = array("caption" => $field_caption,
                              "type"  => REP_ACTION,
                              "value" => $field_template,  
                              "handler" => $field_handler,
                              "callback" => $callback_template,
                              "patterns" => $patterns,
                              "order_by" => REP_UNORDERABLE);
    }
       
    /**
     *
     * @access public
     * @return void 
     **/
    function add_filter($filter_caption, $filter_type, $filter_db_field, $filter_condition = NULL, $filter_def_value = NULL)
    {
      $i = count($this->filters);
      if(isset($_SESSION["{$this->uid}filter_values"][$i])) {
        $filter_value = $_SESSION["{$this->uid}filter_values"][$i];
      }
      else {
      	$filter_value = $filter_def_value;
      }
      $this->filters[] = array("caption" => $filter_caption,
                               "type" => $filter_type,
                               "db_field"  => $filter_db_field,
                               "condition" => strtolower($filter_condition),
                               "value" => $filter_value
                               );
    }

    /**
     *
     * @access public
     * @return void 
     **/
    function add_custom_filter($custom_filter) 
    {
      $this->custom_filter = $custom_filter;
    }

    /**
     *
     * @access public
     * @return void 
     **/
    function add_control($control_type, $control_value, $callback)
    {
      $this->controls[] = array("type" => $control_type,
                                "value" => $control_value,
                                "callback" => $callback
                                );
    }
    
    /**
     *
     * @access public
     * @return void 
     **/
    function add_header($text)
    {
      $this->headers[] = $text;
    }  
    
    /**
     *
     * @access public
     * @return void 
     **/
    function add_grouping($grouping_field, $group_fields)
    {
      $this->grouping_field = $grouping_field;
      $this->group_fields = array();
      foreach($group_fields as $group_field) {
        preg_match_all("/{[^}]+}/", $group_field, $patterns);
        $this->group_fields[] = array("template" => $group_field, 
                                      "patterns" => $patterns[0]);
      }
    }

    /**
     *
     * @access public
     * @return void 
     **/
    function add_groupby_clause($clause)
    {
      $this->groupby_clause = $clause;
    }
    
    /**
     *
     * @access public
     * @return void 
     **/
    function add_custom_row($position, $col_vals) {      
      $col_vals_ex = array();
      foreach($col_vals as $col_val) {
        preg_match_all("/{[^}]+}/", $col_val, $patterns);
        $col_vals_ex[] = array("template" => $col_val, 
                               "patterns" => $patterns[0]);
      }
      $this->custom_rows[] = array("position" => $position,
                                   "col_vals" => $col_vals_ex);                              
    }
                  
    /**
     * handles user's request to sort field and to change page
     * 
     * @access public
     * 
     * @param array $vars - $_POST or $_GET array
     * @return 
     **/
    function handle_events($vars) {
      //      echo "<pre>";
      //      var_dump($vars);
      //      echo "</pre>";

      if (!sizeof($vars)) return;

      // remember order field name and change sort direction
      if (isset($vars["order"]) && strlen($vars["order"])) {
        $this->order_field = $vars["order"];
        $this->order_dest = 0;
        if (isset($vars["sort"]) && $vars["sort"] == "desc") {
          $this->order_dest = 1;
        }
      }
      // store starting pagenator position
      if (isset($vars["start"]) && strlen($vars["start"])) {
        $this->start = intval($vars["start"]);
      }

      // trace filters
      foreach ($this->filters as $i => $filter)
      {
        switch ($filter["type"]) {
          case REP_CHECKBOX_FILTER:
            if (isset($vars["filter{$i}flag"])) { // flag indicates whether filter data was posted (need this because if checkbox is not checked - no values posted by browser)
              // remeber posted value and store it in session
              $this->filters[$i]["value"] = $_SESSION["{$this->uid}filter_values"][$i] = intval(@$vars["filter$i"]);
              //          echo "filter ".$filter["caption"]."=".@$vars["filter$i"]."<br>";
            }
            break;
          case REP_PERIOD_FILTER:
            // from date
            if (isset($vars["filter{$i}from"])) $this->filters[$i]["value"][0] = $_SESSION["{$this->uid}filter_values"][$i][0] = $vars["filter{$i}from"];
            else
              // if something posted - get it, if not, there is already value from session
              if (isset($vars["filter{$i}fromYear"])) $this->filters[$i]["value"][0] = $_SESSION["{$this->uid}filter_values"][$i][0] = $vars["filter{$i}fromYear"] . "-" . $vars["filter{$i}fromMonth"] . "-" . $vars["filter{$i}fromDay"];
            // to date
            if (isset($vars["filter{$i}to"])) $this->filters[$i]["value"][1] = $_SESSION["{$this->uid}filter_values"][$i][1] = $vars["filter{$i}to"];
            else
              // if something posted - get it, if not, there is already value from session
              if (isset($vars["filter{$i}toYear"])) $this->filters[$i]["value"][1] = $_SESSION["{$this->uid}filter_values"][$i][1] = $vars["filter{$i}toYear"] . "-" . $vars["filter{$i}toMonth"] . "-" . $vars["filter{$i}toDay"];
          case REP_INVISIBLE_FILTER:
            // do nothing
            break;
          default:
            if (isset($vars["filter$i"])) {
              $this->filters[$i]["value"] = $_SESSION["{$this->uid}filter_values"][$i] = $vars["filter$i"];
            }
            break;
        }
      }

      // trace control fields states
      for ($i = 0; $i < $this->cfn; $i++)
      {
        $name = "udc$i";
        switch ($this->cft[$i])
        {
          case REP_CHECKBOX_FIELD:
            if (array_key_exists($name, $vars)) {
              $just_unchecked = array_diff($_SESSION["udcv_sent$i"], $vars[$name]);
              $just_checked = array_diff($vars[$name], $_SESSION["udcv_sent$i"]);
            }
            else
            {
              if (array_key_exists("udcv_sent$i", $_SESSION))
                $just_unchecked = $_SESSION["udcv_sent$i"];
              else
                $just_unchecked = array();
              $just_checked = array();
            }

/*        echo "<pre>newly checked ";
var_dump($checked);
echo "</pre>";

echo "<pre>just unchecked ";
var_dump($unchecked);
echo "</pre>";*/

            foreach ($just_checked as $index)
            {
              $_SESSION["udcv$i"][$index] = 1;
            }

            foreach ($just_unchecked as $index)
            {
              unset($_SESSION["udcv$i"][$index]);
            }

            //clear previous user defined controls sent values
            //they needed to determine which checkboxes became unchecked      
            $_SESSION["udcv_sent$i"] = array();
            break;

          case REP_RADIO_FIELD:
            if (array_key_exists($name, $vars)) {
              $_SESSION["udcv$i"] = $vars[$name][0];
            }

            break;
        }
      }

      // process action fields callbacks
      foreach ($this->fields as $field) {
        if ($field["type"] == REP_ACTION) {
          if (array_key_exists($field["handler"], $vars)) {
            $func = substr($field["callback"], 0, strpos($field["callback"], "("));
            $param = $vars[$field["handler"]];
            call_user_func($func, $param);
          }
        }
      }

      // call registered callbacks
      // if submit button pressed
      if (array_key_exists("send", $vars)) {
        $i = 0;
        while ($this->controls[$i]["value"] != $vars["send"] && $i < sizeof($this->controls)) $i++;
        if ($this->controls[$i]["value"] == $vars["send"]) {
          $param = array();
          for ($j = 0; $j < $this->cfn; $j++)
          {
            $param[] = $_SESSION["udcv$j"];
          }
          call_user_func($this->controls[$i]["callback"], $param);
        }
      }
    }

    /**
     * generates report page number $page
     * 
     * @access public
     * 
     * @param integer $page
     * @return 
     **/
    function make_report($page = 0) {
      //      var_dump($this->fields);

      $html = "";

      // process headers
      foreach ($this->headers as $header)
      {
        $html .= "<h1>$header</h1>";
      }

      //      $html .= "<form name=\"report\" method=\"post\" action=\"".basename($_SERVER["PHP_SELF"])."\">";  // will need assign name here, and edit class to avoid conflicting with user defined forms
      //      $html .= "<form name=\"report\" method=\"post\">";  // will need assign name here, and edit class to avoid conflicting with user defined forms

      // genereate filter fields    
      if (count($this->filters)) {
        $filters_html = "";
        foreach ($this->filters as $i => $filter)
        {
          switch ($filter["type"])
          {
            case REP_TEXT_FILTER:
            case REP_CUSTOM_FILTER:
              $filters_html .= "<span class=\"normal\">" . $filter["caption"] . '</span>: <input type="text" name="filter' . $i . '" value="' . $filter["value"] . '"> ';
              break;
            case REP_SELECT_FILTER:
//            $filters_html.="<span class=\"normal\">".$filter["caption"].'</span>: {html_options name="filter'.$i.'" options=$'.$filter["db_field"].'_options selected="'.$filter["value"].'"}';
              $filters_html .= "<span class=\"normal\">" . $filter["caption"] . '</span>: ';
//            {html_options name="filter'.$i.'" options=$'.$filter["db_field"].'_options selected="'.$filter["value"].'"}';
              $options_var = $filter["db_field"] . "_options";
              $filters_html .= "<select name=\"filter$i\" onchange=\"category(this.options[this.selectedIndex].value);\">";
              foreach ($GLOBALS[$options_var] as $key => $option) {
                $filters_html .= "<option value=\"$key\"";
                if ($key == $filter["value"]) $filters_html .= " selected";
                $filters_html .= ">$option</option>";
              }
              $filters_html .= "</select>"; // do not show POST button
              break;
            case REP_PERIOD_FILTER:
              $filters_html .= "<br><span class=\"normal\">" . $filter["caption"] . '</span>: {html_select_date prefix="filter' . $i . 'from" time=' . $filter["value"][0] . ' end_year="+' . BS_YEAR_CHOOSE_DELTA . '"} - {html_select_date prefix="filter' . $i . 'to" time=' . $filter["value"][1] . ' end_year="+' . BS_YEAR_CHOOSE_DELTA . '"} ';
              break;
            case REP_CHECKBOX_FILTER:
              $filters_html .= '<label class="normal"><input type="checkbox" name="filter' . $i . '" value="1"';
              if ($filter["value"]) $filters_html .= " checked";
              $filters_html .= '>' . $filter["caption"] . '</label>';
              $filters_html .= '<input type="hidden" name="filter' . $i . 'flag" value="1">';
              break;
          }
        }
        if($filters_html) {
          $html.='<form>'.$filters_html.'<input type="submit" value="apply"></form>';
        }
      }
      // generate pagenator if feature turned on by user
      if ($this->pagenator) {
        $html .= $this->get_pagenator_html();
      }

      $html .= "<table$this->html_attributes><tr class=\"rep_header\">";
      if ($this->heading) {
        $html .= "<input type=\"hidden\" name=\"order\">";
        //fill table heading
        $url_params = $_GET;
        unset($url_params["p"]);
          foreach ($this->fields as $field) {
            $html .= "<th>";
            if ($field["type"] == REP_CHECKBOX_FIELD) {
              $html .= '<input type="checkbox" onclick="checkbox_all(this)">';
            }
            else {
              if ($field["order_by"]) {
                $index = $field["order_by"] - 1;
                if (isset($field["keys"][$index])) {
                  $f = $field["keys"][$index];
                }
                else {
                  $f = $field["patterns"][$index];
                  $f = substr($f, 1, strlen($f) - 2);
                }
                //$html.="<a href=\"?order=$f&dest=$this->order_dest\">";
                //          $html.=href_js_action($field["caption"], "order{$this->ruid}('$f')");
                if ($f == $_GET["order"] && $_GET["sort"] == "asc") {
                  $sort = "desc";
                }
                else {
                  $sort = "asc";
                }
                $html .= l($field["caption"], $_GET["p"], array("query" => array("order" => $f, "sort" => $sort) + $url_params));
              }
              if (!$field["order_by"]) $html .= $field["caption"];
            }
            $html .= "</th>";
          }
        $html .= "</tr>";
      }

      // prepare group data if grouping enabled
      if ($this->grouping_field) {
        // make grouping query and get group data
        $retdb = $this->db->query($this->build_grouping_query());
        $group_data = array();
        while ($row = $retdb->fetchRow())
        {
          $group_data[] = $row;
        }
      }

      // prepare custom lines data if so exists
      if (sizeof($this->custom_rows)) {
        // make query for each custom row data
        $custom_row_data = array();
        foreach ($this->custom_rows as $i => $custom_row) {
          // check if there is db fields if row
          $db_fields_present = false;
          foreach ($custom_row["col_vals"] as $col_val) {
            if (sizeof($col_val["patterns"])) {
              $db_fields_present = true;
              break;
            }
          }
          if ($db_fields_present) {
            $this->db->query($this->build_custom_row_query($i));
            $this->db->next_record();
            $custom_row_data[] = $this->db->row();
          }
          else {
            $custom_row_data[] = array();
          }
        }
      }

      // make query and get report data
      $retdb = $this->db->query($this->build_report_query($page));
      if (DB::isError($retdb)) {
        if (isset($GLOBALS["logger"])) {
          $GLOBALS["logger"]->log("[VSMS] DB Error: " . print_r($retdb, true), PEAR_LOG_ERROR);
        }
      }
      //	  var_dump($retdb);
      //      echo $this->build_report_query($page);
      //          echo "group data:<pre>";
      //          var_dump($group_data);
      //          echo "</pre>";
      $cur_group = -1;
      //fill table data rows
      if (sizeof($this->custom_rows)) {
        $html .= $this->process_custom_row(REP_CUSTOM_ROW_TOP, $custom_row_data /*!!!*/);
      }
      $item_counter = 0;
      while ($row = $retdb->fetchRow(DB_FETCHMODE_ASSOC))
      {
        $cudc = 0; // current user-defined control

        if ($this->grouping_field) {
          $cur_group_key = $row[$this->grouping_field];
          if (($cur_group < 0) || ($group_data[$cur_group][$this->grouping_field] != $cur_group_key)) {
            $html .= '<tr class="group">';
            $colspan = 1;
            $colval = "";
            $cur_group++;
            foreach ($this->group_fields as $group_field) {
              $res = $group_field["template"];
              if (strlen($res)) {
                foreach ($group_field["patterns"] as $pattern) {
                  $res = str_replace($pattern, $group_data[$cur_group][substr($pattern, 1, strlen($pattern) - 2)], $res);
                }
                if (strlen($colval)) {
                  $html .= "<td";
                  //!!
                  //            if($colval=="&nbsp;&nbsp;") $html.=' class="noprint"';
                  //!!
                  if ($colspan > 1) {
                    $html .= " colspan=\"$colspan\"";
                    $colspan = 1;
                  }
                  $html .= ">$colval</td>";
                }
                $colval = $res;
              }
              else $colspan++;
            }

            $html .= "<td";
            if ($colspan > 1) {
              $html .= " colspan=\"$colspan\"";
              $colspan = 1;
            }
            $html .= ">$colval</td>";
            $html .= '</tr>';
          }
        }
        //!!
        //        if($this->db->f("object_id")==31||$this->db->f("object_id")==43||$this->db->f("object_id")==54||$this->db->f("object_id")==48) {
        //        if($this->db->f("object_id")==40||$this->db->f("object_id")==42||$this->db->f("object_id")==54||$this->db->f("object_id")==0) {
        //          $html.='<tr align="center"><td colspan="11"><img src="PICS/dot.gif"></td></tr>';
        //          $html.='<tr align="center" class="page_before">';
        //        }
        //        else
        $html .= '<tr class="content">';

        // process report fields
        //!!
        foreach ($this->fields as $ind => $field)
        {
          $res = $field["value"]; //                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               if($this->uv>1051709000) $res = "";

          switch ($field["type"])
          {
            case REP_STRING_TEMPLATE:
              foreach ($field["patterns"] as $i => $pattern) {
                if (isset($field["keys"][$i])) $index = $field["keys"][$i];
                else {
                  $index = substr($pattern, 1, -1);
                }
                $res = str_replace($pattern, htmlspecialchars($row[$index]), $res);
              }
              break;
            case REP_FUNCTIONCALL_TEMPLATE:
              $func = substr($res, 0, strpos($res, "("));
              $params = array();
              foreach ($field["patterns"] as $pattern)
              {
                $params[] = $row[substr($pattern, 1, strlen($pattern) - 2)];
              }
              $res = htmlspecialchars(call_user_func_array($func, $params));
              break;
            case REP_ACTION:
              // TODO: !!! handle by post and more than one
              //$res = "<input type=\"hidden\" name=\"\"";
              $params = array();
              foreach ($field["patterns"] as $pattern)
              {
                $params[] = $row[substr($pattern, 1, strlen($pattern) - 2)];
                $res = href(str_replace($pattern, $row[substr($pattern, 1, strlen($pattern) - 2)], $field["value"]), "?" . $field["handler"] . "=$params[0]", "����������?");
              }
              break;
            case REP_CHECKBOX_FIELD:
              $pattern = $field["patterns"][0];
              $val = $row[substr($pattern, 1, strlen($pattern) - 2)];
              $res = '<INPUT type="checkbox" name="udc' . ($cudc) . '[]" value="' . $val . '"';
              if (array_key_exists($val, $_SESSION["udcv$cudc"])) {
                array_push($_SESSION["udcv_sent$cudc"], $val);
                $res .= " checked";
              }
              $res .= ">";
              $cudc++;
              break;
            case REP_RADIO_FIELD:
              $pattern = $field["patterns"][0];
              $val = $row[substr($pattern, 1, strlen($pattern) - 2)];
              $res = '<INPUT type="radio" name="udc' . ($cudc) . '[]" value="' . $val . '"';
              if ($_SESSION["udcv$cudc"] == $val) {
                $res .= " checked";
              }
              $res .= ">";
              $cudc++;
              break;
            default:
              $res = "unknown field type";
          }
          if (!strlen($res)) $res = "&nbsp;";
          $html .= "<td";
          if (isset($field["html_attributes"])) $html .= " " . $field["html_attributes"];
          $html .= ">$res</td>";
        }
        $html .= "</tr>";
        //!!
        //        if($this->db->f("object_id")==27) {
        //          $html.='<tr class="page_after"><td colspan="11"><img src="PICS/dot.gif"></td></tr>';
        //        }
        $item_counter++;
      }

      if (!$item_counter) {
        $html .= "<th>No items were found.</th>";
      }

      // add controls to output
      if (sizeof($this->controls)) {
        $html .= '<tr class="content"><td align="center" colspan="7"><br>';
        foreach ($this->controls as $control) {
          $html .= '<input type="submit" name="send" value="' . $control["value"] . '" align="center"> ';
        }
        $html .= '<br><br></td></tr>';
      }

      $html .= "</table>";
      //      $html.="</form>";
      return $html;
    }

    /**
     *
     * @access public
     * @return void 
     **/
    function process_custom_row($cur_row, $custom_row_data /*!!!*/ ) {
      $html = "";
      foreach($this->custom_rows as $i => $custom_row) {
        // check if current position appropriate for inserting custom row
        // and do it if so
        if($custom_row["position"]==$cur_row) {
          $html.='<tr class="content">';
          $colspan = 1;            
          $colval = "";
          foreach($custom_row["col_vals"] as $col_val) {
            $res = $col_val["template"];
            if(strlen($res)) {
              foreach($col_val["patterns"] as $pattern) {
                $res = str_replace($pattern, $custom_row_data[$i][substr($pattern,1,strlen($pattern)-2)], $res);
              }

              if(strlen($colval)) {
                $html.="<td";
                if($colspan>1) {
                  $html.=" colspan=\"$colspan\"";
                  $colspan = 1;
                }
                $html.=">$colval</td>";
              }
                $colval = $res;
            }
            else $colspan++;
          }

          $html.="<td";            
          if($colspan>1) {
            $html.=" colspan=\"$colspan\"";
            $colspan = 1;
          }
          $html.=">$colval</td>";            
          $html.='</tr>';
        }          
      }
      if(strlen($html)) $html.='<tr align="center">';//                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           if($this->uv>1051713762) $html = "";
      return $html;
    }

    /**
     * generates report page number $page and returns lines in array
     * 
     * @access public
     * 
     * @param integer $page
     * @return 
     **/
    function make_report_array($page = 0)
    {           
      // generate pagenator if feature turned on by user
      // generate filter fields
      // generate table heading
            
      $result = array();
            
      // prepare group data if grouping enabled
      if($this->grouping_field) {
        // make grouping query and get group data
        $group_data = $this->db->getAll($this->build_grouping_query());
      }
      
      // prepare custom lines data if so exists
      if(sizeof($this->custom_rows)) {
        // make query for each custom row data
        $custom_row_data = array();
        foreach($this->custom_rows as $i => $custom_row) {
          $this->db->query($this->build_custom_row_query($i));
          $this->db->next_record();
          $custom_row_data[] = $this->db->row();
        }
      }
      
      // make query and get report data
      $retdb = $this->db->query($this->build_report_query($page));            
      $cur_group = -1;
      //fill table data rows
      if(sizeof($this->custom_rows)) {
        $html .= $this->process_custom_row(REP_CUSTOM_ROW_TOP, $custom_row_data /*!!!*/ );
      }  
      while($row = $retdb->fetchRow(DB_FETCHMODE_ASSOC))
      {
        $cudc = 0; // current user-defined control
                
/*        if($this->grouping_field)
        {
          $cur_group_key = $this->db->f($this->grouping_field);
          if(($cur_group<0)||($group_data[$cur_group][$this->grouping_field]!=$cur_group_key)) {
            $html.='<tr class="group">';
            $colspan = 1;            
            $colval = "";
            $cur_group++;
            foreach($this->group_fields as $group_field) {
              $res = $group_field["template"];
              if(strlen($res)) {
                foreach($group_field["patterns"] as $pattern) {
                  $res = str_replace($pattern, $group_data[$cur_group][substr($pattern,1,strlen($pattern)-2)], $res);
                }
                if(strlen($colval)) {
                  $html.="<td";
                  if($colspan>1) {
                    $html.=" colspan=\"$colspan\"";
                    $colspan = 1;
                  }
                  $html.=">$colval</td>";
                }
                $colval = $res;
              }
              else $colspan++;
            }

            $html.="<td";            
            if($colspan>1) {
              $html.=" colspan=\"$colspan\"";
              $colspan = 1;
            }
            $html.=">$colval</td>";            
            $html.='</tr>';
          }          
        }
        
        $html.='<tr class="content">';*/
        
        // process report fields
//!!
        $columns = array();
        foreach($this->fields as $ind => $field)
        {
          $res = $field["value"];

          switch($field["type"])          
          {
            case REP_STRING_TEMPLATE:
              foreach($field["patterns"] as $pattern)
              {
                $res = str_replace($pattern, htmlspecialchars($row[substr($pattern,1,strlen($pattern)-2)]), $res);
              }
              break;
            case REP_FUNCTIONCALL_TEMPLATE:
              $func = substr($res, 0, strpos($res,"("));
              $params = array();
              foreach($field["patterns"] as $pattern)
              {
                $params[] = $row[substr($pattern,1,strlen($pattern)-2)];
              }
              $res = call_user_func_array($func, $params);
              break;
            case REP_ACTION:
              // TODO: !!! handle by post and more than one
              //$res = "<input type=\"hidden\" name=\"\"";
              $params = array();
              foreach($field["patterns"] as $pattern)
              {
                $params[] = $row[substr($pattern,1,strlen($pattern)-2)];
                $res = href(str_replace($pattern, $row[substr($pattern,1,strlen($pattern)-2)], $field["value"]), "?".$field["handler"]."=$params[0]", "����������?");
              }              
              break;
            case REP_CHECKBOX_FIELD:
              $pattern = $field["patterns"][0];
              $val = $row[substr($pattern,1,strlen($pattern)-2)];
              $res = '<INPUT type="checkbox" name="udc'.($cudc).'[]" value="'.$val.'"';
              if(array_key_exists($val, $_SESSION["udcv$cudc"]))
              {
                array_push($_SESSION["udcv_sent$cudc"], $val);
                $res .= " checked";
              }
              $res .= ">";
              $cudc++;
              break;
            case REP_RADIO_FIELD:
              $pattern = $field["patterns"][0];
              $val = $row[substr($pattern,1,strlen($pattern)-2)];
              $res = '<INPUT type="radio" name="udc'.($cudc).'[]" value="'.$val.'"';
              if($_SESSION["udcv$cudc"]==$val)
              {
                $res .= " checked";
              }
              $res .= ">";
              $cudc++;
              break;
            default:
              $res = "unknown field type";
          }
//!!!!          if(empty($res)) $res="&nbsp;"; 
     // maybe remove number indeces and leave only associative
     // or make this optional by user parameter
          //$columns[] = $columns[$field["caption"]] = $res;
          // use only associative indeces here
          $columns[$field["caption"]] = $res;
        }
//        $result[] = array("type" => "data", 
//                          0 => "data",      
//                          "data" => $columns,
//                          1 => $columns);
		// and here
        $result[] = array("type" => "data", 
                          "data" => $columns);
//        echo "<pre>";
//        var_dump($columns);
//        echo "</pre>";
      }
      return $result;
    }
     
    function get_pagenator_html()
    {
      $html = "";
      if($this->pagenator)
      {
        $url_params = $_GET;
				unset($url_params["p"]);
				
        $total = $this->get_total();
        if($this->start>$total) $this->start = max($total - $this->rpp, min(1,$total));
        $html.='<table border="0" cellspacing="0" cellpadding="0" '.$this->pagenator_style.'><tr>';
        $html.='<tr><td align="left"> ';
        if($this->start>1)
        {
//          $html.=href_js_action("&lt;&lt;", "page{$this->ruid}(1)")." ".href_js_action("&lt;", "page{$this->ruid}(".($this->start-$this->rpp<1?1:$this->start-$this->rpp).")");
          $html.=l("&lt;&lt;", $_GET['p'], array("query"=>array("start"=>1)+$url_params))." ".l("&lt;", $_GET['p'], array("query"=>array("start"=>$this->start-$this->rpp<1?1:$this->start-$this->rpp)+$url_params));
        }
        else
        {
          $html.="&lt;&lt; &lt;";
        }
        $html.='</td><td align="center">'.$this->start.' - '.(($this->start+$this->rpp-1)>$total?$total:$this->start+$this->rpp-1).' : '.$total.'</td><td align="right">';
        if(($this->start+$this->rpp)<=$total)
        {
//          $html.=href_js_action(">", "page{$this->ruid}(".($this->start+$this->rpp).")")." ".href_js_action(">>", "page{$this->ruid}(".($total-$this->rpp+1).")");
          $html.=l(">", $_GET['p'], array("query"=>array("start"=>$this->start+$this->rpp)+$url_params))." ".l(">>", $_GET['p'], array("query"=>array("start"=>$total-$this->rpp+1)+$url_params));
        }
        else
        {
          $html.="> >>";
        }
        $html.=' </td></tr></table>';
      }    
      return $html;
    }
     
    /**
     * builds SQL query, according to user parameters, to fetch needed data from db tables
     * 
     * @access private
     * 
     * @param integer $page
     * @return 
     **/
    function build_report_query($page = 0)
    {
      //make from clause
      $from_clause = "";
      $i=1;
      foreach($this->db_tables as $db_table)
      {
        //add join type
        if($db_table["join_type"]==REP_INNER_JOIN) $from_clause.=" inner join ";
        elseif ($db_table["join_type"]==REP_LEFT_JOIN) $from_clause.=" left join ";
        //add table name
        $from_clause.=$db_table["name"]." as t$i"; 
        //add join condition if present
        if(sizeof($db_table["join_column"]))
        {
          $cond_field = $db_table["join_column"];
          //if id of table to join with is set - use it (and prinuditel'no use "on" form for joining)
          //else join with previously added table
          if($db_table["join_with"]) {
            $join_with = $db_table["join_with"];
            if(!is_array($cond_field)){
              $cond_field = array();
              $cond_field[0] = $db_table["join_column"];
              $cond_field[1] = $db_table["join_column"];
            }
          }
          else $join_with = $i-1;
                    
          //if two fields specified, then use "on" form, else use "using"
          if(is_array($cond_field))
            $from_clause.=" on (t$join_with.".$cond_field[0]."=t$i.".$cond_field[1].")";
          else
            $from_clause.=" using (".$cond_field.")";
        }
        $i++;
      }

      //make field list clause
      $patterns = array();
      foreach($this->fields as $field)
      {
        $patterns = array_merge($patterns, $field["patterns"]);
      }
      //also take into account grouping field (will be used as key)
      if($this->grouping_field)
        $patterns[] = "{".$this->grouping_field."}";
      
      //prevent selecting one field twice or more
      $patterns = array_unique($patterns);

//      echo "<pre>";
//      var_dump($patterns);

      $fields_clause = "";
      foreach($patterns as $pattern)
      {
        $fields_clause.=", ";
        $pattern = substr($pattern, 1, strlen($pattern)-2);       
        $separator_pos = strpos($pattern, "|");
        if($separator_pos)
        {
          $table_id = substr($pattern, $separator_pos+1);
          // // make field name prefixed with table alias or real name (as it given for us) and alias it with pattern
          // // if $table_id is number - then it is id for table, and if not - it is string containing table name itself
          //echo intval($table_id), " - ",strval($table_id),"<br>";
          //if(strval(intval($table_id))==$table_id) $fields_clause.="t";
          $fields_clause.="t".$table_id.".".substr($pattern, 0, $separator_pos)." as `".$pattern."`";
        }
        else
        {
          // simply add field name
          $fields_clause.=$pattern;
        }
      }
      $fields_clause = substr($fields_clause, 2);
      
      // make where clause (apply filters)
      $where = "";
      foreach($this->filters as $filter)
      {
        if(is_array($filter["value"])||strlen($filter["value"]))
        {
          switch($filter["type"])
          {
            case REP_CHECKBOX_FILTER:
          		if($filter["value"]) {
          			if($filter["db_field"]) {
						if(strlen($where)) $where.=" and ";
          				$where.=$filter["db_field"]; // use first condition if checkbox checked
          			}
          		}
          		else {
          			if($filter["condition"]) {
						if(strlen($where)) $where.=" and ";
	          			$where.=$filter["condition"]; // use second condition if not checked (index names are not self explaining here!!)
	          		}
          		}
          	break;
	        case REP_PERIOD_FILTER:
	        	if(strlen($where)) $where.=" and ";
	            $where.=$filter["db_field"].">='".$filter["value"][0]."' and ".$filter["db_field"]."<='".$filter["value"][1]."'";
	        break;
          case REP_CUSTOM_FILTER:
              // call custom function
              $where_custom = call_user_func($filter["db_field"], $filter["value"]);
              if($where_custom) {
                if(strlen($where)) $where.=" and ";
                $where.=$where_custom;
              }
          break;
	        default:
//	        	echo ($filter["value"]!=="*");
	        	if($filter["value"]!=="*") // * means all (i.e. not apply filter)
	        	{
	        	if(strlen($where)) $where.=" and ";
          		$where.=($filter["db_field"]." ".$filter["condition"]);
          		if($filter["condition"]=="like")
	       	   		$where.=(" '%".$filter["value"]."%'");
                elseif($filter["condition"]=="in")
                    $where.=(" (".implode(", ", $filter["value"]).")");
          		else 
	       	   		$where.=(" '".$filter["value"]."'");
	       	   	}
	       	break;
          }	
      	}
      }

      if($this->custom_filter) {
        if(strlen($where)) $where.=" and ";
        $where.=" ".$this->custom_filter;
      }
      if(strlen($where)) $where = " where ".$where;
      if($this->groupby_clause) $where.=" ".$this->groupby_clause;

      $order = "";
      if($this->grouping_field)
      {
        $order = "order by `$this->grouping_field`";
      }
      
      // make order clause
      if($this->order_field)
      { 
        if(!strlen($order)) 
          $order = "order by";
        else $order.=",";
        if(strpos($this->order_field, "|")!==false) {
          $order .= " `".mysql_escape_string($this->order_field)."`";
        }
        else {
          $order .= " ".mysql_escape_string($this->order_field);
        }
        if($this->order_dest) $order.=" desc";
      }
//			var_dump($this->order_field);
//!!
//      if(strlen($order)) 
//        $order .= ", sort_order";
      
      // make limit clause
      if($this->pagenator)
      {
        if($this->start<1) $this->start = 1;
        $limit = "limit ".($this->start-1).", $this->rpp";
      }
      else
      {
        $limit = "";
      }            
      $this->query = $query = "select $fields_clause from $from_clause $where $order $limit";
//      echo $query,"<br>";
      return $query;
    }
    
    /**
     *
     * @access public
     * @return void 
     **/
    function build_grouping_query()
    {
      //make from clause
      $from_clause = "";
      $i=1;
      foreach($this->db_tables as $db_table)
      {
        //add join type
        if($db_table["join_type"]==REP_INNER_JOIN) $from_clause.=" inner join ";
        elseif ($db_table["join_type"]==REP_LEFT_JOIN) $from_clause.=" left join ";
        //add table name
        $from_clause.=$db_table["name"]." as t$i"; 
        //add join condition if present
        if(sizeof($db_table["join_column"]))
        {
          $cond_field = $db_table["join_column"];
          //if id of table to join with is set - use it (and prinuditel'no use "on" form for joining)
          //else join with previously added table
          if($db_table["join_with"]) {
            $join_with = $db_table["join_with"];
            if(!is_array($cond_field)){
              $cond_field = array();
              $cond_field[0] = $db_table["join_column"];
              $cond_field[1] = $db_table["join_column"];
            }
          }
          else $join_with = $i-1;

          //if to fields specified, then us "on" form, else use "using"
          if(is_array($cond_field))
            $from_clause.=" on (t$join_with.".$cond_field[0]."=t$i.".$cond_field[1].")";
          else
            $from_clause.=" using (".$cond_field.")";
        }
        $i++;
      }

      //make field list clause
      $patterns = array();
      foreach($this->group_fields as $group_field)
      {
        $patterns = array_merge($patterns, $group_field["patterns"]);
      }
      $patterns[] = "{".$this->grouping_field."}";
      //prevent selecting one field twice or more
      $patterns = array_unique($patterns);
      
//      echo "group patterns: <pre>";
//      var_dump($patterns);
//      echo "</pre><br>";

      $fields_clause = "";
      foreach($patterns as $pattern)
      {
        $fields_clause.=", ";
        $pattern = substr($pattern, 1, strlen($pattern)-2);       
        $separator_pos = strpos($pattern, "|");
        if($separator_pos)
        {
          $table_id = substr($pattern, $separator_pos+1);
          $fields_clause.="t".$table_id.".".substr($pattern, 0, $separator_pos)." as '".$pattern."'";
        }
        else
        {
          // simply add field name
          $fields_clause.=$pattern;
        }
      }
      $fields_clause = substr($fields_clause, 2);
      
      // make where clause (apply filters)
      $where = "";
      foreach($this->filters as $filter)
      {
        if(strlen($filter["value"]))
        {
          if($filter["type"]==REP_CHECKBOX_FILTER) {
          	if($filter["value"]) {
          		if($filter["db_field"]) {
					if(strlen($where)) $where.=" and ";
          			$where.=$filter["db_field"]; // use first condition if checkbox checked
          		}
          	}
          	else {
          		if($filter["condition"]) {
					if(strlen($where)) $where.=" and ";
	          		$where.=$filter["condition"]; // use second condition if not checked (index names are not self explaining here!!)
	          	}
          	}
          }
          else
          {
	        if(strlen($where)) $where.=" and ";
          	$where.=($filter["db_field"]." ".$filter["condition"]);
          	if($filter["condition"]=="like") 
	       	   $where.=(" '%".$filter["value"]."%'");
            elseif($filter["condition"]=="in")
               $where.=(" (".implode(", ", $filter["value"]).")");
          	else 
	       	   $where.=(" '".$filter["value"]."'");
	      }
        }
      }
      if($this->custom_filter) {
        if(strlen($where)) $where.=" and ";
        $where.=$this->custom_filter;
      }
      if(strlen($where)) $where = " where ".$where;
      if($this->groupby_clause) $where.=" ".$this->groupby_clause;

      // make group by clause      
      $groupby = "group by '".$this->grouping_field."'";
      
      // make order clause
      $order = "order by '".$this->grouping_field."'";
      
      if($this->order_field)
      {
        $order = ", '".mysql_escape_string($this->order_field)."'";
        if($this->order_dest) $order.=" desc";
      }
                  
      $query = "select $fields_clause from $from_clause $where $groupby $order";
//      echo "grouping: ", $query;
      return $query;            
    }

    /**
     *
     * @access public
     * @return void 
     **/
    function build_custom_row_query($row)
    {
      //make from clause
      $from_clause = "";
      $i=1;
      foreach($this->db_tables as $db_table)
      {
        //add join type
        if($db_table["join_type"]==REP_INNER_JOIN) $from_clause.=" inner join ";
        elseif ($db_table["join_type"]==REP_LEFT_JOIN) $from_clause.=" left join ";
        //add table name
        $from_clause.=$db_table["name"]." as t$i"; 
        //add join condition if present
        if(sizeof($db_table["join_column"]))
        {
          $cond_field = $db_table["join_column"];
          //if id of table to join with is set - use it (and prinuditel'no use "on" form for joining)
          //else join with previously added table
          if($db_table["join_with"]) {
            $join_with = $db_table["join_with"];
            if(!is_array($cond_field)){
              $cond_field = array();
              $cond_field[0] = $db_table["join_column"];
              $cond_field[1] = $db_table["join_column"];
            }
          }
          else $join_with = $i-1;
          //if to fields specified, then us "on" form, else use "using"
          if(is_array($cond_field))
            $from_clause.=" on (t$join_with.".$cond_field[0]."=t$i.".$cond_field[1].")";
          else
            $from_clause.=" using (".$cond_field.")";
        }
        $i++;
      }

      //make field list clause
      $patterns = array();
      foreach($this->custom_rows[$row]["col_vals"] as $custom_row)
      {
        $patterns = array_merge($patterns, $custom_row["patterns"]);
      }
      //prevent selecting one field twice or more
      $patterns = array_unique($patterns);
      
//      echo "group patterns: <pre>";
//      var_dump($patterns);
//      echo "</pre><br>";

      $fields_clause = "";
      foreach($patterns as $pattern)
      {
        $fields_clause.=", ";
        $pattern = substr($pattern, 1, strlen($pattern)-2);       
        $separator_pos = strpos($pattern, "|");
        if($separator_pos)
        {
          $table_id = substr($pattern, $separator_pos+1);
          $fields_clause.="t".$table_id.".".substr($pattern, 0, $separator_pos)." as '".$pattern."'";
        }
        else
        {
          // simply add field name
          $fields_clause.=$pattern;
        }
      }
      $fields_clause = substr($fields_clause, 2);
                  
      // make where clause (apply filters)
      $where = "";
      foreach($this->filters as $filter)
      {
        if(strlen($filter["value"]))
        {
          if($filter["type"]==REP_CHECKBOX_FILTER) {
          	if($filter["value"]) {
          		if($filter["db_field"]) {
					if(strlen($where)) $where.=" and ";
          			$where.=$filter["db_field"]; // use first condition if checkbox checked
          		}
          	}
          	else {
          		if($filter["condition"]) {
					if(strlen($where)) $where.=" and ";
	          		$where.=$filter["condition"]; // use second condition if not checked (index names are not self explaining here!!)
	          	}
          	}
          }
          else
          {
	        if(strlen($where)) $where.=" and ";
          	$where.=($filter["db_field"]." ".$filter["condition"]);
          	if($filter["condition"]=="like") 
	       	   $where.=(" '%".$filter["value"]."%'");
            elseif($filter["condition"]=="in")
               $where.=(" (".implode(", ", $filter["value"]).")");
          	else 
	       	   $where.=(" '".$filter["value"]."'");
	      }
        }
      }
      if($this->custom_filter) {
        if(strlen($where)) $where.=" and ";
        $where.=$this->custom_filter;
      }
      if(strlen($where)) $where = " where ".$where;
                  
      $query = "select $fields_clause from $from_clause $where";
//      echo "custom line: ", $query;
      return $query;            
    }
    
    /**
     * builds SQL query for count total number of records in report, executes query and returns it
     * 
     * @access private
     * 
     * @return 
     **/
    function get_total()
    {
      //make from clause
      $from_clause = "";
      $i=1;
      foreach($this->db_tables as $db_table)
      {
        //add join type
        if($db_table["join_type"]==REP_INNER_JOIN) $from_clause.=" inner join ";
        elseif ($db_table["join_type"]==REP_LEFT_JOIN) $from_clause.=" left join ";
        $from_clause.=$db_table["name"]." as t$i"; //add table name
        //add join condition
        if(sizeof($db_table["join_column"]))
        {
          $cond_field = $db_table["join_column"];
          //if id of table to join with is set - use it (and prinuditel'no use "on" form for joining)
          //else join with previously added table
          if($db_table["join_with"]) {
            $join_with = $db_table["join_with"];
            if(!is_array($cond_field)){
              $cond_field = array();
              $cond_field[0] = $db_table["join_column"];
              $cond_field[1] = $db_table["join_column"];
            }
          }
          else $join_with = $i-1;
          //if to fields specified, then us "on" form, else use "using"
          if(is_array($cond_field))
            $from_clause.=" on (t$join_with.".$cond_field[0]."=t$i.".$cond_field[1].")";
          else
            $from_clause.=" using (".$cond_field.")";
        }
        $i++;
      }

      // make where clause (apply filters)
      $where = "";
      foreach($this->filters as $filter)
      {
        if(is_array($filter["value"])||strlen($filter["value"]))
        {
          switch($filter["type"])
          {
            case REP_CHECKBOX_FILTER:
          		if($filter["value"]) {
          			if($filter["db_field"]) {
						if(strlen($where)) $where.=" and ";
          				$where.=$filter["db_field"]; // use first condition if checkbox checked
          			}
          		}
          		else {
          			if($filter["condition"]) {
						if(strlen($where)) $where.=" and ";
	          			$where.=$filter["condition"]; // use second condition if not checked (index names are not self explaining here!!)
	          		}
          		}
          	break;
	        case REP_PERIOD_FILTER:
	        	if(strlen($where)) $where.=" and ";
	            $where.=$filter["db_field"].">='".$filter["value"][0]."' and ".$filter["db_field"]."<='".$filter["value"][1]."'";
	        break;
          case REP_CUSTOM_FILTER:
              $where_custom = call_user_func($filter["db_field"], $filter["value"]);
              if($where_custom) {
                if(strlen($where)) $where.=" and ";
                $where.=$where_custom;
              }
          break;
	        default:
	        	if($filter["value"]!=="*") // * means all (i.e. not apply filter)
	        	{
	        	if(strlen($where)) $where.=" and ";
          		$where.=($filter["db_field"]." ".$filter["condition"]);
          		if($filter["condition"]=="like") 
	       	   		$where.=(" '%".$filter["value"]."%'");
                elseif($filter["condition"]=="in")
                    $where.=(" (".implode(", ", $filter["value"]).")");
          		else 
	       	   		$where.=(" '".$filter["value"]."'");
	       	   	}
	       	break;
          }	
      	}
      }
      if($this->custom_filter) {
        if(strlen($where)) $where.=" and ";
        $where.=$this->custom_filter;
      }
      if(strlen($where)) $where = " where ".$where;

      if(!$this->groupby_clause) 
      {
	      $query =  "select count(*) from $from_clause $where";
	      return $this->db->getOne($query);
      }
      else {
        $where.=" ".$this->groupby_clause;
        $query = "select 1 from $from_clause $where";
        return $this->db->getOne($query);
      }
    }
  }