<?
	class db_sasldb
	{
		var $dsn;
		
		function db_sasldb()
		{
			$this->dsn = "mysql://sasldb:sasldb@localhost/sasldb";
		}

		function &connect($persistent=FALSE)
		{
			$db = DB::connect($this->dsn, $persistent); // use persistant connection

			return $db;
		}
	}
