<?
	class db_ftp
	{
		var $dsn;
		
		function db_ftp()
		{
			$this->dsn = "mysql://ftpdb:ftpdb@localhost/ftpdb";
		}

		function &connect($persistent=FALSE)
		{
			$db = DB::connect($this->dsn, $persistent); // use persistant connection

			return $db;
		}
	}
