<?php
 // backend PIWIK integration, symfony
require_once($_SERVER['DOCUMENT_ROOT'] . '/../symfony/config/ProjectConfiguration.class.php');

$piwik_url = "https://twintierstech.net/piwik/";
$piwik_login_tpl = "module=VerseIntegration&action=logme_token&login=%s&token_auth=%s";

$configuration = ProjectConfiguration::getApplicationConfiguration('backend', 'prod', false);
$context = sfContext::createInstance($configuration);
$user = $context->getUser();
if ($user->isSuperAdmin()) {
    $login = 'admin';
    $piwik_token_auth = $user->getProfile()->getPiwikTokenAuth();
    $piwik_login = sprintf($piwik_login_tpl, $login, $piwik_token_auth);
    header("Location: $piwik_url?$piwik_login");
}
else
    if ($user->hasCredential('Statistics')) {
        // count stats visits
        $conn = Doctrine_Manager::connection();
        $ret = $conn->standaloneQuery("UPDATE sms_stats_visits SET visits=visits+1, last_visit=now() WHERE domain_id=? AND stat_system='piwik'", array(Doctrine::getTable('SmsDomain')->getCurrent()->getDomainId()));
        if ($ret->rowCount() == 0) {
            $conn->standaloneQuery("INSERT sms_stats_visits SET domain_id=?, stat_system='piwik', visits=1, last_visit=now()", array(Doctrine::getTable('SmsDomain')->getCurrent()->getDomainId()));
        }

        $login = $user->getUsername() . '.' . Doctrine::getTable('SmsDomain')->getCurrent()->getDomainName();
        $piwik_token_auth = $user->getProfile()->getPiwikTokenAuth();
        $piwik_login = sprintf($piwik_login_tpl, $login, $piwik_token_auth);
        header("Location: $piwik_url?$piwik_login");
    }
