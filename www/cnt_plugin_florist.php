<?php

include "cnt_plugin_main.php";
require_once($_SERVER['DOCUMENT_ROOT'] . '/../util/soap/nusoap.php');

function process_content($content, $state) {
    global $smarty;

    include_once($_SERVER['DOCUMENT_ROOT'] . '/../symfony/config/ProjectConfiguration.class.php');
    $configuration = ProjectConfiguration::getApplicationConfiguration('frontend', 'prod', false);

    $html = '';

    if(!$state) {

        $category_codes = get_category_codes();
        $category_links = get_category_links();
        $category_titles = get_category_titles();
        $category_images = get_category_images();
        $categories_to_display = array();

        foreach($category_codes as $category_code) {
            $categories_to_display[$category_code] = array(
                'title' => $category_titles[$category_code],
                'link' => $category_links[$category_code],
                'img' => $category_images[$category_code]
            );
        }

        $smarty->assign(array(
            'flowers_page' => sfConfig::get('app_florist_one_page'),
            'categories' => $categories_to_display
        ));
        $html = $smarty->fetch("cnt_plugin_florist_category_list.tpl");

    } else {

        $server_url = sfConfig::get('app_florist_one_soap');
        $key = sfConfig::get('app_florist_one_key');
        $pass = sfConfig::get('app_florist_one_pass');

        $category_link = arg(1);
        $product_code = arg(2);

        if($product_code) {
            $html = get_product_page($server_url, $key, $pass, $product_code);
        } else {
            $category_links = get_category_links();
            if(in_array($category_link, $category_links)) {
                $category_links = array_flip($category_links);
                $category_code = $category_links[$category_link];

                $html = get_product_list($server_url, $key, $pass, $category_code, $category_link);
            }
        }
    }

    return $html;
}

function get_category_codes() {
    return array('sy', 'p', 'fa', 'fs', 'fp', 'fl', 'fw', 'fh', 'fx', 'fc');
}

function get_category_links() {
    return array(
        'sy' => 'sympathy',
        'p'  => 'plants',
        'fa' => 'table-arrangements',
        'fs' => 'sprays',
        'fp' => 'floor-plants',
        'fl' => 'inside-casket',
        'fw' => 'wreaths',
        'fh' => 'hearts',
        'fx' => 'crosses',
        'fc' => 'casket-sprays'
    );
}

function get_category_titles() {
    return array(
        'sy' => 'Sympathy',
        'p'  => 'Plants',
        'fa' => 'Table Arrangements',
        'fs' => 'Sprays',
        'fp' => 'Floor Plants',
        'fl' => 'Inside Casket',
        'fw' => 'Wreaths',
        'fh' => 'Hearts',
        'fx' => 'Crosses',
        'fc' => 'Casket Sprays'
    );
}

function get_category_images() {
    return array(
        'sy' => '/img/florist/sympathy.jpg',
        'p'  => '/img/florist/plants.jpg',
        'fa' => '/img/florist/table_arrangements.jpg',
        'fs' => '/img/florist/sprays.jpg',
        'fp' => '/img/florist/floor_plants.jpg',
        'fl' => '/img/florist/inside_casket.jpg',
        'fw' => '/img/florist/wreaths.jpg',
        'fh' => '/img/florist/hearts.jpg',
        'fx' => '/img/florist/crosses.jpg',
        'fc' => '/img/florist/casket_sprays.jpg'
    );
}

function get_product_page($server_url, $key, $pass, $product_code) {
    global $smarty;

    $html = '';

    $client = new nusoap_client($server_url, true);
    $result = $client->call('getProduct2013', array("APIKey" => $key, "APIPassword" => $pass, "itemCode" => $product_code));
    if($result && !$result['errors']) {
        $product = $result['product'];
        if(@$_SESSION["florist_cart"][$product_code]) {
            $product['count'] = $_SESSION["florist_cart"][$product_code];
        }

        $smarty->assign('product', $product);
        $html = $smarty->fetch("cnt_plugin_florist_product_view.tpl");
    }

    return $html;
}

function get_product_list($server_url, $key, $pass, $category_code, $category_link) {
    global $smarty;

    $start_at = 1;
    if(!empty($_GET['start']) && $_GET['start'] > 0) {
        $start_at = $_GET['start'];
    }

    $products_to_page = 25;

    $products = array();
    $total = 0;

    $client = new nusoap_client($server_url, true);
    $result = $client->call('getProducts2013', array("APIKey" => $key, "APIPassword" => $pass, "category" => $category_code, "numProducts" => $products_to_page, "startAt" => $start_at));
    if($result && !$result['errors']) {
        $products = $result['products'];
        $total = $result['total'];
    }

    if(@$_SESSION["florist_cart"]) {
        foreach($products as $key=>$product) {
            if(@$_SESSION['florist_cart'][$product['code']]) {
                $products[$key]['count'] = $_SESSION['florist_cart'][$product['code']];
            }
        }
    }

    $prev = $start_at - $products_to_page;
    $next = $start_at + $products_to_page;
    $last = intval($total/$products_to_page)*$products_to_page + 1;

    $position_text = $start_at.' - '.($next < $total ? $next - 1 : $total);

    $smarty->assign(array(
        'products' => $products,
        'total' => $total,
        'flowers_page' => sfConfig::get('app_florist_one_page'),
        'category_link' => $category_link,
        'prev' => $prev,
        'next' => $next,
        'position_text' => $position_text,
        'last' => $last
    ));

    $html = $smarty->fetch("cnt_plugin_florist_product_list.tpl");
    return $html;
}