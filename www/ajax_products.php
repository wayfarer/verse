<?php
 // obsolete
// products management, ajax actions
include("inc/verse.inc.php"); //main header - initializes Verse environment
include("inc/imageutil.inc.php");

if ($user->have_role(ROLE_PRODUCTS_MANAGEMENT)) {
    //	$_POST = utf8_to_latin($_POST);
    $action = $_POST["action"];

    switch ($action) {
        case "list":
            $rep = createobject("report_ajax", array($db, "plg_product", REP_WITH_PAGENATOR, true, 20, 'width="650px" style="background-color: #DDEEFF"'));
            $rep->add_table("plg_product_category", REP_INNER_JOIN, "category_id");

            $rep->add_field("", REP_FUNCTIONCALL_TEMPLATE, 'image({image})', REP_UNORDERABLE, 'align="center"');
            $rep->add_field("Product", REP_STRING_TEMPLATE, '{name|1}', REP_ORDERABLE, 'align="center"');
            $rep->add_field("Category", REP_STRING_TEMPLATE, '{name|2}', REP_ORDERABLE, 'align="center"');
            $rep->add_field("Price", REP_STRING_TEMPLATE, '${price}', REP_ORDERABLE, 'align="center"');
            $rep->add_field("Quantity", REP_STRING_TEMPLATE, '{quantity}', REP_ORDERABLE, 'align="center"');
            $rep->add_field("Sold", REP_STRING_TEMPLATE, '{sold_times}', REP_ORDERABLE, 'align="center"');
            $rep->add_field("Enabled", REP_STRING_TEMPLATE, '{enabled}', REP_ORDERABLE, 'align="center"');
            $rep->add_field("", REP_STRING_TEMPLATE, href_js_action("edit", "new ProductEditor({product_id})"), REP_UNORDERABLE, 'align="center"');
            $rep->add_field("", REP_STRING_TEMPLATE, href_js_action("delete", "delete_product({product_id})"), REP_UNORDERABLE, 'align="center"');
            $rep->add_field("", REP_STRING_TEMPLATE, href("print", "?p={product_id}&print=1", 'target="_blank"'), REP_UNORDERABLE, 'align="center"');

            $rep->add_filter("", REP_INVISIBLE_FILTER, "t1.domain_id", "=", $domain_id);

            $rep->html_attributes('width="650px"');

            $rep->handle_events($_POST);

            $html = $rep->make_report();

            echo $html;
            break;
        case "list_categories":
            $rep = createobject("report_ajax", array($db, "plg_product_category", REP_WITH_PAGENATOR, true, 20, 'width="400px" style="background-color: #DDEEFF"'));

            $rep->add_field("Category", REP_STRING_TEMPLATE, '{name}', REP_ORDERABLE, 'align="center"');
            $rep->add_field("", REP_STRING_TEMPLATE, href_js_action("edit", "new CategoryEditor({category_id})"), REP_UNORDERABLE, 'align="center"');
            $rep->add_field("", REP_STRING_TEMPLATE, href_js_action("delete", "delete_category({category_id})"), REP_UNORDERABLE, 'align="center"');

            $rep->add_filter("", REP_INVISIBLE_FILTER, "domain_id", "=", $domain_id);

            $rep->html_attributes('width="400px"');

            $rep->handle_events($_POST);

            $html = $rep->make_report();

            echo $html;
            break;
        case "list_orders":
            $rep = createobject("report_ajax", array($db, "plg_product_order", REP_WITH_PAGENATOR, true, 20, 'width="650px" style="background-color: #DDEEFF"'));

            $rep->add_field("Order#", REP_STRING_TEMPLATE, '{order_id}', REP_ORDERABLE, 'align="center"');
            $rep->add_field("Date", REP_STRING_TEMPLATE, '{timestamp}', REP_ORDERABLE, 'align="center"');
            $rep->add_field("Payer", REP_STRING_TEMPLATE, '{payer}', REP_ORDERABLE, 'align="center"');
            $rep->add_field("Total", REP_STRING_TEMPLATE, '${total}', REP_ORDERABLE, 'align="center"');
            $rep->add_field("Status", REP_STRING_TEMPLATE, '{status}', REP_ORDERABLE, 'align="center"');
            $rep->add_field("Affiliate", REP_CALLBACK, 'affiliate({affiliate_id},{affiliate_from})', REP_UNORDERABLE, 'align="center"');
            $rep->add_field("", REP_STRING_TEMPLATE, href_js_action("view", "new OrderView({order_id})"), REP_UNORDERABLE, 'align="center"');

            $rep->add_filter("", REP_INVISIBLE_FILTER, "domain_id", "=", $domain_id);

            $rep->html_attributes('width="650px"');

            if (count($_POST) < 3) {
                $rep->order_by("timestamp");
            }
            $rep->handle_events($_POST);

            $html = $rep->make_report();

            echo $html;
            break;
        case "load":
            $product_id = intval(@$_POST["id"]);
            $query = "SELECT category_id, name, price, quantity, image, description, sold_times sold, enabled FROM plg_product WHERE product_id='$product_id' AND domain_id='$domain_id'";
            $ret = $db->getRow($query, DB_FETCHMODE_ASSOC);
            header("X-JSON:" . make_json_response($ret));
            break;
        case "load_category":
            $category_id = intval(@$_POST["id"]);
            $query = "SELECT name, ecommerce_enabled, price_enabled, additional_fields, field1enabled, field1title, field1type, field2enabled, field2title, field2type FROM plg_product_category WHERE category_id='$category_id' AND domain_id='$domain_id'";
            $ret = $db->getRow($query, DB_FETCHMODE_ASSOC);
            header("X-JSON:" . make_json_response($ret));
            break;
        case "load_order":
            $order_id = intval(@$_POST["id"]);
//			$query = "SELECT p.name name, p.price price, count FROM plg_product_order o INNER JOIN plg_product_order_product USING(order_id) INNER JOIN plg_product p USING(product_id) WHERE o.order_id='$order_id' AND o.domain_id='$domain_id'";
//			$ret = $db->getAll($query, DB_FETCHMODE_ASSOC);
            $query = "SELECT user_data, status FROM plg_product_order WHERE order_id='$order_id' AND domain_id='$domain_id'";
            $data = $db->getRow($query, DB_FETCHMODE_ASSOC);
            $u = unserialize($data["user_data"]);
            $u["status"] = $data["status"];
//		    $fields = array("E-mail"=>"email","First Name"=>"firstname","Last Name"=>"lastname","Company"=>"company","Phone"=>"phone","Address1"=>"address1","Address2"=>"address2","City"=>"city","State"=>"state","ZIP"=>"zip","Country"=>"country");
            $fields = array("Status" => "status", "Name" => "address_name", "Street" => "address_street", "City" => "address_city", "State" => "address_state", "ZIP" => "address_zip", "Country" => "address_country", "Email" => "payer_email", "Pending reason" => "pending_reason", "Memo" => "memo");
            echo '<h2>User details</h2>';
            echo '<table width="100%">';
            foreach ($fields as $title => $field) {
                echo "<tr><td>$title</td><td>" . @$u[$field] . "</td></tr>";
            }
            echo '</table>';
            echo '<h2>Order details</h2>';
            echo '<table width="100%">';
            $total = 0;
            // variant with items from order status
            $i = 1;
            while (isset($u["item_name$i"])) {
                echo '<tr><td>' . $u["item_name$i"] . '</td><td>x' . $u["quantity$i"] . '</td><td>$' . $u["mc_gross_$i"] . '</td></tr>';
                $i++;
            }

            // variant with items from DB
            /*		foreach($ret as $line) {
                   echo '<tr><td>'.$line["name"].'</td><td>$'.$line["price"].'x'.$line["count"].'</td></tr>';
                   $total += $line["price"]*$line["count"];
               } */
            echo '<tr><td colspan="3"><hr size="1"></td></tr>';
            echo '<tr><td>Shipping:</td><td></td><td>$' . $u["mc_shipping"] . '</td></tr>';
            echo '<tr><td>Taxes:</td><td></td><td>$' . $u["tax"] . '</td></tr>';
            echo '<tr><td>Total:</td><td></td><td><b>$' . $u["mc_gross"] . '</b></td></tr>';
            echo '</table>';
//			var_dump($ret);
            break;
        case "load_config":
            $query = "SELECT param, value FROM plg_product_config WHERE domain_id='$domain_id'";
            $ret = $db->getAssoc($query);
            header("X-JSON:" . make_json_response($ret));
            break;
        case "save":
            $product_id = intval(@$_POST["id"]);
            $p = array();
            $p["category_id"] = intval(@$_POST["category_id"]);
            $p["name"] = in(@$_POST["name"]);
            $p["price"] = in(@$_POST["price"]);
            $p["quantity"] = @intval($_POST["quantity"]);
            $p["image"] = in(@$_POST["image"]);
            $p["description"] = in(@$_POST["description"]);
            $p["sold_times"] = @intval($_POST["sold"]);
            $p["enabled"] = @($_POST["enabled"] ? 1 : 0);
            if ($product_id) {
                $query = "UPDATE plg_product SET " . make_set_clause($p) . ", timestamp=unix_timestamp() WHERE product_id='$product_id' AND domain_id='$domain_id'";
            }
            else {
                $query = "INSERT plg_product SET " . make_set_clause($p) . ", timestamp=unix_timestamp(), domain_id='$domain_id'";
            }
            $db->query($query);

            // create product thumbnail
            $image = "files/$domain_name" . leading_slash(@$_POST["image"]);
            if (!file_exists($image)) {
                $image = "files/$domain_name/image" . leading_slash(@$_POST["image"]);
            }
            if (file_exists($image)) {
                $point = strrpos($image, ".");
                $tn = substr($image, 0, $point) . "-tn" . substr($image, $point);
                resizejpg($image, 100, $tn); // TN max width=200
                echo $image, "=>", $tn;
            }
            echo $query, "ok";
            break;
        case "save_category":
            $category_id = intval(@$_POST["id"]);
            $p = array();
            $p["name"] = in(@$_POST["name"]);
            $p["ecommerce_enabled"] = intval($_POST["ecommerce_enabled"]);
            if ($p["ecommerce_enabled"] > 2 || $p["ecommerce_enabled"] < 0) $p["ecommerce_enabled"] = 0;
            $p["price_enabled"] = intval($_POST["price_enabled"]);
            if ($p["price_enabled"] > 2 || $p["price_enabled"] < 0) $p["price_enabled"] = 0;
            $p["additional_fields"] = intval($_POST["additional_fields"]);
            if ($p["additional_fields"] > 2 || $p["additional_fields"] < 0) $p["additional_fields"] = 0;
            $p["field1enabled"] = intval($_POST["field1enabled"]) ? 1 : 0;
            $p["field2enabled"] = intval($_POST["field2enabled"]) ? 1 : 0;
            $p["field1title"] = in(@$_POST["field1title"]);
            $p["field2title"] = in(@$_POST["field2title"]);
            $p["field1type"] = intval($_POST["field1type"]);
            $p["field2type"] = intval($_POST["field2type"]);

            if ($category_id) {
                $query = "UPDATE plg_product_category SET " . make_set_clause($p) . " WHERE category_id='$category_id' AND domain_id='$domain_id'";
            }
            else {
                $query = "INSERT plg_product_category SET " . make_set_clause($p) . ", domain_id='$domain_id'";
            }
            $db->query($query);
            break;
        case "save_config":
            $paypal_business = in(@$_POST["paypal_business"]);
            $return_page = in(@$_POST["return_page"]);
            $query = "REPLACE plg_product_config SET domain_id='$domain_id', param='paypal_business', value='$paypal_business'";
            $db->query($query);
            $query = "REPLACE plg_product_config SET domain_id='$domain_id', param='return_page', value='$return_page'";
            $db->query($query);
            break;
        case "delete":
            $product_id = intval(@$_POST["id"]);
            $query = "DELETE FROM plg_product WHERE product_id='$product_id' AND domain_id='$domain_id'";
            $db->query($query);
            echo "ok";
            break;
        case "delete_category":
            $category_id = intval(@$_POST["id"]);
            $query = "DELETE FROM plg_product_category WHERE category_id='$category_id' AND domain_id='$domain_id'";
            $db->query($query);
            echo "ok";
            break;
    }
}
function image($image) {
    if (!strlen($image)) {
        $image_tn = "image/products/noimage-tn.jpg";
        return "<img src=\"$image_tn\">";
    }
    else {
        $image_tn = str_replace(".", "-tn.", $image);
        return "<a href=\"$image\" target=\"_blank\"><img src=\"$image_tn\" border=\"0\"></a>";
    }
}

function affiliate($affiliate_id, $affiliate_from) {
    global $db;

    if ($affiliate_id) {
        $query = "SELECT title, ref FROM cms_affiliate WHERE affiliate_id='$affiliate_id'";
        $row = $db->getRow($query, DB_FETCHMODE_ASSOC);
        return "<acronym title=\"$affiliate_from (ref={$row["ref"]})\">" . $row["title"] . '</acronym>';
    }
    else {
        return "no affiliate";
    }
}
