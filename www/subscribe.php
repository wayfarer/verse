<?php
 // frontend subscribe for new obituaries
include("inc/verse.inc.php"); //main header - initializes Verse environment

$hash = in(@$_GET["id"]);
$action = $_GET["action"];

switch ($action) {
    case "subscribe_obits":
        $reply = array();
        $name = @$_POST["name"];
        $email = @$_POST["email"];
        $keywords = @$_POST["keyword"];
        if (!$name) {
            $reply[] = "You haven't entered your name";
        }
        if (!check_email($email)) {
            $reply[] = "The e-mail is invalid";
        }
        if (!count($reply)) {
            // check if subscriber exists
            $query = "SELECT email, enabled, confirmation_cnt FROM plg_subscribe WHERE email='" . in($email) . "' AND domain_id='$domain_id'";
            $ret = $db->getRow($query, DB_FETCHMODE_ASSOC);
            if ($ret) {
                if ($ret["enabled"]) {
                    $reply[] = "The address {$ret["email"]} has been already subscribed and confirmed for the new obituaries notification";
                }
                else {
                    $reply[] = "The address {$ret["email"]} has already requested the subscription for the new obituaries notification, but the subscribtion hasn't been activated yet. Please, click (or copy and paste to a browser address bar) the activation link from the confirmation e-mail.";
                    if ($ret["confirmation_cnt"] <= 5) {
                        $reply[] = "If you didn't receive it, please would check the spam filters and click the following link to send confirmation e-mail again:";
                        $reply[] = "<a class=\"resend\" href=\"#\" onclick='resend_confirmation(\"" . $ret["email"] . "\"); return false;'>Resend confirmation e-mail to {$ret["email"]}</a>";
                    }
                    else {
                        $reply[] = "The confirmation e-mail has been sent 5 times. It seems that you can't use the address {$ret['email']} to subscribe for the some technical reason. Please, use another e-mail address or try to subscribe again in 7 days";
                    }
                }
            }
            else {
                $hash = substr(md5(uniqid("")), 0, 16);
                $query = "INSERT plg_subscribe SET email='" . in($email) . "', name='" . in($name) . "', domain_id='$domain_id', hash='$hash', enabled=0, created_at=now(), updated_at=now()";
                $db->query($query);
                if (count($keywords) > 1 && !$keywords[0]) {
                    $subscribe_id = $db->getOne("SELECT id FROM plg_subscribe WHERE email = '" . $email . "' AND domain_id = $domain_id");
                    foreach ($keywords as $keyword) {
                        if ($keyword) {
                            $query = "SELECT id FROM plg_keyword WHERE domain_id = $domain_id AND keyword = '" . $keyword . "'";
                            if (!$db->getOne($query)) {
                                $db->query("INSERT INTO plg_keyword (keyword, domain_id) VALUES ('" . $keyword . "', $domain_id)");
                            }
                            $keyword_id = $db->getOne($query);
                            $db->query("INSERT INTO plg_subscribe_to_keyword (subscribe_id, keyword_id) VALUES ($subscribe_id, $keyword_id)");
                        }
                    }
                    subscribe_send_confirmation($name, $email, $hash, $keywords);
                } else {
                    subscribe_send_confirmation($name, $email, $hash);
                }
                $reply[] = "The confirmation e-mail has been sent to the address $email. To complete the subscription, please, follow the instructions in the e-mail";
            }
        }
        echo implode("<br>", $reply);
        break;
    case "resend_confirmation":
        $reply = array();
        $name = @$_POST["name"];
        $email = @$_POST["email"];

        // load subscriber data
        $query = "SELECT email, enabled, confirmation_cnt, hash FROM plg_subscribe WHERE email='" . in($email) . "' AND domain_id='$domain_id'";
        $ret = $db->getRow($query, DB_FETCHMODE_ASSOC);
        if ($ret) {
            if ($ret["enabled"]) {
                $reply[] = "The address {$ret["email"]} has been already subscribed and confirmed for the new obituaries notification";
            }
            else {
                $query = "UPDATE plg_subscribe SET confirmation_cnt=confirmation_cnt+1 WHERE email='{$ret["email"]}' AND domain_id='$domain_id'";
                $db->query($query);

                $hash = $ret["hash"];
                subscribe_send_confirmation($name, $email, $hash);
                $reply[] = "The confirmation e-mail has been sent to the address $email. To complete the subscription, please, follow the instructions in the e-mail";
            }
        }
        echo implode("<br>", $reply);
        break;
    case "unsubscribe":
        if ($hash) {
            $query = "DELETE FROM plg_obituary_subscribe WHERE hash='$hash' AND domain_id='$domain_id'";
            $db->query($query);
            if ($db->affectedRows()) {
                echo "You have been unsubscribed from the future email notifications";
            }
            else {
                echo "Your email is not found on our records";
            }
        }
        break;
    case "confirm": // candles subscription
        if ($hash) {
            $query = "UPDATE plg_obituary_subscribe SET enabled=1 WHERE hash='$hash' AND domain_id='$domain_id'";
            $db->query($query);
            if ($db->affectedRows()) {
                echo "Thank you. Your subscription has been confirmed. You will recieve an email when someone leaves a candle.";
            }
        }
        break;
    case "unobits": // unsubscribe_obits
        if ($hash) {
            $query = "DELETE FROM plg_subscribe WHERE hash='$hash' AND domain_id='$domain_id'";
            $db->query($query);
            if ($db->affectedRows()) {
                echo "You have been unsubscribed from future email notifications";
            }
            else {
                echo "Your email is not found on our records";
            }
        }
        break;
    case "obits": // confirm obits
        if ($hash) {
            $query = "UPDATE plg_subscribe SET enabled=1 WHERE hash='$hash' AND domain_id='$domain_id'";
            $db->query($query);
            if ($db->affectedRows()) {
                echo "Thank you. Your subscription has been confirmed. You will recieve an email when new obituary published at $domain_name.";
            }
        }
        break;
    case "confirm-registration": // confirm user registration
        if ($hash) {
            // needs review status
            $confirmed = "a-" . rand(10000, 99999);
            $query = "UPDATE cms_access_user SET enabled=3, hash='$confirmed' WHERE enabled=0 AND hash='$hash' AND domain_id='$domain_id'";
            $db->query($query);
            if ($db->affectedRows()) {
                echo "Your email address has been confirmed. The administrator will review your account and will activate it. You will be notified by e-mail when your account become active.";
            }
        }
        break;
    case "forget-password":
        if ($hash) {
            $query = "SELECT login, password FROM cms_access_user WHERE hash='$hash' AND domain_id='$domain_id'";
            $data = $db->getRow($query, DB_FETCHMODE_ASSOC);
            if ($data && !DB::isError($data)) {
                $theuser = createobject("user", array($db, "cms_access_user"));
                $theuser->login($data["login"], $data["password"], true);
                // reset hash
                $resethash = "fp-" . rand(10000, 99999);
                $query = "UPDATE cms_access_user SET hash='$resethash' WHERE hash='$hash' AND domain_id='$domain_id'";
                $db->query($query);
            }
        }
        if (page_exists("members")) {
            header("Location: /members");
        }
        else {
            header("Location: index.php");
        }
        break;
    case "autocomplete":
        $word = $_GET["q"];
        $keywords = $db->getCol("SELECT DISTINCT keyword AS place FROM plg_keyword WHERE domain_id = $domain_id AND keyword LIKE '$word%' UNION
                                     SELECT DISTINCT home_place FROM plg_obituary WHERE domain_id = $domain_id AND home_place LIKE '$word%' UNION
                                     SELECT DISTINCT birth_place FROM plg_obituary WHERE domain_id = $domain_id AND birth_place LIKE '$word%' UNION
                                     SELECT DISTINCT service_place FROM plg_obituary WHERE domain_id = $domain_id AND service_place LIKE '$word%' LIMIT 20");
        asort($keywords);
        array_unique($keywords);
        foreach ($keywords as $keyword) {
            echo $keyword . "\n";
        }
        break;
}

function subscribe_send_confirmation($name, $email, $hash, $keywords = array()) {
    global $domain_name;

    if (count($keywords)) {
        $keyword_text = ' with keywords: ' . implode(', ', $keywords);
    } else {
        $keyword_text = '';
    }

    $emailfrom = "noreply@twintierstech.net";
    $emailto = "$name <$email>";
    $subj = "Subscription for new obituaries confirmation";
    $text = "This email address has requested to receive new obituaries for site " . $domain_name . $keyword_text . ". To complete the subscription and receive them, please click the following link: http://$domain_name/subscribe.php?action=obits&id=$hash\n\nIf you did not initiate this request, please ignore this email and you will not be subscribed.";
    mail($emailto, $subj, $text, "From: $domain_name subscription service <$emailfrom>");
}
