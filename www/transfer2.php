<?php
 // domain transfer script, historically used to transfer domains from ws2 to ws1
// called by user using browser
include("inc/verse.inc.php"); //main header - initializes Verse environment

set_time_limit(0);

$server0 = "http://twintierstech.net";
$list_action = "/transfer.php?action=list_domains";
$info_action = "/transfer.php?action=info";
$export_action = "/transfer.php?action=export";

$table_map = array(
    'sms_domain' => array('domain_id', 'domain_name', 'enabled', 'email', 'postfix', 'email_enabled', 'ecommerce_enabled', 'price_enabled', 'paypal_checkout', 'affiliates_enabled', 'mode'),
    'cms_site' => array('domain_id', 'title', 'properties', 'css', 'header_mode', 'header', 'main_menu', 'popup_menu', 'footer'),
    'cms_node' => array('node_id', 'internal_name', 'domain_id', 'page_id', 'ord', 'depth', 'menu_type', 'display_name', 'params'),
    'cms_page' => array('page_id', 'domain_id', 'ord', 'page_type', 'internal_name', 'no_header', 'data', 'restricted', 'properties'),
    'cms_page_uc' => array('page_id', 'domain_id', 'page_type', 'name', 'no_header', 'data', 'properties', 'ord'),
    'cms_site_uc' => array('domain_id', 'title', 'properties', 'css', 'header_mode', 'header', 'footer'),
    'sms_user' => array('user_id', 'login', 'password', 'enabled', 'domain_id', 'name', 'email'),
    'plg_event' => array('event_id', 'event_case_id', 'domain_id', 'obituary_id', 'event_type', 'timestamp', 'description'),
    'plg_list' => array('list_id', 'guid', 'domain_id', 'type', 'name', 'company', 'address', 'address2', 'phone', 'fax', 'website', 'email', 'description'),
    'plg_mailbox' => array('mailbox_id', 'domain_id', 'type', 'name', 'login', 'forward'),
    'plg_movieclip' => array('movieclip_id', 'domain_id', 'title', 'description', 'filename', 'timestamp', 'protected', 'password', 'status', 'tmpfilename', 'origfilename'),
    'plg_obituary' => array('obituary_id', 'domain_id', 'obituary_case_id', 'first_name', 'middle_name', 'last_name', 'home_place', 'death_date', 'birth_place', 'birth_date', 'service_date', 'service_time', 'service_place', 'visitation_date', 'visitation_place', 'final_disposition', 'image', 'obit_text', 'candles_policy'),
    'plg_obituary_candle' => array('candle_id', 'candle_case_id', 'domain_id', 'obituary_id', 'name', 'email', 'ip', 'thoughts', 'timestamp', 'state', 'hash'),
    'plg_obituary_config' => array('domain_id', 'param', 'value'),
    'plg_product' => array('product_id', 'guid', 'domain_id', 'category_id', 'name', 'price', 'quantity', 'image', 'description', 'sold_times', 'timestamp', 'enabled'),
    'plg_product_category' => array('category_id', 'domain_id', 'name'),
    'plg_product_config' => array('domain_id', 'param', 'value'),
    'plg_product_order' => array('order_id', 'domain_id', 'timestamp', 'total', 'payer', 'affiliate_id', 'affiliate_from', 'user_data', 'status'),
    'plg_slide' => array('slide_id', 'domain_id', 'obituary_id', 'image', 'description'),
// ---
    'cms_user_role' => array('user_id', 'role_id', 'role_params'),
    'plg_obituary_movieclip' => array('obituary_id', 'movieclip_id')
);
/*  $table_map_dep = array(
    'cms_user_role' => array('table'=>'sms_user', 'key'=>'user_id', 'fields'=>array('user_id', 'role_id', 'role_params')),
    'plg_obituary_movieclip' => array('table'=>'plg_obituary', 'key'=>'obituary_id', 'fields'=>array('obituary_id','movieclip_id')),
  );
*/

$table_map_ftpdb = array(
    'accounts' => array("user_id", "username", "passwd", "uid", "gid", "ftpdir", "homedir")
);

$table_map_ftpdb_new = array(
    'accounts' => array("user_id", "username", "passwd", "uid", "gid", "homedir", "shell")
);

$action = $_GET['action'];

switch ($action) {
    case "list":
        // get domain list from server0
        $data = unserialize(file_get_contents($server0 . $list_action));
        if ($data) {
            //      $domains = array("domains"=>array(array("id"=>1, "name"=>"domain1"), array("id"=>2, "name"=>"domain2")));
            $json = array();
            foreach ($data["domains"] as $domain_id => $domain_name) {
                $json[] = array("id" => $domain_id, "name" => $domain_name);
            }
            $json = array("domains" => $json);
            echo json_encode($json);
        }
        break;
    case "info":
        $domain_id = intval($_GET['id']);
        if ($domain_id) {
            $data = unserialize(file_get_contents($server0 . $info_action . "&id=$domain_id"));
            $json = array("info" => array($data["info"]));
            echo json_encode($json);
        }
        break;
    case "transfer":
        $domain_id = intval($_GET['id']);
        $data = unserialize(file_get_contents($server0 . $export_action . "&id=$domain_id"));
        $flag = false;
        // import versesms tables
        foreach ($data['export']['versesms'] as $table => $rows) {
            //        if(in_array($table, array('sms_domain','cms_site','cms_node','cms_page','cms_page_uc','cms_site_uc','sms_user'))) continue;
            /*        if($table == "plg_product_config") {
            $flag = true;
            continue;
          }
          if(!$flag) continue; */

            $values = array();
            foreach ($rows as $row) {
                $vals = array();
                foreach ($table_map[$table] as $col) {
                    if (!is_null($row[$col])) {
                        $vals[] = "'" . in($row[$col]) . "'";
                    }
                    else {
                        $vals[] = 'NULL';
                    }
                }
                $values[] = "(" . implode(",", $vals) . ")";
            }
            $query = "INSERT IGNORE $table (" . implode(",", $table_map[$table]) . ") VALUES " . implode(",", $values);
            $ret = $db->query($query);
            if (DB::isError($ret)) {
                echo "DB Error!\n";
                var_dump($ret);
                exit;
            }
            else {
                echo "import $table: ", count($values), " rows, " . $db->affectedRows() . " inserted<br>";
            }
        }
        // import ftpdb
        $dbf_con = createobject("db_ftp");
        $dbf = $dbf_con->connect();
        $values = array();
        foreach ($data['export']['ftpdb'] as $row) {
            $vals = array();
            foreach ($table_map_ftpdb['accounts'] as $col) {
                if (!is_null($row[$col])) {
                    if ($col == "uid" || $col == "gid") {
                        $vals[] = "'33'"; // put defaults
                    }
                    else
                        if ($col == "homedir") {
                            $vals[] = "'/usr/sbin/nologin'"; // put defaults
                        }
                        else {
                            $vals[] = "'" . in($row[$col]) . "'";
                        }
                }
                else {
                    $vals[] = 'NULL';
                }
            }
            $values[] = "(" . implode(",", $vals) . ")";
        }

        if ($values) {
            $query = "INSERT IGNORE accounts (" . implode(",", $table_map_ftpdb_new['accounts']) . ") VALUES " . implode(",", $values);
            $ret = $dbf->query($query);
            if (DB::isError($ret)) {
                echo "DB Error!\n";
                var_dump($ret);
                exit;
            }
            else {
                echo "import FTPDB: accounts: ", count($values), " rows, " . $dbf->affectedRows() . " inserted<br>";
            }
        }
        else {
            echo "no FTPDB: accounts data<br>";
            var_dump($values);
            echo "<hr>";
            var_dump($data['export']['ftpdb']);
        }
        break;
}

