<?php
  // products page plugin
include "cnt_plugin_main.php";
include "inc/imageutil.inc.php";

function process_content($content, $state) {
	global $smarty;

	$id = intval(@$state["id"]);
	// track pagetype
	$p = arg(0);
	$smarty->assign("p", $p);

	if(!$id) {
		// use content1 as default content
		$cnt = process_tags($content["content1"], array("product_list"=>"product_list", "product_mylist"=>"product_mylist"), $state);
		// make some hints
		if(!$cnt) {
			$cnt = 'Hint: please use {product_list category="cat"} construct for content';
		}
	}
	else {
		$cnt = process_tags($content["content2"], array("product"=>"product"), $id);
		// make some defaults to defeat that dumb content makers :))
		if(!$cnt) {
			$cnt = cart().product(array("user_params"=>$id));
		}
	}
	return $cnt;
}

function product_list($params) {
    static $pass = 1;
    global $domain, $db, $domain_id;

    $domain_ids = getLinkedDomainIds();

    $nocolumns = array();
    if (isset($params['nocolumns'])) {
        $nocolumns = explode(',', $params['nocolumns']);
        foreach ($nocolumns as $i => $column) {
            $nocolumns[$i] = trim($column);
        }
    }

    // make report
    if (@$params["category"] != "all") {
        if (count($domain_ids)) {
            $query = "SELECT category_id, c.domain_id, domain_name, c.ecommerce_enabled, c.price_enabled FROM plg_product_category c JOIN sms_domain USING(domain_id)
                      WHERE name='" . in(@$params["category"]) . "' AND c.domain_id IN (".implode(", ", $domain_ids).")";
        } else {
            $query = "SELECT category_id, ecommerce_enabled, price_enabled FROM plg_product_category WHERE name='" . in(@$params["category"]) . "' AND domain_id='$domain_id'";
        }
        $categories = $db->getAll($query, DB_FETCHMODE_ASSOC);
        
        $category_ids = array();
        $options = array();
        $options["ecommerce_enabled"] = $domain->get("ecommerce_enabled");
        $options["price_enabled"] = $domain->get("price_enabled");

        if (count($categories) == 1) {
            $category_ids[] = $categories[0]["category_id"];

            $ecommerce_enabled = $categories[0]["ecommerce_enabled"];
            $price_enabled = $categories[0]["price_enabled"];
        } elseif (count($categories) > 1) {
            $ecommerce_enabled = $categories[0]["ecommerce_enabled"];
            $price_enabled = $categories[0]["price_enabled"];

            $ecommerce_enabled_mismatch = array();
            $price_enabled_mismatch = array();

            //check for "ecommerce_enabled" and "price_enabled" mismatches
            foreach($categories as $category) {
                $category_ids[] = $category["category_id"];

                if (get_ecommerce_price_enabled($options["ecommerce_enabled"], $category["ecommerce_enabled"]) != get_ecommerce_price_enabled($options["ecommerce_enabled"], $ecommerce_enabled)) {
                    $ecommerce_enabled_mismatch[] = array(
                        'domain_name' => $category["domain_name"],
                        'ecommerce_enabled' => $category["ecommerce_enabled"]
                    );
                }

                if (get_ecommerce_price_enabled($options["price_enabled"], $category["price_enabled"]) != get_ecommerce_price_enabled($options["price_enabled"], $price_enabled)) {
                    $price_enabled_mismatch[] = array(
                        'domain_name' => $category["domain_name"],
                        'price_enabled' => $category["price_enabled"]
                    );
                }
            }


            $error_message = '';
            $ecommerce_price_state = array(
                '0' => 'Off',
                '1' => 'On'
            );
            $ecommerce_price_category_state = array(
                '0' => 'Default',
                '1' => 'On',
                '2' => 'Off'
            );
            if (count($ecommerce_enabled_mismatch)) {
                $default = $categories[0]['ecommerce_enabled']==0 ? ' (Default = "'.$ecommerce_price_state[$options["ecommerce_enabled"]].'")' : '';
                $error_message .= 'There are "Ecommerce" mismatches:<br/>Domain "'.$domain->get("domain_name").'": Ecommerce = "'.$ecommerce_price_state[$options["ecommerce_enabled"]].
                                  '"<br/>Product category "'.@$params["category"].'" from "'.$categories[0]['domain_name'].'": Ecommerce = "'.$ecommerce_price_category_state[$categories[0]['ecommerce_enabled']].
                                  '"'.$default.'<br/>';

                foreach($ecommerce_enabled_mismatch as $ecommerce_mismatch) {
                    $default = $ecommerce_mismatch['ecommerce_enabled']==0 ? ' (Default = "'.$ecommerce_price_state[$options["ecommerce_enabled"]].'")' : '';
                    $error_message .= 'Product category "'.@$params["category"].'" from "'.$ecommerce_mismatch['domain_name'].'": Ecommerce = "'.$ecommerce_price_category_state[$ecommerce_mismatch['ecommerce_enabled']].'"'.$default.'<br/>';
                }
                $error_message .= '<br/>';
            }

            if (count($price_enabled_mismatch)) {
                $default = $categories[0]['price_enabled']==0 ? ' (Default = "'.$ecommerce_price_state[$options["price_enabled"]].'")' : '';
                $error_message .= 'There are "Price enabled" mismatches:<br/>Domain "'.$domain->get("domain_name").'": Price enabled = "'.$ecommerce_price_state[$options["price_enabled"]].'"<br/>Product category "'.@$params["category"].'" from "'.$categories[0]['domain_name'].'": Price enabled = "'.$ecommerce_price_category_state[$categories[0]['price_enabled']].'"'.$default.'<br/>';
                foreach($price_enabled_mismatch as $price_mismatch) {
                    $default = $price_mismatch['price_enabled']==0 ? ' (Default = "'.$ecommerce_price_state[$options["price_enabled"]].'")' : '';
                    $error_message .= 'Product category "'.@$params["category"].'" from "'.$price_mismatch['domain_name'].'": Price enabled = "'.$ecommerce_price_category_state[$price_mismatch['price_enabled']].'"'.$default.'<br/>';
                }
                $error_message .= '<br/>';
            }

            if ($error_message) {
                return "There are some errors:<br/><br/>".$error_message."Please adjust you settings.";
            }
        }

        if (isset($ecommerce_enabled) && isset($price_enabled)) {
            $options["ecommerce_enabled"] = get_ecommerce_price_enabled($options["ecommerce_enabled"], $ecommerce_enabled);
            $options["price_enabled"] = get_ecommerce_price_enabled($options["price_enabled"], $price_enabled);
        }
    }
    else {
        $category_ids = "all";
        $options = array("ecommerce_enabled" => $domain->get("ecommerce_enabled"), "price_enabled" => $domain->get("price_enabled"));
    }

    if (isset($params['list_maxlength'])) {
        $list_maxlength = $params['list_maxlength'];
    }
    else {
        $list_maxlength = 500;
    }

    $elpp = @$params["items_per_page"] ? $params["items_per_page"] : 25;
    $rep = createobject("report", array($db, "plg_product", @$params["nopagenator"] ? REP_NO_PAGENATOR : REP_WITH_PAGENATOR, true, $elpp, 'id="products_pagenator"', $pass++));

    if ($domain_id != 560) {
        if (!in_array('image', $nocolumns)) {
            $rep->add_field("", REP_CALLBACK, 'image({image}, {domain_id})', REP_UNORDERABLE, 'align="center" width="20%"');
        }
    }
    $rep->add_field("Product", REP_CALLBACK, 'product_link({name},{product_id})', REP_ORDERABLE, 'align="center" width="15%"');
    if ($options["ecommerce_enabled"] == 1 || $options["price_enabled"] == 1) {
        $rep->add_field("Price", REP_CALLBACK, 'format_price({price})', REP_ORDERABLE, 'align="center" width="10%"');
        $rep->add_field("Description", REP_CALLBACK, "description({description},{" . $list_maxlength . "})", REP_ORDERABLE, 'align="center" width="45%"');
        //		$rep->add_field("Description", REP_STRING, "{description}", REP_ORDERABLE, 'align="center" width="45%"');
    }
    else {
        $rep->add_field("Description", REP_CALLBACK, "description({description},{" . $list_maxlength . "})", REP_ORDERABLE, 'align="center" width="65%"');
    }
    if ($options["ecommerce_enabled"] == 1) {
        $rep->add_field("", REP_FUNCTIONCALL_TEMPLATE, 'product_cart({product_id})', REP_UNORDERABLE, 'align="center" width="10%"');
    }

    if (count($domain_ids) && count(getSharedProductIds())) {
        $rep->add_custom_filter("(domain_id=".$domain_id." OR product_id IN (".implode(', ', getSharedProductIds())."))");
    } else {
        $rep->add_filter("", REP_INVISIBLE_FILTER, "domain_id", "=", $domain_id);
    }

    // filter options
    /*        $query = "SELECT category_id, name FROM plg_product_category WHERE domain_id='$domain_id'";
      $category_id_options = $db->getAssoc($query);
      if(!$cat) {
      $cat = array_keys($category_id_options);
      $cat = $cat[0];
      }
      //        $rep->add_filter("Product type", REP_SELECT_FILTER, "category_id", "=", $cat);
      */
    if (count($category_ids) && $category_ids != "all") {
        $rep->add_filter("Product type", REP_INVISIBLE_FILTER, "category_id", "IN", $category_ids);
    }

    $rep->add_filter("", REP_INVISIBLE_FILTER, "enabled", "=", 1);

    // order
    if (isset($params['order'])) {
        $vals = explode(",", $params['order']);
        $rep->order_by($vals[0], ($vals[1] == "desc") ? 1 : 0);
    }

    $rep->html_attributes('id="products_report"');

    $rep->handle_events($_GET);

    //	      echo '<div style="display:none">', $rep->build_report_query(0), '</div>';

    $html = $rep->make_report();

    return str_replace('\n', "<br>", html_entity_decode($html));
}

function product($params) {
	global $db, $domain_id, $smarty, $domain;

	$item_id = $params["user_params"];
    $domain_ids = getLinkedDomainIds();
    
	if($item_id) {
		// show item, currently one form for all types
        if(count($domain_ids) && count(getSharedProductIds())) {
            $query = "SELECT product_id, category_id, domain_id, name, image, price, description FROM plg_product WHERE product_id='$item_id' AND (domain_id=".$domain_id." OR product_id IN (".implode(', ', getSharedProductIds())."))";
        } else {
            $query = "SELECT product_id, category_id, name, image, price, description FROM plg_product WHERE product_id='$item_id' AND domain_id='$domain_id'";
        }
		$data = $db->getRow($query, DB_FETCHMODE_ASSOC);

		// ecommerce config
		// fetch category setting
        if(count($domain_ids)) {
            $query = "SELECT ecommerce_enabled, price_enabled FROM plg_product_category WHERE category_id='".$data["category_id"]."' AND domain_id IN (".implode(", ", $domain_ids).")";
        } else {
            $query = "SELECT ecommerce_enabled, price_enabled FROM plg_product_category WHERE category_id='".$data["category_id"]."' AND domain_id='$domain_id'";
        }

		$cat_ecommerce_price = $db->getRow($query, DB_FETCHMODE_ASSOC);
        
        $cat_ecommerce = $cat_ecommerce_price['ecommerce_enabled'];
		if($cat_ecommerce>0) {
			if($cat_ecommerce>1) {
				$cat_ecommerce = 0;
			}
			$smarty->assign("ecommerce", $cat_ecommerce);
		}
		else {
			$smarty->assign("ecommerce", $domain->get("ecommerce_enabled"));
		}

        $cat_price = $cat_ecommerce_price['price_enabled'];
		if($cat_price>0) {
			if($cat_price>1) {
				$cat_price = 0;
			}
			$smarty->assign("price", $cat_price);
		}
		else {
			$smarty->assign("price", $domain->get("price_enabled"));
		}

		$data = str_replace('\n',"<br>", $data);

        // rewrite image src for product from linked domain
        if(isset($data["domain_id"])) {
            $data["image"] = $db->getOne("SELECT domain_name FROM sms_domain WHERE domain_id = '".$data["domain_id"]."'").leading_slash($data["image"]);
        }

		$data["count"] = intval(@$_SESSION["cart"][$item_id]);
		$smarty->assign("product", $data);
		return $smarty->fetch("cnt_plugin_product_view.tpl");
	}
}

function product_list_ajax($params) {
	global $db, $domain_id, $domain;

	$item_id = @$params["user_params"];

	// check if item_id belongs to domain
	$data = null;
	if($item_id) {
		$query = "SELECT 1 FROM plg_product WHERE product_id='$item_id' AND domain_id='$domain_id'";
		$ret = $db->getOne($query);
		if($ret) {
			$query = "SELECT product_id, category_id, name, image, price, description FROM plg_product WHERE product_id='$item_id'";
			$data = $db->getRow($query, DB_FETCHMODE_ASSOC);
		}
	}

	if(!$data) {
		@$GLOBALS["pass"]++;
		// show list if no item selected
		if(@$params["category"]!="all") {
            $query = "SELECT category_id FROM plg_product_category WHERE name='".in(@$params["category"])."' AND domain_id='$domain_id'";
            $category_id = $db->getOne($query);
		}
		else {
			$category_id = "all";
		}
		$GLOBALS["smarty"]->assign("category", $category_id);
		$GLOBALS["smarty"]->assign("pass", $GLOBALS["pass"]);
		$GLOBALS["smarty"]->assign("nopagenator", @$params["nopagenator"]);
		$GLOBALS["smarty"]->assign("deforder", @$params["order"]);
		$html = $GLOBALS["smarty"]->fetch("cnt_plugin_products.tpl");
	}
	else {
		// show item, currently one form for all types
		// ecommerce config
		// fetch category setting
        $query = "SELECT ecommerce_enabled FROM plg_product_category WHERE category_id='".$data["category_id"]."' AND domain_id='$domain_id'";
		$cat_ecommerce = $db->getOne($query);
		if($cat_ecommerce>0) {
			if($cat_ecommerce>1) {
				$cat_ecommerce = 0;
			}
			$GLOBALS["smarty"]->assign("ecommerce", $cat_ecommerce);
		}
		else {
			$GLOBALS["smarty"]->assign("ecommerce", $domain->get("ecommerce_enabled"));
		}

		$data = str_replace('\n',"<br>", $data);
		$data["count"] = intval(@$_SESSION["cart"][$item_id]);
		$GLOBALS["smarty"]->assign("product", $data);
		$html = $GLOBALS["smarty"]->fetch("cnt_plugin_product_view.tpl");
	}
	return $html;
}

function product_mylist($params)
{
	$item_id = @$params["user_params"];
	$domain_id = $GLOBALS["domain_id"];
	$db = &$GLOBALS["db"];
	// check if item_id belongs to domain
	$data = null;
	if($item_id) {
        $query = "SELECT 1 FROM plg_product WHERE product_id='$item_id' AND domain_id='$domain_id'";
		$ret = $db->getOne($query);
		if($ret) {
			//				$query = "SELECT product_id, name, image, price, description FROM plg_product WHERE product_id='$item_id'";
			//				$data = $db->getRow($query, DB_FETCHMODE_ASSOC);
		}
	}

	if(!$data) {
		// show list if no item selected
        $query = "SELECT category_id FROM plg_product_category WHERE name='".in(@$params["category"])."' AND domain_id='$domain_id'";
		$category_id = $db->getOne($query);
		$hash = @$params["user_params"]["uid"];
		$GLOBALS["smarty"]->assign("category", $category_id);
		$GLOBALS["smarty"]->assign("hash", $hash);
		$html = $GLOBALS["smarty"]->fetch("cnt_plugin_myproducts.tpl");
	}
	else {
		// show item, currently one form for all types
		$data = str_replace('\n',"<br>", $data);
		$GLOBALS["smarty"]->assign("product", $data);
		$html = $GLOBALS["smarty"]->fetch("cnt_plugin_product_view.tpl");
	}
	return $html;
}

function image($image, $domain_id) {
    global $db, $domain_name;
    
    $product_domain_name = $db->getOne("SELECT domain_name FROM sms_domain WHERE domain_id = '$domain_id'");
    if (!strlen($image)) {
        $image = "img/noimage-tn.jpg";
    }
    else {
        $image = thumbnail($product_domain_name.leading_slash($image));

        if ($domain_name == $product_domain_name) {
            $img = "files/". $product_domain_name . leading_slash($image);
        } else {
            $img = "files". leading_slash($image);
        }

        if (!file_exists($img)) {
            //      echo "files/$domain_name".leading_slash($image)."<br>";
            $image = "img/noimage-tn.jpg";
        }
    }
    return '<img src="' . leading_slash($image) . '">';

    /*  global $domain_name;

        if(!strlen($image)) {
            $image = "img/noimage-tn.jpg";
        }
        else {
            $img = str_replace(".","-tn.",$image);
        $img_full = "files/$domain_name".leading_slash($img);
        if(!file_exists($img_full)) {
          // new thumbnails location
          $img = '_thumbs'.leading_slash($image);
          $img_full = "files/$domain_name".leading_slash($img);
          if(!file_exists($img_full)) {
            $image = "img/noimage-tn.jpg";
          }
          else {
            $image = $img;
          }
        }
        else {
          $image = $img;
        }
        }
        return '<img src="'.leading_slash($image).'">';
    */
}

function product_link($name, $product_id) {
	return l($name, $_GET["p"].'/'.$product_id);
}

function product_cart($product_id) {
    global $smarty;
    $count = $_SESSION["cart"][$product_id] ? $_SESSION["cart"][$product_id] : 0;
    $product = array(
        'count' => $count,
        'product_id' => $product_id,
    );
    $smarty->assign("product", $product);
	$html = $smarty->fetch("cnt_plugin_product_addtocart.tpl");
	return $html;
}

function format_price($price) {
	return "<strong>$".number_format($price, 2)."</strong>";
}

function description($str, $maxlength) {
  if(strlen($str)<=$maxlength+3) return $str;
  return substr($str,0,$maxlength)."...";
}

function get_ecommerce_price_enabled($domain_option, $category_option) {
    switch($category_option) {
        case 0: $enabled = $domain_option;
                break;
        case 1: $enabled = 1;
                break;
        case 2: $enabled = 0;
    }
    return $enabled;
}


function getLinkedDomainIds() {
    global $db, $domain_id;
    static $domain_ids;

    if(!$domain_ids) {
        $group_ids = $db->getCol("SELECT lg.id FROM sms_domain_link_group lg JOIN sms_domain_link l on l.group_id=lg.id WHERE lg.is_shared_products='1' AND l.domain_id='$domain_id'");
        if (count($group_ids)) {
            $domain_ids = $db->getCol("SELECT DISTINCT domain_id FROM sms_domain_link WHERE group_id IN (".implode(', ', $group_ids).")");
        } else {
            $domain_ids = array();
        }
    }

    return $domain_ids;
}

function getSharedProductIds() {
    global $db, $domain_id;
    static $product_ids;

    if(!$product_ids) {
        $product_ids = $db->getCol("SELECT DISTINCT product_id FROM sms_domain_link_product WHERE domain_id='$domain_id'");
    }

    return $product_ids;
}