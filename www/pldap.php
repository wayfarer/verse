<?php
// include path initialization
if (substr(PHP_OS, 0, 3) == "WIN")
    $directory_separator = ";";
else
    $directory_separator = ":";

ini_set("include_path", ini_get("include_path") . $directory_separator . "lib/");

require_once("lib/Net/LDAP2.php");

error_reporting(E_ALL & ~E_STRICT);

// The configuration array:
$config = array(
    'binddn' => 'cn=vmailadmin,dc=twintierstech,dc=net',
    'bindpw' => 'ahW*o4uung',
    'basedn' => 'ou=Users,domainName=twintierstech.com,o=domains,dc=twintierstech,dc=net',
    'host' => 'ldaps://ldap1.twintierstech.net',
    'port' => 636
);

// Connecting using the configuration:
$ldap = Net_LDAP2::connect($config);

// Testing for connection error
if (PEAR::isError($ldap)) {
    die('Could not connect to LDAP-server: ' . $ldap->getMessage());
}

$result = $ldap->search(null, '(mail=*)');

// Check, if an error occured and do something.
// Here we use die() to show the message of the error.
if (PEAR::isError($result)) {
    die($result->getMessage());
}

echo "Found " . $result->count() . " entries!";

/*	foreach ($result as $item) {
      echo $item["dn"] . ': ' . $item['cn'][0] . PHP_EOL;
  }
*/