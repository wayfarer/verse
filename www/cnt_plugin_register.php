<?php
 // members, self-register page plugin
include "cnt_plugin_main.php";

function process_content($content, $state) {
    global $domain_id, $domain_name, $db;

    $ret = "";

    switch ($state["action"]) {
        default:
            $ret = process_tags($content["content1"]);
            if ($ret) {
                // user entered registration form
                // add form tag only if not there
                if (strpos($ret, "<form") === false) {
                    $ret = '<form method="post">' . $ret . '</form>';
                }
            }
            else {
                // default registration form
                $ret = file_get_contents("templates/helpers/registration_form.html");
            }
            break;
        case "signup":
            // validation
            $error_count = 0;
            if (strlen($state["name"]) < 2) {
                $error_count++;
                verse_set_message("Please, enter your name", "error");
            }
            if (strlen($state["login"]) < 2) {
                $error_count++;
                verse_set_message("Please, enter your username", "error");
            }
            if (strlen($state["password"]) < 2) {
                $error_count++;
                verse_set_message("Please, enter your password", "error");
            }
            if ($state["password"] != $state["password2"]) {
                $error_count++;
                verse_set_message("Passwords must be the same", "error");
            }
            if (!check_email($state["email"])) {
                $error_count++;
                verse_set_message("Please, specify valid email address", "error");
            }
            if (!$error_count) {
                if (!isset($state["debug"])) {
                    // read data
                    $login = $state["login"];
                    $password = $state["password"];
                    $name = $state["name"];
                    $emailto = $state["email"];

                    // validate params
                    $query = "SELECT email FROM sms_domain WHERE domain_id='" . $GLOBALS["domain_id"] . "'";
                    $emailfrom = $GLOBALS["db"]->getOne($query);

                    $emailfromname = $GLOBALS["domain_name"];
                    $subject = "$domain_name registration confirmation";
                    $text = file_get_contents("templates/helpers/registration_email.txt");

                    $hash = substr(md5(uniqid(rand())), 0, 16);
                    $confirm_url = "http://$domain_name/subscribe.php?action=confirm-registration&id=$hash";
                    $text = str_replace(array("{confirm_url}"), array($confirm_url), $text);

                    $p = array();
                    $p["domain_id"] = $domain_id;
                    $p["login"] = in($login);
                    $p["password"] = in($password);
                    $p["name"] = in($name);
                    $p["email"] = $emailto;
                    $p["hash"] = $hash;
                    $query = "INSERT cms_access_user SET " . make_set_clause($p) . ", created_at=now()";
                    $ret = $db->query($query);

                    if ($db->affectedRows() > 0) {
                        if (check_email($emailto)) {
                            // confirmation for the user if valid email entered
                            send_email($emailto, $subject, $emailfrom, $emailfromname, $text);
                        }
                        // notification for the admin if enabled
                        // fetch notification email for new user signup
                        $query = "SELECT value FROM cms_config WHERE param='member_signup_notification_email' AND domain_id='$domain_id'";
                        $notification_email = $db->getOne($query);
                        if (check_email($notification_email)) {
                            $subject = "$domain_name: new user have signed up";
                            $text = file_get_contents("templates/helpers/registration_notification.txt");
                            $text = str_replace(array("{name}", "{url}"), array($name, "http://$domain_name/members.php"), $text);
                            send_email($notification_email, $subject, $emailfrom, $emailfromname, $text);
                        }
                    }
                    else {
                        verse_set_message("The system cannot register you. Probably your email address already registered. Please try to register with another e-mail addess, or use forget password function to restore access to your account", "error");
                        header("Location: /?p=" . $_GET["p"]);
                        exit;
                    }
                }
                if ($content["content2"]) return process_tags($content["content2"]);
                else return "<div>Please, check your e-mail to confirm registration</div>";
            }
            else {
                header("Location: /?p=" . $_GET["p"]);
                exit;
            }
            break;
    }
    return $ret;
}

function send_email($emailto, $subject, $emailfrom, $emailfromname, $text) {
    global $domain_name;

    return mail($emailto, $subject, $text, "From: $emailfromname <$emailfrom>");
}
  