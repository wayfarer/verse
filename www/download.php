<?php
    include("inc/verse.inc.php"); //main header - initializes Verse environment

$file = @$_GET["f"];
$filename = "files/$domain_name/file/" . $file;

if (file_exists($filename)) {
    $ext = strtolower(substr(strrchr($file, "."), 1));
    switch ($ext) {
        case "jpg":
        case "jpeg":
            header('Content-type: image/jpg');
            break;
        case "pdf":
            header('Content-type: application/pdf');
            break;
    }
    header('Content-Disposition: attachment; filename="' . $file . '"');
    readfile($filename);
}
