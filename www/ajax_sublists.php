<?php
 // sublists management for chinesecasketdistibutor.com, ajax actions
include("inc/verse.inc.php"); //main header - initializes Verse environment

//	$_POST = utf8_to_latin($_POST);
// anonymous part
$action = $_POST["action"];
switch ($action) {
    case "list_front":
        $p = @$_POST["p"];
        $cat = @$_POST["cat"];
        $hash = @$_POST["hash"];

        // determine mylist_id
        $query = "SELECT mylist_id FROM plg_product_mylist WHERE hash='" . in($hash) . "'";
        $mylist_id = $db->getOne($query);

        if (!$mylist_id) {
            //			    echo "undefined";
            return;
        }

        $rep = createobject("report_ajax", array($db, "plg_product", REP_WITH_PAGENATOR, true, 25, 'width="550px" style="background-color: #DDDDDD"'));
        $rep->add_table("plg_product_mylist_item", REP_INNER_JOIN, "product_id");

        $rep->add_field("", REP_FUNCTIONCALL_TEMPLATE, 'image({image})', REP_UNORDERABLE, 'align="center"');
        $rep->add_field("Product", REP_STRING_TEMPLATE, '{name}', REP_ORDERABLE, 'align="center"');
        $rep->add_field("Price", REP_STRING_TEMPLATE, '<b>${price|2}</b>', REP_ORDERABLE, 'align="center"');
        $rep->add_field("Description", REP_STRING_TEMPLATE, '{description}', REP_ORDERABLE, 'align="center"');
//			$rep->add_field("", REP_STRING_TEMPLATE, '<a href="#" onclick="cart({product_id});return false;">add to cart</a>', REP_ORDERABLE, 'align="center" width="80px"');

        $rep->add_filter("", REP_INVISIBLE_FILTER, "t1.domain_id", "=", $domain_id);
        $rep->add_filter("", REP_INVISIBLE_FILTER, "t2.mylist_id", "=", $mylist_id);
        // filter options
        $query = "SELECT category_id, name FROM plg_product_category WHERE domain_id='$domain_id'";
        $category_id_options = $db->getAssoc($query);
        if (!$cat) {
            $cat = array_keys($category_id_options);
            $cat = $cat[0];
        }
        $rep->add_filter("Product type", REP_SELECT_FILTER, "category_id", "=", $cat);
//		    $rep->add_filter("Product type", REP_INVISIBLE_FILTER, "category_id", "=", $cat);

        $rep->html_attributes('width="550px"');

        $rep->handle_events($_POST);

        $html = $rep->make_report();

        echo str_replace('\n', "<br>", html_entity_decode($html));
        break;
}


if ($user->have_role(7)) {

    switch ($action) {
        case "list":
            $rep = createobject("report_ajax", array($db, "plg_product_mylist_item", REP_WITH_PAGENATOR, true, 50, 'width="720px" style="background-color: #DDEEFF"'));
            $rep->add_table("plg_product_mylist", REP_INNER_JOIN, "mylist_id");
            $rep->add_table("plg_product", REP_INNER_JOIN, "product_id", 1);
            $rep->add_table("plg_product_category", REP_INNER_JOIN, "category_id");

            $rep->add_field("", REP_CHECKBOX_FIELD, '{product_id|1}', REP_UNORDERABLE);
            $rep->add_field("", REP_FUNCTIONCALL_TEMPLATE, 'image({image})', REP_UNORDERABLE, 'align="center"');
            $rep->add_field("Product", REP_STRING_TEMPLATE, '{name|3}', REP_ORDERABLE, 'align="center"');
            $rep->add_field("Category", REP_STRING_TEMPLATE, '{name|4}', REP_ORDERABLE, 'align="center"');
            $rep->add_field("Description", REP_CALLBACK, 'desc({description})', REP_ORDERABLE, 'align="center"');
            $rep->add_field("Master Price", REP_STRING_TEMPLATE, '${price|3}', REP_ORDERABLE, 'align="center" width="10%"');
            $rep->add_field("My Price", REP_STRING_TEMPLATE, '${price|1}', REP_ORDERABLE, 'align="center" width="10%"');
            $rep->add_field("", REP_STRING_TEMPLATE, href_js_action("set price", "new ProductEditor({product_id|1})"), REP_UNORDERABLE, 'align="center" width="10%"');
            $rep->add_field("", REP_STRING_TEMPLATE, href_js_action("remove", "remove_item({product_id|1})"), REP_UNORDERABLE, 'align="center" width="10%"');
            $rep->add_field("", REP_STRING_TEMPLATE, href("print", "?l={mylist_id|1}&p={product_id|1}&print=1", 'target="_blank"'), REP_UNORDERABLE, 'align="center" width="10%"');
            // special field, it will place mylist_id to hidden field and only once
            $rep->add_field("", REP_FUNCTIONCALL_TEMPLATE, 'hidden_once({mylist_id|1})');

            $rep->add_filter("", REP_INVISIBLE_FILTER, "t2.domain_id", "=", $domain_id);
            $rep->add_filter("", REP_INVISIBLE_FILTER, "t2.user_id", "=", $user->data["user_id"]);

            $rep->html_attributes('width="720px"');

            $rep->handle_events($_POST);

            $html = '<form method="post" action="sublists.php?print=2" target="_blank"><input type="image" value="print selected" src="img/printer.png" align="left"> print selected<br>' . $rep->make_report() . '</form>';

            echo $html;
            break;
        case "masterlist":
            $rep = createobject("report_ajax", array($db, "plg_product", REP_WITH_PAGENATOR, true, 50, 'width="720px" style="background-color: #DDEEFF"'));
            $rep->add_table("plg_product_category", REP_INNER_JOIN, "category_id");

            $rep->add_field("", REP_FUNCTIONCALL_TEMPLATE, 'image({image})', REP_UNORDERABLE, 'align="center"');
            $rep->add_field("Product", REP_STRING_TEMPLATE, '{name|1}', REP_ORDERABLE, 'align="center"');
            $rep->add_field("Category", REP_STRING_TEMPLATE, '{name|2}', REP_ORDERABLE, 'align="center"');
            $rep->add_field("Price", REP_STRING_TEMPLATE, '${price}', REP_ORDERABLE, 'align="center" width="10%"');
            $rep->add_field("Description", REP_CALLBACK, 'desc({description})', REP_ORDERABLE, 'align="center"');
//			$rep->add_field("Description", REP_STRING_TEMPLATE, '{description}', REP_ORDERABLE, 'align="center"');
//			$rep->add_field("", REP_CALLBACK_TEMPLATE, href_js_action("add to my list", "xxx"), REP_UNORDERABLE, 'align="center"');
            $rep->add_field("", REP_FUNCTIONCALL_TEMPLATE, "action({product_id})", REP_UNORDERABLE, 'align="center" width="15%"');

            $rep->add_filter("", REP_INVISIBLE_FILTER, "t1.domain_id", "=", $domain_id);
            $rep->add_filter("", REP_INVISIBLE_FILTER, "enabled", "=", 1);

            $rep->html_attributes('width="720px"');

            $rep->handle_events($_POST);

            $html = $rep->make_report();

//		    echo $rep->query;

            echo $html;
            break;
        case "add_mylist":
            $product_id = intval(@$_POST["id"]);
            $user_id = $user->data["user_id"];
            // determine mylist_id
            $query = "SELECT mylist_id FROM plg_product_mylist WHERE domain_id='$domain_id' AND user_id='$user_id'";
            $mylist_id = $db->getOne($query);
            $query = "INSERT plg_product_mylist_item SET mylist_id='$mylist_id', product_id='$product_id', price='0'";
            $db->query($query);
            echo "done";
            break;
        case "remove_mylist":
            $product_id = intval(@$_POST["id"]);
            $user_id = $user->data["user_id"];
            // determine mylist_id
            $query = "SELECT mylist_id FROM plg_product_mylist WHERE domain_id='$domain_id' AND user_id='$user_id'";
            $mylist_id = $db->getOne($query);
            $query = "DELETE FROM plg_product_mylist_item WHERE mylist_id='$mylist_id' AND product_id='$product_id'";
            $db->query($query);
            break;
        case "load":
            $product_id = intval(@$_POST["id"]);
            $user_id = $user->data["user_id"];
            $query = "SELECT mylist_id FROM plg_product_mylist WHERE user_id='$user_id' AND domain_id='$domain_id'";
            $mylist_id = $db->getOne($query);
            $query = "SELECT p.name name, i.price price FROM plg_product_mylist_item i INNER JOIN plg_product p USING(product_id) WHERE i.product_id='$product_id' AND mylist_id='$mylist_id'";
            $ret = $db->getRow($query, DB_FETCHMODE_ASSOC);
            header("X-JSON:" . make_json_response($ret));
            break;
        case "save":
            $product_id = intval(@$_POST["id"]);
            $user_id = $user->data["user_id"];
            $query = "SELECT mylist_id FROM plg_product_mylist WHERE user_id='$user_id' AND domain_id='$domain_id'";
            $mylist_id = $db->getOne($query);
            $p = array();
            $p["price"] = in(@$_POST["price"]);
            if ($product_id) {
                $query = "UPDATE plg_product_mylist_item SET " . make_set_clause($p) . " WHERE product_id='$product_id' AND mylist_id='$mylist_id'";
            }
            else {
                $query = "INSERT plg_product SET " . make_set_clause($p) . ", mylist_id='$mylist_id'";
            }
            $db->query($query);
            echo $query, "ok";
            break;
    }
}

function action($product_id) {
    global $domain_id, $user, $db;
    $user_id = $user->data["user_id"];
    // check if product in userlist
    $query = "SELECT count(*) FROM plg_product_mylist_item INNER JOIN plg_product_mylist USING(mylist_id) WHERE product_id='$product_id' AND domain_id='$domain_id' AND user_id='$user_id'";
    $ret = $db->getOne($query);

    if (!$ret) {
        return href_js_action("add to my list", "add_mylist($product_id)");
    }
    else {
        return href_js_action("remove", "remove_mylist($product_id)");
    }
}

function image($image) {
    if (!strlen($image)) {
        $image_tn = "image/products/noimage-tn.jpg";
        return "<img src=\"$image_tn\">";
    }
    else {
        $image_tn = str_replace(".", "-tn.", $image);
        return "<a href=\"$image\" target=\"_blank\"><img src=\"$image_tn\" border=\"0\"></a>";
    }
}

function hidden_once($var) {
    static $done = 0;
    if (!$done) {
        $done = 1;
        return '<input type="hidden" name="l" value="' . $var . '">';
    }
    else return "";
}

function desc($description) {
    return html_entity_decode($description);
}
