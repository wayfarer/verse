<?php
 // utility to copy one domain's DB data to other, files must be copied manually
include("inc/verse.inc.php"); //main header - initializes Verse environment

if ($user->is_super()) {
    $action = @$_POST["action"];
    switch ($action) {
        case "list_pages":
            $domain_id = intval($_POST["domain_id"]);
            $query = "SELECT page_id, internal_name name FROM cms_page WHERE domain_id='$domain_id' ORDER BY internal_name";
            $ret = $db->getAll($query, DB_FETCHMODE_ASSOC);
            echo make_json_response($ret);
            break;
        case "copy":
            $domain_id_from = intval($_POST["domain_id"]);
            $domain_name_from = $db->getOne("SELECT domain_name FROM sms_domain WHERE domain_id=$domain_id_from");
            if (isset($_POST["general"])) {
                $query = "SELECT title, properties FROM cms_site WHERE domain_id='$domain_id_from'";
                $ret = $db->getRow($query, DB_FETCHMODE_ASSOC);
                $title = in($ret["title"]);
                $properties = in($ret["properties"]);
                $query = "UPDATE cms_site SET title='$title', properties='$properties' WHERE domain_id='$domain_id'";
                $ret = $db->query($query);
            }
            if (isset($_POST["header"])) {
                $query = "SELECT header_mode, header FROM cms_site WHERE domain_id='$domain_id_from'";
                $ret = $db->getRow($query, DB_FETCHMODE_ASSOC);
                $header_mode = $ret["header_mode"];
                $header = in($ret["header"]);
                $query = "UPDATE cms_site SET header_mode='$header_mode', header='$header' WHERE domain_id='$domain_id'";
                $ret = $db->query($query);
            }
            if (isset($_POST["footer"])) {
                $query = "SELECT footer FROM cms_site WHERE domain_id='$domain_id_from'";
                $ret = $db->getRow($query, DB_FETCHMODE_ASSOC);
                $footer = $ret['footer'];
                // remove phpmv code from the footer
                $footer = preg_replace("/<!-- phpmyvisites -->.*<!-- \/phpmyvisites -->/is", "", $footer);
                // remove piwik code
                $footer = preg_replace("/<!-- Piwik -->.*<!-- End Piwik Tag -->/is", "", $footer);

                $footer = in($footer);
                $query = "UPDATE cms_site SET footer='$footer' WHERE domain_id='$domain_id'";
                $ret = $db->query($query);
            }
            if (isset($_POST["css"])) {
                $query = "SELECT css FROM cms_site WHERE domain_id='$domain_id_from'";
                $ret = $db->getRow($query, DB_FETCHMODE_ASSOC);
                $css = in($ret["css"]);
                $query = "UPDATE cms_site SET css='$css' WHERE domain_id='$domain_id'";
                $ret = $db->query($query);
            }
            if (isset($_POST["obit"])) {
                $query = "SELECT param, value FROM plg_obituary_config WHERE domain_id='$domain_id_from'";
                $ret = $db->getAll($query, DB_FETCHMODE_ASSOC);
                $vals = array();
                foreach ($ret as $row) {
                    $vals[] = "('$domain_id','" . in($row["param"]) . "','" . in($row["value"]) . "')";
                }
                // remove old parameters
                $query = "DELETE FROM plg_obituary_config WHERE domain_id='$domain_id'";
                $db->query($query);
                if ($vals) {
                    $query = "INSERT plg_obituary_config (domain_id, param, value) VALUES " . implode(",", $vals);
                    $ret = $db->query($query);
                }
            }
            if (isset($_POST["remove_structure"])) {
                $query = "DELETE FROM cms_node WHERE domain_id='$domain_id'";
                $ret = $db->query($query);
            }
            if (isset($_POST["structure"])) {
                $query = "SELECT page_id, display_name, ord, depth, menu_type, params FROM cms_node WHERE domain_id='$domain_id_from'";
                $ret = $db->getAll($query, DB_FETCHMODE_ASSOC);
                $vals = array();
                foreach ($ret as $row) {
                    $row = in($row); // escape things
                    $vals[] = "('" . $row['display_name'] . "','" . $domain_id . "','" . $row['ord'] . "','" . $row['depth'] . "','" . $row['menu_type'] . "','" . $row['params'] . "','" . $row['page_id'] . "')";
                }
                // insert nodes, existing nodes stays untouched
                if ($vals) {
                    $query = "INSERT cms_node (display_name, domain_id, ord, depth, menu_type, params, page_id_old) VALUES " . implode(",", $vals);
                    $ret = $db->query($query);
                }
            }
            if (isset($_POST["remove_pages"])) {
                $query = "DELETE FROM cms_page WHERE domain_id='$domain_id'";
                $ret = $db->query($query);
            }
            if (isset($_POST["pages"])) {
                $page_ids = $_POST["page_id"];
                $query = "SELECT page_id, ord, page_type, internal_name, no_header, data, restricted, properties FROM cms_page WHERE domain_id='$domain_id_from' AND page_id IN (" . implode(",", $page_ids) . ")";
                $ret = $db->getAll($query, DB_FETCHMODE_ASSOC);
                // insert pages, existing pages stays untouched
                foreach ($ret as $row) {
                    $row = in($row); // escape things
                    $values = "('" . $domain_id . "','" . $row['ord'] . "','" . $row['page_type'] . "','" . $row['internal_name'] . "','" . $row['no_header'] . "','" . $row['data'] . "','" . $row['restricted'] . "','" . $row['properties'] . "')";
                    $query = "INSERT cms_page (domain_id, ord, page_type, internal_name, no_header, data, restricted, properties) VALUES $values";
                    $ret = $db->query($query);
                    $page_id = $db->getOne("SELECT last_insert_id()");
                    // find pages-nodes associations, if so reassociate in new domain as well
                    $query = "SELECT node_id FROM cms_node WHERE domain_id='$domain_id' AND page_id_old='" . $row['page_id'] . "'";
                    $node_id = $db->getOne($query);
                    if ($node_id && !DB::isError($node_id)) {
                        $query = "UPDATE cms_node SET page_id='$page_id' WHERE node_id='$node_id' AND domain_id='$domain_id'";
                        $db->query($query);
                    }
                }
            }
            if (isset($_POST["remove_list_items"])) {
                $query = "DELETE FROM plg_list WHERE domain_id='$domain_id'";
                $db->query($query);
            }
            if (isset($_POST["list_items"])) {
                $query = "SELECT type, name, description, created_at FROM plg_list WHERE domain_id='$domain_id_from'";
                $ret = $db->getAll($query, DB_FETCHMODE_ASSOC);
                // insert list items, existing items left untouched
                $values = array();
                foreach ($ret as $row) {
                    $row = in($row);
                    $values[] = "($domain_id, '{$row['type']}', '{$row['name']}', '{$row['description']}', '{$row['created_at']}', now())";
                }
                $query = "INSERT plg_list (domain_id, type, name, description, created_at, updated_at) VALUES " . implode(",", $values);
                $db->query($query);
            }
            if (isset($_POST["remove_products"])) {
                $query = "DELETE FROM plg_product WHERE domain_id='$domain_id'";
                $db->query($query);
                $query = "DELETE FROM plg_product_category WHERE domain_id='$domain_id'";
                $db->query($query);
            }
            if (isset($_POST["products"])) {
                $fields = array('name', 'ecommerce_enabled', 'price_enabled', 'additional_fields', 'field1enabled', 'field1title', 'field1type', 'field2enabled', 'field2title', 'field2type');
                $query = "SELECT category_id, " . implode(',', $fields) . " FROM plg_product_category WHERE domain_id='$domain_id_from'";
                $ret = $db->getAll($query, DB_FETCHMODE_ASSOC);
                $values = array();
                $old_cats_map = array();
                foreach ($ret as $row) {
                    $old_cats_map[$row['category_id']] = $row['name'];
                    $row = in($row);
                    $ins = array($domain_id);
                    foreach ($fields as $field) {
                        $ins[] = "'" . $row[$field] . "'";
                    }
                    $values[] = "(" . implode(',', $ins) . ")";
                }
                $query = "INSERT plg_product_category (domain_id, " . implode(',', $fields) . ") VALUES " . implode(',', $values);
                $db->query($query);
                echo $query;

                $fields = array('category_id', 'name', 'price', 'quantity', 'image', 'description', 'enabled');
                $query = "SELECT " . implode(',', $fields) . " FROM plg_product WHERE domain_id='$domain_id_from'";
                $ret = $db->getAll($query, DB_FETCHMODE_ASSOC);
                $values = array();
                $images = array();
                foreach ($ret as $row) {
                    $row['category_id'] = find_category($old_cats_map[$row['category_id']], $domain_id);
                    $row = in($row);
                    $ins = array($domain_id);
                    foreach ($fields as $field) {
                        $ins[] = "'" . $row[$field] . "'";
                    }
                    $values[] = "(" . implode(',', $ins) . ")";
                    if ($row['image']) {
                        $images[] = $row['image'];
                    }
                }
                $query = "INSERT plg_product (domain_id, " . implode(',', $fields) . ") VALUES " . implode(',', $values);
                $db->query($query);
                // copy images
                foreach ($images as $image) {
                    $ifrom = "files/$domain_name_from" . leading_slash($image);
                    $ito = "files/$domain_name" . leading_slash($image);
                    copy($ifrom, $ito);
                }
            }
            break;
        default:
            // get domain list
            if ($domain->get("mode") > 0) { // not production
                $query = "SELECT domain_id, domain_name FROM sms_domain WHERE mode<2 AND alias_domain_id=0 ORDER BY domain_name";
                $domains = $db->getAssoc($query);
                $smarty->assign("domains", $domains);
            }
            else {
                $smarty->assign("denied", 1);
            }
            $smarty->display("copy.tpl");
    }
}
else {
    header("Location: login.php");
}

function find_category($name, $domain_id) {
    global $db;

    $name = in($name);
    $query = "SELECT category_id FROM plg_product_category WHERE name='$name' AND domain_id='$domain_id'";
    return $db->getOne($query);
}
