function propagateNewPage(str)
{
  var params_vals = str.split("&");

  // available in global scope
  var currentSearchStr = window.location.href;
	
  for( var i=0; i<params_vals.length; i++ ) {
      currentSearchStr = updateUrlParamValue(params_vals[i],currentSearchStr);
  }

  // Now load the new page.
  window.location.href = currentSearchStr;
};

/*
 * updateParamValue(newParamValue,urlStr) -- Helping propagate funtions to update value to url string.
 * eg. I want to update date value to search query or hash query
 *
 * Expecting:
 *        urlStr : A Hash or search query string. e.g: module=whatever&action=index=date=yesterday
 *        newParamValue : A param value pair: e.g: date=2009-05-02
 *
 * Return module=whatever&action=index&date=2009-05-02
 */
function updateUrlParamValue(newParamValue, urlStr)
{
    var p_v = newParamValue.split("=");

    var paramName = p_v[0];
    var valFromUrl = getUrlParamValue(paramName,urlStr);

    if( valFromUrl != '') {
        // replacing current param=value to newParamValue;
        var regToBeReplace = new RegExp(paramName + '=' + valFromUrl, 'ig');
        urlStr = urlStr.replace( regToBeReplace, newParamValue );
    } else {
        urlStr += (urlStr.indexOf('?') == -1 ) ? '?' + newParamValue : '&' + newParamValue;
    }

    return urlStr;
};

/*
 * return value for the requested param, will return the first match.
 * out side of this class should use getValueFromHash() or getValueFromUrl() instead.
 * return:
 *   Empty String if param is not found.
 */
function getUrlParamValue(param, url)
{
    var startStr = url.indexOf(param);

    if( startStr  >= 0 ) {
        var endStr = url.indexOf("&", startStr);
        if( endStr == -1 ) {
            endStr = url.length;
        }
        return url.substring(startStr + param.length +1,endStr);
    } else {
        return '';
    }
};


$(document).ready(function(){
	$("#periodString").hide();
	$("#otherPeriods").hide();
	$("#datepicker").hide();
	$("#periodString").show();
	
	// we get the content of the div before modifying it (append image, etc.)
	// so we can restore its value when we want
	var savedCurrentPeriod = $("#periodString #currentPeriod").html();

	// timeout used to fadeout the menu
	var timeout = null;
	var timeoutLength;

	// restore the normal style of the current period type eg "DAY"	
	function restoreCurrentPeriod()
	{
		$("#currentPeriod")
			.removeClass("hoverPeriod")
			.html(savedCurrentPeriod);
	}
	// remove the sub menu created that contains the other periods availble
	// eg. week | month | year
	function removePeriodMenu() {
		$("#otherPeriods").fadeOut('fast');
		setCurrentPeriodStyle = true;
	}
	
	// state machine a bit complex and was hard to come up with 
	// there should be a simpler way to do it with jquery...
	// if set to true, means that we want to style our current period
	// for example add bold and append the image
	var setCurrentPeriodStyle = true;

	$("#periodString #periods")
			.hover(function(){
				$(this).css({ cursor: "pointer"});
				
				// cancel the timeout
				// indeed if the user goes away of the div and goes back on
				// we don't hide the submenu!
				if(timeout != null)
				{
					clearTimeout(timeout);
					timeout = null;
					timeoutLength = 500;
				}
				else
				{
					timeoutLength = 0;
					setCurrentPeriodStyle = true;
				}
				if( setCurrentPeriodStyle == true) 
				{
					$("#currentPeriod:not(.hoverPeriod)")
						.addClass("hoverPeriod")
						.append('&nbsp;<img src="/img/more_period.gif" style="vertical-align:middle">');
				}
			}, function(){
				restoreCurrentPeriod();
				// we callback the function to hide the sub menu
				// only if it was visible (otherwise it messes the state machine)
				if($("#otherPeriods").is(":visible"))
				{
					timeout = setTimeout( removePeriodMenu , timeoutLength);
				}
				setCurrentPeriodStyle = false;
			})
			.click( function() {
				// we restore the initial style on the DAY link
				restoreCurrentPeriod();
				// the menu shall fadeout after 500ms
				timeoutLength = 500;
				// appearance!
				$("#otherPeriods").fadeIn();

			});

	$("#periodString #date")
		.hover( function(){
				$(this).css({ cursor: "pointer"});
			}, function(){
			
		})
		.click(function(){
			$("#datepicker").toggle();
			if($("#datepicker").is(":visible"))
			{
				$("#datepicker .ui-state-highlight").removeClass('ui-state-highlight');
			}
		});
} );
