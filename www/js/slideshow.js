/* SlideShow for TwinTiersTech
 *
 * (c) 2007 Vladimir Droznik <vladimir@mail.by>
 * SlideShow is freely distributable under the terms of MIT-style license.
 *
 * This library requires the JavaScript Framework "Prototype"
 * For details, see http://prototypejs.org/
/*--------------------------------------------------------------------------------*/

var SlideShow = Class.create();

Effect.Transitions.cubic = function(pos) {
  return pos*pos*pos;
}

SlideShow.prototype = {
  initialize: function(options) {
  	this.images = options.images;
  	this.container = options.container;
  	this.i = 1;
  	this.c = 0;
  },
  run: function() {
  	this.container.style.position = "relative";
  	this.container.innerHTML = '\
		<div class="first" style="text-align:center;position:absolute;width:100%"><img id="ss_first"></div>\
		<div class="second" style="text-align:center;position:absolute;width:100%"><img id="ss_second"></div>\
  	';
  	this.first = $("ss_first");
  	this.second = $("ss_second");
  	
  	this.first.onload = function() {
  		this.loaded = true;
  	}
  	this.second.onload = function() {
  		this.loaded = true;
  	}
  	this.first.loaded = false;
  	this.second.loaded = false;
  	this.first.src = this.images[0];
  	this.second.style.opacity = 0;
  	this.second.style.filter = "alpha(opacity=0)";
  	this.second.src = this.images[1];
  	setInterval(this.switchimages.bind(this), 3000);
  },  
  switchimages: function() {
    if(this.c % 2) {
	  	// launch effect only if new image fully loaded
	  	if(this.first.loaded) {
		  	new Effect.Opacity(this.first, {duration:1, from:0, to:1, transition: Effect.Transitions.cubic});
		  	new Effect.Opacity(this.second, {duration:1, from:1, to:0, transition: Effect.Transitions.cubic, afterFinish:this.changeimages.bind(this)});
		}
	}
	else {
	  	// launch effect only if new image fully loaded
	  	if(this.second.loaded) {
		  	new Effect.Opacity(this.second, {duration:1, from:0, to:1, transition: Effect.Transitions.cubic, afterFinish:this.changeimages.bind(this)});
		  	new Effect.Opacity(this.first, {duration:1, from:1, to:0, transition: Effect.Transitions.cubic});
		}
	}
  },
  changeimages: function() {
    if(this.c % 2) {
		if(++this.i >= this.images.length) {
			this.i=0;
		}		
	  	this.second.loaded = false;
		this.second.src = this.images[this.i];
	}
	else {
		if(++this.i >= this.images.length) {
			this.i=0;
		}		
	  	this.first.loaded = false;
		this.first.src = this.images[this.i];
	}
	this.c++;
  }
};