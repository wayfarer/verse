function show_progress() {
	$('body').prepend('<span class="progress" style="position:fixed;top:0;right:0;margin:10px 10px 0 0"><img src="img/loader.gif"></span>');
}
function hide_progress() {
  $('body span.progress').remove();
}

function open_thread(wall_id, thread_id) {
  show_progress();
  $("#verse-wall").load("wall.php?w="+wall_id+"&t="+thread_id, hide_progress);
}

function create_thread(theform) {
  $(theform).find("input[type=button]").attr("disabled", 1);
  show_progress();
  $.post("wall.php?action=create_thread", $(theform).serializeArray(), updated, 'text');
}

function updated(data) {
  update_wall();
}

function thread_updated(data) {
  update_thread();
}

function post_message(theform) {
  $(theform).find("input[type=button]").attr("disabled", 1);
  show_progress();
  $.post("wall.php?action=post_message", $(theform).serializeArray(), thread_updated, 'text');  
}

function reply_to(message_id) {
  if (!$('#reply' + message_id + ' #post-message-form').length) {
    $("#post-message-form").hide("fast", function(){
      $(this).appendTo($('#reply' + message_id)).slideDown("fast").find("input[name=reply_to_id]").val(message_id);
    });
  }
}

function delete_message(message_id) {
  if(confirm("Are you sure that you want to delete this message?")) {
    var elem = $("#msg-" + message_id);
    elem.fadeTo("slow", 0.5);
    show_progress();
    $.post("wall.php?action=delete_message", {'message_id':message_id}, function() {
        update_thread();
        elem.fadeOut("slow");
    });
  }
}

function delete_thread(thread_id) {
  if (confirm("Are you sure that you want to delete this discussion with all of it's messages?")) {
    show_progress();
    $.post("wall.php?action=delete_thread", {'thread_id':thread_id}, function() {
      update_wall();
    });
  }
}
