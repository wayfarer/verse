function openwin(url, width, height) {
  var width = width || 770;
  var height = height || 570;
  var left = (screen.width-width)/2;
  var top = (screen.height-height)/2;
  win=window.open(url, null, config="scrollbars=no,resizable=no,toolbar=no,location=no,menubar=no,width="+width+",height="+height+",top="+top+",left="+left+"");
  win.focus();
  return false;
}
