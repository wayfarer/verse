jQuery(function() {
    $('.obit-search-date').delegate('.hasDatepicker', 'hover', function() {
        if($('.obit-search-date-popup').length == 0){
            $(this).after('<div class="obit-search-date-popup">You must select a day or type the date into the search field</div>');
        } else {
            $('.obit-search-date-popup').remove();
        }
    });

    $('.obit-search-date').delegate('.obit-search-form-clear-date', 'click', function() {
        var datetype = $(this).attr('datetype');
        if(datetype == 'to' || datetype == 'from') {
            $('#obit_search_date'+datetype).attr('value', '');
        }
        return false;
    });
});
