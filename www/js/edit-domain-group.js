jQuery(function() {
    //obits
    if($('#sms_domain_link_group_is_shared_obits').attr('checked')) {
        add_obits_share_parent_checkbox();
    }

    $('#sms_domain_link_group_is_shared_obits').click(function() {
        if($(this).attr('checked')) {
            add_obits_share_parent_checkbox();
        } else {
            remove_obits_share_parent_checkbox();
        }
    });

    $('#share-from-parent-to-child').click(function() {
        if($(this).attr('checked')) {
            set_obits_share_parent_value("1");
        } else {
            set_obits_share_parent_value("");
        }
    });

    function add_obits_share_parent_checkbox() {
        if($('#shared-obits-share-parent').length == 0) {
            var checked = '';
            if(get_obits_share_parent_value() != "") {
                checked = ' checked="checked"';
            }
            $('.sf_admin_form_field_is_shared_obits').append('<div id="shared-obits-share-parent"><label for="share-from-parent-to-child">Share Obits from parent to children</label><input id="share-from-parent-to-child" type="checkbox"'+checked+'></div>');
        }
    }

    function remove_obits_share_parent_checkbox() {
        set_obits_share_parent_value("");
        $('#shared-obits-share-parent').remove();
    }

    function get_obits_share_parent_value() {
        return $('#sms_domain_link_group_share_obits_from_parent').attr("value");
    }

    function set_obits_share_parent_value(value) {
        $('#sms_domain_link_group_share_obits_from_parent').attr("value", value);
    }



    //domains
    var domain_ids = $('#sms_domain_link_group_domain_ids').attr('value').split(';');

    function set_group_domain_ids(ids) {
        $('#sms_domain_link_group_domain_ids').attr('value', ids.join(';'));
    }

    function reset_domain_ids_select() {
        $('#sms_domain_link_group_domain_id option[value=""]').attr("selected", "selected");
    }

    function add_domain(domain_id) {
        var domain_name = get_domain_name(domain_id);
        if(domain_name != '') {
            $('#domains').append("<div class='domain'>"+domain_name+"<img class='domain-delete' title='Delete \""+domain_name+"\" from this list' alt='Delete' src='/img/delete.png' domain_id='"+domain_id+"'></div>");
        }
    }

    function get_domain_name(domain_id) {
        return $('#sms_domain_link_group_domain_id option[value="'+domain_id+'"]').html();
    }

    function show_domain_error_message(container, message) {
        $(container).append(message).show();
        $(container).animate({opacity: 0}, 5000, function() {
            $(this).html('').hide().css('opacity', 1);
        });
    }

    $('#sms_domain_link_group_domain_id').after('<input type="button" id="add-domain-button" value="Add Domain"><div id="add-domain-message"></div><div id="domains"></div>');

    $('#domains').delegate('.domain-delete', 'click', function() {
        var domain_id = $(this).attr('domain_id');
        domain_ids = $.grep(domain_ids, function(value) {
            return value != domain_id;
        });
        set_group_domain_ids(domain_ids);
        $(this).parent().remove();
        delete_domain_pages(domain_id);
    });

    $.each(domain_ids, function(index, value) {
        add_domain(value);
    });

    $('#add-domain-button').click(function() {
        var selected_domain_id = $('#sms_domain_link_group_domain_id option:selected').val();
        if(selected_domain_id != '') {
            if($.inArray(selected_domain_id, domain_ids) == -1) {
                if(domain_ids != 0) {
                    domain_ids.push(selected_domain_id);
                } else {
                    domain_ids[0] = selected_domain_id;
                }
                set_group_domain_ids(domain_ids);
                add_domain(selected_domain_id);
                reset_options_of_select_list_domain();

                //load domain pages if "Is pages shared" checkbox checked
                if($('#sms_domain_link_group_is_shared_pages').attr('checked')) {
                    load_domain_pages(selected_domain_id);
                }
            } else {
                show_domain_error_message($('#add-domain-message'), 'This domain has been already added');
            }
            reset_domain_ids_select();
        } else {
            show_domain_error_message($('#add-domain-message'), 'You must select domain');
        }
    });



    //domain group pages
    var group_page_ids = window.JSON.parse($('#sms_domain_link_group_shared_page_ids').attr('value'));

    function set_group_page_ids(ids) {
        $('#sms_domain_link_group_shared_page_ids').attr('value', window.JSON.stringify(ids));
    }

    function load_domain_pages(domain_id) {
        if(domain_id != '') {
            $('#group-pages .preloader').show();
            $.post("get-domain-pages", { domain_id:  domain_id}, function(data) {
                add_domain_pages(domain_id, data);
                $('#group-pages .preloader').hide();
            }, "json");
        }
    }

    function add_domain_pages(domain_id, pages) {
        var domain_name = $('#sms_domain_link_group_domain_id option[value="'+domain_id+'"]').html();
        $('#group-pages').append('<div id="domain-'+domain_id+'" class="group-domain-pages"><div class="domain-name"><input type="checkbox" class="check-all-pages" domain_id="'+domain_id+'"><span>'+domain_name+'</span></div><div class="domain-pages"></div></div>');

        if(pages.length != 0) {
            $.each(pages, function(index, value) {
                var checked;
                if(!group_page_ids[domain_id] || $.inArray(index, group_page_ids[domain_id]) == -1) {
                    checked = '';
                } else {
                    checked = ' checked="checked" ';
                }
                $('#domain-'+domain_id+' .domain-pages').append('<div class="domain-page"><input id="page-'+index+'" type="checkbox"'+checked+'page_id="'+index+'" domain_id="'+domain_id+'"><label for="page-'+index+'">'+value+'</label></div>');
            });
        } else {
            $('#domain-'+domain_id+' .domain-pages').append("<div class='domain-page'>This domain has no pages</div>");
        }
    }

    function load_group_pages() {
        $.each(domain_ids, function(index, value) {
            load_domain_pages(value);
        });
    }

    $('#sms_domain_link_group_is_shared_pages').after('<div id="group-pages"><span class="preloader"><img width="16" height="16" src="/img/loader.gif"></span></div>');

    if($('#sms_domain_link_group_is_shared_pages').attr('checked')) {
        load_group_pages();
    }

    $('#sms_domain_link_group_is_shared_pages').click(function() {
        if($(this).attr('checked')) {
            load_group_pages();
        } else {
            $('.group-domain-pages').each(function() {
                $(this).remove();
            });
        }
    });

    $('#group-pages').delegate('.domain-page input[type=checkbox]', 'change', function() {
        var domain_id = $(this).attr('domain_id');
        var page_id = $(this).attr('page_id');
        
        if($(this).attr('checked')) {
            if(!group_page_ids[domain_id]) {
                group_page_ids[domain_id] = [];
            }
            
            //add page to list
            if($.inArray(page_id, group_page_ids[domain_id]) == -1) {
                group_page_ids[domain_id].push(page_id);
                set_group_page_ids(group_page_ids);
            }
        } else {
            //remove page from list
            group_page_ids[domain_id] = $.grep(group_page_ids[domain_id], function(value) {
                return value != page_id;
            });
            set_group_page_ids(group_page_ids);
        }
    });

    $('#group-pages').delegate('.check-all-pages[type=checkbox]', 'click', function() {
        var domain_id = $(this).attr('domain_id');
        
        if($(this).attr('checked')) {
            $('#domain-'+domain_id+' .domain-page input[type=checkbox]').attr('checked', 'checked').change();
        } else {
            $('#domain-'+domain_id+' .domain-page input[type=checkbox]').removeAttr('checked').change();
        }
    });

    function delete_domain_pages(domain_id) {
        if(group_page_ids[domain_id]) {
            group_page_ids[domain_id] = [];
        }
        set_group_page_ids(group_page_ids);
        $('#domain-'+domain_id).remove();
    }



    $(window).unload(function() {
        $.ajax({url: "get-group-domain-ids", async: false, dataType: "json", success: function(data){
            set_group_domain_ids(data);
        }});
        reset_domain_ids_select();

        $.ajax({url: "get-group-shared-page-ids", async: false, dataType: "json", success: function(data){
            set_group_page_ids(data);
        }});
    });



    //list domains
    if($('#sms_domain_link_group_is_shared_lists').attr('checked')) {
        process_list_domains();
    } else {
        delete_all_list_domains();
    }

    $('#sms_domain_link_group_is_shared_lists').click(function() {
        if($(this).attr('checked')) {
            process_list_domains();
        } else {
            $('#shared-list-parent-domains').remove();
            delete_all_list_domains();
        }
    });

    function process_list_domains() {
        var list_domains = get_list_domain_ids();
        add_select_list_domain();
        $.each(list_domains, function(index, value) {
            if(value != '') {
                append_list_domain(value);
            }
        });
    }

    function add_select_list_domain() {
        $('#sms_domain_link_group_is_shared_lists').after('<div id="shared-list-parent-domains"><select id="sms_domain_link_list_domain_id"></select>' +
                                                            '<input id="add-list-domain-button" type="button" value="Add Parent Domain">' +
                                                            '<div id="add-list-domain-message"></div>'+
                                                            '<div id="list-parent-domains"></div>' +
                                                          '</div>');
        add_options_to_select_list_domain();
    }

    function reset_options_of_select_list_domain() {
        $('#sms_domain_link_list_domain_id').empty();
        add_options_to_select_list_domain();
    }

    function add_options_to_select_list_domain() {
        $('#sms_domain_link_list_domain_id').append('<option selected="selected" value=""></option>');
        $.each(domain_ids, function(index, value) {
            $('#sms_domain_link_list_domain_id').append('<option value="'+value+'">'+get_domain_name(value)+'</option>');
        });
    }

    function delete_all_list_domains() {
        $('#sms_domain_link_group_list_domain_ids').attr('value', '');
    }

    $('.sf_admin_form_field_is_shared_lists').delegate('#add-list-domain-button', 'click', function() {
        add_list_domain($('#sms_domain_link_list_domain_id option:selected').val());
        $('#sms_domain_link_list_domain_id option[value=""]').attr("selected", "selected");
    });

    $('.sf_admin_form_field_is_shared_lists').delegate('.list-domain-delete', 'click', function() {
        $(this).parent().remove();
        delete_list_domain($(this).attr('domain_id'));
    });

    function append_list_domain(domain_id) {
        var domain_name = get_domain_name(domain_id);
        if(domain_name != '') {
            $('#list-parent-domains').append("<div class='list-domain'>"+domain_name+"<img class='list-domain-delete' title='Delete \""+domain_name+"\" from this list' alt='Delete' src='/img/delete.png' domain_id='"+domain_id+"'></div>");
        } else {
            show_domain_error_message($('#add-list-domain-message'), 'You must select domain');
        }
    }

    function add_list_domain(domain_id) {
        var list_domain_ids = get_list_domain_ids();
        if($.inArray(domain_id, list_domain_ids) == -1) {
            if(list_domain_ids != 0) {
                list_domain_ids.push(domain_id);
            } else {
                list_domain_ids[0] = domain_id;
            }
            set_list_domain_ids(list_domain_ids);
            append_list_domain(domain_id);
        } else {
            show_domain_error_message($('#add-list-domain-message'), 'This domain has been already added');
        }
    }

    function delete_list_domain(domain_id) {
        var list_domain_ids = get_list_domain_ids();
        list_domain_ids = $.grep(list_domain_ids, function(value) {
            return value != domain_id;
        });
        set_list_domain_ids(list_domain_ids);
    }

    function get_list_domain_ids() {
        return $('#sms_domain_link_group_list_domain_ids').attr('value').split(';');
    }

    function set_list_domain_ids(ids) {
        $('#sms_domain_link_group_list_domain_ids').attr('value', ids.join(';'));
    }
});