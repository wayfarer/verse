function add_obituary_image_loaders() {
    $('#config_obituary_theme_image').parent().append('<img id="config_obituary_theme_image_loader" class="config_obituary_image_loader" src="/img/loader.gif" />');
    $('#config_obituary_military_image').parent().append('<img id="config_obituary_military_image_loader" class="config_obituary_image_loader" src="/img/loader.gif" />');
}

function set_obituary_images(obit_theme, with_default) {
    $('.config_obituary_image_loader').show();
    $.post("/backend.php/config/obituaries/get_obit_images", {obit_theme: obit_theme, with_default: with_default}, function(images) {
        set_obituary_image(images, 'theme');
        set_obituary_image(images, 'military');
    }, "json");
}

function set_obituary_image(images, image_type) {
    if(image_type == 'theme' || image_type == 'military') {
        $('#config_obituary_'+image_type+'_image').empty();
        $.each(images[image_type+'_images'], function(value, image_name) {
            $('#config_obituary_'+image_type+'_image').append('<option value="'+value+'">'+image_name+'</option>');
        });
        $('#config_obituary_'+image_type+'_image option[value="'+$('#config_obituary_selected_'+image_type+'_image').attr('value')+'"]').attr("selected", "selected");
        add_obituary_image_preview(image_type);
    }
}

function add_obituary_image_preview(image_type) {
    if(image_type == 'theme' || image_type == 'military') {
        var image = $('#config_obituary_'+image_type+'_image option:selected').val();
        $('#config_obituary_'+image_type+'_image_preview').remove();

        if(image != '' && image != 'none') {
            $('#config_obituary_'+image_type+'_image_loader').show();
            $.post("/backend.php/config/obituaries/get_obit_image_thumbnail", {image: image}, function(thumb) {
                $('#config_obituary_'+image_type+'_image_loader').hide();
                $('#config_obituary_'+image_type+'_image').parent().append('<img id="config_obituary_'+image_type+'_image_preview" src="'+thumb+'" />');
            }, "json");
        } else {
            $('#config_obituary_'+image_type+'_image_loader').hide();
        }
    }
}