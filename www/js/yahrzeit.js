var jmd0 = new Array(0, 30, 59, 88, 117, 147, 0, 176, 206, 235, 265, 294, 324);
var jmd1 = new Array(0, 30, 59, 89, 118, 148, 0, 177, 207, 236, 266, 295, 325);
var jmd2 = new Array(0, 30, 60, 90, 119, 149, 0, 178, 208, 237, 267, 296, 326);
var jmd3 = new Array(0, 30, 59, 88, 117, 147, 177, 206, 236, 265, 295, 324, 354);
var jmd4 = new Array(0, 30, 59, 89, 118, 148, 178, 207, 237, 266, 296, 325, 355);
var jmd5 = new Array(0, 30, 60, 90, 119, 149, 179, 208, 238, 267, 297, 326, 356);
var jewish_month_days = new Array(jmd0, jmd1, jmd2, jmd3, jmd4, jmd5);
var jewish_month = new Array("Tishri", "Kheshvan", "Kislev", "Tevet", "Shevat", "Adar", "Adar II", "Nisan", "Iyyar", "Sivan", "Tammuz", "Av", "Elul");
var gregorian_month = new Array(0, "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December");
var sp = " ";
var pt = " ";

function common_year(year) {
    var rem = year % 19;
    var result = ((rem != 0) && (rem != 3) && (rem != 6) && (rem != 8) && (rem != 11) && (rem != 14) && (rem != 17));
    return result;
}

function year_after_leap_year(year) {
    var rem = year % 19;
    var result = ((rem == 1) || (rem == 4) || (rem == 7) || (rem == 9) || (rem == 12) || (rem == 15) || (rem == 18));
    return result;
}

function division(zaehler, nenner) {
    var quotient = zaehler / nenner;
    var rest = zaehler % nenner;
    var result = Math.round(quotient - (rest / nenner));
    return result;
}

function index_from_length(l) {
    var result = 3 * (division(l - 300, 80)) + 1 * (l % 10) - 3;
    return result;
}

function dhp_add(dhp1, dhp2) {
    var d = dhp1[0] + dhp2[0];
    var h = dhp1[1] + dhp2[1];
    var p = dhp1[2] + dhp2[2];
    dhp1[2] = p % 1080;
    h = h + division(p, 1080);
    dhp1[1] = h % 24;
    dhp1[0] = d + division(h, 24);
    return dhp1;
}

function dhp_mult(n, dhp) {
    var d = n * dhp[0];
    var h = n * dhp[1];
    var p = n * dhp[2];
    dhp[2] = p % 1080;
    h = h + division(p, 1080);
    dhp[1] = h % 24;
    dhp[0] = d + division(h, 24);
    return dhp;
}

function elapsed_time19(year) {
    var t = new Array(29, 12, 793);
    var n19 = year % 19;
    var moladot = division((n19 * 12.36 + 0.77), 1);
    var result = dhp_mult(moladot, t);
    return result;
}

function elapsed_time235(year) {
    var t = new Array(6939, 16, 595);
    var moladot = division(year, 19);
    var result = dhp_mult(moladot, t);
    return result;
}

function molad(year) {
    var t0 = new Array(118, 7, 695);
    var t1 = elapsed_time235(year);
    var t2 = elapsed_time19(year);
    var t3 = dhp_add(t1, t2);
    var result = dhp_add(t0, t3);
    result[0] = result[0] - 500;
    return result;
}

function new_year(year) {
    var molad_tishri_dhp = molad(year);
    var result = molad_tishri_dhp[0];
    if (molad_tishri_dhp[1] >= 18) {
        result++
    }
    ;
    var dow = result % 7;
    if ((dow == 1) || (dow == 4) || (dow == 6)) {
        result++
    }
    ;
    var time_hp = molad_tishri_dhp[1] * 10000 + 1 * molad_tishri_dhp[2];
    var dow2 = molad_tishri_dhp[0] % 7;
    if ((common_year(year)) && (dow2 == 3) && (time_hp >= 90204) && (time_hp < 180000)) {
        result = 1 * molad_tishri_dhp[0] + 2
    }
    ;
    if ((year_after_leap_year(year)) && (dow2 == 2) && (time_hp >= 150589) && (time_hp < 180000)) {
        result = 1 * molad_tishri_dhp[0] + 1
    }
    ;
    result = 1 * result + 347996;
    return result;
}

function adjusted_month_from_month(m) {
    var result = ((1 * m + 9) % 12) + 3;
    return result;
}

function adjusted_year_from_year(y, m) {
    var result = y - 1 + division((1 * m + 7), 10);
    return result
}

function year_from_adjusted_year(j_strich, m_strich) {
    var result = j_strich + division(m_strich, 13);
    return result;
}

function month_from_adjusted_month(m_strich) {
    var result = ((m_strich + 11) % 12) + 1;
    return result;
}

function jewish_dmy_from_jd(jd) {
    var approx_year = division((jd - 347998), 365.25);
    approx_year += 2;
    while (new_year(approx_year) > jd) {
        approx_year--
    }
    ;
    var year_length = new_year(approx_year * 1 + 1) - new_year(approx_year);
    var d = jd - new_year(approx_year);
    d++;
    var ix = index_from_length(year_length);
    var m = 0;
    while ((jewish_month_days[ix][m]) < d) {
        m++
    }
    ;
    m--;
    if ((jewish_month_days[ix][m] == 0) && (m > 0)) {
        m--
    }
    ;
    var result = new Array(d - jewish_month_days[ix][m], m, approx_year);
    return result;
}

function jd_from_jewish(d, m, y) {
    if ((common_year(y)) && (m == 6)) {
        m = 5
    }
    ;
    var ny0 = new_year(y);
    y++;
    var ny1 = new_year(y);
    var ix = index_from_length(ny1 - ny0);
    var jd = ny0 * 1 + jewish_month_days[ix][m] * 1 + d * 1 - 1;
    return jd;
}

function greg_dmy_from_jd(jd) {
    var a = 1 * jd + 32044;
    var b = division(a, 146097);
    var c = a % 146097;
    var d = Math.min(3, division(c, 36524));
    var e = c - 36524 * d;
    var f = division(e, 1461);
    var g = e % 1461;
    var h = Math.min(3, division(g, 365));
    var k = g - 365 * h;
    var l = division((111 * k + 41), 3395);
    var t = k - 30 * l - division((7 * l + 7), 12) + 1;
    var m_strich = l + 3;
    var j_strich = 400 * b + 100 * d + 4 * f + 1 * h - 4800;
    var m = month_from_adjusted_month(m_strich);
    var j = year_from_adjusted_year(j_strich, m_strich);
    var result = Array(t, m, j);
    return result;
}

function jd_from_greg(d, m_strich, y_strich) {
    var y4800 = y_strich + 4800;
    var a = division(y4800, 100);
    var b = y4800 % 100;
    var result = 146097 * division(a, 4) + 36524 * (a % 4) + 1461 * division(b, 4) + 365 * (b % 4) + division((7 * (m_strich - 2)), 12) + 30 * m_strich + 1 * d + 1721029 - 1753164;
    return result;
}

function jd_from_greg_dmy(dmy) {
    return jd_from_greg(Math.abs(dmy[0]), adjusted_month_from_month(dmy[1]), adjusted_year_from_year(Math.abs(dmy[2]), dmy[1]));
}

function jewish_dmy_from_greg_dmy(greg_day, greg_month, greg_year) {
    var jd = jd_from_greg(Math.abs(greg_day), adjusted_month_from_month(greg_month), adjusted_year_from_year(Math.abs(greg_year), greg_month));
    var jewish_dmy = jewish_dmy_from_jd(jd);
    return jewish_dmy;
}

function jewish_str(dmy) {
    var str = dmy[0] + " ";
    if (!common_year(dmy[2])) {
        str += jewish_month[dmy[1]];
        if (dmy[1] == 5)str += " I";
    }
    else {
        if (dmy[1] == 6) {
            str += jewish_month[5];
        }
        else {
            str += jewish_month[dmy[1]];
        }
    }
    str += " " + dmy[2];
    return str;
}

function greg_str(dmy) {
    return dmy[0] + " " + gregorian_month[dmy[1]] + " " + dmy[2];
}

function get_yahrzeits_table(jd, to_year) {
    var count = 20;
    var to_year = to_year || false;
    jew_dmy = jewish_dmy_from_jd(jd);
    greg_dmy = greg_dmy_from_jd(jd);
    var html = '<table><tr><th width="130px">Jewish date</th><th>Gregorian date</th></tr>';

    if(to_year) {
        while(greg_dmy[2] <= to_year) {
            html += '<tr><td>' + jewish_str(jew_dmy) + '</td><td>' + greg_str(greg_dmy) + '</td></tr>';
            jew_dmy[2]++;
            greg_dmy = greg_dmy_from_jd(jd_from_jewish(jew_dmy[0], jew_dmy[1], jew_dmy[2]));
        }
    } else {
        for (i = 0; i < count; i++) {
            html += '<tr><td>' + jewish_str(jew_dmy) + '</td><td>' + greg_str(greg_dmy) + '</td></tr>';
            jew_dmy[2]++;
            greg_dmy = greg_dmy_from_jd(jd_from_jewish(jew_dmy[0], jew_dmy[1], jew_dmy[2]));
        }
    }

    html += '</table>';
    return html;
}

function calculate_yahrzeit(el, dest_id, to_year) {
    var jew_dmy;
    var frm = el.form;
    var greg_dmy = new Array();
    var dest = document.getElementById(dest_id);
    greg_dmy[0] = frm.greg_day.options[frm.greg_day.selectedIndex].value;
    greg_dmy[1] = frm.greg_month.options[frm.greg_month.selectedIndex].value;
    greg_dmy[2] = frm.greg_year.options[frm.greg_year.selectedIndex].value;
    var jd = jd_from_greg_dmy(greg_dmy);
    if (frm.after_sunset.checked) {
        jd++;
    }
    dest.innerHTML = get_yahrzeits_table(jd, to_year);
}

function generate_year_options(from, to) {
    var html = "";
    for (i = from; i < to; i++) {
        html += '<option value="' + i + '">' + i + '</option>';
    }
    html += '<option value="' + to + '" selected>' + to + '</option>';
    return html;
}

function generate_month_options() {
    var html = "";
    var date = new Date();
    var month = date.getMonth() + 1;
    for (i = 1; i <= 12; i++) {
        html += '<option value="' + i + '"';
        if (i == month) {
            html += " selected";
        }
        html += '>' + gregorian_month[i] + '</option>';
    }
    return html;
}

function generate_day_options() {
    var html = "";
    var date = new Date();
    var day = date.getDay() + 1;
    for (i = 1; i <= 31; i++) {
        html += '<option value="' + i + '"';
        if (i == day) {
            html += " selected";
        }
        html += '>' + i + '</option>';
    }
    return html;
}