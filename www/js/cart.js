function cart(product_id) {
	$("cart").innerHTML = "loading...";
	var elem = $("p"+product_id),
        cart_count = Math.abs(parseInt($("cart-count-"+product_id).value));
    new Ajax.Updater('cart', '/ajax_plg_products.php', {parameters: "action=cart_add&id="+product_id+"&cart_count="+cart_count});
	if(elem) {
		var count = parseInt(elem.getAttribute("count"));
		if(count==0) {
			$("r"+product_id).style.display = "block";
		}
        count+=cart_count;
		elem.setAttribute("count", count);
		elem.innerHTML = count+" item(s) in cart";
	}
}

function cart_remove(product_id) {
	$("cart").innerHTML = "loading...";
	var elem = $("p"+product_id),
        cart_count = Math.abs(parseInt($("cart-count-"+product_id).value));
    new Ajax.Updater('cart', '/ajax_plg_products.php', {parameters: "action=cart_remove_inplace&id="+product_id+"&cart_count="+cart_count});
	if(elem) {
		var count = elem.getAttribute("count");
		if(count>0) {
			count-=cart_count;
			elem.setAttribute("count", count);
			if(count>0) {
				elem.innerHTML = count+" item(s) in cart";
			}
			else {
				$("r"+product_id).style.display = "none";	
				elem.innerHTML = "";
			}
		}
	}	
}
