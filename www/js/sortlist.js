
jQuery(function() {
  $("#scrapbook-images-list").sortable({
    update : function () {
      var order = $('#scrapbook-images-list').sortable('serialize');
      $.get('scrapbookOrder', order);
    }
  });
});
