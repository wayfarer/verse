<?php
// backend MX records display for the domains
include("inc/verse.inc.php"); //main header - initializes Verse environment

if ($user->logged()) {
    // pass only global admin to domain list
    if ($user->data["domain_id"] == 0) {
        $action = @$_POST["action"];
        switch ($action) {
            case "list":
                $rep = createobject("report_ajax", array($db, "sms_domain", REP_WITH_PAGENATOR, true, 50, 'width="500px" style="background-color: #DDEEFF"'));
                $rep->add_field("id", REP_STRING_TEMPLATE, '{domain_id}', REP_ORDERABLE, 'align="center"');
                $rep->add_field("name", REP_STRING_TEMPLATE, href("{domain_name}", "http://{domain_name|1}", 'target="_blank"'), REP_ORDERABLE, 'align="center"');
                $rep->add_field("", REP_FUNCTIONCALL_TEMPLATE, "mx({domain_name})", REP_UNORDERABLE, 'align="center" width="10%"');

                $rep->add_filter("", REP_INVISIBLE_FILTER, "mode", "=", 0);

                $rep->html_attributes('width="500px"');
                if (count($_POST) < 3) {
                    $rep->order_by("domain_name", 0);
                }
                $rep->handle_events($_POST);
                $html = $rep->make_report();
                echo $html;
                break;
            default: // show main content page by default
                $smarty->display("mx.tpl");
        }
    }
}
else {
    header("Location: login.php");
}

function mx($domain_name) {
    $mxhosts = array();
    if (getmxrr($domain_name, $mxhosts)) {
        return implode(", ", $mxhosts);
    }
    else {
        return "MX records not found";
    }
}
