<?php
 // backend candles moderation queue
include("inc/verse.inc.php"); //main header - initializes Verse environment

if ($user->have_role(ROLE_OBITUARIES_MANAGEMENT)) {
    $smarty->display("candles_moderation.tpl");
}
else {
    header("Location: login.php");
}