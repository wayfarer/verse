<?php
 // sublists management, for chinesecasketdistributor.com
include("inc/verse.inc.php"); //main header - initializes Verse environment

if ($user->have_role(7)) { // create product sublists
    if (!isset($_GET["print"])) {
        $user_id = $user->data["user_id"];
        $query = "SELECT hash FROM plg_product_mylist WHERE user_id='$user_id' AND domain_id='$domain_id'";
        $hash = $db->getOne($query);
        if (!$hash) {
            // create one if not exists
            $hash = substr(md5(uniqid(rand())), 0, 16);
            $query = "INSERT plg_product_mylist SET domain_id='$domain_id', user_id='$user_id', hash='$hash'";
            $db->query($query);
        }
        $smarty->assign("hash", $hash);
        $smarty->display("sublists.tpl");
    }
    else {
        if ((isset($_GET["l"]) && isset($_GET["p"])) || count($_POST)) {
            if (!count($_POST)) {
                $mylist_id = intval($_GET["l"]);
                $product_ids = array(intval($_GET["p"]));
            }
            else {
                $mylist_id = intval($_POST["l"]);
                $product_ids = $_POST["udc0"];
            }
            $query = "SELECT mi.price price, image, name, description FROM plg_product_mylist_item mi INNER JOIN plg_product_mylist m USING(mylist_id) INNER JOIN plg_product p ON(mi.product_id=p.product_id) WHERE mi.product_id IN (" . implode(",", $product_ids) . ") AND mi.mylist_id='$mylist_id' AND user_id='" . $user->data["user_id"] . "' AND m.domain_id='$domain_id'";
            $ret = $db->getAll($query, DB_FETCHMODE_ASSOC);
            $smarty->assign("products", $ret);
            $smarty->display("products_print.tpl");
        }
        else {
            $rep = createobject("report_ajax", array($db, "plg_product_mylist_item", REP_NO_PAGENATOR, true, 0, 'width="700px" style="background-color: #DDEEFF"'));
            $rep->add_table("plg_product_mylist", REP_INNER_JOIN, "mylist_id");
            $rep->add_table("plg_product", REP_INNER_JOIN, "product_id", 1);
            $rep->add_table("plg_product_category", REP_INNER_JOIN, "category_id");

            $rep->add_field("Product", REP_STRING_TEMPLATE, '{name|3}', REP_UNORDERABLE, 'align="center"');
            $rep->add_field("Description", REP_STRING_TEMPLATE, '{description}', REP_UNORDERABLE, 'align="center"');
            $rep->add_field("Price", REP_STRING_TEMPLATE, '${price|1}', REP_UNORDERABLE, 'align="center" width="10%"');

            $rep->add_filter("", REP_INVISIBLE_FILTER, "t2.domain_id", "=", $domain_id);
            $rep->add_filter("", REP_INVISIBLE_FILTER, "t2.user_id", "=", $user->data["user_id"]);

            $rep->html_attributes('width="700px"');

            $html = $rep->make_report();

            $html .= '<script>window.print();</script>';
            echo $html;
        }
    }
}
else {
    header("Location: login.php");
}