<?php
 // backend structure for UC (under construction) sites
include("inc/verse.inc.php"); //main header - initializes Verse environment

if ($user->have_role(ROLE_CONTENT_MANAGEMENT)) {
    $action = @$_POST["action"];
    switch ($action) {
        case "create_splash":
            $extra = intval($_POST["extra"]);
            // create splash (home) page
            // load splash page template
            $splash_template = file_get_contents("templates/helpers/splash.html");
            $splash_template2 = file_get_contents("templates/helpers/splash2.html");
            $p["page_type"] = 2; // email-form page
            $p["name"] = "home";
            $p["no_header"] = 1;
            $p["data"] = in(serialize(array("content1" => $splash_template, "content2" => $splash_template2)));
            $p["properties"] = serialize(array("title" => "Home"));
            $p["domain_id"] = $domain_id;
            $p["ord"] = 0;
            $query = "INSERT cms_page_uc SET " . make_set_clause($p);
            $db->query($query);
            if ($extra) {
                // create obituary_search page
                $splash_template = file_get_contents("templates/helpers/obituaries.html");
                $p["page_type"] = 3;
                $p["name"] = "obituaries";
                $p["no_header"] = 0;
                $p["data"] = in(serialize(array("content1" => $splash_template, "content2" => "")));
                $p["properties"] = serialize(array("title" => "Obituaries"));
                $p["ord"] = 1;
                $query = "INSERT cms_page_uc SET " . make_set_clause($p);
                $ret = $db->query($query);
                $logger->info(print_r($ret, 1));

                // create obituary_view page
                $p["page_type"] = 4;
                $p["name"] = "obituary_view";
                $p["no_header"] = 0;
                $p["data"] = in(serialize(array("content1" => "{obituary_view style=\"style2\"}", "content2" => "")));
                $p["properties"] = serialize(array("title" => ""));
                $p["ord"] = 2;
                $query = "INSERT cms_page_uc SET " . make_set_clause($p);
                $db->query($query);
                // create calendar page
                $splash_template = file_get_contents("templates/helpers/calendar.html");
                $p["page_type"] = 5;
                $p["name"] = "calendar";
                $p["no_header"] = 0;
                $p["data"] = in(serialize(array("content1" => $splash_template, "content2" => "")));
                $p["properties"] = serialize(array("title" => "Service Calendar"));
                $p["ord"] = 3;
                $query = "INSERT cms_page_uc SET " . make_set_clause($p);
                $db->query($query);
                // create directions page
                $p["page_type"] = 1;
                $p["name"] = "directions";
                $p["no_header"] = 0;
                $p["data"] = in(serialize(array("content1" => "insert directions here", "content2" => "")));
                $p["properties"] = serialize(array("title" => "Directions"));
                $p["ord"] = 4;
                $query = "INSERT cms_page_uc SET " . make_set_clause($p);
                $db->query($query);
                // create contact us page
                $splash_template = file_get_contents("templates/helpers/contact_us.html");
                $p["page_type"] = 2; // mailing form
                $p["name"] = "contact_us";
                $p["no_header"] = 0;
                $p["data"] = in(serialize(array("content1" => $splash_template, "content2" => "")));
                $p["properties"] = serialize(array("title" => "Contact Us"));
                $p["ord"] = 5;
                $query = "INSERT cms_page_uc SET " . make_set_clause($p);
                $db->query($query);
            }
        // pass to list_pages
        case "list_pages":
            $query = "SELECT page_id, page_type type_id, name FROM cms_page_uc WHERE domain_id='$domain_id' ORDER BY ord";
            $arr = $db->getAll($query, DB_FETCHMODE_ASSOC);
//      uasort($arr, "cmp_pages");

            $smarty->assign("data", $arr);
            $html = $smarty->fetch("ajax_content_pages.tpl");

            echo $html;
            break;

        case "load_site_general":
            $query = "SELECT title, properties FROM cms_site_uc WHERE domain_id='$domain_id'";
            $ret = $db->getRow($query, DB_FETCHMODE_ASSOC);
            $properties = @unserialize($ret["properties"]);
            $properties["title"] = $ret["title"];
            header("X-JSON:" . make_json_response($properties));
            break;
        case "load_page_props":
            $page_id = intval(@$_POST["id"]);
            $query = "SELECT name int_name, page_type, no_header, properties FROM cms_page_uc WHERE page_id='$page_id' AND domain_id='$domain_id'";
            $ret = $db->getRow($query, DB_FETCHMODE_ASSOC);
            $props = @unserialize($ret["properties"]);
            if (is_array($props)) {
                $ret = array_merge($ret, $props);
            }
            header("X-JSON:" . make_json_response($ret));
            break;
        case "load_page":
            $page_id = intval(@$_POST["id"]);
            $query = "SELECT data FROM cms_page_uc WHERE page_id='$page_id' AND domain_id='$domain_id'";
            $ret = $db->getOne($query);
            $content = @unserialize($ret);
            echo @make_json_response($content);
            break;
        case "load_site_header":
            $query = "SELECT header_mode, header FROM cms_site_uc WHERE domain_id='$domain_id'";
            $ret = $db->getRow($query, DB_FETCHMODE_ASSOC);
            if ($ret["header_mode"]) {
                $ret["header_html"] = $ret["header"];
            }
            else {
                $ret["header_image"] = $ret["header"];
            }
            unset($ret["header"]);
            header("X-JSON:" . make_json_response($ret));
            break;
        case "load_site_footer":
            $query = "SELECT footer FROM cms_site_uc WHERE domain_id='$domain_id'";
            $ret = $db->getRow($query, DB_FETCHMODE_ASSOC);
            header("X-JSON:" . make_json_response($ret));
            break;
        case "load_site_css":
            $query = "SELECT css FROM cms_site_uc WHERE domain_id='$domain_id'";
            $ret = $db->getRow($query, DB_FETCHMODE_ASSOC);
            header("X-JSON:" . make_json_response($ret));
            break;
        case "load_site_css_frontend":
            $query = "SELECT css FROM cms_site_uc WHERE domain_id='$domain_id'";
            $ret = $db->getOne($query);
            echo $ret;
            break;

        case "update_pages_ord":
            $ids_order = explode(";", @$_POST["ord"]);
            foreach ($ids_order as $ord_key => $id) {
                $query = "UPDATE cms_page_uc SET ord='$ord_key' WHERE page_id='$id' AND domain_id='$domain_id'";
                $db->query($query);
            }
            break;

        case "save_site_general":
            // fetch prev properties
            $query = "SELECT properties FROM cms_site_uc WHERE domain_id='$domain_id'";
            $ret = $db->getRow($query, DB_FETCHMODE_ASSOC);
            $p = @unserialize($ret["properties"]);
            //title, width, layout, align, bgcolor, bgimage
            $title = in(@$_POST["title"]);
            $p["width"] = substr(@$_POST["width"], 0, 8);
            $p["layout_id"] = intval(@$_POST["layout_id"]);
            $p["align"] = intval(@$_POST["align"]);
            $p["bgcolor"] = substr(@$_POST["bgcolor"], 0, 8);
            $p["bgimage"] = substr(@$_POST["bgimage"], 0, 255);
            if (!$p["layout_id"]) $p["layout_id"] = 1;
            if (!$p["align"]) $p["align"] = 1;
            $p["description"] = @$_POST["description"];
            $p["keywords"] = @$_POST["keywords"];
            $p["headhtml"] = @$_POST["headhtml"];
            $properties = in(serialize($p));
            $query = "UPDATE cms_site_uc SET title='$title', properties='$properties' WHERE domain_id='$domain_id'";
            $db->query($query);
            break;
        case "save_page_props":
            $page_id = intval(@$_POST["id"]);
            $int_name = in($_POST["int_name"]);
            $page_type = intval(@$_POST["page_type"]);
            $no_header = @$_POST["no_header"] ? 1 : 0;
            $p["title"] = @$_POST["title"];
            $p["description"] = @$_POST["description"];
            $p["desc_no_global"] = @$_POST["desc_no_global"] ? 1 : 0;
            $p["keywords"] = @$_POST["keywords"];
            $p["keywords_no_global"] = @$_POST["keywords_no_global"] ? 1 : 0;
            $properties = in(serialize($p));
            if ($page_id) {
                $query = "UPDATE cms_page_uc SET name='$int_name', page_type='$page_type', no_header='$no_header', properties='$properties' WHERE page_id='$page_id' AND domain_id='$domain_id'";
            }
            else {
                // insert with max ord, at the end of page list
                $query = "SELECT max(ord) FROM cms_page_uc WHERE domain_id='$domain_id'";
                $ord = $db->getOne($query) + 1;
                $query = "INSERT cms_page_uc SET name='$int_name', page_type='$page_type', no_header='$no_header', properties='$properties', domain_id='$domain_id', ord='$ord'";
            }
            $db->query($query);
            break;
        case "save_page":
            $page_id = intval(@$_POST["id"]);
            $content["content1"] = @$_POST["content1"];
            $content["content2"] = @$_POST["content2"];
            if ($content["content1"] || $content["content2"]) {
                $cnt = in(serialize($content));
                $query = "UPDATE cms_page_uc SET data='$cnt' WHERE page_id='$page_id' AND domain_id='$domain_id'";
                $ret = $db->query($query);
                $affected = $db->affectedRows();
            }
            else {
                echo "empty content - not saved";
            }
            echo "ok";
            break;
        case "save_site_header":
            $p["header_mode"] = intval(@$_POST["header_mode"]);
            if ($p["header_mode"]) {
                $p["header"] = in(@$_POST["header_html"]);
            }
            else {
                $p["header"] = in(@$_POST["header_image"]);
            }
            $query = "UPDATE cms_site_uc SET " . make_set_clause($p) . " WHERE domain_id='$domain_id'";
            $db->query($query);
            break;
        case "save_site_footer":
            $p["footer"] = in(@$_POST["footer"]);
            $query = "UPDATE cms_site_uc SET " . make_set_clause($p) . " WHERE domain_id='$domain_id'";
            $db->query($query);
            break;
        case "save_site_css":
            $p["css"] = in(@$_POST["css"]);
            $query = "UPDATE cms_site_uc SET " . make_set_clause($p) . " WHERE domain_id='$domain_id'";
            $db->query($query);
            break;

        case "delete_page":
            $page_id = intval(@$_POST["id"]);
            $query = "DELETE FROM cms_page_uc WHERE page_id='$page_id' AND domain_id='$domain_id'";
            $db->query($query);
            break;

        default:
            // check if there is cms_page_uc entry, make default if no
            $query = "SELECT 1 FROM cms_page_uc WHERE domain_id='$domain_id'";
            $ret = $db->getOne($query);
            if (!$ret) {
                // create default cms_page_uc entry
                $p["title"] = "Site is under construction";
                $p["header_mode"] = 1; // html
                $p["header"] = "Site is under construction";
                $p["properties"] = serialize(array("width" => "620px", "align" => "2"));
                $p["domain_id"] = $domain_id;
                $query = "INSERT cms_site_uc SET " . make_set_clause($p);
                $db->query($query);
            }

            // get page types
            $query = "SELECT page_type_id page_type, page_type_name FROM cms_page_type";
            $page_types = $db->getAssoc($query);

            $smarty->assign("page_types", $page_types);
            $smarty->display("structure_uc.tpl");
    }
}
else {
    header("Location: login.php");
}

/* function cmp_pages($a, $b) {
	if($a["name"] == $b["name"]) return 0;
	if($a["name"] == "home") return -1;
	if($b["name"] == "home") return 1;
	return ($a["name"] < $b["name"])?-1:1;
}  */