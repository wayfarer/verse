<?php
 // obsolete
// main backend entry point
$pass_unknown_domain = true;
include("inc/verse.inc.php"); //main header - initializes Verse environment

if (!$user->logged()) {
    header("Location: backend.php");
}
elseif ($user->data["domain_id"] == 0) {
    // pass global admin to domains list
    if ($domain_id == 1) {
        // goto domain list only on ws1.twintierstech.net
        header("Location: domains.php");
    }
    else {
        header("Location: structure.php");
    }
}
    // else choose accessable area for the user and pass there
elseif ($user->have_role(ROLE_CONTENT_MANAGEMENT)) {
    header("Location: structure.php");
}
elseif ($user->have_role(ROLE_USER_MANAGEMENT)) {
    header("Location: backend.php/sf_guard_user");
}
elseif ($user->have_role(ROLE_OBITUARIES_MANAGEMENT)) {
    header("Location: backend.php/obituaries");
}
elseif ($user->have_role(ROLE_PRODUCTS_MANAGEMENT)) {
    header("Location: backend.php/products");
}
elseif ($user->have_role(ROLE_SUBLISTS_MANAGEMENT)) {
    header("Location: sublists.php");
}
elseif ($user->have_role(ROLE_MOVIECLIPS_MANAGEMENT)) {
    header("Location: backend.php/movieclips");
}
elseif ($user->have_role(ROLE_MEMBERS_MANAGEMENT)) {
    header("Location: backend.php/members");
}
elseif ($user->have_role(ROLE_STATISTICS_MANAGEMENT)) {
    header('Location: backend.php/statistics');
}
elseif ($user->have_role(ROLE_MAILBOX)) {
    header("Location: webmail");
}
else {
    echo "You have no interactive privileges";
}
