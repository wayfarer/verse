<?php
 // obsolete
// backend integration with phpMV
/**
 * routes to phpMyVisites site
 * automatically prelogs in there based on FuneralSMS current user
 */

//	$pass_unknown_domain = true;
include('inc/verse.inc.php'); //main header - initializes Verse environment

if (!$user->logged()) {
    header('Location: login.php');
}
elseif ($user->data['domain_id'] == 0) {
    header('Location: ' . PHPMV_PATH . '/funlogin.php?login=' . urlencode(PHPMV_SU_LOGIN) . '&password=' . urlencode(PHPMV_SU_PASSWORD));
}
elseif ($user->have_role(ROLE_STATISTICS_MANAGEMENT)) {
    $query = "SELECT phpmv_login as login,phpmv_password as password FROM sms_user_fsms2phpmv WHERE fsms_uid=" . $user->data['user_id'];
    $res = $db->query($query);
    $obj = $res->fetchRow(DB_FETCHMODE_OBJECT);
    if ($obj->login) {
        // count stats visits
        $query = "UPDATE sms_stats_visits SET visits=visits+1, last_visit=now() WHERE domain_id='$domain_id' AND stat_system='phpmv'";
        $db->query($query);
        if (!$db->affectedRows()) {
            $query = "INSERT sms_stats_visits SET domain_id='$domain_id', stat_system='phpmv', visits=1, last_visit=now()";
            $db->query($query);
        }

        header('Location: ' . PHPMV_PATH . '/funlogin.php?login=' . urlencode($obj->login) . '&password=' . urlencode($obj->password));
    }
}
else {
    print 'An error occured. If you are sure you should have had access to the page contact to the system admin.';
}
