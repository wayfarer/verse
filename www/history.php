<?php
// backend page history
include("inc/verse.inc.php"); //main header - initializes Verse environment

if ($user->have_role(ROLE_CONTENT_MANAGEMENT)) {
    // $_POST = utf8_to_latin($_POST);
    $action = @$_POST["action"];
    switch ($action) {
        case "list": // ajax handlers
            $rep = createobject("report_ajax", array($db, "cms_page_history", REP_WITH_PAGENATOR, true, 50, 'width="700px" style="background-color: #DDEEFF"'));
            $rep->add_table("cms_page", REP_INNER_JOIN, "page_id");
            $rep->add_table("sf_guard_user", REP_INNER_JOIN, array("user_id", "id"), 1);

            $rep->add_field("page", REP_STRING_TEMPLATE, '{internal_name}', REP_ORDERABLE, 'align="center"');
            $rep->add_field("time", REP_FUNCTIONCALL_TEMPLATE, 'format_date({timestamp})', REP_ORDERABLE, 'align="center"');
            $rep->add_field("action", REP_CALLBACK, 'action({action})', REP_UNORDERABLE, 'align="center"');
            $rep->add_field("user", REP_CALLBACK, 'username({username})', REP_ORDERABLE, 'align="center"');
            $rep->add_field("", REP_STRING_TEMPLATE, href_js_action("view content", "view({page_history_id})"), REP_UNORDERABLE, 'align="center"');
            $rep->add_field("", REP_STRING_TEMPLATE, href_js_action("restore", "restore({page_history_id})"), REP_UNORDERABLE, 'align="center"');
            $rep->add_field("", REP_STRING_TEMPLATE, href_js_action("delete", "del({page_history_id})"), REP_UNORDERABLE, 'align="center"');
            $rep->add_filter("", REP_INVISIBLE_FILTER, "t2.domain_id", "=", $domain_id);

            $rep->html_attributes('width="700px"');

            if (count($_POST) < 3) {
                $rep->order_by("timestamp", 1);
            }

            $rep->handle_events($_POST);
            $html = $rep->make_report();
            echo $html;
            break;
        case "load":
            $page_history_id = intval(@$_POST["id"]);
            $query = "SELECT content FROM cms_page_history WHERE page_history_id='$page_history_id'";
            $ret = $db->getOne($query);
            $content = @unserialize($ret);
            echo @make_json_response($content);
            break;
        case "restore":
            $page_history_id = intval(@$_POST["id"]);
            $query = "SELECT page_id, content FROM cms_page_history WHERE page_history_id='$page_history_id'";
            $page_history = $db->getRow($query, DB_FETCHMODE_ASSOC);
            // restore page content
            $query = "UPDATE cms_page SET data='" . in($page_history["content"]) . "' WHERE page_id='" . $page_history["page_id"] . "' AND domain_id='$domain_id'";
            $ret = $db->query($query);
            echo $query . print_r($ret, true);
            // insert history element
            $p["page_id"] = $page_history["page_id"];
            $p["user_id"] = $user->data["user_id"];
            $p["action"] = 3; // page restore
            $p["timestamp"] = time();
            $p["content"] = in($page_history["content"]);
            $query = "INSERT cms_page_history SET " . make_set_clause($p);
            $db->query($query);
            break;
        case "delete":
            $page_history_id = intval(@$_POST["id"]);
            // check for page_history is of current domain
            $query = "SELECT 1 FROM cms_page_history INNER JOIN cms_page USING(page_id) WHERE page_history_id='$page_history_id' AND domain_id='$domain_id'";
            if ($db->getOne($query)) {
                $query = "DELETE FROM cms_page_history WHERE page_history_id='$page_history_id'";
                $db->query($query);
            }
            break;
        default: // show main content page by default
            $smarty->display("history.tpl");
    }
}
else {
    header("Location: login.php");
}

function format_date($timestamp) {
    if ($timestamp) {
        return date("Y-m-d H:i:s", $timestamp);
    }
    else {
        return "unknown";
    }
}

function action($action) {
    $ret = "";
    switch ($action) {
        case 0:
            $ret = "create";
            break;
        case 1:
            $ret = "edit";
            break;
        case 2:
            $ret = "delete";
            break;
        case 3:
            $ret = "restore";
            break;
    }
    return $ret;
}

function username($username) {
    // BC
    if ($username == "WayFarer") $username = "admin";
    return $username;
}