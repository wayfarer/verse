<?php
 // structure management, ajax actions
include("inc/verse.inc.php"); //main header - initializes Verse environment

if ($user->have_role(ROLE_CONTENT_MANAGEMENT)) {
    //	$_POST = utf8_to_latin($_POST);
    $action = $_POST["action"];

    switch ($action) {
        case "list":
            $query = "SELECT node_id, display_name title, depth, n.page_id, p.internal_name page_name, menu_type, params, p.domain_id FROM cms_node n LEFT JOIN cms_page p USING(page_id) WHERE n.domain_id='$domain_id' ORDER BY n.ord";
            $ret = $db->getAll($query, DB_FETCHMODE_ASSOC);
            foreach ($ret as $i => $line) {
                $params = unserialize(@$line["params"]);
                if ($params) {
                    $ret[$i] = array_merge($ret[$i], $params);
                }
                unset($ret[$i]["params"]);
            }
            echo make_json_response($ret);
//			echo "domain_id = $domain_id";

//		    $smarty->assign("data", $arr);
//		    $html = $smarty->fetch("ajax_structure.tpl");
//		    echo $html;
            break;
        case "load_node":
            $node_id = intval(@$_POST["id"]);
            $query = "SELECT internal_name int_name, display_name, menu_type, params FROM cms_node WHERE node_id='$node_id' AND domain_id='$domain_id'";
            $ret = $db->getRow($query, DB_FETCHMODE_ASSOC);
            $params = @unserialize($ret["params"]);
            unset($ret["params"]);
            header("X-JSON:" . make_json_response(array_merge($ret, $params)));
            break;
        case "list_pages":
            //if domain in domain group with shared pages
            if ($_SESSION['is_shared_pages']) {
                $query = "SELECT DISTINCT p.page_id, p.page_type type_id, p.internal_name name, p.domain_id, d.domain_name FROM cms_page p JOIN sms_domain d USING(domain_id) WHERE p.domain_id = '$domain_id' OR p.page_id IN (SELECT page_id FROM sms_domain_link_page JOIN sms_domain_link l USING(group_id) WHERE l.domain_id = '$domain_id' AND l.group_id IN (".implode(', ', $_SESSION['group_ids']).")) ORDER BY p.domain_id, name";
            } else {
                //$query = "SELECT page_id, page_type type_id, internal_name name FROM cms_page WHERE domain_id='$domain_id' ORDER BY ord";
                $query = "SELECT page_id, page_type type_id, internal_name name, domain_id FROM cms_page WHERE domain_id='$domain_id' ORDER BY name";
            }

            $arr = $db->getAll($query, DB_FETCHMODE_ASSOC);
            uasort($arr, "cmp_pages");

            $smarty->assign("classes", $_SESSION['classes']);
            $smarty->assign("data", $arr);
            $html = $smarty->fetch("ajax_content_pages.tpl");

            echo $html;
            break;
        case "load_content_props":
            $page_id = intval(@$_POST["id"]);
            if ($_SESSION['is_shared_pages']) {
                $query = "SELECT internal_name int_name, page_type, no_header, restricted, properties, custom_header FROM cms_page WHERE page_id='$page_id' AND (domain_id='$domain_id' OR page_id IN (SELECT page_id FROM sms_domain_link_page JOIN sms_domain_link l USING(group_id) WHERE l.domain_id = '$domain_id' AND l.group_id IN (".implode(', ', $_SESSION['group_ids']).")))";
            } else {
                $query = "SELECT internal_name int_name, page_type, no_header, restricted, properties, custom_header FROM cms_page WHERE page_id='$page_id' AND domain_id='$domain_id'";
            }
            $ret = $db->getRow($query, DB_FETCHMODE_ASSOC);
            $props = @unserialize($ret["properties"]);
            if (is_array($props)) {
                $ret = array_merge($ret, $props);
            }
            $c = @unserialize($ret["custom_header"]);
            if (is_array($c)) {
                $ret = array_merge($ret, $c);
            }
            header("X-JSON:" . make_json_response($ret));
            break;
        case "load_page":
            $page_id = intval(@$_POST["id"]);
            if ($_SESSION['is_shared_pages']) {
                $query = "SELECT data FROM cms_page WHERE page_id='$page_id' AND (domain_id='$domain_id' OR page_id IN (SELECT page_id FROM sms_domain_link_page JOIN sms_domain_link l USING(group_id) WHERE l.domain_id = '$domain_id' AND l.group_id IN (".implode(', ', $_SESSION['group_ids']).")))";
            } else {
                $query = "SELECT data FROM cms_page WHERE page_id='$page_id' AND domain_id='$domain_id'";
            }
            $ret = $db->getOne($query);
//      var_dump($ret);
            $content = @unserialize($ret);
//      var_dump($content);
            echo @make_json_response($content);
            break;
        case "load_site_general":
            $query = "SELECT title, properties FROM cms_site WHERE domain_id='$domain_id'";
            $ret = $db->getRow($query, DB_FETCHMODE_ASSOC);
            $properties = @unserialize($ret["properties"]);
            $properties["title"] = $ret["title"];
            header("X-JSON:" . make_json_response($properties));
            break;
        case "load_layouts":
            $query = "SELECT layout_id, layout_name FROM cms_layout";
            $layouts = $db->getAssoc($query);
            header("X-JSON:" . make_json_response($layouts));
            break;
        case "load_site_header":
            $query = "SELECT header_mode, header FROM cms_site WHERE domain_id='$domain_id'";
            $ret = $db->getRow($query, DB_FETCHMODE_ASSOC);
            if ($ret["header_mode"]) {
                $ret["header_html"] = $ret["header"];
            }
            else {
                $ret["header_image"] = $ret["header"];
            }
            unset($ret["header"]);
            header("X-JSON:" . make_json_response($ret));
            break;
        case "load_site_footer":
            $query = "SELECT footer FROM cms_site WHERE domain_id='$domain_id'";
            $ret = $db->getRow($query, DB_FETCHMODE_ASSOC);
            header("X-JSON:" . make_json_response($ret));
            break;
        case "load_site_css":
            $query = "SELECT css FROM cms_site WHERE domain_id='$domain_id'";
            $ret = $db->getRow($query, DB_FETCHMODE_ASSOC);
            header("X-JSON:" . make_json_response($ret));
            break;
        case "load_site_css_frontend":
            $query = "SELECT css FROM cms_site WHERE domain_id='$domain_id'";
            $ret = $db->getOne($query);
            echo $ret;
            break;
        case "load_site_mainmenu":
            $query = "SELECT main_menu FROM cms_site WHERE domain_id='$domain_id'";
            $ret = $db->getOne($query);
            $main_menu = @unserialize($ret);
            header("X-JSON:" . @make_json_response($main_menu));
            break;
        case "load_site_popupmenu":
            $query = "SELECT popup_menu FROM cms_site WHERE domain_id='$domain_id'";
            $ret = $db->getOne($query);
            $main_menu = @unserialize($ret);
            header("X-JSON:" . @make_json_response($main_menu));
            break;
        case "load_site_music":
            $query = "SELECT properties FROM cms_site WHERE domain_id='$domain_id'";
            $ret = $db->getRow($query, DB_FETCHMODE_ASSOC);
            $properties = @unserialize($ret["properties"]);
            header("X-JSON:" . make_json_response($properties));
            break;

        case "update_pages_ord":
            $ids_order = explode(";", @$_POST["ord"]);
            foreach ($ids_order as $ord_key => $id) {
                $query = "UPDATE cms_page SET ord='$ord_key' WHERE page_id='$id' AND domain_id='$domain_id'";
                $db->query($query);
            }
            break;

        case "save":
            $data = @$_POST["data"];
            $nodes = explode("\n", $data);
            $c = 1;
            foreach ($nodes as $node) {
                if ($node) {
                    $node_props = explode("|", $node);
                    $p = array("image" => in($node_props[4]), "himage" => in($node_props[5]), "width" => in($node_props[6]), "height" => in($node_props[7]), "newwindow" => in($node_props[8]));
                    if ($node_props[0]) {
                        // if node exists
                        $query = "UPDATE cms_node SET ord='$c', depth='" . in($node_props[1]) . "', display_name='" . in($node_props[2]) . "', menu_type='" . in($node_props[3]) . "', params='" . serialize($p) . "' WHERE node_id='" . $node_props[0] . "' AND domain_id='$domain_id'";
                    }
                    else {
                        // create node
                        $query = "INSERT cms_node SET domain_id='$domain_id', ord='$c', depth='" . in($node_props[1]) . "', display_name='" . in($node_props[2]) . "', menu_type='" . in($node_props[3]) . "', params='" . serialize($p) . "'";
                    }
                    $db->query($query);
                    $c++;
                    echo $query;
                }
            }
            break;
        case "save_node":
            $node_id = intval(@$_POST["id"]);
            $int_name = in($_POST["int_name"]);
            $display_name = in($_POST["display_name"]);
            $menu_type = intval($_POST["menu_type"]);
            $p["image"] = in($_POST["image"]);
            $p["himage"] = in($_POST["himage"]);
            $p["width"] = in($_POST["width"]);
            $p["height"] = in($_POST["height"]);
            $p["newwindow"] = in($_POST["newwindow"]);
            if ($node_id) {
                $query = "UPDATE cms_node SET internal_name='$int_name', display_name='$display_name', menu_type='$menu_type', params='" . serialize($p) . "' WHERE node_id='$node_id' AND domain_id='$domain_id'";
                $db->query($query);
            }
            else {
                $query = "SELECT max(ord) FROM cms_node WHERE domain_id='$domain_id'";
                $ord = $db->getOne($query);
                $ord++;
                $query = "INSERT cms_node SET internal_name='$int_name', display_name='$display_name', menu_type='$menu_type', params='" . serialize($p) . "', domain_id='$domain_id', ord='$ord'";
                $db->query($query);
            }
            break;
        case "save_content_props":
            $page_id = intval(@$_POST["id"]);
            $int_name = in($_POST["int_name"]);
            $page_type = intval(@$_POST["page_type"]);
            $no_header = @$_POST["no_header"] ? 1 : 0;
            $restricted = @$_POST["restricted"] ? 1 : 0;
            $p = array();
            $p["title"] = @$_POST["title"];
            $p["description"] = @$_POST["description"];
            $p["desc_no_global"] = @$_POST["desc_no_global"] ? 1 : 0;
            $p["keywords"] = @$_POST["keywords"];
            $p["keywords_no_global"] = @$_POST["keywords_no_global"] ? 1 : 0;
            $p["no_leftmenu"] = @$_POST["no_leftmenu"] ? 1 : 0;
            $properties = in(serialize($p));
            $c = array();
            $c["custom_header"] = intval($_POST["custom_header"]);
            $c["custom_header_image"] = @$_POST["custom_header_image"];
            $c["custom_header_html"] = @$_POST["custom_header_html"];
            $custom_header = in(serialize($c));

            if ($page_id) {
                $query = "UPDATE cms_page SET internal_name='$int_name', page_type='$page_type', no_header='$no_header', restricted='$restricted', properties='$properties', custom_header='$custom_header' WHERE page_id='$page_id' AND domain_id='$domain_id'";
            }
            else {
                $query = "INSERT cms_page SET internal_name='$int_name', page_type='$page_type', no_header='$no_header', restricted='$restricted', properties='$properties', custom_header='$custom_header', domain_id='$domain_id'";
            }
            $db->query($query);
            echo "ok";
            break;
        case "save_content":
            $page_id = intval(@$_POST["id"]);
            $page_domain_id = $db->getOne("SELECT domain_id FROM cms_page WHERE page_id='$page_id'");

            if (!is_null($page_domain_id) && $domain_id == $page_domain_id) {
                // if there was no page history entries - make one with status "create"
                $query = "SELECT count(*) FROM cms_page_history WHERE page_id='$page_id'";
                $hist_cnt = $db->getOne($query);
                if (!$hist_cnt) {
                    $query = "SELECT data FROM cms_page WHERE page_id='$page_id' AND domain_id='$domain_id'";
                    $cnt = $db->getOne($query);
                    echo "old page: ";
                    var_dump($cnt);
                    if ($cnt) {
                        // make history entry
                        $p["page_id"] = $page_id;
                        $p["user_id"] = $user->data["user_id"];
                        $p["action"] = 0; // page create
                        $p["timestamp"] = 0; // 0 - unknown time
                        $p["content"] = $cnt;
                        $query = "INSERT cms_page_history SET " . make_set_clause($p);
                        $db->query($query);
                    }
                }

                $content["content1"] = @$_POST["content1"];
                $content["content2"] = @$_POST["content2"];

                // temp fixes: CKEditor ruined {} tags
                $content["content1"] = preg_replace_callback("/&lt;!--\{cke_protected\}(.*?)--&gt;/", _ckeditor_fix, $content["content1"]);

                if ($content["content1"] || $content["content2"]) {
                    //        var_dump($content);
                    $cnt = in(serialize($content));
                    $query = "UPDATE cms_page SET data='$cnt' WHERE page_id='$page_id' AND domain_id='$domain_id'";
                    //        echo $query;
                    $ret = $db->query($query);
                    $affected = $db->affectedRows();
                    // save history item if smth changed
                    if ($affected) {
                        $p["page_id"] = $page_id;
                        $p["user_id"] = $user->data["user_id"];
                        $p["action"] = 1; // page edit
                        $p["timestamp"] = time();
                        $p["content"] = $cnt;
                        $query = "INSERT cms_page_history SET " . make_set_clause($p);
                        $db->query($query);
                    }
                }
                else {
                    echo "empty content - not saved";
                }
            } else {
                echo "another domain - not saved";
            }
            echo "ok";
            break;
        case "set_node_content":
            $node_id = intval(@$_POST["node_id"]);
            $page_id = intval(@$_POST["page_id"]);
            $query = "UPDATE cms_node SET page_id='$page_id' WHERE node_id='$node_id' AND domain_id='$domain_id'";
            $db->query($query);
            echo "$query,ok";
            break;

        case "save_site_general":
            // fetch prev properties
            $query = "SELECT properties FROM cms_site WHERE domain_id='$domain_id'";
            $ret = $db->getRow($query, DB_FETCHMODE_ASSOC);
            $p = @unserialize($ret["properties"]);
//title, width, layout, align, bgcolor, bgimage			
            $title = in(@$_POST["title"]);
            $p["width"] = substr(@$_POST["width"], 0, 8);
            $p["layout_id"] = intval(@$_POST["layout_id"]);
            $p["align"] = intval(@$_POST["align"]);
            $p["bgcolor"] = substr(@$_POST["bgcolor"], 0, 8);
            $p["bgimage"] = substr(@$_POST["bgimage"], 0, 255);
            if (!$p["layout_id"]) $p["layout_id"] = 1;
            if (!$p["align"]) $p["align"] = 1;
//			$p["date_format"] = substr(@$_POST["date_format"],0,16);
            $p["description"] = @$_POST["description"];
            $p["keywords"] = @$_POST["keywords"];
            $p["headhtml"] = @$_POST["headhtml"];
            $properties = in(serialize($p));
            $query = "UPDATE cms_site SET title='$title', properties='$properties' WHERE domain_id='$domain_id'";
            $db->query($query);
//			echo "$query,ok";
            break;
        case "save_site_header":
            $p["header_mode"] = intval(@$_POST["header_mode"]);
            if ($p["header_mode"]) {
                $p["header"] = in(@$_POST["header_html"]);
            }
            else {
                $p["header"] = in(@$_POST["header_image"]);
            }
            $query = "UPDATE cms_site SET " . make_set_clause($p) . " WHERE domain_id='$domain_id'";
            $db->query($query);
            break;
        case "save_site_footer":
            $p["footer"] = in(@$_POST["footer"]);
            $query = "UPDATE cms_site SET " . make_set_clause($p) . " WHERE domain_id='$domain_id'";
            $db->query($query);
            break;
        case "save_site_css":
            $p["css"] = in(@$_POST["css"]);
            $query = "UPDATE cms_site SET " . make_set_clause($p) . " WHERE domain_id='$domain_id'";
            $db->query($query);
            echo "$query,ok";
            break;
        case "save_site_mainmenu":
            $p["width"] = in(substr(@$_POST["width"], 0, 8));
            $p["height"] = in(substr(@$_POST["height"], 0, 8));
            $p["color"] = in(substr(@$_POST["color"], 0, 8));
            $p["bgcolor"] = in(substr(@$_POST["bgcolor"], 0, 8));
            $p["hvcolor"] = in(substr(@$_POST["hvcolor"], 0, 8));
            $p["hvbgcolor"] = in(substr(@$_POST["hvbgcolor"], 0, 8));
            $p["font"] = in(@$_POST["font"]);
            $p["prespacer"] = in(@$_POST["prespacer"]);
            $p["postspacer"] = in(@$_POST["postspacer"]);
            $query = "UPDATE cms_site SET main_menu='" . serialize($p) . "' WHERE domain_id='$domain_id'";
            $db->query($query);
            echo "$query,ok";
            break;
        case "save_site_popupmenu":
            $p["width"] = in(substr(@$_POST["width"], 0, 8));
            $p["height"] = in(substr(@$_POST["height"], 0, 8));
            $p["color"] = in(substr(@$_POST["color"], 0, 8));
            $p["bgcolor"] = in(substr(@$_POST["bgcolor"], 0, 8));
            $p["hvcolor"] = in(substr(@$_POST["hvcolor"], 0, 8));
            $p["hvbgcolor"] = in(substr(@$_POST["hvbgcolor"], 0, 8));
            $p["font"] = in(@$_POST["font"]);
            $p["border"] = @$_POST["border"] ? 1 : 0;
            $p["prespacer"] = in(@$_POST["prespacer"]);
            $p["postspacer"] = in(@$_POST["postspacer"]);
            $query = "UPDATE cms_site SET popup_menu='" . serialize($p) . "' WHERE domain_id='$domain_id'";
            $db->query($query);
            echo "$query,ok";
            break;
        case "save_site_music":
            // fetch properties
            $query = "SELECT properties FROM cms_site WHERE domain_id='$domain_id'";
            $ret = $db->getRow($query, DB_FETCHMODE_ASSOC);
            $properties = @unserialize($ret["properties"]);
            // modify
            $properties["site_music_state"] = @$_POST["site_music_state"] ? 1 : 0;
            $properties["site_music_file"] = in(@$_POST["site_music_file"]);
            // save it back
            $props = in(serialize($properties));
            $query = "UPDATE cms_site SET properties='$props' WHERE domain_id='$domain_id'";
            $db->query($query);
            break;
        case "delete_node":
            $node_id = intval(@$_POST["id"]);
            $query = "DELETE FROM cms_node WHERE node_id='$node_id' AND domain_id='$domain_id'";
            $db->query($query);
            echo "$query,ok";
            break;
        case "delete_page":
            $page_id = intval(@$_POST["id"]);
            $query = "DELETE FROM cms_page WHERE page_id='$page_id' AND domain_id='$domain_id'";
            $db->query($query);
            break;
    }
}

function cmp_pages($a, $b) {
    if ($a["name"] == $b["name"] && $a['domain_id'] == $b['domain_id']) return 0;
    if ($a["name"] == "home" && $a['domain_id'] == $b['domain_id']) return -1;
    if ($b["name"] == "home" && $a['domain_id'] == $b['domain_id']) return 1;
    if ($a['domain_id'] < $b['domain_id']) return -1;
    if ($a['domain_id'] > $b['domain_id']) return 1;
    return ($a["name"] < $b["name"] && $a['domain_id'] == $b['domain_id']) ? -1 : 1;
}

function _ckeditor_fix($matches) {
    return urldecode($matches[1]);
}
