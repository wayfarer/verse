<?php
 // symfony backend integration with PHPMV
require_once($_SERVER['DOCUMENT_ROOT'] . '/../symfony/config/ProjectConfiguration.class.php');

define('PHPMV_PATH', 'http://twintierstech.net/phpmv2'); //no trailing slash needed
define('PHPMV_BASE_URL', 'http://twintierstech.net/phpmv2'); //no trailing slash needed
define('PHPMV_SU_LOGIN', 'admin');
define('PHPMV_SU_PASSWORD', '7b503f2bb345cd69757b0bd1d31f4d15'); //ready md5-string goes here

$configuration = ProjectConfiguration::getApplicationConfiguration('backend', 'dev', false);
$context = sfContext::createInstance($configuration);
$user = $context->getUser();
if ($user->isSuperAdmin()) {
    header('Location: ' . PHPMV_PATH . '/funlogin.php?login=' . urlencode(PHPMV_SU_LOGIN) . '&password=' . urlencode(PHPMV_SU_PASSWORD));
}
else
    if ($user->hasCredential('Statistics')) {
        $conn = Doctrine_Manager::connection();

        $ret = $conn->standaloneQuery("SELECT phpmv_login as login, phpmv_password as password FROM sms_user_fsms2phpmv WHERE fsms_uid=?", array($user->getGuardUser()->getId()));
        $obj = $ret->fetchObject();
        if ($obj) {
            // count stats visits
            $ret = $conn->standaloneQuery("UPDATE sms_stats_visits SET visits=visits+1, last_visit=now() WHERE domain_id=? AND stat_system='phpmv'", array(Doctrine::getTable('SmsDomain')->getCurrent()->getDomainId()));
            if ($ret->rowCount() == 0) {
                $conn->standaloneQuery("INSERT sms_stats_visits SET domain_id=?, stat_system='phpmv', visits=1, last_visit=now()", array(Doctrine::getTable('SmsDomain')->getCurrent()->getDomainId()));
            }
            header('Location: ' . PHPMV_PATH . '/funlogin.php?login=' . urlencode($obj->login) . '&password=' . urlencode($obj->password));
        }
        else {
            echo "Sorry, you have no access to PHPMV stats subsystem. If you are sure that you must have it, please click back in your browser and contact Twintierstech's representative (see at bottom of the screen for contact information)";
        }
    }


/*
include('inc/verse.inc.php');  //main header - initializes Verse environment

	if(!$user->logged()) {
		header('Location: login.php');
	}
	elseif($user->data['domain_id']==0) {
		header('Location: '.PHPMV_PATH.'/funlogin.php?login='.urlencode(PHPMV_SU_LOGIN).'&password='.urlencode(PHPMV_SU_PASSWORD));
	}
	elseif($user->have_role(ROLE_STATISTICS_MANAGEMENT)){
		$query = "SELECT phpmv_login as login,phpmv_password as password FROM sms_user_fsms2phpmv WHERE fsms_uid=".$user->data['user_id'];
		$res = $db->query($query);
		$obj = $res->fetchRow(DB_FETCHMODE_OBJECT);
		if($obj->login){
      // count stats visits
      $query = "UPDATE sms_stats_visits SET visits=visits+1, last_visit=now() WHERE domain_id='$domain_id' AND stat_system='phpmv'";
      $db->query($query);
      if(!$db->affectedRows()) {
        $query = "INSERT sms_stats_visits SET domain_id='$domain_id', stat_system='phpmv', visits=1, last_visit=now()";
        $db->query($query);
      }

			header('Location: '.PHPMV_PATH.'/funlogin.php?login='.urlencode($obj->login).'&password='.urlencode($obj->password));
		}
	}
	else{
		print 'An error occured. If you are sure you should have had access to the page contact to the system admin.';
	}
*/