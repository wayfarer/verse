Feature: Leave a candle
  In order to leave a candle for obituary
  As a site visitor
  I need to be able to leave a candle

  Background:
  

  Scenario: Login into backend
    Given I am on "/backend.php"
    When I fill in "Username" with "WayFarer"
    And I fill in "Password" with "admin"
    And I press "sign in"
    Then I should see "Obituaries Management"
