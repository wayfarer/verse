Feature: Login
  In order to manage website
  As website administrator
  I need to be able to login

  Scenario: Login into backend
    Given I am on "/backend.php"
    When I fill in "Username" with "WayFarer"
    And I fill in "Password" with "admin"
    And I press "sign in"
    Then I should see "Obituaries Management"
