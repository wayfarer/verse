<?php
// obsolete
// backend obituaries management
include("inc/verse.inc.php"); //main header - initializes Verse environment
include("inc/imageutil.inc.php");
include("inc/obittheme.inc.php");
include("inc/obituary.inc.php");

if ($user->have_role(ROLE_OBITUARIES_MANAGEMENT)) {
    $action = @$_POST["action"];

    switch ($action) {
        case "list":
            $rep = createobject("report_ajax", array($db, "plg_obituary", REP_WITH_PAGENATOR, true, 20, 'width="850px" style="background-color: #DDEEFF"'));

//			$rep->add_field("name", REP_STRING_TEMPLATE, '{first_name} {middle_name} {last_name}', REP_UNORDERABLE, 'align="center"');
            $rep->add_field("name", REP_FUNCTIONCALL_TEMPLATE, 'name({last_name},{middle_name},{first_name})', REP_ORDERABLE, 'align="center"');
            $rep->add_field("home", REP_STRING_TEMPLATE, '{home_place}', REP_ORDERABLE, 'align="center"');
//			$rep->add_field("death", REP_STRING_TEMPLATE, '{DATE_FORMAT(death_date,"'.DB_DATETEXT_FORMAT.'")}', REP_ORDERABLE, 'align="center"');
            $rep->add_field("death", REP_FUNCTIONCALL_TEMPLATE, 'format_date({death_date})', REP_ORDERABLE, 'align="center"');
            if ($user->have_role(ROLE_MOVIECLIPS_MANAGEMENT)) {
                $rep->add_field("movieclips", REP_FUNCTIONCALL_TEMPLATE, 'movieclips({obituary_id})', REP_ORDERABLE, 'align="center"');
            }
            $rep->add_field("", REP_STRING_TEMPLATE, href_js_action("properties", "new ObituaryEditor({obituary_id})"), REP_UNORDERABLE, 'align="center"');
            $rep->add_field("", REP_STRING_TEMPLATE, href_js_action("scrap book", "new ScrapbookEditor({obituary_id})"), REP_UNORDERABLE, 'align="center"');
            if ($user->have_role(ROLE_MOVIECLIPS_MANAGEMENT)) {
                $rep->add_field("", REP_STRING_TEMPLATE, href_js_action("movieclips", "new ObituaryMovieclips({obituary_id})"), REP_UNORDERABLE, 'align="center"');
            }
            $rep->add_field("", REP_STRING_TEMPLATE, href_js_action("subscribe", "new ObituarySubscriptions({obituary_id})"), REP_UNORDERABLE, 'align="center"');
            $rep->add_field("", REP_STRING_TEMPLATE, href_js_action("delete", "delete_obituary({obituary_id})"), REP_UNORDERABLE, 'align="center"');
            $rep->add_field("", REP_STRING_TEMPLATE, href("candles >", "candles.php?id={obituary_id}"), REP_UNORDERABLE, 'align="center"');
//  		$rep->add_field("", REP_STRING_TEMPLATE, href("movieclips >", "obituary_clips.php?id={obituary_id}"), REP_UNORDERABLE, 'align="center"');

            $rep->add_filter("", REP_INVISIBLE_FILTER, "domain_id", "=", $domain_id);

            $rep->html_attributes('class="sortable" id="uid" width="850px"');

            if (count($_POST) < 3) {
                $rep->order_by("death_date", 1);
            }
            $rep->handle_events($_POST);

            $html = $rep->make_report();

//		    $html .= print_r($_SESSION);

            echo $html;
            break;
        case "list_candles":
        case "list_candles_hidden":
        case "list_candles_notapproved":
            // we are sure here that obituary_id IS OK for current domain
            $obituary_id = intval(@$_POST["id"]);

            $rep = createobject("report_ajax", array($db, "plg_obituary_candle", REP_WITH_PAGENATOR, true, 100, 'width="700px" style="background-color: #DDEEFF"'));
            $rep->add_table("plg_obituary", REP_INNER_JOIN, "obituary_id");

            if ($action == "list_candles_notapproved") {
                $rep->add_field("obit", REP_STRING_TEMPLATE, '{first_name} {middle_name} {last_name}', REP_ORDERABLE, 'align="center"');
            }
            $rep->add_field("time", REP_STRING_TEMPLATE, '{timestamp}', REP_ORDERABLE, 'align="center"');
            $rep->add_field("name", REP_STRING_TEMPLATE, '{name}', REP_ORDERABLE, 'align="center"');
            $rep->add_field("email", REP_STRING_TEMPLATE, '{email}', REP_ORDERABLE, 'align="center"');
            $rep->add_field("ip", REP_STRING_TEMPLATE, '{ip}', REP_ORDERABLE, 'align="center"');
            $rep->add_field("thoughts", REP_STRING_TEMPLATE, '{thoughts}', REP_ORDERABLE, 'align="center"');
            switch ($action) {
                case "list_candles":
                    $rep->add_field("", REP_STRING_TEMPLATE, href_js_action("edit", "new CandleEditor({candle_id})"), REP_UNORDERABLE, 'align="center"');
                    $rep->add_field("", REP_STRING_TEMPLATE, href_js_action("hide", "hide_candle({candle_id})"), REP_UNORDERABLE, 'align="center"');
                    $rep->add_filter("", REP_INVISIBLE_FILTER, "state", "=", 0);

                    $rep->add_filter("", REP_INVISIBLE_FILTER, "t1.obituary_id", "=", $obituary_id);
                    break;
                case "list_candles_hidden":
                    $rep->add_field("", REP_STRING_TEMPLATE, href_js_action("edit", "new CandleEditor({candle_id})"), REP_UNORDERABLE, 'align="center"');
                    $rep->add_field("", REP_STRING_TEMPLATE, href_js_action("unhide", "unhide_candle({candle_id})"), REP_UNORDERABLE, 'align="center"');
                    $rep->add_field("", REP_STRING_TEMPLATE, href_js_action("delete", "delete_candle({candle_id})"), REP_UNORDERABLE, 'align="center"');
                    $rep->add_filter("", REP_INVISIBLE_FILTER, "state", "=", 2);

                    $rep->add_filter("", REP_INVISIBLE_FILTER, "t1.obituary_id", "=", $obituary_id);
                    break;
                case "list_candles_notapproved":
                    $rep->add_field("", REP_STRING_TEMPLATE, href_js_action("approve", "approve({candle_id})"), REP_UNORDERABLE, 'align="center"');
                    $rep->add_field("", REP_STRING_TEMPLATE, href_js_action("hide", "hide_candle({candle_id})"), REP_UNORDERABLE, 'align="center"');
                    $rep->add_filter("", REP_INVISIBLE_FILTER, "state", "=", 1);
                    // show not approved candles for all obituaries
                    break;
            }
            $rep->add_filter("", REP_INVISIBLE_FILTER, "t2.domain_id", "=", $domain_id);

            $rep->html_attributes('width="700px"');

            if (count($_POST) < 3) {
                $rep->order_by("timestamp", 1);
            }
            $rep->handle_events($_POST);

            $html = $rep->make_report();
            echo $html;
            break;
        case "load":
            $obituary_id = intval(@$_POST["id"]);
            $query = "SELECT obituary_case_id, first_name, middle_name, last_name, home_place, DATE_FORMAT(death_date, '" . DB_DATE_FORMAT . "') death_date, birth_place, DATE_FORMAT(birth_date, '" . DB_DATE_FORMAT . "') birth_date, service_date, service_time, service_place, service_type, visitation_date, visitation_place, final_disposition, image, obit_text, candles_policy FROM plg_obituary WHERE obituary_id='$obituary_id' AND domain_id='$domain_id'";
            $ret = $db->getRow($query, DB_FETCHMODE_ASSOC);
            // preprocess
            if ($ret["service_date"]) {
                $ret["service_date"] = date("Y-m-d", $ret["service_date"]);
            }
            else {
                $ret["service_date"] = "";
            }
            // check for associated events
            $query = "SELECT 1 FROM plg_event WHERE obituary_id='$obituary_id' AND event_type='1' AND domain_id='$domain_id'";
            $ret["add_calendar_sp"] = $db->getOne($query);
            $query = "SELECT 1 FROM plg_event WHERE obituary_id='$obituary_id' AND event_type='2' AND domain_id='$domain_id'";
            $ret["add_calendar_vp"] = $db->getOne($query);
//			header("X-JSON:".make_json_response($ret));
            echo make_json_response($ret);
            break;
        case "load_candle":
            // we are sure here that candle_id IS OK for current domain
            $candle_id = intval(@$_POST["id"]);
            $query = "SELECT timestamp, name, email, thoughts FROM plg_obituary_candle WHERE candle_id='$candle_id' AND domain_id='$domain_id'";
            $ret = $db->getRow($query, DB_FETCHMODE_ASSOC);
            header("X-JSON:" . make_json_response($ret));
            break;
        case "load_scrap":
            $obituary_id = intval(@$_POST["id"]);
            $query = "SELECT image, description FROM plg_slide WHERE obituary_id='$obituary_id' AND domain_id='$domain_id' ORDER BY CONVERT(SUBSTRING_INDEX(SUBSTRING_INDEX(image,'.',1),'-',-1), UNSIGNED)";
            $ret = $db->getAssoc($query);
            header("X-JSON:" . make_json_response($ret));
            break;
        case "load_config":
            $query = "SELECT param, value FROM plg_obituary_config WHERE domain_id='$domain_id'";
            $ret = $db->getAssoc($query);
            if (!@$ret["candles_policy"]) $ret["candles_policy"] = "on"; // default candles policy
            if (@$ret["flowers_page_id"]) $ret["flowers"] = 1;
            header("X-JSON:" . make_json_response($ret));
            break;
        case "save":
            $obituary_id = intval(@$_POST["id"]);
            $p = array();
            $obituary_case_id = in(substr(@$_POST["obituary_case_id"], 0, 50));
            // put case id to query only if set, else NULL will be inserted by default
            if ($obituary_case_id) $p["obituary_case_id"] = $obituary_case_id;
            $p["first_name"] = in(substr(@$_POST["first_name"], 0, 32));
            $p["middle_name"] = in(substr(@$_POST["middle_name"], 0, 32));
            $p["last_name"] = in(substr(@$_POST["last_name"], 0, 32));
            $p["home_place"] = in(substr(@$_POST["home_place"], 0, 128));
            $p["death_date"] = in(@$_POST["death_date"]);
            $p["birth_date"] = in(@$_POST["birth_date"]);
            $p["birth_place"] = in(substr(@$_POST["birth_place"], 0, 128));
            if (@$_POST["service_date"]) {
                $p["service_date"] = @strtotime($_POST["service_date"]);
            }
            else {
                $p["service_date"] = "";
            }
            $p["service_time"] = in(substr(@$_POST["service_time"], 0, 128));
            $p["service_place"] = in(substr(@$_POST["service_place"], 0, 128));
            $p["service_type"] = intval(@$_POST["service_type"]);
            $p["visitation_date"] = in($_POST["visitation_date"]);
            $p["visitation_place"] = in(substr(@$_POST["visitation_place"], 0, 128));
            $p["final_disposition"] = in(substr(@$_POST["final_disposition"], 0, 128));

            // process image
            $image = "files/$domain_name/" . @$_POST["image"];
            if (!file_exists($image)) {
                $image = "files/$domain_name/image/" . @$_POST["image"];
            }
            if (file_exists($image)) {
                resizejpg($image, 200); // resize to max width=200
                $p["image"] = in(@$_POST["image"]);
            }
            else {
                $p["image"] = "";
            }

//      if($domain_id == 1) {
            $p["obit_text"] = in(filter_xss($_POST["obit_text"], array('a', 'em', 'strong', 'cite', 'code', 'ul', 'ol', 'li', 'dl', 'dt', 'dd', 'br', 'p', 'img')));
//      }
//      else {
//  			$p["obit_text"] = in(@$_POST["obit_text"]);
//      }

            $candles_policy = in(@$_POST["candles_policy"]);
            if (in_array($candles_policy, array("on", "moderated", "off"))) {
                $p["candles_policy"] = $candles_policy;
            }
            else {
                $p["candles_policy"] = NULL;
            }
            if ($obituary_id) {
                $query = "UPDATE plg_obituary SET " . make_set_clause($p) . " WHERE obituary_id='$obituary_id' AND domain_id='$domain_id'";
                $db->query($query);
                //        send_obituary_notification($obituary_id, $p["first_name"]." ".$p["middle_name"]." ".$p["last_name"], $p["birth_date"], $p["death_date"]);
            }
            else {
                $query = "INSERT plg_obituary SET " . make_set_clause($p) . ", domain_id='$domain_id'";
                $db->query($query);
                $query = "SELECT LAST_INSERT_ID()";
                $obituary_id = $db->getOne($query);
                send_obituary_notification($obituary_id, '', $p["first_name"] . $p["middle_name"] . $p["last_name"], $p["birth_date"], $p["death_date"]);
            }
            // manage associated events events if asked
            if (isset($_POST["add_calendar_sp"])) {
                // add or replace service place
                $description = $p["service_place"];
                $query = "DELETE FROM plg_event WHERE domain_id='$domain_id' AND obituary_id='$obituary_id' AND event_type='1'";
                $db->query($query);
                $query = "INSERT plg_event SET domain_id='$domain_id', obituary_id='$obituary_id', event_type='1', timestamp='" . $p["service_date"] . "', description='$description'";
            }
            else {
                // try to delete service place if event associated with obituary exists
                $query = "DELETE FROM plg_event WHERE domain_id='$domain_id' AND obituary_id='$obituary_id' AND event_type='1'";
            }
            $db->query($query);

            if (isset($_POST["add_calendar_vp"])) {
                // add visitation place
                $description = $p["visitation_place"];
                $visitation_date = @strtotime($_POST["visitation_date"]);
                if ($visitation_date) {
                    $query = "DELETE FROM plg_event WHERE domain_id='$domain_id' AND obituary_id='$obituary_id' AND event_type='2'";
                    $db->query($query);
                    $query = "REPLACE plg_event SET domain_id='$domain_id', obituary_id='$obituary_id', event_type='2', timestamp='$visitation_date', description='$description'";
                }
            }
            else {
                // try to delete visitation place if event associated with obituary exists
                $query = "DELETE FROM plg_event WHERE domain_id='$domain_id' AND obituary_id='$obituary_id' AND event_type='2'";
            }
            $db->query($query);
            echo "ok";
            break;
        case "save_candle":
            $candle_id = intval(@$_POST["id"]);
            $p = array();
            $p["timestamp"] = in(@$_POST["timestamp"]);
            $p["name"] = in(@$_POST["name"]);
            $p["email"] = in(@$_POST["email"]);
            $p["thoughts"] = in(@$_POST["thoughts"]);

            if ($candle_id) {
                $query = "UPDATE plg_obituary_candle SET " . make_set_clause($p) . " WHERE candle_id='$candle_id' AND domain_id='$domain_id'";
                $db->query($query);
            }
            else {
                $obituary_id = intval(@$_POST["oid"]);
                // check if obituary_id is of current domain
                $query = "SELECT 1 FROM plg_obituary WHERE obituary_id='$obituary_id' AND domain_id='$domain_id'";
                $ret = $db->getOne($query);
                if ($ret) {
                    // save new candle
                    $p["obituary_id"] = $obituary_id;
                    //				 	$p["email"] = "admin";
                    if (!$p["timestamp"]) $p["timestamp"] = date("Y-m-d H:i:s");
                    $p["domain_id"] = $domain_id;

                    $query = "INSERT plg_obituary_candle SET " . make_set_clause($p);
                    $db->query($query);
                }
            }
            break;
        case "save_scrap":
            $obituary_id = intval(@$_POST["id"]);
            // clear entries from scrap book
            $query = "DELETE FROM plg_slide WHERE obituary_id='$obituary_id' AND domain_id='$domain_id'";
            $db->query($query);
            // insert new slides
            foreach (@$_POST["img"] as $id => $value) {
                $image = in($value);
                $description = in(@$_POST["desc"][$id]);
                $image_file = "files/$domain_name/$image";
                if (file_exists($image_file)) {
                    resizejpg($image_file, 750); // resize to max width=200
                }
                $query = "INSERT plg_slide SET image='$image', description='$description', obituary_id='$obituary_id', domain_id='$domain_id'";
                $db->query($query);
            }
            break;
        case "save_config":
            $quick_edit_enabled = isset($_POST["quick_edit_enabled"]) ? 1 : 0;
            $quick_edit_codeword = in(@$_POST["quick_edit_codeword"]);
            $remove_candle_codeword = in(@$_POST["remove_candle_codeword"]);
            $candles_policy = in(@$_POST["candles_policy"]);
            if (!in_array($candles_policy, array("on", "moderated", "off"))) $candles_policy = "on";
            $flowers_enabled = isset($_POST["flowers"]) ? 1 : 0;
            if ($flowers_enabled) {
                $flowers_page_id = intval(@$_POST["flowers_page_id"]);
            }
            else {
                $flowers_page_id = 0;
            }
            $light_candle_text = in(@$_POST["light_candle"]);
            $captcha_enabled = isset($_POST["captcha"]) ? 1 : 0;

            $query = "REPLACE plg_obituary_config SET domain_id='$domain_id', param='quick_edit_enabled', value='$quick_edit_enabled'";
            $db->query($query);
            $query = "REPLACE plg_obituary_config SET domain_id='$domain_id', param='quick_edit_codeword', value='$quick_edit_codeword'";
            $db->query($query);
            $query = "REPLACE plg_obituary_config SET domain_id='$domain_id', param='remove_candle_codeword', value='$remove_candle_codeword'";
            $db->query($query);
            $query = "REPLACE plg_obituary_config SET domain_id='$domain_id', param='candles_policy', value='$candles_policy'";
            $db->query($query);
            $query = "REPLACE plg_obituary_config SET domain_id='$domain_id', param='flowers_page_id', value='$flowers_page_id'";
            $db->query($query);
            $query = "REPLACE plg_obituary_config SET domain_id='$domain_id', param='light_candle', value='$light_candle_text'";
            $db->query($query);
            $query = "REPLACE plg_obituary_config SET domain_id='$domain_id', param='captcha', value='$captcha_enabled'";
            $db->query($query);
            echo "ok";
            break;
        case "delete":
            $obituary_id = intval(@$_POST["id"]);
            $query = "DELETE FROM plg_obituary WHERE obituary_id='$obituary_id' AND domain_id='$domain_id'";
            $db->query($query);
            // delete assoc events
            $query = "DELETE FROM plg_event WHERE obituary_id='$obituary_id' AND domain_id='$domain_id'";
            $db->query($query);
            echo "ok";
            break;
        case "hide_candle":
            $candle_id = intval(@$_POST["id"]);
            $query = "UPDATE plg_obituary_candle SET state=2 WHERE candle_id='$candle_id' AND domain_id='$domain_id'";
            $db->query($query);
            break;
        case "unhide_candle":
            $candle_id = intval(@$_POST["id"]);
            $query = "UPDATE plg_obituary_candle SET state=0 WHERE candle_id='$candle_id' AND domain_id='$domain_id'";
            $db->query($query);
            break;
        case "delete_candle":
            $candle_id = intval(@$_POST["id"]);
            $query = "DELETE FROM plg_obituary_candle WHERE candle_id='$candle_id' AND domain_id='$domain_id'";
            $db->query($query);
            break;
        case "approve_candle":
            $candle_id = intval(@$_POST["id"]);
            $query = "UPDATE plg_obituary_candle SET state=0 WHERE candle_id='$candle_id' AND domain_id='$domain_id'";
            $db->query($query);
            break;
        case "load_movieclip_ids":
            $obituary_id = intval(@$_POST["id"]);
            $query = "SELECT om.movieclip_id FROM plg_obituary_movieclip om INNER JOIN plg_movieclip USING(movieclip_id) WHERE om.obituary_id='$obituary_id' AND domain_id='$domain_id'";
            $ret = $db->getCol($query);
            $data = array("movieclip_ids" => implode(", ", $ret));
            header("X-JSON:" . make_json_response($data));
            break;
        case "save_movieclip_ids":
            $obituary_id = intval(@$_POST["id"]);
            // check for obituary_id is of current domain
            $query = "SELECT 1 FROM plg_obituary WHERE obituary_id='$obituary_id' AND domain_id='$domain_id'";
            if ($db->getOne($query)) {
                // clear all movieclips
                $query = "DELETE FROM plg_obituary_movieclip WHERE obituary_id='$obituary_id'";
                $db->query($query);
                $movieclip_ids = @explode(",", $_POST["movieclip_ids"]);
                foreach ($movieclip_ids as $movieclip_id) {
                    // check for movieclip existance
                    $movieclip_id = intval(trim($movieclip_id));
                    $query = "SELECT 1 FROM plg_movieclip WHERE movieclip_id='$movieclip_id' AND domain_id='$domain_id'";
                    if ($db->getOne($query)) {
                        $query = "INSERT plg_obituary_movieclip SET obituary_id='$obituary_id', movieclip_id='$movieclip_id'";
                        $db->query($query);
                    }
                }
            }
            break;
        case "load_theme":
            $query = "SELECT value FROM plg_obituary_config WHERE param='theme' AND domain_id='" . $GLOBALS["domain_id"] . "' AND obituary_id=0";
            $ret = $GLOBALS["db"]->getOne($query);
            if ($ret) {
                $config = unserialize($ret);
            }
            else {
                $config = array();
            }
            header("X-JSON:" . make_json_response($config));
            break;
        case "save_theme":
            $theme = array("frame", "width", "height", "bgcolor", "image", "frameoutbordersize", "frameoutbordercolor", "frameinbordersize", "frameinbordercolor");
            foreach ($theme as $elem) {
                $p["theme_$elem"] = in($_POST["theme_$elem"]);
            }
            $query = "REPLACE plg_obituary_config SET domain_id='$domain_id', param='theme', value='" . serialize($p) . "'";
            $db->query($query);
            echo $query;
            break;
        case "load_subscriptions":
            $obituary_id = intval($_POST["id"]);
            $query = "SELECT name, email, created, enabled FROM plg_obituary_subscribe WHERE obituary_id='$obituary_id' AND domain_id='$domain_id'";
            $data = $db->getAll($query, DB_FETCHMODE_ASSOC);
            header("X-JSON:" . make_json_response($data));
            break;
        case "save_subscriptions":
            $obituary_id = intval($_POST["id"]);
            if ($obituary_id) {
                // process deletes
                $remove = $_POST["r"];
                if ($remove) {
                    $r_emails = explode(",", $remove);
                    foreach ($r_emails as &$r_email) {
                        $r_email = "'" . in($r_email) . "'";
                    }
                    $r_str = implode(",", $r_emails);
                    $query = "DELETE FROM plg_obituary_subscribe WHERE email IN ($r_str) AND obituary_id='$obituary_id' AND domain_id='$domain_id'";
                    $db->query($query);
                }
                //        echo $query;

                $names = $_POST["names"];
                $emails = $_POST["emails"];
                $values = array();
                foreach ($_POST["names"] as $key => $name) {
                    $hash = substr(md5(uniqid()), rand(0, 15), 16);
                    $values[] = "('$obituary_id', '$domain_id', '" . in($name) . "', '" . in($emails[$key]) . "', now(), '$hash', 1, 0)";
                }
                $query = "INSERT plg_obituary_subscribe (obituary_id, domain_id, name, email, created, hash, enabled, isadmin) VALUES " . implode(",", $values);
                $ret = $db->query($query);
            }
            break;
        case "load_newobits_subscription":
            $query = "SELECT name, email, created, enabled FROM plg_subscribe WHERE domain_id='$domain_id' ORDER BY created DESC";
            $data = $db->getAll($query, DB_FETCHMODE_ASSOC);
//  		header("X-JSON:".make_json_response($data));
            echo make_json_response($data);
            break;
        case "save_newobits_subscription":
            $obituary_id = intval($_POST["id"]);
            // process deletes
            $remove = $_POST["r"];
            if ($remove) {
                $r_emails = explode(",", $remove);
                foreach ($r_emails as &$r_email) {
                    $r_email = "'" . in($r_email) . "'";
                }
                $r_str = implode(",", $r_emails);
                $query = "DELETE FROM plg_subscribe WHERE email IN ($r_str) AND domain_id='$domain_id'";
                $db->query($query);
            }

            $names = $_POST["names"];
            $emails = $_POST["emails"];
            $values = array();
            foreach ($_POST["names"] as $key => $name) {
                $hash = substr(md5(uniqid()), rand(0, 15), 16);
                $values[] = "('$domain_id', '" . in($name) . "', '" . in($emails[$key]) . "', now(), '$hash', 1)";
            }
            $query = "INSERT plg_subscribe (domain_id, name, email, created, hash, enabled) VALUES " . implode(",", $values);
            $db->query($query);
//      echo $query;
//      var_dump($_POST);
            break;
        default:
            // fetch existing pages for obituaries config dialogg
            $query = "SELECT page_id, internal_name FROM cms_page WHERE domain_id='$domain_id' ORDER BY internal_name";
            $pages = $db->getAssoc($query);

            $smarty->assign("pages", $pages);
            $smarty->assign("border_styles", $ob_border_styles);
            $smarty->assign("theme_images", $ob_theme_images);
            $smarty->assign("service_types", $obituary_service_types);
//      if($domain_id<>1) {
//  			$smarty->display("obituaries.tpl");
//      }
//      else {
            $smarty->display("obituaries2.tpl");
        //      }
    }
}
else {
    header("Location: login.php");
}

function format_date($date) {
    return date("F d, Y", strtotime($date));
}

function name($last, $middle, $first) {
    return $first . " " . $middle . " " . $last;
}

function movieclips($obituary_id) {
    global $db;
    $query = "SELECT title FROM plg_obituary_movieclip INNER JOIN plg_movieclip USING(movieclip_id) WHERE obituary_id='$obituary_id'";
    $ret = $db->getCol($query);
    return implode("<br>", $ret);
}
