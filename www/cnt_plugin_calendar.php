<?php
// plugin for calendar pages
include "cnt_plugin_main.php";

function process_content($content, $state) {
    //		return "";

    if (isset($state["t"])) {
        $now = intval($state["t"]);
    }
    else {
        $now = time();
    }

    $cnt = process_tags($content["content1"], array("calendar_content" => "calendar_content", "calendar" => "calendar", "calendar_date" => "calendar_date", "calendar_prev" => "calendar_prev", "calendar_next" => "calendar_next"), $now);
    return $cnt;
}

function calendar($params) {
    global $domain_id, $db, $smarty;

    $now = $params["user_params"];
    $period = @$params["mode"];
    if (!$period) $period = "month";

    if ($period == "month") {
        $period_begin = mktime(0, 0, 0, date("m", $now), 1, date("Y", $now));
        $period_end = mktime(23, 59, 59, date("m", $now) + 1, 0, date("Y", $now));
    }
    else {
        $period_begin = strtotime("last monday", strtotime("+1 day", $now));
        $period_end = strtotime("sunday 23:59:59", $now);
    }

    // fetch periods's events
    $query = "SELECT timestamp, description, first_name, middle_name, last_name, event_type, service_time, visitation_time FROM plg_event e LEFT JOIN plg_obituary o USING(obituary_id) WHERE timestamp>='$period_begin' AND timestamp<='$period_end' AND e.domain_id='$domain_id' ORDER BY timestamp";
    $events = $db->getAll($query, DB_FETCHMODE_ASSOC);

    // cycle through each day of period
    $i = 0;
    $data = array();
    $timestamp = $period_begin;
    while ($timestamp < $period_end) {
        $day_begin = mktime(0, 0, 0, date("m", $timestamp), date("d", $timestamp), date("Y", $timestamp));
        $day_end = strtotime("23:59:59", $day_begin);
        $content = "";
        while ($i < count($events) && $events[$i]["timestamp"] >= $day_begin && $events[$i]["timestamp"] <= $day_end) {
            $time = '';
            if ($events[$i]["first_name"] && strcasecmp(substr($events[$i]["description"], 0, 3), "<B>") && strcasecmp(substr($events[$i]["description"], 0, 8), "<STRONG>")) {
                $content .= "<b>" . $events[$i]["first_name"] . " " . $events[$i]["middle_name"] . " " . $events[$i]["last_name"];
                // add event description
                switch ($events[$i]["event_type"]) {
                    case 1:
                        $content .= " Service";
                        $time = $events[$i]["service_time"];
                        break;
                    case 2:
                        $content .= " Calling Hours";
                        $time = $events[$i]["visitation_time"];
                        break;
                }
                $content .= "</b><br>";
            }
            if(!empty($time)) {
                $content .= $time.", ";
            }

            $content .= $events[$i]["description"] . "<br>";
            $i++;
        }
        $data[date("d", $timestamp)] = array("day_name" => date("l", $timestamp), "content" => $content);
        $timestamp = strtotime("next day", $timestamp);
    }

    $smarty->assign("weekday", date("w", $period_begin));
    $smarty->assign("data", $data);
    $html = $smarty->fetch("cnt_plugin_calendar.tpl");
    return $html;
}

function calendar_date($params) {
    $ret = "";
    $period = @$params["mode"];
    if (!$period) $period = "month";
    if ($period == "month") {
        $ret = date("F Y", $params["user_params"]);
    }
    else {
        $period_begin = strtotime("last monday", strtotime("+1 day", $params["user_params"]));
        $period_end = strtotime("sunday 23:59:59", $params["user_params"]);
        $ret = date("F d", $period_begin) . " - " . date("F d", $period_end);
    }
    return $ret;
}

function calendar_prev($params) {
    $ret = "";
    $period = @$params["mode"];
    if (!$period) $period = "month";
    if ($period == "month") {
        $prev = mktime(0, 0, 0, date("m", $params["user_params"]), 0, date("Y", $params["user_params"]));
        //			$ret = '<a href="?p='.$_GET["p"].'&t='.$prev.'">&laquo '.date("F",$prev).'</a>';
        //			$ret = '?p='.$_GET["p"].'&t='.$prev;
        $ret = '/' . arg(0) . '?t=' . $prev;
    }
    else {
        $prev = strtotime("last week", $params["user_params"]);
        //			$ret = '<a href="?p='.$_GET["p"].'&t='.$prev.'">&laquo Prev. week</a>';
        $ret = '/' . arg(0) . '?t=' . $prev;
    }
    return $ret;
}

function calendar_next($params) {
    $ret = "";
    $period = @$params["mode"];
    if (!$period) $period = "month";
    if ($period == "month") {
        $next = mktime(0, 0, 0, date("m", $params["user_params"]) + 1, 1, date("Y", $params["user_params"]));
        //			$ret = '<a href="?p='.$_GET["p"].'&t='.$next.'">'.date("F",$next).' &raquo;</a>';
        $ret = '/' . arg(0) . '?t=' . $next;
    }
    else {
        $next = strtotime("next week", $params["user_params"]);
        //			$ret='<a href="?p='.$_GET["p"].'&t='.$next.'">Next week &raquo;</a>';
        $ret = '/' . arg(0) . '?t=' . $next;
    }
    return $ret;
}

function calendar_content($params) {
    $cnt = file_get_contents("templates/helpers/calendar.html");

    $cnt = process_tags($cnt, array("calendar" => "calendar", "calendar_date" => "calendar_date", "calendar_prev" => "calendar_prev", "calendar_next" => "calendar_next"), $params['user_params']);

    return $cnt;
}
