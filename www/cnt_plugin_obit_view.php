<?php
 // obituary view page plugin
include("cnt_plugin_main.php");
include("inc/obittheme.inc.php");
include("inc/imageutil.inc.php");

function process_content($content, $state) {
    global $domain_id, $domain_name, $theme_frame, $db;

    $emailfrom = "noreply@twintierstech.net";

    $obituary_id = intval(@$state["id"]);
    // check obituary_id here to belong to current domain id
    $query = "SELECT 1 FROM plg_obituary WHERE obituary_id='$obituary_id' AND domain_id='" . $GLOBALS["domain_id"] . "'";
    $ret = $db->getOne($query);
    if (!$ret) $obituary_id = 0;

    if (!$obituary_id) {
        return '<script>document.location="/"</script>';
    }

    // view all candles mode
    /*		if(@$_GET["candle"]=="all") {
          if(strpos($content["content1"], "{obituary_view")!==false) {
                $cnt = process_tags($content["content1"], array("obituary_view"=>"view_candles_all"), $obituary_id);
          }                                                                   cnt_plugin_obit_view.php
          else {
            $cnt = view_candles_all(array("user_params"=>$obituary_id));
          }
                return $cnt;
            }
    */
    // load global obituary config
    $query = "SELECT param, value FROM plg_obituary_config WHERE domain_id='" . $GLOBALS["domain_id"] . "' AND obituary_id=0";
    $config = $GLOBALS["db"]->getAssoc($query);
    $theme = unserialize($config["theme"]);
    if ($theme) {
        unset($config["theme"]);
        $config = array_merge($config, $theme);
    }

    // try to load per-obit config 123
    $query = "SELECT param, value FROM plg_obituary_config WHERE domain_id='" . $GLOBALS["domain_id"] . "' AND obituary_id='$obituary_id'";
    $obit_config = $GLOBALS["db"]->getAssoc($query);
    $theme = unserialize($obit_config["theme"]);
    if ($theme) {
        unset($obit_config["theme"]);
        $obit_config = array_merge($obit_config, $theme);
    }

    $config = array_merge($config, $obit_config);

    // obits2 defaults
    if (!$config["theme_frame"]) $config["theme_frame"] = 1; // no frame
    if (!$config["theme_image"]) $config["theme_image"] = "noimage";
    // preprocess
    $config["theme_frame"] = $theme_frame[$config["theme_frame"]];
    // obits2 defaults second pass
    if (!$config["theme_bgcolor"]) $config["theme_bgcolor"] = $config["theme_frame"]["innerbgcolor"] or "transparent";
    // obits2 user overrides
    if ($config["theme_width"] != "") $config["theme_frame"]["width"] = $config["theme_width"];
    if ($config["theme_height"] != "") $config["theme_frame"]["height"] = $config["theme_height"];
    if ($config["theme_frameoutbordersize"] != "") $config["theme_frame"]["outbordersize"] = $config["theme_frameoutbordersize"];
    if ($config["theme_frameoutbordercolor"] != "") $config["theme_frame"]["outbordercolor"] = $config["theme_frameoutbordercolor"];
    if ($config["theme_frameinbordersize"] != "") $config["theme_frame"]["inbordersize"] = $config["theme_frameinbordersize"];
    if ($config["theme_frameinbordercolor"] != "") $config["theme_frame"]["inbordercolor"] = $config["theme_frameinbordercolor"];

    if (!$config["candles_policy"]) $config["candles_policy"] = "on"; // default candles policy
    // flowers page name
    if ($config["flowers_page_id"]) {
        $query = "SELECT internal_name FROM cms_page WHERE page_id='" . $config["flowers_page_id"] . "'";
        $config["flowers_page"] = $db->getOne($query);
    }

    // instant obituary edit feature (return to obituary)
    if (@$config["quick_edit_enabled"]) {
        if (@$state["return"]) {
            // check if codeword matches
            if ($state["return"] === @$config["quick_edit_codeword"]) {
                // allow obituary edit
                $_SESSION["obit_edit"] = true;
            }
            if ($state["return"] === @$config["remove_candle_codeword"]) {
                // check if we are on candle view page
                if (intval(@$state["candle"])) {
                    $_SESSION["remove_candle"] = $state["candle"];
                }
            }
        }
        else {
            $_SESSION["obit_edit"] = false;
            $_SESSION["remove_candle"] = false;
        }
    }

    // fetch obituary
    $query = "SELECT obituary_id, slug, first_name, middle_name, last_name, title, suffix, home_place, DATE_FORMAT(death_date, '" . DB_DATETEXT_FORMAT . "') death_date, death_date dd, DATE_FORMAT(birth_date, '" . DB_DATETEXT_FORMAT . "') birth_date, birth_place, service_date, service_time, service_place, service_type, visitation_date, visitation_time, visitation_place, final_disposition, image, obit_text, candles_policy FROM plg_obituary WHERE obituary_id='$obituary_id'";
    // age issues:
    // http://normandean.com/?p=obituary_view&id=43920 (Fred J. Sommers)
    // http://ballweg-lunsford.com/obituary_view/10006830 (Dean Craig Cullen)
    $ret = $GLOBALS["db"]->getRow($query, DB_FETCHMODE_ASSOC);

    if(empty($ret['slug'])) {
        $ret['slug'] = get_obituary_slug($ret);
    }

    $dd = new DateTime($ret['death_date']);
    $bd = new DateTime($ret['birth_date']);
    $age = $dd->format('Y') - $bd->format('Y');
    // check if birthday happened last year
    if($dd->format('m-d') < $bd->format('m-d')) {
       	$age--;
    }
    $ret['age'] = $age;

    // apply per-obituary candles policy if it has been set
    if ($ret["candles_policy"]) $config["candles_policy"] = $ret["candles_policy"];

    $ret['image'] = thumbnail($ret['image'], 180);

    // load obituary movieclips
    $query = "SELECT mc.movieclip_id, title FROM plg_movieclip mc INNER JOIN plg_obituary_movieclip USING(movieclip_id) WHERE status=1 AND obituary_id='$obituary_id' AND domain_id='$domain_id'";
    $retmc = $GLOBALS["db"]->getAll($query, DB_FETCHMODE_ASSOC);
    foreach ($retmc as $mc) {
        $ret["movieclips"][] = $mc;
    }
    // check scrapbook presence
    $query = "SELECT 1 FROM plg_slide WHERE obituary_id='$obituary_id'";
    $scrapbook = $GLOBALS["db"]->getOne($query);
    if ($scrapbook) {
        $ret["scrapbookhere"] = 1;
        $ret["scrapbookpage"] = "scrapbook";
    }

    if (@$state["candle"] == "save" && $config["candles_policy"] != "off") {
        $blocked_ips = array("60.190.240.73", "60.190.240.68", "67.182.104.231", "78.129.208.30", "68.37.236.50", "72.82.210.81", "41.214.37.209", "209.212.2.54", "66.26.240.65");

        $block_ips = $GLOBALS["db"]->getAll("SELECT ip FROM plg_block_ip WHERE domain_id=".$GLOBALS["domain_id"]);
        foreach($block_ips as $block_ip) {
            $blocked_ips[] = $block_ip[0];
        }


        $name = in(@$state["name"]);
        $email = in(@$state["email"]);
        $thoughts = in(@$state["thoughts"]);
        $message = @$state["thoughts"]; // for emails, unescaped
        $ip = $_SERVER["REMOTE_ADDR"];

        if ($config["captcha"]) { // if captcha enabled - check it
            $captcha = $state["captcha"];
            if ($captcha != $_SESSION["captcha"] || !$_SESSION["captcha"]) {
                $GLOBALS["errors"]["captcha"] = "Letters do not match, please try again";
            }
            unset($_SESSION["captcha"]); // erase captcha to protect from subsequent posts with same one
        }

        if (!$name) $GLOBALS["errors"]["name"] = "Name required";
        if (strlen($thoughts) < 10) $GLOBALS["errors"]["thoughts"] = "Your message is too short, please try again";

        if (!count($GLOBALS["errors"])) {
            if ($config["candles_policy"] == "moderated") {
                $cstate = 1; // not approved
                $state["notapproved"] = 1;
            }
            else {
                $cstate = 0; // approved
            }

            // Find blocked keywords
            $block_keywords = $GLOBALS["db"]->getAll("SELECT keyword FROM plg_block_keyword WHERE domain_id=0 OR domain_id=".$GLOBALS["domain_id"]);
            $no_block_keyword = true;
            foreach($block_keywords as $block_keyword) {
                if (stripos($thoughts, $block_keyword[0]) !== false) {
                    $no_block_keyword = false;
                    break;
                }
            }

            // do not save anything for blocked IPs
            if (!in_array($ip, $blocked_ips) && $no_block_keyword) {
                $hash = "";
                $query = "INSERT plg_obituary_candle SET obituary_id='$obituary_id', domain_id='$domain_id', name='$name', email='$email', ip='$ip', thoughts='$thoughts', timestamp='" . date("Y-m-d H:i") . "', state='$cstate'";
                // store hash to refer candle from email if not approved (activate from email feature)
                if ($cstate > 0) {
                    $hash = substr(md5(uniqid("")), 0, 16);
                    $query .= ", hash='$hash'";
                }
                $GLOBALS["db"]->query($query);
                // backup candle queries
                $f = fopen("../backup/candles.sql", "a");
                fwrite($f, $query . ";\n");
                fclose($f);
                $query = "SELECT LAST_INSERT_ID()";
                $state["candle"] = $GLOBALS["db"]->getOne($query);

                // mail candle to default address
                $query = "SELECT email FROM sms_domain WHERE domain_id='" . $GLOBALS["domain_id"] . "'";
                $emailto = $GLOBALS["db"]->getOne($query);
                $text = "Obituary: {$ret['first_name']} {$ret['middle_name']} {$ret['last_name']}\nName: $name\nEmail: $email\nThoughts: $message\nIP: $ip\n\n";
                $query = "SELECT internal_name FROM cms_page WHERE page_type=4 AND domain_id=" . $GLOBALS["domain_id"];
                $obit_view_page = $GLOBALS["db"]->getOne($query);
                if ($cstate > 0) {
                    $text .= "A candle submitted to your site is awaiting for approval. If you would like to publish this candle, please click on the following link:\n";
                }
                else {
                    $text .= "This candle has been published. To view it, please click on the following link:\n";
                }
                $text .= "http://{$GLOBALS['domain_name']}/?p=$obit_view_page&id={$ret['obituary_id']}&candle={$state['candle']}";
                if ($cstate > 0) {
                    $text .= "&act=$hash\n\n";
                    $text .= "Note: If you do not want to publish this candle, you do not need to take any additional actions. By not clicking this link the candle will remain unpublished.";
                }
                $subj = "New candle/message: {$ret['last_name']} {$ret['first_name']}";
                mail($emailto, $subj, $text, "From: $emailfrom");
                //  				mail("andrew0223@gmail.com", $subj, $text, "From: andrew0223@gmail.com", "-fandrew0223@gmail.com andrew0223@gmail.com");

                // mail candles to subscribers
                $query = "SELECT name, email, hash FROM plg_obituary_subscribe WHERE obituary_id='$obituary_id' AND domain_id='$domain_id'";
                $subscribers = $db->getAll($query, DB_FETCHMODE_ASSOC);
                if ($subscribers && !DB::isError($subscribers)) {
                    // compose message text
                    $subj = "New obituary candle/message for {$ret['last_name']} {$ret['first_name']}";
                    $text = "From: $name\n\n$message\n\n----------\nIf you wish to unsubscribe from future email notifications for {$ret['last_name']} {$ret['first_name']}, please click the following link: http://{$GLOBALS['domain_name']}/subscribe.php?action=unsubscribe&id=";
                    foreach ($subscribers as $row) {
                        $atext = $text . $row["hash"];
                        $emailto = $row["email"];
                        mail($emailto, $subj, $atext, "From: $emailfrom");
                        //              $GLOBALS['logger']->info("sending email to $emailto from $emailfrom, subj: $subj");
                    }
                }
            }
        }
        else {
            $state["candle"] = "new";
        }
    }
    if ($state["subscribe"] == "save") {
        $name = in(@$state["name"]);
        $email = in(@$state["email"]);
        if ($config["captcha"]) { // if captcha enabled - check it
            $captcha = $state["captcha"];
            if ($captcha != $_SESSION["captcha"] || !$_SESSION["captcha"]) {
                $GLOBALS["errors"]["captcha"] = "Letters do not match, please try again";
            }
            unset($_SESSION["captcha"]); // erase captcha to protect from subsequent posts with same one
        }
        if (!$name) $GLOBALS["errors"]["name"] = "Name required";
        if (!check_email($email)) $GLOBALS["errors"]["email"] = "Valid email required";

        if (!count($GLOBALS["errors"])) {
            // save subscription
            $hash = substr(md5(uniqid()), rand(0, 15), 16);
            $query = "INSERT plg_obituary_subscribe SET obituary_id='$obituary_id', domain_id='$domain_id', name='$name', email='$email', created=now(), hash='$hash', enabled=0";
            $db->query($query);
            if ($db->affectedRows() > 0) {
                // send email
                $emailto = "$name <$email>";
                $subj = "Obituary subscription confirmation";
                $text = "This email address has requested to receive new candles to the obituary for {$ret['first_name']} {$ret['last_name']}. To complete the subscription and receive them, please click the following link: http://$domain_name/subscribe.php?action=confirm&id=$hash\n\nIf you did not initiate this request, please ignore this email and you will not be subscribed.";
                mail($emailto, $subj, $text, "From: $emailfrom");

                $state["subscribe"] = "done";
            }
            else {
                $GLOBALS["errors"]["alreadyexists"] = "You already subscribed on this obituary";
            }
        }
    }

    // process with obituary
    // fix age
    if ($ret["age"] < 1 && $ret["dd"] < "2006-01-01") {
        $ret["age"] = "";
        $ret["birth_date"] = "";
    }
    // process values
    if ($ret["service_date"]) {
        $ret["service_date"] = date("l, F d, Y", $ret["service_date"]);
        if ($ret["service_time"]) $ret["service_date"] .= " " . $ret["service_time"];
    }
    else {
        $ret["service_date"] = "";
    }
    if ($ret["visitation_date"]) {
            $ret["visitation_date"] = date("l, F d, Y", $ret["visitation_date"]);
    }
    if ($ret["visitation_time"]) {
        $ret["visitation_date"] .= " " . $ret["visitation_time"];
    }
    if ($ret["visitation_place"]) {
        $ret["visitation_date"] .= " " . $ret["visitation_place"];
    }

    if ($ret["final_disposition"]) {
        // do not show crematoriums
        $filter_crematorium_keywords = array("crematory", "cremation", "crematorium");
        $fck_exclude_domains = array(57);
        if (!in_array($domain_id, $fck_exclude_domains)) {
            foreach ($filter_crematorium_keywords as $fkw) {
                if (strpos(strtolower($ret["final_disposition"]), strtolower($fkw)) !== false) {
                    $ret["final_disposition"] = "";
                    break;
                }
            }
        }
    }

    $ret["candle"] = @$state["candle"];
    $ret["subscribe"] = @$state["subscribe"];
    $ret["notapproved"] = intval(@$state["notapproved"]);
    $ret["sort"] = @$state["sort"];

    // publish candle if requested
    if (isset($_GET["act"]) && $ret["candle"]) {
        // fetch candle hash
        $query = "SELECT hash FROM plg_obituary_candle WHERE candle_id='" . $ret["candle"] . "'";
        $hash = $GLOBALS["db"]->getOne($query);
        if (strlen($hash) == 16 && $_GET["act"] === $hash) {
            // activate and reset hash
            $query = "UPDATE plg_obituary_candle SET state=0, hash=NULL WHERE candle_id='" . $ret["candle"] . "'";
            $GLOBALS["db"]->query($query);
            $ret["justpublished"] = 1;
        }
    }

    if (!isset($_GET["print"])) {
        // fetch candle ids
        $query = "SELECT candle_id, name FROM plg_obituary_candle WHERE obituary_id='$obituary_id' AND state=0 ORDER BY candle_id";
        $candles = $GLOBALS["db"]->getAll($query, DB_FETCHMODE_ASSOC);
        $ret["candles"] = $candles;
        $ret["candles_count"] = count($candles);
    }
    else {
        // fetch candle texts for printing
        $query = "SELECT name, DATE_FORMAT(timestamp, '" . DB_DATETEXT_FORMAT /*DB_DATETIMELONG_FORMAT*/ . "') timestamp, thoughts FROM plg_obituary_candle WHERE obituary_id='$obituary_id' AND state=0 ORDER BY candle_id";
        $candle_list = $GLOBALS["db"]->getAll($query, DB_FETCHMODE_ASSOC);
    }

    $config["obituary_id"] = $obituary_id;
    $GLOBALS["smarty"]->assign("config", $config);
    $GLOBALS["smarty"]->assign("obituary_service_types", $GLOBALS["obituary_service_types"]);
    $GLOBALS["smarty"]->assign("data", $ret);

    // first step processing (external obituary templates)
    if (isset($_GET["print"]) && $content["content2"]) {
        $cnt = process_tags($content["content2"], array("obituary_view" => "obituary_view"));
    }
    else {
        $cnt = process_tags($content["content1"], array("obituary_view" => "obituary_view"));
    }

    if (!$cnt) {
        // if no ext template
        $process_cnt = $content["content1"];
        if (isset($_GET["print"]) && $content["content2"]) {
            $process_cnt = $content["content2"];
        }
    }
    else {
        $process_cnt = $cnt;
    }
    $cnt = process_tags($process_cnt, array("obituary_left_content" => "obit_left", "obituary_left_content_jc" => "obit_left_jc", "obituary_right_content" => "obit_right", "name" => "name", "funeral_info" => "funeral_info", "biography" => "obit_right", "candles" => "candles", "light_candle_link" => "light_candle_link", "print_link" => "print_link", "all_candles_link" => "all_candles_link", "subscribe_link" => "subscribe_link", "photo" => "photo", "obituary_right_content_jc" => "obit_right_jc", "obit_content" => "obit_content", "obit_candles_all" => "obit_candles_all"), $ret);

    if (isset($_GET["print"]) && $config['candles_policy'] !== "off") {
        // add candle list
        $list = "<br>";
        foreach ($candle_list as $candle) {
            $list .= "<b>" . $candle["name"] . "</b> " . $candle["timestamp"] . "<hr size=\"1\">" . $candle["thoughts"] . "<br><br>";
        }
        $cnt .= $list;
    }

    return $cnt;
}

function obit_left($params) {
    $GLOBALS["smarty"]->assign("data", $params["user_params"]);

    $scrapbookpage = @$params["scrapbook_page"];
    if (!$scrapbookpage) $scrapbookpage = "scrapbook";
    // check scrapbook presence
    $query = "SELECT 1 FROM plg_slide WHERE obituary_id='" . $params["user_params"]["obituary_id"] . "'";
    $scrapbook = $GLOBALS["db"]->getOne($query);
    $GLOBALS["smarty"]->assign("scrapbookhere", $scrapbook);
    $GLOBALS["smarty"]->assign("scrapbookpage", $scrapbookpage);

    $html = $GLOBALS["smarty"]->fetch("cnt_plugin_obit_view_left.tpl");
    return $html;
}

function obit_left_jc($params) {
    $GLOBALS["smarty"]->assign("data", $params["user_params"]);

    $scrapbookpage = @$params["scrapbook_page"];
    if (!$scrapbookpage) $scrapbookpage = "scrapbook";
    // check scrapbook presence
    $query = "SELECT 1 FROM plg_slide WHERE obituary_id='" . $params["user_params"]["obituary_id"] . "'";
    $scrapbook = $GLOBALS["db"]->getOne($query);
    $GLOBALS["smarty"]->assign("scrapbookhere", $scrapbook);
    $GLOBALS["smarty"]->assign("scrapbookpage", $scrapbookpage);

    $html = $GLOBALS["smarty"]->fetch("cnt_plugin_obit_view_left_jc.tpl");
    return $html;
}

function obit_right($params) {
    $cnt = "";
    if (@$params["user_params"]["candle"] == "new") {
        $GLOBALS["smarty"]->assign("ip", $_SERVER["REMOTE_ADDR"]);
        $GLOBALS["smarty"]->assign("errors", @$GLOBALS["errors"]);
        $GLOBALS["smarty"]->assign("input", $_POST);
        $cnt = $GLOBALS["smarty"]->fetch("cnt_plugin_obit_view_candleform.tpl");
    }
    else
        if (@$params["user_params"]["candle"] == "all") {
            $query = "SELECT candle_id, obituary_id, name, DATE_FORMAT(timestamp, '" . DB_DATETEXT_FORMAT /*DB_DATETIMELONG_FORMAT*/ . "') timestamp, thoughts FROM plg_obituary_candle WHERE obituary_id='" . $params["user_params"]["obituary_id"] . "' AND state=0 ORDER BY candle_id";
            $candles = $GLOBALS["db"]->getAll($query, DB_FETCHMODE_ASSOC);

            $GLOBALS["smarty"]->assign("candles", $candles);
            $cnt = $GLOBALS["smarty"]->fetch("cnt_plugin_obit_view_candles_all.tpl");
        }
        else
            if (intval($params["user_params"]["candle"])) {
                $candle_id = intval($params["user_params"]["candle"]);
                // fetch candle
                $query = "SELECT name, email, thoughts, DATE_FORMAT(timestamp, '" . DB_DATETEXT_FORMAT /*DB_DATETIMELONG_FORMAT*/ . "') timestamp, obituary_id FROM plg_obituary_candle WHERE candle_id='$candle_id' AND obituary_id='" . $params["user_params"]["obituary_id"] . "'";
                $candle = $GLOBALS["db"]->getRow($query, DB_FETCHMODE_ASSOC);
                if ($candle) {
                    $GLOBALS["smarty"]->assign("candle", $candle);
                    $GLOBALS["smarty"]->assign("notapproved", @$params["user_params"]["notapproved"]);
                    $GLOBALS["smarty"]->assign("justpublished", @$params["user_params"]["justpublished"]);
                    $cnt = $GLOBALS["smarty"]->fetch("cnt_plugin_obit_view_candleview.tpl");
                }
                // in case of invalid candle id show obituary text
            }
    if (!strlen($cnt)) {
        $scrapbookpage = @$params["scrapbook_page"];
        if (!$scrapbookpage) $scrapbookpage = "scrapbook";
        // check scrapbook presence
        $query = "SELECT 1 FROM plg_slide WHERE obituary_id='" . $params["user_params"]["obituary_id"] . "'";
        $scrapbook = $GLOBALS["db"]->getOne($query);
        $GLOBALS["smarty"]->assign("obit_text", $params["user_params"]["obit_text"]);
        $GLOBALS["smarty"]->assign("scrapbookhere", $scrapbook);
        $GLOBALS["smarty"]->assign("scrapbookpage", $scrapbookpage);
        $cnt = $GLOBALS["smarty"]->fetch("cnt_plugin_obit_view_right.tpl");
    }

    return $cnt;
}

function obit_right_jc($params) {
    $cnt = "";
    if (@$params["user_params"]["candle"] == "new") {
        $GLOBALS["smarty"]->assign("ip", $_SERVER["REMOTE_ADDR"]);
        $GLOBALS["smarty"]->assign("errors", @$GLOBALS["errors"]);
        $GLOBALS["smarty"]->assign("input", $_POST);
        $cnt = $GLOBALS["smarty"]->fetch("cnt_plugin_obit_view_candleform.tpl");
    }
    else
        if (intval($params["user_params"]["candle"])) {
            $candle_id = intval($params["user_params"]["candle"]);
            // fetch candle
            $query = "SELECT name, email, thoughts, DATE_FORMAT(timestamp, '" . DB_DATETIMELONG_FORMAT . "') timestamp, obituary_id FROM plg_obituary_candle WHERE candle_id='$candle_id' AND obituary_id='" . $params["user_params"]["obituary_id"] . "'";
            $candle = $GLOBALS["db"]->getRow($query, DB_FETCHMODE_ASSOC);
            if ($candle) {
                $GLOBALS["smarty"]->assign("candle", $candle);
                $GLOBALS["smarty"]->assign("notapproved", @$params["user_params"]["notapproved"]);
                $cnt = $GLOBALS["smarty"]->fetch("cnt_plugin_obit_view_candleview.tpl");
            }
            // in case of invalid candle id show obituary text
        }
    if (!strlen($cnt)) {
        $GLOBALS["smarty"]->assign("obit_text", $params["user_params"]["obit_text"]);
        $cnt = $GLOBALS["smarty"]->fetch("cnt_plugin_obit_view_right_jc.tpl");
    }

    return $cnt;
}

function name($params) {
    return $params["user_params"]["first_name"] . " " . $params["user_params"]["middle_name"] . " " . $params["user_params"]["last_name"];
}

function funeral_info($params) {
    $GLOBALS["smarty"]->assign("data", $params["user_params"]);
    return $GLOBALS["smarty"]->fetch("cnt_plugin_obit_view_funeral_info.tpl");
}

function biography($params) {
    return $params["user_params"]["obit_text"];
}

function candles($params) {
    $GLOBALS["smarty"]->assign("data", $params["user_params"]);
    return $GLOBALS["smarty"]->fetch("cnt_plugin_obit_view_candles.tpl");
}

function light_candle_link($params) {
    return "/obituary_view/" . $params["user_params"]["slug"] . "/" . $params["user_params"]["obituary_id"] . "?candle=new";
}

function subscribe_link($params) {
    return "/obituary_view/" . $params["user_params"]["slug"] . "/" . $params["user_params"]["obituary_id"] . "?subscribe=1";
}

function print_link($params) {
    return "/obituary_view/" . $params["user_params"]["slug"] . "/" . $params["user_params"]["obituary_id"] . "?print=1";
}

function all_candles_link($params) {
    return "/obituary_view/" . $params["user_params"]["slug"] . "/" . $params["user_params"]["obituary_id"] . "?candle=all";
}

function photo($params) {
    if ($params["user_params"]["image"]) {
        //			return '<a href="'.url('obituary_view', array('id'=>$params["user_params"]["obituary_id"])).'"><img src="/'.$params["user_params"]["image"].'"></a>';
        return l('<img src="' . leading_slash($params["user_params"]["image"]) . '">', "obituary_view/" . $params["user_params"]["obituary_id"], array("html" => true));
    }
    else return "";
}

function obituary_view($params) {
    if (isset($_GET["print"])) {
        $style = "obituaries/" . $params["style"] . "_print.tpl";
        if ($GLOBALS["smarty"]->template_exists($style)) {
            return $GLOBALS["smarty"]->fetch($style);
        }
    }
    $style = "obituaries/" . $params["style"] . ".tpl";
    if ($GLOBALS["smarty"]->template_exists($style)) {
        return $GLOBALS["smarty"]->fetch($style);
    }
    return "";
}

function view_candles_all($params = NULL) {
    $query = "SELECT name, DATE_FORMAT(timestamp, '" . DB_DATETEXT_FORMAT /*DB_DATETIMELONG_FORMAT*/ . "') timestamp, thoughts FROM plg_obituary_candle WHERE obituary_id='" . $params["user_params"] . "' AND state=0 ORDER BY candle_id";
    $candles = $GLOBALS["db"]->getAll($query, DB_FETCHMODE_ASSOC);

    $GLOBALS["smarty"]->assign("candles", $candles);
    $html = $GLOBALS["smarty"]->fetch("cnt_plugin_obit_view_candles_all.tpl");
    return $html;
}

function obit_candles_all($params = NULL) {
    global $smarty;

    $query = "SELECT candle_id, obituary_id, name, DATE_FORMAT(timestamp, '" . DB_DATETEXT_FORMAT /*DB_DATETIMELONG_FORMAT*/ . "') timestamp, thoughts FROM plg_obituary_candle WHERE obituary_id='" . $params["user_params"]["obituary_id"] . "' AND state=0 ORDER BY candle_id";
    $sort = 1;
    if ($params["user_params"]["sort"] == "desc") {
        $query .= " DESC";
        $sort = 2;
    }
    $candles = $GLOBALS["db"]->getAll($query, DB_FETCHMODE_ASSOC);

    $smarty->assign("candles", $candles);
    $smarty->assign("sort", $sort);
    $smarty->assign("obituary_id", $params["user_params"]["obituary_id"]);
    $smarty->assign("slug", $params["user_params"]["slug"]);

    return $smarty->fetch("obit2_candles_all.tpl");
}

function obit_content($params) {
    global $smarty;
    $cnt = "";

    // determine state
    $state = "";
    if (isset($params["user_params"]["candle"])) {
        if ($params["user_params"]["candle"] == "new") {
            $state = "newcandle";
        }
        else if ($params["user_params"]["candle"] == "all") {
            $state = "dispallcandles";
        }
        else {
            $state = "dispcandle";
        }
    }
    else {
        if (isset($params["user_params"]["subscribe"])) {
            if ($params["user_params"]["subscribe"] == "done") {
                $state = "subscribe_done";
            }
            else {
                $state = "subscribe";
            }
        }
    }
    if (!$state) $state = "default";

    switch ($state) {
        case "newcandle":
            $smarty->assign("ip", $_SERVER["REMOTE_ADDR"]);
            $smarty->assign("errors", @$GLOBALS["errors"]);
            $smarty->assign("input", $_POST);
            $cnt = $smarty->fetch("obit2_candleform.tpl");
            break;
        case "dispcandle":
            $candle_id = intval($params["user_params"]["candle"]);
            // fetch candle
            $query = "SELECT candle_id, name, email, thoughts, DATE_FORMAT(timestamp, '" . DB_DATETEXT_FORMAT /*DB_DATETIMELONG_FORMAT*/ . "') timestamp, obituary_id FROM plg_obituary_candle WHERE candle_id='$candle_id' AND obituary_id='" . $params["user_params"]["obituary_id"] . "'";
            $candle = $GLOBALS["db"]->getRow($query, DB_FETCHMODE_ASSOC);
            if ($candle) {
                $smarty->assign("candle", $candle);
                $smarty->assign("notapproved", @$params["user_params"]["notapproved"]);
                $smarty->assign("justpublished", @$params["user_params"]["justpublished"]);
                $cnt = $smarty->fetch("obit2_candleview.tpl");
            }
            break;
        case "dispallcandles":
            $cnt = obit_candles_all($params);
            break;
        case "subscribe":
            $smarty->assign("errors", @$GLOBALS["errors"]);
            $smarty->assign("input", $_POST);
            $cnt = $smarty->fetch("obit2_subscribe.tpl");
            break;
        case "subscribe_done":
            $cnt = $smarty->fetch("obit2_subscribedone.tpl");
            break;
    }

    // show obituary text by default
    if (!strlen($cnt)) {
        $scrapbookpage = @$params["scrapbook_page"];
        if (!$scrapbookpage) $scrapbookpage = "scrapbook";
        // check scrapbook presence
        $query = "SELECT 1 FROM plg_slide WHERE obituary_id='" . $params["user_params"]["obituary_id"] . "'";
        $scrapbook = $GLOBALS["db"]->getOne($query);
        $smarty->assign("obit_text", $params["user_params"]["obit_text"]);
        $smarty->assign("scrapbookhere", $scrapbook);
        $smarty->assign("scrapbookpage", $scrapbookpage);
        $cnt = $GLOBALS["smarty"]->fetch("obit2_right.tpl");
    }

    return $cnt;
}
