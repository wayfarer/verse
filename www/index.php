<?php
// main frontend script
include("inc/verse.inc.php"); //main header - initializes Verse environment
include("src/silex_bootstrap.php");

use Knp\Snappy\Pdf;
use Verse\Domain\Domain;

// check for old-style ?p= URLs and redirect them to new ones
if (strpos($_SERVER['REQUEST_URI'], "/?p=") !== false || strpos($_SERVER['REQUEST_URI'], "index.php?p=") !== false) {
    // log old URL access / for development purposes to track and fix old URLs
    flog($domain_name . ": " . $_SERVER['REQUEST_URI'] . "; referer=" . $_SERVER["HTTP_REFERER"]);

    $new_path = str_replace(array("index.php?p=", "/?p=", "&id="), array("", "/", "/"), $_SERVER['REQUEST_URI']);
    if (($amppos = strpos($new_path, "&")) !== false) {
        $new_path[$amppos] = "?";
    }
    header("HTTP/1.1 301 Moved Permanently");
    header("Location: $new_path");
    exit;
}

$page = arg(0);

// analyze GET parameters and detect wrong image or files references
if (strpos($_GET["p"], ".") !== false) {
    bflog($domain_name . ": " . $_SERVER['REQUEST_URI'] . "; referer=" . $_SERVER["HTTP_REFERER"] . "; " . $_SERVER["HTTP_USER_AGENT"]);
}

//    echo $page,";";
//    var_dump($_GET);

// render web site for domain
$uc_flag = false;
if (!$force_production && $domain->get("mode") > 0) {
    // not production domain
    $uc_flag = uc_settings_exist();
}

if (!$page) {
    $page = "home";
    /*    if(page_exists("home")) {
     $page="home";
   }
   else {
     die("Website is under construction. Please come back later");
   } */
}

// fetch content
$cnt = null;
if ($uc_flag) {
    // try to fetch UC content first
    $query = "SELECT data, page_type, no_header, properties FROM cms_page_uc WHERE name='" . in($page) . "' AND domain_id='$domain_id'";
    $cnt = $db->getRow($query, DB_FETCHMODE_ASSOC);
    //    if($cnt) {
    //      $cnt['resticted'] = 0;
    //    }
}
if (!$cnt) {
    // fetch normal content, if not UC mode or no UC page
    $query = "SELECT data, page_type, no_header, restricted, properties, display_name, custom_header FROM cms_page p LEFT JOIN cms_node USING(page_id) WHERE p.internal_name='" . in($page) . "' AND p.domain_id='$domain_id'";
    $cnt = $db->getRow($query, DB_FETCHMODE_ASSOC);
    
    //if domain in domain group and page isn't from this domain
    if (!$cnt) {
        $group_ids = $db->getCol("SELECT lg.id FROM sms_domain_link_group lg JOIN sms_domain_link l ON lg.id=l.group_id WHERE is_shared_pages='1' AND domain_id='$domain_id'");
        if(count($group_ids)) {
            $query = "SELECT data, page_type, no_header, restricted, properties, display_name, custom_header, p.domain_id FROM cms_page p LEFT JOIN cms_node n USING(page_id) WHERE p.internal_name='" . in($page) . "' AND n.domain_id='$domain_id' AND page_id IN (SELECT page_id FROM sms_domain_link_page JOIN sms_domain_link l USING(group_id) WHERE l.domain_id = '$domain_id' AND l.group_id IN (".implode(', ', $group_ids)."))";
            $cnt = $db->getRow($query, DB_FETCHMODE_ASSOC);
            if (!$cnt) {
                $query = "SELECT data, page_type, no_header, restricted, properties, display_name, custom_header, p.domain_id FROM cms_page p LEFT JOIN cms_node n USING(page_id) WHERE p.internal_name='" . in($page) . "' AND page_id IN (SELECT page_id FROM sms_domain_link_page JOIN sms_domain_link l USING(group_id) WHERE l.domain_id = '$domain_id' AND l.group_id IN (".implode(', ', $group_ids)."))";
                $cnt = $db->getRow($query, DB_FETCHMODE_ASSOC);
            }
            if(!is_null($cnt) && $cnt['domain_id'] != $domain_id) {
                $content = unserialize($cnt['data']);
                $page_domain_name = $db->getOne("SELECT domain_name FROM sms_domain WHERE domain_id='".$cnt['domain_id']."'");
                // rewrite images
                $content['content1'] = preg_replace('/src=([\'"])(?!http:\/\/)([^\'"]+)/i', 'src=\1'.$page_domain_name.'/\2', $content['content1']);
                $content['content2'] = preg_replace('/src=([\'"])(?!http:\/\/)([^\'"]+)/i', 'src=\1'.$page_domain_name.'/\2', $content['content2']);
                $cnt['data'] = serialize($content);
            }
        }
    }
}

// autogenerate subtree for empty nodes
if (!$cnt) {
    $matches = array();
    if (preg_match("/-(\d+)$/", $page, $matches)) {
        $node_id = intval($matches[1]);
        $query = "SELECT n.ord, p.internal_name, display_name, depth
								FROM cms_node n LEFT JOIN cms_page p USING(page_id)
								WHERE n.ord > ( SELECT ord FROM cms_node WHERE node_id = '$node_id' AND domain_id = '$domain_id' )
								AND depth > 0
								AND n.domain_id = '$domain_id' ORDER BY n.ord";
        $ret = $db->getAll($query, DB_FETCHMODE_ASSOC);
        if ($ret && !DB::isError($ret)) {
            $links = array();
            $ord = $ret[0]["ord"];
            $depth = $ret[0]["depth"];
            $i = 0;
            while ($ret[$i]["ord"] == $ord && $ret[$i]["depth"] >= $depth) {
                $ret[$i]["depth"] -= $depth;
                $links[] = $ret[$i];
                $i++;
                $ord++;
            }
            // fetch title
            $query = "SELECT display_name FROM cms_node WHERE node_id='$node_id' AND domain_id='$domain_id'";
            $page_title = $db->getOne($query);
            $smarty->assign("page_title", $page_title);
            $smarty->assign("links", $links);
            $content = $smarty->fetch("menu-auto.tpl");
            $cnt = array("data" => serialize(array("content1" => $content)), "page_type" => 1, "no_header" => 0, "restricted" => 0);
        }
    }
}

// predefined pages
if (!$cnt) {
    switch ($page) {
        case "cart":
            $cnt = array("data" => "", "page_type" => 9, "no_header" => 0, "restricted" => 0);
            break;
        case "checkout":
            $cnt = array("data" => "", "page_type" => 10, "no_header" => 0, "restricted" => 0);
            break;
        case "scrapbook":
            $cnt = array("data" => serialize(array("content1" => file_get_contents("templates/helpers/scrapbook.html"))), "page_type" => 7, "no_header" => 1, "restricted" => 0);
            break;
        case "webcasting":
            $cnt = array("data" => serialize(array("content1" => "{webcasting_player}")), "page_type" => 1, "no_header" => 0, "restricted" => 1);
            break;
        case "login":
            $cnt = array("data" => serialize(array("content1" => file_get_contents("templates/helpers/login.html"))), "page_type" => 1, "no_header" => 0, "restricted" => 0);
            break;
        case "register":
            $cnt = array("data" => "", "page_type" => 13, "no_header" => 0, "restricted" => 0);
            break;
        case "forget_password":
            $cnt = array("data" => "", "page_type" => 14, "no_header" => 0, "restricted" => 0);
            break;
        case "member_profile":
            $cnt = array("data" => "", "page_type" => 15, "no_header" => 0, "restricted" => 1);
            break;
        case "online-obituary":
            $cnt = array("data" => "", "page_type" => 4, "no_header" => 0, "restricted" => 0);
            break;
        case "search-results":
            $cnt = array("data" => serialize(array("content1" => file_get_contents("templates/helpers/search-results.html"))), "page_type" => 1, "no_header" => 0, "restricted" => 0);
            break;
        case "flowerstore":
            $cnt = array("data" => "", "page_type" => 17, "no_header" => 0, "restricted" => 0);;
            break;
        case "flowercart":
            $cnt = array("data" => "", "page_type" => 18, "no_header" => 0, "restricted" => 0);;
            break;
        case "flowercheckout":
            $cnt = array("data" => "", "page_type" => 19, "no_header" => 0, "restricted" => 0);;
            break;
        default:
            // log page not founds for development and fix purposes
            pnflog("$domain_name: $page; (" . $_SERVER['REQUEST_URI'] . "); referer=" . $_SERVER["HTTP_REFERER"] . "; " . $_SERVER["HTTP_USER_AGENT"]);
            header("HTTP/1.1 404 Not Found");
            readfile("404.html");
//				header("Location: /404.html");
            exit;
    }
}
else {
    $props = @unserialize($cnt["properties"]);
    if ($props) {
        $cnt = array_merge($cnt, $props);
    }
    $ch = @unserialize($cnt["custom_header"]);
    if ($ch) {
        $cnt = array_merge($cnt, $ch);
    }
}

$theuser = createobject("user", array($db, "cms_access_user"));

if (isset($_GET["logout"])) {
    $theuser->logout();
    verse_set_message("You have been logged out");
    if ($page) {
        header("Location: {$_GET['p']}");
    }
    else {
        header("Location: /");
    }
    exit;
}

if (isset($_GET['content_switcher'])) {
    $_SESSION['content_switcher'] = 'content'.$_GET['content_switcher'];
}

if (@$cnt["restricted"]) {
    if (isset($_POST["thelogin"])) {
        if ($theuser->login(@$_POST["thelogin"], @$_POST["password"], true)) {
            // check user domain
            if ($theuser->data["domain_id"] != 0 && $theuser->data["domain_id"] != $domain_id && !$theuser->is_shared()) {
                $theuser->logout();
            }
            else {
                // all ok, pass user
                if (@$_SESSION["backtopage"]) {
                    $page = $_SESSION["backtopage"];
                    $_SESSION["backtopage"] = NULL;
                    // clear environment
                    unset($_POST["thelogin"]);
                    unset($_POST["password"]);
                }
            }
        }
        else {
            verse_set_message("The system cannot log you in. Please, check your login and password and try again", "error");
        }
    }

    // restrict access to logged user or show login page
    if (!$theuser->logged()) {
        $_SESSION["backtopage"] = $page;
        $page = "login";
        // fetch content
        $query = "SELECT data, page_type, no_header FROM cms_page WHERE internal_name='$page' AND domain_id='$domain_id'";
        $cnt = $db->getRow($query, DB_FETCHMODE_ASSOC);
        if (!$cnt) {
            $cnt = array("data" => serialize(array("content1" => file_get_contents("templates/helpers/login.html"))), "page_type" => 1, "no_header" => 0, "restricted" => 0);
        }
    }

    if ($theuser->logged() && $page !== "member_profile" && $theuser->data["force_password_change"] && $theuser->data["privileges"] != 2 /*group*/) {
        verse_set_message("Please, update your profile and set new password before using it");
        $page = "member_profile?action=edit";
        header("Location: /$page");
        exit;
    }

    // do not pass to the user profile with group privileges
    if ($theuser->logged() && $page == "member_profile" && $theuser->data["privileges"] == 2) {
        header("Location: /");
        exit;
    }
}

$content = "";
if ($cnt) {
    if ($cnt["page_type"] == 16) {
        $query = "SELECT data FROM cms_page WHERE internal_name='" . in($page) . "' AND domain_id='$domain_id' AND page_type = 16";
        $ret = $db->getRow($query, DB_FETCHMODE_ASSOC);
        if ($ret) {
            $a_content = @unserialize($ret["data"]);
            $content = $a_content['content1'];
        }

        $app->before(function() use ($app, $domain_id) {
            $domain = new Domain($app['db'], $domain_id);
            $app['request_context']->setParameter('domain', $domain);
        });

        ob_start();
        $mail_form = require __DIR__ . '/src/mail_form.php';

        $app->mount('/', $mail_form);

        $app->run();
        $ret = ob_get_clean();

        if ($content && strpos($content, "{mail_form}") !== false) {
            $content = str_replace("{mail_form}", $ret, $content);
        } else {
            $content = $ret;
        }
    } else {
        // process content with chosen plugin
        include "cnt_plugins_map.php";
        $content_type = $plugins_map[$cnt["page_type"]];
        $plugin_name = "cnt_plugin_$content_type.php";
        if (!file_exists($plugin_name)) {
            $content_type = "static";
            $plugin_name = "cnt_plugin_$content_type.php";
        }
        include $plugin_name;
        $state = array_merge($_GET, $_POST);

        if($content_type == 'obit_view') {
            $id = array_pop(explode('/', $_REQUEST['p']));
            if(is_numeric($id)) {
                $state['id'] = $id;
            }
        }

        unset($state["p"]);

        $content = process_content(@unserialize($cnt["data"]), $state);
    }
}
if (!$content) $content = "&nbsp;";

// handle with symfony 1
if ($page == 'online-obituary' && ( (arg(1) && arg(2)) || is_numeric(arg(1)) )) {
    $cnt["page_type"] = 4;
    ob_start();
    // special processing using symfony
    include_once($_SERVER['DOCUMENT_ROOT'] . '/../symfony/config/ProjectConfiguration.class.php');
    $configuration = ProjectConfiguration::getApplicationConfiguration('frontend', 'prod', false);
    sfContext::createInstance($configuration)->dispatch();
    $content = ob_get_contents();
    ob_end_clean();
    if (arg(3) == 'print' || (isset($_SERVER["HTTP_X_REQUESTED_WITH"]) && $_SERVER["HTTP_X_REQUESTED_WITH"] == "XMLHttpRequest")) {
        $cnt['no_header'] = true;
    }
    else {
        $content = str_replace(array('<head>', '</head>', '<body>', '</body>'), '', $content);
    }
}
else {
    // handle with Silex
    if ($page == 'online-obituary') {
        // avoid obituary_view interference
        $query = "SELECT data FROM cms_page WHERE internal_name='" . in($page) . "' AND domain_id='$domain_id' AND page_type != 4";
        $ret = $db->getRow($query, DB_FETCHMODE_ASSOC);
        if ($ret) {
            $a_content = @unserialize($ret["data"]);
            $content = $a_content['content1'];
        }

        $cnt["page_type"] = 3; // obituary search

        // get search theme from config
        $theme = $db->getOne("SELECT value FROM plg_obituary_config WHERE domain_id = ? AND obituary_id = 0 AND param = 'obituary_search_theme'", array($domain_id));
        $theme = (!is_null($theme)) ? $theme : 'default';
        $_SESSION['theme'] = $theme;

        // $app['twig']->addFilter('paginate', new Twig_Filter_Method(new PaginatorExtension(), 'paginate'));
        $app['twig']->addFilter('paginate', new Twig_Filter_Function('paginate'));
        $app['twig']->addFilter('order', new Twig_Filter_Function('order'));
        $app['twig']->addFilter('page', new Twig_Filter_Function('page'));
        $app['twig']->addFilter('truncate', new Twig_Filter_Function('truncate'));
        $app['twig']->addFilter('thumbnail', new Twig_Filter_Function('thumbnail'));

        $app->before(function() use ($app, $domain_id) {
            $domain = new Domain($app['db'], $domain_id);
            $app['request_context']->setParameter('domain', $domain);
        });

        ob_start();
        $obituaries = require __DIR__ . '/src/obituaries.php';
        $app->mount('/', $obituaries);

        $app->run();
        $ret = ob_get_clean();

        if ($content && strpos($content, "{obituary_search_ext}") !== false) {
            $content = str_replace("{obituary_search_ext}", $ret, $content);
        }
        else {
            $content = $ret;
        }

        $content = '<link href="/obits/search/'.$theme.'/style.css" rel="stylesheet" type="text/css">'.$content;
    }
}

// Redirect old obituary links without slug
if($cnt["page_type"] == 4 && is_numeric(arg(1))) {
    $query = "SELECT * FROM plg_obituary WHERE obituary_id=" . arg(1) . " AND domain_id=$domain_id";
    $obituary = $db->getRow($query, DB_FETCHMODE_ASSOC);

    if(!is_null($obituary)) {
        $obit_page = ($page == 'online-obituary') ? 'online-obituary' : get_obituary_view_page_name();

        $slug = get_obituary_slug($obituary);

        header("HTTP/1.1 301 Moved Permanently");
        header("Location: /$obit_page/$slug/".$obituary['obituary_id']);
        exit();
    }
}

$messages = verse_format_messages();
if ($theuser->logged()) {
    $smarty->assign("member", $theuser->data);
}
$smarty->assign("messages", $messages);
$smarty->assign("content", $content);
$smarty->assign("page_type", $cnt["page_type"]);

if (!$cnt["no_header"] && !isset($_GET["print"])) {
    // include main plugin processor if not included
    @include_once("cnt_plugin_main.php");

    // fetch site parameters
    if ($uc_flag) {
        // fetch UC settings
        $query = "SELECT title, properties, css, header_mode, header, footer FROM cms_site_uc WHERE domain_id='$domain_id'";
        $site = $db->getRow($query, DB_FETCHMODE_ASSOC);
    }
    else {
        // fetch normal settings
        
        // check if structure is shared
        $group_ids = $db->getCol("SELECT lg.id FROM sms_domain_link_group lg JOIN sms_domain_link l ON lg.id=l.group_id WHERE is_shared_structure='1' AND domain_id='$domain_id'");
        if(count($group_ids)) {
            $site = $db->getRow("SELECT properties, css, header_mode, header, footer, main_menu, popup_menu FROM cms_site WHERE domain_id=(SELECT domain_id FROM sms_domain_link WHERE group_id='".$group_ids[0]."' ORDER BY id LIMIT 1 OFFSET 0)", DB_FETCHMODE_ASSOC);
            $site_prop_google = $db->getRow("SELECT title, google_stats FROM cms_site WHERE domain_id='$domain_id'", DB_FETCHMODE_ASSOC);

            $site = array_merge($site, $site_prop_google);
        } else {
            $query = "SELECT title, properties, css, header_mode, header, footer, main_menu, popup_menu, google_stats FROM cms_site WHERE domain_id='$domain_id'";
            $site = $db->getRow($query, DB_FETCHMODE_ASSOC);
        }
    }

    // custom headers
    if ($cnt["custom_header"] > 0 && $cnt["custom_header"] < 3) {
        if ($cnt["custom_header"] == 2) { // IMAGE mode
            $site["header_mode"] = 0;
            $site["header"] = $cnt["custom_header_image"];
        }
        else { // HTML mode
            $site["header_mode"] = 1;
            $site["header"] = $cnt["custom_header_html"];
        }
    }

    // preprocess header and footer for {xxx} keywords
    if ($site["header_mode"]) {
        $site["header"] = process_tags($site["header"]);
    }
    $site["footer"] = process_tags($site["footer"]);

    if (isset($site["google_stats"]) && $site["google_stats"]) {
        $smarty->assign("google_id", $site["google_stats"]);
        $site["footer"] .= $smarty->fetch("google_analitics.tpl");
    }

    // preprocess parameters
    $site["props"] = @unserialize($site["properties"]);
    // page head issues
    if (@$cnt["title"]) {
        $site["title"] = $cnt["title"] . " - " . $site["title"];
    }
    else if ($cnt["display_name"]) {
        $site["title"] = $cnt["display_name"] . " - " . $site["title"];
    }
    if (@$cnt["desc_no_global"]) {
        $site["props"]["description"] = "";
    }
    if (@$cnt["keywords_no_global"]) {
        $site["props"]["keywords"] = "";
    }
    if (@$cnt["description"]) {
        if ($site["props"]["description"]) {
            $site["props"]["description"] = $cnt["description"] . " " . $site["props"]["description"];
        }
        else {
            $site["props"]["description"] = $cnt["description"];
        }
    }
    if (@$cnt["keywords"]) {
        if ($site["props"]["keywords"]) {
            $site["props"]["keywords"] = $cnt["keywords"] . ", " . $site["props"]["keywords"];
        }
        else {
            $site["props"]["keywords"] = $cnt["keywords"];
        }
    }
    $site["props"]["no_leftmenu"] = @$cnt["no_leftmenu"];

    // obits music
    if ($cnt["page_type"] == 4) { // obit_view
        $obituary_id = arg(2);
        $smarty->assign("obituary_id", $obituary_id);
        $obituary_music_file = get_obituary_music_file($domain_id, $obituary_id);
        if ($obituary_music_file) {
            if ($obituary_music_file != 'off') {
                $site["props"]["site_music_state"] = 1;
                $site["props"]["site_music_file"] = $obituary_music_file;
            }
            else {
                $site["props"]["site_music_state"] = 0;
            }
        }
    }

    if(!isset($configuration)) {
        include_once($_SERVER['DOCUMENT_ROOT'] . '/../symfony/config/ProjectConfiguration.class.php');
        $configuration = ProjectConfiguration::getApplicationConfiguration('backend', 'prod', false);
        sfContext::createInstance($configuration);
    }

    $sf_user = sfContext::getInstance()->getUser();

    if($sf_user->isAuthenticated() && ((method_exists($sf_user, 'isSuperAdmin') && $sf_user->isSuperAdmin()) || $user->is_super())) {
        $smarty->assign("allow_edit", true);
    } else {
        $smarty->assign("allow_edit", false);
    }

    if ($uc_flag) { // under construction or other domain, obituarues, calendar
        $smarty->assign("site", $site);
        $smarty->assign("domain_name", $domain_name);
        $smarty->assign("uc_menu", uc_menu());
        $smarty->display("layout_uc.tpl");
    }
    elseif ($embedded_mode) { // embedded mode
        $smarty->assign("site", $site);
        $smarty->assign("domain_name", $domain_name);
        $smarty->display("embed.tpl");
    }
    else { // production domain
        $site["main_menu"] = @unserialize($site["main_menu"]);
        $site["popup_menu"] = @unserialize($site["popup_menu"]);

        // fetch structure
        $query = "SELECT node_id, p.internal_name internal_name, depth, menu_type, display_name, params FROM cms_node n LEFT JOIN cms_page p USING(page_id) WHERE n.domain_id='$domain_id' ORDER BY n.ord";
        $structure = $db->getAll($query, DB_FETCHMODE_ASSOC);

        // preprocess structure
        foreach ($structure as $i => $elem) {
            $params = @unserialize($structure[$i]["params"]);
            unset($structure[$i]["params"]);
            if ($params) {
                $structure[$i] = array_merge($structure[$i], $params);
            }
            // stuff to show subtree for empty node
            // processing is on display stage
            if (!$structure[$i]["internal_name"]) {
                $structure[$i]["internal_name"] = slugify($structure[$i]["display_name"]) . "-" . $structure[$i]["node_id"];
            }
        }
        // mark current trail
        $pagename = $page;
        switch ($pagename) {
            case "obituary_view":
            case "online-obituary":
                $pagename = "obituaries";
                break;
        }
        $i = 0;
        while ($structure[$i]['internal_name'] != $pagename && $i < sizeof($structure)) {
            $i++;
        }
        if ($i < sizeof($structure)) { // found current node
            $cur_depth = $structure[$i]['depth'];
            while ($structure[$i]['depth'] > 0 && $i > 0) {
                if ($structure[$i]['depth'] == $cur_depth) {
                    $structure[$i]['active'] = 1;
                    $cur_depth--;
                }
                $i--;
            }
            if (!isset($start_index)) {
                $start_index = $i;
            }
            $structure[$i]['active'] = 1;
            $start_index = $i;
        }

        if (($structure[$start_index]['depth'] < $structure[$start_index + 1]['depth']) && isset($start_index)) {
            $show = true;
        } else {
            $show = false;
        }

        $smarty->assign("site", $site);
        $smarty->assign("domain_name", $domain_name);
        $smarty->assign("structure", $structure);
        $smarty->assign("show", $show);
        $smarty->assign("start_index", $start_index);

        $layouts = array(1 => "menuleft", 2 => "menutop", 3 => "menurighttop", 4 => "menutop2", 5 => "menutop_headerleft", 6 => "menuleft_tree", 7 => 'menutopplusleft');
        // compatibility fix
        if (!@$site["props"]["layout_id"]) $site["props"]["layout_id"] = $site["props"]["layout"];
        if ($site["props"]["layout_id"]) {
            $smarty->display("layout_" . $layouts[$site["props"]["layout_id"]] . ".tpl");
        }
        else {
            $logger->log("[VSMS] invalid layout, domain_id=$domain_id, domain_name=$domain_name, referer=" . $_SERVER["HTTP_REFERER"], PEAR_LOG_INFO);
        }
    }
}
else {
    if (!isset($_GET["pdf"])) {
        if (isset($_GET["print"])) {
            $content .= "<script>window.print();</script>";
        }
        echo $content;
    }
    else {
        spl_autoload_register(
            function($class) {
                static $classes = null;
                if ($classes === null) {
                    $classes = array(
                        'knp\\snappy\\generatorinterface' => '/GeneratorInterface.php',
                        'knp\\snappy\\abstractgenerator' => '/AbstractGenerator.php',
                        'knp\\snappy\\image' => '/Image.php',
                        'knp\\snappy\\pdf' => '/Pdf.php'
                    );
                }
                $cn = strtolower($class);
                if (isset($classes[$cn])) {
                    require __DIR__ . '/src/Knp/Snappy' . $classes[$cn];
                }
            }
        );

        $content = str_replace('<img src="/obits', '<img src="' . realpath('obits'), $content);

        $snappy = new Pdf(WKHTMLTOPDF_EXECUTABLE);
        header('Content-Type: application/pdf');
        header('Content-Disposition: attachment; filename="obituary-print.pdf"');
        echo $snappy->getOutputFromHtml($content);
    }
}

function flog($message) {
    $logfile = "logs/old_urls.log";
    $logline = "[" . date("Y-m-d H:i:s") . "] $message\n";
    file_put_contents($logfile, $logline, FILE_APPEND);
}

function pnflog($message) {
    $logfile = "logs/pagenotfound.log";
    $logline = "[" . date("Y-m-d H:i:s") . "] $message\n";
    file_put_contents($logfile, $logline, FILE_APPEND);
}

function bflog($message) {
    $logfile = "logs/badreference.log";
    $logline = "[" . date("Y-m-d H:i:s") . "] $message\n";
    file_put_contents($logfile, $logline, FILE_APPEND);
}
