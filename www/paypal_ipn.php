<?php
// PAYPAL IPN packet reciever
require("inc/config.inc.php");
require("inc/util.inc.php");
require_once("pear/DB.php");
require_once("pear/Log.php");

// init logging mechanism
$conf = array('title' => 'Log');
$logger = &Log::factory('file', 'logs/paypal_ipn.log', 'versesms');

$logger->info(print_r($_POST, true));

// write IPN packet raw data
$h = fopen('logs/raw/' . date("Ymd-His") . '.ipn', "w");
fwrite($h, serialize($_POST));
fclose($h);

// connect to DB
$db_con = createobject("db_versesms");
$db = $db_con->connect();
if (DB::isError($db)) {
    $logger->err($db->getMessage() . "\n" . $db->getUserInfo());
    // fetch and dump session data
    $session_id = @$_POST["custom"];
    if ($session_id) {
        session_id($session_id);
        session_start();
        dump_session();
    }

    header("HTTP/1.0 500 Internal Server Error");
    exit;
}

$session_id = @$_POST["custom"];
if ($session_id) {
    session_id($session_id);
    session_start();

    dump_session();

    // check for transaction data
    //	if($_SESSION["total"] != @$_POST["mc_gross"] || $_SESSION["business"] != @$_POST["receiver_email"]) {
    if ($_SESSION["business"] != @$_POST["receiver_email"]) {
        $status = "INVALID!";
    }
    else {
        $status = @$_POST["payment_status"];
    }

    $product_owner_domain_id = isset($_SESSION["product_owner_domain_id"]) ? $_SESSION["product_owner_domain_id"] : $_SESSION["domain_id"];

    // insert order
    $p["user_data"] = in(serialize($_POST));
    $p["status"] = $status;
    $p["total"] = $_POST["mc_gross"];
    $p["domain_id"] = $product_owner_domain_id;
    $p["payer"] = $_POST["payer_email"];
    $p["affiliate_id"] = @$_SESSION["affiliate_id"];
    $p["affiliate_from"] = @$_SESSION["affiliate_from"];

    if (isset($_SESSION["product_owner_domain_name"])) {
        $p["order_domain_name"] = isset($_SESSION["domain_name"]) ? $_SESSION["domain_name"] : $db->getOne("SELECT domain_name FROM sms_domain WHERE domain_id='" . $_SESSION["domain_id"] . "'");
    }
    
    $query = "INSERT plg_product_order SET " . make_set_clause($p) . ", timestamp=now()";
    $logger->info($query);
    $ret = $db->query($query);
    if (DB::isError($ret)) {
        $logger->err($ret->getMessage() . "\n" . $ret->getUserInfo());
    }
    else {
        $logger->info("DB query success");
    }

    // add domain name to $p to use in mailing
    if (isset($_SESSION["product_owner_domain_name"])) {
        $p["domain_name"] = $_SESSION["product_owner_domain_name"];
    }
    elseif (isset($_SESSION["domain_name"])) {
        $p["domain_name"] = $_SESSION["domain_name"];
    }
    else {
        $query = "SELECT domain_name FROM sms_domain WHERE domain_id='" . $product_owner_domain_id . "'";
        $p["domain_name"] = $db->getOne($query);
    }
    $p["user_data"] = $_POST;
    $mailto = $_SESSION["business"];
    if (check_email($mailto)) {
        send_order_notification($mailto, $p);
    }
    // notification to default domain's email if it is different
    $query = "SELECT email FROM sms_domain WHERE domain_id='" . $product_owner_domain_id . "'";
    $domain_email = $db->getOne($query);
    if ($domain_email !== $mailto && check_email($domain_email)) {
        send_order_notification($domain_email, $p);
    }

    unset($_SESSION["cart"]);
    unset($_SESSION["order_id"]);
    unset($_SESSION["total"]);
    unset($_SESSION["business"]);
    unset($_SESSION["product_owner_domain_name"]);
}
else {
    $logger->err("paypal IPN packet: session_id='$session_id' not found!");
}

function dump_session() {
    $session_id = session_id();
    $h = fopen('logs/raw/' . $session_id . '.ses', "w");
    fwrite($h, serialize($_SESSION));
    fclose($h);
}

function send_order_notification($mailto, $p) {
    global $logger;

    $data = $p["user_data"];
    $order_domain = isset($p["order_domain_name"]) ? "\nOrder Domain: ".$p["order_domain_name"]."\n" : '';

    $subject = $p["domain_name"] . ": new order notification";
    $body_template = <<<ORDERDETAILS
Order status: %s

User details:
%s

Order details:
%s

Shipping: $%s
Taxes:    $%s
Total:    $%s


Always yours, {$p["domain_name"]}'s notification robot.

ORDERDETAILS;
    $fields = array("Name" => "address_name", "Street" => "address_street", "City" => "address_city", "State" => "address_state", "ZIP" => "address_zip", "Country" => "address_country", "Email" => "payer_email", "Pending reason" => "pending_reason", "Memo" => "memo");
    $user_details = "";
    foreach ($fields as $title => $field) {
        $user_details .= "$title: " . $data[$field] . "\n";
    }

    $order_details = "";
    $i = 1;
    while (isset($data["item_name$i"])) {
        $order_details .= $data["item_name$i"] . " x " . $data["quantity$i"] . " $" . $data["mc_gross_$i"] . "\n";
        $i++;
    }

    $body = sprintf($body_template, $p["status"], $user_details, $order_details, $data["mc_shipping"], $data["tax"], $data["mc_gross"], $order_domain);

    $robot_email = "robot@twintierstech.net";
    $ret = mail($mailto, $subject, $body, "From: $robot_email");
    if ($ret) {
        $status = "sent";
    }
    else {
        $status = "failed to send";
    }

    $logger->info("status: $status, mailing: $mailto, $subject, $body");
}
