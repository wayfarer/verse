<?php
 // obsolete
// backend statistics integration
include("inc/verse.inc.php"); //main header - initializes Verse environment
include("inc/piwik_adapter.inc.php"); //main header - initializes Verse environment

// some change here

if ($user->have_role(ROLE_STATISTICS_MANAGEMENT)) {
    log_usage('statistics', $domain_name, $user->logged_id);

    $action = @$_POST["action"];
    switch ($action) {
        default:
            if (!$user->data['piwik_token_auth']) {
                // add the user to the piwik users
                $piwik_token_auth = piwik_ensure_user($user->data['user_id'], $user->data['password']);
                $user->set_data('piwik_token_auth', $piwik_token_auth);
            }

            $currentPeriod = @$_GET['period'];
            $today = 0;
            if (isset($_GET['date'])) {
                $today = strtotime($_GET['date']);
            }
            if (!$today) {
                $today = time() - 86400; // yesterday
            }

            $availablePeriods = array('day', 'week', 'month', 'year');
            if (!in_array($currentPeriod, $availablePeriods)) {
                $currentPeriod = 'day';
            }
            $periodNames = array(
                'day' => array('singular' => 'Day', 'plural' => 'Days'),
                'week' => array('singular' => 'Week', 'plural' => 'Weeks'),
                'month' => array('singular' => 'Month', 'plural' => 'Monthes'),
                'year' => array('singular' => 'Year', 'plural' => 'Years')
            );

            $found = array_search($currentPeriod, $availablePeriods);
            if ($found !== false) {
                unset($availablePeriods[$found]);
            }

            $period = $currentPeriod;

            $minDate = strtotime("2010-01-01");
            $maxDate = time();

            switch ($period) {
                case 'day':
                    $smarty->assign('prettyDate', date('l d F Y', $today));
                    break;
                case 'week':
                    $last_monday =
                            $smarty->assign('prettyDate', date('W \o\f Y', $today));
                    break;
                case 'month':
                    $smarty->assign('prettyDate', date('F Y', $today));
                    break;
                case 'year':
                    $smarty->assign('prettyDate', date('Y', $today));
                    break;
            }

            $user_id = $user->data['user_id'];
            $query = "SELECT keep FROM sms_stats_keepmv WHERE domain_id='$domain_id' AND user_id='$user_id'";
            $keep_phpmv = $db->getOne($query);
            // var_dump($keep_phpmv);

            $smarty->assign('minDateYear', date("Y", $minDate));
            $smarty->assign('minDateMonth', date("m", $minDate));
            $smarty->assign('minDateDay', date("d", $minDate));

            $smarty->assign('maxDateYear', date("Y", $maxDate));
            $smarty->assign('maxDateMonth', date("m", $maxDate));
            $smarty->assign('maxDateDay', date("d", $maxDate));

            $smarty->assign('date', date("Y-m-d", $today));

            $smarty->assign('period', $currentPeriod);
            $smarty->assign('otherPeriods', $availablePeriods);
            $smarty->assign('periodsNames', $periodNames);
            $smarty->assign('login', $user->data['login']);
            $smarty->assign('user_issuper', $user->is_super());
            $smarty->assign('token_auth', $user->data['piwik_token_auth']);
            $smarty->assign('keep_phpmv', $keep_phpmv);
            $smarty->display("statistics.tpl");
            break;
        case "keep_phpmv":
            $keep = intval($_POST["keep_phpmv"]) ? 1 : 0;
            $user_id = $user->data['user_id'];
            $query = "REPLACE sms_stats_keepmv SET domain_id='$domain_id', user_id='$user_id', keep='$keep', emailed=0";
            $db->query($query);
            exit;
    }
}
else {
    header("Location: login.php");
}