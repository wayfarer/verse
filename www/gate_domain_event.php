<?php
 // script for updating domain from remote server
// uses symfony core
$allowed_ips = array('66.135.56.26');
if (in_array($_SERVER['REMOTE_ADDR'], $allowed_ips)) {
    require_once($_SERVER['DOCUMENT_ROOT'] . '/../symfony/config/ProjectConfiguration.class.php');

    $configuration = ProjectConfiguration::getApplicationConfiguration('backend', 'dev', false);
    $context = sfContext::createInstance($configuration);

    //    error_reporting(E_ALL);
    //    ini_set('display_errors', 1);

    $domain = unserialize($_POST['domain']);
    // preserve values from remote server
    $fields = $domain->exportTo('array');
    // get current values from DB
    $domain->refresh();
    foreach ($fields as $field => $value) {
        // next detects if we have changed value from remote server, those only saves changed fields to DB
        $domain->set($field, $value);
    }

    vlog("Domain transferred: " . $domain->getDomainId() . ', ' . $domain->getDomainName());
    vlog('modified: ' . print_r($domain->getModified(), 1));

    try {
        $domain->save();
    } catch (Exception $e) {
        vlog("Exception while saving object: " . $e->getMessage());
        exit;
    }
}
else {
    vlog("Warning! Unauthorized access from " . $_SERVER['REMOTE_ADDR']);
}

function vlog($message) {
    echo $message;
    $logfile = "logs/gate_domain_event.log";
    $logline = "[" . date("Y-m-d H:i:s") . "] $message\n";
    file_put_contents($logfile, $logline, FILE_APPEND);
}
