<?
require("inc/config.inc.php");
require("inc/util.inc.php");
require_once("pear/Log.php");

// init logging mechanism
$conf = array('title' => 'Log');
$logger = &Log::factory('file', 'logs/paypal.log', 'versesms');

$logger->info(print_r($_POST, true));

$session_id = @$_POST["custom"];
if ($session_id) {
    session_id($session_id);
    session_start();

    unset($_SESSION["cart"]);
    unset($_SESSION["order_id"]);
    unset($_SESSION["total"]);
    unset($_SESSION["business"]);

    header("Location: http://" . $_SESSION["domain_name"] . "/?p=" . $_SESSION["return_page"]);
}
