<?php
 // frontend ajax funeral firms display, for nsfda.org
include("inc/verse.inc.php"); //main header - initializes Verse environment

$rep = createobject("report_ajax", array($db, "plg_funeral_firm", REP_NO_PAGENATOR, REP_NO_HEADING));

$rep->add_field("Firm", REP_STRING_TEMPLATE, '{name}', REP_ORDERABLE);
$rep->add_field("Address", REP_STRING_TEMPLATE, '{address}', REP_UNORDERABLE);
$rep->add_field("City", REP_STRING_TEMPLATE, '{city}', REP_ORDERABLE);
$rep->add_field("State", REP_STRING_TEMPLATE, '{state}', REP_ORDERABLE);
$rep->add_field("Zip", REP_STRING_TEMPLATE, '{zip}', REP_ORDERABLE);
$rep->add_field("Phone", REP_STRING_TEMPLATE, '{phone}', REP_ORDERABLE);
$rep->add_field("Fax", REP_STRING_TEMPLATE, '{fax}', REP_ORDERABLE);
$rep->add_field("Url", REP_STRING_TEMPLATE, '{url}', REP_ORDERABLE);
$rep->add_field("County", REP_STRING_TEMPLATE, '{county}', REP_ORDERABLE);

$rep->html_attributes('width="550px"');

$rep->order_by("county, city");

$rep->handle_events($_POST);

$data = $rep->make_report_array();

$prev_city = "";
$prev_county = "";
echo "<table width=\"100%\"><tr>";
echo "<td width=\"50%\"><b><big>Nassau</big></b></td><td><b><big>Suffolk</big></b></td></tr><tr valign=\"top\">";
foreach ($data as $line) {
    $line = $line["data"];
    if ($prev_county != $line["County"]) {
        if ($prev_county) {
            echo "</td>";
        }
        echo "<td>";
        $prev_county = $line["County"];
    }
    if ($prev_city != $line["City"]) {
        echo "<b><u>", $line["City"], "</u></b><br>";
        $prev_city = $line["City"];
    }
    echo "<b>", $line["Firm"], "</b><br>";
    echo $line["Address"], "<br>", $line["City"], ", ", $line["State"], " ", $line["Zip"], "<br>";
    echo "Phone: ", $line["Phone"], "<br>Fax: ", $line["Fax"], "<br>";
    if ($line["Url"]) {
        echo "<a href=\"http://" . $line["Url"] . "\" target=\"_blank\">", $line["Url"], "</a><br>";
    }
    echo "<br>";
}
echo "</td></tr></table>";
