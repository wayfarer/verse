<?php
 // plugin for checkout page
include "cnt_plugin_main.php";

function process_content($content, $state) {
    global $db, $domain_id, $domain_name;

    if (count($_POST)) {
        if (!strlen($_POST["email"])) $GLOBALS["errors"]["email"] = "Email required";
        if (!strlen($_POST["firstname"])) $GLOBALS["errors"]["firstname"] = "Firstname required";
        if (!strlen($_POST["lastname"])) $GLOBALS["errors"]["lastname"] = "Lastname required";
        if (!strlen($_POST["phone"])) $GLOBALS["errors"]["phone"] = "Phone required";

        if (!count($GLOBALS["errors"])) {
            // save order here
            $query = "SELECT product_id, name, price FROM plg_product WHERE product_id IN(" . implode(",", array_keys($_SESSION["cart"])) . ")";
            $products = $GLOBALS["db"]->getAssoc($query, false, null, DB_FETCHMODE_ASSOC);

            // count order total
            $total = 0;
            foreach ($_SESSION["cart"] as $product_id => $count) {
                $total += $products[$product_id]["price"] * $count;
            }
            // create user_data
            //		    	$fields = array("email","firstname","lastname","company","phone","address1","address2","city","state","zip","country");
            $fields = array("E-mail" => "email", "First Name" => "firstname", "Last Name" => "lastname", "Company" => "company", "Phone" => "phone", "Address1" => "address1", "Address2" => "address2", "City" => "city", "State" => "state", "ZIP" => "zip", "Country" => "country");
            $u = array();
            $userinfo_text = "";
            foreach ($fields as $title => $field) {
                $u[$field] = @$_POST[$field];
                $userinfo_text .= $title . ": " . $u[$field] . "\n";
            }
            $query = "INSERT plg_product_order SET domain_id='" . $GLOBALS["domain_id"] . "', timestamp=now(), total='$total', user_data='" . in(serialize($u)) . "'";
            $db->query($query);
            $order_id = $db->getOne("SELECT last_insert_id()");
            foreach ($_SESSION["cart"] as $product_id => $count) {
                $query = "INSERT plg_product_order_product SET order_id='$order_id', product_id='$product_id', count='$count'";
                $db->query($query);
            }
            // prepare order info
            $order_text = "";
            $query = "SELECT p.name name, p.price price, count FROM plg_product_order o INNER JOIN plg_product_order_product USING(order_id) INNER JOIN plg_product p USING(product_id) WHERE o.order_id='$order_id' AND o.domain_id='$domain_id'";
            $ret = $db->getAll($query, DB_FETCHMODE_ASSOC);
            foreach ($ret as $row) {
                $order_text .= $row["name"] . "  $" . $row["price"] . " x " . $row["count"] . " = $" . $row["count"] * $row["price"] . "\n";
            }
            $order_text .= "Total: $" . $total . "\n";
            // send email notifications
            // one to admin
            $query = "SELECT email FROM sms_domain WHERE domain_id='$domain_id'";
            $site_email = $db->getOne($query);
            //		    	$site_email = "vladimir@mail.by";
            $emailto = $site_email;
            $subj = "New order";
            $text = "Customer info:\n------------------------\n" . $userinfo_text . "\n\nOrder info:\n-------------------------\n" . $order_text;
            mail($emailto, $subj, $text, "From: $emailto");
            // and one to customer
            $emailto = $u["email"];
            $subj = "Your $domain_name order";
            $text = "Your order#$order_id:\n--------------------------------\n" . $order_text;
            mail($emailto, $subj, $text, "From: $emailto");

            // clear cart
            $_SESSION["cart"] = array();
            $GLOBALS["smarty"]->assign("thanks", 1);
        }
        else { // validation errors
            $GLOBALS["smarty"]->assign("errors", $GLOBALS["errors"]);
            $GLOBALS["smarty"]->assign("input", $_POST);
        }
    }

    if (!count($_POST) || count($GLOBALS["errors"])) {
        $query = "SELECT product_id, name, price FROM plg_product WHERE product_id IN(" . implode(",", array_keys($_SESSION["cart"])) . ")";
        $products = $GLOBALS["db"]->getAssoc($query, false, null, DB_FETCHMODE_ASSOC);
        $data = array();
        $total = 0;
        foreach ($products as $product_id => $vals) {
            $products[$product_id]["count"] = $_SESSION["cart"][$product_id];
            $products[$product_id]["amount"] = $vals["price"] * $products[$product_id]["count"];
            $total += $products[$product_id]["amount"];
        }

        $GLOBALS["smarty"]->assign("cart", $products);
        $GLOBALS["smarty"]->assign("total", $total);
    }

    $html = $GLOBALS["smarty"]->fetch("cnt_plugin_checkout.tpl");
    return $html;
}
