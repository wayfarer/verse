<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<head>
<?php include_stylesheets() ?>
<?php include_javascripts() ?>
</head>
<body>
<?php
  $obituary = Doctrine_Core::getTable('PlgObituary')->getCurrent();
  $config = $obituary->getConfig();
  // filesystem path
  $icons_path = 'obits/icons/'.$config['obituary_icons_theme'].'/';
  $icons = sfYaml::load($icons_path.'theme.yml');
  // fix it for the web path
  $icons_path = '/'.$icons_path;
?>
<style type="text/css" media="screen">
.verse-obit-print-holder {
  width: 700px;
}
</style>
<style type="text/css">
.verse-obit-workarea-outer {
  clear: left;
}
.verse-obit-workarea {
  padding: 10px;
}
.verse-obit-photo {
  margin-bottom: 0;
}
.verse-obit-inner {
  overflow: visible;
}
</style>
<div class='verse-obit-print-holder'>
<div id="verse-obit">
  <div class="verse-obit-border-1">
  <div class="verse-obit-border-2">
  <div class="verse-obit-border-3">
  <div class="verse-obit-border-4">
  <div class="verse-obit-border-5">
  <div class="verse-obit-border-6">
  <div class="verse-obit-border-7">
  <div class="verse-obit-border-8">
    <div class="verse-obit-inner">
      <h1><span class="verse-obit-title-span"><?php echo $obituary['name']?></span></h1>
      <div class="verse-obit-left">
      <div class="verse-obit-left-inner">
        <div class="verse-obit-photo">
          <?php echo image_tag(VerseUtil::obit_photo_thumbnail($obituary->getImage(), 180)) ?>
        </div>
      </div>
      </div>
      <div class="verse-obit-main">
      <div class="verse-obit-main-inner">
        <h2>Funeral Information</h2>
        <div class="verse-obit-splitter"></div>
        <div class="verse-obit-overflow">
	        <div class="verse-obit-field-group">
	          <?php if($config['field_home_place_enabled'] && $obituary['home_place']):?>
            <div class="verse-obit-field">
	            <span class="verse-obit-field-title"><?php echo $config['field_home_place_title']?>:</span>
	            <span class="verse-obit-field-content"><?php echo $obituary['home_place']?></span>
	          </div>
            <?php endif ?>
            <?php if($config['field_death_date_enabled'] && $obituary['death_date']):?>
	          <div class="verse-obit-field">
	            <span class="verse-obit-field-title"><?php echo $config['field_death_date_title']?>:</span>
	            <span class="verse-obit-field-content"><?php echo date('F d, Y', strtotime($obituary['death_date']))?></span>
	          </div>
            <?php endif ?>
	        </div>
	        <div class="verse-obit-field-group">
            <?php if($config['field_birth_place_enabled'] && $obituary['birth_place']):?>
	          <div class="verse-obit-field">
	            <span class="verse-obit-field-title"><?php echo $config['field_birth_place_title']?>:</span>
	            <span class="verse-obit-field-content"><?php echo $obituary['birth_place']?></span>
	          </div>
            <?php endif ?>
            <?php if($config['field_birth_date_enabled'] && $obituary['birth_date']):?>
	          <div class="verse-obit-field">
	            <span class="verse-obit-field-title"><?php echo $config['field_birth_date_title']?>:</span>
	            <span class="verse-obit-field-content"><?php echo date('F d, Y', strtotime($obituary['birth_date']))?></span>
	          </div>
            <?php endif ?>
            <?php if($config['field_age_enabled'] && $obituary['birth_date'] && $obituary['death_date']):?>
	          <div class="verse-obit-field">
	            <span class="verse-obit-field-title"><?php echo $config['field_age_title']?>:</span>
	            <span class="verse-obit-field-content"><?php echo $obituary['age']?></span>
	          </div>
            <?php endif ?>
	        </div>
        </div>
        <div class="verse-obit-splitter"></div>
        <?php if($config['field_service_enabled'] && ($obituary['service_place'] || $obituary['service_date'])):?>
        <div class="verse-obit-field">
          <span class="verse-obit-field-title"><?php $st = PlgObituaryTable::getServiceTypes(); echo sprintf($config['field_service_title'], $st[$obituary['service_type']])?>:</span>
          <span class="verse-obit-field-content"><?php if($obituary['service_date']) echo date('l, F d, Y', $obituary['service_date'])?> <?php echo $obituary['service_time']?> <?php echo $obituary['service_place']?></span>
        </div>
        <?php endif ?>
        <?php if($config['field_visitation_enabled'] && ($obituary['visitation_date'] || $obituary['visitation_time'] || $obituary['visitation_place'])):?>
        <div class="verse-obit-field">
          <span class="verse-obit-field-title"><?php echo $config['field_visitation_title']?>:</span>
          <span class="verse-obit-field-content"><?php if($obituary['visitation_date']) echo date('l, F j, Y', $obituary['visitation_date'])?> <?php echo $obituary['visitation_time']?> <?php echo $obituary['visitation_place']?></span>
        </div>
        <?php endif ?>
        <?php if($config['field_interment_enabled'] && $obituary['final_disposition']):?>
        <div class="verse-obit-field">
          <span class="verse-obit-field-title"><?php echo $config['field_interment_title']?>:</span>
          <span class="verse-obit-field-content"><?php echo $obituary['final_disposition']?></span>
        </div>
        <?php endif ?>
        <div class="verse-obit-splitter"></div>
      </div>
      </div>
      <div class="verse-obit-workarea-outer">
	      <div class="verse-obit-workarea">
  				<?php include_partial('obituaries/flashes') ?>
	  			<?php echo $sf_content ?>
	      </div>
      </div>
    </div>
  </div>
  </div>
  </div>
  </div>
  </div>
  </div>
  </div>
  </div>
</div>
</div>
</body>