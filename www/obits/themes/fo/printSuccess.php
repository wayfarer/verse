<h2>Biography</h2>
<div class="verse-obit-text">
  <?php echo $obituary->getObitText(ESC_RAW)?>
</div>
<div class="verse-obit-spacer"></div>

<?php if(count($candles)): ?>
<h2><?php echo $icons['actions']['viewallcandles']['content_header']?></h2>
<?php foreach($candles as $candle): ?>
  <div class="verse-obit-candle">
    <div class="verse-obit-candle-text">
      <div class="verse-obit-candle-text-image"><img src="/obits/themes/fo/img/candle.png"></div>
      <div class="verse-obit-candle-text-message">
          <div class="verse-obit-candle-thoughts-quote-open"><img src="/obits/themes/fo/img/q_open.png"></div>
          <div class="verse-obit-candle-thoughts-text"><?php echo $candle['thoughts']?></div>
          <div class="verse-obit-candle-thoughts-quote-close"><img src="/obits/themes/fo/img/q_close.png"></div>
      </div>
    </div>
    <div class="verse-obit-candle-name">
      <strong><?php echo $candle['name']?></strong>
      <?php echo $candle['timestamp']?>
    </div>
  </div>
<?php endforeach ?>
<?php endif?>
<script type="text/javascript">
jQuery( function() {
  window.print();
} );
</script>
