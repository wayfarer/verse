<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<?php
  $obituary = Doctrine_Core::getTable('PlgObituary')->getCurrent();
  $config = $obituary->getConfig();
  if(isset($config['flowers_page_type']) && $config['flowers_page_type'] == 2) {
      $flowers_page = sfConfig::get('app_florist_one_page');
  } else {
      $flowers_page = $config['flowers_page'];
  }

  $current_path = $_REQUEST['p'];

  //redirect to candles page
  if ($current_path == 'online-obituary/'.$obituary['obituary_id']) {
      if($config['candles_policy']) {
          header("Location: /online-obituary/".$obituary['obituary_id']."/messages");
          exit;
      }
      if($config['media'] && !$config['disable_media']) {
          header("Location: /online-obituary/".$obituary['obituary_id']."/viewmedia");
          exit;
      }
      $obituary_main = true;
  } else {
      $obituary_main = false;
  }
  $current_path = '/'.$current_path;
  
  // filesystem path
  $icons_path = 'obits/icons/'.$config['obituary_icons_theme'].'/';
  $icons = sfYaml::load($icons_path.'theme.yml');
  // fix it for the web path
  $icons_path = '/'.$icons_path;

  if(isset($config['addthis_username'])) {
      $addthis_username = $config['addthis_username'];
  } else {
      $addthis_username = 'xa-4c51345c3303423a';
  }
?>
<head>
<?php include_stylesheets() ?>
<link media="screen" type="text/css" href="/css/highslide.css" rel="stylesheet">
<!--[if lte IE 6 ]>
    <link media="screen" type="text/css" href="/css/highslide-ie6.css" rel="stylesheet">
<![endif]-->
<?php include_javascripts() ?>
<script type="text/javascript" src="/js/highslide-with-gallery.js"></script>
<script type="text/javascript">
    hs.graphicsDir = '/css/graphics/';
    hs.align = 'center';
    hs.transitions = ['expand', 'crossfade'];
    hs.wrapperClassName = 'dark borderless floating-caption';
    hs.fadeInOut = true;
    hs.dimmingOpacity = .75;
    // Add the controlbar
    if (hs.addSlideshow) hs.addSlideshow({
        interval: 5000,
        repeat: false,
        useControls: true,
        fixedControls: 'fit',
        overlayOptions: {
            opacity: .6,
            position: 'bottom center',
            hideOnMouseOut: true
        }
    });
</script>
<script type="text/javascript">
    jQuery(function() {
        $('.verse-obit-left').height($('.verse-obit-left').parent().height());
    });
</script>
<?php if($obituary->GoogleMapsExists()): ?>
    <script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?sensor=false"></script>
    <script type="text/javascript">
        jQuery(function() {
            var mainMap;
            var geocoder = null;
            var myOptions = {
                zoom: 15,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            }

            $('.show-map').bind('click', function() {
                var type = $(this).attr('type');
                if (type == 'service') {
                    var address = '<?= $obituary->_data['service_google_addr'] ?>';
                } else {
                    var address = '<?= $obituary->_data['finaldisposition_google_addr'] ?>';
                }

                if (document.getElementById) {
                    if (address != '') {
                        mainMap = new google.maps.Map(document.getElementById("map-"+type), myOptions);
                        setCenter(mainMap, address, type);
                    }
                }
                $(this).hide();
                $(this).next().show();
                return false;
            });

            $('.hide-map').bind('click', function() {
                var type = $(this).attr('type');
                $("#map-"+type).hide();
                $("#mapDirections-"+type).hide();
                $(this).hide();
                $(this).prev().show();
                return false;
            });

            function setCenter(mainMap, locationAddress, type) {
                geocoder = new google.maps.Geocoder();
                if (geocoder) {
                    geocoder.geocode({ 'address': locationAddress}, function(results, status) {
                        if (status == google.maps.GeocoderStatus.OK) {
                            $("#map-"+type).show();
                            google.maps.event.trigger(mainMap, 'resize');
                            mainMap.setZoom(mainMap.getZoom());
                            mainMap.setCenter(results[0].geometry.location);
                            var marker = new google.maps.Marker({
                                map: mainMap,
                                position: results[0].geometry.location
                            });
                            $("#mapDirections-" + type).show();
                            $("#mapDirections-" + type + " a").attr("href", "http://maps.google.com/maps?saddr=&daddr=" + locationAddress);
                        }
                    });
                }
            }
        });
    </script>
<?php endif; ?>
</head>
<body>
<div id="verse-obit">
    <div class="verse-obit-name">
        <h1>
            <a href="<?php echo url_for('obituaries', $obituary) ?>">
                <?php echo $obituary['name']?>
            </a>
        </h1>
    </div>
    <div class="verse-obit-print"><?php echo link_to('Print', '/online-obituary/'.$obituary->getObituarySlug().'/'.$obituary->getObituaryId().'/print', array('query_string' => 'pdf')) ?></div>
    <div class="verse-obit-inner">
      <div class="verse-obit-left">
      <div class="verse-obit-left-inner">
        <div class="verse-obit-photo-container">
            <?php if($obituary->getImage()):?>
                <div class="verse-obit-photo">
                    <a href="<?= $obituary['image'] ?>" class="verse-obit-photo-image" onclick="return hs.expand(this, { slideshowGroup: 'obit-photo' })"><?php echo image_tag(VerseUtil::obit_photo_thumbnail($obituary->getImage(), 180), array('raw_name'=>true)) ?></a>
                </div>
            <?php endif ?>
            <?php if($config['field_death_date_enabled'] && $obituary['death_date']):?>
	            <div class="verse-obit-field verse-obit-death-date">
                    <span class="verse-obit-field-content">Date of Death</span>
	                <span class="verse-obit-field-content"><?php echo date('n/j/Y', strtotime($obituary['death_date']))?></span>
	            </div>
            <?php endif ?>
        </div>
        <h2>Funeral Information</h2>
        <div class="verse-obit-splitter"></div>
        <div class="verse-obit-overflow">
	        <div class="verse-obit-field-group">
	            <?php if($config['field_home_place_enabled'] && $obituary['home_place']):?>
                    <div class="verse-obit-field">
	                    <span class="verse-obit-field-title"><?php echo $config['field_home_place_title']?>:</span>
                        <span class="verse-obit-field-content"><?php echo $obituary['home_place']?></span>
	                </div>
                <?php endif ?>
	        </div>
	        <div class="verse-obit-field-group">
                <?php if($config['field_birth_place_enabled'] && $obituary['birth_place']):?>
	                <div class="verse-obit-field">
	                    <span class="verse-obit-field-title"><?php echo $config['field_birth_place_title']?>:</span>
	                    <span class="verse-obit-field-content"><?php echo $obituary['birth_place']?></span>
	                </div>
                <?php endif ?>
                <?php if($config['field_birth_date_enabled'] && $obituary['birth_date']):?>
	                <div class="verse-obit-field">
	                    <span class="verse-obit-field-title"><?php echo $config['field_birth_date_title']?>:</span>
	                    <span class="verse-obit-field-content"><?php echo date('F j, Y', strtotime($obituary['birth_date']))?></span>
	                </div>
                <?php endif ?>
                <?php if($config['field_age_enabled'] && $obituary['birth_date'] && $obituary['death_date']):?>
	                <div class="verse-obit-field">
                        <span class="verse-obit-field-title"><?php echo $config['field_age_title']?>:</span>
	                    <span class="verse-obit-field-content"><?php echo $obituary['age']?></span>
	                </div>
                <?php endif ?>
	        </div>
        </div>
        <?php if($config['field_service_enabled'] && ($obituary['service_place'] || $obituary['service_date'])):?>
            <div class="verse-obit-field">
                <span class="verse-obit-field-title"><?php $st = PlgObituaryTable::getServiceTypes(); echo sprintf($config['field_service_title'], $st[$obituary['service_type']])?>:</span>
                <span class="verse-obit-field-content"><?php if($obituary['service_date']) echo date('l, F j, Y', $obituary['service_date'])?> <?php echo $obituary['service_time']?> <?php echo $obituary['service_place']?></span>
                <?php if($config['google_maps'] && !$config['disable_google_maps'] && $obituary['service_google_method'] == 'Address' && !is_null($obituary['service_google_addr']) && $obituary['service_google_addr']): ?>
                    <p><a type="service" class="show-map" href="#">Show map</a><a type="service" class="hide-map" href="#">Hide map</a></p>
                    <div id="map-service" class="google-map"></div>
                    <p id="mapDirections-service" style="display:none;"><a target="_blank">Get Driving Directions</a></p>
                <?php endif ?>
            </div>
        <?php endif ?>
        <?php if($config['field_visitation_enabled'] && ($obituary['visitation_date'] || $obituary['visitation_time'] || $obituary['visitation_place'])):?>
            <div class="verse-obit-field">
                <span class="verse-obit-field-title"><?php echo $config['field_visitation_title']?>:</span>
                <span class="verse-obit-field-content"><?php if($obituary['visitation_date']) echo date('l, F j, Y', $obituary['visitation_date'])?> <?php echo $obituary['visitation_time']?> <?php echo $obituary['visitation_place']?></span>
            </div>
        <?php endif ?>
        <?php if($config['field_interment_enabled'] && $obituary['final_disposition']):?>
            <div class="verse-obit-field">
                <span class="verse-obit-field-title"><?php echo $config['field_interment_title']?>:</span>
                <span class="verse-obit-field-content"><?php echo $obituary['final_disposition']?></span>
                <?php if($config['google_maps'] && !$config['disable_google_maps'] && $obituary['finaldisposition_google_method'] == 'Address' && !is_null($obituary['finaldisposition_google_addr']) && $obituary['finaldisposition_google_addr']): ?>
                    <p><a type="finaldisposition" class="show-map" href="#">Show map</a><a type="finaldisposition" class="hide-map" href="#">Hide map</a></p>
                    <div id="map-finaldisposition" class="google-map"></div>
                    <p id="mapDirections-finaldisposition" style="display:none;"><a target="_blank">Get Driving Directions</a></p>
                <?php endif ?>
            </div>
        <?php endif ?>
        <div class="verse-obit-biography">
            <h2>Biography</h2>
            <div class="verse-obit-splitter"></div>
			<?php echo $obituary['obit_text']; ?>
        </div>
        <div class="verse-obit-share">
	        <!-- AddThis Button BEGIN -->
			<div class="addthis_toolbox addthis_default_style">
			  <a href="http://addthis.com/bookmark.php?v=250&amp;username=<?php echo $addthis_username; ?>" class="addthis_button_compact">Share</a>
			  <span class="addthis_separator">|</span>
			  <a class="addthis_button_email"></a>
			  <a class="addthis_button_facebook"></a>
			  <a class="addthis_button_twitter"></a>
			</div>
			<script type="text/javascript" src="http://s7.addthis.com/js/250/addthis_widget.js#username=<?php echo $addthis_username; ?>"></script>
			<!-- AddThis Button END -->
        </div>
      </div>
      </div>
      <div class="verse-obit-main">
      <div class="verse-obit-main-inner">
        <div class="verse-obit-actions">
          <?php if($config['candles_policy']):?>
            <a href="<?php echo url_for('obituaries_newcandle', $obituary) ?>" class="verse-obit-action<?php if(url_for('obituaries_newcandle', $obituary) == $current_path) echo '-active'; ?> verse-obit-action-ajax light-a-candle"><div><?php echo $icons['actions']['lightacandle']['title'] ?></div></a>
            <a href="<?php echo url_for('obituaries_allcandles', $obituary) ?>" class="verse-obit-action<?php if(url_for('obituaries_allcandles', $obituary) == $current_path) echo '-active'; ?> verse-obit-action-ajax view-all-candles"><div><?php echo $icons['actions']['viewallcandles']['title']?> (<?php echo $obituary->getCandlesCount()?>)</div></a>
          <?php endif?>
          <?php if($config['media'] && !$config['disable_media']): ?>
            <a href="<?php echo url_for('obituaries_addmedia', $obituary) ?>" class="verse-obit-action<?php if(url_for('obituaries_addmedia', $obituary) == $current_path) echo '-active'; ?> verse-obit-action-ajax add-media"><div><?php echo $icons['actions']['addmedia']['title']?></div></a>
            <a href="<?php echo url_for('obituaries_viewmedia', $obituary) ?>" class="verse-obit-action<?php if(url_for('obituaries_viewmedia', $obituary) == $current_path) echo '-active'; ?> verse-obit-action-ajax view-media"><div><?php echo $icons['actions']['viewmedia']['title']?> (<?php echo $obituary->getMediaCount()?>)</div></a>
          <?php endif?>
          <?php if($config['flowers_page']):?>
            <a href="/<?php echo $flowers_page?>" class="verse-obit-action send-flowers"><div><?php echo $icons['actions']['sendflowers']['title']?></div></a>
          <?php endif?>
          <?php if($config['candles_policy']):?>
            <a href="<?php echo url_for('obituaries_update_subscribe', $obituary) ?>" class="verse-obit-action<?php if(url_for('obituaries_update_subscribe', $obituary) == $current_path) echo '-active'; ?> verse-obit-action-ajax subscribe-for-candles"><div><?php echo $icons['actions']['subscribe']['title']?></div></a>
          <?php endif?>

        </div>

        <?php if($obituary->hasSlides()): ?>
          <div class="verse-obit-scrapbook">
            <a href="#" onclick="return openwin('/<?php echo Doctrine_Core::getTable("CmsPage")->getScrapbookPage()?>/<?php echo $obituary->getObituaryId()?>')">click to view photoalbum</a>
          </div>
        <?php endif;?>
        <?php foreach($obituary->getMovieclips() as $movieclip):?>
          <div class="verse-obit-movieclip">
            <a href="#" onclick="return openwin('/mcplayer.php?id=<?php echo $movieclip->getMovieclipId()?>&detectflash=false', 475, 390)"><?php echo $movieclip->getTitle()?></a>
          </div>
        <?php endforeach;?>
        <div class="verse-obit-spacer"></div>

        <div class="verse-obit-progress">loading...</div>
        <div class="verse-obit-workarea-outer">
	        <div class="verse-obit-workarea">
			    <?php include_partial('obituaries/flashes') ?>
                <?php if(!$obituary_main): ?>
                    <?php echo $sf_content ?>
                <?php endif; ?>
	        </div>
        </div>

      </div>
      </div>
    </div>
</div>
<script>
jQuery( function() {
  $('.verse-obit-info').animate({ backgroundColor: "#FF0"}, 1500)
                       .animate({ backgroundColor: "#FFF"}, 1000);
} );
</script>
</body>