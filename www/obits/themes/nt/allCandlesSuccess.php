<?php if(count($candles)>3):?>
<a href="javascript:history.go(-1)" class="verse-obit-backlink">&laquo; back</a>
<div class="verse-obit-spacer"></div>
<?php endif ?>
<h2><?php echo $icons['actions']['viewallcandles']['content_header']?></h2>
<?php if(count($candles)):?>
<div class="verse-obit-candles-order">
<?php if($order=='desc'): ?>
<a href="<?php echo url_for('@obituaries_allcandles?order=asc&obituary_id='.$obituary->getObituaryId().'&obituary_slug='.$obituary->getObituarySlug() )?>">show old to new</a>
<?php else: ?>
showing old to new
<?php endif?> /
<?php if($order=='desc'): ?>
showing new to old
<?php else:?>
<a href="<?php echo url_for('@obituaries_allcandles?order=desc&obituary_id='.$obituary->getObituaryId().'&obituary_slug='.$obituary->getObituarySlug() )?>">show new to old</a>
<?php endif?>
</div>
<?php foreach($candles as $candle): ?>
  <div class="verse-obit-candle">
    <div class="verse-obit-candle-text">
        <div class="verse-obit-candle-text-height"></div>
        <?php echo nl2br($candle['thoughts'])?>
    </div>
    <div class="verse-obit-candle-name">
      <strong><?php echo $candle['name']?></strong>,
      <?php //$ts = new DateTime();
            $dt = date_create($candle['timestamp']);
            $dt->setTimezone(new DateTimeZone('America/New_York'));
            echo $dt->format('F d, Y g:i A T')
      ?>
    </div>
  </div>
<?php endforeach ?>
<?php else:?>
  <div class="verse-obit-no-candles-message"><?php echo $icons['actions']['viewallcandles']['no_candles_message']?></div>
  <div class="verse-obit-spacer"></div>
  <a href="<?php echo url_for('obituaries_newcandle', $obituary) ?>" class="verse-obit-action-ajax verse-obit-backlink">
    <?php echo $icons['actions']['lightacandle']['title']?>
  </a>
<?php endif?>
<div class="verse-obit-spacer"></div>
<a href="javascript:history.go(-1)" class="verse-obit-backlink">&laquo; back</a>
<div class="verse-obit-spacer"></div>
