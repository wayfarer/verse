<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<head>
<?php include_stylesheets() ?>
<style type="text/css" media="screen">
.verse-obit-print-holder {
  overflow: hidden;
  padding: 0;
  width: 100%;
}
</style>
<style type="text/css">
.verse-obit-candle {
  padding: 20px 25px 20px 0;
  page-break-inside: avoid;
}
.verse-obit-candle-name {
  padding: 10px 0 0;
  text-align: right;
}
.verse-obit-candle-text {
  background: none;
  padding-left: 0;
}
.verse-obit-candle-text-image {
  float: left;
  padding: 0 20px 0 15px;
}
.verse-obit-candle-text-message {
  margin-left: 65px;
}
.verse-obit-text {
  padding: 20px 10px 0 15px;
}
.verse-obit-left {
  padding: 10px 10px 10px 25px;
}
.verse-obit-left-inner {
  padding: 0;
}
.verse-obit-left h1 {
  margin: 0;
  position: relative;
  top: 0;
}
.verse-obit-title-span {
  padding: 0;
}
.verse-obit-main-inner {
  float: left;
  padding: 10px 0;
  width: 50%;
}
.verse-obit-main-inner h2 {
  padding-top: 0;
}
.verse-obit-left-inner {
  border: 0;
  float: left;
  padding-top: 10px;
}
.verse-obit-workarea-outer {
  clear: left;
}
.verse-obit-workarea {
  padding: 10px;
}
.verse-obit-photo {
  border: 0;
  box-shadow: none;
  margin: 0;
}
.verse-obit-photo img {
  border: #242424 2px solid;
  -moz-box-shadow: 3px 3px 3px 0 #6F6E6E;
  -webkit-box-shadow: 3px 3px 3px 0 #6F6E6E;
  box-shadow: 3px 3px 3px 0 #6F6E6E;
  margin-right: 20px;
}
.verse-obit-inner {
  overflow: auto;
}
.verse-obit-workarea h2, .verse-obit-workarea h3 {
  padding: 0 10px 0 15px;
}
</style>
<?php include_javascripts() ?>
<script type="text/javascript">
    jQuery(function() {
        $('.verse-obit-candle-text-image > img').each(function(){
            $(this).css('padding-top', $(this).parent().parent().height()/2-19);
        });
    });
</script>
</head>
<body>
<?php
  $obituary = Doctrine_Core::getTable('PlgObituary')->getCurrent();
  $config = $obituary->getConfig();
  // filesystem path
  $icons_path = 'obits/icons/'.$config['obituary_icons_theme'].'/';
  $icons = sfYaml::load($icons_path.'theme.yml');
  // fix it for the web path
  $icons_path = '/'.$icons_path;
?>
<div class='verse-obit-print-holder'>
<div id="verse-obit">
  <div class="verse-obit-inner">
    <div class="verse-obit-left">
      <h1>
        <div class="verse-obit-title-span"><?php echo $obituary['name']?></div>
      </h1>
      <div class="verse-obit-left-inner">
        <div class="verse-obit-photo">
          <?php echo image_tag($obituary->getImage(), array('absolute' => true)) ?>
        </div>
      </div>
      <div class="verse-obit-main-inner">
        <h2>Funeral Information</h2>
        <div class="verse-obit-overflow">
	        <div class="verse-obit-field-group">
	          <?php if($config['field_home_place_enabled'] && $obituary['home_place']):?>
            <div class="verse-obit-field">
	            <span class="verse-obit-field-title"><?php echo $config['field_home_place_title']?>:</span>
	            <span class="verse-obit-field-content"><?php echo $obituary['home_place']?></span>
	          </div>
            <?php endif ?>
            <?php if($config['field_death_date_enabled'] && $obituary['death_date']):?>
	          <div class="verse-obit-field">
	            <span class="verse-obit-field-title"><?php echo $config['field_death_date_title']?>:</span>
	            <span class="verse-obit-field-content"><?php echo date('F d, Y', strtotime($obituary['death_date']))?></span>
	          </div>
            <?php endif ?>
	        </div>
	        <div class="verse-obit-field-group">
            <?php if($config['field_birth_place_enabled'] && $obituary['birth_place']):?>
	          <div class="verse-obit-field">
	            <span class="verse-obit-field-title"><?php echo $config['field_birth_place_title']?>:</span>
	            <span class="verse-obit-field-content"><?php echo $obituary['birth_place']?></span>
	          </div>
            <?php endif ?>
            <?php if($config['field_birth_date_enabled'] && $obituary['birth_date']):?>
	          <div class="verse-obit-field">
	            <span class="verse-obit-field-title"><?php echo $config['field_birth_date_title']?>:</span>
	            <span class="verse-obit-field-content"><?php echo date('F d, Y', strtotime($obituary['birth_date']))?></span>
	          </div>
            <?php endif ?>
            <?php if($config['field_age_enabled'] && $obituary['birth_date'] && $obituary['death_date']):?>
	          <div class="verse-obit-field">
	            <span class="verse-obit-field-title"><?php echo $config['field_age_title']?>:</span>
	            <span class="verse-obit-field-content"><?php echo $obituary['age']?></span>
	          </div>
            <?php endif ?>
	        </div>
        </div>
        <?php if($config['field_service_enabled'] && ($obituary['service_place'] || $obituary['service_date'])):?>
        <div class="verse-obit-field">
          <span class="verse-obit-field-title"><?php $st = PlgObituaryTable::getServiceTypes(); echo sprintf($config['field_service_title'], $st[$obituary['service_type']])?>:</span>
          <span class="verse-obit-field-content"><?php if($obituary['service_date']) echo date('l, F d, Y', $obituary['service_date'])?> <?php echo $obituary['service_time']?> <?php echo $obituary['service_place']?></span>
        </div>
        <?php endif ?>
        <?php if($config['field_visitation_enabled'] && ($obituary['visitation_date'] || $obituary['visitation_time'] || $obituary['visitation_place'])):?>
        <div class="verse-obit-field">
          <span class="verse-obit-field-title"><?php echo $config['field_visitation_title']?>:</span>
          <span class="verse-obit-field-content"><?php if($obituary['visitation_date']) echo date('l, F j, Y', $obituary['visitation_date'])?> <?php echo $obituary['visitation_time']?> <?php echo $obituary['visitation_place']?></span>
        </div>
        <?php endif ?>
        <?php if($config['field_interment_enabled'] && $obituary['final_disposition']):?>
        <div class="verse-obit-field">
          <span class="verse-obit-field-title"><?php echo $config['field_interment_title']?>:</span>
          <span class="verse-obit-field-content"><?php echo $obituary['final_disposition']?></span>
        </div>
        <?php endif ?>

      </div>
      </div>

      <div class="verse-obit-workarea-outer">
        <div class="verse-obit-workarea">
			<?php echo $sf_content ?>
        </div>
    </div>
  </div>
</div>
</div>
</body>