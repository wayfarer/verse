<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<head>
    <?php include_stylesheets() ?>
    <link media="screen" type="text/css" href="/css/highslide.css" rel="stylesheet">
    <!--[if lte IE 6 ]>
    <link media="screen" type="text/css" href="/css/highslide-ie6.css" rel="stylesheet">
    <![endif]-->
    <?php include_javascripts() ?>
    <script type="text/javascript" src="/js/highslide-with-gallery.js"></script>
    <script type="text/javascript">
        hs.graphicsDir = '/css/graphics/';
        hs.align = 'center';
        hs.transitions = ['expand', 'crossfade'];
        hs.wrapperClassName = 'dark borderless floating-caption';
        hs.fadeInOut = true;
        hs.dimmingOpacity = .75;
        // Add the controlbar
        if (hs.addSlideshow) hs.addSlideshow({
            interval: 5000,
            repeat: false,
            useControls: true,
            fixedControls: 'fit',
            overlayOptions: {
                opacity: .6,
                position: 'bottom center',
                hideOnMouseOut: true
            }
        });
    </script>
    <?php include_javascripts() ?>
</head>
<body>
<?php
    $obituary = Doctrine_Core::getTable('PlgObituary')->getCurrent();
    $config = $obituary->getConfig();
    if(isset($config['flowers_page_type']) && $config['flowers_page_type'] == 2) {
        $flowers_page = sfConfig::get('app_florist_one_page');
    } else {
        $flowers_page = $config['flowers_page'];
    }
    // filesystem path
    $icons_path = 'obits/icons/'.$config['obituary_icons_theme'].'/';
    $icons = sfYaml::load($icons_path.'theme.yml');
    // fix it for the web path
    $icons_path = '/'.$icons_path;
?>
<style>
    .light-a-candle {
        background: url(<?php echo $icons_path.$icons['actions']['lightacandle']['icon']?>) no-repeat;
    }
    .view-all-candles {
        background: url(<?php echo $icons_path.$icons['actions']['viewallcandles']['icon']?>) no-repeat;
    }
    .send-flowers {
        background: url(<?php echo $icons_path.$icons['actions']['sendflowers']['icon']?>) no-repeat;
    }
    .subscribe-for-candles {
        background: url(<?php echo $icons_path.$icons['actions']['subscribe']['icon']?>) no-repeat;
    }
    <?php if($config['obituary_theme_image']): ?>
    .verse-obit-themeimage {
    <?php if($config['obituary_theme_image']!='none'): ?>
        background-image: url(/<?php echo $config['obituary_theme_image']?>);
        background-repeat: no-repeat;
    <?php else: ?>
        background-image: none;
    <?php endif ?>
    }
    <?php endif ?>
    <?php if($config['military_status'] && (!isset($config['obituary_military_image']) || $config['obituary_military_image'] !== "none")): ?>
    .verse-obit-military {
        background-image: url(/<?php echo isset($config['obituary_military_image']) ? $config['obituary_military_image'] : "img/ob_theme/military/military.jpg"?>);
    }
    <?php endif ?>
</style>
<div id="verse-obit">
    <div class="verse-obit-border-1">
        <div class="verse-obit-border-2">
            <div class="verse-obit-border-3">
                <div class="verse-obit-border-4">
                    <div class="verse-obit-border-5">
                        <div class="verse-obit-border-6">
                            <div class="verse-obit-border-7">
                                <div class="verse-obit-border-8">
                                    <div class="verse-obit-inner verse-obit-themeimage<?php if($config['military_status']): ?> verse-obit-military<?php endif ?>">
                                        <h1><span class="verse-obit-title-span"><?php echo $obituary['name']?></span></h1>
                                        <div class="verse-obit-left">
                                            <div class="verse-obit-left-inner">
                                                <div class="verse-obit-photo">
                                                    <!--<a href="<?= $obituary['image'] ?>" class="verse-obit-photo-image" onclick="return hs.expand(this, { slideshowGroup: 'obit-photo' })"><?php echo image_tag(VerseUtil::obit_photo_thumbnail($obituary->getImage(), 180)) ?></a>-->
                                                    <a href="<?= $obituary['image'] ?>" class="verse-obit-photo-image" onclick="return hs.expand(this, { slideshowGroup: 'obit-photo' })"><?php echo image_tag($obituary->getImage()) ?></a>
                                                </div>
                                                <div class="verse-obit-actions">
                                                    <?php if($config['candles_policy']):?>
                                                        <a href="<?php echo url_for('obituaries_newcandle', $obituary) ?>" class="verse-obit-action verse-obit-action-ajax light-a-candle">
                                                            <div class="verse-obit-action-title"><?php echo $icons['actions']['lightacandle']['title']?></div>
                                                            <div class="verse-obit-action-desc"><?php echo $icons['actions']['lightacandle']['description']?></div>
                                                        </a>
                                                        <a href="<?php echo url_for('obituaries_allcandles', $obituary) ?>" class="verse-obit-action verse-obit-action-ajax view-all-candles">
                                                            <div class="verse-obit-action-title"><?php echo $icons['actions']['viewallcandles']['title']?> (<?php echo $obituary->getCandlesCount()?>)</div>
                                                            <div class="verse-obit-action-desc"><?php echo $icons['actions']['viewallcandles']['description']?></div>
                                                        </a>
                                                    <?php endif?>
                                                    <?php if($config['flowers_page']):?>
                                                        <a href="/<?php echo $flowers_page?>" class="verse-obit-action send-flowers">
                                                            <div class="verse-obit-action-title"><?php echo $icons['actions']['sendflowers']['title']?></div>
                                                            <div class="verse-obit-action-desc"><?php echo $icons['actions']['sendflowers']['description']?></div>
                                                        </a>
                                                    <?php endif?>
                                                    <?php if($config['candles_policy']):?>
                                                        <a href="<?php echo url_for('obituaries_update_subscribe', $obituary) ?>" class="verse-obit-action verse-obit-action-ajax subscribe-for-candles">
                                                            <div class="verse-obit-action-title"><?php echo $icons['actions']['subscribe']['title']?></div>
                                                            <div class="verse-obit-action-desc"><?php echo $icons['actions']['subscribe']['description']?></div>
                                                        </a>
                                                    <?php endif?>
                                                    <div class="verse-obit-share">
                                                        <!-- AddThis Button BEGIN -->
                                                        <div class="addthis_toolbox addthis_default_style">
                                                            <a href="http://addthis.com/bookmark.php?v=250&amp;username=xa-4c51345c3303423a" class="addthis_button_compact">Share</a>
                                                            <span class="addthis_separator">|</span>
                                                            <a class="addthis_button_email"></a>
                                                            <a class="addthis_button_facebook"></a>
                                                            <a class="addthis_button_twitter"></a>
                                                        </div>
                                                        <script type="text/javascript" src="http://s7.addthis.com/js/250/addthis_widget.js#username=xa-4c51345c3303423a"></script>
                                                        <!-- AddThis Button END -->
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="verse-obit-main">
                                            <div class="verse-obit-main-inner">
                                                <div class="verse-obit-print"><a href="<?php echo url_for('obituaries_print', $obituary)?>" target="_blank">Print</a></div>
                                                <h2>Funeral Information</h2>
                                                <div class="verse-obit-splitter"></div>
                                                <div class="verse-obit-overflow">
                                                    <div class="verse-obit-field-group">
                                                        <?php if($config['field_home_place_enabled'] && $obituary['home_place']):?>
                                                            <div class="verse-obit-field">
                                                                <span class="verse-obit-field-title"><?php echo $config['field_home_place_title']?>:</span>
                                                                <span class="verse-obit-field-content"><?php echo $obituary['home_place']?></span>
                                                            </div>
                                                        <?php endif ?>
                                                        <?php if($config['field_death_date_enabled'] && $obituary['death_date']):?>
                                                            <div class="verse-obit-field">
                                                                <span class="verse-obit-field-title"><?php echo $config['field_death_date_title']?>:</span>
                                                                <span class="verse-obit-field-content"><?php echo date('F d, Y', strtotime($obituary['death_date']))?></span>
                                                            </div>
                                                        <?php endif ?>
                                                    </div>
                                                    <div class="verse-obit-field-group">
                                                        <?php if($config['field_birth_place_enabled'] && $obituary['birth_place']):?>
                                                            <div class="verse-obit-field">
                                                                <span class="verse-obit-field-title"><?php echo $config['field_birth_place_title']?>:</span>
                                                                <span class="verse-obit-field-content"><?php echo $obituary['birth_place']?></span>
                                                            </div>
                                                        <?php endif ?>
                                                        <?php if($config['field_birth_date_enabled'] && $obituary['birth_date']):?>
                                                            <div class="verse-obit-field">
                                                                <span class="verse-obit-field-title"><?php echo $config['field_birth_date_title']?>:</span>
                                                                <span class="verse-obit-field-content"><?php echo date('F d, Y', strtotime($obituary['birth_date']))?></span>
                                                            </div>
                                                        <?php endif ?>
                                                        <?php if($config['field_age_enabled'] && $obituary['birth_date'] && $obituary['death_date']):?>
                                                            <div class="verse-obit-field">
                                                                <span class="verse-obit-field-title"><?php echo $config['field_age_title']?>:</span>
                                                                <span class="verse-obit-field-content"><?php echo $obituary['age']?></span>
                                                            </div>
                                                        <?php endif ?>
                                                    </div>
                                                </div>
                                                <div class="verse-obit-splitter"></div>
                                                <?php if($config['field_service_enabled'] && ($obituary['service_place'] || $obituary['service_date'])):?>
                                                    <div class="verse-obit-field">
                                                        <span class="verse-obit-field-title"><?php $st = PlgObituaryTable::getServiceTypes(); echo sprintf($config['field_service_title'], $st[$obituary['service_type']])?>:</span>
                                                        <span class="verse-obit-field-content"><?php if($obituary['service_date']) echo date('l, F d, Y', $obituary['service_date'])?> <?php echo $obituary['service_time']?> <?php echo $obituary['service_place']?></span>
                                                    </div>
                                                <?php endif ?>
                                                <?php if($config['field_visitation_enabled'] && ($obituary['visitation_date'] || $obituary['visitation_time'] || $obituary['visitation_place'])):?>
                                                    <div class="verse-obit-field">
                                                        <span class="verse-obit-field-title"><?php echo $config['field_visitation_title']?>:</span>
                                                        <span class="verse-obit-field-content"><?php if($obituary['visitation_date']) echo date('l, F j, Y', $obituary['visitation_date'])?> <?php echo $obituary['visitation_time']?> <?php echo $obituary['visitation_place']?></span>
                                                    </div>
                                                <?php endif ?>
                                                <?php if($config['field_interment_enabled'] && $obituary['final_disposition']):?>
                                                    <div class="verse-obit-field">
                                                        <span class="verse-obit-field-title"><?php echo $config['field_interment_title']?>:</span>
                                                        <span class="verse-obit-field-content"><?php echo $obituary['final_disposition']?></span>
                                                    </div>
                                                <?php endif ?>
                                                <div class="verse-obit-splitter"></div>

                                                <?php if($obituary->hasSlides()): ?>
                                                    <div class="verse-obit-scrapbook">
                                                        <a href="#" onclick="return openwin('/<?php echo Doctrine_Core::getTable("CmsPage")->getScrapbookPage()?>/<?php echo $obituary->getObituaryId()?>')">click to view photoalbum</a>
                                                    </div>
                                                <?php endif;?>
                                                <?php foreach($obituary->getMovieclips() as $movieclip):?>
                                                    <div class="verse-obit-movieclip">
                                                        <a href="#" onclick="return openwin('/mcplayer.php?id=<?php echo $movieclip->getMovieclipId()?>&detectflash=false', 475, 390)"><?php echo $movieclip->getTitle()?></a>
                                                    </div>
                                                <?php endforeach;?>
                                                <div class="verse-obit-spacer"></div>

                                                <div class="verse-obit-progress">loading...</div>
                                                <div class="verse-obit-workarea-outer">
                                                    <div class="verse-obit-workarea">
                                                        <?php include_partial('obituaries/flashes') ?>
                                                        <?php echo $sf_content ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    jQuery( function() {
        $('.verse-obit-info').animate({ backgroundColor: "#FF0"}, 1500)
                             .animate({ backgroundColor: "#FFF"}, 1000);
    } );
</script>
<!-- <script>
  // onload
  jQuery( function() {
    hook_links('#verse-obit', '.verse-obit-workarea');
  });

  function hook_links(source, target) {
    target = target || source;
    // hook links
    $(source+' a.verse-obit-action-ajax').bind('click', function() {
      $(target).parent().css("min-height", $(target).height());
      $(target).slideUp('normal', function() {
        $('.verse-obit-progress').show();
      } ).load(this.href, null, function(response) {
        $('.verse-obit-progress').hide();
        $(this).slideDown('normal', function() {
          $(target).parent().css("min-height", 0);
          hook_links(target);
        } );
      } );
      return false;
    } );

    // hook forms
    $(source+' form').ajaxForm({
      beforeSubmit: function() {
        $(target).parent().css("min-height", $(target).height());
        $(target).slideUp("normal", function() {
          $('.verse-obit-progress').show();
        } );
      },
      success: function(response) {
        $('.verse-obit-progress').hide();
        $(target).html(response).slideDown("normal", function() {
          $(target).parent().css("min-height", 0);
          hook_links(target);
        } );
      }
    } );

  }
</script> -->
</body>