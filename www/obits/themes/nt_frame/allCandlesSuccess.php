<?php if(count($candles)>3):?>
<a href="javascript:history.go(-1)">&laquo; back</a>
<div class="verse-obit-spacer"></div>
<?php endif ?>
<?php if(count($candles)):?>
<h2><?php echo $icons['actions']['viewallcandles']['content_header']?></h2>
<div class="verse-obit-leave-a-message">To leave a message on this obituary, click the "light a candle" button above.</div>
<div class="verse-obit-candles-order">
<?php if($order=='desc'): ?>
<a href="<?php echo url_for('@obituaries_allcandles?order=asc&obituary_id='.$obituary->getObituaryId().'&obituary_slug='.$obituary->getObituarySlug() )?>">show old to new</a>
<?php else: ?>
showing old to new
<?php endif?> /
<?php if($order=='desc'): ?>
showing new to old
<?php else:?>
<a href="<?php echo url_for('@obituaries_allcandles?order=desc&obituary_id='.$obituary->getObituaryId().'&obituary_slug='.$obituary->getObituarySlug() )?>">show new to old</a>
<?php endif?>
</div>
<div class="verse-obit-splitter"></div>
<div class="verse-obit-spacer"></div>
<?php foreach($candles as $candle): ?>
    <div class="verse-obit-candle">
        <div class="verse-obit-candle-text candle-image-<?php echo is_null($candle['image']) || $candle['image'] >= count($icons['actions']['viewallcandles']['candle_images']) ? 0 : $candle['image'] ?>">
            <div class="verse-obit-candle-text-height"></div>
            <div class="verse-obit-candle-thoughts">
                <div class="verse-obit-candle-thoughts-quote-open"></div>
                <div class="verse-obit-candle-thoughts-text"><?php echo nl2br($candle['thoughts'])?></div>
                <div class="verse-obit-candle-thoughts-quote-close"></div>
            </div>
        </div>
        <div class="verse-obit-candle-name-date">
            <span class="verse-obit-candle-name"><?php echo $candle['name']?></span>
            <span class="verse-obit-candle-date">
                <?php //$ts = new DateTime();
                    $dt = date_create($candle['timestamp']);
                    $dt->setTimezone(new DateTimeZone('America/New_York'));
                    echo $dt->format('F d, Y g:ia')
                ?>
            </span>
        </div>
        <div class="verse-obit-splitter"></div>
    </div>
<?php endforeach ?>
<?php else:?>
    <div class="verse-obit-no-candles-message">No candles have been left yet.<br/>To leave a candle on this obituary, click the "light a candle" button above.</div>
<?php endif?>
<div class="verse-obit-spacer"></div>
<a href="javascript:history.go(-1)">&laquo; back</a>
<div class="verse-obit-spacer"></div>
