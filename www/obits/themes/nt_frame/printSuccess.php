<h2>Biography</h2>
<div class="verse-obit-text">
  <?php echo $obituary->getObitText(ESC_RAW)?>
</div>
<div class="verse-obit-spacer"></div>

<?php if(count($candles)): ?>
<h2><?php echo $icons['actions']['viewallcandles']['content_header']?></h2>
<?php foreach($candles as $candle): ?>
  <div class="verse-obit-candle">
    <div class="verse-obit-candle-text">
      <div class="verse-obit-candle-text-image"><img src="/obits/icons/<?php echo $config['obituary_icons_theme']?>/img/<?php echo $config['obituary_icons_theme'] == 'guestbook' ? 'guestbook' : 'candle'  ?><?php echo is_null($candle['image']) || $candle['image'] >= count($icons['actions']['viewallcandles']['candle_images']) ? 1 : $candle['image']+1 ?>.png"></div>
      <div class="verse-obit-candle-text-message">
          <div class="verse-obit-candle-thoughts-quote-open"><img src="/obits/themes/nt_frame/img/q_open.png"></div>
          <div class="verse-obit-candle-thoughts-text"><?php echo $candle['thoughts']?></div>
          <div class="verse-obit-candle-thoughts-quote-close"><img src="/obits/themes/nt_frame/img/q_close.png"></div>
      </div>
    </div>
    <div class="verse-obit-candle-name-date">
        <span class="verse-obit-candle-name"><?php echo $candle['name']?></span>
        <span class="verse-obit-candle-date">
            <?php //$ts = new DateTime();
                $dt = date_create($candle['timestamp']);
                $dt->setTimezone(new DateTimeZone('America/New_York'));
                echo $dt->format('F d, Y g:ia')
            ?>
        </span>
    </div>
    <div class="verse-obit-splitter"></div>
  </div>
<?php endforeach ?>
<?php endif?>
<script type="text/javascript">
jQuery( function() {
  window.print();
} );
</script>
