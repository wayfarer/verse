<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<head>
<?php include_stylesheets() ?>
<style type="text/css" media="screen">
    .verse-obit-print-holder {
        width: 700px;
    }
</style>
<style type="text/css">
    .verse-obit-workarea-outer {
        clear: left;
    }
    .verse-obit-workarea {
        padding: 10px;
    }
    .verse-obit-photo {
        background-color: #ffffff;
        border: none;
        box-shadow: none;
        margin-bottom: 0;
    }
    .verse-obit-main {
        background-color: #ffffff;
        border: none;
        box-shadow: none;
        margin: 0;
        min-height: 0;
        overflow: hidden;
    }
    .verse-obit-inner {
        overflow: visible;
        width: 100%;
    }
    .verse-obit-inner h1 {
        position: relative;
        top: 0;
    }
    .verse-obit-candle-text {
        margin-left: 30px;
        padding-left: 0;
    }
    .verse-obit-candle-text-image {
        float: left;
        margin-right: 45px;
    }
    .verse-obit-candle .verse-obit-splitter {
        margin-left: 190px;
    }
</style>
<?php include_javascripts() ?>
    <script type="text/javascript">
        jQuery(function() {
            $('.verse-obit-candle-text-image').each(function(){
                $(this).css('margin-top', ($(this).parent().height()-$(this).height())/2);
            });
        });
    </script>
</head>
<body>
<?php
  $obituary = Doctrine_Core::getTable('PlgObituary')->getCurrent();
  $config = $obituary->getConfig();
?>
<div class='verse-obit-print-holder'>
<div id="verse-obit">
    <div class="verse-obit-inner">
      <h1><span class="verse-obit-title-span"><?php echo $obituary['name']?></span></h1>
      <div class="verse-obit-left">
      <div class="verse-obit-left-inner">
        <div class="verse-obit-photo">
            <?php if($obituary->getImage()): ?>
                <?php echo image_tag(VerseUtil::obit_photo_thumbnail($obituary->getImage(), 180)) ?>
            <?php else: ?>
                <img src="/img/nt-nophoto-big.png">
            <?php endif; ?>
        </div>
      </div>
      </div>
      <div class="verse-obit-main">
      <div class="verse-obit-main-inner">
        <h2>Funeral Information</h2>
        <div class="verse-obit-splitter"></div>
        <div class="verse-obit-overflow">
	        <div class="verse-obit-field-group">
	          <?php if($config['field_home_place_enabled'] && $obituary['home_place']):?>
            <div class="verse-obit-field">
	            <span class="verse-obit-field-title"><?php echo $config['field_home_place_title']?>:</span>
	            <span class="verse-obit-field-content"><?php echo $obituary['home_place']?></span>
	          </div>
            <?php endif ?>
            <?php if($config['field_death_date_enabled'] && $obituary['death_date']):?>
	          <div class="verse-obit-field">
	            <span class="verse-obit-field-title"><?php echo $config['field_death_date_title']?>:</span>
	            <span class="verse-obit-field-content"><?php echo date('F d, Y', strtotime($obituary['death_date']))?></span>
	          </div>
            <?php endif ?>
	        </div>
	        <div class="verse-obit-field-group">
            <?php if($config['field_birth_place_enabled'] && $obituary['birth_place']):?>
	          <div class="verse-obit-field">
	            <span class="verse-obit-field-title"><?php echo $config['field_birth_place_title']?>:</span>
	            <span class="verse-obit-field-content"><?php echo $obituary['birth_place']?></span>
	          </div>
            <?php endif ?>
            <?php if($config['field_birth_date_enabled'] && $obituary['birth_date']):?>
	          <div class="verse-obit-field">
	            <span class="verse-obit-field-title"><?php echo $config['field_birth_date_title']?>:</span>
	            <span class="verse-obit-field-content"><?php echo date('F d, Y', strtotime($obituary['birth_date']))?></span>
	          </div>
            <?php endif ?>
            <?php if($config['field_age_enabled'] && $obituary['birth_date'] && $obituary['death_date']):?>
	          <div class="verse-obit-field">
	            <span class="verse-obit-field-title"><?php echo $config['field_age_title']?>:</span>
	            <span class="verse-obit-field-content"><?php echo $obituary['age']?></span>
	          </div>
            <?php endif ?>
	        </div>
        </div>
        <div class="verse-obit-splitter"></div>
        <?php if($config['field_service_enabled'] && ($obituary['service_place'] || $obituary['service_date'])):?>
        <div class="verse-obit-field">
          <span class="verse-obit-field-title"><?php $st = PlgObituaryTable::getServiceTypes(); echo sprintf($config['field_service_title'], $st[$obituary['service_type']])?>:</span>
          <span class="verse-obit-field-content"><?php if($obituary['service_date']) echo date('l, F d, Y', $obituary['service_date'])?> <?php echo $obituary['service_time']?> <?php echo $obituary['service_place']?></span>
        </div>
        <?php endif ?>
        <?php if($config['field_visitation_enabled'] && ($obituary['visitation_date'] || $obituary['visitation_time'] || $obituary['visitation_place'])):?>
        <div class="verse-obit-field">
          <span class="verse-obit-field-title"><?php echo $config['field_visitation_title']?>:</span>
          <span class="verse-obit-field-content"><?php if($obituary['visitation_date']) echo date('l, F j, Y', $obituary['visitation_date'])?> <?php echo $obituary['visitation_time']?> <?php echo $obituary['visitation_place']?></span>
        </div>
        <?php endif ?>
        <?php if($config['field_interment_enabled'] && $obituary['final_disposition']):?>
        <div class="verse-obit-field">
          <span class="verse-obit-field-title"><?php echo $config['field_interment_title']?>:</span>
          <span class="verse-obit-field-content"><?php echo $obituary['final_disposition']?></span>
        </div>
        <?php endif ?>
        <div class="verse-obit-splitter"></div>
      </div>
      </div>
      <div class="verse-obit-workarea-outer">
	      <div class="verse-obit-workarea">
  				<?php include_partial('obituaries/flashes') ?>
	  			<?php echo $sf_content ?>
	      </div>
      </div>
    </div>
</div>
</body>