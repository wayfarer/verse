<?php $photo_count = count($photos); ?>
<?php if($photo_count): ?>
    <link rel="stylesheet" type="text/css" href="/shadowbox/shadowbox.css">
    <script type="text/javascript" src="/shadowbox/shadowbox.js"></script>
    <script type="text/javascript">
        Shadowbox.init();
    </script>
<?php endif; ?>
<?php if($photo_count > 4): ?>
    <script type="text/javascript" src="/js/jquery.jcarousel.min.js"></script>
    <script type="text/javascript">
        jQuery(function() {
            $('.verse-obit-photos').jcarousel({
                scroll: 1,
                animation: 1000,
                auto: 10,
                wrap: 'circular',
                itemFallbackDimension: 100
            });
        });
    </script>
<?php endif; ?>

<h3 class="verse-obit-left-margin">Photos</h3>
<?php if($photo_count): ?>
    <div class="verse-obit-mediaphotos">
        <div class="verse-obit-photos highslide-gallery">
            <ul>
                <?php foreach($photos as $index => $photo): ?>
                    <li<?php echo ($photo_count <= 4 && $index == ($photo_count-1)) ? ' class="last"' : '' ?>>
                        <a href="/<?php echo $photo['path']; ?>" class="highslide verse-obit-media" onclick="return hs.expand(this, { slideshowGroup: 'obit-media' })" ><img src="/<?php echo VerseUtil::thumbnail($photo['path'], 100); ?>" title="Click to enlarge"></a>
                        <?php if($photo['caption']): ?>
                            <div class="verse-obit-media-caption"><?php echo $photo['caption']; ?></div>
                        <?php endif; ?>
                    </li>
                <?php endforeach; ?>
            </ul>
        </div>
    </div>
<?php else: ?>
    <div class="verse-obit-no-media-message">There are no photos yet. You can add one.</div>
<?php endif; ?>
<a href="<?php echo url_for('obituaries_addmedia', $obituary) ?>" class="verse-obit-action-ajax verse-obit-add-photo verse-obit-left-margin">Add Photo</a>

<h3 class="verse-obit-left-margin">Videos</h3>
<?php if(count($movies)): ?>
    <div class="verse-obit-mediamovies">
        <div class="verse-obit-movie-container">
        <?php foreach($movies as $movie): ?>
            <div class="verse-obit-movie">
                <a href="/<?php echo $movie['path']; ?>" class="verse-obit-movie-link" rel="shadowbox"><img src="/<?php echo $movie['thumbnail']; ?>" alt="Video"></a>
                <?php if($movie['caption']): ?>
                    <div class="verse-obit-media-caption"><?php echo $movie['caption']; ?></div>
                <?php endif; ?>
            </div>
        <?php endforeach; ?>
        </div>
    </div>
<?php else: ?>
    <div class="verse-obit-no-media-message">There are no videos yet. You can add one.</div>
<?php endif; ?>
<a href="<?php echo url_for('obituaries_addmedia', $obituary) ?>" class="verse-obit-action-ajax verse-obit-add-movie verse-obit-left-margin">Add Video</a>

<div class="verse-obit-spacer"></div>
<a href="javascript:history.go(-1)" class="verse-obit-backlink verse-obit-left-margin">&laquo; back</a>
