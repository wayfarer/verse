<?php
 // initial LDAP create script

include("inc/verse.inc.php"); //main header - initializes Verse environment
require_once("Net/LDAP2.php");

ini_set("display_errors", 1);
set_time_limit(0);

if ($user->have_role(ROLE_EMAIL_MANAGEMENT)) {
    $action = @$_POST["action"];

    $myCacheConfig = array(
        'path' => 'c:/temp/ldap/ttt.cache',
        'max_age' => 1200
    );
    $myCacheObject = new Net_LDAP2_SimpleFileSchemaCache($myCacheConfig);

    // Connect using the configuration:
    $ldap = Net_LDAP2::connect($ldap_config);
    if (PEAR::isError($ldap)) {
        die('Could not connect to LDAP-server: ' . $ldap->getMessage());
    }
    $ldap->registerSchemaCache($myCacheObject);

    $baseDN = "o=domains,dc=twintierstech,dc=net";

    // ws0 data
    $emails_data = unserialize(file_get_contents("!devel/email_transfer/email_data.ws1"));

    $i = 0;
    foreach ($emails_data as $domain => $emails) {
        echo "<b>$domain</b> (", count($emails), ") - ", $domain_status = ensure_domain_name($domain), "<br>";
        if ($domain_status != "exists") {
            foreach ($emails as $email) {
                echo $email["name"], "@$domain - ", ($email["type"] == 0 ? "mailbox"
                        : "forward (" . $email["forward"] . ")"), " - ", $email["password"], " - ";
                if ($email["type"] == 0) {
                    echo ensure_email($email, $domain);
                }
                else {
                    echo ensure_alias($email, $domain);
                }
                echo "<br>";
            }
        }
        else {
            echo "skipping emails<br>";
        }
        $i++;
        //if($i>4) break;
    }
}

function ensure_domain_name($domain_name) {
    global $ldap, $baseDN;

    $dsn = "domainName=$domain_name,$baseDN";
    if (!ldap_entry_exists($dsn)) {
        $attributes = array(
            'objectClass' => 'mailDomain',
            'domainName' => $domain_name,
            'accountStatus' => 'active',
            'cn' => $domain_name,
            'enabledService' => array('mail', 'recipientbcc', 'senderbcc'),
            'mtaTransport' => 'dovecot'
        );
        // create domain entry
        $entry = Net_LDAP2_Entry::createFresh($dsn, $attributes);
        // Add the entry to the directory:
        $ret = $ldap->add($entry);

        if (PEAR::isError($ret)) {
            return "failed - " . $ret->getMessage();
        }
        else {
            // create ou=Users and ou=Aliases entries under the domain
            $users_attributes = array(
                'objectClass' => array('organizationalUnit', 'top'),
                'ou' => "Users"
            );
            $aliases_attributes = array(
                'objectClass' => array('organizationalUnit', 'top'),
                'ou' => "Aliases"
            );

            $entry = Net_LDAP2_Entry::createFresh("ou=Users,$dsn", $users_attributes);
            $ret = $ldap->add($entry);
            if (PEAR::isError($ret)) {
                return "failed - " . $ret->getMessage();
            }

            $entry = Net_LDAP2_Entry::createFresh("ou=Aliases,$dsn", $aliases_attributes);
            $ret = $ldap->add($entry);
            if (PEAR::isError($ret)) {
                return "failed - " . $ret->getMessage();
            }

            return "created";
        }
    }
    return "exists";
}

function ensure_email($an_email, $domain_name) {
    global $ldap, $baseDN;

    $email = $an_email["name"] . "@$domain_name";

    $dsn = "mail=$email,ou=Users,domainName=$domain_name,$baseDN";
    if (!ldap_entry_exists($dsn)) {
        $username = $cn = $an_email["name"];
        $email_pass = $an_email["password"];

        $storagebasedir = "/home/vmail/vmail01";
        // generate mail message store
        $mailmessagestore = sprintf("%s/%s/%s/%s/%s-%s/", $domain_name, $username[0], substr($username, 0, 2), substr($username, 0, 3), $username, date("Y.m.d.H.i.s"));
        $homedir = $storagebasedir . "/" . $mailmessagestore;

        $attributes = array(
            'objectClass' => array("inetOrgPerson", "mailUser", "shadowAccount"),
            'cn' => $cn,
            'mail' => $email,
            'sn' => $username,
            'uid' => $username,
            'accountStatus' => "active",
            'enabledService' => array('mail', 'smtp', 'pop3', 'imap', 'deliver', 'forward',
                                      'senderbcc', 'recipientbcc', 'managesieve',
                                      'displayedInGlobalAddressBook'),
            'homeDirectory' => $homedir,
            'mailMessageStore' => $mailmessagestore,
            'mailQuota' => 1073741824,
            'memberOfGroup' => "",
            'mtaTransport' => "dovecot",
            'storageBaseDirectory' => $storagebasedir,
            'userPassword' => make_ssha_password($email_pass)
        );

        // create email entry
        $entry = Net_LDAP2_Entry::createFresh($dsn, $attributes);
        // Add the entry to the directory:
        $ret = $ldap->add($entry);

        if (PEAR::isError($ret)) {
            return "failed - " . $ret->getMessage();
        }
        else {
            return "created";
        }
    }
    return "exists";
}

function ensure_alias($an_email, $domain_name) {
    global $ldap, $baseDN;

    $email = $an_email["name"] . "@$domain_name";

    $dsn = "mail=$email,ou=Aliases,domainName=$domain_name,$baseDN";
    if (!ldap_entry_exists($dsn)) {
        $aliases = explode(",", $an_email["forward"]);
        $attributes = array(
            "objectClass" => array("mailAlias", "top"),
            "mail" => $email,
            "mailForwardingAddress" => $aliases,
            "accountStatus" => "active",
            "enabledService" => array("mail", "deliver")
        );

        // create alias entry
        $entry = Net_LDAP2_Entry::createFresh($dsn, $attributes);
        // Add the entry to the directory:
        $ret = $ldap->add($entry);

        if (PEAR::isError($ret)) {
            return "failed - " . $ret->getMessage();
        }
        else {
            return "created";
        }
    }
    return "exists";
}

function ldap_entry_exists($dsn) {
    global $ldap;

    $entry = $ldap->getEntry($dsn);
    return !PEAR::isError($entry);
}

function make_ssha_password($password) {
    mt_srand((double)microtime() * 1000000);
    $salt = pack("CCCC", mt_rand(), mt_rand(), mt_rand(), mt_rand());
    $hash = "{SSHA}" . base64_encode(pack("H*", sha1($password . $salt)) . $salt);
    return $hash;
}
