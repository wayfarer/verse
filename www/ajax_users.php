<?php
 // obsolete
// users management, ajax actions
include("inc/verse.inc.php"); //main header - initializes Verse environment
//	include("inc/mailbox.inc.php");

if ($user->have_role(ROLE_USER_MANAGEMENT)) {
    $action = $_POST["action"];

    switch ($action) {
        case "list":
            $rep = createobject("report_ajax", array($db, "sms_user", REP_WITH_PAGENATOR, true, 20, 'width="600px" style="background-color: #DDEEFF"'));

            $rep->add_field("Login", REP_STRING_TEMPLATE, '{login}', REP_ORDERABLE, 'align="center"');
            $rep->add_field("Name", REP_STRING_TEMPLATE, '{name}', REP_ORDERABLE, 'align="center"');
            $rep->add_field("Status", REP_FUNCTIONCALL_TEMPLATE, 'enabled({enabled|1})', REP_ORDERABLE, 'align="center"');
            $rep->add_field("", REP_STRING_TEMPLATE, href_js_action("properties", "new UserEditor({user_id})"), REP_UNORDERABLE, 'align="center"');
            $rep->add_field("", REP_STRING_TEMPLATE, href_js_action("delete", "delete_user({user_id})"), REP_UNORDERABLE, 'align="center"');

            $rep->add_filter("", REP_INVISIBLE_FILTER, "domain_id", "=", $domain_id);

            $rep->html_attributes('class="sortable" id="uid" width="600px"');

            $rep->handle_events($_POST);

            $html = $rep->make_report();

            echo $html;
            break;
        case "load":
            $role_names = array(1 => "users", 2 => "content", 5 => "obits", 6 => "products", 3 => "ftp", 7 => "sublists", 8 => 'phpmvstats', 9 => 'movieclips', 10 => 'webcasting', 11 => 'members', 12 => 'email');
            $user_id = intval(@$_POST["id"]);
            $query = "SELECT login ulogin, name, email, domain_id, enabled FROM sms_user WHERE user_id='$user_id'";
            $ret = $db->getRow($query, DB_FETCHMODE_ASSOC);
            $query = "SELECT role_id FROM cms_user_role WHERE user_id='$user_id'";
            $roles = $db->getCol($query);
            foreach ($roles as $role) {
                $ret[$role_names[$role]] = 1;
            }
            // check if mailbox login exists
/*			$query = "SELECT postfix FROM sms_domain WHERE domain_id='$domain_id'";
			$postfix = $db->getOne($query);
			$login = $ret["ulogin"]."-".$postfix;
			$query = "SELECT 1 FROM plg_mailbox WHERE login='$login'";
			$ret["mail"] = $db->getOne($query); */
            header("X-JSON:" . make_json_response($ret));
            break;
        case "save":
            $user_id = intval(@$_POST["id"]);
            $p = array();
            $p["domain_id"] = $domain_id;
            $p["login"] = in(@$_POST["ulogin"]);
            $p["name"] = in(@$_POST["name"]);
//			$p["email"] = in(@$_POST["email"]);
            $p["enabled"] = @$_POST["enabled"] ? 1 : 0;
            $password = in(@$_POST["password"]);
            $ftp_pass = in(@$_POST["ftp_pass"]);
//			$mail_pass = in(@$_POST["mail_pass"]);

            if ($user_id) {
                $query = "UPDATE sms_user SET " . make_set_clause($p);
                if (strlen($password)) {
                    $query .= ", password=old_password('$password')";
                }
                $query .= " WHERE user_id='$user_id'";
                $db->query($query);
            }
            else {
                $query = "INSERT sms_user SET " . make_set_clause($p) . ", password=old_password('$password')";
                $db->query($query);
                $query = "SELECT last_insert_id()";
                $user_id = $db->getOne($query);
            }
            // process user rights
            $role_names = array(1 => "users", 2 => "content", 5 => "obits", 6 => "products", 3 => "ftp", 7 => "sublists", 8 => 'phpmvstats', 9 => 'movieclips', 10 => 'webcasting', 11 => 'members', 12 => 'email');

            foreach ($role_names as $role_id => $role) {
                if ((@$_POST[$role] ? 1 : 0) != @intval($_POST[$role . "_old"])) {
                    if (@$_POST[$role]) {
                        $rp = array();
                        switch ($role_id) {
                            case 3:
                                $ftp_user_id = add_ftp_user($p["login"], $ftp_pass);
                                $rp["ftp_user_id"] = $ftp_user_id;
                                break;
                            case 7: // create mylist
                                $hash = substr(md5(uniqid(rand())), 0, 16);
                                $query = "INSERT plg_product_mylist SET domain_id='$domain_id', user_id='$user_id', hash='$hash'";
                                $db->query($query);
                                break;
                            //phpMV stats logic
                            case 8:
                                include_once('inc/piwik_adapter.inc.php');
                                // for non-production domain need to ensure if domain exists
                                if ($domain->get('mode') !== 0) {
                                    piwik_ensure_domain($domain_id);
                                }
                                piwik_ensure_user($user_id, $password);

                                include_once('inc/phpmv_adapter.inc.php');
                                phpmv_user_set($user_id, $domain_id);
                                break;
                        }
                        $rp = serialize($rp);
                        $query = "INSERT cms_user_role SET user_id='$user_id', role_id='$role_id', role_params='" . in($rp) . "'";
                    }
                    else {
                        switch ($role_id) {
                            case 3:
                                $query = "SELECT role_params FROM cms_user_role WHERE user_id='$user_id' AND role_id=3";
                                $rp = $db->getOne($query);
                                $rp = unserialize($rp);
                                remove_ftp_user($rp["ftp_user_id"]);
                                break;
                            case 7:
                                // clear user list items
                                $query = "SELECT mylist_id FROM plg_product_mylist WHERE domain_id='$domain_id' AND user_id='$user_id'";
                                $mylist_id = $db->getOne($query);
                                $query = "DELETE FROM plg_product_mylist_item WHERE mylist_id='$mylist_id'";
                                $db->query($query);

                                // remove list itself
                                $query = "DELETE FROM plg_product_mylist WHERE mylist_id='$mylist_id'";
                                $db->query($query);
                                break;
                            //phpMV stats logic
                            case 8:
                                include_once('inc/piwik_adapter.inc.php');
                                piwik_delete_user($user_id);

                                include_once('inc/phpmv_adapter.inc.php');
                                phpmv_user_unset($user_id);
                                break;
                        }
                        $query = "DELETE FROM cms_user_role WHERE user_id='$user_id' AND role_id='$role_id'";
                    }
                    $ret = $db->query($query);
                    //					echo "$query";
                    //					var_dump($ret);
                }
                else {
                    //					echo "role $role_id unchanged, checkig passes";
                    if (@$_POST[$role] && $role_id == 3 && $ftp_pass) {
                        $query = "SELECT role_params FROM cms_user_role WHERE user_id='$user_id' AND role_id=3";
                        $rp = $db->getOne($query);
                        $rp = unserialize($rp);
                        update_ftp_user($rp["ftp_user_id"], $ftp_pass);
                    }
                }
            }
            // create mailbox if present
/*			if(isset($_POST["mail"])) {
			  // check if email exists first
			  $query = "SELECT postfix FROM sms_domain WHERE domain_id='$domain_id'";
				$postfix = $db->getOne($query);
				$login = $p["login"]."-".$postfix;
				$query = "SELECT 1 FROM plg_mailbox WHERE login='$login'";
				$ret = $db->getOne($query);
				if(!$ret) { // create mailbox if not exists
					$mb["name"] = $p["login"];
					$mb["login"] = $p["login"]."-".$postfix;
					$query = "INSERT plg_mailbox SET ".make_set_clause($mb).", domain_id='$domain_id'";
					$db->query($query);
					add_mailbox($p["login"], $mail_pass);
					generate_sendmail_files();
				}
			} */
            break;
        case "delete":
            $user_id = intval(@$_POST["id"]);

            // remove FTP if any
            $query = "SELECT role_params FROM cms_user_role WHERE user_id='$user_id' AND role_id=3";
            $rp = $db->getOne($query);
            if ($rp) {
                $rp = unserialize($rp);
                remove_ftp_user($rp["ftp_user_id"]);
            }

            include_once('inc/piwik_adapter.inc.php');
            piwik_delete_user($user_id);
            //phpmv logic
            include_once('inc/phpmv_adapter.inc.php');
            phpmv_user_unset($user_id);

            // remove associated roles
            $query = "DELETE FROM cms_user_role WHERE user_id='$user_id'";
            $db->query($query);

            // remove user
            $query = "DELETE FROM sms_user WHERE user_id='$user_id'";
            $db->query($query);

            echo "ok";
            break;
    }
}

function enabled($enabled) {
    if ($enabled) {
        return "active";
    }
    else {
        return "disabled";
    }
}

function add_ftp_user($login, $passwd) {
    global $domain_id, $db;
    // connect to DB
    $db_con = createobject("db_ftp");
    $dbf = $db_con->connect();
    // fetch domain_name
    $query = "SELECT domain_name, postfix FROM sms_domain WHERE domain_id='$domain_id'";
    $domain = $db->getRow($query, DB_FETCHMODE_ASSOC);
    $username = $login . "-" . $domain["postfix"];
    $ftpdir = "/home/ttt-ws-production/www/files/" . $domain["domain_name"];
    $query = "INSERT accounts SET username='$username', passwd='$passwd', uid=33, gid=33, homedir='$ftpdir', shell='/usr/sbin/nologin'";
    $dbf->query($query);
    $query = "SELECT last_insert_id()";
    $ftp_user_id = $dbf->getOne($query);
    //$dbf->disconnect();
    return $ftp_user_id;
}

function update_ftp_user($user_id, $newpasswd) {
    global $domain_id, $db;

    $db_con = createobject("db_ftp");
    $dbf = $db_con->connect();
    $query = "UPDATE accounts SET passwd='$newpasswd' WHERE user_id='$user_id'";
    $dbf->query($query);
    $dbf->disconnect();
}

function remove_ftp_user($ftp_user_id) {
    // connect to DB
    $db_con = createobject("db_ftp");
    $dbf = $db_con->connect();
    $query = "DELETE FROM accounts WHERE user_id='$ftp_user_id'";
    $dbf->query($query);
    $dbf->disconnect();
}

