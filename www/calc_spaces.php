<?php
 // script calculates space used by domains and put it to DB
include("inc/verse.inc.php"); //main header - initializes Verse environment

if (!isset($_GET["all"])) {
    $path = "files/$domain_name";
    $space_consumed = dirsize($path);

    // save to DB
    $query = "UPDATE sms_domain SET space_consumed='$space_consumed' WHERE domain_id='$domain_id'";
    $db->query($query);
}
else {
    $query = "SELECT domain_id, domain_name FROM sms_domain WHERE alias_domain_id=0";
    $domains = $db->getAll($query, DB_FETCHMODE_ASSOC);
    foreach ($domains as $domain) {
        $space_consumed = dirsize('files/' . $domain['domain_name']);
        $query = "UPDATE sms_domain SET space_consumed='$space_consumed' WHERE domain_id='" . $domain['domain_id'] . "'";
        $db->query($query);
    }

    // save last calculation time
    $query = "REPLACE cms_config SET domain_id=0, param='sites_space_consumed_last_calc', value=now()";
    $db->query($query);
}

header("Location: backend.php/domains");

/**
 * Calculate the size of a directory by iterating its contents
 *
 * New algorithm means only 1 branch is open in memory at a time
 * so extremely large directories can be calculated quickly.
 *
 * @author      Aidan Lister <aidan@php.net>
 * @version     1.2.0
 * @link        http://aidanlister.com/repos/v/function.dirsize.php
 * @param       string   $directory    Path to directory
 */
function dirsize($path) {
    // Init
    $size = 0;

    // Trailing slash
    if (substr($path, -1, 1) !== DIRECTORY_SEPARATOR) {
        $path .= DIRECTORY_SEPARATOR;
    }

    // Sanity check
    if (is_file($path)) {
        return filesize($path);
    } elseif (!is_dir($path)) {
        return false;
    }

    // Iterate queue
    $queue = array($path);
    for ($i = 0, $j = count($queue); $i < $j; ++$i)
    {
        // Open directory
        $parent = $i;
        if (is_dir($queue[$i]) && $dir = @dir($queue[$i])) {
            $subdirs = array();
            while (false !== ($entry = $dir->read())) {
                // Skip pointers
                if ($entry == '.' || $entry == '..') {
                    continue;
                }

                // Get list of directories or filesizes
                $path = $queue[$i] . $entry;
                if (is_dir($path)) {
                    $path .= DIRECTORY_SEPARATOR;
                    $subdirs[] = $path;
                } elseif (is_file($path)) {
                    $size += filesize($path);
                }
            }

            // Add subdirectories to start of queue
            unset($queue[0]);
            $queue = array_merge($subdirs, $queue);

            // Recalculate stack size
            $i = -1;
            $j = count($queue);

            // Clean up
            $dir->close();
            unset($dir);
        }
    }

    return $size;
}
	