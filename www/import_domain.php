<?php
/**
 * Created by PhpStorm.
 * User: Vladimir Droznik
 * Date: 10.12.2010
 */

include("inc/verse.inc.php"); //main header - initializes Verse environment
$import_from = "exports/santangelo.ws1.twintierstech.net";

if ($user->is_super()) {
    $action = @$_POST["action"];
    switch ($action) {
        case "import":
            $domain_id_to = $_POST['domain_id'];
            $data = unserialize(file_get_contents($import_from));
            if (isset($data['site'])) {
                $site = $data['site'];
                $site['domain_id'] = $domain_id_to;
                $fields = array("domain_id", "title", "properties", "css", "header_mode", "header", "main_menu", "popup_menu", "footer");
                echo "importing: site";
                $values = array();
                foreach ($fields as $field) {
                    $values[] = '"' . in($site[$field]) . '"';
                }
                $query = "REPLACE cms_site (" . implode(", ", $fields) . ") VALUES (" . implode(", ", $values) . ")";
                $ret = $db->query($query);
                if (DB::isError($ret)) {
                    echo "DB error:", $ret->getMessage(), ';', $ret->getUserInfo();
                    exit;
                }
                echo " - done<br>";
            }
            $pagemap = array();
            if (isset($data['page'])) {
                echo "importing: pages";
                // clear old pages
                $query = "DELETE FROM cms_page WHERE domain_id='$domain_id_to'";
                $db->query($query);
                $pages = $data['page'];
                $fields = array("domain_id", "ord", "page_type", "internal_name", "no_header", "data", "content", "restricted", "properties", "custom_header");
                $c = 0;
                foreach ($pages as $page) {
                    $page['domain_id'] = $domain_id_to;
                    $values = array();
                    foreach ($fields as $field) {
                        $values[] = '"' . in($page[$field]) . '"';
                    }
                    $query = "INSERT cms_page (" . implode(", ", $fields) . ") VALUES (" . implode(", ", $values) . ")";
                    $ret = $db->query($query);
                    if (DB::isError($ret)) {
                        echo "DB error:", $ret->getMessage(), ';', $ret->getUserInfo();
                        exit;
                    }
                    $pagemap[$page['page_id']] = $db->getOne("SELECT last_insert_id()");
                    $c++;
                }
                echo " - done<br>";
            }
            if (isset($data['node'])) {
                echo "importing: nodes";
                $query = "DELETE FROM cms_node WHERE domain_id='$domain_id_to'";
                $db->query($query);
                $nodes = $data['node'];
                $fields = array("domain_id", "page_id", "ord", "depth", "menu_type", "display_name", "params");
                $c = 0;
                foreach ($nodes as $node) {
                    $node['domain_id'] = $domain_id_to;
                    if (isset($pagemap[$node['page_id']])) {
                        $node['page_id'] = $pagemap[$node['page_id']];
                    }
                    $values = array();
                    foreach ($fields as $field) {
                        if (!is_null($node[$field])) {
                            $values[] = '"' . in($node[$field]) . '"';
                        }
                        else {
                            $values[] = "null";
                        }
                    }
                    $query = "INSERT cms_node (" . implode(", ", $fields) . ") VALUES (" . implode(", ", $values) . ")";
                    $ret = $db->query($query);
                    if (DB::isError($ret)) {
                        echo "DB error:", $ret->getMessage(), ';', $ret->getUserInfo();
                        exit;
                    }
                }
                echo " - done<br>";
            }
//            if(isset($data['obit_config'])) {
//                echo "importing: obit_config";
//                $query = "DELETE FROM plg_obituary_config WHERE domain_id='$domain_id_to'";
//                $db->query($query);
//            }
            break;
        default:
            // get domain list
            $query = "SELECT domain_id, domain_name, mode FROM sms_domain WHERE mode>0 AND alias_domain_id=0 ORDER BY domain_name";
            $ret = $db->getAll($query, DB_FETCHMODE_ASSOC);
            $domains = array();
            foreach ($ret as $row) {
                $domains[$row['domain_id']] = $row['domain_name'] . ' (' . $row['mode'] . ')';
            }
            $smarty->assign("domains", $domains);
            $smarty->assign("from", $import_from);
            $smarty->display("import_domain.tpl");
    }
}
else {
    echo "Access denied";
}
