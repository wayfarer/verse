<?php
 // domain transfer script
// called by remote script, transfer2.php
include("inc/verse.inc.php"); //main header - initializes Verse environment

$table_map = array(
    'sms_domain' => array('domain_id', 'domain_name', 'enabled', 'email', 'postfix', 'email_enabled', 'ecommerce_enabled', 'price_enabled', 'paypal_checkout', 'affiliates_enabled', 'mode', 'alias_domain_id'),
    'cms_site' => array('domain_id', 'title', 'properties', 'css', 'header_mode', 'header', 'main_menu', 'popup_menu', 'footer'),
    'cms_node' => array('node_id', 'internal_name', 'domain_id', 'page_id', 'ord', 'depth', 'menu_type', 'display_name', 'params'),
    'cms_page' => array('page_id', 'domain_id', 'ord', 'page_type', 'internal_name', 'no_header', 'data', 'restricted', 'properties'),
    'cms_page_uc' => array('page_id', 'domain_id', 'page_type', 'name', 'no_header', 'data', 'properties', 'ord'),
    'cms_site_uc' => array('domain_id', 'title', 'properties', 'css', 'header_mode', 'header', 'footer'),
    'sms_user' => array('user_id', 'login', 'password', 'enabled', 'domain_id', 'name', 'email'),
    'plg_event' => array('event_id', 'event_case_id', 'domain_id', 'obituary_id', 'event_type', 'timestamp', 'description'),
    'plg_list' => array('list_id', 'guid', 'domain_id', 'type', 'name', 'company', 'address', 'address2', 'phone', 'fax', 'website', 'email', 'description'),
    'plg_movieclip' => array('movieclip_id', 'domain_id', 'title', 'description', 'filename', 'timestamp', 'protected', 'password', 'status', 'tmpfilename', 'origfilename'),
    'plg_obituary' => array('obituary_id', 'domain_id', 'obituary_case_id', 'first_name', 'middle_name', 'last_name', 'home_place', 'death_date', 'birth_place', 'birth_date', 'service_date', 'service_time', 'service_place', 'visitation_date', 'visitation_place', 'final_disposition', 'image', 'obit_text', 'candles_policy'),
    'plg_obituary_candle' => array('candle_id', 'candle_case_id', 'domain_id', 'obituary_id', 'name', 'email', 'ip', 'thoughts', 'timestamp', 'state', 'hash'),
    'plg_obituary_config' => array('domain_id', 'param', 'value'),
    'plg_product' => array('product_id', 'guid', 'domain_id', 'category_id', 'name', 'price', 'quantity', 'image', 'description', 'sold_times', 'timestamp', 'enabled'),
    'plg_product_category' => array('category_id', 'domain_id', 'name'),
    'plg_product_config' => array('domain_id', 'param', 'value'),
    'plg_product_order' => array('order_id', 'domain_id', 'timestamp', 'total', 'payer', 'affiliate_id', 'affiliate_from', 'user_data', 'status'),
    'plg_slide' => array('slide_id', 'domain_id', 'obituary_id', 'image', 'description')
);
$table_map_dep = array(
    'cms_user_role' => array('table' => 'sms_user', 'key' => 'user_id', 'fields' => array('user_id', 'role_id', 'role_params')),
    'plg_obituary_movieclip' => array('table' => 'plg_obituary', 'key' => 'obituary_id', 'fields' => array('obituary_id', 'movieclip_id')),
);

$table_map_ftpdb = array(
    'accounts' => array("user_id", "username", "passwd", "uid", "gid", "ftpdir", "homedir")
);

$reply = array();
$action = @$_GET["action"];
switch ($action) {
    case "list_domains":
        $query = "SELECT domain_id, domain_name FROM sms_domain WHERE alias_domain_id=0 ORDER BY domain_name";
        $domains = $db->getAssoc($query);
        $reply["domains"] = $domains;
        break;
    case "info":
        $domain_id = $_GET["id"];
        if ($domain_id) {
            $info = array();
            $query = "SELECT domain_name FROM sms_domain WHERE domain_id='$domain_id'";
            $domain_name = $db->getOne($query);
            $path = "files/$domain_name/";
            // calculate files size
            $info["dirsize"] = dirsize($path);
            // calculate page count
            $query = "SELECT count(*) FROM cms_page WHERE domain_id='$domain_id'";
            $info["page_count"] = $db->getOne($query);
            // calculate obituaries count
            $query = "SELECT count(*) FROM plg_obituary WHERE domain_id='$domain_id'";
            $info["obituary_count"] = $db->getOne($query);
            // calculate slides count
            $query = "SELECT count(*) FROM plg_slide WHERE domain_id='$domain_id'";
            $info["slide_count"] = $db->getOne($query);
            // calculate candles count
            $query = "SELECT count(*) FROM plg_obituary_candle WHERE domain_id='$domain_id'";
            $info["candle_count"] = $db->getOne($query);
            // calculate calendar events count
            $query = "SELECT count(*) FROM plg_event WHERE domain_id='$domain_id'";
            $info["event_count"] = $db->getOne($query);
            // calculate products count
            $query = "SELECT count(*) FROM plg_product WHERE domain_id='$domain_id'";
            $info["product_count"] = $db->getOne($query);
            // calculate orders count
            $query = "SELECT count(*) FROM plg_product_order WHERE domain_id='$domain_id'";
            $info["order_count"] = $db->getOne($query);
            // calculate list items count
            $query = "SELECT count(*) FROM plg_list WHERE domain_id='$domain_id'";
            $info["list_count"] = $db->getOne($query);
            // calculate movieclips count
            $query = "SELECT count(*) FROM plg_movieclip WHERE domain_id='$domain_id'";
            $info["movieclip_count"] = $db->getOne($query);
            // calculate users count
            $query = "SELECT count(*) FROM sms_user WHERE domain_id='$domain_id'";
            $info["user_count"] = $db->getOne($query);

            $reply["info"] = $info;
        }
        break;
    case "export":
        $domain_id = $_GET["id"];
        $export = array();
        if ($domain_id) {
            // read main tables
            foreach ($table_map as $table => $fields) {
                $query = "SELECT " . implode(",", $fields) . " FROM $table WHERE domain_id='$domain_id'";
                $ret = $db->getAll($query, DB_FETCHMODE_ASSOC);
                if (!DB::isError($ret)) {
                    if ($ret) {
                        $export[$table] = $ret;
                    }
                }
                else {
                    echo "DB Error!";
                    var_dump($ret);
                    exit;
                }
            }
            // read dependent table datas
            foreach ($table_map_dep as $table => $dep) {
                // make keys array
                $keys = array();
                if ($export[$dep['table']]) {
                    foreach ($export[$dep['table']] as $row) {
                        $keys[] = $row[$dep['key']];
                    }
                }
                // fetch data
                if ($keys) {
                    $query = "SELECT " . implode(",", $dep['fields']) . " FROM $table WHERE " . $dep['key'] . " IN (" . implode(",", $keys) . ")";
                    $ret = $db->getAll($query, DB_FETCHMODE_ASSOC);
                    if (!DB::isError($ret)) {
                        if ($ret) {
                            $export[$table] = $ret;
                        }
                    }
                    else {
                        echo "DB Error!";
                        var_dump($ret);
                        exit;
                    }
                }
            }
            /*
            // read data from ftpdb database
              $dbf_con = createobject("db_ftp");
                $dbf = $dbf_con->connect();
            // find user_id's of account from ftpdb to read
            $ftp_user_ids = array();
            if($export["cms_user_role"]) {
              foreach($export["cms_user_role"] as $user_role) {
                if($user_role["role_id"] == 3) { // ftp role params
                  $role_params = unserialize($user_role["role_params"]);
                  if($role_params["ftp_user_id"]) {
                    $ftp_user_ids[] = $role_params["ftp_user_id"];
                  }
                }
              }
            }

            $export_ftpdb = array();
            if($ftp_user_ids) {
              $query = "SELECT ".implode(",",$table_map_ftpdb["accounts"])." FROM accounts WHERE user_id IN (".implode(",",$ftp_user_ids).")";
              $ret = $dbf->getAll($query, DB_FETCHMODE_ASSOC);
              if(!DB::isError($ret)) {
                if($ret) {
                  $export_ftpdb = $ret;
                }
              }
              else {
                echo "DB Error!";
                var_dump($ret);
                exit;
              }
            }
            */
            //        $reply["export"] = array("versesms"=>$export, "ftpdb"=>$export_ftpdb);
            $reply["export"] = array("versesms" => $export, "ftpdb" => array());
        }
        break;
}

echo serialize($reply);


/**
 * Calculate the size of a directory by iterating its contents
 *
 * New algorithm means only 1 branch is open in memory at a time
 * so extremely large directories can be calculated quickly.
 *
 * @author      Aidan Lister <aidan@php.net>
 * @version     1.2.0
 * @link        http://aidanlister.com/repos/v/function.dirsize.php
 * @param       string   $directory    Path to directory
 */
function dirsize($path) {
    // Init
    $size = 0;

    // Trailing slash
    if (substr($path, -1, 1) !== DIRECTORY_SEPARATOR) {
        $path .= DIRECTORY_SEPARATOR;
    }

    // Sanity check
    if (is_file($path)) {
        return filesize($path);
    } elseif (!is_dir($path)) {
        return false;
    }

    // Iterate queue
    $queue = array($path);
    for ($i = 0, $j = count($queue); $i < $j; ++$i)
    {
        // Open directory
        $parent = $i;
        if (is_dir($queue[$i]) && $dir = @dir($queue[$i])) {
            $subdirs = array();
            while (false !== ($entry = $dir->read())) {
                // Skip pointers
                if ($entry == '.' || $entry == '..') {
                    continue;
                }

                // Get list of directories or filesizes
                $path = $queue[$i] . $entry;
                if (is_dir($path)) {
                    $path .= DIRECTORY_SEPARATOR;
                    $subdirs[] = $path;
                } elseif (is_file($path)) {
                    $size += filesize($path);
                }
            }

            // Add subdirectories to start of queue
            unset($queue[0]);
            $queue = array_merge($subdirs, $queue);

            // Recalculate stack size
            $i = -1;
            $j = count($queue);

            // Clean up
            $dir->close();
            unset($dir);
        }
    }

    return $size;
}
