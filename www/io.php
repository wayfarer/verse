<?php
  include("inc/verse.inc.php"); //main header - initializes Verse environment

$page = @$_GET["p"];
if (!$page) {
    // resolve obituary_search page for the domain and fetch it
    $query = "SELECT data, page_type, no_header, restricted FROM cms_page WHERE page_type=3 AND domain_id='$domain_id'";
    $cnt = $db->getRow($query, DB_FETCHMODE_ASSOC);
    if (DB::isError($cnt) || !$cnt) {
        // default obituary_search
        $cnt = array("data" => serialize(array("content1" => "{obituary_search style=\"style1\"}")), "page_type" => 3, "no_header" => 0, "restricted" => 0);
    }
}
else {
    // fetch content
    $query = "SELECT data, page_type, no_header, restricted FROM cms_page WHERE internal_name='$page' AND domain_id='$domain_id'";
    $cnt = $db->getRow($query, DB_FETCHMODE_ASSOC);
    if (DB::isError($cnt) || !$cnt) {
        // default obituary_search
        $cnt = array("data" => serialize(array("content1" => "{obituary_view style=\"stylenew\"}")), "page_type" => 4, "no_header" => 0, "restricted" => 0);
    }
}
// process content with chosen plugin
include "cnt_plugins_map.php";
$content_type = $plugins_map[$cnt["page_type"]];
$plugin_name = "cnt_plugin_$content_type.php";
if (!file_exists($plugin_name)) {
    $content_type = "static";
    $plugin_name = "cnt_plugin_$content_type.php";
}
include $plugin_name;
$state = array_merge($_GET, $_POST);
unset($state["p"]);
$content = process_content(@unserialize($cnt["data"]), $state);

// fetch CSS
$query = "SELECT css FROM cms_site WHERE domain_id='$domain_id'";
$site = $db->getRow($query, DB_FETCHMODE_ASSOC);

$smarty->assign("site", $site);
$smarty->assign("content", $content);
$smarty->display("integrate.tpl");
