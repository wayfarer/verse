<?php
 // remote ecommerce embedding
include("inc/verse.inc.php"); //main header - initializes Verse environment

$page = @$_GET["p"];
if (!$page) $page = "products";

// fetch content
$query = "SELECT data, page_type, no_header, restricted FROM cms_page WHERE internal_name='$page' AND domain_id='$domain_id'";
$cnt = $db->getRow($query, DB_FETCHMODE_ASSOC);

// predefined pages
if (!$cnt) {
    switch ($page) {
        case "cart":
            $cnt = array("data" => "", "page_type" => 9, "no_header" => 0, "restricted" => 0);
            break;
        case "checkout":
            $cnt = array("data" => "", "page_type" => 10, "no_header" => 0, "restricted" => 0);
            break;
    }
}

$content = "";
if ($cnt) {
    // process content with chosen plugin
    include "cnt_plugins_map.php";
    $content_type = $plugins_map[$cnt["page_type"]];
    $plugin_name = "cnt_plugin_$content_type.php";
    if (!file_exists($plugin_name)) {
        $content_type = "static";
        $plugin_name = "cnt_plugin_$content_type.php";
    }
    include $plugin_name;
    $state = array_merge($_GET, $_POST);
    unset($state["p"]);
    $content = process_content(@unserialize($cnt["data"]), $state);
}
if (!$content) $content = "&nbsp;";

$smarty->assign("content", $content);
$smarty->display("integrate.tpl");
